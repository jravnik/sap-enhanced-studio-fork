﻿using SAP.Copernicus.Core.Util;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: InternalsVisibleTo("CopernicusTest")]
[assembly: AssemblyFileVersion("142.0.3211.0035")]
[assembly: AssemblyTitle("Copernicus")]
[assembly: AssemblyDescription("")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: SuppressMessage("Microsoft.Design", "CA1017:MarkAssembliesWithComVisible")]
[assembly: AssemblyCSN("AP-RC-BDS")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SAP AG")]
[assembly: AssemblyProduct("SAP® Business ByDesign™")]
[assembly: AssemblyCopyright("© 2011 SAP AG. All rights reserved.")]
[assembly: AssemblyTrademark("SAP and Business ByDesign are trademark(s) or registered trademark(s) of SAP AG in Germany and in several other countries.")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(false)]
[assembly: AssemblyVersion("142.0.3211.35")]
