﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.RepositoryViewWindow
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SAP.Copernicus
{
  [Guid("0128a988-1402-4c49-a09b-df226597dc6c")]
  public class RepositoryViewWindow : CopernicusToolWindowPane
  {
    private RepositoryViewControl control;

    public RepositoryViewWindow()
      : base(HELP_IDS.BDS_MYSOLUTIONS_SELECTION)
    {
      this.Caption = CopernicusResources.ToolWindowTitle;
      this.BitmapResourceID = 302;
      this.BitmapIndex = 0;
      this.control = new RepositoryViewControl(this);
    }

    public override IWin32Window Window
    {
      get
      {
        return (IWin32Window) this.control;
      }
    }
  }
}
