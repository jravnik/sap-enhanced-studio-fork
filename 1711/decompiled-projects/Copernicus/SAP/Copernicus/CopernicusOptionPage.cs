﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusOptionPage
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.ToolOptions;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SAP.Copernicus
{
  [ComVisible(true)]
  [Guid("1A0791D6-3987-4f49-97B6-3AC92E59D9D6")]
  [ClassInterface(ClassInterfaceType.AutoDual)]
  [CLSCompliant(false)]
  public class CopernicusOptionPage : OptionPageFields
  {
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    protected override IWin32Window Window
    {
      get
      {
        CopernicusOptionDialog copernicusOptionDialog = new CopernicusOptionDialog(this);
        copernicusOptionDialog.Initialize();
        return (IWin32Window) copernicusOptionDialog;
      }
    }
  }
}
