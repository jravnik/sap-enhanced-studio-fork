﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.AboutBox.AboutBoxTexts
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Properties;
using System.Reflection;
using System.Resources;

namespace SAP.Copernicus.AboutBox
{
  internal static class AboutBoxTexts
  {
    private static Assembly assembly = Assembly.GetExecutingAssembly();
    private static string buildNumber = PropertyAccess.GeneralProps.buildNumber;
    private static string buildDate = PropertyAccess.GeneralProps.buildDate;
    private static string repositoryVersion = PropertyAccess.GeneralProps.RepositoryVersion;
    private static string logonPatchLevel = PropertyAccess.GeneralProps.LogonPatchLevel;
    private static string release = PropertyAccess.GeneralProps.RepositoryRelease;
    public static ResourceManager txtMan = new ResourceManager("SAP.Copernicus.CopernicusResources", AboutBoxTexts.assembly);

    public static string GetString(string txtSymbol)
    {
      return AboutBoxTexts.txtMan.GetString(txtSymbol);
    }

    public static string GetBuildNumber
    {
      get
      {
        return AboutBoxTexts.buildNumber;
      }
    }

    public static string GetRelease
    {
      get
      {
        return AboutBoxTexts.release;
      }
    }

    public static string GetBuildDate
    {
      get
      {
        if (AboutBoxTexts.buildDate == null)
          return AboutBoxTexts.buildDate;
        return AboutBoxTexts.buildDate + " (UTC)";
      }
    }

    public static string GetRepositoryVersion
    {
      get
      {
        return AboutBoxTexts.repositoryVersion;
      }
    }

    public static string GetLogonPatchLevel
    {
      get
      {
        return AboutBoxTexts.logonPatchLevel;
      }
    }
  }
}
