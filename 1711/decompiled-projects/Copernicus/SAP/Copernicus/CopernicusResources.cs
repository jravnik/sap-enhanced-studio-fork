﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusResources
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus
{
    [CompilerGenerated]
    [DebuggerNonUserCode]
    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    public class CopernicusResources
    {
        private static ResourceManager resourceMan;
        private static CultureInfo resourceCulture;

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CopernicusResources()
        {
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public static ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals((object)CopernicusResources.resourceMan, (object)null))
                    CopernicusResources.resourceMan = new ResourceManager("SAP.Copernicus.CopernicusResources", typeof(CopernicusResources).Assembly);
                return CopernicusResources.resourceMan;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public static CultureInfo Culture
        {
            get
            {
                return CopernicusResources.resourceCulture;
            }
            set
            {
                CopernicusResources.resourceCulture = value;
            }
        }

        public static Bitmap _180Warning
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject("180Warning", CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap _21BONode_16x16
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject("21BONode_16x16", CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap _22Association16x16
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject("22Association16x16", CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap _254HorizontalWindows16x16
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject("254HorizontalWindows16x16", CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap _255VerticalWindows16x16
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject("255VerticalWindows16x16", CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap _271help16x16
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject("271help16x16", CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap Action
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(Action), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap Activate
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(Activate), CopernicusResources.resourceCulture);
            }
        }

        public static string ActivateContentTitle
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(ActivateContentTitle), CopernicusResources.resourceCulture);
            }
        }

        public static string ActivateFormsGroup
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(ActivateFormsGroup), CopernicusResources.resourceCulture);
            }
        }

        public static string ActivatePrintForms
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(ActivatePrintForms), CopernicusResources.resourceCulture);
            }
        }

        public static string BAdIFolderName
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(BAdIFolderName), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap BOInstance16x16
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(BOInstance16x16), CopernicusResources.resourceCulture);
            }
        }

        public static string Btn_Cancel
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(Btn_Cancel), CopernicusResources.resourceCulture);
            }
        }

        public static string Btn_Ok
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(Btn_Ok), CopernicusResources.resourceCulture);
            }
        }

        public static string BYD_STUDIO
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(BYD_STUDIO), CopernicusResources.resourceCulture);
            }
        }

        public static string Call_VS_Failed
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(Call_VS_Failed), CopernicusResources.resourceCulture);
            }
        }

        public static string CanNotCreateWindow
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(CanNotCreateWindow), CopernicusResources.resourceCulture);
            }
        }

        public static string CheckContentTitle
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(CheckContentTitle), CopernicusResources.resourceCulture);
            }
        }

        public static string CheckInAllFailed
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(CheckInAllFailed), CopernicusResources.resourceCulture);
            }
        }

        public static string CleanContentTitle
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(CleanContentTitle), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap ClearSearch16x16
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(ClearSearch16x16), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap CloseSolution
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(CloseSolution), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap Copy
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(Copy), CopernicusResources.resourceCulture);
            }
        }

        public static string copyright2008MS
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(copyright2008MS), CopernicusResources.resourceCulture);
            }
        }

        public static string copyright2008SAP
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(copyright2008SAP), CopernicusResources.resourceCulture);
            }
        }

        public static string copyright2010MS
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(copyright2010MS), CopernicusResources.resourceCulture);
            }
        }

        public static string copyright2010SAP
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(copyright2010SAP), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap CreateBO
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(CreateBO), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap CreateSolution
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(CreateSolution), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap Delete
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(Delete), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap DeleteSolution
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(DeleteSolution), CopernicusResources.resourceCulture);
            }
        }

        public static string DialogTitle
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(DialogTitle), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap Edit
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(Edit), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap EditImpl
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(EditImpl), CopernicusResources.resourceCulture);
            }
        }

        public static string FormCreateBO_ExistenceValidation_Error
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(FormCreateBO_ExistenceValidation_Error), CopernicusResources.resourceCulture);
            }
        }

        public static string FormCreateBO_NameValidation_Caption
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(FormCreateBO_NameValidation_Caption), CopernicusResources.resourceCulture);
            }
        }

        public static string FormCreateBO_NameValidation_Error
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(FormCreateBO_NameValidation_Error), CopernicusResources.resourceCulture);
            }
        }

        public static string FormCreateBOName
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(FormCreateBOName), CopernicusResources.resourceCulture);
            }
        }

        public static string FormCreateBOTitle
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(FormCreateBOTitle), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap GenerateBO
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(GenerateBO), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap GoToNextHS
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(GoToNextHS), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap GoToPreviousHS
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(GoToPreviousHS), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap Help
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(Help), CopernicusResources.resourceCulture);
            }
        }

        public static string labelBuildDate
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(labelBuildDate), CopernicusResources.resourceCulture);
            }
        }

        public static string labelBuildNumber
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(labelBuildNumber), CopernicusResources.resourceCulture);
            }
        }

        public static string LoadRepositoryFailed
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(LoadRepositoryFailed), CopernicusResources.resourceCulture);
            }
        }

        public static string Logoff
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(Logoff), CopernicusResources.resourceCulture);
            }
        }

        public static string Logon
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(Logon), CopernicusResources.resourceCulture);
            }
        }

        public static string LogonPatchLevel
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(LogonPatchLevel), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap Method
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(Method), CopernicusResources.resourceCulture);
            }
        }

        public static string MySolCtxMenuCreateSol
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(MySolCtxMenuCreateSol), CopernicusResources.resourceCulture);
            }
        }

        public static string NameCharError
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(NameCharError), CopernicusResources.resourceCulture);
            }
        }

        public static string NameDescError
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(NameDescError), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap OpenSolution
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(OpenSolution), CopernicusResources.resourceCulture);
            }
        }

        public static string PCDdisabled
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(PCDdisabled), CopernicusResources.resourceCulture);
            }
        }

        public static string PCDenabled
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(PCDenabled), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap ProcessIntegration
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(ProcessIntegration), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap ProcessIntegrationActivate
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(ProcessIntegrationActivate), CopernicusResources.resourceCulture);
            }
        }

        public static string ProdBugFixDisclaimer
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(ProdBugFixDisclaimer), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap Properties
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(Properties), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap Query
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(Query), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap Refresh
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(Refresh), CopernicusResources.resourceCulture);
            }
        }

        public static string RefreshSolutionTitle
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(RefreshSolutionTitle), CopernicusResources.resourceCulture);
            }
        }

        public static string RefreshText
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(RefreshText), CopernicusResources.resourceCulture);
            }
        }

        public static string RepositoryLoading
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(RepositoryLoading), CopernicusResources.resourceCulture);
            }
        }

        public static string RepositoryLogon
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(RepositoryLogon), CopernicusResources.resourceCulture);
            }
        }

        public static string RepositoryVersion
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(RepositoryVersion), CopernicusResources.resourceCulture);
            }
        }

        public static string SaveFile
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(SaveFile), CopernicusResources.resourceCulture);
            }
        }

        public static string SuccessMessageTitle
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(SuccessMessageTitle), CopernicusResources.resourceCulture);
            }
        }

        public static string TaskCheckedSuccessfully
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TaskCheckedSuccessfully), CopernicusResources.resourceCulture);
            }
        }

        public static string TaskCleanedSuccessfully
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TaskCleanedSuccessfully), CopernicusResources.resourceCulture);
            }
        }

        public static string TaskDepolyedSuccessfully
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TaskDepolyedSuccessfully), CopernicusResources.resourceCulture);
            }
        }

        public static string titleAboutBox
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(titleAboutBox), CopernicusResources.resourceCulture);
            }
        }

        public static string TNCtxMenuCreateBO
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TNCtxMenuCreateBO), CopernicusResources.resourceCulture);
            }
        }

        public static string TNCtxMenuCreateScript
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TNCtxMenuCreateScript), CopernicusResources.resourceCulture);
            }
        }

        public static string TNCtxMenuDeleteBO
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TNCtxMenuDeleteBO), CopernicusResources.resourceCulture);
            }
        }

        public static string TNCtxMenuDeleteScript
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TNCtxMenuDeleteScript), CopernicusResources.resourceCulture);
            }
        }

        public static string TNCtxMenuDeleteSolution
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TNCtxMenuDeleteSolution), CopernicusResources.resourceCulture);
            }
        }

        public static string TNCtxMenuEditBO
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TNCtxMenuEditBO), CopernicusResources.resourceCulture);
            }
        }

        public static string TNCtxMenuGenerateBO
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TNCtxMenuGenerateBO), CopernicusResources.resourceCulture);
            }
        }

        public static string TNCtxMenuReadScript
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TNCtxMenuReadScript), CopernicusResources.resourceCulture);
            }
        }

        public static string TNCtxMenuRefresh
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TNCtxMenuRefresh), CopernicusResources.resourceCulture);
            }
        }

        public static string TNCtxMenuViewBO
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TNCtxMenuViewBO), CopernicusResources.resourceCulture);
            }
        }

        public static string TNCtxMenuViewDT
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TNCtxMenuViewDT), CopernicusResources.resourceCulture);
            }
        }

        public static string ToolWindowTitle
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(ToolWindowTitle), CopernicusResources.resourceCulture);
            }
        }

        public static string TreeNodeTextActionFolder
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TreeNodeTextActionFolder), CopernicusResources.resourceCulture);
            }
        }

        public static string TreeNodeTextBO
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TreeNodeTextBO), CopernicusResources.resourceCulture);
            }
        }

        public static string TreeNodeTextDeterminationFolder
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TreeNodeTextDeterminationFolder), CopernicusResources.resourceCulture);
            }
        }

        public static string TreeNodeTextDT
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(TreeNodeTextDT), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap View
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(View), CopernicusResources.resourceCulture);
            }
        }

        public static Bitmap View1
        {
            get
            {
                return (Bitmap)CopernicusResources.ResourceManager.GetObject(nameof(View1), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitActivate
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitActivate), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitBOGenerate
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitBOGenerate), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitCaption
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitCaption), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitCheck
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitCheck), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitClean
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitClean), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitDeleteSolution
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitDeleteSolution), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitOpenSolution
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitOpenSolution), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitPIActivate
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitPIActivate), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitPIClean
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitPIClean), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitRefresh
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitRefresh), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitScriptCreate
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitScriptCreate), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitScriptDelete
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitScriptDelete), CopernicusResources.resourceCulture);
            }
        }

        public static string WaitScriptRead
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WaitScriptRead), CopernicusResources.resourceCulture);
            }
        }

        public static string WebServiceActivatedSuccesfully
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WebServiceActivatedSuccesfully), CopernicusResources.resourceCulture);
            }
        }

        public static string WebServiceCheckedSuccesfully
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WebServiceCheckedSuccesfully), CopernicusResources.resourceCulture);
            }
        }

        public static string WebServiceCleanedSuccesfully
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(WebServiceCleanedSuccesfully), CopernicusResources.resourceCulture);
            }
        }

        public static string XBOMigrationSuccessfully
        {
            get
            {
                return CopernicusResources.ResourceManager.GetString(nameof(XBOMigrationSuccessfully), CopernicusResources.resourceCulture);
            }
        }
    }
}
