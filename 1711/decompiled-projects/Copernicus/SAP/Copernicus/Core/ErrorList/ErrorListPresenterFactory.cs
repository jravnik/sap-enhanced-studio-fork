﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ErrorList.ErrorListPresenterFactory
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text.Adornments;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;
using System;
using System.ComponentModel.Composition;

namespace SAP.Copernicus.Core.ErrorList
{
  [TextViewRole("DOCUMENT")]
  [Export(typeof (IWpfTextViewCreationListener))]
  [ContentType("any")]
  internal class ErrorListPresenterFactory : IWpfTextViewCreationListener
  {
    [Import]
    private IErrorProviderFactory SquiggleProviderFactory { get; set; }

    [Import(typeof (SVsServiceProvider))]
    private IServiceProvider ServiceProvider { get; set; }

    public void TextViewCreated(IWpfTextView textView)
    {
      textView.Properties.GetOrCreateSingletonProperty<ErrorListPresenter>((Func<ErrorListPresenter>) (() => new ErrorListPresenter(textView, this.SquiggleProviderFactory, this.ServiceProvider)));
    }
  }
}
