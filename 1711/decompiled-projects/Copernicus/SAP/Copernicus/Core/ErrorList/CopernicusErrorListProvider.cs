﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ErrorList.CopernicusErrorListProvider
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.BOCompiler.Common;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SAP.Copernicus.Core.ErrorList
{
  internal class CopernicusErrorListProvider : ErrorListProvider
  {
    private static object INSTANCE_LOCK = new object();
    private readonly Dictionary<string, ErrorListUpdated> eventTable = new Dictionary<string, ErrorListUpdated>();
    private static CopernicusErrorListProvider instance;
    private readonly IServiceProvider serviceProvider;

    internal static CopernicusErrorListProvider GetInstance(IServiceProvider serviceProvider)
    {
      lock (CopernicusErrorListProvider.INSTANCE_LOCK)
      {
        if (CopernicusErrorListProvider.instance == null)
          CopernicusErrorListProvider.instance = new CopernicusErrorListProvider(serviceProvider);
        return CopernicusErrorListProvider.instance;
      }
    }

    private CopernicusErrorListProvider(IServiceProvider serviceProvider)
      : base(serviceProvider)
    {
      if (serviceProvider == null)
        throw new ArgumentNullException(nameof (serviceProvider));
      this.serviceProvider = serviceProvider;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
        this.Tasks.Clear();
      base.Dispose(disposing);
    }

    private IVsHierarchy GetSelectedProject(string fileName)
    {
      if (string.IsNullOrEmpty(fileName))
        return (IVsHierarchy) null;
      if (!DTEUtil.IsInitialized)
        return (IVsHierarchy) null;
      Solution2 solution = DTEUtil.GetDTE().Solution as Solution2;
      if (solution == null || !solution.IsOpen)
        return (IVsHierarchy) null;
      ProjectItem projectItem = solution.FindProjectItem(fileName);
      if (projectItem == null)
        return (IVsHierarchy) null;
      return projectItem.ContainingProject != null ? projectItem.ContainingProject.Object as IVsHierarchy : projectItem.Object as IVsHierarchy;
    }

    internal IEnumerable<CopernicusErrorTask> GetErrorTasksForDocument(string fileName)
    {
      fileName = CopernicusErrorListProvider.ConvertPath(fileName);
      List<CopernicusErrorTask> copernicusErrorTaskList = new List<CopernicusErrorTask>();
      foreach (Microsoft.VisualStudio.Shell.Task task in this.Tasks)
      {
        CopernicusErrorTask copernicusErrorTask = task as CopernicusErrorTask;
        if (copernicusErrorTask != null && copernicusErrorTask.Document == fileName)
          copernicusErrorTaskList.Add(copernicusErrorTask);
      }
      return (IEnumerable<CopernicusErrorTask>) copernicusErrorTaskList;
    }

    private ErrorTask CreateErrorTaskItem(BOCompilerError error)
    {
      Origin origin;
      switch (error.ErrorLevel)
      {
        case BOErrorLevel.MIN_LEVEL:
          origin = Origin.BOCompilerLevel1;
          break;
        case BOErrorLevel.LEVEL2:
          origin = Origin.BOCompilerLevel2;
          break;
        case BOErrorLevel.LEVEL3:
          origin = Origin.BOCompilerLevel3;
          break;
        default:
          throw new ArgumentException("illegal error level ALL for new BO error task");
      }
      TextSpan span = new TextSpan();
      TaskErrorCategory errorCategory = error.IsWarning ? TaskErrorCategory.Warning : TaskErrorCategory.Error;
      span.iStartLine = span.iEndLine = Math.Max(error.Line, 0);
      span.iStartIndex = Math.Max(error.Column, 0);
      span.iEndIndex = span.iStartIndex + Math.Max(error.Length, 1);
      ErrorTask errorTask = (ErrorTask) new CopernicusErrorTask(span, error.FileName, error.Message, errorCategory, origin);
      IVsHierarchy selectedProject = this.GetSelectedProject(error.FileName);
      if (selectedProject != null)
      {
        errorTask.HierarchyItem = selectedProject;
        errorTask.Navigate += new EventHandler(this.ErrorTaskItemNavigate);
      }
      return errorTask;
    }

    private CopernicusErrorTask CreateErrorTaskItem(Task error)
    {
      TextSpan span = new TextSpan();
      TaskErrorCategory errorCategory;
      switch (error.Severity)
      {
        case Severity.Warning:
          errorCategory = TaskErrorCategory.Warning;
          break;
        case Severity.Error:
          errorCategory = TaskErrorCategory.Error;
          break;
        default:
          errorCategory = TaskErrorCategory.Message;
          break;
      }
      span.iStartLine = span.iEndLine = Math.Max(error.Line, 0);
      span.iStartIndex = Math.Max(error.Column, 0);
      span.iEndIndex = span.iStartIndex + Math.Max(error.Length, 1);
      string fileName = CopernicusErrorListProvider.ConvertPath(error.FileName);
      CopernicusErrorTask copernicusErrorTask = new CopernicusErrorTask(span, fileName, error.Message, errorCategory, error.Origin);
      IVsHierarchy selectedProject = this.GetSelectedProject(copernicusErrorTask.Document);
      if (error.NavigationHandler != null)
        copernicusErrorTask.Navigate += error.NavigationHandler;
      else if (selectedProject != null)
      {
        copernicusErrorTask.HierarchyItem = selectedProject;
        copernicusErrorTask.Navigate += new EventHandler(this.ErrorTaskItemNavigate);
      }
      return copernicusErrorTask;
    }

    private void ErrorTaskItemNavigate(object sender, EventArgs e)
    {
      CopernicusErrorTask copernicusErrorTask = sender as CopernicusErrorTask;
      if (copernicusErrorTask == null)
        return;
      IVsUIHierarchy hierarchy;
      uint itemID;
      IVsWindowFrame windowFrame;
      IVsTextView view;
      VsShellUtilities.OpenDocument(this.serviceProvider, copernicusErrorTask.Document, Guid.Empty, out hierarchy, out itemID, out windowFrame, out view);
      if (windowFrame.Show() != 0)
      {
        Trace.TraceError("unable to open document {0}", (object) copernicusErrorTask.Document);
        int num = (int) CopernicusMessageBox.Show("Unable to open document", "Navigation Error");
      }
      if (view.SetCaretPos(copernicusErrorTask.Line, copernicusErrorTask.Column) != 0)
      {
        Trace.TraceError("unable to open document {0}", (object) copernicusErrorTask.Document);
        int num = (int) CopernicusMessageBox.Show("Unable to set caret position.", "Navigation Error");
      }
      view.CenterLines(copernicusErrorTask.Line, 1);
    }

    internal void AddHandler(string fileName, ErrorListUpdated handler)
    {
      fileName = CopernicusErrorListProvider.ConvertPath(fileName);
      if (handler == null)
        return;
      lock (this.eventTable)
      {
        ErrorListUpdated errorListUpdated;
        if (this.eventTable.TryGetValue(fileName, out errorListUpdated))
          this.eventTable[fileName] = errorListUpdated + handler;
        else
          this.eventTable[fileName] = handler;
      }
    }

    public void RemoveHandler(string fileName, ErrorListUpdated handler)
    {
      if (handler == null)
        return;
      lock (this.eventTable)
      {
        if (this.eventTable[fileName] == null)
          return;
        this.eventTable[fileName] = this.eventTable[fileName] - handler;
      }
    }

    private void RaiseErrorListUpdated(string filePath)
    {
      if (string.IsNullOrEmpty(filePath) || !this.eventTable.ContainsKey(filePath))
        return;
      ErrorListUpdated errorListUpdated;
      lock (this.eventTable)
        errorListUpdated = this.eventTable[filePath];
      if (errorListUpdated == null)
        return;
      errorListUpdated();
    }

    internal void UpdateErrorList(BOCompilerErrors errorHandler, BOErrorLevel clearErrorsUpto)
    {
      if (errorHandler == null)
        return;
      this.SuspendRefresh();
      HashSet<string> stringSet = new HashSet<string>();
      stringSet.Add(errorHandler.FileName);
      foreach (BOCompilerError boCompilerError in errorHandler)
        stringSet.Add(boCompilerError.FileName);
      foreach (string filePath in stringSet)
        this.ClearErrors(filePath, clearErrorsUpto);
      foreach (BOCompilerError error in errorHandler)
      {
        if (error.Line >= 0)
          this.Tasks.Add((Microsoft.VisualStudio.Shell.Task) this.CreateErrorTaskItem(error));
      }
      this.ResumeRefresh();
      foreach (string filePath in stringSet)
        this.RaiseErrorListUpdated(filePath);
    }

    internal void ClearErrors(string filePath, BOErrorLevel clearErrorsUpto)
    {
      Origin originMask;
      switch (clearErrorsUpto)
      {
        case BOErrorLevel.MIN_LEVEL:
          originMask = Origin.BOCompilerLevel1;
          break;
        case BOErrorLevel.LEVEL2:
          originMask = Origin.BOCompilerLevel1 | Origin.BOCompilerLevel2;
          break;
        case BOErrorLevel.LEVEL3:
          originMask = Origin.BOCompilerLevel1 | Origin.BOCompilerLevel2 | Origin.BOCompilerLevel3;
          break;
        default:
          originMask = Origin.All;
          break;
      }
      this.ClearErrorList(filePath, originMask);
    }

    internal void ClearErrorList(string filePath, Origin originMask)
    {
      filePath = CopernicusErrorListProvider.ConvertPath(filePath);
      this.SuspendRefresh();
      IList<Microsoft.VisualStudio.Shell.Task> taskList = (IList<Microsoft.VisualStudio.Shell.Task>) new List<Microsoft.VisualStudio.Shell.Task>();
      foreach (Microsoft.VisualStudio.Shell.Task task in this.Tasks)
      {
        CopernicusErrorTask copernicusErrorTask = task as CopernicusErrorTask;
        if (copernicusErrorTask != null && (string.IsNullOrEmpty(filePath) || copernicusErrorTask.Document.Equals(filePath) || copernicusErrorTask.Document.EndsWith(filePath)) && (copernicusErrorTask.Origin & originMask) != (Origin) 0)
          taskList.Add((Microsoft.VisualStudio.Shell.Task) copernicusErrorTask);
      }
      foreach (Microsoft.VisualStudio.Shell.Task task in (IEnumerable<Microsoft.VisualStudio.Shell.Task>) taskList)
        this.Tasks.Remove(task);
      this.ResumeRefresh();
      if (taskList.Count <= 0)
        return;
      if (string.IsNullOrEmpty(filePath))
      {
        foreach (Microsoft.VisualStudio.Shell.Task task in (IEnumerable<Microsoft.VisualStudio.Shell.Task>) taskList)
          this.RaiseErrorListUpdated(task.Document);
      }
      else
        this.RaiseErrorListUpdated(filePath);
    }

    private static string ConvertPath(string filePath)
    {
      if (!string.IsNullOrEmpty(filePath) && filePath.StartsWith("/"))
        filePath = XRepMapper.GetInstance().GetLocalPathforXRepProjectEntities(filePath);
      return filePath;
    }

    internal void UpdateErrorList(IEnumerable<Task> errors)
    {
      if (errors == null)
        return;
      Dictionary<string, Origin> dictionary = new Dictionary<string, Origin>();
      foreach (Task error in errors)
      {
        string key = CopernicusErrorListProvider.ConvertPath(error.FileName);
        Origin origin;
        if (dictionary.TryGetValue(key, out origin))
        {
          origin |= error.Origin;
          dictionary.Remove(key);
        }
        else
          origin = error.Origin;
        dictionary.Add(key, origin);
      }
      this.SuspendRefresh();
      foreach (KeyValuePair<string, Origin> keyValuePair in dictionary)
        this.ClearErrorList(keyValuePair.Key, keyValuePair.Value);
      foreach (Task error in errors)
      {
        if (error.Line >= 0 || string.IsNullOrEmpty(error.FileName))
          this.Tasks.Add((Microsoft.VisualStudio.Shell.Task) this.CreateErrorTaskItem(error));
      }
      this.ResumeRefresh();
      foreach (string key in dictionary.Keys)
        this.RaiseErrorListUpdated(key);
    }
  }
}
