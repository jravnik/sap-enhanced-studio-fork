﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.RepositoryTreeBuilder
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Repository.DataModel;
using SAP.Copernicus.Core.RepositoryView.ContextMenu;
using SAP.Copernicus.Core.RepositoryView.TreeModel;
using SAP.Copernicus.Core.Util;
using System;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.RepositoryView
{
  public class RepositoryTreeBuilder
  {
    private RepositoryDataSet repositoryDataSet;
    internal MySolutionsNode mySolutionsNode;

    public RepositoryTreeBuilder(RepositoryDataSet repositoryDataSet)
    {
      this.repositoryDataSet = repositoryDataSet;
    }

    public void BuildFromRoot(TreeView treeView)
    {
      this.BuildMySolutionNode(treeView.Nodes);
      ContextMenuBuilder.RegisterMouseClickHandler(treeView);
    }

    private void BuildSAPNode(TreeNodeCollection baseNode)
    {
      SAPNode sapNode = new SAPNode();
      ContextMenuBuilder.AddContextMenuStrip((BaseNode) sapNode);
      this.buildNamespaces(sapNode.Nodes);
      baseNode.Add((TreeNode) sapNode);
    }

    private void BuildMySolutionNode(TreeNodeCollection baseNode)
    {
      this.mySolutionsNode = new MySolutionsNode();
      ContextMenuBuilder.AddContextMenuStrip((BaseNode) this.mySolutionsNode);
      this.BuildSolutions(this.mySolutionsNode.Nodes);
      this.mySolutionsNode.Expand();
      baseNode.Add((TreeNode) this.mySolutionsNode);
    }

    private string CreateSolutionToolTip(RepositoryDataSet.SolutionsRow solution)
    {
      try
      {
        string str1 = "Description: \t\t" + solution.Description + "\nName: \t\t\t" + solution.Name + "\n";
        SolutionType solutionType = SolutionType.fromSolutionTypeCode(solution.Type);
        if (solutionType != null)
        {
          if (solutionType.Equals((object) SolutionType.MultiCustomer_Solutions))
            str1 = str1 + "Global Name: \t\t" + solution.GlobalName + "\n";
          str1 = str1 + "Type: \t\t\t" + solutionType.ToString() + "\n";
        }
        if (!solution.Name.ToUpper().StartsWith("Y"))
          str1 = !solution.Name.ToUpper().StartsWith("Z") ? str1 + "Sandbox: \t\tNo\n" : str1 + "Sandbox: \t\tYes\n";
        string str2 = str1 + "Status: \t\t\t" + solution.OveralStatus + "\n";
        if (!solution.Name.ToUpper().StartsWith("Z") && !solution.Name.ToUpper().StartsWith("Y"))
          str2 = str2 + "Quality Review Status:\t" + solution.CertificationStatus + "\n";
        if (solution.Deleted)
          str2 += "Deletion Pending: \tYes\n";
        if (solution.Name.ToUpper().StartsWith("Y") && solution.PatchSolution != null)
        {
          string str3 = str2 + "Patch Solution: \t\t";
          str2 = !solution.PatchSolution.Equals("P") ? str3 + "No" : str3 + "Yes";
        }
        return str2;
      }
      catch (Exception ex)
      {
        return "";
      }
    }

    private void BuildSolutions(TreeNodeCollection baseNode)
    {
      foreach (RepositoryDataSet.SolutionsRow solution in (RepositoryDataSet.SolutionsRow[]) this.repositoryDataSet.Tables["Solutions"].Select())
      {
        SolutionNode solutionNode = new SolutionNode(solution.Name, solution.Description, solution.Version, solution.Type, solution.PDISubType, solution.PatchSolution, solution.Deleted, solution.DevPartner, solution.ContactPerson, solution.EMail, solution.DeploymentUnit);
        solutionNode.ToolTipText = this.CreateSolutionToolTip(solution);
        ContextMenuBuilder.AddContextMenuStrip((BaseNode) solutionNode);
        baseNode.Insert(0, (TreeNode) solutionNode);
      }
    }

    public void buildNamespaces(TreeNodeCollection baseNode)
    {
      foreach (RepositoryDataSet.NamespacesRow nsRow in (RepositoryDataSet.NamespacesRow[]) this.repositoryDataSet.Tables["Namespaces"].Select())
      {
        string nsName = nsRow.NSName;
        if (RepositoryQueryHandler.IsSAPNamespace(nsName))
        {
          NamespaceNode namespaceNode = new NamespaceNode(nsRow);
          ContextMenuBuilder.AddContextMenuStrip((BaseNode) namespaceNode);
          BOFolderNode boFolderNode = new BOFolderNode();
          ContextMenuBuilder.AddContextMenuStrip((BaseNode) boFolderNode);
          DTFolderNode dtFolderNode = new DTFolderNode();
          ContextMenuBuilder.AddContextMenuStrip((BaseNode) dtFolderNode);
          EnhancementOptionFolderNode optionFolderNode = new EnhancementOptionFolderNode();
          ContextMenuBuilder.AddContextMenuStrip((BaseNode) optionFolderNode);
          namespaceNode.Nodes.Add((TreeNode) boFolderNode);
          namespaceNode.Nodes.Add((TreeNode) dtFolderNode);
          namespaceNode.Nodes.Add((TreeNode) optionFolderNode);
          this.buildBOs(boFolderNode.Nodes, nsName);
          this.buildDTs(dtFolderNode.Nodes, nsName);
          this.buildEnhancementOptions(optionFolderNode.Nodes, nsName);
          baseNode.Add((TreeNode) namespaceNode);
        }
      }
    }

    public void buildBOs(TreeNodeCollection baseNode, string nsName)
    {
      RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select("NSName='" + nsName + "'");
      if (businessObjectsRowArray == null)
        return;
      foreach (RepositoryDataSet.BusinessObjectsRow boRow in businessObjectsRowArray)
      {
        BusinessObjectNode businessObjectNode = new BusinessObjectNode(boRow);
        ContextMenuBuilder.AddContextMenuStrip((BaseNode) businessObjectNode);
        string proxyName = boRow.ProxyName;
        this.buildBONodesHierarchical(businessObjectNode.Nodes, proxyName, false, (string) null);
        baseNode.Add((TreeNode) businessObjectNode);
      }
    }

    public void buildDTs(TreeNodeCollection baseNode, string nsName)
    {
      RepositoryDataSet.DataTypesRow[] dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select("NSName='" + nsName + "'");
      if (dataTypesRowArray == null)
        return;
      foreach (RepositoryDataSet.DataTypesRow dtRow in dataTypesRowArray)
      {
        DataTypeNode dataTypeNode = new DataTypeNode(dtRow);
        ContextMenuBuilder.AddContextMenuStrip((BaseNode) dataTypeNode);
        baseNode.Add((TreeNode) dataTypeNode);
      }
    }

    public void buildBONodesHierarchical(TreeNodeCollection baseNode, string boProxyName, bool isTreeRootBO, string parentNodeID)
    {
      RepositoryDataSet.NodesRow[] nodesRowArray;
      if (parentNodeID == null)
        nodesRowArray = (RepositoryDataSet.NodesRow[]) this.repositoryDataSet.Tables["Nodes"].Select("BOProxyName='" + boProxyName + "' AND ParentNodeName=''");
      else
        nodesRowArray = (RepositoryDataSet.NodesRow[]) this.repositoryDataSet.Tables["Nodes"].Select("BOProxyName='" + boProxyName + "' AND ParentNodeName='" + parentNodeID.ToUpperInvariant() + "'");
      if (nodesRowArray == null)
        return;
      foreach (RepositoryDataSet.NodesRow nodeRow in nodesRowArray)
      {
        BONodeNode boNodeNode = new BONodeNode(nodeRow);
        ContextMenuBuilder.AddContextMenuStrip((BaseNode) boNodeNode);
        string proxyName = nodeRow.ProxyName;
        if (!isTreeRootBO)
        {
          this.buildActions(boNodeNode.Nodes, boProxyName, proxyName);
        }
        else
        {
          BOActionFolderNode actionFolderNode = new BOActionFolderNode(proxyName, nodeRow.Name);
          BODeterminationFolderNode determinationFolderNode = new BODeterminationFolderNode(proxyName, nodeRow.Name);
          boNodeNode.Nodes.Add((TreeNode) actionFolderNode);
          boNodeNode.Nodes.Add((TreeNode) determinationFolderNode);
        }
        this.buildBONodesHierarchical(boNodeNode.Nodes, boProxyName, isTreeRootBO, nodeRow.Name);
        baseNode.Add((TreeNode) boNodeNode);
      }
    }

    public void buildBONodes(TreeNodeCollection baseNode, string boProxyName, bool isTreeRootBO)
    {
      RepositoryDataSet.NodesRow[] nodesRowArray = (RepositoryDataSet.NodesRow[]) this.repositoryDataSet.Tables["Nodes"].Select("BOProxyName='" + boProxyName + "'");
      if (nodesRowArray == null)
        return;
      foreach (RepositoryDataSet.NodesRow nodeRow in nodesRowArray)
      {
        BONodeNode boNodeNode = new BONodeNode(nodeRow);
        ContextMenuBuilder.AddContextMenuStrip((BaseNode) boNodeNode);
        string proxyName = nodeRow.ProxyName;
        if (!isTreeRootBO)
        {
          this.buildActions(boNodeNode.Nodes, boProxyName, proxyName);
        }
        else
        {
          BOActionFolderNode actionFolderNode = new BOActionFolderNode(proxyName, nodeRow.Name);
          BODeterminationFolderNode determinationFolderNode = new BODeterminationFolderNode(proxyName, nodeRow.Name);
          boNodeNode.Nodes.Add((TreeNode) actionFolderNode);
          boNodeNode.Nodes.Add((TreeNode) determinationFolderNode);
        }
        baseNode.Add((TreeNode) boNodeNode);
      }
    }

    public void buildActions(TreeNodeCollection baseNode, string boProxyName, string nodeProxyName)
    {
      RepositoryDataSet.ActionsRow[] actionsRowArray = (RepositoryDataSet.ActionsRow[]) this.repositoryDataSet.Tables["Actions"].Select("BOProxyName='" + boProxyName + "' AND NodeProxyName='" + nodeProxyName + "'");
      if (actionsRowArray == null)
        return;
      foreach (RepositoryDataSet.ActionsRow actionRow in actionsRowArray)
      {
        BOActionNode boActionNode = new BOActionNode(actionRow);
        ContextMenuBuilder.AddContextMenuStrip((BaseNode) boActionNode);
        baseNode.Add((TreeNode) boActionNode);
      }
    }

    public void refreshBOFolder(BaseNode boFolderTN, string nsName)
    {
      TreeNode treeNode = new TreeNode();
      this.buildBOs(treeNode.Nodes, nsName);
      TreeNode[] nodes = new TreeNode[treeNode.Nodes.Count];
      treeNode.Nodes.CopyTo((Array) nodes, 0);
      boFolderTN.Nodes.Clear();
      boFolderTN.Nodes.AddRange(nodes);
    }

    public void refreshMySolutionsNode(BaseNode mySolutionsNode)
    {
      TreeNode treeNode = new TreeNode();
      this.BuildSolutions(treeNode.Nodes);
      TreeNode[] nodes = new TreeNode[treeNode.Nodes.Count];
      treeNode.Nodes.CopyTo((Array) nodes, 0);
      mySolutionsNode.Nodes.Clear();
      mySolutionsNode.Nodes.AddRange(nodes);
    }

    public void refreshDTFolder(BaseNode dtFolderTN, string nsName)
    {
      TreeNode treeNode = new TreeNode();
      this.buildDTs(treeNode.Nodes, nsName);
      TreeNode[] nodes = new TreeNode[treeNode.Nodes.Count];
      treeNode.Nodes.CopyTo((Array) nodes, 0);
      dtFolderTN.Nodes.Clear();
      dtFolderTN.Nodes.AddRange(nodes);
    }

    public void refreshEnhancementOptionsFolder(BaseNode dtFolderTN, string nsName)
    {
      TreeNode treeNode = new TreeNode();
      this.buildEnhancementOptions(treeNode.Nodes, nsName);
      TreeNode[] nodes = new TreeNode[treeNode.Nodes.Count];
      treeNode.Nodes.CopyTo((Array) nodes, 0);
      dtFolderTN.Nodes.Clear();
      dtFolderTN.Nodes.AddRange(nodes);
    }

    public void buildEnhancementOptions(TreeNodeCollection baseNode, string nsName)
    {
      RepositoryDataSet.BAdIsRow[] badIsRowArray = (RepositoryDataSet.BAdIsRow[]) this.repositoryDataSet.Tables["BAdIs"].Select("NSName='" + nsName + "'");
      if (badIsRowArray == null)
        return;
      foreach (RepositoryDataSet.BAdIsRow dtRow in badIsRowArray)
      {
        EnhancementOptionNode enhancementOptionNode = new EnhancementOptionNode(dtRow);
        ContextMenuBuilder.AddContextMenuStrip((BaseNode) enhancementOptionNode);
        baseNode.Add((TreeNode) enhancementOptionNode);
      }
    }

    public void refreshBO(BaseNode boTN, string boProxyName)
    {
      TreeNode treeNode = new TreeNode();
      this.buildBONodes(treeNode.Nodes, boProxyName, false);
      TreeNode[] nodes = new TreeNode[treeNode.Nodes.Count];
      treeNode.Nodes.CopyTo((Array) nodes, 0);
      boTN.Nodes.Clear();
      boTN.Nodes.AddRange(nodes);
    }
  }
}
