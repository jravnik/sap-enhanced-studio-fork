﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BOFolderNode
// Assembly: Copernicus, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: EB8B16D6-2127-4E7F-B242-B08497EA011A
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository;
using System;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class BOFolderNode : BaseNode
  {
    public BOFolderNode()
      : this((DataRow) null)
    {
    }

    public BOFolderNode(DataRow dataRow)
      : base(dataRow)
    {
      this.Text = CopernicusResources.TreeNodeTextBO;
      this.ImageIndex = 9;
      this.SelectedImageIndex = 9;
    }

    public override NodeType getNodeType()
    {
      return NodeType.BOFolder;
    }

    public override void refresh()
    {
      DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Refreshing " + (object) this.getNodeType() + " (" + this.Namespace + ") ...");
      string nsName = this.Namespace;
      RepositoryDataCache.GetInstance().RefreshBOFolderNode(nsName);
      this.getRepositoryViewControl().getTreeBuilder().refreshBOFolder((BaseNode) this, nsName);
      SAP.Copernicus.Core.Util.Util.endMeasurement(start, "Refreshed in: ");
    }
  }
}
