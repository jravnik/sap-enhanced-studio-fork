﻿// Decompiled with JetBrains decompiler
// Type: Microsoft.VisualStudio.VSHelp80.Help2
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.VSHelp;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Microsoft.VisualStudio.VSHelp80
{
  [CompilerGenerated]
  [TypeIdentifier]
  [Guid("78413D2D-0492-4A9B-AB25-730633679977")]
  [ComImport]
  public interface Help2 : Help
  {
    [SpecialName]
    void _VtblGap1_9();

    [DispId(10)]
    void DisplayTopicFromF1Keyword([MarshalAs(UnmanagedType.BStr), In] string pszKeyword);
  }
}
