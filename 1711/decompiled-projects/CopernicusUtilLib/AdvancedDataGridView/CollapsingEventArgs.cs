﻿// Decompiled with JetBrains decompiler
// Type: AdvancedDataGridView.CollapsingEventArgs
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;

namespace AdvancedDataGridView
{
  public class CollapsingEventArgs : CancelEventArgs
  {
    private TreeGridNode _node;

    private CollapsingEventArgs()
    {
    }

    public CollapsingEventArgs(TreeGridNode node)
    {
      this._node = node;
    }

    public TreeGridNode Node
    {
      get
      {
        return this._node;
      }
    }
  }
}
