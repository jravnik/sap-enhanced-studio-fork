﻿// Decompiled with JetBrains decompiler
// Type: com.sap.JSONConnector.Helper
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Web.Script.Serialization;

namespace com.sap.JSONConnector
{
  public static class Helper
  {
    public static string toJSON(this object obj)
    {
      return new JavaScriptSerializer().Serialize(obj);
    }
  }
}
