﻿// Decompiled with JetBrains decompiler
// Type: WindowsInstaller.Database
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace WindowsInstaller
{
  [Guid("000C109D-0000-0000-C000-000000000046")]
  [CompilerGenerated]
  [TypeIdentifier]
  [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
  [ComImport]
  public interface Database
  {
    [DispId(3)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.Interface)]
    View OpenView([MarshalAs(UnmanagedType.BStr), In] string Sql);
  }
}
