﻿// Decompiled with JetBrains decompiler
// Type: WindowsInstaller.View
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace WindowsInstaller
{
  [CompilerGenerated]
  [Guid("000C109C-0000-0000-C000-000000000046")]
  [TypeIdentifier]
  [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
  [ComImport]
  public interface View
  {
    [DispId(1)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    void Execute([MarshalAs(UnmanagedType.Interface), In] Record Params = null);

    [DispId(2)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.Interface)]
    Record Fetch();
  }
}
