﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Properties.Settings
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus.Properties
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }

    [DefaultSettingValue("https://atf-cust810.dev.sapbydesign.com/sap/bc/srt/rfc/sap/zget_version_service/zsn_version/zbn_version?sap-vhost=atf-cust810.dev.sapbydesign.com")]
    [ApplicationScopedSetting]
    [DebuggerNonUserCode]
    [SpecialSetting(SpecialSetting.WebServiceUrl)]
    public string CopernicusUtilLib_WebReferenceVersion_ZSN_VERSION
    {
      get
      {
        return (string) this[nameof (CopernicusUtilLib_WebReferenceVersion_ZSN_VERSION)];
      }
    }
  }
}
