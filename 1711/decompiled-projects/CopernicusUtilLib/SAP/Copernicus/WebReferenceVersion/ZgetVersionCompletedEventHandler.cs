﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.WebReferenceVersion.ZgetVersionCompletedEventHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;

namespace SAP.Copernicus.WebReferenceVersion
{
  [GeneratedCode("System.Web.Services", "4.0.30319.17929")]
  public delegate void ZgetVersionCompletedEventHandler(object sender, ZgetVersionCompletedEventArgs e);
}
