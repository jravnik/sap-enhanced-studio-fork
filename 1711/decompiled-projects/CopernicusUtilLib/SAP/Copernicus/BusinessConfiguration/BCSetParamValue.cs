﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.BCSetParamValue
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.BusinessConfiguration.Model;
using System.Collections.Generic;

namespace SAP.Copernicus.BusinessConfiguration
{
  public class BCSetParamValue : ValueStructureType
  {
    public string ValueID;
    public string ValueDescription;
    public string ParameterID;
    public bool usedOnlyOnce;
    public List<string> BCSetID;

    public BCSetParamValue(string ValueID, string ParameterID, string BCSetID)
    {
      this.BCSetID = new List<string>();
      this.BCSetID.Add(BCSetID);
      this.ValueID = ValueID;
      this.ParameterID = ParameterID;
      this.usedOnlyOnce = true;
    }
  }
}
