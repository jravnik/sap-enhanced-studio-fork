﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BCSetType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCSetDefinition")]
  [XmlRoot("BCSet", IsNullable = false, Namespace = "http://sap.com/ByD/PDI/BCSetDefinition")]
  [Serializable]
  public class BCSetType
  {
    private HeadType headField;
    private ParamType[] parameterField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public HeadType Head
    {
      get
      {
        return this.headField;
      }
      set
      {
        this.headField = value;
      }
    }

    [XmlElement("Parameter", Form = XmlSchemaForm.Unqualified)]
    public ParamType[] Parameter
    {
      get
      {
        return this.parameterField;
      }
      set
      {
        this.parameterField = value;
      }
    }
  }
}
