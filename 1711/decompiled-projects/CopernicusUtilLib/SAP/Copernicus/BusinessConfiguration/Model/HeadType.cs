﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.HeadType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model
{
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCSetDefinition")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [Serializable]
  public class HeadType
  {
    private string idField;
    private string descriptionField;
    private string typeField;
    private string subTypeField;
    private bool fineTuningRelevantField;
    private string sAPBCOProxyNameField;
    private string partnerBCOProxyNameField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ID
    {
      get
      {
        return this.idField;
      }
      set
      {
        this.idField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Description
    {
      get
      {
        return this.descriptionField;
      }
      set
      {
        this.descriptionField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Type
    {
      get
      {
        return this.typeField;
      }
      set
      {
        this.typeField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string SubType
    {
      get
      {
        return this.subTypeField;
      }
      set
      {
        this.subTypeField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public bool FineTuningRelevant
    {
      get
      {
        return this.fineTuningRelevantField;
      }
      set
      {
        this.fineTuningRelevantField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string SAPBCOProxyName
    {
      get
      {
        return this.sAPBCOProxyNameField;
      }
      set
      {
        this.sAPBCOProxyNameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string PartnerBCOProxyName
    {
      get
      {
        return this.partnerBCOProxyNameField;
      }
      set
      {
        this.partnerBCOProxyNameField = value;
      }
    }
  }
}
