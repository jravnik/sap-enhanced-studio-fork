﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BCO.BCOType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BCO
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [XmlRoot("BCO", IsNullable = false, Namespace = "http://sap.com/ByD/PDI/BCODefinition")]
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCODefinition")]
  [Serializable]
  public class BCOType
  {
    private BCOHeadType headField;
    private BCONodeType[] nodeField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public BCOHeadType Head
    {
      get
      {
        return this.headField;
      }
      set
      {
        this.headField = value;
      }
    }

    [XmlElement("Node", Form = XmlSchemaForm.Unqualified)]
    public BCONodeType[] Node
    {
      get
      {
        return this.nodeField;
      }
      set
      {
        this.nodeField = value;
      }
    }
  }
}
