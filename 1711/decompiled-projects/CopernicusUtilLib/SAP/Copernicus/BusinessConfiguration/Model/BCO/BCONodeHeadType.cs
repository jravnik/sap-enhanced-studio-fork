﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BCO.BCONodeHeadType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BCO
{
  [DesignerCategory("code")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCODefinition")]
  [Serializable]
  public class BCONodeHeadType
  {
    private string idField;
    private string parentIDField;
    private string descriptionField;
    private bool isTextField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ID
    {
      get
      {
        return this.idField;
      }
      set
      {
        this.idField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ParentID
    {
      get
      {
        return this.parentIDField;
      }
      set
      {
        this.parentIDField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Description
    {
      get
      {
        return this.descriptionField;
      }
      set
      {
        this.descriptionField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public bool IsText
    {
      get
      {
        return this.isTextField;
      }
      set
      {
        this.isTextField = value;
      }
    }
  }
}
