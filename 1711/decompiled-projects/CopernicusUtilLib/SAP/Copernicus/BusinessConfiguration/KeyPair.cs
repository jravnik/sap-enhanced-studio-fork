﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.KeyPair
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.BusinessConfiguration
{
  public struct KeyPair
  {
    public string Parameter;
    public string Value;
    public string DataType;

    public KeyPair(string p, string v, string d)
    {
      this.Parameter = p;
      this.Value = v;
      this.DataType = d;
    }
  }
}
