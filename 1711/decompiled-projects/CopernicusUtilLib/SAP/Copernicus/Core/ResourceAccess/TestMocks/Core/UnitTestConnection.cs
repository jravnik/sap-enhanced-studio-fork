﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.TestMocks.Core.UnitTestConnection
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;

namespace SAP.Copernicus.Core.ResourceAccess.TestMocks.Core
{
  public class UnitTestConnection : Connection
  {
    public UnitTestConnection()
    {
      PropertyAccess.GeneralProps.ProxySupport = true;
      Connection.instance = (Connection) this;
    }
  }
}
