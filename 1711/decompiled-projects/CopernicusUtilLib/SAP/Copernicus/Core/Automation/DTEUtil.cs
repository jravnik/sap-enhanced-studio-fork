﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Automation.DTEUtil
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using EnvDTE80;
using System;

namespace SAP.Copernicus.Core.Automation
{
  public class DTEUtil
  {
    private static DTEUtil.GetDteDelegate theDelegate;

    public static void Initialize(DTEUtil.GetDteDelegate theDelegate)
    {
      DTEUtil.theDelegate = theDelegate;
    }

    public static DTE2 GetDTE()
    {
      if (DTEUtil.theDelegate == null)
        throw new NotSupportedException("DTEUtil not yet initialized");
      return DTEUtil.theDelegate();
    }

    public static bool IsInitialized
    {
      get
      {
        return DTEUtil.theDelegate != null;
      }
    }

    public delegate DTE2 GetDteDelegate();
  }
}
