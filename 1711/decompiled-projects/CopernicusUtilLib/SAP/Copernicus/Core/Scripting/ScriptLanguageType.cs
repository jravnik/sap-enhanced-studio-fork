﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Scripting.ScriptLanguageType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Scripting
{
  public class ScriptLanguageType
  {
    public static readonly ScriptLanguageType ByDScript = new ScriptLanguageType("BDS");
    public static readonly ScriptLanguageType ABAP = new ScriptLanguageType(nameof (ABAP));
    public static readonly ScriptLanguageType ABAP1 = new ScriptLanguageType(nameof (ABAP1));
    private readonly string LangCode;

    public static ScriptLanguageType[] Values
    {
      get
      {
        return new ScriptLanguageType[3]
        {
          ScriptLanguageType.ByDScript,
          ScriptLanguageType.ABAP,
          ScriptLanguageType.ABAP1
        };
      }
    }

    public static IEnumerable<ScriptLanguageType> ValuesEnumerator
    {
      get
      {
        yield return ScriptLanguageType.ByDScript;
        yield return ScriptLanguageType.ABAP;
        yield return ScriptLanguageType.ABAP1;
      }
    }

    private ScriptLanguageType(string langCode)
    {
      this.LangCode = langCode;
    }

    public string FileExtension
    {
      get
      {
        return this.ToFileExtension();
      }
    }

    public override string ToString()
    {
      switch (this.LangCode)
      {
        case "BDS":
          return "ByD script";
        case "ABAP":
          return "ABAP";
        case "ABAP1":
          return "ABAP1";
        default:
          return (string) null;
      }
    }

    private string ToFileExtension()
    {
      switch (this.LangCode)
      {
        case "BDS":
          return ".absl";
        case "ABAP":
          return ".abap";
        case "ABAP1":
          return ".abap1";
        default:
          return (string) null;
      }
    }

    public static ScriptLanguageType FromFileExtension(string fileExtension)
    {
      if (!fileExtension.StartsWith("."))
      {
        int startIndex = fileExtension.LastIndexOf('.');
        if (startIndex > 0)
          fileExtension = fileExtension.Substring(startIndex);
      }
      switch (fileExtension)
      {
        case ".absl":
          return ScriptLanguageType.ByDScript;
        case ".abap":
          return ScriptLanguageType.ABAP;
        case ".abap1":
          return ScriptLanguageType.ABAP1;
        default:
          return (ScriptLanguageType) null;
      }
    }

    public static bool IsScriptFile(string filePath)
    {
      return filePath.EndsWith(ScriptLanguageType.ByDScript.FileExtension);
    }
  }
}
