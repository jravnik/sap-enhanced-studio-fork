﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.HierarchicalTreeView.DataGridViewDisengageableCheckBoxColumn
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.HierarchicalTreeView
{
  public class DataGridViewDisengageableCheckBoxColumn : DataGridViewColumn
  {
    public DataGridViewDisengageableCheckBoxColumn()
      : base((DataGridViewCell) new DataGridViewDisengageableCheckBoxCell())
    {
    }

    public override DataGridViewCell CellTemplate
    {
      get
      {
        return base.CellTemplate;
      }
      set
      {
        if (value != null && !value.GetType().IsAssignableFrom(typeof (DataGridViewDisengageableCheckBoxCell)))
          throw new InvalidCastException("Must be a DataGridViewDisengageableCheckBoxCell");
        base.CellTemplate = value;
      }
    }
  }
}
