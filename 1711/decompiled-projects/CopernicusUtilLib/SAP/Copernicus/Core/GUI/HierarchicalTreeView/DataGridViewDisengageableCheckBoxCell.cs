﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.HierarchicalTreeView.DataGridViewDisengageableCheckBoxCell
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.HierarchicalTreeView
{
  public class DataGridViewDisengageableCheckBoxCell : DataGridViewCheckBoxCell, DataGridViewDisengageableCell
  {
    private bool enabled;

    public bool Enabled
    {
      set
      {
        this.enabled = value;
      }
      get
      {
        return this.enabled;
      }
    }

    public DataGridViewDisengageableCheckBoxCell()
    {
      this.enabled = true;
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
      if (!this.enabled)
      {
        if ((cellState & DataGridViewElementStates.Selected) != DataGridViewElementStates.None)
          graphics.FillRectangle((Brush) new SolidBrush(cellStyle.SelectionBackColor), cellBounds);
        else
          graphics.FillRectangle((Brush) new SolidBrush(cellStyle.BackColor), cellBounds);
        this.PaintBorder(graphics, clipBounds, cellBounds, cellStyle, advancedBorderStyle);
      }
      else
        base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
    }
  }
}
