﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.HierarchicalTreeView.DataGridViewDisableCheckBoxCell
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace SAP.Copernicus.Core.GUI.HierarchicalTreeView
{
  public class DataGridViewDisableCheckBoxCell : DataGridViewCheckBoxCell
  {
    private bool enabledValue;

    public bool Enabled
    {
      get
      {
        return this.enabledValue;
      }
      set
      {
        this.enabledValue = value;
      }
    }

    public override object Clone()
    {
      DataGridViewDisableCheckBoxCell disableCheckBoxCell = (DataGridViewDisableCheckBoxCell) base.Clone();
      disableCheckBoxCell.Enabled = this.Enabled;
      return (object) disableCheckBoxCell;
    }

    public DataGridViewDisableCheckBoxCell()
    {
      this.enabledValue = true;
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
      int num1 = 8;
      int num2 = 8;
      Point glyphLocation = new Point(cellBounds.X, cellBounds.Y);
      int num3 = cellBounds.Width / 2;
      int num4 = cellBounds.Height / 2;
      glyphLocation.X += num3 - num1 - 1;
      glyphLocation.Y += num4 - num2 - 1;
      if (!this.enabledValue)
      {
        if ((cellState & DataGridViewElementStates.Selected) != DataGridViewElementStates.None)
        {
          graphics.FillRectangle((Brush) new SolidBrush(cellStyle.SelectionBackColor), cellBounds);
          if (value != null && value.GetType() == typeof (bool))
          {
            if ((bool) value)
              CheckBoxRenderer.DrawCheckBox(graphics, glyphLocation, CheckBoxState.CheckedDisabled);
            else
              CheckBoxRenderer.DrawCheckBox(graphics, glyphLocation, CheckBoxState.UncheckedDisabled);
          }
        }
        else
        {
          graphics.FillRectangle((Brush) new SolidBrush(cellStyle.BackColor), cellBounds);
          if (value != null && value.GetType() == typeof (bool))
          {
            if ((bool) value)
              CheckBoxRenderer.DrawCheckBox(graphics, glyphLocation, CheckBoxState.CheckedDisabled);
            else
              CheckBoxRenderer.DrawCheckBox(graphics, glyphLocation, CheckBoxState.UncheckedDisabled);
          }
        }
        this.PaintBorder(graphics, clipBounds, cellBounds, cellStyle, advancedBorderStyle);
      }
      else
      {
        if ((cellState & DataGridViewElementStates.Selected) != DataGridViewElementStates.None)
        {
          graphics.FillRectangle((Brush) new SolidBrush(cellStyle.SelectionBackColor), cellBounds);
          if (value != null && value.GetType() == typeof (bool))
          {
            if ((bool) value)
              CheckBoxRenderer.DrawCheckBox(graphics, glyphLocation, CheckBoxState.CheckedNormal);
            else
              CheckBoxRenderer.DrawCheckBox(graphics, glyphLocation, CheckBoxState.UncheckedNormal);
          }
        }
        else
        {
          graphics.FillRectangle((Brush) new SolidBrush(cellStyle.BackColor), cellBounds);
          if (value != null && value.GetType() == typeof (bool))
          {
            if ((bool) value)
              CheckBoxRenderer.DrawCheckBox(graphics, glyphLocation, CheckBoxState.CheckedNormal);
            else
              CheckBoxRenderer.DrawCheckBox(graphics, glyphLocation, CheckBoxState.UncheckedNormal);
          }
        }
        this.PaintBorder(graphics, clipBounds, cellBounds, cellStyle, advancedBorderStyle);
      }
    }
  }
}
