﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CopernicusStatusBar
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using EnvDTE;
using EnvDTE80;

namespace SAP.Copernicus.Core.GUI
{
  public class CopernicusStatusBar : IStatus
  {
    private DTE2 dte;

    public static IStatus Instance { get; set; }

    public CopernicusStatusBar(DTE2 dte)
    {
      this.dte = dte;
    }

    public void ShowMessage(string message)
    {
      this.dte.StatusBar.Progress(true, message, 0, 100);
    }

    public void StartProcess(string message)
    {
      this.dte.StatusBar.Animate(true, (object) vsStatusAnimation.vsStatusAnimationGeneral);
      this.dte.StatusBar.Progress(true, message, 0, 100);
    }

    public void EndProcess(string message)
    {
      this.dte.StatusBar.Progress(true, message, 0, 100);
      this.dte.StatusBar.Animate(false, (object) vsStatusAnimation.vsStatusAnimationGeneral);
    }
  }
}
