﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.VSPackageUtil
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel.Composition.Hosting;

namespace SAP.Copernicus.Core.Util
{
  public static class VSPackageUtil
  {
    public static CompositionContainer CompositionContainer { get; set; }

    public static IServiceProvider ServiceProvider { get; set; }

    public static bool VsTestHostInstalled { get; set; }
  }
}
