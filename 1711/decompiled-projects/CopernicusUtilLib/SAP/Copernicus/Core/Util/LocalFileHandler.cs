﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.LocalFileHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace SAP.Copernicus.Core.Util
{
  public class LocalFileHandler
  {
    public static FileInfo CreateFileOnDisk(byte[] fileContentBytes, string filePath, bool updateWriteableFile)
    {
      if (fileContentBytes == null)
        return (FileInfo) null;
      string str = filePath.Substring(0, filePath.LastIndexOf("\\") + 1);
      string path2 = filePath.Substring(filePath.LastIndexOf("\\") + 1);
      Directory.CreateDirectory(str);
      FileInfo fileInfo = new FileInfo(Path.Combine(str, path2));
      FileStream fileStream = (FileStream) null;
      try
      {
        if (fileInfo.Exists)
        {
          if (!fileInfo.Attributes.ToString().Contains(FileAttributes.ReadOnly.ToString()))
          {
            if (!updateWriteableFile)
              goto label_13;
          }
          LocalFileHandler.MakeFileWriteable(fileInfo.FullName);
          fileInfo.Delete();
          fileStream = fileInfo.Create();
          LocalFileHandler.MakeFileWriteable(fileInfo.FullName);
          LocalFileHandler.TagCopernicusFile(fileInfo.FullName);
          fileStream.Write(fileContentBytes, 0, fileContentBytes.Length);
          if (!updateWriteableFile)
            LocalFileHandler.MakeFileReadOnly(fileInfo.FullName);
        }
        else
        {
          fileStream = fileInfo.Create();
          fileInfo.Attributes = FileAttributes.Normal;
          fileStream.Write(fileContentBytes, 0, fileContentBytes.Length);
          fileInfo.Attributes = FileAttributes.ReadOnly;
          LocalFileHandler.TagCopernicusFile(fileInfo.FullName);
        }
      }
      catch (Exception ex)
      {
        Trace.WriteLine("Error in CreateFileOnDisk:" + filePath);
        Trace.WriteLine(ex.Message);
        return (FileInfo) null;
      }
      finally
      {
        if (fileStream != null)
          fileStream.Close();
      }
label_13:
      return fileInfo;
    }

    public static FileInfo CreateOrUpdateFileOnDisk(string fileContent, string filePath, bool updateWriteableFile)
    {
      if (fileContent == null)
        return (FileInfo) null;
      string str = filePath.Substring(0, filePath.LastIndexOf("\\") + 1);
      string path2 = filePath.Substring(filePath.LastIndexOf("\\") + 1);
      Directory.CreateDirectory(str);
      FileInfo fileInfo = new FileInfo(Path.Combine(str, path2));
      if (fileInfo.Exists)
      {
        if (fileInfo.Attributes.ToString().Contains(FileAttributes.ReadOnly.ToString()) || updateWriteableFile)
        {
          LocalFileHandler.MakeFileWriteable(fileInfo.FullName);
          LocalFileHandler.TagCopernicusFile(fileInfo.FullName);
          File.WriteAllText(fileInfo.FullName, fileContent, Encoding.UTF8);
          if (!updateWriteableFile)
            LocalFileHandler.MakeFileReadOnly(fileInfo.FullName);
        }
      }
      else
      {
        File.WriteAllText(fileInfo.FullName, fileContent, Encoding.UTF8);
        fileInfo.Attributes = FileAttributes.ReadOnly;
        LocalFileHandler.TagCopernicusFile(fileInfo.FullName);
      }
      return fileInfo;
    }

    public static FileInfo CreateFileOnDisk(string fileContent, string filePath, bool updateWriteableFile)
    {
      if (fileContent == null)
        return (FileInfo) null;
      return LocalFileHandler.CreateFileOnDisk(Encoding.UTF8.GetBytes(fileContent), filePath, updateWriteableFile);
    }

    public static string ReadFileFromDisk(string filePathwithFilename)
    {
      return LocalFileHandler.ReadFileFromDisk(new FileInfo(filePathwithFilename));
    }

    public static byte[] ReadFileBytesFromDisk(string filePathwithFilename)
    {
      return LocalFileHandler.ReadFileBytesFromDisk(new FileInfo(filePathwithFilename));
    }

    public static void MakeFileWriteable(string filePath)
    {
      if (!File.Exists(filePath))
        return;
      FileAttributes attributes = File.GetAttributes(filePath);
      new FileInfo(filePath).Attributes = attributes & ~FileAttributes.ReadOnly;
    }

    public static void MakeFileReadOnly(string filePath)
    {
      if (!File.Exists(filePath))
        return;
      FileAttributes attributes = File.GetAttributes(filePath);
      new FileInfo(filePath).Attributes = attributes | FileAttributes.ReadOnly;
    }

    public static string ReadFileFromDisk(FileInfo file)
    {
      string str = "";
      if (file.Exists)
      {
        byte[] numArray = new byte[file.Length];
        FileStream fileStream = file.OpenRead();
        fileStream.Read(numArray, 0, numArray.Length);
        fileStream.Close();
        str = Encoding.UTF8.GetString(numArray);
      }
      return str;
    }

    public static byte[] ReadFileBytesFromDisk(FileInfo file)
    {
      byte[] buffer = (byte[]) null;
      if (file.Exists)
      {
        buffer = new byte[file.Length];
        FileStream fileStream = file.OpenRead();
        fileStream.Read(buffer, 0, buffer.Length);
        fileStream.Close();
      }
      return buffer;
    }

    public static void DeleteFileFromDisk(string filePath)
    {
      FileInfo fileInfo = new FileInfo(filePath);
      if (!fileInfo.Exists)
        return;
      LocalFileHandler.MakeFileWriteable(filePath);
      fileInfo.Delete();
    }

    public static void TagCopernicusFile(string filePath)
    {
      if (!File.Exists(filePath))
        return;
      FileAttributes attributes = File.GetAttributes(filePath);
      new FileInfo(filePath).Attributes = attributes | FileAttributes.Offline;
    }

    public static void RemoveCopernicusTagFromFile(string filePath)
    {
      if (!File.Exists(filePath))
        return;
      FileAttributes attributes = File.GetAttributes(filePath);
      FileAttributes fileAttributes = FileAttributes.Offline;
      new FileInfo(filePath).Attributes = attributes & ~fileAttributes;
    }

    public static List<string> CleanCopernicusProjectFolder(string ProjectFolder, List<string> listOfFilesInBackend)
    {
      List<string> stringList1 = new List<string>();
      if (Directory.Exists(ProjectFolder))
      {
        DirectoryInfo directoryInfo = new DirectoryInfo(ProjectFolder);
        foreach (FileInfo file in directoryInfo.GetFiles())
        {
          if (!listOfFilesInBackend.Contains(file.FullName) && (File.GetAttributes(file.FullName) & FileAttributes.Offline) == FileAttributes.Offline && file.Exists)
          {
            LocalFileHandler.DeleteFileFromDisk(file.FullName);
            stringList1.Add(file.FullName);
          }
        }
        foreach (DirectoryInfo directory in directoryInfo.GetDirectories())
        {
          List<string> stringList2 = new List<string>();
          List<string> stringList3 = LocalFileHandler.CleanCopernicusProjectFolder(directory.FullName, listOfFilesInBackend);
          stringList1.AddRange((IEnumerable<string>) stringList3);
        }
      }
      return stringList1;
    }
  }
}
