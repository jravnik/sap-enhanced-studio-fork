﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.TraceUtil
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace SAP.Copernicus.Core.Util
{
  public class TraceUtil
  {
    private static TraceUtil instance = new TraceUtil();
    private readonly Dictionary<string, int> traceCounters = new Dictionary<string, int>();
    private const string DEFAULT_LOCATION = "c:\\tmp\\clouddevstudio_trace";
    private bool isOn;
    private string location;

    public static TraceUtil Instance
    {
      get
      {
        return TraceUtil.instance;
      }
    }

    private TraceUtil()
    {
      this.location = "c:\\tmp\\clouddevstudio_trace";
    }

    public Stream GetNextFile(string subPath, string fileName)
    {
      string path2 = Connection.getInstance().GetConnectionName() ?? string.Empty;
      string key = subPath + (object) '~' + path2;
      string str = Path.Combine(Path.Combine(this.Location, subPath), path2);
      lock (this.traceCounters)
      {
        DirectoryInfo directoryInfo = new DirectoryInfo(str);
        int num;
        if (this.traceCounters.TryGetValue(key, out num))
        {
          this.traceCounters.Remove(key);
        }
        else
        {
          num = 1000;
          if (directoryInfo.Exists)
            directoryInfo.Delete(true);
        }
        this.traceCounters.Add(key, ++num);
        if (!directoryInfo.Exists)
        {
          directoryInfo.Create();
          Thread.Sleep(2000);
        }
        return (Stream) File.Create(Path.Combine(str, num.ToString() + "_" + fileName));
      }
    }

    public bool IsOn
    {
      get
      {
        lock (this.traceCounters)
          return this.isOn;
      }
      set
      {
        lock (this.traceCounters)
        {
          if (!value)
            this.traceCounters.Clear();
          this.isOn = value;
        }
      }
    }

    public string Location
    {
      get
      {
        lock (this.traceCounters)
          return this.location;
      }
      set
      {
        lock (this.traceCounters)
          this.location = value;
      }
    }
  }
}
