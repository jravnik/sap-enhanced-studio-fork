﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.FileUtil
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;
using System.IO;

namespace SAP.Copernicus.Core.Util
{
  public static class FileUtil
  {
    public static List<string> GetFilesFromDir(string dirPath, string pattern)
    {
      List<string> fileNames = new List<string>();
      FileUtil.GetFilesFromDir(dirPath, pattern, fileNames);
      return fileNames;
    }

    private static void GetFilesFromDir(string dirPath, string pattern, List<string> fileNames)
    {
      fileNames.AddRange((IEnumerable<string>) Directory.GetFiles(dirPath, pattern));
      foreach (string directory in Directory.GetDirectories(dirPath))
        fileNames.AddRange((IEnumerable<string>) FileUtil.GetFilesFromDir(directory, pattern));
    }
  }
}
