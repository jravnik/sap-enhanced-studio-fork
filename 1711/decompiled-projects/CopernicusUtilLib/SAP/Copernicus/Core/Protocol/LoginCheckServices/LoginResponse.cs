﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginResponse
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  internal sealed class LoginResponse
  {
    internal LoginResponse()
    {
    }

    internal IList<ILoginMessage> Messages { get; set; }

    internal IDictionary<LoginActionType, ILoginAction> Actions { get; set; }

    internal IDictionary<string, ILoginParameter> Parameters { get; set; }

    internal IList<ILoginSession> Sessions { get; set; }
  }
}
