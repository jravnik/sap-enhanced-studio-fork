﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_T_MSG_LIST
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_T_MSG_LIST
  {
    [DataMember]
    public string FILE_NAME;
    [DataMember]
    public string SEVERITY;
    [DataMember]
    public int LINE_NO;
    [DataMember]
    public int COLUMN_NO;
    [DataMember]
    public int LENGTH;
    [DataMember]
    public string TEXT;
  }
}
