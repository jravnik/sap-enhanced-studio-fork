﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_GET_SCENARIOS
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_GET_SCENARIOS : AbstractRemoteFunction<PDI_EXT_GET_SCENARIOS.ImportingType, PDI_EXT_GET_SCENARIOS.ExportingType, PDI_EXT_GET_SCENARIOS.ChangingType, PDI_EXT_GET_SCENARIOS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01ED08D85ABA422445691";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public PDI_EXT_S_BO_NODE_KEY[] IT_BO_NODES;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SCENARIOS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
