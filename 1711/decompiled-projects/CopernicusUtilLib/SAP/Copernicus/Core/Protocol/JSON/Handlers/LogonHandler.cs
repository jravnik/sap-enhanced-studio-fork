﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.LogonHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Util;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
    internal class LogonHandler : JSONHandler
    {
        public LogonData CheckLogon(string url, string user, string xrepSessionDescription, ConnectionDataSet.SystemDataRow systemDataRow, string xrepSessionID, bool suppressDialogMessages)
        {
            LogonData logonData = new LogonData();
            logonData.backend_user = user;
            PDI_LM_SYSTEM_LOGON pdiLmSystemLogon = new PDI_LM_SYSTEM_LOGON();
            pdiLmSystemLogon.Importing = new PDI_LM_SYSTEM_LOGON.ImportingType();
            pdiLmSystemLogon.Importing.IV_SID = xrepSessionID;
            pdiLmSystemLogon.Importing.IV_TIMEOUT = PropertyAccess.GeneralProps.XRepSessionTimeOut;
            pdiLmSystemLogon.Importing.IV_DESCRIPTION = xrepSessionDescription;
            pdiLmSystemLogon.Importing.IS_LOGON_COMPONENT = new PDI_S_LOGON_COMPONENT();
            pdiLmSystemLogon.Importing.IS_LOGON_COMPONENT.UI_RELEASE = PropertyAccess.GeneralProps.release;
            pdiLmSystemLogon.Importing.IS_LOGON_COMPONENT.UI_BUILD_NUMBER = PropertyAccess.GeneralProps.buildNumber;
            pdiLmSystemLogon.Importing.IS_LOGON_COMPONENT.BASE_COMPONENT = PropertyAccess.GeneralProps.RepositoryBaseComponent;
            pdiLmSystemLogon.Importing.IS_LOGON_COMPONENT.BYD_RELEASE = PropertyAccess.GeneralProps.RepositoryVersion;
            pdiLmSystemLogon.Importing.IS_LOGON_COMPONENT.PATCH_LEVEL = PropertyAccess.GeneralProps.LogonPatchLevel;
            JSONClient.getInstance(new com.sap.JSONConnector.Connection(url, (string)null, logonData.backend_user, systemDataRow.SecurePassword, false)).callFunctionModule((SAPFunctionModule)pdiLmSystemLogon, false, false, false);
            if (pdiLmSystemLogon.Exporting.EV_SUCCESS == "" && pdiLmSystemLogon.Exporting.ET_MESSAGES != null && pdiLmSystemLogon.Exporting.ET_MESSAGES.Length > 0)
            {
                if (pdiLmSystemLogon.Exporting.EV_SUCCESS == "")
                    this.reportServerSideProtocolException((SAPFunctionModule)pdiLmSystemLogon, false, false);
                return (LogonData)null;
            }
            if (pdiLmSystemLogon.Exporting.EV_SUCCESS == "X" && pdiLmSystemLogon.Exporting.ET_MESSAGES != null && (pdiLmSystemLogon.Exporting.ET_MESSAGES.Length > 0 && !suppressDialogMessages))
                this.displayMessages(pdiLmSystemLogon.Exporting.ET_MESSAGES);
            logonData.ValidatedXRepSessionID = pdiLmSystemLogon.Exporting.EV_SID;
            logonData.IsAdminUser = pdiLmSystemLogon.Exporting.EV_USER_IS_ADMIN == "X";
            logonData.client = pdiLmSystemLogon.Exporting.EV_CLIENT;
            if (pdiLmSystemLogon.Exporting.ES_LOGON_DETAILS != null)
            {
                logonData.officialName = pdiLmSystemLogon.Exporting.ES_LOGON_DETAILS.SAP_SOLUTION_OFFICIAL_NAME;
                logonData.releaseName = pdiLmSystemLogon.Exporting.ES_LOGON_DETAILS.SAP_SOLUTION_RELEASE_NAME;
                switch (pdiLmSystemLogon.Exporting.ES_LOGON_DETAILS.USAGE_MODE)
                {
                    case " ":
                        logonData.UsageMode = UsageMode.Unkown;
                        break;
                    case "1":
                        logonData.UsageMode = UsageMode.Scalable;
                        break;
                    case "2":
                        logonData.UsageMode = UsageMode.OneOff;
                        break;
                    case "3":
                        logonData.UsageMode = UsageMode.miltiCustomer;
                        break;
                    default:
                        logonData.UsageMode = UsageMode.Unkown;
                        break;
                }
                switch (pdiLmSystemLogon.Exporting.ES_LOGON_DETAILS.SYSTEM_MODE)
                {
                    case " ":
                        logonData.SystemMode = SystemMode.Unkown;
                        break;
                    case "D":
                        logonData.SystemMode = SystemMode.Development;
                        break;
                    case "P":
                        logonData.SystemMode = SystemMode.Productive;
                        break;
                    default:
                        logonData.SystemMode = SystemMode.Unkown;
                        break;
                }
                switch (pdiLmSystemLogon.Exporting.ES_LOGON_DETAILS.RESTRICTION_MODE)
                {
                    case " ":
                        logonData.RestrictionMode = RestrictionMode.Unkown;
                        break;
                    case "W":
                        logonData.RestrictionMode = RestrictionMode.ReadAndWrite;
                        break;
                    case "R":
                        logonData.RestrictionMode = RestrictionMode.ReadOnly;
                        break;
                    default:
                        logonData.RestrictionMode = RestrictionMode.Unkown;
                        break;
                }
                switch (pdiLmSystemLogon.Exporting.ES_LOGON_DETAILS.TENANT_ROLE_CODE)
                {
                    case "":
                        logonData.TenantRoleCode = TenantRoleCode.Unkown;
                        break;
                    case "01":
                        logonData.TenantRoleCode = TenantRoleCode.Administration;
                        break;
                    case "02":
                        logonData.TenantRoleCode = TenantRoleCode.Master;
                        break;
                    case "03":
                        logonData.TenantRoleCode = TenantRoleCode.Stock;
                        break;
                    case "04":
                        logonData.TenantRoleCode = TenantRoleCode.Production;
                        break;
                    case "05":
                        logonData.TenantRoleCode = TenantRoleCode.Reference;
                        break;
                    case "06":
                        logonData.TenantRoleCode = TenantRoleCode.Development;
                        break;
                    case "07":
                        logonData.TenantRoleCode = TenantRoleCode.Test;
                        break;
                    case "08":
                        logonData.TenantRoleCode = TenantRoleCode.Partner_Development;
                        break;
                    default:
                        logonData.TenantRoleCode = TenantRoleCode.Unkown;
                        break;
                }
                switch (pdiLmSystemLogon.Exporting.ES_LOGON_DETAILS.SAP_SOLUTION)
                {
                    case "BYD_ODS":
                        logonData.SapSolutionCode = SapSolutionCode.BYD_ODS;
                        break;
                    case "SAP_LEAP":
                        logonData.SapSolutionCode = SapSolutionCode.SAP_LEAP;
                        break;
                    case "BYD_COD":
                        logonData.SapSolutionCode = SapSolutionCode.BYD_COD;
                        break;
                    case "BYD_ODT":
                        logonData.SapSolutionCode = SapSolutionCode.BYD_ODT;
                        break;
                    case "BYD_ODF":
                        logonData.SapSolutionCode = SapSolutionCode.BYD_ODF;
                        break;
                    default:
                        logonData.SapSolutionCode = SapSolutionCode.Unknown;
                        break;
                }
            }
            if (pdiLmSystemLogon.Exporting.EV_PARTNER != null)
            {
                logonData.PartnerName = pdiLmSystemLogon.Exporting.EV_PARTNER.PARTNER;
                logonData.PartnerID = pdiLmSystemLogon.Exporting.EV_PARTNER.PARTNER_ID;
                logonData.PartnerDomain = pdiLmSystemLogon.Exporting.EV_PARTNER.PARTNERDOMAIN;
                logonData.UsePSM = pdiLmSystemLogon.Exporting.EV_PARTNER.PSM_RELEVANT_IND == "X";
                logonData.IsMultiCustomerEnabled = pdiLmSystemLogon.Exporting.EV_PARTNER.IS_MULTI_CUSTOMER_ENABLED == "X";
            }
            return logonData;
        }

        private void displayMessages(PDI_RI_S_MESSAGE[] messages)
        {
            if (messages == null || messages.Length == 0)
                return;
            List<Message> messages1 = new List<Message>();
            foreach (PDI_RI_S_MESSAGE message in messages)
                messages1.Add(new Message(Message.MessageSeverity.FromABAPCode(message.SEVERITY), message.TEXT));
            MessageDialog md = new MessageDialog(messages1);
            if (!md.BlockForm)
            {
                int num = (int)md.ShowDialog();
            }
        }

        public LogonData CheckAliasLogon(ConnectionDataSet.SystemDataRow systemDataRow, string userName)
        {
            LogonData logonData = new LogonData();
            PDI_LM_ALIAS_LOGON pdiLmAliasLogon = new PDI_LM_ALIAS_LOGON();
            pdiLmAliasLogon.Importing = new PDI_LM_ALIAS_LOGON.ImportingType();
            pdiLmAliasLogon.Importing.IV_ALIAS = new ALIAS_USER();
            pdiLmAliasLogon.Importing.IV_ALIAS.USERALIAS = userName.ToUpper();
            JSONClient.getInstance(new com.sap.JSONConnector.Connection(WebUtil.BuildURL(systemDataRow, SAP.Copernicus.Core.Protocol.Connection.getAliasLogonJsonURLPath()), SAP.Copernicus.Core.Util.Util.clientIntToString(systemDataRow.Client), userName, systemDataRow.SecurePassword, false)).callFunctionModule((SAPFunctionModule)pdiLmAliasLogon, false, false, false);
            if (pdiLmAliasLogon.Exporting.EV_SUCCESS == "" && pdiLmAliasLogon.Exporting.ET_MESSAGES != null && pdiLmAliasLogon.Exporting.ET_MESSAGES.Length > 0)
            {
                if (pdiLmAliasLogon.Exporting.EV_SUCCESS == "")
                    this.reportServerSideProtocolException((SAPFunctionModule)pdiLmAliasLogon, false, false);
                return (LogonData)null;
            }
            logonData.backend_user = pdiLmAliasLogon.Exporting.EV_USER;
            return logonData;
        }
    }
}
