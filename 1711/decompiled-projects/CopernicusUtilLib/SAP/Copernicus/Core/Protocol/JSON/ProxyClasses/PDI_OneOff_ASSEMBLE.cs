﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_OneOff_ASSEMBLE
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_OneOff_ASSEMBLE : AbstractRemoteFunction<PDI_OneOff_ASSEMBLE.ImportingType, PDI_OneOff_ASSEMBLE.ExportingType, PDI_OneOff_ASSEMBLE.ChangingType, PDI_OneOff_ASSEMBLE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01ED09084CA334CB6C087";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SESSION_ID;
      [DataMember]
      public string IV_SOLUTION_NAME;
      [DataMember]
      public string IV_MCUST_DOWNLOAD_FOR_DELIVERY;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SOLUTION_ASSEMBLE;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
