﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ANA_PROPOSE_VIEW_ELEMENT
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_ANA_PROPOSE_VIEW_ELEMENT : AbstractRemoteFunction<PDI_ANA_PROPOSE_VIEW_ELEMENT.ImportingType, PDI_ANA_PROPOSE_VIEW_ELEMENT.ExportingType, PDI_ANA_PROPOSE_VIEW_ELEMENT.ChangingType, PDI_ANA_PROPOSE_VIEW_ELEMENT.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194EC88B8F08C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public PDI_RI_S_NODE_ELEMENT_KEY[] IT_BO_NODE_ELEMENT_KEY;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_VIEW_ELEM_PROP[] ET_VIEW_ELEMENT_PROP;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
