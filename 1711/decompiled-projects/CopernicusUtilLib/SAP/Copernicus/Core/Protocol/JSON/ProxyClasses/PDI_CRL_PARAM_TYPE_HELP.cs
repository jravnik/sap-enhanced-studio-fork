﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_CRL_PARAM_TYPE_HELP
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_CRL_PARAM_TYPE_HELP : AbstractRemoteFunction<PDI_CRL_PARAM_TYPE_HELP.ImportingType, PDI_CRL_PARAM_TYPE_HELP.ExportingType, PDI_CRL_PARAM_TYPE_HELP.ChangingType, PDI_CRL_PARAM_TYPE_HELP.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0383AC1EE2A4C6601D403B098E";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember(Order = 1)]
      public string IV_SOLUTION_NAMESPACE;
      [DataMember(Order = 2)]
      public string IV_WITHOUT_SAP;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_CRL_S_PARAM_TYPE_HELP[] ET_RESULT;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
