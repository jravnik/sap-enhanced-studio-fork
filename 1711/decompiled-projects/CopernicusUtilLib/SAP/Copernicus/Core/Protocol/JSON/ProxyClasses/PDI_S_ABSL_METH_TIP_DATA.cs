﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_ABSL_METH_TIP_DATA
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_ABSL_METH_TIP_DATA
  {
    [DataMember]
    public string METHOD_NAME;
    [DataMember]
    public int PARAMETER_IDX;
    [DataMember]
    public int METHOD_TOKEN_IDX;
    [DataMember]
    public PDI_S_ABSL_METH_TIP[] METHODS;
  }
}
