﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ANA_PROPOSE_DIMENSION
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_ANA_PROPOSE_DIMENSION : AbstractRemoteFunction<PDI_ANA_PROPOSE_DIMENSION.ImportingType, PDI_ANA_PROPOSE_DIMENSION.ExportingType, PDI_ANA_PROPOSE_DIMENSION.ChangingType, PDI_ANA_PROPOSE_DIMENSION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return nameof (PDI_ANA_PROPOSE_DIMENSION);
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public PDI_RI_S_NODE_ELEMENT_KEY[] IT_BO_NODE_ELEMENT_KEY;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_DIMENSION_PROP[] ET_DIMENSION_PROP;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
