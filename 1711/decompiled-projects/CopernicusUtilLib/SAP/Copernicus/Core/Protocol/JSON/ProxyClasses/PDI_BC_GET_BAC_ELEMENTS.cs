﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_GET_BAC_ELEMENTS
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_GET_BAC_ELEMENTS : AbstractRemoteFunction<PDI_BC_GET_BAC_ELEMENTS.ImportingType, PDI_BC_GET_BAC_ELEMENTS.ExportingType, PDI_BC_GET_BAC_ELEMENTS.ChangingType, PDI_BC_GET_BAC_ELEMENTS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB71FCC9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public int IV_TYPES;
      [DataMember]
      public bool IV_SEMANTIC_LABEL;
      [DataMember]
      public BCT_S_BAC_ELEMENT_ID[] IT_BAC_IDS;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public BAC_PDI_S_TREE_ELEMENT[] ET_BAC_NODES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
