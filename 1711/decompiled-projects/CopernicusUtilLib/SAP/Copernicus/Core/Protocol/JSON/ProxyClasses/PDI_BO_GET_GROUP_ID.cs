﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BO_GET_GROUP_ID
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BO_GET_GROUP_ID : AbstractRemoteFunction<PDI_BO_GET_GROUP_ID.ImportingType, PDI_BO_GET_GROUP_ID.ExportingType, PDI_BO_GET_GROUP_ID.ChangingType, PDI_BO_GET_GROUP_ID.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E028DAE1ED1B6E0EA5FB35709AC";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PROXY;
      [DataMember]
      public string IV_UI_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_GROUP_CODE;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
