﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_GET_APPROVAL_STEP_ITEMS
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_GET_APPROVAL_STEP_ITEMS : AbstractRemoteFunction<PDI_RI_GET_APPROVAL_STEP_ITEMS.ImportingType, PDI_RI_GET_APPROVAL_STEP_ITEMS.ExportingType, PDI_RI_GET_APPROVAL_STEP_ITEMS.ChangingType, PDI_RI_GET_APPROVAL_STEP_ITEMS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194F015C5242C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_XREP_PATH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public ET_APPROVAL_STEP_ITEM[] ET_APPROVAL_STEP_ITEMS;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
