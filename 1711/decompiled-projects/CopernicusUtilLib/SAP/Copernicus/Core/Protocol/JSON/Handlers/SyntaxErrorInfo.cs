﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.SyntaxErrorInfo
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Text;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public struct SyntaxErrorInfo
  {
    public string Type;
    public string Method;
    public string Message;

    public SyntaxErrorInfo(string type, string method, string message)
    {
      this.Type = type;
      this.Method = method;
      this.Message = message;
    }

    public bool Equals(SyntaxErrorInfo other)
    {
      if (this.Type.Equals(other.Type) && this.Method.Equals(other.Method))
        return this.Message.Equals(other.Message);
      return false;
    }

    public override int GetHashCode()
    {
      int num = 0;
      if (this.Type != null)
        num += this.Type.GetHashCode();
      if (this.Method != null)
        num += this.Method.GetHashCode();
      if (this.Message != null)
        num += this.Message.GetHashCode();
      return num;
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("Syntax ");
      if (this.Type == "A")
        stringBuilder.Append("Abend");
      else if (this.Type == "S")
        stringBuilder.Append("Success");
      else if (this.Type == "I")
        stringBuilder.Append("Info");
      else if (this.Type == "W")
        stringBuilder.Append("Warning");
      else
        stringBuilder.Append("Error");
      stringBuilder.Append(" in generated code: ").Append(this.Method).Append(" - ").Append(this.Message);
      return stringBuilder.ToString();
    }
  }
}
