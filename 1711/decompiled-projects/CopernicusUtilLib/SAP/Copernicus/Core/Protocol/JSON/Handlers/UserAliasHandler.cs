﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.UserAliasHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  internal class UserAliasHandler : JSONHandler
  {
    public virtual PDI_T_BAPIALIAS[] GetAliasForUserName(PDI_T_BAPIBNAME[] names)
    {
      PDI_LM_GET_UNAME_FOR_ALIAS getUnameForAlias = new PDI_LM_GET_UNAME_FOR_ALIAS();
      getUnameForAlias.Importing = new PDI_LM_GET_UNAME_FOR_ALIAS.ImportingType();
      getUnameForAlias.Importing.IT_BAPIBNAME = names;
      Client.getInstance().getJSONClient(false).callFunctionModule((SAPFunctionModule) getUnameForAlias, false, false, false);
      if (!(getUnameForAlias.Exporting.EV_SUCCESS == "") || getUnameForAlias.Exporting.ET_MESSAGES == null || getUnameForAlias.Exporting.ET_MESSAGES.Length <= 0)
        return getUnameForAlias.Exporting.ET_BAPIALIAS;
      if (getUnameForAlias.Exporting.EV_SUCCESS == "")
        this.reportServerSideProtocolException((SAPFunctionModule) getUnameForAlias, false, false);
      return (PDI_T_BAPIALIAS[]) null;
    }
  }
}
