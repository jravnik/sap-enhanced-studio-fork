﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.MDRSHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class MDRSHandler : JSONHandler
  {
    private static readonly string staticRootPath = Path.Combine(Path.Combine(Path.Combine(Path.GetTempPath(), "SAP"), "Copernicus"), "Metadata");
    internal const string TRACE_CATEGORY = "MetamodelCache";
    private readonly JSONClient client;
    private string rootPath;
    private readonly DirectoryInfo cacheRoot;
    private readonly bool usePSM;

    public static DirectoryInfo GetCacheRootForConnectionName(string connectionName, bool usePSM)
    {
      return Directory.CreateDirectory(Path.Combine(Path.Combine(MDRSHandler.staticRootPath, Uri.EscapeDataString(connectionName)), usePSM ? "Public" : "Internal"));
    }

    public MDRSHandler(string connectionName, bool usePSM)
      : this(Client.getInstance().getJSONClient(false), connectionName, usePSM)
    {
    }

    public MDRSHandler(string rootPath)
    {
      if (string.IsNullOrEmpty(rootPath))
        throw new ArgumentNullException(nameof (rootPath));
      this.rootPath = rootPath;
      this.client = (JSONClient) null;
      this.usePSM = false;
      this.cacheRoot = (DirectoryInfo) null;
    }

    public MDRSHandler(JSONClient client, string connectionName, bool usePSM)
    {
      if (client == null)
        throw new ArgumentNullException(nameof (client));
      this.client = client;
      this.usePSM = usePSM;
      this.cacheRoot = MDRSHandler.GetCacheRootForConnectionName(connectionName, usePSM);
    }

    public static void ClearCache(string connectionName, bool usePSM)
    {
      DirectoryInfo forConnectionName = MDRSHandler.GetCacheRootForConnectionName(connectionName, usePSM);
      if (forConnectionName == null || !forConnectionName.Exists)
        return;
      forConnectionName.Delete(true);
    }

    public bool SaveMetaObject(string resourceName, IDictionary<string, string> cachedProperties, IDictionary<string, TextWriter> xmlContent, out IList<Message> messages)
    {
      messages = (IList<Message>) new List<Message>();
      if (this.client == null)
      {
        foreach (KeyValuePair<string, TextWriter> keyValuePair in (IEnumerable<KeyValuePair<string, TextWriter>>) xmlContent)
        {
          string fullPath = Path.GetFullPath(Path.Combine(this.rootPath, RepositoryUtil.ShortenFileName(RepositoryUtil.EncodeName(keyValuePair.Key))));
          try
          {
            File.WriteAllText(fullPath, keyValuePair.Value.ToString());
          }
          catch (Exception ex)
          {
            messages.Add(new Message(Message.MessageSeverity.Error, string.Format("Unable to save file {0}, reason {1}", (object) resourceName, (object) ex.Message)));
            return false;
          }
        }
        return true;
      }
      try
      {
        int startIndex = resourceName.LastIndexOf('.');
        string name;
        string str1;
        if (startIndex >= 0)
        {
          name = resourceName.Remove(startIndex);
          str1 = resourceName.Substring(startIndex + 1);
        }
        else
        {
          name = resourceName;
          str1 = "";
        }
        string str2 = RepositoryUtil.DecodeName(name);
        XML_TABLE[] xmlTableArray = new XML_TABLE[xmlContent.Count];
        int index = 0;
        foreach (KeyValuePair<string, TextWriter> keyValuePair in (IEnumerable<KeyValuePair<string, TextWriter>>) xmlContent)
        {
          xmlTableArray[index] = new XML_TABLE();
          xmlTableArray[index].PROXY_NAME = keyValuePair.Key;
          xmlTableArray[index].CONTENT = Convert.ToBase64String(Encoding.UTF8.GetBytes(keyValuePair.Value.ToString()));
          ++index;
        }
        if (TraceUtil.Instance.IsOn)
          MDRSHandler.CreateZipPackage(resourceName, xmlContent);
        PDI_RI_MDRS_SAVE_MO_API pdiRiMdrsSaveMoApi = new PDI_RI_MDRS_SAVE_MO_API();
        pdiRiMdrsSaveMoApi.Importing = new PDI_RI_MDRS_SAVE_MO_API.ImportingType();
        pdiRiMdrsSaveMoApi.Importing.IV_PROXY_NAME = str2;
        pdiRiMdrsSaveMoApi.Importing.IV_TYPE = str1;
        pdiRiMdrsSaveMoApi.Importing.IV_PROTOCOL_VERSION = 2;
        pdiRiMdrsSaveMoApi.Importing.IT_XML_TABLE = xmlTableArray;
        this.client.callFunctionModule((SAPFunctionModule) pdiRiMdrsSaveMoApi, false, false, false);
        foreach (PDI_RI_S_MESSAGE pdiRiSMessage in pdiRiMdrsSaveMoApi.Exporting.ET_MESSAGES)
          messages.Add(new Message(Message.MessageSeverity.FromABAPCode(pdiRiSMessage.SEVERITY), pdiRiSMessage.TEXT));
        bool flag = pdiRiMdrsSaveMoApi.Exporting.EV_SUCCESS == "X";
        if (flag)
          this.UpdateInCache(resourceName, pdiRiMdrsSaveMoApi.Exporting.EV_TRANSITIVE_HASH, pdiRiMdrsSaveMoApi.Exporting.ET_XML_TABLE, pdiRiMdrsSaveMoApi.Exporting.EV_RESULT_SET, true);
        return flag;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    private void UpdateInCache(string resourceName, string transitiveHash, XML_TABLE[] resources, string queryResultSet, bool force)
    {
      if (this.client == null)
        return;
      try
      {
        lock (this.client)
        {
          int startIndex = resourceName.LastIndexOf('.');
          string name;
          string metaObjectType;
          if (startIndex >= 0)
          {
            name = resourceName.Remove(startIndex);
            metaObjectType = resourceName.Substring(startIndex + 1);
          }
          else
          {
            name = resourceName;
            metaObjectType = "";
          }
          string moProxyName = RepositoryUtil.DecodeName(name);
          if (RepositoryDataCache.GetInstance().GetHashOfTransitiveClosureFor(this.GetMOType(metaObjectType), moProxyName) != transitiveHash || force)
          {
            string xmlContent = (string) null;
            if (queryResultSet != null)
              xmlContent = Encoding.UTF8.GetString(Convert.FromBase64String(queryResultSet));
            transitiveHash = RepositoryDataCache.GetInstance().AddMetaObject(this.GetMOType(metaObjectType), moProxyName, xmlContent);
            Trace.WriteLine(string.Format("resource {0} updated in repository view, time stamp {1}", (object) resourceName, (object) transitiveHash), "MetamodelCache");
          }
          foreach (XML_TABLE resource in resources)
          {
            string path2 = RepositoryUtil.ShortenFileName(RepositoryUtil.EncodeName(resource.PROXY_NAME));
            string str = transitiveHash == null ? RepositoryDataCache.NOT_IN_CACHE : RepositoryUtil.EncodeName(transitiveHash);
            bool flag = false;
            foreach (FileInfo enumerateFile in this.cacheRoot.EnumerateFiles(path2 + (object) '*'))
            {
              if (!enumerateFile.Name.EndsWith(str))
                enumerateFile.Delete();
              else
                flag = true;
            }
            string path = Path.Combine(this.cacheRoot.FullName, path2) + (object) '.' + str;
            if (!flag)
            {
              File.WriteAllText(path, Encoding.UTF8.GetString(Convert.FromBase64String(resource.CONTENT)));
              Trace.WriteLine(string.Format("resource {0} updated in cache, time stamp {1}", (object) resource.PROXY_NAME, (object) transitiveHash), "MetamodelCache");
            }
          }
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
    }

    private RepositoryDataCache.MetaObjectTypes GetMOType(string metaObjectType)
    {
      if ("businessobject".Equals(metaObjectType, StringComparison.InvariantCultureIgnoreCase))
        return RepositoryDataCache.MetaObjectTypes.BusinessObject;
      if ("datatype".Equals(metaObjectType, StringComparison.InvariantCultureIgnoreCase))
        return RepositoryDataCache.MetaObjectTypes.DataType;
      if ("datatransformationdefinition".Equals(metaObjectType, StringComparison.InvariantCultureIgnoreCase))
        return RepositoryDataCache.MetaObjectTypes.Library;
      return "messagetype".Equals(metaObjectType, StringComparison.InvariantCultureIgnoreCase) ? RepositoryDataCache.MetaObjectTypes.MessageType : RepositoryDataCache.MetaObjectTypes.Unknown;
    }

    public bool ReadMetaObject(string resourceName, string moResourceName, out string xmlContent, out IList<Message> messages)
    {
      xmlContent = (string) null;
      messages = (IList<Message>) new List<Message>();
      if (this.client == null)
      {
        string[] files = Directory.GetFiles(this.rootPath, RepositoryUtil.ShortenFileName(resourceName) + ".*");
        if (files != null)
        {
          if (files.Length >= 1)
          {
            try
            {
              xmlContent = File.ReadAllText(files[0]);
              return true;
            }
            catch (Exception ex)
            {
              messages.Add(new Message(Message.MessageSeverity.Error, string.Format("Failed to load file {0}, reason {1}", (object) files[0], (object) ex.Message)));
              return false;
            }
          }
        }
        string str = Path.Combine(this.rootPath, RepositoryUtil.ShortenFileName(resourceName));
        messages.Add(new Message(Message.MessageSeverity.Error, string.Format("File {0} does not exist.", (object) str)));
        return false;
      }
      try
      {
        int startIndex1 = resourceName.LastIndexOf('.');
        string name1;
        if (startIndex1 >= 0)
        {
          name1 = resourceName.Remove(startIndex1);
          resourceName.Substring(startIndex1 + 1);
        }
        else
          name1 = resourceName;
        RepositoryUtil.DecodeName(name1);
        int startIndex2 = moResourceName.LastIndexOf('.');
        string name2;
        string metaObjectType;
        if (startIndex2 >= 0)
        {
          name2 = moResourceName.Remove(startIndex2);
          metaObjectType = moResourceName.Substring(startIndex2 + 1);
        }
        else
        {
          name2 = moResourceName;
          metaObjectType = "";
        }
        string moProxyName = RepositoryUtil.DecodeName(name2);
        string transitiveClosureFor = RepositoryDataCache.GetInstance().GetHashOfTransitiveClosureFor(this.GetMOType(metaObjectType), moProxyName);
        if (transitiveClosureFor != RepositoryDataCache.NOT_IN_CACHE)
        {
          lock (this.client)
          {
            string path = Path.Combine(this.cacheRoot.FullName, RepositoryUtil.ShortenFileName(resourceName)) + (object) '.' + RepositoryUtil.EncodeName(transitiveClosureFor);
            if (File.Exists(path))
            {
              xmlContent = File.ReadAllText(path);
              Trace.WriteLine(string.Format("resource {0} loaded from cache, time stamp {1}", (object) resourceName, (object) transitiveClosureFor), "MetamodelCache");
              return true;
            }
          }
        }
        PDI_RI_MDRS_READ_MO_API pdiRiMdrsReadMoApi = new PDI_RI_MDRS_READ_MO_API();
        pdiRiMdrsReadMoApi.Importing = new PDI_RI_MDRS_READ_MO_API.ImportingType();
        pdiRiMdrsReadMoApi.Importing.IV_PROXY_NAME = moProxyName;
        pdiRiMdrsReadMoApi.Importing.IV_TYPE = metaObjectType;
        pdiRiMdrsReadMoApi.Importing.IV_PUBLIC_ONLY = this.usePSM;
        pdiRiMdrsReadMoApi.Importing.IV_PROTOCOL_VERSION = 2;
        pdiRiMdrsReadMoApi.Importing.IV_CACHE_ENABLE = true;
        this.client.callFunctionModule((SAPFunctionModule) pdiRiMdrsReadMoApi, false, false, false);
        foreach (PDI_RI_S_MESSAGE pdiRiSMessage in pdiRiMdrsReadMoApi.Exporting.ET_MESSAGES)
          messages.Add(new Message(Message.MessageSeverity.FromABAPCode(pdiRiSMessage.SEVERITY), pdiRiSMessage.TEXT));
        bool flag = pdiRiMdrsReadMoApi.Exporting.EV_SUCCESS == "X";
        if (flag)
        {
          string evTransitiveHash = pdiRiMdrsReadMoApi.Exporting.EV_TRANSITIVE_HASH;
          Trace.WriteLine(string.Format("resource {0} loaded from repository, time stamp {1}", (object) moResourceName, (object) evTransitiveHash), "MetamodelCache");
          string str = RepositoryUtil.DecodeName(resourceName);
          foreach (XML_TABLE xmlTable in pdiRiMdrsReadMoApi.Exporting.ET_XML_TABLE)
          {
            if (xmlTable.PROXY_NAME == str)
              xmlContent = Encoding.UTF8.GetString(Convert.FromBase64String(xmlTable.CONTENT));
          }
          this.UpdateInCache(moResourceName, evTransitiveHash, pdiRiMdrsReadMoApi.Exporting.ET_XML_TABLE, pdiRiMdrsReadMoApi.Exporting.EV_RESULT_SET, false);
        }
        return flag;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    private static void CreateZipPackage(string packageName, IDictionary<string, TextWriter> xmlContent)
    {
      using (ZipPackage zipPackage = new ZipPackage(TraceUtil.Instance.GetNextFile("MDRS", packageName + ".zip")))
      {
        foreach (KeyValuePair<string, TextWriter> keyValuePair in (IEnumerable<KeyValuePair<string, TextWriter>>) xmlContent)
          zipPackage.AddContent(keyValuePair.Key, keyValuePair.Value.ToString(), "text/xml");
      }
    }
  }
}
