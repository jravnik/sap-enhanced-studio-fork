﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXECUTE_QUERY
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_EXECUTE_QUERY : AbstractRemoteFunction<PDI_EXECUTE_QUERY.ImportingType, PDI_EXECUTE_QUERY.ExportingType, PDI_EXECUTE_QUERY.ChangingType, PDI_EXECUTE_QUERY.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E03836E1ED2B3BA79790254C2D3";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BONAME;
      [DataMember]
      public string IV_BONODENAME;
      [DataMember]
      public string IV_QUERYNAME;
      [DataMember]
      public SESF_SELECTION_PARAMETER[] IT_SELECTIONPARAMS;
      [DataMember]
      public int IV_MAXROWS;
      [DataMember]
      public char IV_ONLYCOUNT;
      [DataMember]
      public char IV_INTERN;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_OUT_DATA;
      [DataMember]
      public int EV_COUNT;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
