﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_UTIL_MSG_CLASS_GENERATE
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_UTIL_MSG_CLASS_GENERATE : AbstractRemoteFunction<PDI_UTIL_MSG_CLASS_GENERATE.ImportingType, PDI_UTIL_MSG_CLASS_GENERATE.ExportingType, PDI_UTIL_MSG_CLASS_GENERATE.ChangingType, PDI_UTIL_MSG_CLASS_GENERATE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194EC88B8EE2C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_ESR_NAMESPACE;
      [DataMember]
      public string IV_PROXY_NAME;
      [DataMember]
      public PDI_UTIL_MSG_CLASS_GENERATE.PDI_UTIL_T_MESSAGE[] IT_MESSAGES;
      [DataMember]
      public bool IV_SMTG_GENERATE;
      [DataMember]
      public bool IV_SMTG_ACTIVATE;
    }

    [DataContract]
    public class PDI_UTIL_T_MESSAGE
    {
      [DataMember]
      public string ID;
      [DataMember]
      public string TEXT;
      [DataMember]
      public string ATTRIBUTE1_TYPE;
      [DataMember]
      public string ATTRIBUTE2_TYPE;
      [DataMember]
      public string ATTRIBUTE3_TYPE;
      [DataMember]
      public string ATTRIBUTE4_TYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_MESSAGE_CLASS;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
