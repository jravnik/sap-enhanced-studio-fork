﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.TranslationHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class TranslationHandler : JSONHandler
  {
    public bool ExportTextHandler(string[] xprep_path, string language, string fileName, string addDefaultTrans, out string xmlsstring)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_GET_TEXTS pdiGetTexts = new PDI_GET_TEXTS();
      pdiGetTexts.Importing = new PDI_GET_TEXTS.ImportingType();
      pdiGetTexts.Importing.IT_FILE_PATH = xprep_path;
      pdiGetTexts.Importing.IV_TARGET_LANG = language;
      pdiGetTexts.Importing.IV_FILENAME = fileName;
      pdiGetTexts.Importing.IV_ADD_DEFTRAN = addDefaultTrans;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiGetTexts, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      try
      {
        if (pdiGetTexts.Exporting != null)
        {
          if (pdiGetTexts.Exporting.EV_SUCCESS == "X")
          {
            byte[] bytes = Convert.FromBase64String(pdiGetTexts.Exporting.EV_X_STRING);
            Encoding utF8 = Encoding.UTF8;
            xmlsstring = utF8.GetString(bytes);
            return true;
          }
          if (pdiGetTexts.Exporting.ET_MESSAGES.Length == 0)
          {
            int num = (int) CopernicusMessageBox.Show("Error occured during Export, please try again", "Export Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
          }
          else
            this.reportServerSideProtocolException((SAPFunctionModule) pdiGetTexts, false, false);
        }
      }
      catch (Exception ex)
      {
      }
      xmlsstring = (string) null;
      return false;
    }

    public ZPDI_CHECK_STR[] CheckTextHandler(string[] xprep_path)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_PERFORM_CHECK pdiPerformCheck = new PDI_PERFORM_CHECK();
        pdiPerformCheck.Importing = new PDI_PERFORM_CHECK.ImportingType();
        pdiPerformCheck.Importing.IT_PATH = xprep_path;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiPerformCheck, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiPerformCheck, false, false);
        if (pdiPerformCheck.Exporting != null)
          return pdiPerformCheck.Exporting.ET_CHECK_INFO;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (ZPDI_CHECK_STR[]) null;
    }

    public ZLANG_STRUC[] getLanguage()
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_GET_AVAILABLE_LANGUAGES availableLanguages = new PDI_GET_AVAILABLE_LANGUAGES();
        availableLanguages.Importing = new PDI_GET_AVAILABLE_LANGUAGES.ImportingType();
        jsonClient.callFunctionModule((SAPFunctionModule) availableLanguages, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) availableLanguages, false, false);
        if (availableLanguages.Exporting != null)
          return availableLanguages.Exporting.E_SUPP_LANG;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return (ZLANG_STRUC[]) null;
    }

    public bool ImportTextHandler(string[] xprep_path, string xStringXml)
    {
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_UPLOAD_TEXTS pdiUploadTexts = new PDI_UPLOAD_TEXTS();
        pdiUploadTexts.Importing = new PDI_UPLOAD_TEXTS.ImportingType();
        pdiUploadTexts.Importing.IT_FILE_PATH = xprep_path;
        pdiUploadTexts.Importing.IV_X_STRING = xStringXml;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiUploadTexts, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiUploadTexts, false, false);
        if (pdiUploadTexts.Exporting != null)
        {
          if (pdiUploadTexts.Exporting.EV_SUCCESS == "X")
            return true;
          if (pdiUploadTexts.Exporting.ET_MESSAGES.Length == 0)
          {
            int num = (int) CopernicusMessageBox.Show("Error occured during import, please try again", "Import Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
          }
          return false;
        }
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return false;
    }
  }
}
