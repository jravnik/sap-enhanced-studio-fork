﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_NODE_EXT_GET_XREP_FILES
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_NODE_EXT_GET_XREP_FILES : AbstractRemoteFunction<PDI_NODE_EXT_GET_XREP_FILES.ImportingType, PDI_NODE_EXT_GET_XREP_FILES.ExportingType, PDI_NODE_EXT_GET_XREP_FILES.ChangingType, PDI_NODE_EXT_GET_XREP_FILES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F91EE6AE88474DA33F8C73";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION;
      [DataMember]
      public string IV_BO_NAMESPACE;
      [DataMember]
      public bool IV_XBO_FILES;
      [DataMember]
      public string IV_BO_NAME;
      [DataMember]
      public string IV_EXT_NODE;
      [DataMember]
      public string IV_BO_NODE;
      [DataMember]
      public string IV_SCENARIO;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public OSLS_NODE_XREP_MASS_VERS_CONTENT[] ET_CONTENT_NXS_FILES;
      [DataMember]
      public OSLS_NODE_XREP_MASS_VERS_CONTENT[] ET_CONTENT_XBO_FILES;
      [DataMember]
      public string EV_EXISTS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
