﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_UTIL_SMTG_GENERATE
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_UTIL_SMTG_GENERATE : AbstractRemoteFunction<PDI_UTIL_SMTG_GENERATE.ImportingType, PDI_UTIL_SMTG_GENERATE.ExportingType, PDI_UTIL_SMTG_GENERATE.ChangingType, PDI_UTIL_SMTG_GENERATE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DEFB38B866A1EB4DD74";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_ESR_NAMESPACE;
      [DataMember]
      public string IV_SEMANTICAL_NAME;
      [DataMember]
      public string IV_BO_PROXY_NAME;
      [DataMember]
      public PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE[] IT_MESSAGES;
    }

    [DataContract]
    public class PDI_UTIL_T_MESSAGE
    {
      [DataMember]
      public string ID;
      [DataMember]
      public string TEXT;
      [DataMember]
      public string ATTRIBUTE1_TYPE;
      [DataMember]
      public string ATTRIBUTE2_TYPE;
      [DataMember]
      public string ATTRIBUTE3_TYPE;
      [DataMember]
      public string ATTRIBUTE4_TYPE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_MESSAGE_CLASS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_PROXY_NAME;
      [DataMember]
      public PDI_UTIL_SMTG_GENERATE.PDI_RI_T_MESSAGE[] ET_ERROR_MESSAGES;
    }

    [DataContract]
    public class PDI_RI_T_MESSAGE
    {
      [DataMember]
      public string SEVERITY;
      [DataMember]
      public string TEXT;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
