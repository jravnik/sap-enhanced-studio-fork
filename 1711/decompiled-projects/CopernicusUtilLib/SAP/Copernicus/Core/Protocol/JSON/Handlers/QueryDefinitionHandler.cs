﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.QueryDefinitionHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class QueryDefinitionHandler : JSONHandler
  {
    public PDI_RI_S_VIEW_ELEM_PROP[] getMDAVViewElementProposals(PDI_RI_S_NODE_ELEMENT_KEY[] nodeElementKeys)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ANA_PROPOSE_VIEW_ELEMENT proposeViewElement = new PDI_ANA_PROPOSE_VIEW_ELEMENT();
      proposeViewElement.Importing = new PDI_ANA_PROPOSE_VIEW_ELEMENT.ImportingType();
      proposeViewElement.Importing.IT_BO_NODE_ELEMENT_KEY = nodeElementKeys;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) proposeViewElement, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (proposeViewElement.Exporting != null)
      {
        if (!(proposeViewElement.Exporting.EV_SUCCESS == "") || proposeViewElement.Exporting.ET_MESSAGES == null || proposeViewElement.Exporting.ET_MESSAGES.Length <= 0)
          return proposeViewElement.Exporting.ET_VIEW_ELEMENT_PROP;
        this.reportServerSideProtocolException((SAPFunctionModule) proposeViewElement, false, false);
      }
      return (PDI_RI_S_VIEW_ELEM_PROP[]) null;
    }

    public PDI_RI_S_MDAV[] getMasterDataMDAVProposals(PDI_RI_S_ASSOC_KEY[] assocKeys)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ANA_FIND_MASTER_DATA_MDAV findMasterDataMdav = new PDI_ANA_FIND_MASTER_DATA_MDAV();
      findMasterDataMdav.Importing = new PDI_ANA_FIND_MASTER_DATA_MDAV.ImportingType();
      findMasterDataMdav.Importing.IT_BO_ASSOCIATION_KEY = assocKeys;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) findMasterDataMdav, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (findMasterDataMdav.Exporting != null)
      {
        if (!(findMasterDataMdav.Exporting.EV_SUCCESS == "") || findMasterDataMdav.Exporting.ET_MESSAGES == null || findMasterDataMdav.Exporting.ET_MESSAGES.Length <= 0)
          return findMasterDataMdav.Exporting.ET_MASTER_DATA;
        this.reportServerSideProtocolException((SAPFunctionModule) findMasterDataMdav, false, false);
      }
      return (PDI_RI_S_MDAV[]) null;
    }

    public bool getMDAVStatus(string mdavProxyName, string mdavXrepPath)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ANA_GET_MDAV_STATUS anaGetMdavStatus = new PDI_ANA_GET_MDAV_STATUS();
      anaGetMdavStatus.Importing = new PDI_ANA_GET_MDAV_STATUS.ImportingType();
      anaGetMdavStatus.Importing.IV_MDAV_PROXY_NAME = mdavProxyName;
      anaGetMdavStatus.Importing.IV_MDAV_XREP_PATH = mdavXrepPath;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) anaGetMdavStatus, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (anaGetMdavStatus.Exporting != null)
      {
        if (anaGetMdavStatus.Exporting.EV_SUCCESS == "" && anaGetMdavStatus.Exporting.ET_MESSAGES != null && anaGetMdavStatus.Exporting.ET_MESSAGES.Length > 0)
          this.reportServerSideProtocolException((SAPFunctionModule) anaGetMdavStatus, false, false);
        else if (anaGetMdavStatus.Exporting.EV_IS_ACTIVE == "X" && anaGetMdavStatus.Exporting.EV_IS_UP_TO_DATE == "X")
          return true;
      }
      return false;
    }

    public PDI_ANA_GET_THINGTYPE.OSLS_XREP_COMP_TT[] getThingtype(string BoName, string name_space)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ANA_GET_THINGTYPE pdiAnaGetThingtype = new PDI_ANA_GET_THINGTYPE();
      pdiAnaGetThingtype.Importing = new PDI_ANA_GET_THINGTYPE.ImportingType();
      pdiAnaGetThingtype.Importing.IV_BO_NAME = BoName;
      pdiAnaGetThingtype.Importing.IV_NAMESPACE = name_space;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiAnaGetThingtype, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (pdiAnaGetThingtype.Exporting != null && pdiAnaGetThingtype.Exporting.EV_SUCCESS == "X")
        return pdiAnaGetThingtype.Exporting.ET_THING_TYPE;
      return (PDI_ANA_GET_THINGTYPE.OSLS_XREP_COMP_TT[]) null;
    }

    public string getHashValue(int length, string text)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_HASH_VALUE_GET pdiHashValueGet = new PDI_HASH_VALUE_GET();
      pdiHashValueGet.Importing = new PDI_HASH_VALUE_GET.ImportingType();
      if (text == "")
        pdiHashValueGet.Importing.IV_FLG_NO_SPECIALS = "X";
      else
        pdiHashValueGet.Importing.IV_FLG_NO_SPECIALS = "";
      pdiHashValueGet.Importing.IV_FLG_UPPER_CASE = "X";
      pdiHashValueGet.Importing.IV_HASH_LEN = length;
      pdiHashValueGet.Importing.IV_TEXT = text;
      pdiHashValueGet.Importing.IV_TEXT_LEN = text.Length;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiHashValueGet, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (pdiHashValueGet.Exporting != null)
      {
        if (pdiHashValueGet.Exporting.EV_HASH != null && !pdiHashValueGet.Exporting.EV_SUCCESS.Equals("") && pdiHashValueGet.Exporting.EV_HASH.Length == length)
          return pdiHashValueGet.Exporting.EV_HASH;
        this.reportServerSideProtocolException((SAPFunctionModule) pdiHashValueGet, false, false);
      }
      return (string) null;
    }

    public string getHashValue(int length)
    {
      return this.getHashValue(length, "");
    }

    public PDI_ASSOCIATION_TYPE[] getAssociationTypes(PDI_ASSOCIATION[] associations)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ANA_CHECK_BO_ASSOCIATION checkBoAssociation = new PDI_ANA_CHECK_BO_ASSOCIATION();
      checkBoAssociation.Importing = new PDI_ANA_CHECK_BO_ASSOCIATION.ImportingType();
      checkBoAssociation.Importing.IT_ASSOCIATIONS = associations;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) checkBoAssociation, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (checkBoAssociation.Exporting != null)
      {
        if (!(checkBoAssociation.Exporting.EV_SUCCESS == "") || checkBoAssociation.Exporting.ET_MESSAGES == null || checkBoAssociation.Exporting.ET_MESSAGES.Length <= 0)
          return checkBoAssociation.Exporting.ET_ASSOCIATION_TYPES;
        this.reportServerSideProtocolException((SAPFunctionModule) checkBoAssociation, false, false);
      }
      return (PDI_ASSOCIATION_TYPE[]) null;
    }

    public PDI_PROXY_NAME_AND_DESCR[] getTransformationDefinitionDescription(PDI_PROXY_NAME[] proxyNames)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ANA_GET_TRANS_DEF_DESCR getTransDefDescr = new PDI_ANA_GET_TRANS_DEF_DESCR();
      getTransDefDescr.Importing = new PDI_ANA_GET_TRANS_DEF_DESCR.ImportingType();
      getTransDefDescr.Importing.IV_LANGUAGE_CODE = "E";
      getTransDefDescr.Importing.IT_TD_PROXY_NAME = proxyNames;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) getTransDefDescr, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (getTransDefDescr.Exporting != null)
      {
        if (!(getTransDefDescr.Exporting.EV_SUCCESS == "") || getTransDefDescr.Exporting.ET_MESSAGES == null || getTransDefDescr.Exporting.ET_MESSAGES.Length <= 0)
          return getTransDefDescr.Exporting.ET_TD_PROXY_NAME_AND_DESCR;
        this.reportServerSideProtocolException((SAPFunctionModule) getTransDefDescr, false, false);
      }
      return (PDI_PROXY_NAME_AND_DESCR[]) null;
    }

    public PDI_DS_GET_BO_NAV_INFOS.NAV_INFO[] GetBoNavInfo(string IV_BO_ID, string IV_BO_NODE_ID, string IV_BO_NS_ID, string IV_DT_MODE, string IV_DT_SOLUTION)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_DS_GET_BO_NAV_INFOS pdiDsGetBoNavInfos = new PDI_DS_GET_BO_NAV_INFOS();
      pdiDsGetBoNavInfos.Importing = new PDI_DS_GET_BO_NAV_INFOS.ImportingType();
      pdiDsGetBoNavInfos.Importing.IV_BO_ID = IV_BO_ID;
      pdiDsGetBoNavInfos.Importing.IV_NODE_ID = IV_BO_NODE_ID;
      pdiDsGetBoNavInfos.Importing.IV_BO_NS_ID = IV_BO_NS_ID;
      pdiDsGetBoNavInfos.Importing.IV_DT_MODE = IV_DT_MODE;
      pdiDsGetBoNavInfos.Importing.IV_DT_SOLUTION = IV_DT_SOLUTION;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiDsGetBoNavInfos, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (pdiDsGetBoNavInfos.Exporting != null)
        return pdiDsGetBoNavInfos.Exporting.ET_NAV_INFOS;
      return (PDI_DS_GET_BO_NAV_INFOS.NAV_INFO[]) null;
    }

    public void SyncAnalyticsXrepContent(string abapNamespace, string xrepPath)
    {
      ErrorSink.Instance.ClearTasksFor(Origin.AnalyticsContent);
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_ANA_SYNC_XREP_CONTENT anaSyncXrepContent = new PDI_ANA_SYNC_XREP_CONTENT();
      anaSyncXrepContent.Importing = new PDI_ANA_SYNC_XREP_CONTENT.ImportingType();
      anaSyncXrepContent.Importing.IV_ABAP_NAMESPACE = abapNamespace;
      anaSyncXrepContent.Importing.IV_XREP_PATH = xrepPath;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) anaSyncXrepContent, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (anaSyncXrepContent.Exporting == null || !(anaSyncXrepContent.Exporting.EV_SUCCESS == "") || (anaSyncXrepContent.Exporting.ET_MESSAGES == null || anaSyncXrepContent.Exporting.ET_MESSAGES.Length <= 0))
        return;
      foreach (PDI_RI_S_MESSAGE pdiRiSMessage in anaSyncXrepContent.Exporting.ET_MESSAGES)
        ErrorSink.Instance.AddTask(pdiRiSMessage.TEXT, Origin.AnalyticsContent, Severity.Error, (EventHandler) null);
    }
  }
}
