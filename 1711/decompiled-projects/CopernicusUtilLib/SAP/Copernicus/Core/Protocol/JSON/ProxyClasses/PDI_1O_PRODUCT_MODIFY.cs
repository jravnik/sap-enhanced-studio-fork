﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_PRODUCT_MODIFY
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_PRODUCT_MODIFY : AbstractRemoteFunction<PDI_1O_PRODUCT_MODIFY.ImportingType, PDI_1O_PRODUCT_MODIFY.ExportingType, PDI_1O_PRODUCT_MODIFY.ChangingType, PDI_1O_PRODUCT_MODIFY.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E04DC271EE383E1F91BDB5E5665";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
      [DataMember]
      public string IV_DEV_PARTNER;
      [DataMember]
      public string IV_CONTACT_PERSON;
      [DataMember]
      public string IV_EMAIL;
      [DataMember]
      public string IV_LONG_TEXT;
      [DataMember]
      public string IV_KEYWORDS;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_S_PRODUCT_VERS_AND_COMPS ES_PRODUCT;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
