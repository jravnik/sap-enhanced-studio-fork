﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_GET_SECURITY_INFO
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_GET_SECURITY_INFO : AbstractRemoteFunction<PDI_1O_GET_SECURITY_INFO.ImportingType, PDI_1O_GET_SECURITY_INFO.ExportingType, PDI_1O_GET_SECURITY_INFO.ChangingType, PDI_1O_GET_SECURITY_INFO.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0135771ED0B88696F5F80580EB";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_KTD_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_HTML;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
