﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.IBusinessConfigurationHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public interface IBusinessConfigurationHandler
  {
    void deployBusinessConfiguration(string iv_component_name, string sessionID, bool bacDefined);

    BAC_PDI_S_TREE_ELEMENT[] getBACTreeNodes(int types, string[] bac_ids);

    BAC_PDI_S_TREE_ELEMENT[] getBACTreeNodesBySemanticLabel(string[] bac_ids);

    PDI_S_BCO_SCHEMA_NODE[] getBCOInformation(string proxyname, string abapNamespace, out string bcoDescription, out PDI_S_APPLICATION_F4[] valueHelp, out bool ftExists);

    PDI_S_BC_VALUE_DESCRIPTION[] getBCOForeignKeyValues(string bcNodeID, string fieldName, string abapNamespace, BCT_S_FIELD_VALUE_RANGE[] valueFilter);

    PDI_S_BC_VALUE_DESCRIPTION[] getBCOValueHelp(string bcoProxyname, string bcNodeID, string fieldName, string abapNamespace, BCT_S_FIELD_VALUE_RANGE[] valueFilter);

    PDI_S_BC_VALUE_DESCRIPTION[] getCodeValues(string nsName, string codeProxyName);

    bool completeImplementationProjectTemplate(string abapNamespace, string templateID, string tmeplateType);

    bool reopenImplementationProjectTemplate(string abapNamespace, string templateID, string tmeplateType);

    string getImplementationProjectTemplateID(string abapNamespace, string templateID, string templateType);

    bool checkConsistency(string content, PDI_S_CC_BCNODE_LINE[] nodes, out BCE_S_DTE_CHECK_ERROR_OCC[] occurences);

    BCT_S_BCSET_LIST[] checkBCSetOfBCO(string nsname, string bconame);

    bool createBCView(string nsname, string bcoproxyname, out PDI_BC_BCVIEW_MAINTAIN.ExportingType ecoInfo);

    bool deleteBCView(string nsname, string bcoproxyname);
  }
}
