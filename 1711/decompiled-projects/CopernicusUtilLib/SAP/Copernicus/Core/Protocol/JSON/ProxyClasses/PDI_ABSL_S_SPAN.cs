﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_S_SPAN
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.TextManager.Interop;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_S_SPAN
  {
    [DataMember]
    public int START_LINE;
    [DataMember]
    public int START_COL;
    [DataMember]
    public int END_LINE;
    [DataMember]
    public int END_COL;
    private TextSpan? _textspan;

    public TextSpan textspan
    {
      get
      {
        if (!this._textspan.HasValue)
          this._textspan = new TextSpan?(new TextSpan()
          {
            iStartLine = this.START_LINE,
            iStartIndex = this.START_COL,
            iEndLine = this.END_LINE,
            iEndIndex = this.END_COL
          });
        return this._textspan.Value;
      }
      private set
      {
      }
    }
  }
}
