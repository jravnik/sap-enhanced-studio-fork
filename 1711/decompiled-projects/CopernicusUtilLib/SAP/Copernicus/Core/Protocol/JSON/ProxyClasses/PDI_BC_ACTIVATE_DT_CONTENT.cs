﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_ACTIVATE_DT_CONTENT
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_ACTIVATE_DT_CONTENT : AbstractRemoteFunction<PDI_BC_ACTIVATE_DT_CONTENT.ImportingType, PDI_BC_ACTIVATE_DT_CONTENT.ExportingType, PDI_BC_ACTIVATE_DT_CONTENT.ChangingType, PDI_BC_ACTIVATE_DT_CONTENT.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0286FA1ED2ACF7D1835C6AC3E9";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PATH;
      [DataMember]
      public string IV_TREE_ID;
      [DataMember]
      public string IV_TREE_DESC;
      [DataMember]
      public string IV_COUNTRY;
      [DataMember]
      public bool IV_WHT;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_XMLSTRING;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_BC_ACTIVATE_DT_CONTENT.PDI_DT[] E_DT;
      [DataMember]
      public PDI_BC_ACTIVATE_DT_CONTENT.PDI_DT_T[] E_DT_T;
      [DataMember]
      public PDI_BC_ACTIVATE_DT_CONTENT.PDI_DT_DATA[] E_DT_DATA;
      [DataMember]
      public PDI_BC_ACTIVATE_DT_CONTENT.PDI_DT_TEXTS[] E_DT_TEXTS;
      [DataMember]
      public PDI_BC_ACTIVATE_DT_CONTENT.PDI_DT_TGTCTX[] E_DT_TGTCTX;
      [DataMember]
      public PDI_BC_ACTIVATE_DT_CONTENT.PDI_DT_DECTR_DET[] E_DT_DECTR_DET;
      [DataMember]
      public string E_STRING;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }

    [DataContract]
    public class PDI_DT_DATA
    {
      [DataMember]
      public string CLIENT;
      [DataMember]
      public string TREETYPE;
      [DataMember]
      public string TREEID;
      [DataMember]
      public string VERSION;
      [DataMember]
      public string XMLDOC;
      [DataMember]
      public string CR_DATE;
      [DataMember]
      public string CR_TIME;
      [DataMember]
      public string CR_USER;
      [DataMember]
      public string CHG_DATE;
      [DataMember]
      public string CHG_TIME;
      [DataMember]
      public string CHG_USER;
      [DataMember]
      public string REL_DATE;
      [DataMember]
      public string REL_TIME;
      [DataMember]
      public string REL_USER;
      [DataMember]
      public string STATUS;
      [DataMember]
      public string ORIGIN;
      [DataMember]
      public string VALIDFROM;
      [DataMember]
      public string TTE_VERSION;
    }

    [DataContract]
    public class PDI_DT_TEXTS
    {
      [DataMember]
      public string CLIENT;
      [DataMember]
      public string LANGU;
      [DataMember]
      public string TREEID;
      [DataMember]
      public string TREETYPE;
      [DataMember]
      public string VERSION;
      [DataMember]
      public string TEXTID;
      [DataMember]
      public string TEXT;
    }

    [DataContract]
    public class PDI_DT_T
    {
      [DataMember]
      public string CLIENT;
      [DataMember]
      public string LANGU;
      [DataMember]
      public string TREETYPE;
      [DataMember]
      public string TREEID;
      [DataMember]
      public string TREETEXT;
    }

    [DataContract]
    public class PDI_DT
    {
      [DataMember]
      public string CLIENT;
      [DataMember]
      public string TREETYPE;
      [DataMember]
      public string TREEID;
      [DataMember]
      public string FCATVAR;
      [DataMember]
      public string ORIGIN;
      [DataMember]
      public string STATUS;
      [DataMember]
      public string REL_VERSION;
      [DataMember]
      public string MNT_VERSION;
    }

    [DataContract]
    public class PDI_DT_TGTCTX
    {
      [DataMember]
      public string CLIENT;
      [DataMember]
      public string TREETYPE;
      [DataMember]
      public string TREEID;
      [DataMember]
      public string VERSION;
      [DataMember]
      public string FIELDNAME;
      [DataMember]
      public string CTX_FLDNAME;
      [DataMember]
      public string CTX_VALUE;
    }

    [DataContract]
    public class PDI_DT_DECTR_DET
    {
      [DataMember]
      public string CLIENT;
      [DataMember]
      public string COUNTRYISO;
      [DataMember]
      public string TAXCASE;
      [DataMember]
      public string VALIDTO;
      [DataMember]
      public string VALIDFR;
      [DataMember]
      public string TREETYPE;
      [DataMember]
      public string TREEID;
    }
  }
}
