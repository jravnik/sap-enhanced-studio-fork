﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GDPR_DISCLOSURE_PROFILES
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_GDPR_DISCLOSURE_PROFILES : AbstractRemoteFunction<PDI_GDPR_DISCLOSURE_PROFILES.ImportingType, PDI_GDPR_DISCLOSURE_PROFILES.ExportingType, PDI_GDPR_DISCLOSURE_PROFILES.ChangingType, PDI_GDPR_DISCLOSURE_PROFILES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F91ED79E810E43BE8D8374";
      }
    }

    [DataContract]
    public class ImportingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public ET_PROFILE[] ET_PROFILE;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
