﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_CONTENT_INFO
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses
{
  [DataContract]
  public class OSLS_XREP_CONTENT_INFO
  {
    [DataMember]
    public string FILE_PATH;
    [DataMember]
    public string FULL_PATH;
    [DataMember]
    public string IS_DIRECTORY;
    [DataMember]
    public string IS_VIRTUAL;
    [DataMember]
    public string IS_DELETED;
    [DataMember]
    public OSLS_XREP_FILE_ATTR[] FILE_ATTRS;
  }
}
