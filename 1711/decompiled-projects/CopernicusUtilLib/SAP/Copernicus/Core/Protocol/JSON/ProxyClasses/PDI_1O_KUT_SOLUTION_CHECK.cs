﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_KUT_SOLUTION_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_1O_KUT_SOLUTION_CHECK : AbstractRemoteFunction<PDI_1O_KUT_SOLUTION_CHECK.ImportingType, PDI_1O_KUT_SOLUTION_CHECK.ExportingType, PDI_1O_KUT_SOLUTION_CHECK.ChangingType, PDI_1O_KUT_SOLUTION_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E04DC271EE3BFA2800B538ECA19";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_KUT_SOLUTION;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
