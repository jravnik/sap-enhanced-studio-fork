﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_LIST_FORM_TMPL
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_LIST_FORM_TMPL : AbstractRemoteFunction<PDI_EXT_LIST_FORM_TMPL.ImportingType, PDI_EXT_LIST_FORM_TMPL.ExportingType, PDI_EXT_LIST_FORM_TMPL.ChangingType, PDI_EXT_LIST_FORM_TMPL.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01151C1EE084B4A11B045B8950";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PATH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_EXT_S_FORM_TMPL_INFO[] ET_FORM_TMPL;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
