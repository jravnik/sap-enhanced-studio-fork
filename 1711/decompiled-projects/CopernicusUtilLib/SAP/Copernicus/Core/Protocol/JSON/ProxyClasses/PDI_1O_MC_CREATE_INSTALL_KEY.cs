﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_MC_CREATE_INSTALL_KEY
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_1O_MC_CREATE_INSTALL_KEY : AbstractRemoteFunction<PDI_1O_MC_CREATE_INSTALL_KEY.ImportingType, PDI_1O_MC_CREATE_INSTALL_KEY.ExportingType, PDI_1O_MC_CREATE_INSTALL_KEY.ChangingType, PDI_1O_MC_CREATE_INSTALL_KEY.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0520391ED38594ADC5F1A093FD";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_GENERATION_NAMESPACE;
      [DataMember]
      public string IV_CUSTOMER_ID;
      [DataMember]
      public string IV_CUSTOMER_NAME;
      [DataMember]
      public string IV_CUSTOMER_TENANT_URL;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_INSTALLATION_KEY;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
