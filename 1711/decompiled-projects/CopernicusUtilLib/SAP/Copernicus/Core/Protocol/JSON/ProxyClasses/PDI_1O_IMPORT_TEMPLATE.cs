﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_IMPORT_TEMPLATE
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_IMPORT_TEMPLATE : AbstractRemoteFunction<PDI_1O_IMPORT_TEMPLATE.ImportingType, PDI_1O_IMPORT_TEMPLATE.ExportingType, PDI_1O_IMPORT_TEMPLATE.ChangingType, PDI_1O_IMPORT_TEMPLATE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011ED19CE9007C442B4432";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION_NAME;
      [DataMember]
      public string IV_SOLUTION_ASSEMBLE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
