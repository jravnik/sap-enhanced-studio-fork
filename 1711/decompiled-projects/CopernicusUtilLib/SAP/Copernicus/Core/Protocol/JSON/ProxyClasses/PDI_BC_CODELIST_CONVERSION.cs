﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_CODELIST_CONVERSION
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_CODELIST_CONVERSION : AbstractRemoteFunction<PDI_BC_CODELIST_CONVERSION.ImportingType, PDI_BC_CODELIST_CONVERSION.ExportingType, PDI_BC_CODELIST_CONVERSION.ChangingType, PDI_BC_CODELIST_CONVERSION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E03836E1ED2B3BB4C6FC1B94498";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_XREP_PATH;
      [DataMember]
      public string IV_SESSION_ID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string[] ET_FILE_CREATED;
      [DataMember]
      public string[] ET_FILE_DELETED;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
