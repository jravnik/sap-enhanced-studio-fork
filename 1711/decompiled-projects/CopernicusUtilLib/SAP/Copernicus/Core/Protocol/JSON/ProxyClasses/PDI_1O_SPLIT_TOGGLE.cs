﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_SPLIT_TOGGLE
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_SPLIT_TOGGLE : AbstractRemoteFunction<PDI_1O_SPLIT_TOGGLE.ImportingType, PDI_1O_SPLIT_TOGGLE.ExportingType, PDI_1O_SPLIT_TOGGLE.ChangingType, PDI_1O_SPLIT_TOGGLE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0520391ED595DA96F85CBD9E15";
      }
    }

    [DataContract]
    public class ImportingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SPLIT_ENABLED;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
