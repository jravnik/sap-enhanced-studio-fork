﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LogonForm
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol
{
    public class LogonForm : Form
    {
        private IContainer components;
        private Label labelSystem;
        private Label labelUser;
        private Label labelPwd;
        private LinkLabel labelFavourite1;
        private LinkLabel labelFavourite2;
        private LinkLabel labelFavourite3;
        private Button buttonOk;
        private Button buttonCancel;
        private TextBox textBoxPwd;
        private TextBox textBoxUser;
        private GroupedComboBox comboBoxSystem;
        private ImageList imageList1;
        private Label label1;
        private Label label2;
        private ToolTip toolTip1;
        private Label labelEditPassword;
        private Label labelEditSystem;
        private LogonFormSettings logonFormSettings;
        private Connection connection;
        private bool lostFocus;
        private List<ConnectionDataSet.SystemDataRow> listFavourites;

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = (IContainer)new Container();
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(LogonForm));
            this.labelFavourite1 = new LinkLabel();
            this.labelFavourite2 = new LinkLabel();
            this.labelFavourite3 = new LinkLabel();
            this.labelSystem = new Label();
            this.labelUser = new Label();
            this.labelPwd = new Label();
            this.buttonOk = new Button();
            this.buttonCancel = new Button();
            this.textBoxPwd = new TextBox();
            this.textBoxUser = new TextBox();
            this.comboBoxSystem = new GroupedComboBox();
            this.imageList1 = new ImageList(this.components);
            this.label1 = new Label();
            this.label2 = new Label();
            this.toolTip1 = new ToolTip(this.components);
            this.labelEditPassword = new Label();
            this.labelEditSystem = new Label();
            this.SuspendLayout();
            this.labelFavourite1.AutoSize = true;
            this.labelFavourite1.Location = new Point(4, 90);
            this.labelFavourite1.Name = "labelFavourite1";
            this.labelFavourite1.Size = new Size(41, 13);
            this.labelFavourite1.TabIndex = 0;
            this.labelFavourite1.Text = "";
            this.labelFavourite1.Font = new Font("Microsoft Sans Serif", 7.25f, FontStyle.Underline, GraphicsUnit.Point, (byte)0);
            this.labelFavourite1.MouseClick += new MouseEventHandler(this.labelFavourite_Click);
            this.labelFavourite2.AutoSize = true;
            this.labelFavourite2.Location = new Point(4, 108);
            this.labelFavourite2.Name = "labelFavourite2";
            this.labelFavourite2.Size = new Size(41, 13);
            this.labelFavourite2.TabIndex = 0;
            this.labelFavourite2.Text = "";
            this.labelFavourite2.Font = new Font("Microsoft Sans Serif", 7.25f, FontStyle.Underline, GraphicsUnit.Point, (byte)0);
            this.labelFavourite2.MouseClick += new MouseEventHandler(this.labelFavourite_Click);
            this.labelFavourite3.AutoSize = true;
            this.labelFavourite3.Location = new Point(4, 126);
            this.labelFavourite3.Name = "labelFavourite3";
            this.labelFavourite3.Size = new Size(41, 13);
            this.labelFavourite3.TabIndex = 0;
            this.labelFavourite3.Text = "";
            this.labelFavourite3.Font = new Font("Microsoft Sans Serif", 7.25f, FontStyle.Underline, GraphicsUnit.Point, (byte)0);
            this.labelFavourite3.MouseClick += new MouseEventHandler(this.labelFavourite_Click);
            this.labelSystem.AutoSize = true;
            this.labelSystem.Location = new Point(4, 15);
            this.labelSystem.Name = "labelSystem";
            this.labelSystem.Size = new Size(41, 13);
            this.labelSystem.TabIndex = 0;
            this.labelSystem.Text = "System";
            this.labelUser.AutoSize = true;
            this.labelUser.Location = new Point(4, 42);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new Size(29, 13);
            this.labelUser.TabIndex = 0;
            this.labelUser.Text = "User";
            this.labelPwd.AutoSize = true;
            this.labelPwd.Location = new Point(4, 68);
            this.labelPwd.Name = "labelPwd";
            this.labelPwd.Size = new Size(53, 13);
            this.labelPwd.TabIndex = 0;
            this.labelPwd.Text = "Password";
            this.buttonOk.Location = new Point(237, 113);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new Size(75, 23);
            this.buttonOk.TabIndex = 5;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new EventHandler(this.buttonOk_Click);
            this.buttonCancel.Location = new Point(318, 113);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new Size(75, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
            this.textBoxPwd.Location = new Point(63, 65);
            this.textBoxPwd.Name = "textBoxPwd";
            this.textBoxPwd.PasswordChar = '*';
            this.textBoxPwd.Size = new Size(330, 20);
            this.textBoxPwd.TabIndex = 4;
            this.textBoxPwd.TextChanged += new EventHandler(this.textChanged);
            this.textBoxUser.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.textBoxUser.AutoCompleteSource = AutoCompleteSource.CustomSource;
            this.textBoxUser.Location = new Point(63, 39);
            this.textBoxUser.Name = "textBoxUser";
            this.textBoxUser.Size = new Size(330, 20);
            this.textBoxUser.TabIndex = 3;
            this.textBoxUser.TextChanged += new EventHandler(this.textChanged);
            this.textBoxUser.KeyDown += new KeyEventHandler(this.textBoxKeyDown);
            this.comboBoxSystem.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.comboBoxSystem.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxSystem.FormattingEnabled = true;
            this.comboBoxSystem.Location = new Point(63, 12);
            this.comboBoxSystem.Name = "comboBoxSystem";
            this.comboBoxSystem.Size = new Size(330, 21);
            this.comboBoxSystem.TabIndex = 1;
            this.comboBoxSystem.DropDown += new EventHandler(this.comboBoxSystem_DropDown);
            this.comboBoxSystem.SelectedIndexChanged += new EventHandler(this.comboBoxSystem_SelectedIndexChanged);
            this.comboBoxSystem.KeyDown += new KeyEventHandler(this.textBoxKeyDown);
            this.comboBoxSystem.ValueMember = "Value";
            this.comboBoxSystem.DisplayMember = "Display";
            this.comboBoxSystem.GroupMember = "Group";
            this.imageList1.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("imageList1.ImageStream");
            this.imageList1.TransparentColor = Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "InformationMark.png");
            this.imageList1.Images.SetKeyName(1, "DisplayEdit16x16.png");
            this.label1.ImageKey = "InformationMark.png";
            this.label1.ImageList = this.imageList1;
            this.label1.Location = new Point(397, 12);
            this.label1.Name = "label1";
            this.label1.Size = new Size(32, 25);
            this.label1.TabIndex = 7;
            this.toolTip1.SetToolTip((Control)this.label1, "Select the system you want to connect to. You can change  the logon data in the Administration menu by clicking “Options and Settings…”.");
            this.label1.Click += new EventHandler(this.ShowSystemToolTip);
            this.label2.ImageKey = "InformationMark.png";
            this.label2.ImageList = this.imageList1;
            this.label2.Location = new Point(397, 64);
            this.label2.Name = "label2";
            this.label2.Size = new Size(32, 23);
            this.label2.TabIndex = 8;
            this.toolTip1.SetToolTip((Control)this.label2, "If you need to change your password or your user is locked, click “Change Password”. You then log on to the system in the browser where you can adjust your user or password.");
            this.label2.Click += new EventHandler(this.ShowPasswordToolTip);
            this.labelEditPassword.ImageKey = "DisplayEdit16x16.png";
            this.labelEditPassword.ImageList = this.imageList1;
            this.labelEditPassword.Location = new Point(429, 64);
            this.labelEditPassword.Name = "labelEditPassword";
            this.labelEditPassword.Size = new Size(32, 23);
            this.labelEditPassword.TabIndex = 13;
            this.toolTip1.SetToolTip((Control)this.labelEditPassword, "Change Password. ");
            this.labelEditPassword.Click += new EventHandler(this.labelEditPassword_Click);
            this.labelEditSystem.ImageKey = "DisplayEdit16x16.png";
            this.labelEditSystem.ImageList = this.imageList1;
            this.labelEditSystem.Location = new Point(429, 12);
            this.labelEditSystem.Name = "labelEditSystem";
            this.labelEditSystem.Size = new Size(32, 23);
            this.labelEditSystem.TabIndex = 14;
            this.toolTip1.SetToolTip((Control)this.labelEditSystem, "Edit System Entries.");
            this.labelEditSystem.Click += new EventHandler(this.labelEditSystem_Click);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(470, 148);
            this.Controls.Add((Control)this.labelEditSystem);
            this.Controls.Add((Control)this.labelEditPassword);
            this.Controls.Add((Control)this.label2);
            this.Controls.Add((Control)this.label1);
            this.Controls.Add((Control)this.comboBoxSystem);
            this.Controls.Add((Control)this.textBoxUser);
            this.Controls.Add((Control)this.textBoxPwd);
            this.Controls.Add((Control)this.buttonCancel);
            this.Controls.Add((Control)this.buttonOk);
            this.Controls.Add((Control)this.labelPwd);
            this.Controls.Add((Control)this.labelUser);
            this.Controls.Add((Control)this.labelSystem);
            this.Controls.Add((Control)this.labelFavourite1);
            this.Controls.Add((Control)this.labelFavourite2);
            this.Controls.Add((Control)this.labelFavourite3);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.Name = nameof(LogonForm);
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "Connect to Repository";
            this.Activated += new EventHandler(this.LogonForm_Activated_1);
            this.Leave += new EventHandler(this.LogonForm_Leave);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private void labelFavourite_Click(object sender, MouseEventArgs e)
        {
            ConnectionDataSet.SystemDataRow favourite = null;

            if (sender.Equals(labelFavourite1))
            {
                favourite = this.listFavourites[0];
            } 
            else if (sender.Equals(labelFavourite2))
            {
                favourite = this.listFavourites[1];
            }
            else if (sender.Equals(labelFavourite3))
            {
                favourite = this.listFavourites[2];
            }

            if (favourite != null)
            {
                string str = String.Format("{0} ({1})", favourite.Name, favourite.Host);
                this.comboBoxSystem.SelectedValue = str;
            }
        }

        public bool Result { get; private set; }

        public LogonForm(Connection connection)
        {
            this.Result = false;
            this.InitializeComponent();
            this.labelEditPassword.BringToFront();
            this.labelEditSystem.BringToFront();
            this.logonFormSettings = new LogonFormSettings();
            this.textBoxUser.AutoCompleteCustomSource = this.logonFormSettings.UsedUserIDHistory;
            this.Icon = Resource.SAPBusinessByDesignStudioIcon;
            this.Text = Resource.LogonForm_Title;
            this.labelSystem.Text = Resource.LogonForm_Label_System;
            this.labelUser.Text = Resource.LogonForm_Label_User;
            this.labelPwd.Text = Resource.LogonForm_Label_Pwd;
            this.buttonOk.Text = Resource.LogonForm_Btn_Ok;
            this.buttonCancel.Text = Resource.LogonForm_Btn_Cancel;
            this.buttonOk.Enabled = false;
            this.connection = connection;
            this.listFavourites = new List<ConnectionDataSet.SystemDataRow>();
            this.comboBoxSystem.DataSource = this.fillSystemDropDown();
            if (this.comboBoxSystem.Items.Count > 0)
                this.comboBoxSystem.SelectedIndex = 0;
            this.textBoxUser.KeyPress += new KeyPressEventHandler(this.checkForEnter_KeyPress);
            this.textBoxPwd.KeyPress += new KeyPressEventHandler(this.checkForEnter_KeyPress);
            this.Activated += new EventHandler(this.LogonForm_Activated);
        }

        private void checkForEnter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar != 13)
                return;
            e.Handled = true;
            if (!this.buttonOk.Focus())
                return;
            this.submitForm();
        }

        private void LogonForm_Activated(object sender, EventArgs e)
        {
            this.textBoxUser.Focus();
        }

        private ArrayList fillSystemDropDown()
        {
            List<object> list = new List<object>();
            this.listFavourites.Clear();

            foreach (ConnectionDataSet.SystemDataRow row in (InternalDataCollectionBase)this.connection.Connections.Tables["SystemData"].Rows)
            {
                string name = row.Name;
                string host = row.Host;
                string group = (row.IsGroupNull() || row.Group == null || row.Group.Length == 0 ? "Other" : row.Group);
                int httpPort = row.HttpPort;

                string str = String.Format("{0} ({1})", name, host);

                list.Add(new SystemsComboBoxItem { Group = group, Value = str, Display = str });

                if (row.Favourite)
                {
                    this.listFavourites.Add(row);
                }
            }

            this.listFavourites.Sort((x, y) => x.Name.CompareTo(y.Name));

            while (this.listFavourites.Count < 3)
            {
                this.listFavourites.Add(null);
            }

            this.labelFavourite1.Text = (this.listFavourites[0] != null ? this.listFavourites[0].Name : "");
            this.labelFavourite2.Text = (this.listFavourites[1] != null ? this.listFavourites[1].Name : "");
            this.labelFavourite3.Text = (this.listFavourites[2] != null ? this.listFavourites[2].Name : "");

            return new ArrayList(list.ToArray());
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this.submitForm();
        }

        private void submitForm()
        {
            object selectedItem = this.comboBoxSystem.SelectedValue;
            if (selectedItem == null)
                return;
            string str1 = (string)selectedItem;
            string connectionName = str1.Substring(0, str1.IndexOf(" "));
            string str2 = this.textBoxUser.Text.Trim();
            this.Result = Connection.getInstance().Connect(connectionName, str2, SecureStringUtil.stringToSecureString(this.textBoxPwd.Text), false);
            if (!this.Result)
                return;
            this.addUserIDIntoHistoryList(str2);
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Result = false;
            this.Close();
        }

        private void comboBoxSystem_SelectedIndexChanged(object sender, EventArgs e)
        {
            object selectedItem = this.comboBoxSystem.SelectedValue;
            if (selectedItem == null)
                return;

            string str = (string)selectedItem;

            ConnectionDataSet.SystemDataRow row = ((ConnectionDataSet.SystemDataRow[])this.connection.Connections.Tables["SystemData"].Select("Name='" + str.Substring(0, str.IndexOf(" ")) + "'"))[0];

            int client = row.Client;

            if (!row.IsDefaultUserNull() && row.DefaultUser != null && row.DefaultUser.Length != 0)
            {
                if (row.DefaultUser.Contains("/"))
                {
                    string[] users = row.DefaultUser.Split('/');

                    if (users[0].Length != 0)
                    {
                        textBoxUser.Text = users[0];
                    }

                    if (users[1].Length != 0)
                    {
                        PropertyAccess.GeneralProps.TracingUser = users[1].ToUpper();
                    }
                }
                else
                {
                    textBoxUser.Text = row.DefaultUser;
                }
            }
            else
            {
                textBoxUser.Text = "";
            }
        }

        private void addUserIDIntoHistoryList(string userID)
        {
            if (this.logonFormSettings.UsedUserIDHistory.Contains(userID.Trim()))
                return;
            if (this.logonFormSettings.UsedUserIDHistory.Count >= 50)
                this.logonFormSettings.UsedUserIDHistory.RemoveAt(0);
            this.logonFormSettings.UsedUserIDHistory.Add(userID);
            this.logonFormSettings.Save();
        }

        private void ShowSystemToolTip(object sender, EventArgs e)
        {
            int num = (int)CopernicusMessageBox.Show(Resource.MsgSystemToolTip, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void ShowPasswordToolTip(object sender, EventArgs e)
        {
            int num = (int)CopernicusMessageBox.Show(Resource.MsgPasswordToolTip, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void ShowBrowserLogonToolTip(object sender, EventArgs e)
        {
            int num = (int)CopernicusMessageBox.Show(Resource.MsgBrowserLogonToolTip, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void OpenBrowserLogon(object sender, EventArgs e)
        {
            this.changePassowrd();
        }

        private void changePassowrd()
        {
            object selectedItem = this.comboBoxSystem.SelectedValue;
            if (selectedItem == null)
                return;
            string str = (string)selectedItem;
            ByDUIUtil.LaunchLogonUI(((ConnectionDataSet.SystemDataRow[])this.connection.Connections.Tables["SystemData"].Select("Name='" + str.Substring(0, str.IndexOf(" ")) + "'"))[0]);
        }

        private void textChanged(object sender, EventArgs e)
        {
            if (this.textBoxPwd.Text.Length > 0 && this.textBoxUser.Text.Length > 0)
                this.buttonOk.Enabled = true;
            else
                this.buttonOk.Enabled = false;
        }

        private void userEnter(object sender, EventArgs e)
        {
        }

        private void textBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue != 13 || !this.buttonOk.Focus())
                return;
            this.submitForm();
        }

        private void labelEditPassword_Click(object sender, EventArgs e)
        {
            this.changePassowrd();
        }

        private void labelEditSystem_Click(object sender, EventArgs e)
        {
            DTEUtil.GetDTE().ExecuteCommand("Administration.OptionsandSettings", "");
        }

        private void LogonForm_Activated_1(object sender, EventArgs e)
        {
            if (!this.lostFocus)
                return;
            this.comboBoxSystem.Items.Clear();
            this.fillSystemDropDown();
        }

        private void LogonForm_Leave(object sender, EventArgs e)
        {
            this.lostFocus = true;
        }

        private void comboBoxSystem_DropDown(object sender, EventArgs e)
        {
            this.comboBoxSystem.DataSource = this.fillSystemDropDown();
            this.comboBoxSystem.DropDownHeight = 300;
        }

        private void comboBoxSystem_Click(object sender, EventArgs e)
        {
            this.comboBoxSystem.Items.Clear();
            this.fillSystemDropDown();
        }
    }
}
