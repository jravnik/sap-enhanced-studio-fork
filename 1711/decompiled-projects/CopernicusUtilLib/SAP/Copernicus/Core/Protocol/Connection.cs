﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Connection
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.LoginCheckServices;
using SAP.Copernicus.Core.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Security;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol
{
  public class Connection
  {
    internal static string DEFAULT_PATH_REP = "/sap/ap/pdimdrs";
    private static string ALIAS_LOGON_PATH_JSON = "/sap/ap/xrep/json_pdi";
    private static string DEFAULT_PATH_JSON = "/sap/ap/xrep/json3";
    private static string TEST_PATH_JSON = "/sap/ap/pdi";
    protected static Connection instance = new Connection();
    [ThreadStatic]
    private static int threadConnnectionID = 0;
    protected ConnectionDataSet connectionDataSet = new ConnectionDataSet();
    private readonly object connectionLock = new object();
    private LogonData logonData = new LogonData();
    protected bool HeadLessFlag;
    private int connectionID;
    private ConnectionDataSet.SystemDataRow connectedSystem;

    public bool HeadLessMode
    {
      get
      {
        return this.HeadLessFlag;
      }
      private set
      {
        this.HeadLessFlag = value;
      }
    }

    public event onDisconnect disconnectEvent;

    public event onConnect connectEvent;

    protected Connection()
    {
      ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(Connection.ValidateRemoteCertificate);
      this.ResetToDefault();
    }

    public void ResetToDefault()
    {
      this.connectionDataSet.Tables["SystemData"].Clear();
      this.connectionDataSet.Tables["systemdata"].Rows.Add((object) "QWF245", (object) "qwf-cust245.dev.sapbydesign.com", (object) 50000, (object) 443, (object) 0, (object) 801);
    }

    public ConnectionDataSet Connections
    {
      get
      {
        return this.connectionDataSet;
      }
    }

    public UsageMode UsageMode
    {
      get
      {
        return this.LogonData.UsageMode;
      }
    }

    public SystemMode SystemMode
    {
      get
      {
        return this.LogonData.SystemMode;
      }
    }

    public RestrictionMode RestrictionMode
    {
      get
      {
        return this.LogonData.RestrictionMode;
      }
    }

    public TenantRoleCode TenantRoleCode
    {
      get
      {
        return this.LogonData.TenantRoleCode;
      }
    }

    public string PartnerName
    {
      get
      {
        return this.LogonData.PartnerName;
      }
    }

    public string PartnerDomain
    {
      get
      {
        return this.LogonData.PartnerDomain;
      }
    }

    public string PartnerID
    {
      get
      {
        return this.LogonData.PartnerID;
      }
    }

    public string OfficialName
    {
      get
      {
        return this.LogonData.officialName;
      }
    }

    public string ReleaseName
    {
      get
      {
        return this.LogonData.releaseName;
      }
    }

    public bool IsMultiCustomerEnabled
    {
      get
      {
        return this.LogonData.IsMultiCustomerEnabled;
      }
    }

    public string getLoggedInUser()
    {
      ConnectionDataSet.SystemDataRow connectedSystem = this.ConnectedSystem;
      if (connectedSystem != null)
        return connectedSystem.User;
      return (string) null;
    }

    public string getLoggedInUserAlias()
    {
      ConnectionDataSet.SystemDataRow connectedSystem = this.ConnectedSystem;
      if (connectedSystem != null)
        return connectedSystem.UserAlias;
      return (string) null;
    }

    public static Connection getInstance()
    {
      for (int index1 = 0; index1 < Connection.instance.Connections.SystemData.Rows.Count; ++index1)
      {
        for (int index2 = 0; index2 < Connection.instance.Connections.SystemData.Rows[index1].ItemArray.Length; ++index2)
        {
          ConnectionDataSet.SystemDataRow row = (ConnectionDataSet.SystemDataRow) Connection.instance.Connections.SystemData.Rows[index1];
          if (row.Name == null || row.Name == "")
            row.Name = "SYS" + index2.ToString();
          int client = row.Client;
          if (row.Host == null || row.Host == "")
            row.Host = "HOST";
          int sslPort = row.SSLPort;
          int httpPort = row.HttpPort;
          int sysNo = row.SysNo;
          if (row.Language == null || row.Language == "")
            row.Language = "EN";
        }
      }
      return Connection.instance;
    }

    private static bool ValidateRemoteCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
    {
      return true;
    }

    public static string getJsonURLPath()
    {
      return Connection.DEFAULT_PATH_JSON;
    }

    public static string getAliasLogonJsonURLPath()
    {
      return Connection.ALIAS_LOGON_PATH_JSON;
    }

    public static string getJsonTestURLPath()
    {
      return Connection.TEST_PATH_JSON;
    }

    public static int GetDefaultTimeout()
    {
      return PropertyAccess.GeneralProps.ConnectivityTimeout;
    }

    internal void CheckConnectionChanged()
    {
      lock (this.connectionLock)
      {
        if (Connection.threadConnnectionID > 0)
        {
          if (Connection.threadConnnectionID != this.connectionID)
            throw new ClientDisconnectedException();
        }
        else
          Connection.threadConnnectionID = this.connectionID;
      }
    }

    public bool isConnected()
    {
      lock (this.connectionLock)
      {
        this.CheckConnectionChanged();
        return this.ConnectedSystem != null;
      }
    }

    public bool connect()
    {
      if (this.isConnected())
        return true;
      LogonForm logonForm = new LogonForm(this);
      int num1 = (int) logonForm.ShowDialog();
      if (!logonForm.Result || this.LogonData.UsageMode != UsageMode.miltiCustomer)
        return logonForm.Result;
      MCSLogon mcsLogon = new MCSLogon();
      int num2 = (int) mcsLogon.ShowDialog();
      this.ConnectedSystem.MCSKey = mcsLogon.Key;
      this.ConnectedSystem.MCSMode = mcsLogon.Type;
      if (!mcsLogon.Result)
        this.disconnect();
      return mcsLogon.Result;
    }

    public string GetXRepSessionID()
    {
      return this.LogonData.ValidatedXRepSessionID;
    }

    public bool IsAdminUser()
    {
      return this.LogonData.IsAdminUser;
    }

    public bool UsePSM
    {
      get
      {
        return this.LogonData.UsePSM;
      }
    }

    public bool Connect(string connectionName, string user, SecureString pwd)
    {
      return this.Connect(connectionName, user, pwd, true);
    }

    public bool Connect(string connectionName, string user, SecureString pwd, bool headLess)
    {
      if (this.isConnected())
        return true;
      ConnectionDataSet.SystemDataRow systemDataRow = ((ConnectionDataSet.SystemDataRow[]) this.connectionDataSet.Tables["SystemData"].Select("Name='" + connectionName + "'"))[0];
      systemDataRow.SecurePassword = pwd;
      List<ILoginMessage> loginMessages1 = new List<ILoginMessage>();
      bool tryAgain = false;
      LoginProcessor loginProcessor1 = new LoginProcessor();
      bool flag1 = loginProcessor1.PerformLogin(connectionName, user, systemDataRow.SecurePassword, out loginMessages1, out tryAgain);
      JSONClient.SetCookies(loginProcessor1.Cookies);
      if (!flag1)
      {
        if (tryAgain)
        {
          LoginProcessor loginProcessor2 = new LoginProcessor();
          List<ILoginMessage> loginMessages2;
          bool flag2 = loginProcessor2.PerformLogin(connectionName, user, systemDataRow.SecurePassword, out loginMessages2, out tryAgain);
          JSONClient.SetCookies(loginProcessor2.Cookies);
          if (!flag2)
          {
            string text = "";
            foreach (ILoginMessage loginMessage in loginMessages2)
              text = text + loginMessage.Text + "\n";
            int num = (int) CopernicusMessageBox.Show(text, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            return false;
          }
          if (loginMessages1.Count > 0)
          {
            string text = "";
            foreach (ILoginMessage loginMessage in loginMessages1)
              text = text + loginMessage.Text + "\n";
            int num = (int) CopernicusMessageBox.Show(text, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
          }
        }
        else
        {
          if (loginMessages1.Count > 0)
          {
            string text = "";
            foreach (ILoginMessage loginMessage in loginMessages1)
              text = text + loginMessage.Text + "\n";
            int num = (int) CopernicusMessageBox.Show(text, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
          }
          return false;
        }
      }
      LogonData logonData = CopernicusWorkspace.Instance.EstablishBackendSession(systemDataRow, user, headLess);
      this.RaiseConnect();
      lock (this.connectionLock)
      {
        if (logonData == null)
          return false;
        Connection.threadConnnectionID = ++this.connectionID;
        this.ConnectedSystem = systemDataRow;
        this.ConnectedSystem.UserAlias = user.ToUpper();
        this.ConnectedSystem.User = logonData.backend_user;
        this.ConnectedSystem.SecurePassword = systemDataRow.SecurePassword;
        this.ConnectedSystem.SapSolution = logonData.SapSolutionCode;
        this.HeadLessFlag = headLess;
        this.logonData = logonData;
        return true;
      }
    }

    public void RaiseDisconnect()
    {
      if (this.disconnectEvent == null)
        return;
      this.disconnectEvent();
    }

    private void RaiseConnect()
    {
    }

    public void disconnect()
    {
      CopernicusWorkspace.Instance.UnlockWorkspace();
      lock (this.connectionLock)
      {
        this.ConnectedSystem = (ConnectionDataSet.SystemDataRow) null;
        this.logonData = new LogonData();
        Connection.threadConnnectionID = ++this.connectionID;
        JSONClient.ResetCookies();
      }
    }

    public string GetConnectionName()
    {
      ConnectionDataSet.SystemDataRow connectedSystem = this.ConnectedSystem;
      if (connectedSystem != null)
        return connectedSystem.Name;
      return (string) null;
    }

    public SapSolutionCode GetSAPSolution()
    {
      return this.ConnectedSystem.SapSolution;
    }

    public ConnectionDataSet.SystemDataRow getConnectedSystem()
    {
      return this.ConnectedSystem;
    }

    public virtual string GetJsonHttpURL()
    {
      ConnectionDataSet.SystemDataRow connectedSystem = this.ConnectedSystem;
      if (connectedSystem == null)
        return (string) null;
      return WebUtil.BuildURL(connectedSystem, Connection.getJsonURLPath());
    }

    public virtual string GetJsonTestHttpURL()
    {
      ConnectionDataSet.SystemDataRow connectedSystem = this.ConnectedSystem;
      if (connectedSystem == null)
        return (string) null;
      return WebUtil.BuildURL(connectedSystem, Connection.getJsonTestURLPath());
    }

    public ConnectionDataSet.SystemDataRow ConnectedSystem
    {
      get
      {
        ConnectionDataSet.SystemDataRow systemDataRow = (ConnectionDataSet.SystemDataRow) null;
        lock (this.connectionLock)
        {
          if (this.connectedSystem != null)
            systemDataRow = this.connectedSystem;
        }
        return systemDataRow;
      }
      protected set
      {
        lock (this.connectionLock)
        {
          if (value == null)
          {
            this.connectedSystem = (ConnectionDataSet.SystemDataRow) null;
          }
          else
          {
            DataTable dataTable = value.Table.Clone();
            dataTable.ImportRow((DataRow) value);
            this.connectedSystem = dataTable.Rows[0] as ConnectionDataSet.SystemDataRow;
          }
        }
      }
    }

    protected LogonData LogonData
    {
      get
      {
        LogonData logonData;
        lock (this.connectionLock)
          logonData = this.logonData;
        return logonData ?? new LogonData();
      }
    }
  }
}
