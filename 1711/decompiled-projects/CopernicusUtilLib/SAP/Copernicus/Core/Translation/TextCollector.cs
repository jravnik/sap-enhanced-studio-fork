﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Translation.TextCollector
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Translation
{
  public class TextCollector
  {
    private IDictionary<string, string> textfileAttributes = (IDictionary<string, string>) new Dictionary<string, string>();
    public const string AddTextContractName = "TextSink";

    public void AddText(string textID, TextType textType, string value)
    {
      string key = "TRANSEN_" + new TextIDCreator().getXrepTextType(textType) + "_" + textID;
      if (this.textfileAttributes.ContainsKey(key))
        this.textfileAttributes.Remove(key);
      this.textfileAttributes.Add(key, value);
    }

    public IDictionary<string, string> GetAllTextAttributes()
    {
      return this.textfileAttributes;
    }
  }
}
