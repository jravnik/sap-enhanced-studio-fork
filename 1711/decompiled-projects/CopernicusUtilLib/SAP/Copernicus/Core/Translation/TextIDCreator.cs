﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Translation.TextIDCreator
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Util;

namespace SAP.Copernicus.Core.Translation
{
  public class TextIDCreator
  {
    public string GetTextID_XBO_ExtensionField(TextType textType, string fieldName)
    {
      return RepositoryUtil.GetProxyNameForName((string) null, (string) null, "XF" + this.getXrepTextType(textType) + "_" + fieldName, 32);
    }

    public string GetTextID_BO_Element(string nameSpace, TextType textType, string boName, string nodeName, string elementName, out string mdrsTextName)
    {
      mdrsTextName = RepositoryUtil.GetProxyNameForName(ProjectUtil.GetRuntimeNamespacePrefixForPartnerNamespace(nameSpace), (string) null, boName + (object) '+' + nodeName + (object) '+' + elementName, 26);
      return "E" + this.getXrepTextType(textType) + "_" + mdrsTextName;
    }

    public string getXrepTextType(TextType texttype)
    {
      string str = (string) null;
      switch (texttype)
      {
        case TextType.Label:
          str = "XFLD";
          break;
        case TextType.Tooltip:
          str = "XTOL";
          break;
        case TextType.Message:
          str = "XMSG";
          break;
      }
      return str;
    }
  }
}
