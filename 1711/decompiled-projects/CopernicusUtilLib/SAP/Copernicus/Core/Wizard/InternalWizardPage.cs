﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Wizard.InternalWizardPage
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Automation;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Wizard
{
  public class InternalWizardPage : CopernicusWizardPage
  {
    public WizardBanner Banner;
    private IContainer components;
    private string HelpID;

    public InternalWizardPage()
    {
      this.InitializeComponent();
      this.HelpID = HELP_IDS.MSDNSTART;
      this.Banner.HelpRequested += new WizardBanner.HelpRequestedEventHandler(this.HelpRequested);
    }

    public InternalWizardPage(string helpID)
    {
      this.InitializeComponent();
      this.HelpID = helpID;
      this.Banner.HelpRequested += new WizardBanner.HelpRequestedEventHandler(this.HelpRequested);
    }

    private void HelpRequested(object sender)
    {
      HelpUtil.DisplayF1Help(this.HelpID);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.Banner = new WizardBanner();
      this.SuspendLayout();
      this.Banner.BackColor = SystemColors.Window;
      this.Banner.Dock = DockStyle.Top;
      this.Banner.Location = new Point(0, 0);
      this.Banner.Name = "Banner";
      this.Banner.Size = new Size(853, 64);
      this.Banner.Subtitle = "Subtitle";
      this.Banner.TabIndex = 0;
      this.Banner.Title = "Title";
      this.AutoSize = true;
      this.Controls.Add((Control) this.Banner);
      this.Name = nameof (InternalWizardPage);
      this.Size = new Size(853, 845);
      this.ResumeLayout(false);
    }
  }
}
