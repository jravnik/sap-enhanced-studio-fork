﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Wizard.CopernicusWizardPage
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Wizard
{
  [DefaultEvent("SetActive")]
  public class CopernicusWizardPage : UserControl
  {
    private Container components;

    public CopernicusWizardPage()
    {
      this.InitializeComponent();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.SuspendLayout();
      this.AutoScroll = true;
      this.AutoSize = true;
      this.Name = nameof (CopernicusWizardPage);
      this.Size = new Size(1103, 845);
      this.ResumeLayout(false);
    }

    protected CopernicusWizardSheet GetWizard()
    {
      return (CopernicusWizardSheet) this.ParentForm;
    }

    protected void SetWizardButtons(WizardPageType pageType)
    {
      this.GetWizard().SetWizardButtons(pageType);
    }

    protected void EnableNextButton(bool enableButton)
    {
      this.GetWizard().EnableNextButton(enableButton);
    }

    protected void EnableFinishButton(bool enableButton)
    {
      this.GetWizard().EnableFinishButton(enableButton);
    }

    protected void EnableBackButton(bool enableButton)
    {
      this.GetWizard().EnableBackButton(enableButton);
    }

    protected void EnableCancelButton(bool enableButton)
    {
      this.GetWizard().EnableCancelButton(enableButton);
    }

    protected void PressButton(WizardButtons buttons)
    {
      this.GetWizard().PressButton(buttons);
    }

    [Category("Wizard")]
    public event CancelEventHandler SetActive;

    public virtual void OnSetActive(CancelEventArgs e)
    {
      if (this.SetActive == null)
        return;
      this.SetActive((object) this, e);
    }

    [Category("Wizard")]
    public event WizardPageEventHandler WizardNext;

    public virtual void OnWizardNext(CopernicusWizardPageEventArgs e)
    {
      if (this.WizardNext == null)
        return;
      this.WizardNext((object) this, e);
    }

    [Category("Wizard")]
    public event WizardPageEventHandler WizardBack;

    public virtual void OnWizardBack(CopernicusWizardPageEventArgs e)
    {
      if (this.WizardBack == null)
        return;
      this.WizardBack((object) this, e);
    }

    [Category("Wizard")]
    public event CancelEventHandler WizardFinish;

    public virtual void OnWizardFinish(CancelEventArgs e)
    {
      if (this.WizardFinish == null)
        return;
      this.WizardFinish((object) this, e);
    }

    [Category("Wizard")]
    public event CancelEventHandler QueryCancel;

    public virtual void OnQueryCancel(CancelEventArgs e)
    {
      if (this.QueryCancel == null)
        return;
      this.QueryCancel((object) this, e);
    }
  }
}
