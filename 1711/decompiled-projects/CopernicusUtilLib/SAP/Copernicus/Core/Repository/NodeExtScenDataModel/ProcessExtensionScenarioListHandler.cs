﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeExtScenDataModel.ProcessExtensionScenarioListHandler
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Repository.NodeExtScenDataList;
using System;
using System.IO;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.NodeExtScenDataModel
{
  public class ProcessExtensionScenarioListHandler
  {
    public NodeExtensionScenarioList loadModelByString(string content)
    {
      try
      {
        using (StringReader stringReader = new StringReader(content))
          return (NodeExtensionScenarioList) new XmlSerializer(typeof (NodeExtensionScenarioList)).Deserialize((TextReader) stringReader);
      }
      catch (Exception ex)
      {
        Console.Out.WriteLine("[ERROR] An exception occurred when trying to deserialize string " + content);
        return (NodeExtensionScenarioList) null;
      }
    }
  }
}
