﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.EntityUsageIndex.EntityUsageType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository.EntityUsageIndex
{
  public enum EntityUsageType
  {
    SOURCE_USES_SAP_GDT = 10,
    SOURCE_USES_SAP_BO = 20,
    SOURCE_USES_SAP_BO_NODE = 21,
    SOURCE_USES_SAP_BO_NODE_ELEMENT = 22,
    SOURCE_USES_SAP_BO_NODE_ACTION = 23,
    SOURCE_USES_SAP_BO_NODE_QUERY = 24,
    SOURCE_USES_SAP_BO_ASSOCIATION = 25,
    XBO_USES_SAP_BO = 140,
    XBO_USES_SAP_BO_NODE = 141,
    XBO_USES_SAP_GDT = 142,
    SOURCE_USES_PROCESS_COMPONENT = 152,
  }
}
