﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ExtScenDataModel.ProcessExtensionScenarioType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.ExtScenDataModel
{
  [DesignerCategory("code")]
  [XmlRoot("ProcessExtensionScenario", IsNullable = false, Namespace = "http://sap.com/ByD/PDI/ProcessExtensionScenarioDefinition")]
  [DebuggerStepThrough]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/ProcessExtensionScenarioDefinition")]
  [Serializable]
  public class ProcessExtensionScenarioType
  {
    private string nameField;
    private string boNameSpaceField;
    private string boNameField;
    private string boNodeNameField;
    private ExtensionScenarioType[] extensionScenarioListField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Name
    {
      get
      {
        return this.nameField;
      }
      set
      {
        this.nameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string BoNameSpace
    {
      get
      {
        return this.boNameSpaceField;
      }
      set
      {
        this.boNameSpaceField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string BoName
    {
      get
      {
        return this.boNameField;
      }
      set
      {
        this.boNameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string BoNodeName
    {
      get
      {
        return this.boNodeNameField;
      }
      set
      {
        this.boNodeNameField = value;
      }
    }

    [XmlArrayItem("ExtensionScenario", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
    [XmlArray(Form = XmlSchemaForm.Unqualified)]
    public ExtensionScenarioType[] ExtensionScenarioList
    {
      get
      {
        return this.extensionScenarioListField;
      }
      set
      {
        this.extensionScenarioListField = value;
      }
    }
  }
}
