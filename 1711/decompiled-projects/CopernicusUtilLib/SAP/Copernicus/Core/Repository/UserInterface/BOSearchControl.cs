﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.UserInterface.BOSearchControl
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Repository.DataModel;
using SAP.Copernicus.Core.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Repository.UserInterface
{
  public class BOSearchControl : UserControl
  {
    private Label labelResult;
    private DataGridView dataGridView1;
    private TextBox textBoxBOPrxName;
    private Label labelBOPrxName;
    private Label labelNamespace;
    private ComboBox comboBoxNamespace;
    private Label labelTechCat;
    private ComboBox comboBoxTechCat;
    private Label labelCategory;
    private ComboBox comboBoxCategory;
    private Label labelBOName;
    private TextBox textBoxBOName;
    private Button button1;
    private bool useWriteAccessIndicator;
    private RepositoryDataSet.BusinessObjectsRow[] boRows;
    private FlowLayoutPanel flowLayoutPanel1;
    public List<RepositoryDataSet.BusinessObjectsRow> selectedBORows;
    private Label labelDU;
    private ComboBox comboBoxDU;
    private Label labelWriteAccess;
    private CheckBox checkBoxWriteAccess;
    private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
    private DataGridViewTextBoxColumn BOName;
    private DataGridViewTextBoxColumn Namespace;
    private DataGridViewTextBoxColumn Category;
    private DataGridViewTextBoxColumn TechnicalCategory;
    private DataGridViewTextBoxColumn BOProxyName;
    private DataGridViewTextBoxColumn DeploymentUnitCol;
    private DataGridViewCheckBoxColumn WriteAccess;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    private Label labelSAPOnly;
    private CheckBox checkBoxOnlySAP;
    private Panel panel1;

    public event BOSearchControl.BusinessObjectSelectionChangedEventHandler BusinessObjectSelectionChangedEvent;

    internal void FireBusinessObjectSelectionChangedEvent(List<RepositoryDataSet.BusinessObjectsRow> selectedRows)
    {
      if (this.BusinessObjectSelectionChangedEvent == null)
        return;
      this.BusinessObjectSelectionChangedEvent((object) this, selectedRows);
    }

    public void overrideNameLabel(string name)
    {
      this.labelBOName.Text = name;
    }

    public BOSearchControl()
    {
      this.InitializeComponent();
      this.InitializeNamespacesComboBox();
      this.InitalizeComboBoxes();
    }

    private void InitalizeComboBoxes()
    {
      this.comboBoxCategory.Items.Clear();
      this.comboBoxTechCat.Items.Clear();
      this.comboBoxDU.Items.Clear();
      foreach (BusinessObjectCategory businessObjectCategory in BusinessObjectCategory.getBusinessObjectCategories())
        this.comboBoxCategory.Items.Add((object) businessObjectCategory);
      this.comboBoxCategory.Items.Add((object) new BusinessObjectCategory("", ""));
      foreach (BusinessObjectTechnicalCategory technicalCategory in BusinessObjectTechnicalCategory.getBusinessObjectTechnicalCategories())
        this.comboBoxTechCat.Items.Add((object) technicalCategory);
      this.comboBoxTechCat.Items.Add((object) new BusinessObjectTechnicalCategory("", ""));
      foreach (DeploymentUnit deploymentUnit in DeploymentUnit.Values)
        this.comboBoxDU.Items.Add((object) deploymentUnit.GetDeploymentUnitName());
      this.comboBoxDU.Items.Add((object) "");
    }

    private void InitializeNamespacesComboBox()
    {
      List<string> namespaces = RepositoryDataCache.GetInstance().GetNamespaces();
      this.comboBoxNamespace.Items.Clear();
      this.comboBoxNamespace.Items.AddRange((object[]) namespaces.ToArray<string>());
    }

    private void InitializeComponent()
    {
      this.button1 = new Button();
      this.labelResult = new Label();
      this.dataGridView1 = new DataGridView();
      this.BOName = new DataGridViewTextBoxColumn();
      this.Namespace = new DataGridViewTextBoxColumn();
      this.Category = new DataGridViewTextBoxColumn();
      this.TechnicalCategory = new DataGridViewTextBoxColumn();
      this.BOProxyName = new DataGridViewTextBoxColumn();
      this.DeploymentUnitCol = new DataGridViewTextBoxColumn();
      this.WriteAccess = new DataGridViewCheckBoxColumn();
      this.textBoxBOPrxName = new TextBox();
      this.labelBOPrxName = new Label();
      this.labelNamespace = new Label();
      this.comboBoxNamespace = new ComboBox();
      this.labelTechCat = new Label();
      this.comboBoxTechCat = new ComboBox();
      this.labelCategory = new Label();
      this.comboBoxCategory = new ComboBox();
      this.labelBOName = new Label();
      this.textBoxBOName = new TextBox();
      this.flowLayoutPanel1 = new FlowLayoutPanel();
      this.labelSAPOnly = new Label();
      this.checkBoxOnlySAP = new CheckBox();
      this.labelDU = new Label();
      this.comboBoxDU = new ComboBox();
      this.labelWriteAccess = new Label();
      this.checkBoxWriteAccess = new CheckBox();
      this.panel1 = new Panel();
      this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn3 = new DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn4 = new DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn5 = new DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn6 = new DataGridViewTextBoxColumn();
      this.dataGridViewCheckBoxColumn1 = new DataGridViewCheckBoxColumn();
      ((ISupportInitialize) this.dataGridView1).BeginInit();
      this.flowLayoutPanel1.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      this.button1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.button1.Location = new Point(465, 3);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 21);
      this.button1.TabIndex = 0;
      this.button1.Text = "Search";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.labelResult.AutoSize = true;
      this.labelResult.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.labelResult.Location = new Point(6, 9);
      this.labelResult.Name = "labelResult";
      this.labelResult.Size = new Size(108, 13);
      this.labelResult.TabIndex = 1;
      this.labelResult.Text = "Business Objects:";
      this.dataGridView1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
      this.dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange((DataGridViewColumn) this.BOName, (DataGridViewColumn) this.Namespace, (DataGridViewColumn) this.Category, (DataGridViewColumn) this.TechnicalCategory, (DataGridViewColumn) this.BOProxyName, (DataGridViewColumn) this.DeploymentUnitCol, (DataGridViewColumn) this.WriteAccess);
      this.dataGridView1.Location = new Point(9, 30);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.ReadOnly = true;
      this.dataGridView1.Size = new Size(525, 191);
      this.dataGridView1.TabIndex = 2;
      this.dataGridView1.SelectionChanged += new EventHandler(this.dataGridView1_SelectionChanged);
      this.BOName.HeaderText = "Name";
      this.BOName.Name = "BOName";
      this.BOName.ReadOnly = true;
      this.Namespace.HeaderText = "Namespace";
      this.Namespace.Name = "Namespace";
      this.Namespace.ReadOnly = true;
      this.Category.HeaderText = "Category";
      this.Category.Name = "Category";
      this.Category.ReadOnly = true;
      this.TechnicalCategory.HeaderText = "Technical Category";
      this.TechnicalCategory.Name = "TechnicalCategory";
      this.TechnicalCategory.ReadOnly = true;
      this.BOProxyName.HeaderText = "Proxy Name";
      this.BOProxyName.Name = "BOProxyName";
      this.BOProxyName.ReadOnly = true;
      this.DeploymentUnitCol.HeaderText = "Deployment Unit";
      this.DeploymentUnitCol.Name = "DeploymentUnitCol";
      this.DeploymentUnitCol.ReadOnly = true;
      this.WriteAccess.HeaderText = "Write Access";
      this.WriteAccess.Name = "WriteAccess";
      this.WriteAccess.ReadOnly = true;
      this.flowLayoutPanel1.SetFlowBreak((Control) this.textBoxBOPrxName, true);
      this.textBoxBOPrxName.Location = new Point(165, 57);
      this.textBoxBOPrxName.Name = "textBoxBOPrxName";
      this.textBoxBOPrxName.Size = new Size(307, 20);
      this.textBoxBOPrxName.TabIndex = 3;
      this.textBoxBOPrxName.KeyPress += new KeyPressEventHandler(this.onKeyPress);
      this.labelBOPrxName.Location = new Point(6, 60);
      this.labelBOPrxName.Margin = new Padding(6);
      this.labelBOPrxName.Name = "labelBOPrxName";
      this.labelBOPrxName.Size = new Size(150, 13);
      this.labelBOPrxName.TabIndex = 4;
      this.labelBOPrxName.Text = "Business Object Proxy Name:";
      this.labelNamespace.Location = new Point(6, 113);
      this.labelNamespace.Margin = new Padding(6);
      this.labelNamespace.Name = "labelNamespace";
      this.labelNamespace.Size = new Size(150, 13);
      this.labelNamespace.TabIndex = 4;
      this.labelNamespace.Text = "Namespace:";
      this.flowLayoutPanel1.SetFlowBreak((Control) this.comboBoxNamespace, true);
      this.comboBoxNamespace.Location = new Point(165, 110);
      this.comboBoxNamespace.Name = "comboBoxNamespace";
      this.comboBoxNamespace.Size = new Size(307, 21);
      this.comboBoxNamespace.TabIndex = 3;
      this.comboBoxNamespace.KeyPress += new KeyPressEventHandler(this.onKeyPress);
      this.labelTechCat.Location = new Point(6, 33);
      this.labelTechCat.Margin = new Padding(6);
      this.labelTechCat.Name = "labelTechCat";
      this.labelTechCat.Size = new Size(150, 13);
      this.labelTechCat.TabIndex = 4;
      this.labelTechCat.Text = "Tech. Category:";
      this.comboBoxTechCat.DropDownStyle = ComboBoxStyle.DropDownList;
      this.flowLayoutPanel1.SetFlowBreak((Control) this.comboBoxTechCat, true);
      this.comboBoxTechCat.Location = new Point(165, 30);
      this.comboBoxTechCat.Name = "comboBoxTechCat";
      this.comboBoxTechCat.Size = new Size(307, 21);
      this.comboBoxTechCat.TabIndex = 3;
      this.labelCategory.Location = new Point(6, 6);
      this.labelCategory.Margin = new Padding(6);
      this.labelCategory.Name = "labelCategory";
      this.labelCategory.Size = new Size(150, 13);
      this.labelCategory.TabIndex = 4;
      this.labelCategory.Text = "Category:";
      this.comboBoxCategory.DropDownStyle = ComboBoxStyle.DropDownList;
      this.flowLayoutPanel1.SetFlowBreak((Control) this.comboBoxCategory, true);
      this.comboBoxCategory.Location = new Point(165, 3);
      this.comboBoxCategory.Name = "comboBoxCategory";
      this.comboBoxCategory.Size = new Size(307, 21);
      this.comboBoxCategory.TabIndex = 3;
      this.labelBOName.Location = new Point(6, 165);
      this.labelBOName.Margin = new Padding(6);
      this.labelBOName.Name = "labelBOName";
      this.labelBOName.Size = new Size(150, 13);
      this.labelBOName.TabIndex = 4;
      this.labelBOName.Text = "Name:";
      this.flowLayoutPanel1.SetFlowBreak((Control) this.textBoxBOName, true);
      this.textBoxBOName.Location = new Point(165, 162);
      this.textBoxBOName.Name = "textBoxBOName";
      this.textBoxBOName.Size = new Size(307, 20);
      this.textBoxBOName.TabIndex = 3;
      this.textBoxBOName.KeyPress += new KeyPressEventHandler(this.onKeyPress);
      this.flowLayoutPanel1.AutoSize = true;
      this.flowLayoutPanel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.flowLayoutPanel1.Controls.Add((Control) this.labelCategory);
      this.flowLayoutPanel1.Controls.Add((Control) this.comboBoxCategory);
      this.flowLayoutPanel1.Controls.Add((Control) this.labelTechCat);
      this.flowLayoutPanel1.Controls.Add((Control) this.comboBoxTechCat);
      this.flowLayoutPanel1.Controls.Add((Control) this.labelBOPrxName);
      this.flowLayoutPanel1.Controls.Add((Control) this.textBoxBOPrxName);
      this.flowLayoutPanel1.Controls.Add((Control) this.labelDU);
      this.flowLayoutPanel1.Controls.Add((Control) this.comboBoxDU);
      this.flowLayoutPanel1.Controls.Add((Control) this.labelNamespace);
      this.flowLayoutPanel1.Controls.Add((Control) this.comboBoxNamespace);
      this.flowLayoutPanel1.Controls.Add((Control) this.labelSAPOnly);
      this.flowLayoutPanel1.Controls.Add((Control) this.checkBoxOnlySAP);
      this.flowLayoutPanel1.Controls.Add((Control) this.labelBOName);
      this.flowLayoutPanel1.Controls.Add((Control) this.textBoxBOName);
      this.flowLayoutPanel1.Controls.Add((Control) this.labelWriteAccess);
      this.flowLayoutPanel1.Controls.Add((Control) this.checkBoxWriteAccess);
      this.flowLayoutPanel1.Location = new Point(3, 3);
      this.flowLayoutPanel1.Name = "flowLayoutPanel1";
      this.flowLayoutPanel1.Size = new Size(475, 210);
      this.flowLayoutPanel1.TabIndex = 5;
      this.labelSAPOnly.Location = new Point(6, 140);
      this.labelSAPOnly.Margin = new Padding(6);
      this.labelSAPOnly.Name = "labelSAPOnly";
      this.labelSAPOnly.Size = new Size(150, 13);
      this.labelSAPOnly.TabIndex = 4;
      this.labelSAPOnly.Text = "Only SAP Namespaces:";
      this.checkBoxOnlySAP.AutoSize = true;
      this.flowLayoutPanel1.SetFlowBreak((Control) this.checkBoxOnlySAP, true);
      this.checkBoxOnlySAP.Location = new Point(165, 137);
      this.checkBoxOnlySAP.Name = "checkBoxOnlySAP";
      this.checkBoxOnlySAP.Size = new Size(15, 14);
      this.checkBoxOnlySAP.TabIndex = 5;
      this.checkBoxOnlySAP.UseVisualStyleBackColor = true;
      this.labelDU.Location = new Point(6, 86);
      this.labelDU.Margin = new Padding(6);
      this.labelDU.Name = "labelDU";
      this.labelDU.Size = new Size(150, 13);
      this.labelDU.TabIndex = 4;
      this.labelDU.Text = "Deployment Unit:";
      this.comboBoxDU.DropDownStyle = ComboBoxStyle.DropDownList;
      this.flowLayoutPanel1.SetFlowBreak((Control) this.comboBoxDU, true);
      this.comboBoxDU.Location = new Point(165, 83);
      this.comboBoxDU.Name = "comboBoxDU";
      this.comboBoxDU.Size = new Size(307, 21);
      this.comboBoxDU.TabIndex = 3;
      this.labelWriteAccess.Location = new Point(6, 191);
      this.labelWriteAccess.Margin = new Padding(6);
      this.labelWriteAccess.Name = "labelWriteAccess";
      this.labelWriteAccess.Size = new Size(150, 13);
      this.labelWriteAccess.TabIndex = 4;
      this.labelWriteAccess.Text = "Write Access:";
      this.checkBoxWriteAccess.AutoSize = true;
      this.checkBoxWriteAccess.Location = new Point(165, 188);
      this.checkBoxWriteAccess.Name = "checkBoxWriteAccess";
      this.checkBoxWriteAccess.Size = new Size(15, 14);
      this.checkBoxWriteAccess.TabIndex = 5;
      this.checkBoxWriteAccess.UseVisualStyleBackColor = true;
      this.panel1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.panel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.panel1.Controls.Add((Control) this.labelResult);
      this.panel1.Controls.Add((Control) this.button1);
      this.panel1.Controls.Add((Control) this.dataGridView1);
      this.panel1.Location = new Point(3, 220);
      this.panel1.Name = "panel1";
      this.panel1.Size = new Size(543, 224);
      this.panel1.TabIndex = 6;
      this.dataGridViewTextBoxColumn1.HeaderText = "Name";
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.ReadOnly = true;
      this.dataGridViewTextBoxColumn1.Width = 69;
      this.dataGridViewTextBoxColumn2.HeaderText = "Namespace";
      this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
      this.dataGridViewTextBoxColumn2.ReadOnly = true;
      this.dataGridViewTextBoxColumn2.Width = 69;
      this.dataGridViewTextBoxColumn3.HeaderText = "Category";
      this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
      this.dataGridViewTextBoxColumn3.ReadOnly = true;
      this.dataGridViewTextBoxColumn3.Width = 69;
      this.dataGridViewTextBoxColumn4.HeaderText = "Technical Category";
      this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
      this.dataGridViewTextBoxColumn4.ReadOnly = true;
      this.dataGridViewTextBoxColumn4.Width = 68;
      this.dataGridViewTextBoxColumn5.HeaderText = "Proxy Name";
      this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
      this.dataGridViewTextBoxColumn5.ReadOnly = true;
      this.dataGridViewTextBoxColumn5.Width = 69;
      this.dataGridViewTextBoxColumn6.HeaderText = "Deployment Unit";
      this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
      this.dataGridViewTextBoxColumn6.ReadOnly = true;
      this.dataGridViewTextBoxColumn6.Width = 69;
      this.dataGridViewCheckBoxColumn1.HeaderText = "Write Access";
      this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
      this.dataGridViewCheckBoxColumn1.ReadOnly = true;
      this.dataGridViewCheckBoxColumn1.Width = 69;
      this.Controls.Add((Control) this.panel1);
      this.Controls.Add((Control) this.flowLayoutPanel1);
      this.Name = nameof (BOSearchControl);
      this.Size = new Size(549, 447);
      ((ISupportInitialize) this.dataGridView1).EndInit();
      this.flowLayoutPanel1.ResumeLayout(false);
      this.flowLayoutPanel1.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public void useOnlySAPNameSpaces(bool show, bool changeable, bool preset)
    {
      this.labelSAPOnly.Visible = show;
      this.checkBoxOnlySAP.Visible = show;
      this.checkBoxOnlySAP.Enabled = changeable;
      this.checkBoxOnlySAP.Checked = preset;
      this.adaptGridPosition();
    }

    public void useNamespace(bool show, bool changeable, string preset)
    {
      this.labelNamespace.Visible = show;
      this.comboBoxNamespace.Visible = show;
      this.comboBoxNamespace.Enabled = changeable;
      this.comboBoxNamespace.Text = preset;
      this.Namespace.Visible = show;
      this.adaptGridPosition();
    }

    public void useBOName(bool show, bool changeable, string preset)
    {
      this.labelBOName.Visible = show;
      this.textBoxBOName.Visible = show;
      this.textBoxBOName.Enabled = changeable;
      this.textBoxBOName.Text = preset;
      this.BOName.Visible = show;
      this.adaptGridPosition();
    }

    public void useBOProxyName(bool show, bool changeable, string preset)
    {
      this.labelBOPrxName.Visible = show;
      this.textBoxBOPrxName.Visible = show;
      this.textBoxBOPrxName.Enabled = changeable;
      this.textBoxBOPrxName.Text = preset;
      this.BOProxyName.Visible = show;
      this.adaptGridPosition();
    }

    public void useCategory(bool show, bool changeable, BusinessObjectCategory preset)
    {
      this.labelCategory.Visible = show;
      this.comboBoxCategory.Visible = show;
      this.comboBoxCategory.Enabled = changeable;
      this.comboBoxCategory.Text = preset.Text;
      this.Category.Visible = show;
      this.adaptGridPosition();
    }

    public void useTechCategory(bool show, bool changeable, BusinessObjectTechnicalCategory preset)
    {
      this.labelTechCat.Visible = show;
      this.comboBoxTechCat.Visible = show;
      this.comboBoxTechCat.Enabled = changeable;
      this.comboBoxTechCat.Text = preset.Text;
      this.TechnicalCategory.Visible = show;
      this.adaptGridPosition();
    }

    public void useDeploymentUnit(bool show, bool changeable, string preset)
    {
      this.labelDU.Visible = show;
      this.comboBoxDU.Visible = show;
      this.comboBoxDU.Enabled = changeable;
      this.comboBoxDU.Text = preset;
      this.DeploymentUnitCol.Visible = show;
      this.adaptGridPosition();
    }

    public void useWriteAccess(bool use, bool show, bool changeable, bool preset)
    {
      this.labelWriteAccess.Visible = show;
      this.checkBoxWriteAccess.Visible = show;
      this.checkBoxWriteAccess.Enabled = changeable;
      this.checkBoxWriteAccess.Checked = preset;
      this.WriteAccess.Visible = show;
      this.useWriteAccessIndicator = use;
      this.adaptGridPosition();
    }

    private void adaptGridPosition()
    {
      this.panel1.Top = this.flowLayoutPanel1.Height + 6;
      this.panel1.Height = this.Height - (this.flowLayoutPanel1.Height + 12);
      this.PerformLayout();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      string category = (string) null;
      string techCategory = (string) null;
      object selectedItem1 = this.comboBoxCategory.SelectedItem;
      if (selectedItem1 != null)
        category = ((BusinessObjectCategory) selectedItem1).Code;
      object selectedItem2 = this.comboBoxTechCat.SelectedItem;
      if (selectedItem2 != null)
        techCategory = ((BusinessObjectTechnicalCategory) selectedItem2).Code;
      string writeaccess = (string) null;
      if (this.useWriteAccessIndicator)
        writeaccess = this.checkBoxWriteAccess.Checked.ToString();
      string deploymentUnit = (string) null;
      if (this.comboBoxDU.SelectedItem != null)
      {
        string MDRSNameOfDU = this.comboBoxDU.SelectedItem.ToString();
        if (!MDRSNameOfDU.Equals(string.Empty))
          deploymentUnit = DeploymentUnit.DUProxyNameForName(MDRSNameOfDU);
      }
      this.boRows = RepositoryDataCache.GetInstance().QueryBOs(category, techCategory, this.comboBoxNamespace.Text, this.textBoxBOPrxName.Text, this.textBoxBOName.Text, deploymentUnit, writeaccess);
      this.dataGridView1.Rows.Clear();
      if (this.boRows == null)
        return;
      if (this.boRows.Length > 0)
      {
        for (int index = 0; index < this.boRows.Length; ++index)
        {
          if (!this.checkBoxOnlySAP.Checked || this.boRows[index].NSName.StartsWith("http://sap.com"))
          {
            this.dataGridView1.Rows.Add();
            this.dataGridView1[0, index].Value = (object) this.boRows[index].Name;
            this.dataGridView1[1, index].Value = (object) this.boRows[index].NSName;
            this.dataGridView1[2, index].Value = (object) this.boRows[index].ObjectCategory;
            this.dataGridView1[3, index].Value = (object) this.boRows[index].TechCategory;
            this.dataGridView1[4, index].Value = (object) this.boRows[index].ProxyName;
            this.dataGridView1[5, index].Value = (object) this.boRows[index].DeploymentUnit;
            this.dataGridView1.Rows[index].Tag = (object) this.boRows[index];
          }
        }
        if (this.boRows.Length == 1)
          this.dataGridView1.Rows[0].Selected = true;
        else
          this.dataGridView1.Sort((DataGridViewColumn) this.BOName, ListSortDirection.Ascending);
      }
      this.labelResult.Text = "Business Objects (" + (object) this.boRows.Length + "):";
    }

    private void dataGridView1_SelectionChanged(object sender, EventArgs e)
    {
      this.selectedBORows = new List<RepositoryDataSet.BusinessObjectsRow>();
      for (int index = 0; index < this.dataGridView1.SelectedRows.Count; ++index)
        this.selectedBORows.Add((RepositoryDataSet.BusinessObjectsRow) this.dataGridView1.SelectedRows[index].Tag);
      this.FireBusinessObjectSelectionChangedEvent(this.selectedBORows);
    }

    private void onKeyPress(object sender, KeyPressEventArgs e)
    {
      if ((int) e.KeyChar != 13)
        return;
      e.Handled = true;
      this.button1_Click((object) null, (EventArgs) null);
    }

    public delegate void BusinessObjectSelectionChangedEventHandler(object sender, List<RepositoryDataSet.BusinessObjectsRow> selectedRows);
  }
}
