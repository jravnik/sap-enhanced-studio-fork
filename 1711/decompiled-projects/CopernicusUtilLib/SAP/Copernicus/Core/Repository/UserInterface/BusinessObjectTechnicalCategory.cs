﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.UserInterface.BusinessObjectTechnicalCategory
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.Core.Repository.UserInterface
{
  public struct BusinessObjectTechnicalCategory
  {
    public static BusinessObjectTechnicalCategory StandardBusinessObject = new BusinessObjectTechnicalCategory("Standard Business Object", "1");
    public static BusinessObjectTechnicalCategory DependentObject = new BusinessObjectTechnicalCategory("Dependent Object", "2");
    public static BusinessObjectTechnicalCategory TransformedObject = new BusinessObjectTechnicalCategory("Transformed Object", "3");
    public static BusinessObjectTechnicalCategory AbstractTemplate = new BusinessObjectTechnicalCategory("Abstract Template", "4");
    public static BusinessObjectTechnicalCategory MaxBusinessObject = new BusinessObjectTechnicalCategory("Max Business Object", "5");
    public static BusinessObjectTechnicalCategory ComponentTemplate = new BusinessObjectTechnicalCategory("Component Template", "6");
    public static BusinessObjectTechnicalCategory ControllerObject = new BusinessObjectTechnicalCategory("Controller Object", "7");
    public static BusinessObjectTechnicalCategory PreDeliverableObject = new BusinessObjectTechnicalCategory("Pre Deliverable Object", "8");
    public static BusinessObjectTechnicalCategory NotImplementedObject = new BusinessObjectTechnicalCategory("Not Implemented Object", "9");
    private string text;
    private string code;
    private static BusinessObjectTechnicalCategory[] businessObjectTechCategories;

    public string Text
    {
      get
      {
        return this.text;
      }
    }

    public string Code
    {
      get
      {
        return this.code;
      }
    }

    internal BusinessObjectTechnicalCategory(string text, string code)
    {
      this.text = text;
      this.code = code;
    }

    public override string ToString()
    {
      return this.Text;
    }

    public static BusinessObjectTechnicalCategory[] getBusinessObjectTechnicalCategories()
    {
      if (BusinessObjectTechnicalCategory.businessObjectTechCategories == null)
        BusinessObjectTechnicalCategory.businessObjectTechCategories = new List<BusinessObjectTechnicalCategory>()
        {
          BusinessObjectTechnicalCategory.StandardBusinessObject,
          BusinessObjectTechnicalCategory.DependentObject,
          BusinessObjectTechnicalCategory.TransformedObject,
          BusinessObjectTechnicalCategory.MaxBusinessObject,
          BusinessObjectTechnicalCategory.ComponentTemplate,
          BusinessObjectTechnicalCategory.PreDeliverableObject,
          BusinessObjectTechnicalCategory.NotImplementedObject
        }.ToArray<BusinessObjectTechnicalCategory>();
      return BusinessObjectTechnicalCategory.businessObjectTechCategories;
    }
  }
}
