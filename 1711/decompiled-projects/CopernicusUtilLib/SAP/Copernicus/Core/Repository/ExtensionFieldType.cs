﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ExtensionFieldType
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class ExtensionFieldType
  {
    private string name;
    private string extensibilityName;
    private string esrNameSpace;
    private string esrName;
    private string proxyName;

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public string ExtensibilityName
    {
      get
      {
        return this.extensibilityName;
      }
      set
      {
        this.extensibilityName = value;
      }
    }

    public string EsrNameSpace
    {
      get
      {
        return this.esrNameSpace;
      }
      set
      {
        this.esrNameSpace = value;
      }
    }

    public string EsrName
    {
      get
      {
        return this.esrName;
      }
      set
      {
        this.esrName = value;
      }
    }

    public string ProxyName
    {
      get
      {
        return this.proxyName;
      }
      set
      {
        this.proxyName = value;
      }
    }
  }
}
