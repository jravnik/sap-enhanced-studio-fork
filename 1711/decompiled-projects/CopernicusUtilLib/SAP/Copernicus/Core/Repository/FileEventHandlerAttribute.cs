﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.FileEventHandlerAttribute
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel.Composition;

namespace SAP.Copernicus.Core.Repository
{
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
  [MetadataAttribute]
  public class FileEventHandlerAttribute : ExportAttribute
  {
    public FileEventHandlerAttribute(FileEventType eventTypeMask, int priority = 100)
      : base(typeof (GenericFileEventHandler))
    {
      this.EventTypeMask = eventTypeMask;
      this.Priority = priority;
    }

    public FileEventType EventTypeMask { get; set; }

    public int Priority { get; set; }
  }
}
