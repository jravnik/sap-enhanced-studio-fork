﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeExtScenDataList.NodeExtensionScenarioList
// Assembly: CopernicusUtilLib, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: 92BF42AB-6B78-414A-95FD-864EE6F18975
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.NodeExtScenDataList
{
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/ByD/PDI/NodeExtensionScenarioList")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlRoot(IsNullable = false, Namespace = "http://sap.com/ByD/PDI/NodeExtensionScenarioList")]
  [Serializable]
  public class NodeExtensionScenarioList
  {
    private ExtensionScenarioType[] extensionScenarioField;

    [XmlElement("ExtensionScenario", Form = XmlSchemaForm.Unqualified)]
    public ExtensionScenarioType[] ExtensionScenario
    {
      get
      {
        return this.extensionScenarioField;
      }
      set
      {
        this.extensionScenarioField = value;
      }
    }
  }
}
