﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.Properties
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  public class Properties
  {
    private static readonly string keyValueSeparators = "=: \t\r\n\f";
    private static readonly string strictKeyValueSeparators = "=:";
    private static readonly string whiteSpaceChars = " \t\r\n\f";
    private static readonly string specialSaveChars = "=: \t\r\n\f#!";
    private Dictionary<string, string> values = new Dictionary<string, string>();
    private Encoding resourceEncoding = Encoding.GetEncoding("UTF-8");
    private char[] hexDigit = new char[16]
    {
      '0',
      '1',
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
      'A',
      'B',
      'C',
      'D',
      'E',
      'F'
    };

    private char toHex(int nibble)
    {
      return this.hexDigit[nibble & 15];
    }

    public string Get(string key)
    {
      if (this.values.ContainsKey(key))
        return this.values[key];
      return (string) null;
    }

    public string Get(string key, string defaultValue)
    {
      return this.Get(key) ?? defaultValue;
    }

    public int Get(string key, int defaultValue)
    {
      string s = this.Get(key);
      int num = defaultValue;
      if (s != null)
      {
        try
        {
          num = int.Parse(s);
        }
        catch (Exception ex)
        {
        }
      }
      return num;
    }

    public void Set(string key, string value)
    {
      if (this.values.ContainsKey(key))
        this.values.Remove(key);
      if (value == null)
        return;
      this.values[key] = value;
    }

    public void Remove(string key)
    {
      this.Set(key, (string) null);
    }

    public bool Load(string filePathName)
    {
      if (!File.Exists(filePathName))
        return false;
      return this.Load(File.OpenText(filePathName));
    }

    public bool Load(StreamReader inputStream)
    {
      return this.ParseClientSideProperties(inputStream);
    }

    private bool ParseClientSideProperties(StreamReader inBR)
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      while (true)
      {
        string line;
        int length;
        int startIndex1;
        do
        {
          do
          {
            line = inBR.ReadLine();
            if (line == null)
              goto label_27;
          }
          while (line.Length <= 0);
          length = line.Length;
          startIndex1 = 0;
          while (startIndex1 < length && Properties.whiteSpaceChars.IndexOf(line[startIndex1]) != -1)
            ++startIndex1;
        }
        while (startIndex1 == length);
        switch (line[startIndex1])
        {
          case '#':
          case '!':
            continue;
          default:
            while (this.continueLine(line))
            {
              string str1 = inBR.ReadLine() ?? "";
              string str2 = line.Substring(0, length - 1);
              int startIndex2 = 0;
              while (startIndex2 < str1.Length && Properties.whiteSpaceChars.IndexOf(str1[startIndex2]) != -1)
                ++startIndex2;
              string str3 = str1.Substring(startIndex2, str1.Length - startIndex2);
              line = str2 + str3;
              length = line.Length;
            }
            int index;
            for (index = startIndex1; index < length; ++index)
            {
              char ch = line[index];
              if ((int) ch == 92)
                ++index;
              else if (Properties.keyValueSeparators.IndexOf(ch) != -1)
                break;
            }
            int startIndex3 = index;
            while (startIndex3 < length && Properties.whiteSpaceChars.IndexOf(line[startIndex3]) != -1)
              ++startIndex3;
            if (startIndex3 < length && Properties.strictKeyValueSeparators.IndexOf(line[startIndex3]) != -1)
              ++startIndex3;
            while (startIndex3 < length && Properties.whiteSpaceChars.IndexOf(line[startIndex3]) != -1)
              ++startIndex3;
            string theString1 = line.Substring(startIndex1, index - startIndex1);
            string theString2 = index < length ? line.Substring(startIndex3, length - startIndex3) : "";
            string key = this.loadConvert(theString1);
            string str = this.loadConvert(theString2);
            dictionary.Add(key, str);
            continue;
        }
      }
label_27:
      this.values = dictionary;
      inBR.Close();
      inBR.Dispose();
      return true;
    }

    private bool continueLine(string line)
    {
      int num1 = 0;
      int num2 = line.Length - 1;
      while (num2 >= 0 && (int) line[num2--] == 92)
        ++num1;
      return num1 % 2 == 1;
    }

    private string saveConvert(string theString, bool escapeSpace)
    {
      int length = theString.Length;
      StringBuilder stringBuilder = new StringBuilder(length * 2);
      for (int index = 0; index < length; ++index)
      {
        char ch = theString[index];
        switch (ch)
        {
          case '\t':
            stringBuilder.Append('\\');
            stringBuilder.Append('t');
            break;
          case '\n':
            stringBuilder.Append('\\');
            stringBuilder.Append('n');
            break;
          case '\f':
            stringBuilder.Append('\\');
            stringBuilder.Append('f');
            break;
          case '\r':
            stringBuilder.Append('\\');
            stringBuilder.Append('r');
            break;
          case ' ':
            if (index == 0 || escapeSpace)
              stringBuilder.Append('\\');
            stringBuilder.Append(' ');
            break;
          case '\\':
            stringBuilder.Append('\\');
            stringBuilder.Append('\\');
            break;
          default:
            if ((int) ch < 32 || (int) ch > 126)
            {
              stringBuilder.Append('\\');
              stringBuilder.Append('u');
              stringBuilder.Append(this.toHex((int) ch >> 12 & 15));
              stringBuilder.Append(this.toHex((int) ch >> 8 & 15));
              stringBuilder.Append(this.toHex((int) ch >> 4 & 15));
              stringBuilder.Append(this.toHex((int) ch & 15));
              break;
            }
            if (Properties.specialSaveChars.IndexOf(ch) != -1)
              stringBuilder.Append('\\');
            stringBuilder.Append(ch);
            break;
        }
      }
      return stringBuilder.ToString();
    }

    private string loadConvert(string theString)
    {
      int length = theString.Length;
      StringBuilder stringBuilder = new StringBuilder(length);
      int num1 = 0;
      while (num1 < length)
      {
        char ch1 = theString[num1++];
        if ((int) ch1 == 92)
        {
          char ch2 = theString[num1++];
          switch (ch2)
          {
            case 'u':
              int num2 = 0;
              for (int index = 0; index < 4; ++index)
              {
                char ch3 = theString[num1++];
                switch (ch3)
                {
                  case '0':
                  case '1':
                  case '2':
                  case '3':
                  case '4':
                  case '5':
                  case '6':
                  case '7':
                  case '8':
                  case '9':
                    num2 = (num2 << 4) + (int) ch3 - 48;
                    break;
                  case 'A':
                  case 'B':
                  case 'C':
                  case 'D':
                  case 'E':
                  case 'F':
                    num2 = (num2 << 4) + 10 + (int) ch3 - 65;
                    break;
                  case 'a':
                  case 'b':
                  case 'c':
                  case 'd':
                  case 'e':
                  case 'f':
                    num2 = (num2 << 4) + 10 + (int) ch3 - 97;
                    break;
                  default:
                    throw new Exception("Malformed \\uxxxx encoding.");
                }
              }
              stringBuilder.Append((char) num2);
              continue;
            case 't':
              ch2 = '\t';
              break;
            case 'r':
              ch2 = '\r';
              break;
            case 'n':
              ch2 = '\n';
              break;
            case 'f':
              ch2 = '\f';
              break;
          }
          stringBuilder.Append(ch2);
        }
        else
          stringBuilder.Append(ch1);
      }
      return stringBuilder.ToString();
    }

    public string Save()
    {
      StringWriter stringWriter = new StringWriter();
      Dictionary<string, string>.Enumerator enumerator = this.values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        string key = enumerator.Current.Key;
        string theString = enumerator.Current.Value;
        string str1 = this.saveConvert(key, true);
        string str2 = this.saveConvert(theString, false);
        stringWriter.WriteLine(str1 + "=" + str2);
      }
      stringWriter.Close();
      return stringWriter.GetStringBuilder().ToString();
    }

    public void Save(StreamWriter writer)
    {
      Dictionary<string, string>.Enumerator enumerator = this.values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        string key = enumerator.Current.Key;
        string theString = enumerator.Current.Value;
        string str1 = this.saveConvert(key, true);
        string str2 = this.saveConvert(theString, false);
        writer.WriteLine(str1 + "=" + str2);
      }
      writer.Flush();
    }

    public void Save(string filePathName)
    {
      StreamWriter streamWriter = new StreamWriter((Stream) File.OpenWrite(filePathName), this.resourceEncoding);
      Dictionary<string, string>.Enumerator enumerator = this.values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        string key = enumerator.Current.Key;
        string theString = enumerator.Current.Value;
        string str1 = this.saveConvert(key, true);
        string str2 = this.saveConvert(theString, false);
        streamWriter.WriteLine(str1 + "=" + str2);
      }
      streamWriter.Close();
    }
  }
}
