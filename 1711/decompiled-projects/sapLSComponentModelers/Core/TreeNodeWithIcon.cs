﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.TreeNodeWithIcon
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  public class TreeNodeWithIcon : TreeViewItem
  {
    private StackPanel stack;
    private TextBlock textBlock;
    private System.Windows.Controls.Image icon;
    private TextBox txtBox;

    public event DependencyPropertyChangedEventHandler TextChanged;

    public TreeNodeWithIcon(string headerText, Bitmap iconSource)
    {
      this.stack = new StackPanel();
      this.stack.Orientation = Orientation.Horizontal;
      this.Header = (object) this.stack;
      if (iconSource != null)
      {
        this.icon = new System.Windows.Controls.Image();
        this.icon.VerticalAlignment = VerticalAlignment.Center;
        this.icon.Margin = new Thickness(0.0, 0.0, 4.0, 0.0);
        this.icon.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(iconSource);
        this.stack.Children.Add((UIElement) this.icon);
      }
      this.textBlock = new TextBlock();
      this.textBlock.VerticalAlignment = VerticalAlignment.Center;
      this.stack.Children.Add((UIElement) this.textBlock);
      this.textBlock.Text = headerText;
      this.textBlock.Visibility = Visibility.Visible;
      this.textBlock.MouseDown += new MouseButtonEventHandler(this.textBlock_MouseDown);
      this.txtBox = new TextBox();
      this.txtBox.VerticalAlignment = VerticalAlignment.Center;
      this.txtBox.Visibility = Visibility.Hidden;
      this.txtBox.LostKeyboardFocus += new KeyboardFocusChangedEventHandler(this.txtBox_LostKeyboardFocus);
      this.txtBox.LostFocus += new RoutedEventHandler(this.txtBox_LostFocus);
      this.txtBox.KeyUp += new KeyEventHandler(this.txtBox_KeyUp);
      this.txtBox.IsKeyboardFocusedChanged += new DependencyPropertyChangedEventHandler(this.txtBox_IsKeyboardFocusedChanged);
    }

    public string HeaderText
    {
      get
      {
        return this.textBlock.Text;
      }
      set
      {
        this.textBlock.Text = value;
        this.txtBox.Clear();
        this.txtBox.Text = value;
      }
    }

    public ImageSource Icon
    {
      get
      {
        return this.icon.Source;
      }
      set
      {
        this.icon.Source = value;
      }
    }

    private void textBlock_MouseDown(object sender, MouseButtonEventArgs e)
    {
      if (e.ClickCount != 2 || this.Tag == null)
        return;
      this.BeginEdit();
    }

    private void txtBox_KeyUp(object sender, KeyEventArgs e)
    {
      if (e.Key != Key.Return)
        return;
      this.EndEdit();
    }

    private void txtBox_LostFocus(object sender, RoutedEventArgs e)
    {
      this.EndEdit();
    }

    public void txtBox_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
    {
      this.EndEdit();
    }

    public void txtBox_IsKeyboardFocusedChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      if (this.TextChanged == null || this.txtBox.IsKeyboardFocused)
        return;
      this.TextChanged(sender, e);
    }

    public void BeginEdit()
    {
      this.txtBox.Clear();
      this.stack.Children.Remove((UIElement) this.textBlock);
      this.txtBox.MinWidth = 50.0;
      this.txtBox.IsHitTestVisible = true;
      if (this.stack.Children.Contains((UIElement) this.txtBox))
        return;
      this.stack.Children.Add((UIElement) this.txtBox);
      this.txtBox.SelectedText = this.textBlock.Text;
      this.txtBox.Visibility = Visibility.Visible;
      this.txtBox.Focus();
      this.txtBox.SelectAll();
      this.textBlock.Visibility = Visibility.Hidden;
      this.textBlock.Visibility = Visibility.Collapsed;
    }

    public void EndEdit()
    {
      this.stack.Children.Remove((UIElement) this.txtBox);
      if (!string.IsNullOrEmpty(this.txtBox.Text))
        this.textBlock.Text = this.txtBox.Text;
      if (this.stack.Children.Contains((UIElement) this.textBlock))
        return;
      this.stack.Children.Add((UIElement) this.textBlock);
      this.txtBox.Visibility = Visibility.Hidden;
      this.textBlock.Visibility = Visibility.Visible;
    }
  }
}
