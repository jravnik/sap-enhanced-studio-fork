﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.WizardStepConfiguration
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System.Collections.Generic;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  public class WizardStepConfiguration
  {
    private static WizardStepConfiguration m_Instance;

    public static WizardStepConfiguration GetInstance()
    {
      if (WizardStepConfiguration.m_Instance == null)
        WizardStepConfiguration.m_Instance = new WizardStepConfiguration();
      return WizardStepConfiguration.m_Instance;
    }

    private WizardStepConfiguration()
    {
    }

    public List<IWizardStep> CreateWCFWizardSteps()
    {
      List<IWizardStep> wizardStepList = new List<IWizardStep>();
      ViewSelectionStep viewSelectionStep = new ViewSelectionStep();
      wizardStepList.Add((IWizardStep) new WizardStep()
      {
        FinishEnabled = false,
        StepContent = (BaseWizardStepControl) viewSelectionStep,
        StepName = "Views Selection",
        StepExplanation = "Configure Additional WCF Views"
      });
      SummaryStep summaryStep = new SummaryStep();
      wizardStepList.Add((IWizardStep) new WizardStep()
      {
        FinishEnabled = true,
        StepContent = (BaseWizardStepControl) summaryStep,
        StepName = "Summary",
        StepExplanation = "Summary of the Configuration"
      });
      return wizardStepList;
    }
  }
}
