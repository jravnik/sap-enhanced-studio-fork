﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Core.ConfigResourceAccessor
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Windows;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Core
{
  public class ConfigResourceAccessor
  {
    public static readonly Thickness DEFAULT_MARGIN = new Thickness(0.0);
    private static readonly Color RED = Color.FromArgb(byte.MaxValue, byte.MaxValue, (byte) 0, (byte) 0);
    private static ConfigResourceAccessor instance = new ConfigResourceAccessor();
    private Dictionary<string, ImageInfo> imageInfos;
    private Dictionary<string, Color> colors;
    private Dictionary<string, SolidColorBrush> solidBrushes;
    private Dictionary<string, SlicingRenderingInfo> slicingRenderingInfos;
    private Dictionary<string, LinearGradientBrush> lgradientBrushes;
    private Dictionary<string, FontInfo> fontInfos;
    private Dictionary<string, object> resourceInfos;
    private Dictionary<string, Thickness> margins;

    private ConfigResourceAccessor()
    {
      this.imageInfos = new Dictionary<string, ImageInfo>();
      this.colors = new Dictionary<string, Color>();
      this.solidBrushes = new Dictionary<string, SolidColorBrush>();
      this.slicingRenderingInfos = new Dictionary<string, SlicingRenderingInfo>();
      this.lgradientBrushes = new Dictionary<string, LinearGradientBrush>();
      this.fontInfos = new Dictionary<string, FontInfo>();
      this.margins = new Dictionary<string, Thickness>();
    }

    public static ConfigResourceAccessor Instance
    {
      get
      {
        return ConfigResourceAccessor.instance;
      }
    }

    public ImageInfo GetImageInfo(string accessorString)
    {
      if (accessorString == null)
        return (ImageInfo) null;
      if (this.imageInfos.ContainsKey(accessorString))
        return this.imageInfos[accessorString];
      string str = accessorString;
      if (str.StartsWith("image::"))
        str = str.Substring(7);
      string[] strArray = str.Split(',');
      if (strArray.Length != 3)
        return (ImageInfo) null;
      string name = strArray[0];
      int result1 = 0;
      if (!int.TryParse(strArray[1], out result1))
        return (ImageInfo) null;
      int result2 = 0;
      if (!int.TryParse(strArray[2], out result2))
        return (ImageInfo) null;
      ImageInfo imageInfo = new ImageInfo(name, result1, result2);
      this.imageInfos.Add(accessorString, imageInfo);
      return imageInfo;
    }

    public Brush GetBrush(string value)
    {
      if (value == null)
        return (Brush) null;
      if (value.StartsWith("style::"))
        return (Brush) this.GetResource(value);
      if (value.StartsWith("color::"))
        return (Brush) this.GetSolidColorBrush(value);
      if (value.StartsWith("lgradient::"))
        return (Brush) this.GetLinearGradientBrush(value);
      return (Brush) null;
    }

    public Style GetStyle(string value)
    {
      return (Style) this.GetResource(value);
    }

    public object GetResource(string value)
    {
      if (this.resourceInfos == null)
        this.resourceInfos = new Dictionary<string, object>();
      if (this.resourceInfos.ContainsKey(value))
        return this.resourceInfos[value];
      string str = value;
      if (str.StartsWith("style::"))
        str = str.Substring("style::".Length);
      object obj = (object) null;
      string[] strArray = str.Split(';');
      if (strArray.Length == 3)
      {
        if (!(strArray[0] == "sapLSUISkins"))
          throw new NotImplementedException();
        obj = ((FrameworkElement) this.CreateObject(strArray[1])).Resources[(object) strArray[2]];
      }
      if (obj != null)
        this.resourceInfos.Add(value, obj);
      return obj;
    }

    public object CreateObject(string typeName)
    {
      return Assembly.Load("sapLSUISkins").CreateInstance(typeName);
    }

    public LinearGradientBrush GetLinearGradientBrush(string value)
    {
      return this.GetLinearGradientBrush(value, new Point(0.5, 0.0), new Point(0.5, 1.0));
    }

    public LinearGradientBrush GetLinearGradientBrush(string value, Point startPoint, Point endPoint)
    {
      if (this.lgradientBrushes.ContainsKey(value))
        return this.lgradientBrushes[value];
      string str = value;
      if (str.StartsWith("lgradient::"))
        str = str.Substring("lgradient::".Length);
      LinearGradientBrush linearGradientBrush = new LinearGradientBrush();
      linearGradientBrush.EndPoint = endPoint;
      linearGradientBrush.StartPoint = startPoint;
      string[] strArray = str.Split(',');
      int num = 0;
      for (int index = strArray.Length / 2; num < index; ++num)
      {
        Color color = ColorTranslator.FromHtml(strArray[num * 2]);
        double result = 0.0;
        double.TryParse(strArray[num * 2 + 1], NumberStyles.Float, (IFormatProvider) NumberFormatInfo.InvariantInfo, out result);
        linearGradientBrush.GradientStops.Add(new GradientStop()
        {
          Color = color,
          Offset = result
        });
      }
      this.lgradientBrushes.Add(value, linearGradientBrush);
      return linearGradientBrush;
    }

    public Color GetColor(string value)
    {
      if (this.colors.ContainsKey(value))
        return this.colors[value];
      Color red = ConfigResourceAccessor.RED;
      string hexColor = value;
      if (hexColor.StartsWith("color::"))
        hexColor = hexColor.Substring("color::".Length);
      Color color = ColorTranslator.FromHtml(hexColor);
      this.colors.Add(value, color);
      return color;
    }

    public FontInfo GetFontInfos(string accessorString)
    {
      if (accessorString == null)
        return (FontInfo) null;
      if (this.fontInfos.ContainsKey(accessorString))
        return this.fontInfos[accessorString];
      string str = accessorString;
      if (str.StartsWith("font::"))
        str = str.Substring(6);
      string[] strArray = str.Split(',');
      if (strArray.Length != 3)
        return (FontInfo) null;
      FontFamily fontFamily = new FontFamily(strArray[0]);
      int result = 0;
      if (!int.TryParse(strArray[1], out result))
        return (FontInfo) null;
      FontStyle style = FontStyles.Normal;
      FontWeight weight = FontWeights.Normal;
      bool underline = false;
      switch (strArray[2])
      {
        case "Bold":
          weight = FontWeights.Bold;
          break;
        case "BoldItalic":
          weight = FontWeights.Bold;
          style = FontStyles.Italic;
          break;
        case "NormalUnderline":
          underline = true;
          break;
      }
      FontInfo fontInfo = new FontInfo(fontFamily, result, weight, style, underline);
      this.fontInfos.Add(accessorString, fontInfo);
      return fontInfo;
    }

    public Thickness GetMargin(string accessorString)
    {
      if (this.margins.ContainsKey(accessorString))
        return this.margins[accessorString];
      string str = accessorString;
      if (str.StartsWith("margin::"))
        str = str.Substring("margin::".Length);
      string[] strArray = str.Split(',');
      if (strArray.Length != 4)
        return ConfigResourceAccessor.DEFAULT_MARGIN;
      int result1 = 0;
      if (!int.TryParse(strArray[0], out result1))
        return ConfigResourceAccessor.DEFAULT_MARGIN;
      int result2 = 0;
      if (!int.TryParse(strArray[1], out result2))
        return ConfigResourceAccessor.DEFAULT_MARGIN;
      int result3 = 0;
      if (!int.TryParse(strArray[2], out result3))
        return ConfigResourceAccessor.DEFAULT_MARGIN;
      int result4 = 0;
      if (!int.TryParse(strArray[3], out result4))
        return ConfigResourceAccessor.DEFAULT_MARGIN;
      Thickness thickness = new Thickness((double) result1, (double) result2, (double) result3, (double) result4);
      this.margins.Add(accessorString, thickness);
      return thickness;
    }

    public SolidColorBrush GetSolidColorBrush(string value)
    {
      if (value == null)
        return (SolidColorBrush) null;
      if (this.solidBrushes.ContainsKey(value))
        return this.solidBrushes[value];
      SolidColorBrush solidColorBrush = new SolidColorBrush(this.GetColor(value));
      this.solidBrushes.Add(value, solidColorBrush);
      return solidColorBrush;
    }

    public SlicingRenderingInfo GetSlicingRenderingInfo(string value)
    {
      if (this.slicingRenderingInfos.ContainsKey(value))
        return this.slicingRenderingInfos[value];
      SlicingRenderingInfo slicingRenderingInfo = new SlicingRenderingInfo(value);
      this.slicingRenderingInfos.Add(value, slicingRenderingInfo);
      return slicingRenderingInfo;
    }
  }
}
