﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.ComponentModelerUtilities
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers
{
  public class ComponentModelerUtilities : UICoreUtilities
  {
    private static ComponentModelerUtilities instance = new ComponentModelerUtilities();

    private ComponentModelerUtilities()
    {
    }

    public static ComponentModelerUtilities Instance
    {
      get
      {
        return ComponentModelerUtilities.instance;
      }
    }

    public ComponentDesigner GetWPFDesigner(IFloorplan fpToOpen)
    {
      ComponentDesigner componentDesigner = (ComponentDesigner) null;
      if (fpToOpen is QuickActivityFloorplan)
        componentDesigner = (ComponentDesigner) new QAFDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is MobileAppContainer)
        componentDesigner = (ComponentDesigner) new MADesigner((IModelObject) fpToOpen);
      else if (fpToOpen is FactSheetComponent)
        componentDesigner = (ComponentDesigner) new FSDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is WorkCenterView)
        componentDesigner = (ComponentDesigner) new WCViewDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is TileComponent)
        componentDesigner = (ComponentDesigner) new TLDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is ModalDialogFloorplan)
        componentDesigner = (ComponentDesigner) new MDFDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is ObjectInstanceFloorplan)
        componentDesigner = (ComponentDesigner) new OIFDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is EmbeddedComponent)
        componentDesigner = (ComponentDesigner) new ECDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is PortTypePackage)
        componentDesigner = (ComponentDesigner) new PTPDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is WorkCenterFloorplan)
        componentDesigner = (ComponentDesigner) new WCFDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is WorkCenterView)
        componentDesigner = (ComponentDesigner) new WCViewDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is OVSComponent)
        componentDesigner = (ComponentDesigner) new OVSDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is GuidedActivityFloorplan)
        componentDesigner = (ComponentDesigner) new GAFDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is OWLComponent)
        componentDesigner = (ComponentDesigner) new OWLDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is TaskListView)
        componentDesigner = (ComponentDesigner) new TaskListViewDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is AnalyticsFloorplan)
        componentDesigner = (ComponentDesigner) new AnalyticsDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is XMLObject)
        componentDesigner = (ComponentDesigner) new XMLViewDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is UISettingsComponent)
        componentDesigner = (ComponentDesigner) new UISettingsDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is UIDependencyComponent)
        componentDesigner = (ComponentDesigner) new DependencyDesigner((IModelObject) fpToOpen);
      else if (fpToOpen is ThingTypeComponent)
        componentDesigner = (ComponentDesigner) new ThingTypeDesigner(fpToOpen as ThingTypeComponent);
      else if (fpToOpen is DataObjectComponent)
        componentDesigner = (ComponentDesigner) new DataObjectDesigner(fpToOpen as DataObjectComponent);
      else if (fpToOpen is QuickViewComponent)
        componentDesigner = (ComponentDesigner) new QuickViewDesigner(fpToOpen as QuickViewComponent);
      else if (fpToOpen is ThingInspectorComponent)
        componentDesigner = (ComponentDesigner) new ThingInspectorDesigner(fpToOpen as ThingInspectorComponent);
      else if (fpToOpen is QuickCreateComponent)
        componentDesigner = (ComponentDesigner) new QuickCreateDesigner(fpToOpen as QuickCreateComponent);
      else if (fpToOpen is MashupComponent)
        componentDesigner = (ComponentDesigner) new MashupDesigner((IModelObject) (fpToOpen as MashupComponent));
      else if (fpToOpen is MashupCategoryComponent)
        componentDesigner = (ComponentDesigner) new MashupCategoryDesigner((IModelObject) (fpToOpen as MashupCategoryComponent));
      else if (fpToOpen is OfflineComponentModel)
        componentDesigner = (ComponentDesigner) new OfflineComponentModelDesigner(fpToOpen as OfflineComponentModel);
      else if (fpToOpen is PrintTemplateFloorplan)
        componentDesigner = (ComponentDesigner) new PrintTemplateDesigner((IModelObject) fpToOpen);
      if (componentDesigner != null)
      {
        if (UICoreUtilities.Designers.ContainsKey(fpToOpen))
          UICoreUtilities.Designers.Remove(fpToOpen);
        UICoreUtilities.Designers.Add(fpToOpen, (BaseDesigner) componentDesigner);
      }
      return componentDesigner;
    }

    public bool IsParentControl(FrameworkElement designerControl, FrameworkElement parentControl)
    {
      for (; designerControl != null; designerControl = designerControl.Parent as FrameworkElement)
      {
        if (designerControl == parentControl)
          return true;
      }
      return false;
    }

    public BaseVisualControl GetBaseVisualControl(FrameworkElement designerControl)
    {
      for (; designerControl != null; designerControl = designerControl.Parent as FrameworkElement)
      {
        if (designerControl is BaseVisualControl)
          return designerControl as BaseVisualControl;
      }
      return (BaseVisualControl) null;
    }

    public BaseSelectableControl GetBaseSelectableControl(FrameworkElement designerControl)
    {
      for (; designerControl != null; designerControl = designerControl.Parent != null ? designerControl.Parent as FrameworkElement : designerControl.TemplatedParent as FrameworkElement)
      {
        if (designerControl is BaseSelectableControl)
          return designerControl as BaseSelectableControl;
      }
      return (BaseSelectableControl) null;
    }
  }
}
