﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls.BaseBorderControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System.Windows;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls
{
  public abstract class BaseBorderControl : BaseSelectableControl
  {
    public BaseBorderControl(IModelObject model)
      : base(model, false, true)
    {
      this.BorderBrush = (Brush) ComponentModelersConstants.ACTIVE_BORDER_BRUSH;
      this.BorderThickness = ComponentModelersConstants.CONTROL_BORDER_THICKNESS;
      this.BorderCornerRadius = new CornerRadius(5.0);
    }
  }
}
