﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls.ContextPanelMinMaxWidthConverter
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System;
using System.Globalization;
using System.Windows.Data;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls
{
  public class ContextPanelMinMaxWidthConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is double && parameter is double)
      {
        double num1 = System.Convert.ToDouble(value);
        double num2 = System.Convert.ToDouble(parameter);
        if (num1 <= num2)
          return (object) (num2 + 175.0);
        if (num1 > num2)
          return (object) num1;
      }
      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
