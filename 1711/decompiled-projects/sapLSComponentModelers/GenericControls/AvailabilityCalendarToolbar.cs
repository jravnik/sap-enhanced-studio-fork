﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.AvailabilityCalendarToolbar
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class AvailabilityCalendarToolbar : BaseSelectableControl, IContextMenuContainer
  {
    private StackPanel container;
    private Grid containerGrid;
    private CalendarPaginator paginator;
    private VisualToolbar m_VisualToolBar;
    private AvailabilityCalendarPaneToolbar customToolBar;

    public AvailabilityCalendarToolbar(IModelObject toolbar)
      : base(toolbar)
    {
      this.container = new StackPanel();
      this.containerGrid = new Grid();
      GridUtil.AddRowDef(this.containerGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.containerGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.containerGrid, 60.0, GridUnitType.Pixel);
      GridUtil.AddColumnDef(this.containerGrid, 1.0, GridUnitType.Star);
      this.container.Orientation = Orientation.Horizontal;
      this.container.VerticalAlignment = VerticalAlignment.Center;
      this.paginator = new CalendarPaginator();
      GridUtil.PlaceElement(this.containerGrid, (UIElement) this.paginator, 0, 0);
      if (toolbar != null)
        this.AddCustomToolBar(toolbar as AvailabilityCalendarPaneToolbar);
      this.Content = (object) this.containerGrid;
    }

    internal AvailabilityCalendarPaneToolbar CustomToolBar
    {
      get
      {
        return this.customToolBar;
      }
      set
      {
        this.customToolBar = value;
        if (this.customToolBar == null)
          return;
        this.AddCustomToolBar(this.customToolBar);
      }
    }

    public override string SelectionText
    {
      get
      {
        return "ToolBar";
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (this.m_VisualToolBar == null)
        return;
      this.m_VisualToolBar.HandleContextMenuItemClickWrapper(source);
    }

    protected override void AddContextMenuItems()
    {
      if (this.m_VisualToolBar == null)
        return;
      this.m_VisualToolBar.AddContextMenuItems((IContextMenuContainer) this);
    }

    public void AddContextMenuItemsWrapper()
    {
      this.AddContextMenuItems();
    }

    public void HandleContextMenuItemWrapper(MenuItem source)
    {
      this.HandleContextMenuItemClick(source);
    }

    private void AddCustomToolBar(AvailabilityCalendarPaneToolbar toolbar)
    {
      if (this.m_VisualToolBar == null)
      {
        this.m_VisualToolBar = new VisualToolbar((IModelObject) toolbar);
        GridUtil.PlaceElement(this.containerGrid, (UIElement) this.m_VisualToolBar, 0, 2);
      }
      this.m_VisualToolBar.Toolbar = (Toolbar) toolbar;
      this.m_VisualToolBar.ReloadView();
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
