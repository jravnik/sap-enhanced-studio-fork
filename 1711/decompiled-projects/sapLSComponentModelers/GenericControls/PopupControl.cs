﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.PopupControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class PopupControl : Popup, IDisposable
  {
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool GetWindowRect(IntPtr hWnd, out PopupControl.RECT lpRect);

    [DllImport("user32")]
    private static extern int SetWindowPos(IntPtr hWnd, int hwndInsertAfter, int x, int y, int cx, int cy, int wFlags);

    protected override void OnMouseEnter(MouseEventArgs e)
    {
      base.OnMouseEnter(e);
      this.IsOpen = true;
    }

    protected override void OnOpened(EventArgs e)
    {
      base.OnOpened(e);
      IntPtr handle = ((HwndSource) PresentationSource.FromVisual((Visual) this.Child)).Handle;
      PopupControl.RECT lpRect;
      if (!PopupControl.GetWindowRect(handle, out lpRect))
        return;
      PopupControl.SetWindowPos(handle, -2, lpRect.Left, lpRect.Top, (int) this.Width, (int) this.Height, 16);
    }

    public void Dispose()
    {
      this.Child = (UIElement) null;
    }

    public struct RECT
    {
      public int Left;
      public int Top;
      public int Right;
      public int Bottom;
    }
  }
}
