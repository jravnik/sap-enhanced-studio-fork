﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualLabel
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualLabel : UserControl
  {
    private const string RESOURCE_ASTERISK_FONT = "Arial,12,Bold";
    private const string RESOURCE_BOLDTEXT_FONT = "Arial,11,Bold";
    private const string RESOURCE_NORMALTEXT_FONT = "Arial,11,Normal";
    private readonly double ASTERISK_WIDTH;
    private TextBlock textBlock;
    private TextBlock asteriskBlock;
    private Grid contentPanel;
    private bool showAsterisk;
    private bool allowWrap;
    private string text;
    private bool isBold;
    private double desiredWidth;

    public double DesiredWidth
    {
      get
      {
        return this.desiredWidth;
      }
    }

    public string Text
    {
      get
      {
        return this.text;
      }
      set
      {
        this.text = value;
        this.UpdateText();
      }
    }

    public bool ShowAsterisk
    {
      get
      {
        return this.showAsterisk;
      }
      set
      {
        this.showAsterisk = value;
        this.UpdateMandatoryState();
      }
    }

    public VisualLabel(string text, bool showAsterisk, bool allowWrap)
      : this(text, showAsterisk, allowWrap, false, 0.0)
    {
    }

    public VisualLabel(string text, bool showAsterisk, bool allowWrap, bool isBold, double maxLabelLength = 0.0)
    {
      if (maxLabelLength > 0.0)
        this.Width = maxLabelLength;
      this.VerticalAlignment = VerticalAlignment.Center;
      this.VerticalContentAlignment = VerticalAlignment.Center;
      this.isBold = isBold;
      this.allowWrap = allowWrap;
      this.showAsterisk = showAsterisk;
      this.text = text;
      this.InitializeComponent();
      this.UpdateText();
      this.UpdateMandatoryState();
    }

    private void UpdateText()
    {
      if (this.text == null)
        this.text = "";
      string text = !this.text.EndsWith(":") ? this.text + ":" : this.text;
      this.desiredWidth = this.ASTERISK_WIDTH + this.MeasureTextBlockWidth(this.textBlock, text);
      this.textBlock.Text = text;
    }

    private void InitializeComponent()
    {
      this.contentPanel = new Grid();
      this.Content = (object) this.contentPanel;
      GridUtil.AddRowDef(this.contentPanel, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.contentPanel, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.contentPanel, 1.0, GridUnitType.Auto);
      this.textBlock = new TextBlock();
      this.textBlock.TextWrapping = this.allowWrap ? TextWrapping.Wrap : TextWrapping.NoWrap;
      FontManager.Instance.SetFontStyle1(this.isBold ? "Arial,11,Bold" : "Arial,11,Normal", this.textBlock);
      Brush commoncanvasText = (Brush) SkinConstants.BRUSH_COMMONCANVAS_TEXT;
      if (commoncanvasText != null)
        this.textBlock.Foreground = commoncanvasText;
      GridUtil.PlaceElement(this.contentPanel, (UIElement) this.textBlock, 0, 0);
      this.asteriskBlock = new TextBlock();
      this.asteriskBlock.Margin = new Thickness(4.0, 0.0, 0.0, 0.0);
      FontManager.Instance.SetFontStyle1("Arial,12,Bold", this.asteriskBlock);
      Brush brushCommonAsterisk = (Brush) SkinConstants.BRUSH_COMMON_ASTERISK;
      if (brushCommonAsterisk != null)
        this.asteriskBlock.Foreground = brushCommonAsterisk;
      this.asteriskBlock.Text = "*";
      GridUtil.PlaceElement(this.contentPanel, (UIElement) this.asteriskBlock, 0, 1);
      this.UpdateText();
      this.UpdateMandatoryState();
    }

    private void UpdateMandatoryState()
    {
      if (this.showAsterisk)
        this.asteriskBlock.Visibility = Visibility.Visible;
      else
        this.asteriskBlock.Visibility = Visibility.Collapsed;
    }

    public void ArrangeTextBlockWidth(double allocatedWidth)
    {
      this.contentPanel.Width = allocatedWidth + 50.0;
      this.contentPanel.UpdateLayout();
    }

    private double MeasureTextBlockWidth(TextBlock textblock, string text)
    {
      Typeface typeface = new Typeface(textblock.FontFamily, textblock.FontStyle, textblock.FontWeight, textblock.FontStretch);
      return new FormattedText(text, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, typeface, textblock.FontSize, textblock.Foreground).WidthIncludingTrailingWhitespace;
    }
  }
}
