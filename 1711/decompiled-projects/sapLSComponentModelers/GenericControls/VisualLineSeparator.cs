﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualLineSeparator
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualLineSeparator : ContentControl
  {
    private StackPanel contentPanel;

    public VisualLineSeparator()
    {
      this.InitializeComponent();
    }

    private void InitializeComponent()
    {
      this.contentPanel = new StackPanel();
      this.contentPanel.Orientation = Orientation.Horizontal;
      this.Content = (object) this.contentPanel;
      Brush brush = (Brush) new SolidColorBrush(Color.FromRgb((byte) 44, (byte) 118, (byte) 118));
      Line line = new Line();
      line.Stroke = brush;
      line.X1 = 1.0;
      line.X2 = 1000.0;
      line.Y1 = 1.0;
      line.Y2 = 1.0;
      line.HorizontalAlignment = HorizontalAlignment.Left;
      line.VerticalAlignment = VerticalAlignment.Center;
      line.StrokeThickness = 1.0;
      this.contentPanel.Children.Add((UIElement) line);
    }
  }
}
