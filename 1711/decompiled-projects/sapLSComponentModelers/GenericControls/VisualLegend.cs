﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualLegend
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  internal class VisualLegend : Button
  {
    private static string RESOURCE_NAME_STYLE_STANDARD = "style_standard";
    public const string DEFAULT_CONTROL_CONFIG_NAME = "com.sap.byd.canvas.legendcontrol";
    private VisualLegend.LegendStyles legendStyle;

    public VisualLegend()
      : this(VisualLegend.LegendStyles.Standard, false)
    {
      this.Height = 20.0;
    }

    public VisualLegend(VisualLegend.LegendStyles legendStyle, bool handleFocus)
    {
      this.legendStyle = legendStyle;
      this.SetLegendStyle(VisualLegend.RESOURCE_NAME_STYLE_STANDARD);
    }

    public void SetLegendStyle(string resourceName)
    {
      this.Style = new ResourceDictionary()
      {
        Source = new Uri("pack://application:,,,/sapLSUICore;component/XamlResource.xaml")
      }[(object) "ButtonStyleStandard"] as Style;
    }

    ~VisualLegend()
    {
    }

    public VisualLegend.LegendStyles LegendStyle
    {
      get
      {
        return this.legendStyle;
      }
    }

    protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
    {
      base.OnMouseLeftButtonDown(e);
      e.Handled = false;
    }

    public enum LegendStyles
    {
      Standard,
      Next,
      Previous,
      Emphasized,
      Menu,
      Toggle,
    }
  }
}
