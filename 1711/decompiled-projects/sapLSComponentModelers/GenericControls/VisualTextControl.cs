﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualTextControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualTextControl : BaseBorderControl
  {
    private Grid contentGrid;
    private TextBlock textBlock;

    public VisualTextControl(IModelObject modelItem)
      : base(modelItem)
    {
      this.MinHeight = 15.0;
      this.MinWidth = 80.0;
      this.BorderBrush = (Brush) Brushes.Green;
      this.contentGrid = new Grid();
      this.textBlock = new TextBlock();
      this.textBlock.Margin = new Thickness(2.0, 3.0, 2.0, 2.0);
      this.textBlock.TextWrapping = TextWrapping.Wrap;
      this.textBlock.VerticalAlignment = VerticalAlignment.Center;
      this.textBlock.HorizontalAlignment = HorizontalAlignment.Left;
      this.contentGrid.Children.Add((UIElement) this.textBlock);
      this.Content = (object) this.contentGrid;
    }

    public string Text
    {
      get
      {
        return this.textBlock.Text;
      }
      set
      {
        this.textBlock.Text = value;
      }
    }

    public override string SelectionText
    {
      get
      {
        return "TextControl";
      }
    }
  }
}
