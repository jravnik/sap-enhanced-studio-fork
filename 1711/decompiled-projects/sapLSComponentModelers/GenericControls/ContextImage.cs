﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.ContextImage
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class ContextImage : Image
  {
    public string Action { get; set; }

    public override string ToString()
    {
      if (!string.IsNullOrEmpty(this.Action))
        return this.Action;
      return string.Empty;
    }
  }
}
