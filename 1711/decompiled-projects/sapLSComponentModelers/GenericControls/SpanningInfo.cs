﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.SpanningInfo
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class SpanningInfo
  {
    private int parentColumns;
    private int parentRows;
    private int column;
    private int row;
    private int colSpan;
    private int rowSpan;

    public int ParentColumns
    {
      get
      {
        return this.parentColumns;
      }
      set
      {
        this.parentColumns = value;
      }
    }

    public int ParentRows
    {
      get
      {
        return this.parentRows;
      }
      set
      {
        this.parentRows = value;
      }
    }

    public int ColSpan
    {
      get
      {
        return this.colSpan;
      }
      set
      {
        this.colSpan = value;
      }
    }

    public int RowSpan
    {
      get
      {
        return this.rowSpan;
      }
      set
      {
        this.rowSpan = value;
      }
    }

    public int Column
    {
      get
      {
        return this.column;
      }
      set
      {
        this.column = value;
      }
    }

    public int Row
    {
      get
      {
        return this.row;
      }
      set
      {
        this.row = value;
      }
    }

    public SpanningInfo(int totalColumns, int totalRows, int currentColumn, int currentRow, int currentColSpan, int currentRowSpan)
    {
      this.parentColumns = totalColumns;
      this.parentRows = totalRows;
      this.column = currentColumn;
      this.row = currentRow;
      this.colSpan = currentColSpan;
      this.rowSpan = currentRowSpan;
    }
  }
}
