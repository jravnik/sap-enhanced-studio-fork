﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualCompoundField
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public class VisualCompoundField : UserControl
  {
    private const string RESOURCE_ICON = "sapLSComponentModelers;component/Images/Icons/Icon.gif,16,16";
    private const string RESOURCE_IMAGE = "sapLSComponentModelers;component/Images/Icons/Image.gif,16,16";
    private Grid rootGrid;
    private StackPanel sp;
    private CompoundField m_ControlDefinition;

    public Grid RootGrid
    {
      get
      {
        return this.rootGrid;
      }
    }

    public VisualCompoundField(CompoundField controlDefinition, UsageType usage)
    {
      this.m_ControlDefinition = controlDefinition;
      this.LoadView(usage);
    }

    private void LoadView(UsageType usage)
    {
      this.rootGrid = new Grid();
      double num1 = usage == UsageType.cellRenderer ? 0.0 : 5.0;
      int col = 0;
      GridUtil.AddRowDef(this.rootGrid, 25.0, GridUnitType.Pixel);
      int num2 = 0;
      this.sp = new StackPanel();
      foreach (AbstractControl controlDefinition in this.m_ControlDefinition.Items)
      {
        UIElement element = ControlFactory.ConstructFormpaneControl(controlDefinition, this.m_ControlDefinition.CCTSType, usage);
        FrameworkElement frameworkElement = element as FrameworkElement;
        if (usage == UsageType.cellRenderer)
        {
          if (controlDefinition.CCTSType != UXCCTSTypes.none)
            GridUtil.AddColumnDef(this.rootGrid, 1.0, GridUnitType.Auto);
          else
            GridUtil.AddColumnDef(this.rootGrid, 1.0, GridUnitType.Star);
          GridUtil.PlaceElement(this.rootGrid, element, 0, col);
          GridUtil.AddColumnDef(this.rootGrid, num1, GridUnitType.Pixel);
          col += 2;
        }
        else if ((usage == UsageType.form || usage == UsageType.formReadonly) && frameworkElement.Width.ToString() != "NaN")
        {
          if (!string.IsNullOrEmpty(controlDefinition.FieldWidth))
          {
            string source = controlDefinition.FieldWidth.ToString();
            if (source.Contains<char>('%'))
            {
              double num3 = (element as FrameworkElement).Width / 100.0;
              if (controlDefinition.CCTSType != UXCCTSTypes.none)
                GridUtil.AddColumnDef(this.rootGrid, num3, GridUnitType.Auto);
              else
                GridUtil.AddColumnDef(this.rootGrid, num3, GridUnitType.Star);
              (element as FrameworkElement).Width = double.NaN;
              GridUtil.PlaceElement(this.rootGrid, element, 0, col);
              GridUtil.AddColumnDef(this.rootGrid, num1, GridUnitType.Pixel);
              col += 2;
            }
            else if (source.Contains("px") || source.Contains("ex"))
            {
              this.sp.Orientation = Orientation.Horizontal;
              string fieldWidth = this.m_ControlDefinition.FieldWidth;
              this.sp.Children.Add(element);
              ++num2;
              if (num2 != this.m_ControlDefinition.Items.Count)
                (element as FrameworkElement).Margin = new Thickness(0.0, 0.0, 5.0, 0.0);
              if (num2 == this.m_ControlDefinition.Items.Count)
                GridUtil.PlaceElement(this.rootGrid, (UIElement) this.sp, 0, 0);
            }
          }
          else
          {
            this.sp.Orientation = Orientation.Horizontal;
            string fieldWidth = this.m_ControlDefinition.FieldWidth;
            this.sp.Children.Add(element);
            ++num2;
            if (num2 != this.m_ControlDefinition.Items.Count)
              (element as FrameworkElement).Margin = new Thickness(0.0, 0.0, 5.0, 0.0);
            if (num2 == this.m_ControlDefinition.Items.Count)
              GridUtil.PlaceElement(this.rootGrid, (UIElement) this.sp, 0, 0);
          }
        }
        else if ((usage == UsageType.form || usage == UsageType.formReadonly) && frameworkElement.Width.ToString() == "NaN")
        {
          this.sp.Orientation = Orientation.Horizontal;
          element.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
          frameworkElement.Width = element.DesiredSize.Width;
          this.sp.Children.Add((UIElement) frameworkElement);
          ++num2;
          if (num2 != this.m_ControlDefinition.Items.Count)
            (element as FrameworkElement).Margin = new Thickness(0.0, 0.0, 5.0, 0.0);
          if (num2 == this.m_ControlDefinition.Items.Count)
            GridUtil.PlaceElement(this.rootGrid, (UIElement) this.sp, 0, 0);
        }
      }
      this.Content = (object) this.rootGrid;
    }
  }
}
