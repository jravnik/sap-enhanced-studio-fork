﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.WinFormControls.RadioButtonEditingControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.WinFormControls
{
  public class RadioButtonEditingControl : RadioButton, IDataGridViewEditingControl
  {
    protected int rowIndex;
    protected DataGridView dataGridView;
    protected bool valueChanged;

    public RadioButtonEditingControl()
    {
      this.Checked = true;
    }

    protected override void OnCheckedChanged(EventArgs e)
    {
      base.OnCheckedChanged(e);
      this.NotifyDataGridViewOfValueChange();
    }

    protected virtual void NotifyDataGridViewOfValueChange()
    {
      this.valueChanged = true;
      if (this.dataGridView == null)
        return;
      this.dataGridView.NotifyCurrentCellDirty(true);
    }

    public Cursor EditingPanelCursor
    {
      get
      {
        return Cursors.IBeam;
      }
    }

    public Cursor EditingControlCursor
    {
      get
      {
        return this.Cursor;
      }
    }

    public DataGridView EditingControlDataGridView
    {
      get
      {
        return this.dataGridView;
      }
      set
      {
        this.dataGridView = value;
      }
    }

    public object EditingControlFormattedValue
    {
      get
      {
        return (object) this.Checked.ToString();
      }
      set
      {
        this.Checked = (bool) value;
      }
    }

    public bool EditingControlWantsInputKey(Keys keyData, bool dataGridViewWantsInputKey)
    {
      switch (keyData & Keys.KeyCode)
      {
        case Keys.Prior:
        case Keys.Next:
        case Keys.End:
        case Keys.Home:
        case Keys.Left:
        case Keys.Right:
        case Keys.Delete:
          return true;
        default:
          return !dataGridViewWantsInputKey;
      }
    }

    public void PrepareEditingControlForEdit(bool selectAll)
    {
      if (selectAll)
        this.Checked = false;
      else
        this.Checked = true;
    }

    public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
    {
      return (object) this.Checked.ToString();
    }

    public bool RepositionEditingControlOnValueChange
    {
      get
      {
        return this.valueChanged;
      }
    }

    public int EditingControlRowIndex
    {
      get
      {
        return this.rowIndex;
      }
      set
      {
        this.rowIndex = value;
      }
    }

    public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
    {
      this.Font = dataGridViewCellStyle.Font;
      this.ForeColor = dataGridViewCellStyle.ForeColor;
      this.BackColor = dataGridViewCellStyle.BackColor;
    }

    public bool EditingControlValueChanged
    {
      get
      {
        return this.valueChanged;
      }
      set
      {
        this.valueChanged = value;
      }
    }

    private static HorizontalAlignment translateAlignment(DataGridViewContentAlignment align)
    {
      switch (align)
      {
        case DataGridViewContentAlignment.BottomCenter:
        case DataGridViewContentAlignment.TopCenter:
        case DataGridViewContentAlignment.MiddleCenter:
          return HorizontalAlignment.Center;
        case DataGridViewContentAlignment.BottomRight:
        case DataGridViewContentAlignment.MiddleRight:
        case DataGridViewContentAlignment.TopRight:
          return HorizontalAlignment.Right;
        case DataGridViewContentAlignment.BottomLeft:
        case DataGridViewContentAlignment.TopLeft:
        case DataGridViewContentAlignment.MiddleLeft:
          return HorizontalAlignment.Left;
        default:
          throw new ArgumentException("Error: Invalid Content Alignment!");
      }
    }

    protected override void OnClick(EventArgs e)
    {
      if (this.Checked)
        this.Checked = false;
      else
        this.Checked = true;
    }
  }
}
