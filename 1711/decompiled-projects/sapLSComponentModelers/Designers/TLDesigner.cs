﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.TLDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using System.ComponentModel;
using System.Windows;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class TLDesigner : ComponentDesigner
  {
    private VisualIdentificationRegion m_VisualIDR;
    private VisualTileDataRegion m_VisualTL;
    private System.ComponentModel.IContainer components;

    internal TileComponent Component
    {
      get
      {
        return this.m_ModelObject as TileComponent;
      }
    }

    public TLDesigner(IModelObject rootModel)
      : base(rootModel, true, true)
    {
      this.AllowDrop = true;
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.AddIdentificationRegion();
      this.AddTileDataRegion();
    }

    private void AddIdentificationRegion()
    {
      if (this.m_VisualIDR != null)
        this.m_VisualIDR.Terminate();
      this.m_VisualIDR = new VisualIdentificationRegion((IModelObject) this.Component.IdentificationRegion);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_VisualIDR, 0, 0);
    }

    private void AddTileDataRegion()
    {
      if (this.m_VisualTL != null)
        this.m_VisualTL.Terminate();
      this.m_VisualTL = new VisualTileDataRegion((IModelObject) this.Component.TileDataRegion);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_VisualTL, 1, 0);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (System.ComponentModel.IContainer) new Container();
      this.AutoScaleMode = AutoScaleMode.Font;
    }
  }
}
