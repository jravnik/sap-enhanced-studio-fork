﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.MashupDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Xml;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class MashupDesigner : ComponentDesigner, ISelectionProvider
  {
    private WebBrowser webBowser;
    private System.ComponentModel.IContainer components;

    internal MashupComponent OberonModelObject
    {
      get
      {
        return this.m_ModelObject as MashupComponent;
      }
    }

    public MashupDesigner(IModelObject rootModel)
      : base(rootModel, true, true)
    {
    }

    public override void LoadView()
    {
      base.LoadView();
      this.AddTitle();
      this.AddContent();
    }

    private void AddTitle()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      DisplayTextControl displayTextControl = new DisplayTextControl(TextViewStyles.PageHeaderTitle, "Mashup");
      displayTextControl.Margin = new Thickness(5.0, 5.0, 5.0, 5.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) displayTextControl, 0, 0);
    }

    private void AddContent()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      try
      {
        this.webBowser = new WebBrowser();
        GridUtil.PlaceElement(this.Grid, (UIElement) new WindowsFormsHost()
        {
          Child = (Control) this.webBowser
        }, 1, 0, 1, 2);
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(this.OberonModelObject.MashupXml);
        string str = Path.GetTempPath() + "TemporaryXml.xml";
        using (StreamWriter streamWriter = new StreamWriter(str, false, Encoding.UTF8))
        {
          streamWriter.Write(xmlDocument.OuterXml);
          streamWriter.Close();
          streamWriter.Dispose();
        }
        this.webBowser.Navigate(str);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (System.ComponentModel.IContainer) new Container();
      this.AutoScaleMode = AutoScaleMode.Font;
    }
  }
}
