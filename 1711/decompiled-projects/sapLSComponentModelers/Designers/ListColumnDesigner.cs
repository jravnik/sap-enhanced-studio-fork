﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ListColumnDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class ListColumnDesigner : ComponentDesigner
  {
    private BaseSelectableControl m_LayoutControl;
    private Grid listColumnDesignerGrid;

    public event EventHandler BackToDesignerClicked;

    internal ListColumn Component
    {
      get
      {
        return this.m_ModelObject as ListColumn;
      }
    }

    public Grid ListColumnGrid
    {
      get
      {
        return this.listColumnDesignerGrid;
      }
    }

    public ListColumnDesigner(IModelObject rootModel)
      : base(rootModel, false, false)
    {
      this.AllowDrop = true;
      this.listColumnDesignerGrid = new Grid();
      this.LoadView();
    }

    public override void LoadView()
    {
      this.AddTitle();
      this.AddContentRegion();
    }

    public override void Terminate()
    {
      base.Terminate();
      if (this.listColumnDesignerGrid == null)
        return;
      ComponentModelerUtilities.Instance.TerminatePanelControls((FrameworkElement) this.listColumnDesignerGrid);
    }

    private void AddTitle()
    {
      GridUtil.AddRowDef(this.listColumnDesignerGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.listColumnDesignerGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.listColumnDesignerGrid, 1.0, 150.0, GridUnitType.Pixel);
      GridUtil.PlaceElement(this.listColumnDesignerGrid, (UIElement) new DisplayTextControl(TextViewStyles.PageHeaderTitle, string.Concat((object) this.Component.Label)), 0, 0);
      System.Windows.Controls.Button button = new System.Windows.Controls.Button();
      button.Background = (Brush) Brushes.Transparent;
      button.BorderBrush = (Brush) Brushes.Transparent;
      StackPanel stackPanel = new StackPanel();
      stackPanel.Orientation = Orientation.Horizontal;
      stackPanel.Children.Add((UIElement) new System.Windows.Controls.Image()
      {
        Source = (ImageSource) this.ConvertToBitMapImage(Resource.GoBack)
      });
      TextBlock textBlock = new TextBlock();
      textBlock.Text = "Back To Designer";
      textBlock.TextAlignment = TextAlignment.Center;
      textBlock.Foreground = (Brush) Brushes.Blue;
      textBlock.TextDecorations = TextDecorations.Underline;
      textBlock.VerticalAlignment = VerticalAlignment.Center;
      stackPanel.Children.Add((UIElement) textBlock);
      stackPanel.MouseDown += new MouseButtonEventHandler(this.buttonPanel_MouseDown);
      button.Content = (object) stackPanel;
      GridUtil.PlaceElement(this.listColumnDesignerGrid, (UIElement) button, 0, 1);
    }

    private void buttonPanel_MouseDown(object sender, MouseButtonEventArgs e)
    {
      try
      {
        if (this.BackToDesignerClicked == null)
          return;
        this.BackToDesignerClicked(sender, (EventArgs) e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    private void AddContentRegion()
    {
      if (this.m_LayoutControl != null)
        this.m_LayoutControl.Terminate();
      if (this.Component.Item == null || !(this.Component.Item is LayoutContainerControl))
        return;
      GridUtil.AddRowDef(this.listColumnDesignerGrid, 1.0, GridUnitType.Star);
      this.m_LayoutControl = ControlFactory.ConstructFormpaneControl(this.Component.Item, UXCCTSTypes.none, UsageType.cellRenderer) as BaseSelectableControl;
      this.m_LayoutControl.SetValue(Grid.ColumnSpanProperty, (object) 2);
      GridUtil.PlaceElement(this.listColumnDesignerGrid, (UIElement) this.m_LayoutControl, 1, 0);
    }
  }
}
