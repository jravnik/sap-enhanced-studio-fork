﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep.ViewSelectionStep
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep
{
  public partial class ViewSelectionStep : BaseWizardStepControl, IComponentConnector
  {
    private object m_WizardResult;
    public IFloorplan m_FpToOpen;
    public List<string> m_FolderDetails;
    //internal Grid rootGrid;
    //internal GroupBox groupBoxOverview;
    //internal StackPanel stackPanelOverview;
    //internal CheckBox chkOverview;
    //internal OverviewInfo overviewControl;
    //internal GroupBox groupBoxMytasks;
    //internal StackPanel stackPanelMyTasks;
    //internal CheckBox chkMyTasks;
    //internal MyTasksInfo mytasksControl;
    //internal GroupBox groupBoxReports;
    //internal StackPanel stackPanelReports;
    //internal CheckBox chkReports;
    //private bool _contentLoaded;

    public override object WizardResult
    {
      get
      {
        return this.m_WizardResult;
      }
    }

    public ViewSelectionStep()
    {
      this.InitializeComponent();
      this.Background = (Brush) new SolidColorBrush(Color.FromRgb(byte.MaxValue, byte.MaxValue, byte.MaxValue));
      bool? isChecked1 = this.chkOverview.IsChecked;
      if ((isChecked1.GetValueOrDefault() ? 0 : (isChecked1.HasValue ? 1 : 0)) != 0)
        this.overviewControl.IsEnabled = false;
      bool? isChecked2 = this.chkMyTasks.IsChecked;
      if ((isChecked2.GetValueOrDefault() ? 0 : (isChecked2.HasValue ? 1 : 0)) == 0)
        return;
      this.mytasksControl.IsEnabled = false;
    }

    private void OnOverviewCheckedChanged(object sender, RoutedEventArgs e)
    {
      bool? isChecked = this.chkOverview.IsChecked;
      if ((!isChecked.GetValueOrDefault() ? 0 : (isChecked.HasValue ? 1 : 0)) != 0)
      {
        if (!this.stackPanelOverview.Children.Contains((UIElement) this.overviewControl))
          return;
        this.overviewControl.IsEnabled = true;
        this.overviewControl.InitializeMembers(this);
        this.overviewControl.SetECComponentDetails();
        this.overviewControl.SetWCViewComponentDetails();
      }
      else
      {
        if (!this.stackPanelOverview.Children.Contains((UIElement) this.overviewControl))
          return;
        this.overviewControl.IsEnabled = false;
        this.overviewControl.ClearValues();
      }
    }

    private void OnMyTaskCheckedChanged(object sender, RoutedEventArgs e)
    {
      bool? isChecked = this.chkMyTasks.IsChecked;
      if ((!isChecked.GetValueOrDefault() ? 0 : (isChecked.HasValue ? 1 : 0)) != 0)
      {
        if (!this.stackPanelMyTasks.Children.Contains((UIElement) this.mytasksControl))
          return;
        this.mytasksControl.IsEnabled = true;
        this.mytasksControl.InitializeMembers(this);
        this.mytasksControl.SetOWLComponentDetails();
        this.mytasksControl.SetWCViewComponentDetails();
      }
      else
      {
        if (!this.stackPanelMyTasks.Children.Contains((UIElement) this.mytasksControl))
          return;
        this.mytasksControl.IsEnabled = false;
        this.mytasksControl.ClearValues();
      }
    }

    public override void OnFinish(PropertyBag propertyBag, out bool finishStep)
    {
      finishStep = true;
      bool? isChecked1 = this.chkOverview.IsChecked;
      if ((!isChecked1.GetValueOrDefault() ? 0 : (isChecked1.HasValue ? 1 : 0)) == 0 || !string.IsNullOrEmpty(this.overviewControl.txtboxShortId.Text))
      {
        bool? isChecked2 = this.chkMyTasks.IsChecked;
        if ((!isChecked2.GetValueOrDefault() ? 0 : (isChecked2.HasValue ? 1 : 0)) == 0 || !string.IsNullOrEmpty(this.mytasksControl.txtboxShortId.Text))
        {
          if (propertyBag == null)
            return;
          ViewSelectionStep viewSelectionStep = propertyBag.GetValue(nameof (ViewSelectionStep)) as ViewSelectionStep;
          if (viewSelectionStep == null)
            return;
          IFloorplan centerFloorplanModel = (IFloorplan) ModelLayer.ObjectManager.CreateWorkCenterFloorplanModel();
          if (centerFloorplanModel != null && centerFloorplanModel.CenterStructure != null)
          {
            bool? isChecked3 = viewSelectionStep.chkOverview.IsChecked;
            if ((!isChecked3.GetValueOrDefault() ? 0 : (isChecked3.HasValue ? 1 : 0)) != 0 && viewSelectionStep.overviewControl.m_OverviewECDetails.Count == 3 && viewSelectionStep.overviewControl.m_OverviewWCViewDetails.Count == 3)
              centerFloorplanModel.CenterStructure.AddOverview(viewSelectionStep.overviewControl.m_OverviewECDetails, viewSelectionStep.overviewControl.m_OverviewWCViewDetails, viewSelectionStep.overviewControl.IsExistingFile);
            bool? isChecked4 = viewSelectionStep.chkMyTasks.IsChecked;
            if ((!isChecked4.GetValueOrDefault() ? 0 : (isChecked4.HasValue ? 1 : 0)) != 0 && viewSelectionStep.mytasksControl.m_MyTasksOWLDetails.Count == 3 && viewSelectionStep.mytasksControl.m_MyTasksWCViewDetails.Count == 3)
              centerFloorplanModel.CenterStructure.AddMyTasks(viewSelectionStep.mytasksControl.m_MyTasksOWLDetails, viewSelectionStep.mytasksControl.m_MyTasksWCViewDetails, viewSelectionStep.mytasksControl.IsExistingFile);
            bool? isChecked5 = viewSelectionStep.chkReports.IsChecked;
            if ((!isChecked5.GetValueOrDefault() ? 0 : (isChecked5.HasValue ? 1 : 0)) != 0)
              centerFloorplanModel.CenterStructure.AddReports();
          }
          this.m_WizardResult = (object) centerFloorplanModel;
          List<string> stringList = propertyBag.GetValue("FolderSelectionDetails") as List<string>;
          if (centerFloorplanModel == null || stringList == null || stringList.Count <= 0)
            return;
          centerFloorplanModel.RepositoryPath = stringList[0];
          centerFloorplanModel.Name = stringList[1];
          centerFloorplanModel.DevClass = stringList[2];
          if (!ModelLayer.ObjectManager.SaveNewConfiguration(centerFloorplanModel))
            return;
          centerFloorplanModel.IsNewConfiguration = false;
          centerFloorplanModel.IsDirty = false;
          return;
        }
      }
      finishStep = false;
      int num = (int) DisplayMessage.Show("It is mandatory to have ShortId for WCView");
    }

    public override bool PreValidate(PropertyBag propertyBag)
    {
      return true;
    }

    public override void OnCancel(PropertyBag propertyBag)
    {
    }

    public override void OnNext(PropertyBag propertyBag, out bool moveNext)
    {
      moveNext = true;
      bool? isChecked1 = this.chkOverview.IsChecked;
      if ((!isChecked1.GetValueOrDefault() ? 0 : (isChecked1.HasValue ? 1 : 0)) == 0 || !string.IsNullOrEmpty(this.overviewControl.txtboxShortId.Text))
      {
        bool? isChecked2 = this.chkMyTasks.IsChecked;
        if ((!isChecked2.GetValueOrDefault() ? 0 : (isChecked2.HasValue ? 1 : 0)) == 0 || !string.IsNullOrEmpty(this.mytasksControl.txtboxShortId.Text))
        {
          if (propertyBag == null)
            return;
          propertyBag.SetValue(nameof (ViewSelectionStep), (object) this);
          return;
        }
      }
      moveNext = false;
      int num = (int) DisplayMessage.Show("It is mandatory to have ShortId for WCView");
    }

    public override void OnPrevious(PropertyBag propertyBag, out bool movePrevious)
    {
      movePrevious = true;
    }

    public override void LoadStepView(PropertyBag propertyBag)
    {
      if (propertyBag == null)
        return;
      propertyBag.SetValue("FolderSelectionDetails", (object) this.m_FolderDetails);
    }

    public override bool SetDetails(Dictionary<string, string> details)
    {
      this.m_FolderDetails = new List<string>();
      if (details == null || details.Count <= 0)
        return false;
      this.m_FolderDetails.Add(details["RepositoryPath"]);
      this.m_FolderDetails.Add(details["ConfigurationName"]);
      this.m_FolderDetails.Add(details["DevClass"]);
      this.m_FolderDetails.Add(details["ShortId"]);
      return true;
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/designers/workcenterarea/wcfwizardstep/viewselectionstep.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.rootGrid = (Grid) target;
    //      break;
    //    case 2:
    //      this.groupBoxOverview = (GroupBox) target;
    //      break;
    //    case 3:
    //      this.stackPanelOverview = (StackPanel) target;
    //      break;
    //    case 4:
    //      this.chkOverview = (CheckBox) target;
    //      this.chkOverview.Unchecked += new RoutedEventHandler(this.OnOverviewCheckedChanged);
    //      this.chkOverview.Checked += new RoutedEventHandler(this.OnOverviewCheckedChanged);
    //      break;
    //    case 5:
    //      this.overviewControl = (OverviewInfo) target;
    //      break;
    //    case 6:
    //      this.groupBoxMytasks = (GroupBox) target;
    //      break;
    //    case 7:
    //      this.stackPanelMyTasks = (StackPanel) target;
    //      break;
    //    case 8:
    //      this.chkMyTasks = (CheckBox) target;
    //      this.chkMyTasks.Unchecked += new RoutedEventHandler(this.OnMyTaskCheckedChanged);
    //      this.chkMyTasks.Checked += new RoutedEventHandler(this.OnMyTaskCheckedChanged);
    //      break;
    //    case 9:
    //      this.mytasksControl = (MyTasksInfo) target;
    //      break;
    //    case 10:
    //      this.groupBoxReports = (GroupBox) target;
    //      break;
    //    case 11:
    //      this.stackPanelReports = (StackPanel) target;
    //      break;
    //    case 12:
    //      this.chkReports = (CheckBox) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
