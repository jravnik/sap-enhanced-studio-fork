﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep.WCFFolderSelection
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep
{
  public partial class WCFFolderSelection : BaseWizardStepControl, IComponentConnector
  {
    public List<string> m_WCFolderDetails;
    public FolderSelectionControl m_FSControl;
    //internal Grid rootGrid;
    //internal WindowsFormsHost elementHost;
    //private bool _contentLoaded;

    public override object WizardResult
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public WCFFolderSelection()
    {
      this.InitializeComponent();
      this.Background = (Brush) new SolidColorBrush(Color.FromRgb(byte.MaxValue, byte.MaxValue, byte.MaxValue));
      this.InitializeStep();
    }

    private void InitializeStep()
    {
      if (this.m_FSControl == null)
        this.m_FSControl = new FolderSelectionControl(FloorplanType.WCF, "New_WCF");
      this.m_FSControl.Dock = DockStyle.Fill;
      this.elementHost.Child = (System.Windows.Forms.Control) this.m_FSControl;
      this.elementHost.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.elementHost.VerticalAlignment = VerticalAlignment.Stretch;
    }

    public override void OnNext(PropertyBag propertyBag, out bool moveNext)
    {
      moveNext = true;
      if (string.IsNullOrEmpty(this.m_FSControl.ShortId))
      {
        moveNext = false;
        int num = (int) DisplayMessage.Show("It is mandatory to enter shortId for a WCF component");
      }
      else
      {
        this.m_WCFolderDetails = new List<string>();
        if (this.m_FSControl != null)
        {
          this.m_WCFolderDetails.Add(this.m_FSControl.RepositoryPath);
          this.m_WCFolderDetails.Add(this.m_FSControl.ConfigurationName);
          this.m_WCFolderDetails.Add(this.m_FSControl.DevClass);
          this.m_WCFolderDetails.Add(this.m_FSControl.ShortId);
        }
        propertyBag.SetValue("FolderSelectionDetails", (object) this.m_WCFolderDetails);
      }
    }

    public override void OnPrevious(PropertyBag propertyBag, out bool movePrevious)
    {
      movePrevious = true;
    }

    public override void OnCancel(PropertyBag propertyBag)
    {
    }

    public override void OnFinish(PropertyBag propertyBag, out bool finishStep)
    {
      finishStep = true;
      if (!string.IsNullOrEmpty(this.m_FSControl.ShortId))
        return;
      finishStep = false;
      int num = (int) DisplayMessage.Show("It is mandatory to enter shortId for a WCF component");
    }

    public override bool PreValidate(PropertyBag propertyBag)
    {
      return true;
    }

    public override void LoadStepView(PropertyBag propertyBag)
    {
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/designers/workcenterarea/wcfwizardstep/wcffolderselection.xaml", UriKind.Relative));
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.rootGrid = (Grid) target;
    //      break;
    //    case 2:
    //      this.elementHost = (WindowsFormsHost) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
