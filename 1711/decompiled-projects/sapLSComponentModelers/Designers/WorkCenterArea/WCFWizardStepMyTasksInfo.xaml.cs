﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep.MyTasksInfo
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep
{
  public partial class MyTasksInfo : System.Windows.Controls.UserControl
  {
    private const string WCVIEW_UICOMPONENT_EXTENSION = ".WCVIEW.uiwocview";
    private const string OWL_UICOMPONENT_EXTENSION = ".OWL.uicomponent";
    public List<string> m_WCFFolderDetails;
    public List<string> m_MyTasksOWLDetails;
    public List<string> m_MyTasksWCViewDetails;
    public bool IsExistingFile;
    //internal System.Windows.Controls.GroupBox groupBoxMyTasks;
    //internal StackPanel stackPanelMyTasks;
    //internal TextBlock lblEC;
    //internal StackPanel ECSelection;
    //internal TextBlock lblName;
    //internal System.Windows.Controls.TextBox txtboxOWLName;
    //internal System.Windows.Controls.TextBox txtboxOWLComponent;
    //internal System.Windows.Controls.Button btnBrowse2;
    //internal System.Windows.Controls.Button fileSelector;
    //internal TextBlock lblWCView;
    //internal StackPanel WCViewSelection;
    //internal TextBlock lblWCName;
    //internal System.Windows.Controls.TextBox txtboxOWLWCName;
    //internal System.Windows.Controls.TextBox txtboxWCViewComponent;
    //internal System.Windows.Controls.Button btnBrowse3;
    //internal StackPanel shortidstack;
    //internal TextBlock lblShortId;
    //internal System.Windows.Controls.TextBox txtboxShortId;
    //private bool _contentLoaded;

    public MyTasksInfo()
    {
      this.InitializeComponent();
    }

    public MyTasksInfo(ViewSelectionStep parent)
    {
      this.InitializeComponent();
      if (parent != null)
        this.InitializeMembers(parent);
      this.SetOWLComponentDetails();
      this.SetWCViewComponentDetails();
    }

    public void InitializeMembers(ViewSelectionStep parent)
    {
      this.m_WCFFolderDetails = parent.m_FolderDetails;
      this.m_MyTasksOWLDetails = this.GetClone(this.m_WCFFolderDetails, this.m_MyTasksOWLDetails);
      this.m_MyTasksOWLDetails[1] = this.m_MyTasksOWLDetails[1] + "_Work_OWL";
      this.m_MyTasksWCViewDetails = this.GetClone(this.m_WCFFolderDetails, this.m_MyTasksWCViewDetails);
      this.m_MyTasksWCViewDetails.Add(this.txtboxShortId.Text);
      this.m_MyTasksWCViewDetails[1] = this.m_MyTasksWCViewDetails[1] + "_Work_WCView";
    }

    private List<string> GetClone(List<string> wcfDetails, List<string> clonedList)
    {
      clonedList = new List<string>();
      clonedList.Add(wcfDetails[0]);
      clonedList.Add(wcfDetails[1]);
      clonedList.Add(wcfDetails[2]);
      return clonedList;
    }

    public void SetOWLComponentDetails()
    {
      if (this.m_MyTasksOWLDetails == null || this.m_MyTasksOWLDetails.Count < 3)
        return;
      this.txtboxOWLName.Text = this.m_MyTasksOWLDetails[1];
      this.txtboxOWLComponent.Text = this.m_MyTasksOWLDetails[0] + "/" + this.m_MyTasksOWLDetails[1] + ".OWL.uicomponent";
    }

    public void SetWCViewComponentDetails()
    {
      if (this.m_MyTasksWCViewDetails == null || this.m_MyTasksWCViewDetails.Count < 3)
        return;
      this.txtboxOWLWCName.Text = this.m_MyTasksWCViewDetails[1];
      this.txtboxWCViewComponent.Text = this.m_MyTasksWCViewDetails[0] + "/" + this.m_MyTasksWCViewDetails[1] + ".WCVIEW.uiwocview";
    }

    public void ClearValues()
    {
      this.txtboxOWLName.Text = string.Empty;
      this.txtboxOWLComponent.Text = string.Empty;
      this.txtboxOWLWCName.Text = string.Empty;
      this.txtboxWCViewComponent.Text = string.Empty;
    }

    private void OpenMyTasksFolderSelector(object sender, RoutedEventArgs e)
    {
      FolderSelectionDialog folderSelectionDialog = new FolderSelectionDialog(FloorplanType.EC, this.txtboxOWLName.Text);
      if (folderSelectionDialog.ShowDialog() != DialogResult.OK)
        return;
      this.m_MyTasksOWLDetails = new List<string>();
      this.m_MyTasksOWLDetails.Add(folderSelectionDialog.RepositoryPath);
      this.m_MyTasksOWLDetails.Add(folderSelectionDialog.ConfigurationName);
      this.m_MyTasksOWLDetails.Add(folderSelectionDialog.DevClass);
      this.txtboxOWLComponent.Text = folderSelectionDialog.RepositoryPath + "/" + folderSelectionDialog.ConfigurationName + ".OWL.uicomponent";
      this.txtboxOWLName.Text = folderSelectionDialog.ConfigurationName;
    }

    private void OpenMyTasksWCViewFolderSelector(object sender, RoutedEventArgs e)
    {
      FolderSelectionDialog folderSelectionDialog = new FolderSelectionDialog(FloorplanType.WCVIEW, this.txtboxOWLWCName.Text);
      if (folderSelectionDialog.ShowDialog() != DialogResult.OK)
        return;
      this.m_MyTasksWCViewDetails = new List<string>();
      this.m_MyTasksWCViewDetails.Add(folderSelectionDialog.RepositoryPath);
      this.m_MyTasksWCViewDetails.Add(folderSelectionDialog.ConfigurationName);
      this.m_MyTasksWCViewDetails.Add(folderSelectionDialog.DevClass);
      this.m_MyTasksWCViewDetails.Add(folderSelectionDialog.ShortId);
      this.txtboxWCViewComponent.Text = folderSelectionDialog.RepositoryPath + "/" + folderSelectionDialog.ConfigurationName + ".WCVIEW.uiwocview";
      this.txtboxOWLWCName.Text = folderSelectionDialog.ConfigurationName;
      this.txtboxShortId.Text = folderSelectionDialog.ShortId;
    }

    private void OpenMyTasksFileSelector(object sender, RoutedEventArgs e)
    {
      RepositoryBrowser instance = RepositoryBrowser.GetInstance();
      instance.OnFloorplanSelection -= new RepositoryBrowser.OnFloorPlanSelection(this.OnOWLComponentSelected);
      instance.OnFloorplanSelection += new RepositoryBrowser.OnFloorPlanSelection(this.OnOWLComponentSelected);
      int num = (int) instance.ShowDialog();
    }

    private bool OnOWLComponentSelected(object sender, EventArgs e)
    {
      try
      {
        if (sender is FloorplanInfo)
        {
          FloorplanInfo floorplanInfo = sender as FloorplanInfo;
          if (floorplanInfo != null)
          {
            if (floorplanInfo.ConfigurationType.ToLower() == FloorplanType.OWL.ToString().ToLower())
            {
              this.m_MyTasksOWLDetails = new List<string>();
              this.m_MyTasksOWLDetails.Add(floorplanInfo.RepositoryPath);
              this.m_MyTasksOWLDetails.Add(floorplanInfo.Name);
              this.m_MyTasksOWLDetails.Add(floorplanInfo.DevClass);
              this.txtboxOWLName.Text = floorplanInfo.Name;
              this.txtboxOWLComponent.Text = floorplanInfo.RepositoryPath + "/" + this.m_MyTasksOWLDetails[1] + ".OWL.uicomponent";
              this.IsExistingFile = true;
              return true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Exception selecting the OWL Component", ex));
      }
      return false;
    }

    private void OnOWLNameChanged(object sender, RoutedEventArgs e)
    {
      if (this.m_MyTasksOWLDetails != null)
      {
        this.txtboxOWLComponent.Text = this.m_MyTasksOWLDetails[0] + "/" + this.txtboxOWLName.Text + ".OWL.uicomponent";
        this.m_MyTasksOWLDetails[1] = this.txtboxOWLName.Text;
      }
      else
      {
        if (this.txtboxOWLComponent.Text == null)
          return;
        int num = this.txtboxOWLComponent.Text.LastIndexOf('/');
        this.txtboxOWLComponent.Text = this.txtboxOWLComponent.Text.Remove(num + 1, this.txtboxOWLComponent.Text.Length - (num + 1)) + this.txtboxOWLName.Text + ".OWL.uicomponent";
      }
    }

    private void OnWCViewNameChanged(object sender, RoutedEventArgs e)
    {
      if (this.m_MyTasksWCViewDetails != null)
      {
        this.txtboxWCViewComponent.Text = this.m_MyTasksWCViewDetails[0] + "/" + this.txtboxOWLWCName.Text + ".WCVIEW.uiwocview";
        this.m_MyTasksWCViewDetails[1] = this.txtboxOWLWCName.Text;
      }
      else
      {
        if (this.txtboxWCViewComponent.Text == null)
          return;
        int num = this.txtboxWCViewComponent.Text.LastIndexOf('/');
        this.txtboxWCViewComponent.Text = this.txtboxWCViewComponent.Text.Remove(num + 1, this.txtboxWCViewComponent.Text.Length - (num + 1)) + this.txtboxOWLWCName.Text + ".WCVIEW.uiwocview";
      }
    }

    private void OnShortIdChanged(object sender, RoutedEventArgs e)
    {
      if (this.m_MyTasksWCViewDetails == null)
        return;
      if (new Regex("^[_a-zA-Z0-9]*$").IsMatch(this.txtboxShortId.Text))
      {
        if (this.m_MyTasksWCViewDetails.Count <= 3)
          this.m_MyTasksWCViewDetails.Add(this.txtboxShortId.Text);
        if (this.m_MyTasksWCViewDetails[2] == "" || this.m_MyTasksWCViewDetails[2] == "$TMP" && !this.txtboxShortId.Text.StartsWith("Z"))
          this.m_MyTasksWCViewDetails[3] = "Z" + this.txtboxShortId.Text;
        else
          this.m_MyTasksWCViewDetails[3] = this.txtboxShortId.Text;
      }
      else
      {
        int num = (int) DisplayMessage.Show("Special charaters are not allowed other than Underscore", MessageBoxButtons.OK);
        this.txtboxShortId.Text = this.txtboxShortId.Text.Remove(this.txtboxShortId.Text.Length - 1, 1);
      }
    }

    public void Reload()
    {
      this.SetOWLComponentDetails();
      this.SetWCViewComponentDetails();
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/designers/workcenterarea/wcfwizardstep/mytasksinfo.xaml", UriKind.Relative));
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.groupBoxMyTasks = (System.Windows.Controls.GroupBox) target;
    //      break;
    //    case 2:
    //      this.stackPanelMyTasks = (StackPanel) target;
    //      break;
    //    case 3:
    //      this.lblEC = (TextBlock) target;
    //      break;
    //    case 4:
    //      this.ECSelection = (StackPanel) target;
    //      break;
    //    case 5:
    //      this.lblName = (TextBlock) target;
    //      break;
    //    case 6:
    //      this.txtboxOWLName = (System.Windows.Controls.TextBox) target;
    //      this.txtboxOWLName.TextChanged += new TextChangedEventHandler(this.OnOWLNameChanged);
    //      break;
    //    case 7:
    //      this.txtboxOWLComponent = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 8:
    //      this.btnBrowse2 = (System.Windows.Controls.Button) target;
    //      this.btnBrowse2.Click += new RoutedEventHandler(this.OpenMyTasksFolderSelector);
    //      break;
    //    case 9:
    //      this.fileSelector = (System.Windows.Controls.Button) target;
    //      this.fileSelector.Click += new RoutedEventHandler(this.OpenMyTasksFileSelector);
    //      break;
    //    case 10:
    //      this.lblWCView = (TextBlock) target;
    //      break;
    //    case 11:
    //      this.WCViewSelection = (StackPanel) target;
    //      break;
    //    case 12:
    //      this.lblWCName = (TextBlock) target;
    //      break;
    //    case 13:
    //      this.txtboxOWLWCName = (System.Windows.Controls.TextBox) target;
    //      this.txtboxOWLWCName.TextChanged += new TextChangedEventHandler(this.OnWCViewNameChanged);
    //      break;
    //    case 14:
    //      this.txtboxWCViewComponent = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 15:
    //      this.btnBrowse3 = (System.Windows.Controls.Button) target;
    //      this.btnBrowse3.Click += new RoutedEventHandler(this.OpenMyTasksWCViewFolderSelector);
    //      break;
    //    case 16:
    //      this.shortidstack = (StackPanel) target;
    //      break;
    //    case 17:
    //      this.lblShortId = (TextBlock) target;
    //      break;
    //    case 18:
    //      this.txtboxShortId = (System.Windows.Controls.TextBox) target;
    //      this.txtboxShortId.TextChanged += new TextChangedEventHandler(this.OnShortIdChanged);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
