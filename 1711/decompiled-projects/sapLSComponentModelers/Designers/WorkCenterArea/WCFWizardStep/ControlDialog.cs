﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep.ControlDialog
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep
{
  public class ControlDialog : Form
  {
    private const string WCVIEW_UICOMPONENT_EXTENSION = ".WCVIEW.uiwocview";
    private const string EMBEDDED_UICOMPONENT = ".EC.uicomponent";
    private const string OWL_UICOMPONENT_EXTENSION = ".OWL.uicomponent";
    public OverviewInfo m_OInfo;
    public MyTasksInfo m_MyTasksInfo;
    private System.ComponentModel.IContainer components;
    private TableLayoutPanel tableLayoutPanel1;
    private Button btnOK;
    private Button btnCancel;
    private ElementHost elementHost1;

    public ControlDialog(bool IsOverview, IModelObject floorplan)
    {
      this.InitializeComponent();
      WorkCenterFloorplan workCenterFloorplan = floorplan as WorkCenterFloorplan;
      if (IsOverview)
      {
        OverviewInfo overviewInfo = new OverviewInfo();
        overviewInfo.VerticalAlignment = VerticalAlignment.Stretch;
        overviewInfo.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        overviewInfo.txtboxQLECName.Text = workCenterFloorplan.Name + "_QL_EC";
        overviewInfo.txtboxQLECComponent.Text = workCenterFloorplan.RepositoryPathValue + "/" + workCenterFloorplan.Name + "_QL_EC.EC.uicomponent";
        overviewInfo.txtboxQLWCName.Text = workCenterFloorplan.Name + "_Overview_WCView";
        overviewInfo.txtboxQLWCComponent.Text = workCenterFloorplan.RepositoryPathValue + "/" + workCenterFloorplan.Name + "_Overview_WCView.WCVIEW.uiwocview";
        this.elementHost1.Child = (UIElement) overviewInfo;
        this.m_OInfo = overviewInfo;
      }
      else
      {
        MyTasksInfo myTasksInfo = new MyTasksInfo();
        myTasksInfo.VerticalAlignment = VerticalAlignment.Stretch;
        myTasksInfo.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        myTasksInfo.txtboxOWLName.Text = workCenterFloorplan.Name + "_Work_OWL";
        myTasksInfo.txtboxOWLComponent.Text = workCenterFloorplan.RepositoryPathValue + "/" + workCenterFloorplan.Name + "_Work_OWL.OWL.uicomponent";
        myTasksInfo.txtboxOWLWCName.Text = workCenterFloorplan.Name + "_Work_WCView";
        myTasksInfo.txtboxWCViewComponent.Text = workCenterFloorplan.RepositoryPathValue + "/" + workCenterFloorplan.Name + "_Work_WCView.WCVIEW.uiwocview";
        this.elementHost1.Child = (UIElement) myTasksInfo;
        this.m_MyTasksInfo = myTasksInfo;
      }
    }

    public bool getTextdetails()
    {
      if (this.m_MyTasksInfo != null)
      {
        string text = this.m_MyTasksInfo.txtboxShortId.Text;
        if (!(text == string.Empty) && text != null)
          return true;
        int num = (int) DisplayMessage.Show("It is mandatory to have ShortId for WCView");
        return false;
      }
      if (this.m_OInfo == null)
        return false;
      string text1 = this.m_OInfo.txtboxShortId.Text;
      if (!(text1 == string.Empty) && text1 != null)
        return true;
      int num1 = (int) DisplayMessage.Show("It is mandatory to have ShortId for WCView");
      return false;
    }

    public void SetDialogResult(DialogResult result)
    {
      this.DialogResult = result;
      this.Close();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.tableLayoutPanel1 = new TableLayoutPanel();
      this.btnOK = new Button();
      this.btnCancel = new Button();
      this.elementHost1 = new ElementHost();
      this.tableLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 78f));
      this.tableLayoutPanel1.Controls.Add((Control) this.btnOK, 0, 1);
      this.tableLayoutPanel1.Controls.Add((Control) this.btnCancel, 1, 1);
      this.tableLayoutPanel1.Controls.Add((Control) this.elementHost1, 0, 0);
      this.tableLayoutPanel1.Dock = DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 29f));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(710, 216);
      this.tableLayoutPanel1.TabIndex = 0;
      this.btnOK.Anchor = AnchorStyles.Right;
      this.btnOK.DialogResult = DialogResult.OK;
      this.btnOK.Location = new System.Drawing.Point(554, 190);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 2;
      this.btnOK.Text = "OK";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnCancel.DialogResult = DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(635, 190);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(72, 23);
      this.btnCancel.TabIndex = 3;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.tableLayoutPanel1.SetColumnSpan((Control) this.elementHost1, 2);
      this.elementHost1.Dock = DockStyle.Fill;
      this.elementHost1.Location = new System.Drawing.Point(3, 3);
      this.elementHost1.Name = "elementHost1";
      this.elementHost1.Size = new System.Drawing.Size(704, 181);
      this.elementHost1.TabIndex = 4;
      this.elementHost1.Text = "elementHost1";
      this.elementHost1.Child = (UIElement) null;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(710, 216);
      this.Controls.Add((Control) this.tableLayoutPanel1);
      this.Name = nameof (ControlDialog);
      this.Text = "SelectionDialog";
      this.tableLayoutPanel1.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
