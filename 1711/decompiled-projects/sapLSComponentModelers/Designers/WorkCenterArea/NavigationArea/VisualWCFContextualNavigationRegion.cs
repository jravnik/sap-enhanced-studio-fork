﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea.VisualWCFContextualNavigationRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.WCFWizardStep;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.WorkCenterArea.NavigationArea
{
  public class VisualWCFContextualNavigationRegion : BaseSelectableControl
  {
    private List<ViewSwitchButton> buttonList = new List<ViewSwitchButton>();
    private TextBlock componentID = new TextBlock();
    internal const int CNR_WIDTH = 210;
    internal const string RESOURCE_BACKGROUND_TABAREA = "#E9F0F5,0.0,#EDF1F5,0.3,#FAFAFB,1.0";
    internal const string DEFAULT_ADDIN_CONTENTINFO = "WorkCenterView Path: ";
    internal const string VIEW = "View";
    internal const string SUBVIEW = "SubView";
    private const string QUICKLINKCOMPONENTPATH = "/SAP_BYD_UI_CT/Overview/basicquicklinks.EC.uicomponent";
    private Canvas gridCanvas;
    private ViewSwitchButton selectedButton;
    private ViewSwitchButton viewBottomButton;
    private SubViewSwitchFrame subViewSwitchFrame;

    public event ViewSelected OnViewSelected;

    public VisualWCFContextualNavigationRegion(IModelObject model)
      : base(model)
    {
      this.Margin = new Thickness(0.0);
      this.AddContextNavigationRegion();
    }

    internal OberonCenterStructure CenterStructure
    {
      get
      {
        return this.m_ModelObject as OberonCenterStructure;
      }
    }

    internal IViewSwitch selectedView
    {
      get
      {
        if (this.selectedButton != null)
          return this.selectedButton.Tag as IViewSwitch;
        return (IViewSwitch) null;
      }
    }

    private void AddContextNavigationRegion()
    {
      if (this.gridCanvas == null)
        this.gridCanvas = new Canvas();
      this.gridCanvas.Background = (Brush) ResourceUtil.GetLinearGradientBrush("#E9F0F5,0.0,#EDF1F5,0.3,#FAFAFB,1.0");
      int index1 = 0;
      double num1 = 0.0;
      ViewSwitchTopLine viewSwitchTopLine1 = new ViewSwitchTopLine();
      viewSwitchTopLine1.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      viewSwitchTopLine1.Height = 1.0;
      viewSwitchTopLine1.Width = 210.0;
      double num2 = num1 + viewSwitchTopLine1.Height;
      viewSwitchTopLine1.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.gridCanvas.Children.Add((UIElement) viewSwitchTopLine1);
      Canvas.SetTop((UIElement) viewSwitchTopLine1, 0.0);
      ViewSwitchButton viewSwitchButton = new ViewSwitchButton((IViewSwitch) null, index1);
      viewSwitchButton.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      viewSwitchButton.VerticalAlignment = VerticalAlignment.Stretch;
      viewSwitchButton.Height = 25.0;
      double num3 = num2 + viewSwitchButton.Height;
      viewSwitchButton.Width = 210.0;
      viewSwitchButton.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.gridCanvas.Children.Add((UIElement) viewSwitchButton);
      Canvas.SetTop((UIElement) viewSwitchButton, 1.0);
      int index2 = index1 + 1;
      this.buttonList.Clear();
      foreach (IViewSwitch viewSwitch in this.CenterStructure.ViewSwitches)
      {
        ViewSwitchTopLine viewSwitchTopLine2 = new ViewSwitchTopLine();
        viewSwitchTopLine2.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        viewSwitchTopLine2.Height = 1.0;
        num3 += viewSwitchTopLine2.Height;
        viewSwitchTopLine2.Width = 210.0;
        viewSwitchTopLine2.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        this.gridCanvas.Children.Add((UIElement) viewSwitchTopLine2);
        if (index2 == 0)
          Canvas.SetTop((UIElement) viewSwitchTopLine2, 0.0);
        else
          Canvas.SetTop((UIElement) viewSwitchTopLine2, (double) (index2 * 25 + index2));
        ViewSwitchButton viewButton = new ViewSwitchButton(viewSwitch, index2);
        viewButton.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        viewButton.ButtonClicked += new EventHandler<EventArgs>(this.viewButton_ButtonClicked);
        viewButton.Height = 25.0;
        num3 += viewSwitchTopLine2.Height;
        viewButton.Width = 210.0;
        viewButton.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        VisualViewSwitch visualViewSwitch = new VisualViewSwitch((IModelObject) viewSwitch);
        visualViewSwitch.Grid.RowDefinitions.Add(new RowDefinition()
        {
          Height = new GridLength(1.0, GridUnitType.Pixel)
        });
        visualViewSwitch.Grid.ColumnDefinitions.Add(new ColumnDefinition()
        {
          Width = new GridLength(1.0, GridUnitType.Pixel)
        });
        visualViewSwitch.AddViewButton(viewButton);
        visualViewSwitch.SubViewRegionChanged += new EventHandler(this.viewSwitch_SubViewRegionChanged);
        this.gridCanvas.Children.Add((UIElement) visualViewSwitch);
        this.buttonList.Add(viewButton);
        viewButton.Tag = (object) viewSwitch;
        if (index2 == 1)
        {
          this.selectedButton = viewButton;
          this.selectedButton.Selected = true;
        }
        if (index2 == 0)
          Canvas.SetTop((UIElement) visualViewSwitch, 1.0);
        else
          Canvas.SetTop((UIElement) visualViewSwitch, (double) (1 + index2 * 25 + index2));
        ++index2;
      }
      this.AddSubView();
      ViewSwitchTopLine viewSwitchTopLine3 = new ViewSwitchTopLine();
      viewSwitchTopLine3.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      viewSwitchTopLine3.Height = 1.0;
      double num4 = num3 + viewSwitchTopLine3.Height;
      viewSwitchTopLine3.Width = 210.0;
      viewSwitchTopLine3.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.gridCanvas.Children.Add((UIElement) viewSwitchTopLine3);
      if (index2 == 0)
        Canvas.SetTop((UIElement) viewSwitchTopLine3, 0.0);
      else
        Canvas.SetTop((UIElement) viewSwitchTopLine3, (double) (index2 * 25 + index2) + (this.subViewSwitchFrame != null ? this.subViewSwitchFrame.Height : 0.0));
      this.viewBottomButton = new ViewSwitchButton((IViewSwitch) null, index2);
      this.viewBottomButton.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      this.viewBottomButton.VerticalAlignment = VerticalAlignment.Stretch;
      this.viewBottomButton.Height = 1000.0;
      this.viewBottomButton.Width = 210.0;
      this.viewBottomButton.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.gridCanvas.Children.Add((UIElement) this.viewBottomButton);
      if (index2 == 0)
        Canvas.SetTop((UIElement) this.viewBottomButton, 0.0);
      else
        Canvas.SetTop((UIElement) this.viewBottomButton, (double) (1 + index2 * 25 + index2) + (this.subViewSwitchFrame != null ? this.subViewSwitchFrame.Height : 0.0));
      this.Content = (object) this.gridCanvas;
      this.InvalidateMeasure();
    }

    protected override Size MeasureOverride(Size constraint)
    {
      Size size = new Size();
      foreach (UIElement child in this.gridCanvas.Children)
      {
        double left = Canvas.GetLeft(child);
        double top = Canvas.GetTop(child);
        double num1 = double.IsNaN(left) ? 0.0 : left;
        double num2 = double.IsNaN(top) ? 0.0 : top;
        child.Measure(constraint);
        Size desiredSize = child.DesiredSize;
        if (!double.IsNaN(desiredSize.Width) && !double.IsNaN(desiredSize.Height))
        {
          size.Width = Math.Max(size.Width, num1 + desiredSize.Width);
          size.Height = Math.Max(size.Height, num2 + desiredSize.Height);
        }
      }
      if (!this.gridCanvas.Children.Contains((UIElement) this.subViewSwitchFrame))
        size.Height -= 1000.0;
      else if (this.subViewSwitchFrame.Height < 1000.0)
        size.Height -= 1000.0 - this.subViewSwitchFrame.Height;
      return size;
    }

    private void viewSwitch_SubViewRegionChanged(object sender, EventArgs e)
    {
      this.AddSubView();
    }

    private void AddSubView()
    {
      int num = this.buttonList.Count + 1;
      if (this.selectedView != null && this.selectedView.SubViewSwitches != null && this.selectedView.SubViewSwitches.Count > 0)
      {
        if (this.subViewSwitchFrame != null)
        {
          if (!this.gridCanvas.Children.Contains((UIElement) this.subViewSwitchFrame))
            this.gridCanvas.Children.Add((UIElement) this.subViewSwitchFrame);
          else
            this.gridCanvas.Children.Remove((UIElement) this.subViewSwitchFrame);
        }
        if (this.selectedView != null)
        {
          this.subViewSwitchFrame = new SubViewSwitchFrame((IModelObject) this.selectedView);
          this.subViewSwitchFrame.OnSubViewSelected += new ViewSelected(this.subViewSwitchFrame_OnViewSelected);
        }
        if (num == 0)
          Canvas.SetTop((UIElement) this.subViewSwitchFrame, 0.0);
        else
          Canvas.SetTop((UIElement) this.subViewSwitchFrame, (double) (num * 25 + num));
        if (!this.gridCanvas.Children.Contains((UIElement) this.subViewSwitchFrame))
          this.gridCanvas.Children.Add((UIElement) this.subViewSwitchFrame);
      }
      else
      {
        if (this.gridCanvas.Children.Contains((UIElement) this.subViewSwitchFrame))
          this.gridCanvas.Children.Remove((UIElement) this.subViewSwitchFrame);
        if (this.viewBottomButton != null)
        {
          if (num == 0)
            Canvas.SetTop((UIElement) this.viewBottomButton, 0.0);
          else
            Canvas.SetTop((UIElement) this.viewBottomButton, (double) (num * 25 + num));
        }
        this.subViewSwitchFrame = (SubViewSwitchFrame) null;
      }
      this.InvalidateMeasure();
    }

    private void viewButton_ButtonClicked(object sender, EventArgs e)
    {
      ViewSwitchButton viewSwitchButton = sender as ViewSwitchButton;
      this.selectedButton = viewSwitchButton;
      if (viewSwitchButton == null)
        return;
      foreach (ViewSwitchButton button in this.buttonList)
        button.Selected = button == viewSwitchButton;
      if (this.OnViewSelected != null)
        this.OnViewSelected(viewSwitchButton.Tag as IViewSwitch);
      object tag = viewSwitchButton.Tag;
      this.AddSubView();
    }

    public override string SelectionText
    {
      get
      {
        return "ContextualNavigationRegion";
      }
    }

    public override void SelectMe()
    {
    }

    public override void UnSelect()
    {
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      bool flag1 = false;
      bool flag2 = false;
      bool flag3 = false;
      System.Windows.Controls.MenuItem parentItem = this.AddContextMenuItem("Add");
      OberonCenterStructure modelObject = this.m_ModelObject as OberonCenterStructure;
      if (modelObject == null)
        return;
      foreach (OberonViewSwitch viewSwitch in modelObject.ViewSwitches)
      {
        if (viewSwitch.ViewSwitchTypes == ViewSwitchTypes.Overview)
          flag1 = true;
        else if (viewSwitch.ViewSwitchTypes == ViewSwitchTypes.MyTasks)
          flag2 = true;
        else if (viewSwitch.ViewSwitchTypes == ViewSwitchTypes.Reports)
          flag3 = true;
      }
      if (!flag1)
        this.AddContextMenuItem("Overview", parentItem);
      if (!flag2)
        this.AddContextMenuItem("My Tasks", parentItem);
      this.AddContextMenuItem("WorkCenterView", parentItem);
      if (flag3)
        return;
      this.AddContextMenuItem("Reports", parentItem);
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      OberonCenterStructure modelObject = this.m_ModelObject as OberonCenterStructure;
      WorkCenterFloorplan parent = modelObject.Parent as WorkCenterFloorplan;
      if (modelObject == null)
        return;
      if (Convert.ToString(source.Header).Equals("Overview"))
      {
        if (DisplayMessage.Show("Do you want to Auto create the Quick links components", MessageBoxButtons.YesNo) == DialogResult.Yes)
        {
          ControlDialog controlDialog = new ControlDialog(true, (IModelObject) parent);
          if (controlDialog.ShowDialog() != DialogResult.OK)
            return;
          modelObject.AddOverview(controlDialog.m_OInfo.m_OverviewECDetails, controlDialog.m_OInfo.m_OverviewWCViewDetails, controlDialog.m_OInfo.IsExistingFile);
        }
        else
          modelObject.AddOverview();
      }
      else if (Convert.ToString(source.Header).Equals("My Tasks"))
      {
        if (DisplayMessage.Show("Do you want to Auto create the OWL MyTasks component", MessageBoxButtons.YesNo) == DialogResult.Yes)
        {
          ControlDialog controlDialog = new ControlDialog(false, (IModelObject) parent);
          if (controlDialog.ShowDialog() != DialogResult.OK)
            return;
          modelObject.AddMyTasks(controlDialog.m_MyTasksInfo.m_MyTasksOWLDetails, controlDialog.m_MyTasksInfo.m_MyTasksWCViewDetails, controlDialog.m_MyTasksInfo.IsExistingFile);
        }
        else
          modelObject.AddMyTasks();
      }
      else if (Convert.ToString(source.Header).Equals("WorkCenterView"))
      {
        modelObject.AddWorkCenterView();
      }
      else
      {
        if (!Convert.ToString(source.Header).Equals("Reports"))
          return;
        modelObject.AddReports();
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      this.AddContextNavigationRegion();
      base.OnModelObjectAdded(sender, e);
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      this.AddContextNavigationRegion();
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      this.AddContextNavigationRegion();
    }

    protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
    {
    }

    protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
    {
    }

    private void subViewSwitchFrame_OnViewSelected(IViewSwitch viewSelected)
    {
      if (this.OnViewSelected == null)
        return;
      this.OnViewSelected(viewSelected);
    }
  }
}
