﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.MashupCategoryDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class MashupCategoryDesigner : ComponentDesigner, ISelectionProvider
  {
    private List<KeyValuePair<string, string>> SemanticCategoryList = new List<KeyValuePair<string, string>>()
    {
      new KeyValuePair<string, string>("Business & Financial", "BusinessFinance"),
      new KeyValuePair<string, string>("Location & Travel", "LocationTravel"),
      new KeyValuePair<string, string>("News & Reference", "NewsReference"),
      new KeyValuePair<string, string>("Productivity & Tools", "ProductivityTools"),
      new KeyValuePair<string, string>("Social & Communication", "SocialCommunication"),
      new KeyValuePair<string, string>("Application Integration", "ApplicationIntegration")
    };
    private Dictionary<string, string> SemanticCategoryDic = new Dictionary<string, string>()
    {
      {
        "BusinessFinance",
        "Business & Financial"
      },
      {
        "LocationTravel",
        "Location & Travel"
      },
      {
        "NewsReference",
        "News & Reference"
      },
      {
        "ProductivityTools",
        "Productivity & Tools"
      },
      {
        "SocialCommunication",
        "Social & Communication"
      },
      {
        "ApplicationIntegration",
        "Application Integration"
      }
    };
    private System.Windows.Forms.WebBrowser webBowser;
    private System.Windows.Controls.ComboBox inPortTypeReferenceTB;
    private System.Windows.Controls.ComboBox outPortTypeReferenceTB;
    private DisplayTextControl title;
    private bool m_DisplayOnly;
    private bool firstTime;
    private System.ComponentModel.IContainer components;

    internal MashupCategoryComponent OberonModelObject
    {
      get
      {
        return this.m_ModelObject as MashupCategoryComponent;
      }
    }

    private bool DisplayOnly
    {
      get
      {
        this.m_DisplayOnly = !ValidationManager.GetInstance().IsComponentEditable(this.m_ModelObject, false);
        return this.m_DisplayOnly;
      }
    }

    public MashupCategoryDesigner(IModelObject rootModel)
      : base(rootModel, true, true)
    {
    }

    public override void LoadView()
    {
      base.LoadView();
      this.m_DisplayOnly = !ValidationManager.GetInstance().IsComponentEditable(this.m_ModelObject, false);
      this.firstTime = true;
      this.OberonModelObject.ImplementationController.UXControllerTypeImplementation.MashupModel.MashupRegistry.MashupCategories.MashupCategory[0].name = this.OberonModelObject.Name;
      this.AddTitle();
      this.AddContent();
      this.firstTime = false;
    }

    private void AddTitle()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      this.title = new DisplayTextControl(TextViewStyles.PageHeaderTitle, this.OberonModelObject.Name);
      this.title.Margin = new Thickness(5.0, 5.0, 5.0, 5.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.title, 0, 0);
    }

    private void AddContent()
    {
      try
      {
        List<MashupCategoryType> mashupCategory = this.OberonModelObject.ImplementationController.UXControllerTypeImplementation.MashupModel.MashupRegistry.MashupCategories.MashupCategory;
        List<TextPoolEntryType> textPoolEntry = this.OberonModelObject.TextPoolController.TextPool[0].TextPoolEntry;
        if (mashupCategory.Count <= 0 || textPoolEntry.Count <= 0)
          return;
        string name = mashupCategory[0].name;
        string semanticCategory = mashupCategory[0].semanticCategory;
        string outPortTypePackage = mashupCategory[0].outPortTypePackage;
        string portTypeReference1 = mashupCategory[0].outPortTypeReference;
        string inPortTypePackage = mashupCategory[0].inPortTypePackage;
        string portTypeReference2 = mashupCategory[0].inPortTypeReference;
        string text1 = textPoolEntry.Where<TextPoolEntryType>((Func<TextPoolEntryType, bool>) (entry => entry.textUuid.Equals("displayName"))).FirstOrDefault<TextPoolEntryType>().text;
        string text2 = textPoolEntry.Where<TextPoolEntryType>((Func<TextPoolEntryType, bool>) (entry => entry.textUuid.Equals("description"))).FirstOrDefault<TextPoolEntryType>().text;
        List<string> portTypePackages = this.GetPortTypePackages();
        List<string> stringList = new List<string>((IEnumerable<string>) portTypePackages);
        TextBlock textBlock1 = new TextBlock();
        textBlock1.Text = "Display Name:";
        System.Windows.Controls.TextBox textBox1 = new System.Windows.Controls.TextBox();
        textBox1.Text = text1;
        textBox1.Margin = new Thickness(0.0, 0.0, 0.0, 20.0);
        textBox1.TextChanged += new TextChangedEventHandler(this.categoryNameTB_TextChanged);
        textBox1.MinWidth = 200.0;
        TextBlock textBlock2 = new TextBlock();
        textBlock2.Text = "Description:";
        System.Windows.Controls.TextBox textBox2 = new System.Windows.Controls.TextBox();
        textBox2.Text = text2;
        textBox2.Margin = new Thickness(0.0, 0.0, 0.0, 20.0);
        textBox2.TextChanged += new TextChangedEventHandler(this.descriptionTB_TextChanged);
        TextBlock textBlock3 = new TextBlock();
        textBlock3.Text = "Category:";
        TextBlock textBlock4 = new TextBlock();
        textBlock4.Text = "*";
        textBlock4.FontFamily = new FontFamily("Verdana");
        textBlock4.Foreground = (Brush) Brushes.Red;
        StackPanel stackPanel1 = new StackPanel();
        stackPanel1.Children.Add((UIElement) textBlock3);
        stackPanel1.Children.Add((UIElement) textBlock4);
        stackPanel1.Orientation = System.Windows.Controls.Orientation.Horizontal;
        System.Windows.Controls.ComboBox comboBox1 = new System.Windows.Controls.ComboBox();
        comboBox1.ItemsSource = (IEnumerable) this.SemanticCategoryList;
        comboBox1.DisplayMemberPath = "Key";
        comboBox1.Margin = new Thickness(0.0, 0.0, 0.0, 20.0);
        if (!string.IsNullOrEmpty(semanticCategory))
        {
          if (this.SemanticCategoryDic.ContainsKey(semanticCategory))
            comboBox1.Text = this.SemanticCategoryDic[semanticCategory];
        }
        else
          comboBox1.SelectedIndex = 0;
        comboBox1.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.sematicCategoryTB_SelectionChanged);
        TextBlock textBlock5 = new TextBlock();
        textBlock5.Text = "InPort Type Package:";
        System.Windows.Controls.ComboBox comboBox2 = new System.Windows.Controls.ComboBox();
        this.inPortTypeReferenceTB = new System.Windows.Controls.ComboBox();
        comboBox2.ItemsSource = (IEnumerable) portTypePackages;
        comboBox2.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.inPortTypePackageTB_SelectionChanged);
        comboBox2.Text = inPortTypePackage;
        comboBox2.Margin = new Thickness(0.0, 0.0, 0.0, 20.0);
        TextBlock textBlock6 = new TextBlock();
        textBlock6.Text = "*";
        textBlock6.FontFamily = new FontFamily("Verdana");
        textBlock6.Foreground = (Brush) Brushes.Red;
        StackPanel stackPanel2 = new StackPanel();
        stackPanel2.Children.Add((UIElement) textBlock5);
        stackPanel2.Children.Add((UIElement) textBlock6);
        stackPanel2.Orientation = System.Windows.Controls.Orientation.Horizontal;
        TextBlock textBlock7 = new TextBlock();
        textBlock7.Text = "InPort Type Reference:";
        this.inPortTypeReferenceTB.Margin = new Thickness(0.0, 0.0, 0.0, 20.0);
        this.inPortTypeReferenceTB.Text = portTypeReference2;
        this.inPortTypeReferenceTB.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.inPortTypeReferenceTB_SelectionChanged);
        TextBlock textBlock8 = new TextBlock();
        textBlock8.Text = "*";
        textBlock8.FontFamily = new FontFamily("Verdana");
        textBlock8.Foreground = (Brush) Brushes.Red;
        StackPanel stackPanel3 = new StackPanel();
        stackPanel3.Children.Add((UIElement) textBlock7);
        stackPanel3.Children.Add((UIElement) textBlock8);
        stackPanel3.Orientation = System.Windows.Controls.Orientation.Horizontal;
        if (string.IsNullOrEmpty(inPortTypePackage))
          comboBox2.SelectedIndex = 0;
        TextBlock textBlock9 = new TextBlock();
        textBlock9.Text = "OutPort Type Package:";
        System.Windows.Controls.ComboBox comboBox3 = new System.Windows.Controls.ComboBox();
        this.outPortTypeReferenceTB = new System.Windows.Controls.ComboBox();
        stringList.Insert(0, string.Empty);
        comboBox3.ItemsSource = (IEnumerable) stringList;
        comboBox3.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.outPortTypePackageTB_SelectionChanged);
        comboBox3.Text = outPortTypePackage;
        comboBox3.Margin = new Thickness(0.0, 0.0, 0.0, 20.0);
        TextBlock textBlock10 = new TextBlock();
        textBlock10.Text = "OutPort Type Reference:";
        this.outPortTypeReferenceTB.Margin = new Thickness(0.0, 0.0, 0.0, 20.0);
        this.outPortTypeReferenceTB.Text = portTypeReference1;
        this.outPortTypeReferenceTB.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.outPortTypeReferenceTB_SelectionChanged);
        Grid grid = new Grid();
        GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
        GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
        GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
        GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
        GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
        GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
        GridUtil.AddRowDef(grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(grid, (UIElement) textBlock1, 0, 0);
        GridUtil.PlaceElement(grid, (UIElement) textBox1, 0, 1);
        GridUtil.PlaceElement(grid, (UIElement) textBlock2, 1, 0);
        GridUtil.PlaceElement(grid, (UIElement) textBox2, 1, 1);
        GridUtil.PlaceElement(grid, (UIElement) stackPanel1, 2, 0);
        GridUtil.PlaceElement(grid, (UIElement) comboBox1, 2, 1);
        GridUtil.PlaceElement(grid, (UIElement) stackPanel2, 3, 0);
        GridUtil.PlaceElement(grid, (UIElement) comboBox2, 3, 1);
        GridUtil.PlaceElement(grid, (UIElement) stackPanel3, 4, 0);
        GridUtil.PlaceElement(grid, (UIElement) this.inPortTypeReferenceTB, 4, 1);
        GridUtil.PlaceElement(grid, (UIElement) textBlock9, 5, 0);
        GridUtil.PlaceElement(grid, (UIElement) comboBox3, 5, 1);
        GridUtil.PlaceElement(grid, (UIElement) textBlock10, 6, 0);
        GridUtil.PlaceElement(grid, (UIElement) this.outPortTypeReferenceTB, 6, 1);
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) grid, 1, 0);
        if (!this.DisplayOnly)
          return;
        textBox1.IsEnabled = false;
        textBox2.IsEnabled = false;
        comboBox1.IsEnabled = false;
        comboBox2.IsEnabled = false;
        this.inPortTypeReferenceTB.IsEnabled = false;
        comboBox3.IsEnabled = false;
        this.outPortTypeReferenceTB.IsEnabled = false;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    private void sematicCategoryTB_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.OberonModelObject.ImplementationController.UXControllerTypeImplementation.MashupModel.MashupRegistry.MashupCategories.MashupCategory[0].semanticCategory = ((KeyValuePair<string, string>) (sender as System.Windows.Controls.ComboBox).SelectedItem).Value;
      if (this.firstTime)
        return;
      this.OberonModelObject.RaisePropertyChanged((string) null, (object) null, (object) null);
    }

    private void outPortTypeReferenceTB_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.OberonModelObject.ImplementationController.UXControllerTypeImplementation.MashupModel.MashupRegistry.MashupCategories.MashupCategory[0].outPortTypeReference = (sender as System.Windows.Controls.ComboBox).SelectedItem as string;
      if (this.firstTime)
        return;
      this.OberonModelObject.RaisePropertyChanged((string) null, (object) null, (object) null);
    }

    private void inPortTypeReferenceTB_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.OberonModelObject.ImplementationController.UXControllerTypeImplementation.MashupModel.MashupRegistry.MashupCategories.MashupCategory[0].inPortTypeReference = (sender as System.Windows.Controls.ComboBox).SelectedItem as string;
      if (this.firstTime)
        return;
      this.OberonModelObject.RaisePropertyChanged((string) null, (object) null, (object) null);
    }

    private void categoryNameTB_TextChanged(object sender, TextChangedEventArgs e)
    {
      this.OberonModelObject.TextPoolController.TextPool[0].TextPoolEntry[0].text = (sender as System.Windows.Controls.TextBox).Text;
      if (this.firstTime)
        return;
      this.OberonModelObject.RaisePropertyChanged((string) null, (object) null, (object) null);
    }

    private void descriptionTB_TextChanged(object sender, TextChangedEventArgs e)
    {
      this.OberonModelObject.TextPoolController.TextPool[0].TextPoolEntry[1].text = (sender as System.Windows.Controls.TextBox).Text;
      if (this.firstTime)
        return;
      this.OberonModelObject.RaisePropertyChanged((string) null, (object) null, (object) null);
    }

    private void outPortTypePackageTB_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      System.Windows.Controls.ComboBox comboBox = sender as System.Windows.Controls.ComboBox;
      if (comboBox == null)
        return;
      string selectedItem = comboBox.SelectedItem as string;
      if (!string.IsNullOrEmpty(selectedItem))
      {
        this.OberonModelObject.ImplementationController.UXControllerTypeImplementation.MashupModel.MashupRegistry.MashupCategories.MashupCategory[0].outPortTypePackage = selectedItem;
        List<RepositoryAttribute> Attributes;
        ContentChangeability changeability;
        string s = XRepositoryProxy.Instance.ReadXRepContentAsString(selectedItem, out Attributes, out changeability, (List<RepositoryAttribute>) null);
        if (s.Length > 0)
        {
          UXComponent uxComponent;
          using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(s)))
          {
            uxComponent = new UXComponentSerializer().Deserialize((Stream) memoryStream) as UXComponent;
            memoryStream.Close();
            memoryStream.Dispose();
          }
          if (uxComponent != null && uxComponent.Interface != null && (uxComponent.Interface.PortTypes != null && uxComponent.Interface.PortTypes.PortType != null) && uxComponent.Interface.PortTypes.PortType.Count > 0)
          {
            List<UXPortType> portType = uxComponent.Interface.PortTypes.PortType;
            List<string> stringList = new List<string>();
            foreach (NamedModelEntity namedModelEntity in portType)
            {
              string name = namedModelEntity.name;
              stringList.Add(name);
            }
            this.outPortTypeReferenceTB.ItemsSource = (IEnumerable) stringList;
            this.outPortTypeReferenceTB.SelectedIndex = 0;
          }
        }
      }
      else
        this.outPortTypeReferenceTB.ItemsSource = (IEnumerable) null;
      if (this.firstTime)
        return;
      this.OberonModelObject.RaisePropertyChanged((string) null, (object) null, (object) null);
    }

    private void inPortTypePackageTB_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      System.Windows.Controls.ComboBox comboBox = sender as System.Windows.Controls.ComboBox;
      if (comboBox == null)
        return;
      string selectedItem = comboBox.SelectedItem as string;
      if (string.IsNullOrEmpty(selectedItem))
        return;
      this.OberonModelObject.ImplementationController.UXControllerTypeImplementation.MashupModel.MashupRegistry.MashupCategories.MashupCategory[0].inPortTypePackage = selectedItem;
      List<RepositoryAttribute> Attributes;
      ContentChangeability changeability;
      string s = XRepositoryProxy.Instance.ReadXRepContentAsString(selectedItem, out Attributes, out changeability, (List<RepositoryAttribute>) null);
      if (s.Length > 0)
      {
        UXComponent uxComponent;
        using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(s)))
        {
          uxComponent = new UXComponentSerializer().Deserialize((Stream) memoryStream) as UXComponent;
          memoryStream.Close();
          memoryStream.Dispose();
        }
        if (uxComponent != null && uxComponent.Interface != null && (uxComponent.Interface.PortTypes != null && uxComponent.Interface.PortTypes.PortType != null) && uxComponent.Interface.PortTypes.PortType.Count > 0)
        {
          List<UXPortType> portType = uxComponent.Interface.PortTypes.PortType;
          List<string> stringList = new List<string>();
          foreach (NamedModelEntity namedModelEntity in portType)
          {
            string name = namedModelEntity.name;
            stringList.Add(name);
          }
          this.inPortTypeReferenceTB.ItemsSource = (IEnumerable) stringList;
          this.inPortTypeReferenceTB.SelectedIndex = 0;
        }
      }
      if (this.firstTime)
        return;
      this.OberonModelObject.RaisePropertyChanged((string) null, (object) null, (object) null);
    }

    private List<string> GetPortTypePackages()
    {
      List<RepositoryElement> repositoryElementList = XRepositoryProxy.Instance.QueryFolderDeep("/", new List<RepositoryAttributeFilter>()
      {
        new RepositoryAttributeFilter()
        {
          Name = "~ENTITY_TYPE",
          Sign = "I",
          Option = "EQ",
          Low = "uicomponent"
        },
        new RepositoryAttributeFilter()
        {
          Name = "COMPONENTTYPE",
          Sign = "I",
          Option = "EQ",
          Low = "PTP"
        }
      });
      List<string> stringList = new List<string>();
      Regex regex = new Regex(LoginManager.Instance.DTMode != DTModes.Developer ? "^.*/SRC/.+\\.PTP\\.uicomponent$" : "^.*PTP\\.uicomponent$");
      foreach (RepositoryElement repositoryElement in repositoryElementList)
      {
        if (regex.IsMatch(repositoryElement.AbsolutePath))
          stringList.Add("/" + repositoryElement.AbsolutePath);
      }
      return stringList;
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (!(e.PropertyName == "Name") || e.NewValue == null)
        return;
      this.title.Text = e.NewValue as string;
      this.OberonModelObject.ImplementationController.UXControllerTypeImplementation.MashupModel.MashupRegistry.MashupCategories.MashupCategory[0].name = e.NewValue as string;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (System.ComponentModel.IContainer) new Container();
      this.AutoScaleMode = AutoScaleMode.Font;
    }
  }
}
