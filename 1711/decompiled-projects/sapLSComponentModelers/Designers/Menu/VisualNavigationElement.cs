﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu.VisualNavigationElement
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu
{
  public class VisualNavigationElement : BaseSelectableControl
  {
    private VisualLink m_LinkLabel;

    internal Button NavElement
    {
      get
      {
        return this.m_ModelObject as Button;
      }
    }

    public VisualNavigationElement(IModelObject navElementButton)
      : base(navElementButton)
    {
      this.m_LinkLabel = new VisualLink(VisualLink.LinkStyles.Toolbar);
      this.m_LinkLabel.Text = this.NavElement.Text.Text;
      this.m_LinkLabel.IsEnabled = Convert.ToBoolean(this.NavElement.Enabled.FallbackValue);
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.ControlBackground = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        this.m_LinkLabel.TextHorizontalAlignment = HorizontalAlignment.Center;
        this.m_LinkLabel.TextVerticalAlignment = VerticalAlignment.Bottom;
        this.m_LinkLabel.TextPadding = new Thickness(1.0, 8.0, 1.0, 2.0);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.m_LinkLabel, 0, 0);
        if (this.NavElement.Menu == null)
          return;
        int count = this.NavElement.Menu.NavigationItems.Count;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("You Can Also initialization failed.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (e.PropertyName == "Text")
        {
          this.m_LinkLabel.Text = this.NavElement.Text.Text;
        }
        else
        {
          if (!(e.PropertyName == "Enabled"))
            return;
          this.m_LinkLabel.IsEnabled = Convert.ToBoolean(this.NavElement.Enabled.FallbackValue);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update the property changes in the model.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Navigation Element";
      }
    }
  }
}
