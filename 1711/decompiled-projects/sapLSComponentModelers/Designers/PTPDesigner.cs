﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.PTPDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controller;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.Oberon.Controller;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class PTPDesigner : ComponentDesigner
  {
    private readonly string AddPortTypeMenu = "Add PortType";
    private readonly string AddParameterMenu = "Add Parameter";
    private readonly string AddListParameterMenu = "Add List Parameter";
    private readonly string DeleteMenu = "Delete";
    private readonly string RenameMenu = "Rename";
    private readonly string SetAsKeyMenu = "Set as Key";
    private readonly string ResetKeyMenu = "Reset Key";
    private System.Windows.Controls.TreeView portTypePackageTree;
    private TreeNodeWithIcon selectedItem;
    private bool m_DisplayOnly;

    internal bool DisplayOnly
    {
      get
      {
        this.m_DisplayOnly = !ValidationManager.GetInstance().IsComponentEditable(this.m_ModelObject, false);
        return this.m_DisplayOnly;
      }
    }

    internal PortTypePackage Component
    {
      get
      {
        return this.m_ModelObject as PortTypePackage;
      }
    }

    public PTPDesigner(IModelObject rootModel)
      : base(rootModel, true, false)
    {
      this.AllowDrop = true;
      this.m_TabWindow.Controls.Remove((System.Windows.Forms.Control) this.m_DataModelTab);
      this.m_TabWindow.Controls.Remove((System.Windows.Forms.Control) this.m_ControllerTab);
      this.m_TabWindow.Controls.Remove((System.Windows.Forms.Control) this.m_OberonPreviewTab);
    }

    public override void LoadView()
    {
      this.m_DisplayOnly = !ValidationManager.GetInstance().IsComponentEditable(this.m_ModelObject, false);
      base.LoadView();
      GridUtil.AddRowDef(this.Grid, 1.0, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, 1.0, GridUnitType.Star);
      this.LoadToolBar();
      this.Component.ControllerInterface.ModelObjectAdded -= new ModelAddedEventHandler(this.OnModelObjectAdded);
      this.Component.ControllerInterface.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
      this.Component.ControllerInterface.ModelObjectRemoved -= new ModelRemovedEventHandler(this.OnModelObjectRemoved);
      this.Component.ControllerInterface.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      DisplayTextControl displayTextControl = new DisplayTextControl(TextViewStyles.SectionGroupTitle, "Configure Port Types");
      displayTextControl.Padding = new Thickness(5.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) displayTextControl, 1, 0);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      this.portTypePackageTree = new System.Windows.Controls.TreeView();
      this.portTypePackageTree.BorderThickness = new Thickness(3.0);
      this.portTypePackageTree.ContextMenu = new System.Windows.Controls.ContextMenu();
      this.portTypePackageTree.ContextMenuOpening += new ContextMenuEventHandler(this.OnContextMenuOpening);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.portTypePackageTree, 2, 0);
      TreeNodeWithIcon treeNodeWithIcon1 = new TreeNodeWithIcon(this.Component.Name, SkinResource.PTP16x16);
      treeNodeWithIcon1.PreviewMouseDown += new MouseButtonEventHandler(this.OnMouseClick);
      foreach (IPortType portType in this.Component.ControllerInterface.PortTypes)
      {
        TreeNodeWithIcon treeNodeWithIcon2 = new TreeNodeWithIcon(portType.Name, Resource.Port_Type);
        treeNodeWithIcon2.PreviewMouseDown += new MouseButtonEventHandler(this.OnMouseClick);
        treeNodeWithIcon2.TextChanged += new DependencyPropertyChangedEventHandler(this.OnTextChanged);
        treeNodeWithIcon2.Tag = (object) portType;
        treeNodeWithIcon1.Items.Add((object) treeNodeWithIcon2);
        portType.ModelObjectAdded -= new ModelAddedEventHandler(this.OnModelObjectAdded);
        portType.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        portType.ModelObjectRemoved -= new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        portType.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        portType.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
        portType.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
        foreach (IParameter parameter in portType.Parameters)
        {
          TreeNodeWithIcon treeNodeWithIcon3 = !parameter.Key ? new TreeNodeWithIcon(parameter.Name, Resource.Port_Type_Parameter) : new TreeNodeWithIcon(parameter.Name, Resource.Key);
          treeNodeWithIcon3.Tag = (object) parameter;
          treeNodeWithIcon3.PreviewMouseDown += new MouseButtonEventHandler(this.OnMouseClick);
          treeNodeWithIcon3.TextChanged += new DependencyPropertyChangedEventHandler(this.OnTextChanged);
          treeNodeWithIcon2.Items.Add((object) treeNodeWithIcon3);
        }
        foreach (IListParameter listParameter in portType.ListParameters)
        {
          TreeNodeWithIcon treeNodeWithIcon3 = new TreeNodeWithIcon(listParameter.Name, Resource.ListParameter);
          treeNodeWithIcon3.Tag = (object) listParameter;
          treeNodeWithIcon3.PreviewMouseDown += new MouseButtonEventHandler(this.OnMouseClick);
          treeNodeWithIcon3.TextChanged += new DependencyPropertyChangedEventHandler(this.OnTextChanged);
          treeNodeWithIcon2.Items.Add((object) treeNodeWithIcon3);
          listParameter.ModelObjectAdded -= new ModelAddedEventHandler(this.OnModelObjectAdded);
          listParameter.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
          listParameter.ModelObjectRemoved -= new ModelRemovedEventHandler(this.OnModelObjectRemoved);
          listParameter.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
          listParameter.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
          listParameter.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
          foreach (IParameter parameter in listParameter.Parameters)
          {
            TreeNodeWithIcon treeNodeWithIcon4 = !parameter.Key ? new TreeNodeWithIcon(parameter.Name, Resource.Port_Type_Parameter) : new TreeNodeWithIcon(parameter.Name, Resource.Key);
            treeNodeWithIcon4.Tag = (object) parameter;
            treeNodeWithIcon4.PreviewMouseDown += new MouseButtonEventHandler(this.OnMouseClick);
            treeNodeWithIcon4.TextChanged += new DependencyPropertyChangedEventHandler(this.OnTextChanged);
            treeNodeWithIcon3.Items.Add((object) treeNodeWithIcon4);
          }
        }
      }
      this.portTypePackageTree.Items.Add((object) treeNodeWithIcon1);
      this.ExpandAll((TreeViewItem) treeNodeWithIcon1);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      base.OnModelObjectAdded(sender, e);
      INamedModelObject addedModel = e.AddedModel as INamedModelObject;
      TreeNodeWithIcon treeNodeWithIcon = (TreeNodeWithIcon) null;
      if (this.selectedItem == null || addedModel == null)
        return;
      if (addedModel is IPortType)
      {
        treeNodeWithIcon = new TreeNodeWithIcon(addedModel.Name, Resource.Port_Type);
        treeNodeWithIcon.Tag = (object) (addedModel as IPortType);
        addedModel.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        addedModel.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
      }
      else if (addedModel is IParameter)
      {
        treeNodeWithIcon = new TreeNodeWithIcon(addedModel.Name, Resource.Port_Type_Parameter);
        treeNodeWithIcon.Tag = (object) (addedModel as IParameter);
      }
      else if (addedModel is IListParameter)
      {
        treeNodeWithIcon = new TreeNodeWithIcon(addedModel.Name, Resource.ListParameter);
        treeNodeWithIcon.Tag = (object) (addedModel as IListParameter);
        addedModel.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        addedModel.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
      }
      treeNodeWithIcon.TextChanged += new DependencyPropertyChangedEventHandler(this.OnTextChanged);
      treeNodeWithIcon.PreviewMouseDown += new MouseButtonEventHandler(this.OnMouseClick);
      this.selectedItem.Items.Add((object) treeNodeWithIcon);
      this.ExpandAll((TreeViewItem) this.selectedItem);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      if (this.selectedItem == null || e.RemovedModel == null)
        return;
      TreeNodeWithIcon parent = this.selectedItem.Parent as TreeNodeWithIcon;
      parent.Items.Remove((object) this.selectedItem);
      this.selectedItem = parent;
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (!(e.PropertyName == "Name") || !(sender is PortTypePackage) || e.NewValue == null)
        return;
      ((TreeNodeWithIcon) this.portTypePackageTree.Items[0]).HeaderText = e.NewValue.ToString();
    }

    public void OnContextMenuOpening(object sender, ContextMenuEventArgs e)
    {
      this.selectedItem = !(e.Source is System.Windows.Controls.TextBlock) ? e.Source as TreeNodeWithIcon : ((FrameworkElement) ((FrameworkElement) e.Source).Parent).Parent as TreeNodeWithIcon;
      if (this.selectedItem != null && this.portTypePackageTree.ContextMenu != null)
      {
        this.portTypePackageTree.ContextMenu.Items.Clear();
        if (this.selectedItem.Tag is UXPort)
        {
          System.Windows.Controls.MenuItem menuItem1 = new System.Windows.Controls.MenuItem();
          menuItem1.Header = (object) this.AddParameterMenu;
          menuItem1.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
          this.portTypePackageTree.ContextMenu.Items.Add((object) menuItem1);
          System.Windows.Controls.MenuItem menuItem2 = new System.Windows.Controls.MenuItem();
          menuItem2.Header = (object) this.AddListParameterMenu;
          menuItem2.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
          this.portTypePackageTree.ContextMenu.Items.Add((object) menuItem2);
          System.Windows.Controls.MenuItem menuItem3 = new System.Windows.Controls.MenuItem();
          menuItem3.Header = (object) this.DeleteMenu;
          menuItem3.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
          this.portTypePackageTree.ContextMenu.Items.Add((object) menuItem3);
          System.Windows.Controls.MenuItem menuItem4 = new System.Windows.Controls.MenuItem();
          menuItem4.Header = (object) this.RenameMenu;
          menuItem4.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
          this.portTypePackageTree.ContextMenu.Items.Add((object) menuItem4);
          if (this.DisplayOnly)
          {
            menuItem1.IsEnabled = false;
            menuItem2.IsEnabled = false;
            menuItem3.IsEnabled = false;
            menuItem4.IsEnabled = false;
          }
        }
        else if (this.selectedItem.Tag is IListParameter)
        {
          System.Windows.Controls.MenuItem menuItem1 = new System.Windows.Controls.MenuItem();
          menuItem1.Header = (object) this.AddParameterMenu;
          menuItem1.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
          this.portTypePackageTree.ContextMenu.Items.Add((object) menuItem1);
          System.Windows.Controls.MenuItem menuItem2 = new System.Windows.Controls.MenuItem();
          menuItem2.Header = (object) this.DeleteMenu;
          menuItem2.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
          this.portTypePackageTree.ContextMenu.Items.Add((object) menuItem2);
          System.Windows.Controls.MenuItem menuItem3 = new System.Windows.Controls.MenuItem();
          menuItem3.Header = (object) this.RenameMenu;
          menuItem3.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
          this.portTypePackageTree.ContextMenu.Items.Add((object) menuItem3);
          if (this.DisplayOnly)
          {
            menuItem1.IsEnabled = false;
            menuItem2.IsEnabled = false;
            menuItem3.IsEnabled = false;
          }
        }
        else if (this.selectedItem.Tag is IParameter)
        {
          System.Windows.Controls.MenuItem menuItem1 = new System.Windows.Controls.MenuItem();
          menuItem1.Header = (object) this.DeleteMenu;
          menuItem1.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
          this.portTypePackageTree.ContextMenu.Items.Add((object) menuItem1);
          System.Windows.Controls.MenuItem menuItem2 = new System.Windows.Controls.MenuItem();
          menuItem2.Header = (object) this.RenameMenu;
          menuItem2.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
          this.portTypePackageTree.ContextMenu.Items.Add((object) menuItem2);
          System.Windows.Controls.MenuItem menuItem3 = new System.Windows.Controls.MenuItem();
          if ((this.selectedItem.Tag as IParameter).Key)
            menuItem3.Header = (object) this.ResetKeyMenu;
          else
            menuItem3.Header = (object) this.SetAsKeyMenu;
          menuItem3.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
          this.portTypePackageTree.ContextMenu.Items.Add((object) menuItem3);
          if (this.DisplayOnly)
          {
            menuItem3.IsEnabled = false;
            menuItem1.IsEnabled = false;
            menuItem2.IsEnabled = false;
          }
        }
        else
        {
          System.Windows.Controls.MenuItem menuItem = new System.Windows.Controls.MenuItem();
          menuItem.Header = (object) this.AddPortTypeMenu;
          menuItem.Click += new RoutedEventHandler(this.OnContextMenuItemClicked);
          this.portTypePackageTree.ContextMenu.Items.Add((object) menuItem);
          if (this.DisplayOnly)
            menuItem.IsEnabled = false;
        }
        this.portTypePackageTree.ContextMenu.Visibility = Visibility.Visible;
      }
      else
        this.portTypePackageTree.ContextMenu.Visibility = Visibility.Hidden;
    }

    private void OnTextChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      TreeNodeWithIcon selectedItem = this.portTypePackageTree.SelectedItem as TreeNodeWithIcon;
      if (selectedItem == null)
        return;
      string text = (sender as System.Windows.Controls.TextBox).Text;
      if (selectedItem.Tag is IPortType)
      {
        IPortType tag = selectedItem.Tag as IPortType;
        if (tag.Name.Equals(text, StringComparison.OrdinalIgnoreCase))
          return;
        string name = tag.Name;
        tag.Name = text;
        if (!(tag.Name != text))
          return;
        selectedItem.HeaderText = name;
      }
      else if (selectedItem.Tag is IListParameter)
      {
        IListParameter tag = selectedItem.Tag as IListParameter;
        if (tag.Name.Equals(text, StringComparison.OrdinalIgnoreCase))
          return;
        string name = tag.Name;
        tag.Name = text;
        if (!(tag.Name != text))
          return;
        selectedItem.HeaderText = name;
      }
      else
      {
        if (!(selectedItem.Tag is IParameter))
          return;
        IParameter tag = selectedItem.Tag as IParameter;
        if (tag.Name.Equals(text, StringComparison.OrdinalIgnoreCase))
          return;
        string name = tag.Name;
        tag.Name = text;
        if (!(tag.Name != text))
          return;
        selectedItem.HeaderText = name;
      }
    }

    private void OnMouseClick(object sender, MouseButtonEventArgs e)
    {
      this.selectedItem = e.Source is System.Windows.Controls.TextBlock || e.Source is Image ? ((FrameworkElement) ((FrameworkElement) e.Source).Parent).Parent as TreeNodeWithIcon : e.Source as TreeNodeWithIcon;
      if (this.selectedItem == null)
        return;
      ModelObject tag = this.selectedItem.Tag as ModelObject;
      IDesigner parentDesigner = (IDesigner) UICoreUtilities.Instance.GetParentDesigner((IDesignerControl) this);
      if (parentDesigner == null)
        return;
      if (tag == null)
        parentDesigner.RaiseSelectionChanged((IDesignerControl) this, this.Model);
      else
        parentDesigner.RaiseSelectionChanged(this.selectedItem.Tag as IDesignerControl, (IModelObject) tag);
    }

    private void LoadToolBar()
    {
      System.Windows.Controls.ToolBar toolBar = new System.Windows.Controls.ToolBar();
      toolBar.Band = 0;
      toolBar.BandIndex = 0;
      System.Windows.Controls.Button button1 = new System.Windows.Controls.Button();
      button1.Content = (object) new Image()
      {
        Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(Resource.AddIcon)
      };
      button1.Click += new RoutedEventHandler(this.AddBtn_Click);
      toolBar.Items.Add((object) button1);
      System.Windows.Controls.Button button2 = new System.Windows.Controls.Button();
      button2.Content = (object) new Image()
      {
        Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.Delete)
      };
      button2.Click += new RoutedEventHandler(this.DeleteBtn_Click);
      toolBar.Items.Add((object) button2);
      System.Windows.Controls.Button button3 = new System.Windows.Controls.Button();
      button3.Content = (object) new Image()
      {
        Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.rename)
      };
      button3.Click += new RoutedEventHandler(this.RenameBtn_Click);
      toolBar.Items.Add((object) button3);
      if (this.m_DisplayOnly)
      {
        button1.IsEnabled = false;
        button2.IsEnabled = false;
        button3.IsEnabled = false;
      }
      GridUtil.PlaceElement(this.Grid, (UIElement) toolBar, 0, 0);
    }

    private void OnContextMenuItemClicked(object sender, RoutedEventArgs e)
    {
      System.Windows.Controls.MenuItem source = e.Source as System.Windows.Controls.MenuItem;
      if (this.selectedItem == null)
        return;
      if (source.Header.ToString() == this.AddPortTypeMenu)
        this.Component.ControllerInterface.CreatePortType();
      else if (source.Header.ToString() == this.AddListParameterMenu)
      {
        UXPort tag = this.selectedItem.Tag as UXPort;
        if (tag == null)
          return;
        tag.CreateListParameter();
      }
      else if (source.Header.ToString() == this.AddParameterMenu)
      {
        if (this.selectedItem.Tag is IListParameter)
        {
          UXListParameter tag = this.selectedItem.Tag as UXListParameter;
          if (tag == null)
            return;
          tag.CreateParameter();
        }
        else
        {
          if (!(this.selectedItem.Tag is IPortType))
            return;
          UXPort tag = this.selectedItem.Tag as UXPort;
          if (tag == null)
            return;
          tag.CreateParameter();
        }
      }
      else if (source.Header.ToString() == this.DeleteMenu)
        this.OnDeleteClicked();
      else if (source.Header.ToString() == this.RenameMenu)
        this.OnRenameClicked();
      else if (source.Header.ToString() == this.SetAsKeyMenu)
      {
        this.OnSetAsKeyMenuClicked();
      }
      else
      {
        if (!(source.Header.ToString() == this.ResetKeyMenu))
          return;
        this.OnResetKeyMenuClicked();
      }
    }

    private void AddBtn_Click(object sender, RoutedEventArgs e)
    {
      this.OnAddClicked();
    }

    private void DeleteBtn_Click(object sender, RoutedEventArgs e)
    {
      this.OnDeleteClicked();
    }

    private void RenameBtn_Click(object sender, RoutedEventArgs e)
    {
      this.OnRenameClicked();
    }

    private void ExpandAll(TreeViewItem node)
    {
      foreach (TreeViewItem node1 in (IEnumerable) node.Items)
        this.ExpandAll(node1);
      node.IsExpanded = true;
    }

    private void OnDeleteClicked()
    {
      if (this.selectedItem == null)
        return;
      if (this.selectedItem.Tag is IPortType)
      {
        IPortType tag = this.selectedItem.Tag as IPortType;
        (tag.Parent as IInterface).RemovePortType(tag);
      }
      else if (this.selectedItem.Tag is IListParameter)
      {
        IListParameter tag = this.selectedItem.Tag as IListParameter;
        (tag.Parent as IPortType).RemoveListParameter(tag);
      }
      else
      {
        if (!(this.selectedItem.Tag is IParameter))
          return;
        IParameter tag = this.selectedItem.Tag as IParameter;
        if (tag.Parent is IPortType)
        {
          (tag.Parent as IPortType).RemoveParameter(tag);
        }
        else
        {
          if (!(tag.Parent is IListParameter))
            return;
          (tag.Parent as IListParameter).RemoveParameter(tag);
        }
      }
    }

    private void OnResetKeyMenuClicked()
    {
      if (this.selectedItem == null || !(this.selectedItem.Tag is IParameter))
        return;
      (this.selectedItem.Tag as IParameter).Key = false;
      this.selectedItem.Icon = (ImageSource) ResourceUtil.ConvertToBitMapImage(Resource.Port_Type_Parameter);
    }

    private void OnSetAsKeyMenuClicked()
    {
      if (this.selectedItem == null || !(this.selectedItem.Tag is IParameter))
        return;
      (this.selectedItem.Tag as IParameter).Key = true;
      this.selectedItem.Icon = (ImageSource) ResourceUtil.ConvertToBitMapImage(Resource.Key);
    }

    private void OnAddClicked()
    {
      if (this.selectedItem == null)
        return;
      if (this.selectedItem.Tag is IPortType)
        (this.selectedItem.Tag as IPortType).CreateParameter();
      else if (this.selectedItem.Tag is IListParameter)
      {
        (this.selectedItem.Tag as IListParameter).CreateParameter();
      }
      else
      {
        if (this.selectedItem.Tag != null)
          return;
        this.Component.ControllerInterface.CreatePortType();
      }
    }

    private void OnRenameClicked()
    {
      if (this.selectedItem == null || this.selectedItem.Tag == null)
        return;
      this.selectedItem.BeginEdit();
    }
  }
}
