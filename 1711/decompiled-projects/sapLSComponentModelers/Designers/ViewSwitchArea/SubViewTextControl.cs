﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea.SubViewTextControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea
{
  public class SubViewTextControl : BaseSelectableControl
  {
    private const string RESOURCE_SUBVIEWTITLE_FONT = "Arial,11,Normal";
    private const string RESOURCE_SUBVIEWTITLE_BOLDFONT = "Arial,11,Bold";
    private TextBlock textBlock;
    private StackPanel mainPanel;
    private VisualSubViewSwitchNavigation subViewNav;
    private SubViewSwitchNavItem subItem;

    public override string SelectionText
    {
      get
      {
        return "SubView Item";
      }
    }

    public TextBlock TextBlock
    {
      get
      {
        return this.textBlock;
      }
      set
      {
        this.textBlock = value;
      }
    }

    public VisualSubViewSwitchNavigation SubViewNav
    {
      get
      {
        return this.subViewNav;
      }
    }

    internal NavigationItem ChildNavItem
    {
      get
      {
        return this.m_ModelObject as NavigationItem;
      }
    }

    public SubViewTextControl(VisualSubViewSwitchNavigation subViewNav, NavigationItem childItem, SubViewSwitchNavItem subItem)
      : base((IModelObject) childItem)
    {
      this.subViewNav = subViewNav;
      this.subItem = subItem;
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      this.ControlBackground = (Brush) SkinConstants.BRUSH_SUBORDINATE_BACKGROUND;
      this.IsTabStop = true;
      this.mainPanel = new StackPanel();
      this.Content = (object) this.mainPanel;
      this.textBlock = new TextBlock();
      this.textBlock.IsHitTestVisible = true;
      this.textBlock.VerticalAlignment = VerticalAlignment.Center;
      this.textBlock.HorizontalAlignment = HorizontalAlignment.Center;
      FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.textBlock);
      this.mainPanel.Children.Add((UIElement) this.textBlock);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) "Delete")
        return;
      this.subViewNav.ParentNavItem.ViewSwitchNavigationList.ViewSwitchCNR.RemoveView(this.ChildNavItem);
    }

    public override void SelectMe()
    {
    }

    public override void UnSelect()
    {
    }

    protected override void OnMouseEnter(MouseEventArgs e)
    {
    }

    protected override void OnMouseLeave(MouseEventArgs e)
    {
    }

    public void setFontToBold()
    {
      FontManager.Instance.SetFontStyle1("Arial,11,Bold", this.textBlock);
    }

    public void setFontToNormal()
    {
      FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.textBlock);
    }
  }
}
