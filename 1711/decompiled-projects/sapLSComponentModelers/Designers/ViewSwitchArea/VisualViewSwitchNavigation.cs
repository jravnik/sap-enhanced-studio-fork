﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea.VisualViewSwitchNavigation
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea
{
  public class VisualViewSwitchNavigation : BaseSelectableControl
  {
    public static readonly RoutedEvent SelectedEvent = EventManager.RegisterRoutedEvent("Selected", RoutingStrategy.Bubble, typeof (RoutedEventHandler), typeof (VisualViewSwitchNavigation));
    private const string RESOURCE_OIFTAB_RIGHT_ON = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_on_end.png,14,27";
    private const string RESOURCE_OIFTAB_RIGHT_OFF = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_off_end.png,14,27";
    private const string RESOURCE_OIFTAB_RIGHT_OFFON = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_off_on_right.png,14,27";
    private const string RESOURCE_OIFTAB_RIGHT_ONOFF = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_on_off_right.png,14,27";
    private const string RESOURCE_OIFTAB_BACK_ON = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_on_back.png,1,27";
    private const string RESOURCE_OIFTAB_BACK_OFF = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_off_back.png,1,27";
    private const string RESOURCE_OIFTAB_LEFT_ONOFF = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_on_off_left.png,13,27";
    private const string RESOURCE_OIFTAB_LEFT_OFFON = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_off_on_left.png,13,27";
    private const string RESOURCE_OIFTAB_LEFT_ON = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_on.png,27,27";
    private const string RESOURCE_OIFTAB_LEFT_OFF = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_off.png,27,27";
    private const string SOLID_BACKCOLOR = "#CFDDE8";
    private const string ADDCOMMAND = "Add";
    private const string ADD_OIF_VIEW = "OIF View";
    private const string ADD_OIF_SUBVIEW = "OIF SubView";
    private Grid tabRectangleGrid;
    private VisualViewSwitchNavItem parentViewSwitchItem;
    private VisualViewSwitchNavItemList oifTabItems;
    private VisualSubViewSwitchNavigation subordinateList;
    private ViewContainerRegion vcRegion;
    private ViewSwitchContextualNavigationRegion oifCNR;
    private ViewSwitchNavArea vsNavArea;
    private string m_NavigationItemID;
    private List<VisualViewTextControl> tabItemText;
    private Dictionary<VisualViewSwitchNavItem, System.Windows.Controls.Image> leftImages;
    private Dictionary<VisualViewSwitchNavItem, System.Windows.Controls.Image> rightImages;
    private Dictionary<VisualViewSwitchNavItem, System.Windows.Controls.Image> centerImages;
    private ImageInfo[] tabItemImageInfos;
    private int colCount;

    public event VisualViewSwitchNavigation.SubTabItemOperationDelegate SubordinateSelected;

    public VisualViewSwitchNavItemList Items
    {
      get
      {
        return this.oifTabItems;
      }
    }

    public string NavigationItemID
    {
      get
      {
        return this.m_NavigationItemID;
      }
    }

    public ViewSwitchContextualNavigationRegion ViewSwitchCNR
    {
      get
      {
        return this.oifCNR;
      }
    }

    internal Navigation ViewSwitchNavigation
    {
      get
      {
        return this.m_ModelObject as Navigation;
      }
    }

    public VisualViewSwitchNavigation(IModelObject model, ViewContainerRegion vcRegion, ViewSwitchNavArea vsNavArea)
      : base(model)
    {
      this.vcRegion = vcRegion;
      this.vsNavArea = vsNavArea;
      this.oifCNR = vsNavArea.OIFContextNavigationRegion;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.ControlBackground = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        this.leftImages = new Dictionary<VisualViewSwitchNavItem, System.Windows.Controls.Image>();
        this.rightImages = new Dictionary<VisualViewSwitchNavItem, System.Windows.Controls.Image>();
        this.centerImages = new Dictionary<VisualViewSwitchNavItem, System.Windows.Controls.Image>();
        this.tabRectangleGrid = new Grid();
        this.tabItemText = new List<VisualViewTextControl>();
        this.InitializeOIFTabItemImages();
        this.oifTabItems = new VisualViewSwitchNavItemList((IModelObject) this.ViewSwitchNavigation, this);
        this.oifTabItems.ItemSelected += new EventHandler<TabItemEventArgs>(this.tabItems_ItemSelected);
        this.oifTabItems.ItemAdded += new EventHandler<TabItemEventArgs>(this.tabItems_ItemAdded);
        this.oifTabItems.SubordinateSelected += new VisualViewSwitchNavigation.SubTabItemOperationDelegate(this.tabItems_SubordinateSelected);
        this.InitializeGrid();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to create ViewSwitchNavigation UI.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      MenuItem parentItem = this.AddContextMenuItem("Add");
      this.AddContextMenuItem("OIF View", parentItem);
      this.AddContextMenuItem("OIF SubView", parentItem);
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header.Equals((object) "OIF View"))
        this.AddCurrentView();
      else if (source.Header.Equals((object) "OIF SubView"))
      {
        this.AddOIFSubView();
      }
      else
      {
        if (!source.Header.Equals((object) "Delete"))
          return;
        this.oifCNR.RemoveView(this.parentViewSwitchItem.ViewSwitchNavItem);
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (!(e.PropertyName == "NavigationItems"))
          return;
        this.RefreshViewSwitchNavigation();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update the property changes in the model.", ex));
      }
    }

    public override void Terminate()
    {
      try
      {
        base.Terminate();
        if (this.oifTabItems != null)
        {
          this.oifTabItems.Terminate();
          this.oifTabItems = (VisualViewSwitchNavItemList) null;
        }
        if (this.tabItemText == null)
          return;
        foreach (BaseVisualControl baseVisualControl in this.tabItemText)
          baseVisualControl.Terminate();
        this.tabItemText = (List<VisualViewTextControl>) null;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Termination of ViewSwitchNavigation failed.", ex));
      }
    }

    public override void SelectMe()
    {
    }

    public override void UnSelect()
    {
    }

    protected override void OnMouseLeave(MouseEventArgs e)
    {
      e.Handled = false;
    }

    protected override void OnMouseEnter(MouseEventArgs e)
    {
      e.Handled = false;
    }

    public override string SelectionText
    {
      get
      {
        return "NavigationItem";
      }
    }

    public event RoutedEventHandler Selected
    {
      add
      {
        this.AddHandler(VisualViewSwitchNavigation.SelectedEvent, (Delegate) value);
      }
      remove
      {
        this.RemoveHandler(VisualViewSwitchNavigation.SelectedEvent, (Delegate) value);
      }
    }

    private void RaiseSelectedEvent()
    {
      this.RaiseEvent(new RoutedEventArgs(VisualViewSwitchNavigation.SelectedEvent));
    }

    public void Select(string itemId)
    {
      this.Items.setSelectedItemId(itemId);
      this.SelectViewNavigation(itemId);
    }

    public void SelectViewNavigation(string itemId)
    {
      this.m_NavigationItemID = itemId;
      this.RaiseSelectedEvent();
    }

    public void RefreshViewSwitchNavigation()
    {
      List<NavigationItem> navigationItems = this.vsNavArea.OIFContextNavigationRegion.ViewSwitchNavigation.NavigationItems;
      this.tabItemImageInfos = (ImageInfo[]) null;
      this.ReloadView();
      this.vsNavArea.initTabStrip();
      if (navigationItems.Count <= 0)
        return;
      if (navigationItems[0].NavigationItemId != null)
      {
        this.Select(navigationItems[0].NavigationItemId);
      }
      else
      {
        if (navigationItems[0].ChildNavigationItems.Count <= 0)
          return;
        this.Select(navigationItems[0].ChildNavigationItems[0].NavigationItemId);
      }
    }

    private void ReArrangeNavigationItem()
    {
      List<NavigationItem> navigationItems = this.vsNavArea.OIFContextNavigationRegion.ViewSwitchNavigation.NavigationItems;
      int index = 0;
      foreach (NavigationItem navigationItem in navigationItems)
      {
        if (this.oifTabItems.Get(index) == null)
          this.Items.AddItem(navigationItem, navigationItem.NavigationItemId, navigationItem.ChildNavigationItems, this.vcRegion);
        ++index;
      }
      this.RemoveViewFromCollection();
    }

    private void RemoveViewFromCollection()
    {
      List<NavigationItem> navigationItems = this.vsNavArea.OIFContextNavigationRegion.ViewSwitchNavigation.NavigationItems;
      foreach (VisualViewSwitchNavItem viewSwitchNavItem in this.oifTabItems.ViewSwitchNavItemList)
      {
        VisualViewSwitchNavItem tabItem = viewSwitchNavItem;
        if (navigationItems.Find((Predicate<NavigationItem>) (item => item.NavigationItemId.Equals(tabItem.ItemId))) == null)
        {
          VisualViewTextControl textControl = this.tabItemText.Find((Predicate<VisualViewTextControl>) (visualTxt => visualTxt.VisualViewText.Equals(tabItem.TabText)));
          this.tabItemText.Remove(textControl);
          this.Items.RemoveItem(tabItem);
          this.RemoveTabItem(tabItem, textControl);
          this.RemoveViewFromCollection();
          break;
        }
      }
      this.updateAllTabItems(true);
    }

    public void AddCurrentView()
    {
      this.oifCNR.AddView();
      List<NavigationItem> navigationItems = this.vsNavArea.OIFContextNavigationRegion.ViewSwitchNavigation.NavigationItems;
      if (navigationItems == null)
        return;
      if (navigationItems.FindIndex((Predicate<NavigationItem>) (item => item.Title.Text.Equals("Attachments"))) == -1)
      {
        NavigationItem navigationItem = this.vsNavArea.OIFContextNavigationRegion.ViewSwitchNavigation.NavigationItems[navigationItems.Count - 1];
        this.Items.AddItem(navigationItem, navigationItem.NavigationItemId, navigationItem.ChildNavigationItems, this.vcRegion);
      }
      else
        this.RefreshViewSwitchNavigation();
    }

    public void AddOIFSubView()
    {
      NavigationItem parent = this.parentViewSwitchItem.ViewSwitchNavItem.Parent as NavigationItem;
      if (parent != null)
        this.oifCNR.AddChildView(parent);
      else
        this.oifCNR.AddChildView(this.parentViewSwitchItem.ViewSwitchNavItem);
      if (parent == null && this.parentViewSwitchItem.ViewSwitchNavItem.ChildNavigationItems.Count <= 2)
      {
        NavigationItem childNavigationItem = this.parentViewSwitchItem.ViewSwitchNavItem.ChildNavigationItems[0];
        this.subordinateList = new VisualSubViewSwitchNavigation((IModelObject) this.parentViewSwitchItem.ViewSwitchNavItem.ChildNavigationItems[0], this.parentViewSwitchItem.ViewSwitchNavItem.ChildNavigationItems, this.parentViewSwitchItem, this.vcRegion);
        this.subordinateList.ParentNavItem.TabItemSubordinates = this.subordinateList;
        this.parentViewSwitchItem.ItemId = childNavigationItem.NavigationItemId;
        if (this.parentViewSwitchItem != null)
        {
          Panel panel = this.subordinateList.constructSubordinates();
          if (panel != null)
          {
            GridUtil.PlaceElement(this.Grid, (UIElement) panel, 2, 0);
            panel.SetValue(Grid.ColumnSpanProperty, (object) this.Grid.ColumnDefinitions.Count);
          }
        }
        this.subordinateList.ShowSuboridnates();
      }
      else
      {
        if (this.parentViewSwitchItem.TabItemSubordinates == null)
          return;
        this.parentViewSwitchItem.TabItemSubordinates.AddCurrentSubView();
        this.parentViewSwitchItem.TabItemSubordinates.constructSubordinates();
        this.parentViewSwitchItem.TabItemSubordinates.ShowSuboridnates();
      }
    }

    private void updateAllTabItems(bool hideSubordinates)
    {
      int index = 0;
      for (int count = this.oifTabItems.Count; index < count; ++index)
      {
        VisualViewSwitchNavItem viewSwitchNavItem = this.oifTabItems.Get(index);
        this.UpdateTabItem(viewSwitchNavItem);
        if (hideSubordinates && viewSwitchNavItem.TabItemSubordinates != null)
          viewSwitchNavItem.TabItemSubordinates.HideSubordinates();
      }
    }

    private void InitializeGrid()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Pixel);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Pixel);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.tabRectangleGrid, 1, 1);
      string resourceInfo1 = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_footer_left.png,5,3";
      System.Windows.Controls.Image image1 = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image1, resourceInfo1);
      GridUtil.PlaceElement(this.Grid, (UIElement) image1, 1, 0);
      image1.VerticalAlignment = VerticalAlignment.Bottom;
      string resourceInfo2 = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_footer_center.png,1,3";
      System.Windows.Controls.Image image2 = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image2, resourceInfo2);
      GridUtil.PlaceElement(this.Grid, (UIElement) image2, 1, 2);
      image2.VerticalAlignment = VerticalAlignment.Bottom;
      image2.Width = double.NaN;
      image2.Stretch = Stretch.Fill;
      string resourceInfo3 = "sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/tab_footer_right.png,3,3";
      System.Windows.Controls.Image image3 = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image3, resourceInfo3);
      GridUtil.PlaceElement(this.Grid, (UIElement) image3, 1, 3);
      image3.VerticalAlignment = VerticalAlignment.Bottom;
      Rectangle rectangle = new Rectangle();
      rectangle.Fill = (Brush) ComponentModelersConstants.CONTAINER_BORDER_BRUSH;
      rectangle.Height = 1.0;
      GridUtil.PlaceElement(this.Grid, (UIElement) rectangle, 3, 0);
      rectangle.SetValue(Grid.ColumnSpanProperty, (object) 4);
      this.colCount = 0;
    }

    private void InitializeOIFTabItemImages()
    {
      this.tabItemImageInfos = new ImageInfo[10];
      this.tabItemImageInfos[0] = this.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_off.png,27,27");
      this.tabItemImageInfos[1] = this.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_on.png,27,27");
      this.tabItemImageInfos[2] = this.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_off_on_left.png,13,27");
      this.tabItemImageInfos[3] = this.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_on_off_left.png,13,27");
      this.tabItemImageInfos[5] = this.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_off_back.png,1,27");
      this.tabItemImageInfos[4] = this.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_on_back.png,1,27");
      this.tabItemImageInfos[6] = this.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_on_end.png,14,27");
      this.tabItemImageInfos[7] = this.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_off_end.png,14,27");
      this.tabItemImageInfos[8] = this.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_off_on_right.png,14,27");
      this.tabItemImageInfos[9] = this.GetImageInfo("sapLSUISkins;component/skin.nova/Images/Controls/TabStrip/taboif_on_off_right.png,14,27");
    }

    public ImageInfo GetImageInfo(string accessorString)
    {
      if (accessorString == null)
        return (ImageInfo) null;
      string[] strArray = accessorString.Split(',');
      if (strArray.Length != 3)
        return (ImageInfo) null;
      string name = strArray[0];
      int result1 = 0;
      if (!int.TryParse(strArray[1], out result1))
        return (ImageInfo) null;
      int result2 = 0;
      if (!int.TryParse(strArray[2], out result2))
        return (ImageInfo) null;
      return new ImageInfo(name, result1, result2);
    }

    public void UpdateTabItem(VisualViewSwitchNavItem item)
    {
      int index = this.oifTabItems.IndexOf(item);
      bool isSelected = index == this.oifTabItems.SelectedItemIndex;
      this.tabItemText[index].IsSelected = isSelected;
      ImageInfo tabItemImageInfo1 = this.tabItemImageInfos[(int) this.SelectLeftImage(isSelected, item)];
      ResourceUtil.LoadImage(this.leftImages[item], tabItemImageInfo1.Name);
      ImageInfo tabItemImageInfo2 = this.tabItemImageInfos[(int) this.SelectRightImage(isSelected, item)];
      ResourceUtil.LoadImage(this.rightImages[item], tabItemImageInfo2.Name);
      ImageInfo tabItemImageInfo3 = this.tabItemImageInfos[(int) this.SelectCenterImage(isSelected)];
      ResourceUtil.LoadImage(this.centerImages[item], tabItemImageInfo3.Name);
      this.centerImages[item].Stretch = Stretch.Fill;
      this.centerImages[item].Width = double.NaN;
    }

    private void RemoveTabItem(VisualViewSwitchNavItem item, VisualViewTextControl textControl)
    {
      foreach (UIElement child in this.tabRectangleGrid.Children)
      {
        VisualViewTextControl visualViewTextControl = child as VisualViewTextControl;
        if (visualViewTextControl != null && visualViewTextControl == textControl)
        {
          int num = (int) textControl.GetValue(Grid.ColumnProperty);
          if (this.leftImages.ContainsKey(item))
            this.leftImages.Remove(item);
          if (this.rightImages.ContainsKey(item))
            this.rightImages.Remove(item);
          if (this.centerImages.ContainsKey(item))
            this.centerImages.Remove(item);
          this.tabRectangleGrid.ColumnDefinitions.Remove(this.tabRectangleGrid.ColumnDefinitions[this.tabRectangleGrid.ColumnDefinitions.Count - 1]);
          this.tabRectangleGrid.ColumnDefinitions.Remove(this.tabRectangleGrid.ColumnDefinitions[this.tabRectangleGrid.ColumnDefinitions.Count - 1]);
          this.tabRectangleGrid.ColumnDefinitions.Remove(this.tabRectangleGrid.ColumnDefinitions[this.tabRectangleGrid.ColumnDefinitions.Count - 1]);
          break;
        }
      }
    }

    private void AddTabItem(VisualViewSwitchNavItem item, VisualViewTextControl textControl)
    {
      bool isSelected = item.ItemIndex == this.oifTabItems.SelectedItemIndex;
      System.Windows.Controls.Image image1 = new System.Windows.Controls.Image();
      ImageInfo tabItemImageInfo1 = this.tabItemImageInfos[(int) this.SelectLeftImage(isSelected, item)];
      ResourceUtil.LoadImage(image1, tabItemImageInfo1.Name);
      this.leftImages.Add(item, image1);
      System.Windows.Controls.Image image2 = new System.Windows.Controls.Image();
      ImageInfo tabItemImageInfo2 = this.tabItemImageInfos[(int) this.SelectRightImage(isSelected, item)];
      ResourceUtil.LoadImage(image2, tabItemImageInfo2.Name);
      this.rightImages.Add(item, image2);
      System.Windows.Controls.Image image3 = new System.Windows.Controls.Image();
      ImageInfo tabItemImageInfo3 = this.tabItemImageInfos[(int) this.SelectCenterImage(isSelected)];
      ResourceUtil.LoadImage(image3, tabItemImageInfo3.Name);
      image3.Stretch = Stretch.Fill;
      image3.Width = double.NaN;
      this.centerImages.Add(item, image3);
      GridUtil.AddRowDef(this.tabRectangleGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.tabRectangleGrid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.tabRectangleGrid, (UIElement) image1, 0, this.colCount);
      item.RegisterTabEventHandlers((FrameworkElement) image1, item);
      ++this.colCount;
      GridUtil.AddColumnDef(this.tabRectangleGrid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.tabRectangleGrid, (UIElement) image3, 0, this.colCount);
      item.RegisterTabEventHandlers((FrameworkElement) image3, item);
      GridUtil.PlaceElement(this.tabRectangleGrid, (UIElement) textControl, 0, this.colCount);
      ++this.colCount;
      item.RegisterTabEventHandlers((FrameworkElement) textControl, item);
      GridUtil.AddColumnDef(this.tabRectangleGrid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.tabRectangleGrid, (UIElement) image2, 0, this.colCount);
      item.RegisterTabEventHandlers((FrameworkElement) image2, item);
      ++this.colCount;
      this.tabRectangleGrid.Tag = (object) item.TabText;
      if (item.TabItemSubordinates == null)
        return;
      Panel panel = item.TabItemSubordinates.constructSubordinates();
      if (panel == null)
        return;
      GridUtil.PlaceElement(this.Grid, (UIElement) panel, 2, 0);
      panel.SetValue(Grid.ColumnSpanProperty, (object) this.Grid.ColumnDefinitions.Count);
    }

    private VisualViewSwitchNavigation.TabItemImages SelectLeftImage(bool isSelected, VisualViewSwitchNavItem item)
    {
      if (item.ItemIndex == 0)
        return !isSelected ? VisualViewSwitchNavigation.TabItemImages.LeftImageOff : VisualViewSwitchNavigation.TabItemImages.LeftImageOn;
      return item.ItemIndex == this.oifTabItems.SelectedItemIndex ? VisualViewSwitchNavigation.TabItemImages.LeftImageOffOn : VisualViewSwitchNavigation.TabItemImages.LeftImageOffNext;
    }

    private VisualViewSwitchNavigation.TabItemImages SelectCenterImage(bool isSelected)
    {
      return !isSelected ? VisualViewSwitchNavigation.TabItemImages.CenterImageOff : VisualViewSwitchNavigation.TabItemImages.CenterImageOn;
    }

    private VisualViewSwitchNavigation.TabItemImages SelectRightImage(bool isSelected, VisualViewSwitchNavItem item)
    {
      if (item.ItemIndex == this.oifTabItems.Count - 1)
        return !isSelected ? VisualViewSwitchNavigation.TabItemImages.RightImageOff : VisualViewSwitchNavigation.TabItemImages.RightImageOn;
      if (item.ItemIndex + 1 == this.oifTabItems.SelectedItemIndex)
        return VisualViewSwitchNavigation.TabItemImages.RightImageOffOn;
      return !isSelected ? VisualViewSwitchNavigation.TabItemImages.RightImageOff : VisualViewSwitchNavigation.TabItemImages.RightImageOnOff;
    }

    private void tabItems_SubordinateSelected(object sender, TabItemEventArgs eventArgs)
    {
      if (this.SubordinateSelected == null)
        return;
      this.updateAllTabItems(false);
      this.SubordinateSelected(sender, eventArgs);
    }

    private void tabItems_ItemAdded(object sender, TabItemEventArgs e)
    {
      VisualViewTextControl textControl = new VisualViewTextControl((IModelObject) e.Item.ViewSwitchNavItem, e.Item);
      this.tabItemText.Add(textControl);
      this.AddTabItem(e.Item, textControl);
    }

    private void tabItems_ItemSelected(object sender, TabItemEventArgs e)
    {
      if (e.Item == null)
        return;
      this.updateAllTabItems(true);
      this.parentViewSwitchItem = e.Item;
      if (e.Item.TabItemSubordinates == null || e.Item.TabItemSubordinates.IsEmpty)
        return;
      e.Item.TabItemSubordinates.ShowSuboridnates();
    }

    public delegate void TabItemOperationDelegate(TabItem item);

    public delegate void SubTabItemOperationDelegate(object sender, TabItemEventArgs item);

    private enum TabItemImages
    {
      LeftImageOff,
      LeftImageOn,
      LeftImageOffOn,
      LeftImageOffNext,
      CenterImageOn,
      CenterImageOff,
      RightImageOn,
      RightImageOff,
      RightImageOffOn,
      RightImageOnOff,
    }
  }
}
