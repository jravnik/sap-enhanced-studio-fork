﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea.VisualSubViewSwitchNavigation
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea
{
  public class VisualSubViewSwitchNavigation : BaseSelectableControl
  {
    private const string RESOURCE_SEPERATOR = "#7294BD,0.5,#B1C7E4";
    private const string SEPARATOR_IMAGE = "sapLSUISkins;component/skin.nova/Images/Bars/ToolBar/toolbar_divider.png,2,15";
    private List<SubViewSwitchNavItem> subordinateList;
    private List<NavigationItem> childItems;
    private ViewContainerRegion vcRegion;
    private SubViewSwitchNavItem subordinateItem;
    private SubViewSwitchNavItem subItemClicked;
    private VisualViewSwitchNavItem parentTabItem;
    private StackPanel mainPanel;
    private Canvas leftMarginPanel;
    private Thickness suboridnatemargin;
    private double seperatorThickness;
    private Thickness seperatorMargin;
    private Brush seperatorBrush;
    private Brush textBlockBrush;

    internal NavigationItem ChildViewSwitchNavItem
    {
      get
      {
        return this.m_ModelObject as NavigationItem;
      }
    }

    public List<SubViewSwitchNavItem> ChildItemsList
    {
      get
      {
        return this.subordinateList;
      }
    }

    public VisualViewSwitchNavItem ParentNavItem
    {
      get
      {
        return this.parentTabItem;
      }
    }

    public VisualSubViewSwitchNavigation(IModelObject model, List<NavigationItem> TabItemSubordinates, VisualViewSwitchNavItem parentTabItem, ViewContainerRegion vcRegion)
      : base(model)
    {
      this.parentTabItem = parentTabItem;
      this.childItems = TabItemSubordinates;
      this.vcRegion = vcRegion;
      this.subordinateList = new List<SubViewSwitchNavItem>(TabItemSubordinates.Count);
      foreach (NavigationItem tabItemSubordinate in TabItemSubordinates)
      {
        tabItemSubordinate.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
        this.subordinateItem = new SubViewSwitchNavItem(tabItemSubordinate, parentTabItem);
        this.subordinateList.Add(this.subordinateItem);
      }
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.leftMarginPanel = new Canvas();
        this.leftMarginPanel.Margin = new Thickness(10.0, 0.0, 0.0, 0.0);
        this.mainPanel = new StackPanel();
        this.mainPanel.Orientation = Orientation.Horizontal;
        this.mainPanel.Visibility = Visibility.Collapsed;
        this.mainPanel.Children.Add((UIElement) this.leftMarginPanel);
        this.ControlBackground = (Brush) SkinConstants.BRUSH_SUBORDINATE_BACKGROUND;
        this.suboridnatemargin = new Thickness(4.0, 4.0, 4.0, 1.0);
        this.textBlockBrush = (Brush) SkinConstants.BRUSH_SUBORDINATE_FOREGROUND;
        this.seperatorBrush = (Brush) ResourceUtil.GetLinearGradientBrush("#7294BD,0.5,#B1C7E4");
        this.seperatorThickness = 2.0;
        this.seperatorMargin = new Thickness(4.0, 4.0, 4.0, 1.0);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Fialed to create SubView Item.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (!(e.PropertyName == "Title"))
          return;
        this.subItemClicked.SubViewTextControl.TextBlock.Text = this.subItemClicked.Text;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update the property changes in the model.", ex));
      }
    }

    public override void SelectMe()
    {
    }

    public override void UnSelect()
    {
    }

    protected override void OnMouseLeave(MouseEventArgs e)
    {
      e.Handled = false;
    }

    protected override void OnMouseEnter(MouseEventArgs e)
    {
      e.Handled = false;
    }

    public override string SelectionText
    {
      get
      {
        return "SubNavigationItm";
      }
    }

    public bool IsEmpty
    {
      get
      {
        return this.subordinateList.Count == 0;
      }
    }

    public Panel constructSubordinates()
    {
      if (this.subordinateList.Count == 0)
        return (Panel) null;
      int num = 0;
      this.mainPanel.Children.Clear();
      this.mainPanel.Background = this.ControlBackground;
      this.mainPanel.Children.Add((UIElement) this.leftMarginPanel);
      foreach (SubViewSwitchNavItem subordinate in this.subordinateList)
      {
        foreach (NavigationItem childItem in this.childItems)
        {
          if (childItem.NavigationItemId == subordinate.ItemId)
          {
            if (num > 0)
              this.LoadToolbarSeperatorImage();
            SubViewTextControl subViewTextControl = new SubViewTextControl(this, childItem, subordinate);
            subViewTextControl.TextBlock.Margin = this.suboridnatemargin;
            subViewTextControl.TextBlock.Text = subordinate.Text;
            subViewTextControl.TextBlock.Foreground = this.textBlockBrush;
            subViewTextControl.TextBlock.MouseLeftButtonDown += new MouseButtonEventHandler(this.textBlock_MouseLeftButtonDown);
            subViewTextControl.TextBlock.Tag = (object) num;
            this.mainPanel.Children.Add((UIElement) subViewTextControl);
            subordinate.SubViewTextControl = subViewTextControl;
            ++num;
            break;
          }
        }
      }
      this.initFirstSubViewTextBlocks();
      return (Panel) this.mainPanel;
    }

    private void textBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      this.setTextBlockClicked(sender);
      foreach (NavigationItem childItem in this.childItems)
      {
        if (this.subItemClicked.ItemId == childItem.NavigationItemId)
        {
          this.m_ModelObject = (IModelObject) childItem;
          break;
        }
      }
      this.OnMouseLeftButtonDown(e);
      this.parentTabItem.ViewSwitchNavigationList.SelectViewNavigation(this.subItemClicked.ItemId);
    }

    public void AddCurrentSubView()
    {
      NavigationItem parent = this.parentTabItem.ViewSwitchNavItem.Parent as NavigationItem;
      NavigationItem child = parent == null ? this.parentTabItem.ViewSwitchNavItem.ChildNavigationItems[this.childItems.Count - 1] : parent.ChildNavigationItems[this.childItems.Count - 1];
      child.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      this.subordinateItem = new SubViewSwitchNavItem(child, this.parentTabItem);
      this.subordinateList.Add(this.subordinateItem);
    }

    public SubViewSwitchNavItem setTextBlockClicked(object sender)
    {
      SubViewTextControl subViewTextControl = sender as SubViewTextControl;
      TextBlock textBlock = subViewTextControl == null ? sender as TextBlock : subViewTextControl.TextBlock;
      if (textBlock != null)
      {
        this.initAllTextBlocks();
        textBlock.Cursor = Cursors.Arrow;
        textBlock.FontWeight = FontWeights.Bold;
        foreach (SubViewSwitchNavItem subordinate in this.subordinateList)
          subordinate.IsSelected = false;
        this.subItemClicked = this.subordinateList[(int) textBlock.Tag];
        this.subItemClicked.IsSelected = true;
      }
      return this.subItemClicked;
    }

    private void initAllTextBlocks()
    {
      foreach (UIElement child in this.mainPanel.Children)
      {
        SubViewTextControl subViewTextControl = child as SubViewTextControl;
        if (subViewTextControl != null)
        {
          subViewTextControl.TextBlock.Cursor = Cursors.Hand;
          subViewTextControl.setFontToNormal();
        }
      }
    }

    public void initFirstSubViewTextBlocks()
    {
      this.initAllTextBlocks();
      foreach (UIElement child in this.mainPanel.Children)
      {
        SubViewTextControl subViewTextControl = child as SubViewTextControl;
        if (subViewTextControl != null)
        {
          subViewTextControl.TextBlock.Cursor = Cursors.Arrow;
          subViewTextControl.setFontToBold();
          subViewTextControl.FontWeight = FontWeights.Bold;
          this.subItemClicked = this.subordinateList[0];
          this.subItemClicked.IsSelected = true;
          using (List<NavigationItem>.Enumerator enumerator = this.childItems.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              NavigationItem current = enumerator.Current;
              if (this.subItemClicked.ItemId == current.NavigationItemId)
              {
                this.m_ModelObject = (IModelObject) current;
                break;
              }
            }
            break;
          }
        }
      }
    }

    public void deselectAllSubordinates()
    {
      foreach (SubViewSwitchNavItem subordinate in this.subordinateList)
        subordinate.IsSelected = false;
      this.initAllTextBlocks();
    }

    public void HideSubordinates()
    {
      this.mainPanel.Visibility = Visibility.Collapsed;
    }

    public void ShowSuboridnates()
    {
      this.mainPanel.Visibility = Visibility.Visible;
    }

    private void LoadToolbarSeperatorImage()
    {
      System.Windows.Controls.Image image = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image, "sapLSUISkins;component/skin.nova/Images/Bars/ToolBar/toolbar_divider.png,2,15");
      image.Stretch = Stretch.Fill;
      image.Margin = this.seperatorMargin;
      image.Height = 15.0;
      this.mainPanel.Children.Add((UIElement) image);
    }

    public IEnumerator GetEnumerator()
    {
      return (IEnumerator) this.subordinateList.GetEnumerator();
    }

    public SubViewSwitchNavItem this[int index]
    {
      get
      {
        return this.subordinateList[index];
      }
    }
  }
}
