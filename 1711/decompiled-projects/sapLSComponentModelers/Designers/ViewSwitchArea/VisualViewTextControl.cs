﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea.VisualViewTextControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea
{
  public class VisualViewTextControl : BaseSelectableControl
  {
    private const string RESOURCE_TITLE_NORMALFONT = "Arial,11,Normal";
    private const string RESOURCE_TITLE_BOLDFONT = "Arial,11,Bold";
    private const string ADDCOMMAND = "Add";
    private const string ADD_OIF_VIEW = "OIF View";
    private const string ADD_OIF_SUBVIEW = "OIF SubView";
    private VisualViewSwitchNavItem item;
    private TextBlock textBlock;
    private TextBlock boldTextBlock;
    private bool isSelected;
    private EventHandler<EventArgs> itemTextChangedEventHandler;

    internal NavigationItem NavigItem
    {
      get
      {
        return this.m_ModelObject as NavigationItem;
      }
    }

    public VisualViewSwitchNavItem Item
    {
      get
      {
        return this.item;
      }
    }

    public VisualViewTextControl(IModelObject model, VisualViewSwitchNavItem item)
      : base(model)
    {
      this.item = item;
      this.Background = (Brush) null;
      this.IsTabStop = true;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.textBlock = new TextBlock();
        this.textBlock.IsHitTestVisible = true;
        this.textBlock.VerticalAlignment = VerticalAlignment.Center;
        this.textBlock.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        FontManager.Instance.SetFontStyle1("Arial,11,Normal", this.textBlock);
        this.boldTextBlock = new TextBlock();
        this.boldTextBlock.IsHitTestVisible = true;
        this.boldTextBlock.VerticalAlignment = VerticalAlignment.Center;
        this.boldTextBlock.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        FontManager.Instance.SetFontStyle1("Arial,11,Bold", this.boldTextBlock);
        this.Content = (object) this.textBlock;
        this.textBlock.MouseLeftButtonDown += new MouseButtonEventHandler(this.TabItemTextControl_MouseLeftButtonDown);
        this.boldTextBlock.MouseLeftButtonDown += new MouseButtonEventHandler(this.TabItemTextControl_MouseLeftButtonDown);
        this.item.MouseLeftButtonDown += new MouseButtonEventHandler(this.TabItemTextControl_MouseLeftButtonDown);
        this.itemTextChangedEventHandler = new EventHandler<EventArgs>(this.item_TextChanged);
        this.item.TextChanged += this.itemTextChangedEventHandler;
        this.UpdateText();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to create ViewTextControl UI.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      System.Windows.Controls.MenuItem parentItem = this.AddContextMenuItem("Add");
      this.AddContextMenuItem("OIF View", parentItem);
      this.AddContextMenuItem("OIF SubView", parentItem);
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header.Equals((object) "OIF View"))
        this.item.ViewSwitchNavigationList.AddCurrentView();
      else if (source.Header.Equals((object) "OIF SubView"))
      {
        this.item.ViewSwitchNavigationList.AddOIFSubView();
      }
      else
      {
        if (!source.Header.Equals((object) "Delete"))
          return;
        if (this.item.ViewSwitchNavigationList.ViewSwitchNavigation.NavigationItems.Count > 2)
        {
          this.item.ViewSwitchNavigationList.ViewSwitchCNR.RemoveView(this.item.ViewSwitchNavItem);
        }
        else
        {
          int num = (int) DisplayMessage.Show("An Object Instance Floorplan needs to have atleast 2 views.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
    }

    public override string SelectionText
    {
      get
      {
        return "NavigationItem";
      }
    }

    public override void Terminate()
    {
      base.Terminate();
      if (this.item == null)
        return;
      this.item.TextChanged -= this.itemTextChangedEventHandler;
      this.item = (VisualViewSwitchNavItem) null;
    }

    public override void SelectMe()
    {
    }

    public override void UnSelect()
    {
    }

    protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
    {
    }

    protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
    {
    }

    private void TabItemTextControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      if (this.item.TabItemSubordinates != null)
      {
        this.item.TabItemSubordinates.initFirstSubViewTextBlocks();
        if (this.item.ViewSwitchNavItem.ChildNavigationItems.Count > 0)
          this.m_ModelObject = (IModelObject) this.item.ViewSwitchNavItem.ChildNavigationItems[0];
        else
          this.m_ModelObject = (IModelObject) this.item.ViewSwitchNavItem;
      }
      this.OnMouseLeftButtonDown(e);
      this.item.Select();
      e.Handled = true;
    }

    private void item_TextChanged(object sender, EventArgs e)
    {
      if (this.Dispatcher.CheckAccess())
        this.UpdateText();
      else
        this.Dispatcher.BeginInvoke((Delegate) new Action(this.UpdateText));
    }

    private void UpdateText()
    {
      this.textBlock.Text = this.item.TabText;
      this.boldTextBlock.Text = this.item.TabText;
    }

    public string VisualViewText
    {
      get
      {
        return this.item.TabText;
      }
    }

    public bool IsSelected
    {
      get
      {
        return this.isSelected;
      }
      set
      {
        if (this.isSelected == value)
          return;
        this.isSelected = value;
        this.Content = this.isSelected ? (object) this.boldTextBlock : (object) this.textBlock;
      }
    }
  }
}
