﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea.SubViewSwitchNavItem
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Collections.Generic;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea
{
  public class SubViewSwitchNavItem
  {
    private KeyValuePair<string, NavigationItem> content;
    private bool isSelected;
    private VisualViewSwitchNavItem parentTabItem;
    private SubViewTextControl m_SubViewTextControl;

    public SubViewSwitchNavItem(NavigationItem child, VisualViewSwitchNavItem parentTabItem)
    {
      this.content = new KeyValuePair<string, NavigationItem>(child.Title.Text, child);
      this.isSelected = false;
      this.parentTabItem = parentTabItem;
    }

    public string Text
    {
      get
      {
        return this.content.Value.Title.ToString();
      }
    }

    public SubViewTextControl SubViewTextControl
    {
      get
      {
        return this.m_SubViewTextControl;
      }
      set
      {
        this.m_SubViewTextControl = value;
      }
    }

    public string ItemId
    {
      get
      {
        return this.content.Value.NavigationItemId;
      }
    }

    public bool IsSelected
    {
      get
      {
        return this.isSelected;
      }
      set
      {
        this.isSelected = value;
      }
    }
  }
}
