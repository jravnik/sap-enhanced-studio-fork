﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.DataObjectDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using SAP.BYD.LS.UIDesigner.UICore.DataModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Integration;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class DataObjectDesigner : ComponentDesigner
  {
    public DataObjectDesigner(DataObjectComponent component)
      : base((IModelObject) component, false, false)
    {
    }

    protected override void InitializeComponentDesigner()
    {
      this.m_TabWindow = new TabControl();
      this.m_DataModelTab = new TabPage();
      this.m_ControllerTab = new TabPage();
      this.m_TabWindow.SuspendLayout();
      this.m_DataModelTab.SuspendLayout();
      this.m_ControllerTab.SuspendLayout();
      this.SuspendLayout();
      this.m_TabWindow.Alignment = TabAlignment.Bottom;
      this.m_TabWindow.Controls.Add((Control) this.m_DataModelTab);
      this.m_TabWindow.Controls.Add((Control) this.m_ControllerTab);
      this.m_TabWindow.SelectedIndex = 0;
      this.m_TabWindow.Dock = DockStyle.Fill;
      this.m_TabWindow.SelectedIndexChanged += new EventHandler(this.OnTabWindowSelectedIndexChanged);
      this.m_DataModelTab.BackColor = ComponentModelersConstants.WF_CONTENT_AREA_COLOR;
      this.m_DataModelTab.Text = "DataModel";
      this.m_ControllerTab.BackColor = ComponentModelersConstants.WF_CONTENT_AREA_COLOR;
      this.m_ControllerTab.Text = "Controller";
      this.AllowDrop = true;
      this.BackColor = Color.Transparent;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BorderStyle = BorderStyle.FixedSingle;
      this.Controls.Add((Control) this.m_TabWindow);
      this.Margin = new Padding(0);
      this.Size = new Size(590, 340);
      this.Click += new EventHandler(this.OnFloorplanDesignerClick);
      this.m_TabWindow.ResumeLayout(false);
      this.m_DataModelTab.ResumeLayout(false);
      this.m_ControllerTab.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    public override void LoadView()
    {
      this.LoadDataModelView();
    }

    public override void Terminate()
    {
      if (this.m_ModelObject != null)
      {
        this.m_ModelObject.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
        this.m_ModelObject.ModelObjectAdded -= new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.m_ModelObject.ModelObjectRemoved -= new ModelRemovedEventHandler(this.OnModelObjectRemoved);
      }
      if (this.m_ControllerDesigner != null && !this.m_ControllerDesigner.IsDisposed)
      {
        this.m_ControllerVisualControl.Terminate();
        this.m_ControllerDesigner.Dispose();
        this.m_ControllerTab.Controls.Clear();
        this.m_ControllerDesigner = (ElementHost) null;
      }
      if (this.m_DataModelDesigner == null || this.m_DataModelDesigner.IsDisposed)
        return;
      this.m_DataModelDesigner.OnTerminate();
      this.m_DataModelDesigner.Dispose();
      this.m_DataModelTab.Controls.Clear();
      this.m_DataModelDesigner = (DataModelDesigner) null;
    }

    public override void ReloadView()
    {
      this.Terminate();
      if (this.m_ModelObject != null)
      {
        ComponentModelerUtilities.Instance.AddDesigner(this.m_ModelObject as IFloorplan, (BaseDesigner) this);
        this.m_ModelObject.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
        this.m_ModelObject.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.m_ModelObject.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
      }
      this.LoadControllerView();
      this.LoadView();
    }

    protected override void BringSelectedModelToFront(IModelObject modelToSelect)
    {
      List<IModelObject> modelObjectList = new List<IModelObject>();
      switch (modelToSelect.GetConfigurationArea())
      {
        case "Controller":
          this.m_TabWindow.SelectedIndex = 1;
          this.m_ControllerVisualControl.SelectModelInControllerTab(modelToSelect, this.m_DesignerControlDictionary);
          break;
        case "DataModel":
          this.m_TabWindow.SelectedIndex = 0;
          this.m_DataModelDesigner.SelectNodeInDataModelTab(modelToSelect);
          break;
      }
    }
  }
}
