﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.TaskListViewContent
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Connector.LoggingLayer;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class TaskListViewContent : BaseSelectableControl
  {
    private ObservableCollection<TaskTypeItem> m_AvailableTaskTypeCollection = new ObservableCollection<TaskTypeItem>();
    private ObservableCollection<TaskTypeItem> m_SelectedTaskTypeCollection = new ObservableCollection<TaskTypeItem>();
    private ToolTip m_Tooltip = new ToolTip();
    private const int COLUMNWIDTH = 200;
    private ListView avTaskTypes;
    private ListView selectedTaskTypes;
    private TextBox searchTextBox;
    private Button btnSearch;
    private static List<SAP.BYD.LS.UIDesigner.Connector.TaskType> m_TaskTypes;
    private TaskTypes selTaskItems;
    private GridViewColumn lastColumnSorted;

    internal OberonCenterStructure centerStrucuture
    {
      get
      {
        return this.m_ModelObject as OberonCenterStructure;
      }
    }

    public TaskListViewContent(IModelObject contentRegion)
      : base(contentRegion, false, false)
    {
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      this.InitializeUI();
      this.LoadData();
    }

    private void InitializeUI()
    {
      GridUtil.AddColumnDef(this.Grid, 200.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.Grid, 30.0, GridUnitType.Pixel);
      GridUtil.AddColumnDef(this.Grid, 30.0, GridUnitType.Pixel);
      GridUtil.AddColumnDef(this.Grid, 200.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 30.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.Grid, 300.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 30.0, GridUnitType.Pixel);
      GridUtil.AddRowDef(this.Grid, 30.0, GridUnitType.Pixel);
      GridUtil.AddRowDef(this.Grid, 300.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 30.0, GridUnitType.Pixel);
      TextBlock textBlock1 = new TextBlock();
      textBlock1.Text = "Available TaskTypes";
      textBlock1.TextAlignment = TextAlignment.Center;
      textBlock1.Margin = new Thickness(0.0, 4.0, 0.0, 4.0);
      textBlock1.FontWeight = FontWeights.Bold;
      GridUtil.PlaceElement(this.Grid, (UIElement) textBlock1, 0, 0);
      textBlock1.SetValue(Grid.ColumnSpanProperty, (object) 2);
      TextBlock textBlock2 = new TextBlock();
      textBlock2.Text = "Selected TaskTypes";
      textBlock2.TextAlignment = TextAlignment.Center;
      textBlock2.FontWeight = FontWeights.Bold;
      textBlock2.Margin = new Thickness(0.0, 4.0, 0.0, 4.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) textBlock2, 0, 3);
      this.avTaskTypes = new ListView();
      GridView gridView1 = new GridView();
      GridViewColumn gridViewColumn1 = new GridViewColumn();
      GridViewColumnHeader viewColumnHeader1 = new GridViewColumnHeader();
      viewColumnHeader1.Content = (object) "TaskType Name";
      viewColumnHeader1.Click += new RoutedEventHandler(this.OnAvTasksHeaderClick);
      gridViewColumn1.Header = (object) viewColumnHeader1;
      gridViewColumn1.DisplayMemberBinding = (BindingBase) new Binding("Name");
      gridViewColumn1.Width = 200.0;
      gridView1.Columns.Add(gridViewColumn1);
      GridViewColumn gridViewColumn2 = new GridViewColumn();
      GridViewColumnHeader viewColumnHeader2 = new GridViewColumnHeader();
      viewColumnHeader2.Content = (object) "Description";
      viewColumnHeader2.Click += new RoutedEventHandler(this.OnAvTasksHeaderClick);
      gridViewColumn2.Header = (object) viewColumnHeader2;
      gridViewColumn2.DisplayMemberBinding = (BindingBase) new Binding("Description");
      gridViewColumn2.Width = 200.0;
      gridView1.Columns.Add(gridViewColumn2);
      this.avTaskTypes.View = (ViewBase) gridView1;
      GridUtil.PlaceElement(this.Grid, (UIElement) this.avTaskTypes, 1, 0);
      this.avTaskTypes.SetValue(Grid.RowSpanProperty, (object) 4);
      this.avTaskTypes.SetValue(Grid.ColumnSpanProperty, (object) 2);
      this.avTaskTypes.Margin = new Thickness(4.0, 4.0, 4.0, 4.0);
      this.avTaskTypes.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.avTaskTypes.VerticalAlignment = VerticalAlignment.Stretch;
      this.searchTextBox = new TextBox();
      GridUtil.PlaceElement(this.Grid, (UIElement) this.searchTextBox, 5, 0);
      this.searchTextBox.KeyDown += new KeyEventHandler(this.KeyDownEventHanlder);
      this.searchTextBox.Margin = new Thickness(4.0, 4.0, 4.0, 4.0);
      this.btnSearch = new Button();
      GridUtil.PlaceElement(this.Grid, (UIElement) this.btnSearch, 5, 1);
      this.btnSearch.Margin = new Thickness(4.0, 4.0, 4.0, 4.0);
      this.btnSearch.Click += new RoutedEventHandler(this.btnSearch_Click);
      this.btnSearch.Content = (object) new Image()
      {
        Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.Search)
      };
      Button button1 = new Button();
      GridUtil.PlaceElement(this.Grid, (UIElement) button1, 2, 2);
      button1.Margin = new Thickness(4.0, 4.0, 4.0, 4.0);
      button1.Click += new RoutedEventHandler(this.btnMoveRight_Click);
      button1.Content = (object) new Image()
      {
        Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.Select)
      };
      Button button2 = new Button();
      GridUtil.PlaceElement(this.Grid, (UIElement) button2, 3, 2);
      button2.Margin = new Thickness(4.0, 4.0, 4.0, 4.0);
      button2.Click += new RoutedEventHandler(this.btnMoveLeft_Click);
      button2.Content = (object) new Image()
      {
        Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.UnSelect)
      };
      this.selectedTaskTypes = new ListView();
      GridView gridView2 = new GridView();
      GridViewColumn gridViewColumn3 = new GridViewColumn();
      GridViewColumnHeader viewColumnHeader3 = new GridViewColumnHeader();
      viewColumnHeader3.Content = (object) "TaskType Name";
      viewColumnHeader3.Click += new RoutedEventHandler(this.OnSelTasksHeaderClick);
      gridViewColumn3.Header = (object) viewColumnHeader3;
      gridViewColumn3.DisplayMemberBinding = (BindingBase) new Binding("Name");
      gridViewColumn3.Width = 200.0;
      gridView2.Columns.Add(gridViewColumn3);
      GridViewColumn gridViewColumn4 = new GridViewColumn();
      GridViewColumnHeader viewColumnHeader4 = new GridViewColumnHeader();
      viewColumnHeader4.Content = (object) "Description";
      viewColumnHeader4.Click += new RoutedEventHandler(this.OnSelTasksHeaderClick);
      gridViewColumn4.Header = (object) viewColumnHeader4;
      gridViewColumn4.DisplayMemberBinding = (BindingBase) new Binding("Description");
      gridViewColumn4.Width = 200.0;
      gridView2.Columns.Add(gridViewColumn4);
      this.selectedTaskTypes.View = (ViewBase) gridView2;
      GridUtil.PlaceElement(this.Grid, (UIElement) this.selectedTaskTypes, 1, 3);
      this.selectedTaskTypes.SetValue(Grid.RowSpanProperty, (object) 5);
      this.selectedTaskTypes.SetValue(Grid.ColumnSpanProperty, (object) 2);
      this.selectedTaskTypes.Margin = new Thickness(4.0, 4.0, 4.0, 4.0);
      this.selectedTaskTypes.HorizontalAlignment = HorizontalAlignment.Stretch;
      this.selectedTaskTypes.VerticalAlignment = VerticalAlignment.Stretch;
    }

    private void LoadData()
    {
      try
      {
        this.selTaskItems = this.centerStrucuture.tType;
        if (TaskListViewContent.m_TaskTypes == null)
          TaskListViewContent.m_TaskTypes = CrossServicesProxy.Instance.GetTaskTypes();
        if (TaskListViewContent.m_TaskTypes != null)
        {
          this.m_AvailableTaskTypeCollection.Clear();
          this.m_SelectedTaskTypeCollection.Clear();
          foreach (SAP.BYD.LS.UIDesigner.Connector.TaskType taskType in TaskListViewContent.m_TaskTypes)
            this.AddTaskToList(taskType, this.selTaskItems);
          if (this.selTaskItems != null && this.selTaskItems.TaskType != null && (this.selTaskItems.TaskType.Count == 0 && TaskListViewContent.m_TaskTypes.Count > 0) && this.m_AvailableTaskTypeCollection.Count > 0)
          {
            TaskTypeItem availableTaskType = this.m_AvailableTaskTypeCollection[0];
            if (availableTaskType != null)
            {
              SAP.BYD.LS.UIDesigner.Connector.TaskType tag = availableTaskType.Tag as SAP.BYD.LS.UIDesigner.Connector.TaskType;
              if (tag != null && this.selTaskItems != null && this.selTaskItems.TaskType != null)
              {
                if (availableTaskType.TaType == null)
                  availableTaskType.TaType = new SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure.TaskType((IModelObject) this.selTaskItems)
                  {
                    TType = tag.Name
                  };
                this.selTaskItems.TaskType.Add(availableTaskType.TaType);
              }
              this.m_AvailableTaskTypeCollection.Remove(availableTaskType);
              this.m_SelectedTaskTypeCollection.Add(availableTaskType);
            }
          }
        }
        this.avTaskTypes.ItemsSource = (IEnumerable) this.m_AvailableTaskTypeCollection;
        this.selectedTaskTypes.ItemsSource = (IEnumerable) this.m_SelectedTaskTypeCollection;
        List<SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure.TaskType> taskTypeList = new List<SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure.TaskType>();
        if (this.selTaskItems != null && this.selTaskItems.TaskType != null && (this.m_SelectedTaskTypeCollection != null && this.selTaskItems.TaskType.Count != this.m_SelectedTaskTypeCollection.Count))
        {
          foreach (SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure.TaskType taskType in this.selTaskItems.TaskType)
          {
            bool flag = true;
            foreach (TaskTypeItem selectedTaskType in (Collection<TaskTypeItem>) this.m_SelectedTaskTypeCollection)
            {
              if (selectedTaskType.Name == taskType.TType)
              {
                flag = false;
                break;
              }
            }
            if (flag)
              taskTypeList.Add(taskType);
          }
        }
        string str = string.Empty;
        if (taskTypeList.Count <= 0)
          return;
        foreach (SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure.TaskType taskType in taskTypeList)
        {
          this.selTaskItems.TaskType.Remove(taskType);
          str = str + " , " + taskType.TType;
        }
        IFloorplan floorplanObject = Utilities.GetFloorplanObject(this.Model);
        if (!floorplanObject.IsDisplayOnly)
        {
          floorplanObject.IsDirty = true;
          LoggingManager.Instance.WriteToLog("Following task types are obsolete and are removed from the seleted list:" + str + ". Please save the changes for the correction to be persisted ", LogCategory.ModelLayer);
        }
        else
          LoggingManager.Instance.WriteToLog("Following task types are obsolete and are removed from the seleted list:" + str + ". Please edit the component and save the changes for the correction to be persisted ", LogCategory.ModelLayer);
        floorplanObject.IsModelCorrected = true;
      }
      catch (Exception ex)
      {
        int num = (int) DisplayMessage.Show("SearchText Not found");
      }
    }

    private void AddTaskToList(SAP.BYD.LS.UIDesigner.Connector.TaskType taskType, TaskTypes selTaskItems)
    {
      bool flag = false;
      if (selTaskItems != null && selTaskItems.TaskType != null && selTaskItems.TaskType.Count > 0)
      {
        foreach (SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure.TaskType taskType1 in selTaskItems.TaskType)
        {
          if (taskType1.TType == taskType.Name)
          {
            this.m_SelectedTaskTypeCollection.Add(new TaskTypeItem()
            {
              Name = taskType.Name,
              Description = taskType.Description,
              Tag = (object) taskType,
              TaType = taskType1
            });
            flag = true;
            break;
          }
        }
      }
      if (flag)
        return;
      this.m_AvailableTaskTypeCollection.Add(new TaskTypeItem()
      {
        Name = taskType.Name,
        Description = taskType.Description,
        Tag = (object) taskType,
        TaType = (SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure.TaskType) null
      });
    }

    private void btnSearch_Click(object sender, RoutedEventArgs e)
    {
      if (this.searchTextBox.Text != string.Empty)
      {
        object obj = (object) this.SearchItem(this.avTaskTypes, this.searchTextBox.Text);
        if (obj != null)
        {
          this.avTaskTypes.SelectedItem = obj;
          this.ScrollToItem(this.avTaskTypes.SelectedIndex);
        }
        else
        {
          int num = (int) DisplayMessage.Show("The Search text does not match with the Available TaskTypes.");
        }
      }
      else
      {
        int num1 = (int) DisplayMessage.Show("The Search text is empty");
      }
    }

    private void btnMoveRight_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        if (this.avTaskTypes != null && this.avTaskTypes.SelectedItems.Count > 0)
        {
          if (ValidationManager.GetInstance().IsComponentEditable((IModelObject) this.centerStrucuture, true))
          {
            ObservableCollection<TaskTypeItem> observableCollection = new ObservableCollection<TaskTypeItem>();
            foreach (TaskTypeItem selectedItem in (IEnumerable) this.avTaskTypes.SelectedItems)
            {
              observableCollection.Add(selectedItem);
              SAP.BYD.LS.UIDesigner.Connector.TaskType tag = selectedItem.Tag as SAP.BYD.LS.UIDesigner.Connector.TaskType;
              if (tag != null && this.selTaskItems != null && this.selTaskItems.TaskType != null)
              {
                if (selectedItem.TaType == null)
                  selectedItem.TaType = new SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure.TaskType((IModelObject) this.selTaskItems)
                  {
                    TType = tag.Name
                  };
                this.selTaskItems.TaskType.Add(selectedItem.TaType);
              }
            }
            foreach (TaskTypeItem taskTypeItem in (Collection<TaskTypeItem>) observableCollection)
            {
              this.m_AvailableTaskTypeCollection.Remove(taskTypeItem);
              this.m_SelectedTaskTypeCollection.Add(taskTypeItem);
            }
            this.centerStrucuture.tType = this.selTaskItems;
          }
        }
        else
        {
          int num = (int) DisplayMessage.Show("Select an Item from the Left List");
        }
        if (this.avTaskTypes.Items.Count <= 0)
          return;
        this.avTaskTypes.SelectedItem = this.avTaskTypes.Items[0];
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Error while Selecting the TaskTypes", ex));
      }
    }

    private void btnMoveLeft_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        if (this.selectedTaskTypes.SelectedItems.Count > 0)
        {
          if (ValidationManager.GetInstance().IsComponentEditable((IModelObject) this.centerStrucuture, true))
          {
            if (this.selectedTaskTypes.SelectedItems.Count < this.selectedTaskTypes.Items.Count)
            {
              ObservableCollection<TaskTypeItem> observableCollection = new ObservableCollection<TaskTypeItem>();
              foreach (TaskTypeItem selectedItem in (IEnumerable) this.selectedTaskTypes.SelectedItems)
              {
                observableCollection.Add(selectedItem);
                SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure.TaskType taType = selectedItem.TaType;
                if (taType != null && this.selTaskItems != null && (this.selTaskItems.TaskType != null && this.selTaskItems.TaskType.Contains(taType)))
                  this.selTaskItems.TaskType.Remove(taType);
              }
              foreach (TaskTypeItem taskTypeItem in (Collection<TaskTypeItem>) observableCollection)
              {
                this.m_SelectedTaskTypeCollection.Remove(taskTypeItem);
                this.m_AvailableTaskTypeCollection.Add(taskTypeItem);
              }
              this.centerStrucuture.tType = this.selTaskItems;
            }
            else
            {
              int num1 = (int) DisplayMessage.Show("Atleast one item should be selected for a TaskListView. So please select the required task before you delete the current task");
            }
          }
        }
        else
        {
          int num2 = (int) DisplayMessage.Show("Select an Item from the Right List");
        }
        if (this.selectedTaskTypes.Items.Count <= 0)
          return;
        this.selectedTaskTypes.SelectedItem = this.selectedTaskTypes.Items[0];
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Error while UnSelecting the TaskTypes", ex));
      }
    }

    private TaskTypeItem SearchItem(ListView listView, string searchItemText)
    {
      TaskTypeItem taskTypeItem1 = (TaskTypeItem) null;
      try
      {
        int lineLength = this.searchTextBox.GetLineLength(0);
        foreach (TaskTypeItem taskTypeItem2 in (IEnumerable) listView.Items)
        {
          if (taskTypeItem2.Name != string.Empty)
          {
            string lower = taskTypeItem2.Name.ToString().ToLower();
            string str = string.Empty;
            if (lower.Length >= lineLength)
              str = lower.Substring(0, lineLength);
            if (str == searchItemText.ToLower())
            {
              taskTypeItem1 = taskTypeItem2;
              break;
            }
          }
        }
      }
      catch (Exception ex)
      {
        int num = (int) DisplayMessage.Show("Search Text not Found in TaskTypes");
      }
      return taskTypeItem1;
    }

    private void OnAvTasksHeaderClick(object sender, RoutedEventArgs e)
    {
      GridViewColumn column = ((GridViewColumnHeader) e.OriginalSource).Column;
      if (this.lastColumnSorted != null)
        this.lastColumnSorted.HeaderTemplate = (DataTemplate) null;
      SortDescriptionCollection sortDescriptions = this.avTaskTypes.Items.SortDescriptions;
      this.RenderSort(sortDescriptions, column, this.GetSortDirection(sortDescriptions, column));
    }

    private void OnSelTasksHeaderClick(object sender, RoutedEventArgs e)
    {
      GridViewColumn column = ((GridViewColumnHeader) e.OriginalSource).Column;
      if (this.lastColumnSorted != null)
        this.lastColumnSorted.HeaderTemplate = (DataTemplate) null;
      SortDescriptionCollection sortDescriptions = this.selectedTaskTypes.Items.SortDescriptions;
      this.RenderSort(sortDescriptions, column, this.GetSortDirection(sortDescriptions, column));
    }

    private ListSortDirection GetSortDirection(SortDescriptionCollection sorts, GridViewColumn column)
    {
      return column == this.lastColumnSorted && sorts[0].Direction == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending;
    }

    private void RenderSort(SortDescriptionCollection sorts, GridViewColumn column, ListSortDirection direction)
    {
      Binding displayMemberBinding = column.DisplayMemberBinding as Binding;
      if (displayMemberBinding == null)
        return;
      sorts.Clear();
      sorts.Add(new SortDescription(displayMemberBinding.Path.Path, direction));
      this.lastColumnSorted = column;
    }

    private void KeyDownEventHanlder(object sender, KeyEventArgs e)
    {
      if (e.Key != Key.Return || !this.searchTextBox.IsFocused)
        return;
      object obj = (object) this.SearchItem(this.avTaskTypes, this.searchTextBox.Text);
      if (obj == null)
        return;
      this.avTaskTypes.SelectedItem = obj;
      this.ScrollToItem(this.avTaskTypes.SelectedIndex);
    }

    public void ScrollToItem(int index)
    {
      base.Dispatcher.BeginInvoke(DispatcherPriority.Background, new DispatcherOperationCallback(delegate (object arg)
      {
        int count = this.avTaskTypes.Items.Count;
        if (count == 0)
        {
            return null;
        }
        if (index < 0)
        {
            this.avTaskTypes.ScrollIntoView(this.avTaskTypes.Items[0]);
        }
        else if (index < count)
        {
            this.avTaskTypes.ScrollIntoView(this.avTaskTypes.Items[index]);
        }
        else
        {
            this.avTaskTypes.ScrollIntoView(this.avTaskTypes.Items[count - 1]);
        }
        return null;
      }), null);
    }

    public override string SelectionText
    {
      get
      {
        return "Content Region";
      }
    }
  }
}
