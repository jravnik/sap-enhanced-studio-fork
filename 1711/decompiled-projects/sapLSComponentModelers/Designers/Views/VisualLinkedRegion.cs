﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualLinkedRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualLinkedRegion : BaseSelectableControl
  {
    private readonly string ADDCOMMAND = "Add Action Form";
    private readonly string COLLAPSE = "Collapse";
    private readonly string EXPAND = "Expand";
    private WrapPanel wrapPanel;
    private System.Windows.Controls.Button collapseBtn;
    private System.Windows.Controls.Image image;
    private ToolTip tooltip;

    public event EventHandler OnLinkedControlDoubleClicked;

    private ActionFormsCollection ActionForms
    {
      get
      {
        return this.UXView.ActionForms;
      }
    }

    private ModalDialogs ModalDialogs
    {
      get
      {
        return this.UXView.ModalDialogs;
      }
    }

    private SideCarViewPanels SideCarViewPanels
    {
      get
      {
        return this.UXView.SideCarViewPanels;
      }
    }

    private UXView UXView
    {
      get
      {
        return this.m_ModelObject as UXView;
      }
    }

    public VisualLinkedRegion(IModelObject uxView)
      : base(uxView, false)
    {
      if (this.ActionForms != null)
      {
        this.ActionForms.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.ActionForms.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.ActionForms.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      if (this.ModalDialogs != null)
      {
        this.ModalDialogs.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.ModalDialogs.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.ModalDialogs.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      if (this.SideCarViewPanels != null)
      {
        this.SideCarViewPanels.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.SideCarViewPanels.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.SideCarViewPanels.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      if (this.UXView.ListModificationForms != null)
      {
        this.UXView.ListModificationForms.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.UXView.ListModificationForms.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.UXView.ListModificationForms.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      this.LoadView();
    }

    public override void Terminate()
    {
      if (this.ActionForms != null)
      {
        this.ActionForms.ModelObjectAdded -= new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.ActionForms.ModelObjectRemoved -= new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.ActionForms.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      if (this.ModalDialogs != null)
      {
        this.ModalDialogs.ModelObjectAdded -= new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.ModalDialogs.ModelObjectRemoved -= new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.ModalDialogs.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      if (this.SideCarViewPanels != null)
      {
        this.SideCarViewPanels.ModelObjectAdded -= new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.SideCarViewPanels.ModelObjectRemoved -= new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.SideCarViewPanels.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      if (this.UXView.ListModificationForms != null)
      {
        this.UXView.ListModificationForms.ModelObjectAdded -= new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.UXView.ListModificationForms.ModelObjectRemoved -= new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.UXView.ListModificationForms.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      base.Terminate();
    }

    public override void ReloadView()
    {
      base.ReloadView();
      if (this.UXView.ActionForms != null)
      {
        this.UXView.ActionForms.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.UXView.ActionForms.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.UXView.ActionForms.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      if (this.ModalDialogs != null)
      {
        this.ModalDialogs.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.ModalDialogs.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.ModalDialogs.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      if (this.SideCarViewPanels != null)
      {
        this.SideCarViewPanels.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
        this.SideCarViewPanels.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
        this.SideCarViewPanels.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      }
      if (this.UXView.ListModificationForms == null)
        return;
      this.UXView.ListModificationForms.ModelObjectAdded += new ModelAddedEventHandler(this.OnModelObjectAdded);
      this.UXView.ListModificationForms.ModelObjectRemoved += new ModelRemovedEventHandler(this.OnModelObjectRemoved);
      this.UXView.ListModificationForms.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
    }

    public override void LoadView()
    {
      try
      {
        this.wrapPanel = (WrapPanel) null;
        if (this.ActionForms != null && this.ActionForms.ActionFormList != null && this.ActionForms.ActionFormList.Count > 0)
        {
          this.InitializeWrapPanel();
          foreach (IModelObject actionForm in this.ActionForms.ActionFormList)
            this.wrapPanel.Children.Add((UIElement) new VisualActionForm(actionForm));
        }
        if (this.ModalDialogs != null && this.ModalDialogs.ModalDialogsList != null && this.ModalDialogs.ModalDialogsList.Count > 0)
        {
          if (this.wrapPanel == null)
            this.InitializeWrapPanel();
          foreach (IModelObject modalDialogs in this.ModalDialogs.ModalDialogsList)
            this.wrapPanel.Children.Add((UIElement) new VisualInComponentModalDialog(modalDialogs));
        }
        if (this.UXView.PlaceableFields != null)
        {
          if (this.wrapPanel == null)
            this.InitializeWrapPanel();
          this.wrapPanel.Children.Add((UIElement) new VisualPlaceableLayout((IModelObject) this.UXView.PlaceableFields));
        }
        if (this.UXView.GridLayoutSnippets != null && this.UXView.GridLayoutSnippets.Count > 0)
        {
          if (this.wrapPanel == null)
            this.InitializeWrapPanel();
          foreach (IModelObject gridLayoutSnippet in this.UXView.GridLayoutSnippets)
            this.wrapPanel.Children.Add((UIElement) new VisualGridLayoutSnippet(gridLayoutSnippet, (IDesignerControl) this));
        }
        if (this.SideCarViewPanels != null && this.SideCarViewPanels.SideCarViewPanelList != null && this.SideCarViewPanels.SideCarViewPanelList.Count > 0)
        {
          this.InitializeWrapPanel();
          foreach (IModelObject sideCarViewPanel in this.SideCarViewPanels.SideCarViewPanelList)
            this.wrapPanel.Children.Add((UIElement) new VisualSideCarViewPanel(sideCarViewPanel));
        }
        if (this.UXView.ListModificationForms == null || this.UXView.ListModificationForms.Items == null || this.UXView.ListModificationForms.Items.Count <= 0)
          return;
        if (this.wrapPanel == null)
          this.InitializeWrapPanel();
        foreach (ListModificationForm listModificationForm in this.UXView.ListModificationForms.Items)
          this.wrapPanel.Children.Add((UIElement) new VisualListModificationForm(listModificationForm));
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("ActionForms Tray UI initialization failed.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is ActionForm)
        {
          ActionForm addedModel = e.AddedModel as ActionForm;
          if (this.wrapPanel == null)
            this.InitializeWrapPanel();
          VisualActionForm visualActionForm = new VisualActionForm((IModelObject) addedModel);
          this.wrapPanel.Children.Add((UIElement) visualActionForm);
          this.OnLinkedControlDoubleClicked((object) visualActionForm, (EventArgs) e);
        }
        else if (e.AddedModel is ModalDialog)
        {
          ModalDialog addedModel = e.AddedModel as ModalDialog;
          if (this.wrapPanel == null)
            this.InitializeWrapPanel();
          VisualInComponentModalDialog componentModalDialog = new VisualInComponentModalDialog((IModelObject) addedModel);
          this.wrapPanel.Children.Add((UIElement) componentModalDialog);
          this.OnLinkedControlDoubleClicked((object) componentModalDialog, (EventArgs) e);
        }
        else if (e.AddedModel is SideCarViewPanel)
        {
          SideCarViewPanel addedModel = e.AddedModel as SideCarViewPanel;
          if (this.wrapPanel == null)
            this.InitializeWrapPanel();
          VisualSideCarViewPanel sideCarViewPanel = new VisualSideCarViewPanel((IModelObject) addedModel);
          this.wrapPanel.Children.Add((UIElement) sideCarViewPanel);
          this.OnLinkedControlDoubleClicked((object) sideCarViewPanel, (EventArgs) e);
        }
        else if (e.AddedModel is ListModificationForm)
        {
          ListModificationForm addedModel = e.AddedModel as ListModificationForm;
          if (this.wrapPanel == null)
            this.InitializeWrapPanel();
          VisualListModificationForm modificationForm = new VisualListModificationForm(addedModel);
          this.wrapPanel.Children.Add((UIElement) modificationForm);
          this.OnLinkedControlDoubleClicked((object) modificationForm, (EventArgs) e);
        }
        else if (e.AddedModel is GridLayout)
        {
          GridLayout addedModel = e.AddedModel as GridLayout;
          if (this.wrapPanel == null)
            this.InitializeWrapPanel();
          VisualGridLayoutSnippet gridLayoutSnippet = new VisualGridLayoutSnippet((IModelObject) addedModel, (IDesignerControl) this);
          this.wrapPanel.Children.Add((UIElement) gridLayoutSnippet);
          this.OnLinkedControlDoubleClicked((object) gridLayoutSnippet, (EventArgs) e);
        }
        else if (e.AddedModel is PlaceableFields)
        {
          PlaceableFields addedModel = e.AddedModel as PlaceableFields;
          if (this.wrapPanel == null)
            this.InitializeWrapPanel();
          VisualPlaceableLayout visualPlaceableLayout = new VisualPlaceableLayout((IModelObject) addedModel);
          this.wrapPanel.Children.Add((UIElement) visualPlaceableLayout);
          this.OnLinkedControlDoubleClicked((object) visualPlaceableLayout, (EventArgs) e);
        }
        this.wrapPanel.Visibility = Visibility.Visible;
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the newly added Pane Containers.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        if (e.RemovedModel is ActionForm)
        {
          ActionForm removedModel = e.RemovedModel as ActionForm;
          foreach (UIElement child in this.wrapPanel.Children)
          {
            if (child is IDesignerControl)
            {
              IDesignerControl designerControl = child as IDesignerControl;
              if (designerControl.Model == removedModel)
              {
                designerControl.Terminate();
                this.wrapPanel.Children.Remove(child);
                break;
              }
            }
          }
        }
        else if (e.RemovedModel is PlaceableFields)
        {
          PlaceableFields removedModel = e.RemovedModel as PlaceableFields;
          foreach (UIElement child in this.wrapPanel.Children)
          {
            if (child is IDesignerControl)
            {
              IDesignerControl designerControl = child as IDesignerControl;
              if (designerControl.Model == removedModel)
              {
                designerControl.Terminate();
                this.wrapPanel.Children.Remove(child);
                break;
              }
            }
          }
        }
        else if (e.RemovedModel is ModalDialog)
        {
          ModalDialog removedModel = e.RemovedModel as ModalDialog;
          foreach (UIElement child in this.wrapPanel.Children)
          {
            if (child is IDesignerControl)
            {
              IDesignerControl designerControl = child as IDesignerControl;
              if (designerControl.Model == removedModel)
              {
                designerControl.Terminate();
                this.wrapPanel.Children.Remove(child);
                break;
              }
            }
          }
        }
        else if (e.RemovedModel is GridLayout)
        {
          GridLayout removedModel = e.RemovedModel as GridLayout;
          foreach (UIElement child in this.wrapPanel.Children)
          {
            if (child is IDesignerControl)
            {
              IDesignerControl designerControl = child as IDesignerControl;
              if (designerControl.Model == removedModel)
              {
                designerControl.Terminate();
                this.wrapPanel.Children.Remove(child);
                break;
              }
            }
          }
        }
        if (!(e.RemovedModel is SideCarViewPanel))
          return;
        SideCarViewPanel removedModel1 = e.RemovedModel as SideCarViewPanel;
        foreach (UIElement child in this.wrapPanel.Children)
        {
          if (child is IDesignerControl)
          {
            IDesignerControl designerControl = child as IDesignerControl;
            if (designerControl.Model == removedModel1)
            {
              designerControl.Terminate();
              this.wrapPanel.Children.Remove(child);
              break;
            }
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the removed Pane Containers.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem(this.ADDCOMMAND);
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) this.ADDCOMMAND)
        return;
      this.ActionForms.AddActionForm();
    }

    public override string SelectionText
    {
      get
      {
        return "Action Form Collection";
      }
    }

    private void InitializeWrapPanel()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      this.collapseBtn = new System.Windows.Controls.Button();
      this.collapseBtn.HorizontalAlignment = HorizontalAlignment.Right;
      this.collapseBtn.Height = 20.0;
      this.collapseBtn.Click += new RoutedEventHandler(this.collapseBtn_Click);
      this.tooltip = new ToolTip();
      this.tooltip.Content = (object) this.COLLAPSE;
      this.collapseBtn.ToolTip = (object) this.tooltip;
      this.image = new System.Windows.Controls.Image();
      this.image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.expandall);
      this.collapseBtn.Content = (object) this.image;
      GridUtil.PlaceElement(this.Grid, (UIElement) this.collapseBtn, 0, 0);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.wrapPanel = new WrapPanel();
      this.wrapPanel.ClipToBounds = true;
      this.wrapPanel.ItemHeight = 65.0;
      GridUtil.PlaceElement(this.Grid, (UIElement) this.wrapPanel, 1, 0);
    }

    private void collapseBtn_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        if (this.tooltip.Content.ToString() == this.COLLAPSE)
        {
          this.image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.collapseall);
          this.tooltip.Content = (object) this.EXPAND;
          this.wrapPanel.Visibility = Visibility.Collapsed;
        }
        else
        {
          this.image.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.expandall);
          this.tooltip.Content = (object) this.COLLAPSE;
          this.wrapPanel.Visibility = Visibility.Visible;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
    {
      try
      {
        if (!(e.OriginalSource is FrameworkElement))
          return;
        IDesignerControl selectableControl = (IDesignerControl) ComponentModelerUtilities.Instance.GetBaseSelectableControl(e.OriginalSource as FrameworkElement);
        if (selectableControl == null)
          return;
        this.OnLinkedControlMouseDoubleClick((object) selectableControl, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute MouseDoubleClick handler.", ex));
      }
    }

    public void OnLinkedControlMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      this.OnLinkedControlDoubleClicked(sender, (EventArgs) e);
    }
  }
}
