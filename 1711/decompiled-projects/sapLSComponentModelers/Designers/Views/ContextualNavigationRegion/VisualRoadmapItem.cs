﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion.VisualRoadmapItem
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Foundation.WizardControls;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion
{
  public class VisualRoadmapItem : BaseSelectableControl
  {
    public static readonly RoutedEvent SelectedEvent = EventManager.RegisterRoutedEvent("Selected", RoutingStrategy.Bubble, typeof (RoutedEventHandler), typeof (VisualRoadmapItem));
    private static FontInfo fontPast = ResourceUtil.GetFontInfo(RoadMapControl.FONT_VIEWPART_ROADMAP_PAST);
    private static FontInfo fontCurrent = ResourceUtil.GetFontInfo(RoadMapControl.FONT_VIEWPART_ROADMAP_CURRENT);
    private static FontInfo fontFuture = ResourceUtil.GetFontInfo(RoadMapControl.FONT_VIEWPART_ROADMAP_FUTURE);
    private static FontInfo fontNumberPast = ResourceUtil.GetFontInfo(RoadMapControl.FONT_VIEWPART_ROADMAP_NUMBERPAST);
    private static FontInfo fontNumberCurrent = ResourceUtil.GetFontInfo(RoadMapControl.FONT_VIEWPART_ROADMAP_NUMBERCURRENT);
    private static FontInfo fontNumberFuture = ResourceUtil.GetFontInfo(RoadMapControl.FONT_VIEWPART_ROADMAP_NUMBERFUTURE);
    private const double SPACER_WIDTH = 12.0;
    private int index;
    private int selectedIndex;
    private int selectedChildIndex;
    private bool isEnabled;

    private RoadmapContextualNavigationRegion RoadmapContextualNavigationRegion
    {
      get
      {
        return this.m_ModelObject.Parent.Parent as RoadmapContextualNavigationRegion;
      }
    }

    private RoadmapNavigation RoadmapNavigation
    {
      get
      {
        return this.m_ModelObject.Parent as RoadmapNavigation;
      }
    }

    private NavigationItem NavigationItem
    {
      get
      {
        return this.m_ModelObject as NavigationItem;
      }
    }

    public int Index
    {
      get
      {
        return this.index;
      }
    }

    public int SelectedChildIndex
    {
      get
      {
        return this.selectedChildIndex;
      }
      set
      {
        this.selectedChildIndex = value;
      }
    }

    public event RoutedEventHandler Selected
    {
      add
      {
        this.AddHandler(VisualRoadmapItem.SelectedEvent, (Delegate) value);
      }
      remove
      {
        this.RemoveHandler(VisualRoadmapItem.SelectedEvent, (Delegate) value);
      }
    }

    private void RaiseSelectedEvent()
    {
      this.RaiseEvent(new RoutedEventArgs(VisualRoadmapItem.SelectedEvent));
    }

    public VisualRoadmapItem(IModelObject model, int index, int selectedIndex)
      : base(model)
    {
      this.index = index;
      this.selectedIndex = selectedIndex;
      this.isEnabled = Converter.ToBoolean((DependentProperty) this.NavigationItem.Enabled, true);
      this.LoadView();
    }

    public override void LoadView()
    {
      int col1 = 0;
      string resourceInfo1;
      string resourceInfo2;
      FontInfo info1;
      FontInfo info2;
      Brush brush1;
      Brush brush2;
      if (this.index < this.selectedIndex)
      {
        resourceInfo1 = RoadMapControl.IMAGE_VIEWPART_ROADMAP_BACKGROUNDPAST;
        resourceInfo2 = this.isEnabled ? RoadMapControl.IMAGE_VIEWPART_ROADMAP_ITEMPAST : RoadMapControl.IMAGE_VIEWPART_ROADMAP_ITEMDISABLED;
        info1 = VisualRoadmapItem.fontPast;
        info2 = VisualRoadmapItem.fontNumberPast;
        brush1 = this.isEnabled ? (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_NUMBERPAST : (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_DISABLED;
        brush2 = this.isEnabled ? (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_PAST : (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_DISABLED;
      }
      else if (this.index == this.selectedIndex)
      {
        resourceInfo1 = RoadMapControl.IMAGE_VIEWPART_ROADMAP_BACKGROUNDPAST;
        resourceInfo2 = this.isEnabled ? RoadMapControl.IMAGE_VIEWPART_ROADMAP_ITEMCURRENT : RoadMapControl.IMAGE_VIEWPART_ROADMAP_ITEMDISABLED;
        info1 = this.isEnabled ? VisualRoadmapItem.fontCurrent : VisualRoadmapItem.fontPast;
        info2 = this.isEnabled ? VisualRoadmapItem.fontNumberCurrent : VisualRoadmapItem.fontNumberPast;
        brush1 = this.isEnabled ? (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_NUMBERCURRENT : (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_DISABLED;
        brush2 = this.isEnabled ? (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_CURRENT : (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_DISABLED;
        if (this.isEnabled)
        {
          SolidColorBrush roadmapCurrentoffset = RoadMapControl.BRUSH_VIEWPART_ROADMAP_CURRENTOFFSET;
        }
      }
      else
      {
        resourceInfo1 = RoadMapControl.IMAGE_VIEWPART_ROADMAP_BACKGROUNDFUTURE;
        resourceInfo2 = this.isEnabled ? RoadMapControl.IMAGE_VIEWPART_ROADMAP_ITEMFUTURE : RoadMapControl.IMAGE_VIEWPART_ROADMAP_ITEMDISABLED;
        info1 = VisualRoadmapItem.fontFuture;
        info2 = VisualRoadmapItem.fontNumberFuture;
        brush1 = this.isEnabled ? (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_NUMBERFUTURE : (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_DISABLED;
        brush2 = this.isEnabled ? (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_FUTURE : (Brush) RoadMapControl.BRUSH_VIEWPART_ROADMAP_DISABLED;
      }
      System.Windows.Controls.Image image1 = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image1, resourceInfo1);
      image1.Width = double.NaN;
      image1.Stretch = Stretch.Fill;
      image1.HorizontalAlignment = HorizontalAlignment.Stretch;
      GridUtil.AddColumnDef(this.Grid, 12.0, GridUnitType.Pixel);
      GridUtil.PlaceElement(this.Grid, (UIElement) image1, 0, col1);
      int col2 = col1 + 1;
      System.Windows.Controls.Image image2 = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image2, resourceInfo2);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) image2, 0, col2);
      TextBlock textBlock1 = new TextBlock();
      textBlock1.Text = (this.index + 1).ToString() + ".";
      textBlock1.Foreground = brush1;
      textBlock1.HorizontalAlignment = HorizontalAlignment.Center;
      textBlock1.VerticalAlignment = VerticalAlignment.Center;
      FontManager.Instance.SetFontStyle(info2, textBlock1);
      GridUtil.PlaceElement(this.Grid, (UIElement) textBlock1, 0, col2);
      int col3 = col2 + 1;
      System.Windows.Controls.Image image3 = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image3, resourceInfo1);
      image3.Width = double.NaN;
      image3.Stretch = Stretch.Fill;
      image3.HorizontalAlignment = HorizontalAlignment.Stretch;
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) image3, 0, col3);
      TextBlock textBlock2 = new TextBlock();
      textBlock2.Text = this.NavigationItem.Title == null ? string.Empty : this.NavigationItem.Title.Text;
      textBlock2.Margin = new Thickness(1.0);
      textBlock2.Foreground = brush2;
      textBlock2.VerticalAlignment = VerticalAlignment.Center;
      FontManager.Instance.SetFontStyle(info1, textBlock2);
      GridUtil.PlaceElement(this.Grid, (UIElement) textBlock2, 0, col3);
      int col4 = col3 + 1;
      if (this.index != this.selectedIndex)
        return;
      System.Windows.Controls.Image image4 = new System.Windows.Controls.Image();
      ResourceUtil.LoadImage(image4, RoadMapControl.IMAGE_VIEWPART_ROADMAP_ARROW);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, (UIElement) image4, 0, col4);
      int num = col4 + 1;
    }

    public override string SelectionText
    {
      get
      {
        return "Step";
      }
    }

    public override void SelectMe()
    {
      this.RaiseSelectedEvent();
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      MenuItem parentItem1 = this.AddContextMenuItem("Add");
      this.AddContextMenuItem("Add Step", parentItem1);
      this.AddContextMenuItem("Add StepVariant", parentItem1);
      this.AddSeperatorToContextMenu();
      MenuItem parentItem2 = this.AddContextMenuItem("Delete");
      this.AddContextMenuItem("Delete Step", parentItem2);
      NavigationItem navigationItem = this.RoadmapNavigation.NavigationItems[this.selectedIndex];
      if (navigationItem == null || navigationItem.ChildNavigationItems == null || navigationItem.ChildNavigationItems.Count <= 0)
        return;
      this.AddContextMenuItem("Delete StepVariant", parentItem2);
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (source.Header == (object) "Add Step")
        this.RoadmapContextualNavigationRegion.AddGuidedActivityStep();
      else if (source.Header == (object) "Add StepVariant")
        this.RoadmapContextualNavigationRegion.AddStepVariant(this.RoadmapNavigation.NavigationItems[this.selectedIndex]);
      else if (source.Header == (object) "Delete Step")
        this.RoadmapContextualNavigationRegion.RemoveGuidedActivityStep(this.RoadmapNavigation.NavigationItems[this.selectedIndex]);
      else if (source.Header == (object) "Delete StepVariant")
      {
        NavigationItem navigationItem = this.RoadmapNavigation.NavigationItems[this.selectedIndex];
        if (navigationItem == null)
          return;
        this.RoadmapContextualNavigationRegion.RemoveStepVariant(navigationItem.ChildNavigationItems[this.selectedChildIndex]);
      }
      else
        base.HandleContextMenuItemClick(source);
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        base.OnModelPropertyChanged(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to reflect the property changes", ex));
      }
    }
  }
}
