﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.VisualIdentificationRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views
{
  public class VisualIdentificationRegion : BaseSelectableControl
  {
    private const string RESOURCE_BACKGROUND = "#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0";
    private const string RESOURCE_TITLE_FONT = "textview.pageheadertitle.font";
    private const string SEPARATOR_IMAGE = "sapLSUISkins;component/skin.nova/Images/Bars/ToolBar/toolbar_divider.png,2,15";
    private const string ADDCOMMAND = "Add";
    private const string TICKET = "Ticket";
    private const string EXTIDR = "Extended IDR Element";
    private VisualExIdentificationRegion extIDR;
    private TicketRegion ticketArea;
    private DisplayTextControl title;
    private MenuItem addTicket;

    internal IdentificationRegion IdrRegion
    {
      get
      {
        return this.m_ModelObject as IdentificationRegion;
      }
    }

    public VisualIdentificationRegion(IModelObject idrRegion)
      : base(idrRegion, false, false)
    {
      this.title = this.IdrRegion.FloorplanTitle == null ? new DisplayTextControl(TextViewStyles.PageHeaderTitle, "") : new DisplayTextControl(TextViewStyles.PageHeaderTitle, this.IdrRegion.FloorplanTitle.Text);
      if (this.IdrRegion.TicketArea != null)
        this.ticketArea = new TicketRegion(this.IdrRegion.TicketArea);
      if (this.IdrRegion.ExtendedIdentificationArea != null)
        this.extIDR = new VisualExIdentificationRegion((IModelObject) this.IdrRegion.ExtendedIdentificationArea);
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.Padding = new Thickness(1.0);
        this.ControlBackground = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.Grid, 35.0, GridUnitType.Pixel);
        this.title.Background = (Brush) ResourceUtil.GetLinearGradientBrush("#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0");
        this.title.Padding = new Thickness(5.0, 0.0, 0.0, 0.0);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.title, 0, 0, 1, 3);
        this.LoadExtIDR();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Identification Region UI initialization failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "IDR";
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      MenuItem parentItem = this.AddContextMenuItem("Add");
      this.addTicket = this.AddContextMenuItem("Ticket", parentItem);
      if (this.IdrRegion.TicketArea != null && this.IdrRegion.TicketArea.Tickets != null && this.IdrRegion.TicketArea.Tickets.Count >= 3)
        this.addTicket.IsEnabled = false;
      this.AddContextMenuItem("Extended IDR Element", parentItem);
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == (object) "Ticket")
      {
        this.IdrRegion.AddTicket();
        if (this.IdrRegion.TicketArea.Tickets.Count >= 3)
          this.addTicket.IsEnabled = false;
        else
          this.addTicket.IsEnabled = true;
      }
      else
      {
        if (source.Header != (object) "Extended IDR Element")
          return;
        this.IdrRegion.AddExtendedIdetification();
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      IdentificationRegion identificationRegion = sender as IdentificationRegion;
      if (!(e.PropertyName == IdentificationRegion.FloorPlanTitle))
        return;
      this.title.Text = identificationRegion.FloorplanTitle != null ? identificationRegion.FloorplanTitle.Text : (string) null;
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      IModelObject addedModel = e.AddedModel;
      if (addedModel is ExtendedIdentificationRegion)
      {
        if (this.extIDR == null)
        {
          this.extIDR = new VisualExIdentificationRegion((IModelObject) (addedModel as ExtendedIdentificationRegion));
          if (this.Grid.RowDefinitions.Count < 2)
            GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
          this.extIDR.ClipToBounds = true;
          GridUtil.PlaceElement(this.Grid, (UIElement) this.extIDR, 1, 2, 1, 1);
        }
      }
      else if (addedModel is TicketArea && this.ticketArea == null)
      {
        this.ticketArea = new TicketRegion(addedModel as TicketArea);
        if (this.Grid.RowDefinitions.Count < 2)
          GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.ticketArea, 1, 0);
      }
      base.OnModelObjectAdded(sender, e);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      if (e.RemovedModel is TicketArea)
      {
        if (this.addTicket != null)
          this.addTicket.IsEnabled = true;
        this.ticketArea = (TicketRegion) null;
      }
      else
      {
        if (!(e.RemovedModel is ExtendedIdentificationRegion))
          return;
        this.extIDR = (VisualExIdentificationRegion) null;
      }
    }

    private void LoadExtIDR()
    {
      if ((this.ticketArea == null || this.ticketArea.TicketCollection == null || this.ticketArea.TicketCollection.Tickets.Count <= 0) && (this.extIDR == null || this.IdrRegion.ExtendedIdentificationArea == null))
        return;
      if (this.Grid.RowDefinitions.Count < 2)
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      if (this.ticketArea != null && !this.Grid.Children.Contains((UIElement) this.ticketArea))
      {
        GridUtil.PlaceElement(this.Grid, (UIElement) this.ticketArea, 1, 0, 1, 1);
        System.Windows.Controls.Image image = new System.Windows.Controls.Image();
        ResourceUtil.LoadImage(image, "sapLSUISkins;component/skin.nova/Images/Bars/ToolBar/toolbar_divider.png,2,15");
        image.Stretch = Stretch.Fill;
        image.Height = double.NaN;
        GridUtil.PlaceElement(this.Grid, (UIElement) image, 1, 1);
      }
      if (this.extIDR == null || this.Grid.Children.Contains((UIElement) this.extIDR))
        return;
      this.extIDR.ClipToBounds = true;
      GridUtil.PlaceElement(this.Grid, (UIElement) this.extIDR, 1, 2, 1, 1);
    }
  }
}
