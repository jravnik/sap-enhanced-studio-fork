﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualHierarchicalGraphPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.HGraphPaneControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualHierarchicalGraphPane : VisualBasePane, IContextMenuContainer
  {
    private Grid splitterGrid;
    private VisualToolbar hGraphToolbar;

    internal HierarchicalGraphPane HierarchicalGraphPane
    {
      get
      {
        return this.m_ModelObject as HierarchicalGraphPane;
      }
    }

    public VisualHierarchicalGraphPane(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "HierarchicalGraphPane";
      }
    }

    public override void ConstructBodyElement()
    {
      int row = 0;
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, 100.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      if (this.HierarchicalGraphPane.HierarchicalGraphToolbar != null)
      {
        GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
        this.hGraphToolbar = new VisualToolbar((IModelObject) this.HierarchicalGraphPane.HierarchicalGraphToolbar);
        this.hGraphToolbar.VerticalAlignment = VerticalAlignment.Top;
        this.hGraphToolbar.HorizontalAlignment = HorizontalAlignment.Stretch;
        this.hGraphToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        this.hGraphToolbar.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.hGraphToolbar, 0, 0, 1, 3);
        row = 1;
      }
      GridUtil.AddRowDef(this.BodyElementGrid, 150.0, 150.0, GridUnitType.Star);
      if (this.HierarchicalGraphPane.TemplateArea != null)
      {
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) new VisualTemplateArea((IModelObject) this.HierarchicalGraphPane.TemplateArea), row, 0);
        this.CreateSplitter();
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.splitterGrid, row, 1);
      }
      if (this.HierarchicalGraphPane.HierarchicalGraph == null)
        return;
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) new VisualHierarchicalGraph((IModelObject) this.HierarchicalGraphPane.HierarchicalGraph), row, 2);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        HierarchicalGraphPane parent = e.RemovedModel.Parent as HierarchicalGraphPane;
        if (parent != null)
          parent.TemplateArea = (TemplateArea) null;
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        if (e.PropertyName == HierarchicalGraphPane.HierarchicalGraphToolbarProperty)
        {
          this.hGraphToolbar.Toolbar = (Toolbar) this.HierarchicalGraphPane.HierarchicalGraphToolbar;
          this.hGraphToolbar.ReloadView();
        }
        else
          this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of HierarchicalGraph Pane failed.", ex));
      }
      base.OnModelPropertyChanged(sender, e);
    }

    protected override void AddContextMenuItems()
    {
      if (this.hGraphToolbar != null)
        this.hGraphToolbar.AddContextMenuItems((IContextMenuContainer) this);
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (source.Header == (object) "Delete")
      {
        if (this.HierarchicalGraphPane == null)
          return;
        PaneContainerVariant parentOfType1 = Utilities.GetParentOfType(this.Model, typeof (PaneContainerVariant)) as PaneContainerVariant;
        if (parentOfType1 == null)
        {
          PaneContainer parentOfType2 = Utilities.GetParentOfType(this.Model, typeof (PaneContainer)) as PaneContainer;
          if (parentOfType2 == null)
            return;
          parentOfType2.RemoveModelObject((IModelObject) this.HierarchicalGraphPane);
        }
        else
          parentOfType1.RemoveModelObject((IModelObject) this.HierarchicalGraphPane);
      }
      else
      {
        if (this.hGraphToolbar == null)
          return;
        this.hGraphToolbar.HandleContextMenuItemClickWrapper(source);
      }
    }

    private void CreateSplitter()
    {
      this.splitterGrid = new Grid();
      GridUtil.AddColumnDef(this.splitterGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.splitterGrid, 1.0, GridUnitType.Star);
      Rectangle rectangle = new Rectangle();
      rectangle.Width = 5.0;
      rectangle.MinHeight = 150.0;
      SolidColorBrush solidColorBrush = new SolidColorBrush(ComponentModelersConstants.NODE_BACKGROUND);
      rectangle.Fill = (Brush) solidColorBrush;
      GridUtil.PlaceElement(this.splitterGrid, (UIElement) rectangle, 0, 0);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
