﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ServiceMapPaneControls.VisualServiceLink
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ServiceMapPaneControls
{
  public class VisualServiceLink : BaseSelectableControl
  {
    private UIElement visualControl;
    private TextBlock explanationText;
    private VisualServiceMapGroup visualServiceMapGroup;
    private VisualStaticText linkControl;

    internal Link Link
    {
      get
      {
        return this.m_ModelObject as Link;
      }
    }

    public VisualServiceLink(Link link, VisualServiceMapGroup serviceMapGroup)
      : base((IModelObject) link)
    {
      this.visualServiceMapGroup = serviceMapGroup;
      this.IsDraggable = true;
      link.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      this.LoadView();
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.ControlBackground = (Brush) ComponentModelersConstants.FIELD_AREA_BRUSH;
      this.AllowDrop = true;
    }

    public override void ReloadView()
    {
      base.ReloadView();
      this.Link.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
    }

    public override void LoadView()
    {
      try
      {
        GridUtil.AddRowDef(this.Grid, 25.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        this.linkControl = new VisualStaticText(false, false, true, false, VisualStaticText.StaticTextFormats.Standard);
        this.linkControl.IsLink = true;
        this.linkControl.IsMenuVisible = this.Link.HasMenu;
        if (this.Link.Text != null)
        {
          if (!string.IsNullOrEmpty(this.Link.Text.Text))
            this.linkControl.Text = this.Link.Text.Text;
          else if (!string.IsNullOrEmpty(this.Link.Text.FallbackValue))
            this.linkControl.Text = this.Link.Text.FallbackValue;
        }
        else
          this.linkControl.Text = "Link";
        this.visualControl = (UIElement) this.linkControl;
        GridUtil.PlaceElement(this.Grid, this.visualControl, 0, 0);
        GridUtil.AddRowDef(this.Grid, 25.0, GridUnitType.Auto);
        if (this.Link.Explanation != null && !string.IsNullOrEmpty(this.Link.Explanation.Text))
        {
          this.explanationText = new TextBlock();
          this.explanationText.TextWrapping = TextWrapping.Wrap;
          this.explanationText.Background = (Brush) ComponentModelersConstants.CONTENT_AREA_BRUSH;
          this.explanationText.Text = this.Link.Explanation.Text;
          GridUtil.PlaceElement(this.Grid, (UIElement) this.explanationText, 1, 0);
        }
        this.MouseDoubleClick += new MouseButtonEventHandler(this.Field_MouseDoubleClick);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Field Control initialization failed.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        base.OnModelPropertyChanged(sender, e);
        if (e.PropertyName == Link.HasMenuProperty)
        {
          VisualStaticText visualControl = this.visualControl as VisualStaticText;
          if (this.Link == null || visualControl == null)
            return;
          visualControl.IsMenuVisible = this.Link.HasMenu;
        }
        else if (e.PropertyName == Link.TextProperty)
        {
          if (this.Link.Text == null)
            return;
          if (!string.IsNullOrEmpty(this.Link.Text.Text))
          {
            this.linkControl.Text = this.Link.Text.Text;
          }
          else
          {
            if (string.IsNullOrEmpty(this.Link.Text.FallbackValue))
              return;
            this.linkControl.Text = this.Link.Text.FallbackValue;
          }
        }
        else if (e.PropertyName == AbstractControl.ExplanationProperty)
        {
          if (this.explanationText == null)
          {
            this.explanationText = new TextBlock();
            this.explanationText.TextWrapping = TextWrapping.Wrap;
            this.explanationText.Background = (Brush) ComponentModelersConstants.CONTENT_AREA_BRUSH;
            GridUtil.PlaceElement(this.Grid, (UIElement) this.explanationText, 1, 0);
          }
          this.explanationText.Text = this.Link.Explanation.Text;
        }
        else
          this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update UI on Property Changed event.", ex));
      }
    }

    public override void Terminate()
    {
      base.Terminate();
      this.Link.PropertyChanged -= new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        base.AddContextMenuItems();
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for VisualField.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      try
      {
        base.HandleContextMenuItemClick(source);
        if (source.Header == null || !(source.Header.ToString() == "Delete"))
          return;
        ServiceMapGroup parent = this.Link.Parent as ServiceMapGroup;
        if (parent == null)
          return;
        parent.RemoveModelObject((IModelObject) this.Link);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Field";
      }
    }

    protected override void OnBaseVisualControlDrop(IModelObject droppedModel)
    {
      if (droppedModel == null || !(droppedModel is Link))
        return;
      ServiceMapGroup parent1 = droppedModel.Parent as ServiceMapGroup;
      ServiceMapGroup parent2 = this.Link.Parent as ServiceMapGroup;
      if (parent2 == null)
        return;
      parent2.Links.IndexOf(this.Link);
      parent1.RemoveModelObject(droppedModel);
      droppedModel.Parent = (IModelObject) parent2;
    }

    protected override void OnToolBoxControlDragOver(DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      base.OnToolBoxControlDragOver(e, toolboxEventArg);
      if (!ValidationManager.GetInstance().ValidateModel(this.Model, toolboxEventArg.Tag.ClassName) || this.Model == null || !(this.Model.Parent.Parent is FindFormPane))
        return;
      if (this.Model is SectionGroupItem && toolboxEventArg.Tag.ControlName == "RadioButtonGroup")
        e.Effects = DragDropEffects.None;
      else
        e.Effects = DragDropEffects.Copy;
    }

    private void Field_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
    }
  }
}
