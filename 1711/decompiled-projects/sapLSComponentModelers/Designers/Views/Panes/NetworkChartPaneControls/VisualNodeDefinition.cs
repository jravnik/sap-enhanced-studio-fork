﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.NetworkChartPaneControls.VisualNodeDefinition
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.GanttChartPaneControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.NetworkChartPaneControls
{
  public class VisualNodeDefinition : BaseSelectableControl
  {
    private BaseVisualControl parentVisualControl;
    private VisualNetworkChartNodeVariant m_ListVariant;
    private VisualNetworkChartPane networkChartPane;
    private NetworkChartNodeVariant selectedListVariant;

    internal NetworkChartNode NetworkChartNode
    {
      get
      {
        return this.m_ModelObject as NetworkChartNode;
      }
    }

    public VisualNodeDefinition(IModelObject model, BaseVisualControl parentControl)
      : base(model)
    {
      this.AllowDrop = true;
      this.parentVisualControl = parentControl;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return nameof (VisualNodeDefinition);
      }
    }

    public override void LoadView()
    {
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 30.0, GridUnitType.Pixel);
      this.networkChartPane = this.parentVisualControl as VisualNetworkChartPane;
      if (this.NetworkChartNode.ListVariant != null && this.NetworkChartNode.ListVariant.Count > 0)
      {
        if (this.selectedListVariant == null)
          this.selectedListVariant = this.NetworkChartNode.ListVariant[0];
        foreach (NetworkChartNodeVariant chartNodeVariant in this.NetworkChartNode.ListVariant)
        {
          if (chartNodeVariant == this.selectedListVariant)
          {
            this.m_ListVariant = new VisualNetworkChartNodeVariant((IModelObject) chartNodeVariant, (BaseVisualControl) this);
            break;
          }
        }
        this.networkChartPane.OnListVariantChanged -= new ListNodeVariantChanged(this.networkChartPane_OnListVariantChanged);
        this.networkChartPane.OnListVariantChanged += new ListNodeVariantChanged(this.networkChartPane_OnListVariantChanged);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.m_ListVariant, 0, 0);
      }
      if (this.NetworkChartNode.RelationshipDefinition == null)
        return;
      GridUtil.PlaceElement(this.Grid, (UIElement) new VisualNetworkRelationShip((IModelObject) this.NetworkChartNode.RelationshipDefinition), 1, 0);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is NetworkChartNode)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (e.PropertyName == NetworkChartNode.ListVariantProperty)
          this.parentVisualControl.ReloadView();
        else
          this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of NodeDefinition failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    private void networkChartPane_OnListVariantChanged(NetworkChartNodeVariant var)
    {
      this.selectedListVariant = var;
      if (this.selectedListVariant == null)
        return;
      this.ReloadView();
    }
  }
}
