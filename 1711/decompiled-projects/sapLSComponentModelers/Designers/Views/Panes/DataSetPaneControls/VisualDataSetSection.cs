﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls.VisualDataSetSection
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls
{
  public class VisualDataSetSection : BaseSelectableControl
  {
    internal DataSetSection DataSetSection
    {
      get
      {
        return this.m_ModelObject as DataSetSection;
      }
    }

    public override string SelectionText
    {
      get
      {
        return this.DataSetSection.TypeName;
      }
    }

    public VisualDataSetSection(IModelObject dataSetSection)
      : base(dataSetSection, true)
    {
      this.IsDraggable = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.Margin = new Thickness(1.0);
        this.Padding = new Thickness(1.0);
        int num = 0;
        for (int index = 0; index < this.DataSetSection.Rows; ++index)
          GridUtil.AddRowDef(this.Grid, 150.0, 150.0, GridUnitType.Auto);
        for (int index = 0; index < this.DataSetSection.Columns; ++index)
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        if (this.DataSetSection.DataSetContainers == null)
          return;
        foreach (DataSetContainer dataSetContainer in this.DataSetSection.DataSetContainers)
          GridUtil.PlaceElement(this.Grid, (UIElement) new VisualDataSetContainer((IModelObject) dataSetContainer, (BaseVisualControl) this), dataSetContainer.Row + num, dataSetContainer.Column, dataSetContainer.RowSpan, dataSetContainer.ColumnSpan);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("DataSetSection UI initialization failed.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the newly added DataSet Containers.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("UI Updation failed to update the removed DataSet Containers.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        base.OnModelPropertyChanged(sender, e);
        if (!(e.PropertyName == GridLayout.RowsProperty) && !(e.PropertyName == GridLayout.ColumnsProperty))
          return;
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to change the Property change.", ex));
      }
    }

    protected override void OnBaseVisualControlDragOver(DragEventArgs e, IModelObject droppedModel)
    {
      if (droppedModel is GridLayout && (droppedModel as GridLayout).IsSnippet)
        e.Effects = DragDropEffects.Copy;
      else
        base.OnBaseVisualControlDragOver(e, droppedModel);
    }

    private VisualDataSetContainer GetVisualDataSetContainer(DataSetContainer dsContainer)
    {
      VisualDataSetContainer dataSetContainer = (VisualDataSetContainer) null;
      foreach (UIElement child in this.Grid.Children)
      {
        dataSetContainer = child as VisualDataSetContainer;
        if (dataSetContainer != null)
        {
          if (dataSetContainer.DataSetContainer == dsContainer)
            break;
        }
      }
      return dataSetContainer;
    }

    private bool IsVisualDataSetContainerPresent(DataSetContainer dataSetContainer)
    {
      foreach (VisualDataSetContainer child in this.Grid.Children)
      {
        if (child.DataSetContainer == dataSetContainer)
          return true;
      }
      return false;
    }

    private void OnSpanRight(object source, EventArgs e)
    {
      VisualDataSetContainer dataSetContainer1 = source as VisualDataSetContainer;
      if (dataSetContainer1 == null)
        return;
      int num1 = (int) dataSetContainer1.GetValue(Grid.ColumnProperty);
      int num2 = (int) dataSetContainer1.GetValue(Grid.RowProperty);
      int num3 = (int) dataSetContainer1.GetValue(Grid.ColumnSpanProperty);
      foreach (UIElement child in this.Grid.Children)
      {
        VisualDataSetContainer dataSetContainer2 = child as VisualDataSetContainer;
        if (dataSetContainer2 != null)
        {
          int num4 = (int) dataSetContainer2.GetValue(Grid.ColumnProperty);
          int num5 = (int) dataSetContainer2.GetValue(Grid.RowProperty);
          if (num4 == num1 + 1 && num2 == num5)
          {
            this.Grid.Children.Remove((UIElement) dataSetContainer2);
            dataSetContainer1.SetValue(Grid.ColumnSpanProperty, (object) (num3 + 1));
            break;
          }
        }
      }
    }

    private void OnSpanUp(object source, EventArgs e)
    {
    }

    private void OnSpanLeft(object source, EventArgs e)
    {
    }

    private void OnSpanDown(object source, EventArgs e)
    {
      VisualDataSetContainer dataSetContainer1 = source as VisualDataSetContainer;
      if (dataSetContainer1 == null)
        return;
      int num1 = (int) dataSetContainer1.GetValue(Grid.ColumnProperty);
      int num2 = (int) dataSetContainer1.GetValue(Grid.RowProperty);
      int num3 = (int) dataSetContainer1.GetValue(Grid.ColumnSpanProperty);
      foreach (UIElement child in this.Grid.Children)
      {
        VisualDataSetContainer dataSetContainer2 = child as VisualDataSetContainer;
        if (dataSetContainer2 != null)
        {
          int num4 = (int) dataSetContainer2.GetValue(Grid.ColumnProperty);
          int num5 = (int) dataSetContainer2.GetValue(Grid.RowProperty);
          if (num4 == num1 && num5 == num2 + 1)
          {
            this.Grid.Children.Remove((UIElement) dataSetContainer2);
            dataSetContainer1.SetValue(Grid.RowSpanProperty, (object) (num3 + 1));
            break;
          }
        }
      }
    }
  }
}
