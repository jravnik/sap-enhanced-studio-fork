﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls.VisualDataSetContainer
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.EventArg;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls
{
  public class VisualDataSetContainer : BaseBorderControl
  {
    private const string DELETE = "Delete";
    private BaseVisualControl parentVisualControl;
    private VisualToolbar m_Toolbar;

    internal DataSetSection DataSetSection
    {
      get
      {
        if (this.m_ModelObject == null)
          return (DataSetSection) null;
        return this.m_ModelObject.Parent as DataSetSection;
      }
    }

    internal DataSetContainer DataSetContainer
    {
      get
      {
        return this.m_ModelObject as DataSetContainer;
      }
    }

    public VisualDataSetContainer(IModelObject model, BaseVisualControl parentControl)
      : base(model)
    {
      this.parentVisualControl = parentControl;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.AllowDrop = true;
      this.IsSpanable = true;
      this.InitializeSpanningDetails();
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        if (this.DataSetContainer.Items != null && this.DataSetContainer.Items.Count > 0)
        {
          int num = 0;
          foreach (DataSetControlContainerItem controlContainerItem in this.DataSetContainer.Items)
          {
            GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
            GridUtil.PlaceElement(this.Grid, (UIElement) new VisualDataSetContainerItem((IModelObject) controlContainerItem), num++, 0);
          }
        }
        if (!this.DataSetContainer.UseToolbar)
          return;
        this.CreateToolBar(this.DataSetContainer);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new ModelLayerException("Loading of Visual DataSet container failed.", ex), true);
      }
    }

    private void CreateToolBar(DataSetContainer dataSetContainer)
    {
      if (dataSetContainer.Toolbar == null)
        return;
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      VisualToolbar visualToolbar = new VisualToolbar((IModelObject) dataSetContainer.Toolbar);
      this.m_Toolbar = visualToolbar;
      visualToolbar.VerticalAlignment = VerticalAlignment.Top;
      visualToolbar.HorizontalAlignment = HorizontalAlignment.Stretch;
      visualToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      visualToolbar.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
      visualToolbar.VerticalAlignment = VerticalAlignment.Bottom;
      GridUtil.PlaceElement(this.Grid, (UIElement) visualToolbar, this.Grid.RowDefinitions.Count, 0);
    }

    public override string SelectionText
    {
      get
      {
        return "DataSetContainer";
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new ModelLayerException("UI Updating of ModelObjectAdded event in DataSetContainer failed.", ex), true);
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new ModelLayerException("UI Updating of ModelObjectRemoved event in DataSetContainer failed.", ex), true);
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      if (e.PropertyName == PaneContainer.ItemsProperty || e.PropertyName == DataSetContainer.UseToolBarProperty)
        this.ReloadView();
      else if (e.PropertyName == PaneContainer.ColumnProperty || e.PropertyName == PaneContainer.RowProperty)
      {
        if (this.parentVisualControl == null)
          return;
        this.parentVisualControl.ReloadView();
      }
      else
      {
        if (!(e.PropertyName == DataSetContainer.ToolbarProperty))
          return;
        this.m_Toolbar.Toolbar = (Toolbar) this.DataSetContainer.Toolbar;
        this.m_Toolbar.ReloadView();
      }
    }

    protected override void OnSpanDownArrowClicked(object sender, EventArgs e)
    {
      try
      {
        base.OnSpanDownArrowClicked(sender, e);
        this.DataSetSection.IncrementRowSpan((IContainer) this.DataSetContainer);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnSpanLeftArrowClicked(object sender, EventArgs e)
    {
      try
      {
        base.OnSpanLeftArrowClicked(sender, e);
        this.DataSetSection.DecrementColSpan((IContainer) this.DataSetContainer);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnSpanRightArrowClicked(object sender, EventArgs e)
    {
      try
      {
        base.OnSpanRightArrowClicked(sender, e);
        this.DataSetSection.IncrementColSpan((IContainer) this.DataSetContainer);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnSpanUpArrowClicked(object sender, EventArgs e)
    {
      try
      {
        base.OnSpanUpArrowClicked(sender, e);
        this.DataSetSection.DecrementRowSpan((IContainer) this.DataSetContainer);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to execute span operation", ex));
      }
    }

    protected override void OnBOElementDragOver(DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      base.OnBOElementDragOver(e, boBrowserArgs);
      if (e.Effects == DragDropEffects.None)
        return;
      BOElement boElement = (BOElement) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
      {
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
        if (boElement == null)
        {
          BOElementAttribute draggedElement = boBrowserArgs[0].DraggedElement as BOElementAttribute;
          if (draggedElement != null)
            boElement = draggedElement.ParentElement;
        }
      }
      if (boElement != null && boElement.ParentQuery != null)
        e.Effects = DragDropEffects.None;
      if (boElement == null || boElement.ParentAction == null)
        return;
      e.Effects = DragDropEffects.None;
    }

    protected override void OnBOElementDrop(BOBrowserDragEventArgs boBrowserArgs)
    {
      foreach (BOBrowserDragEventArg boBrowserArg in (List<BOBrowserDragEventArg>) boBrowserArgs)
        this.DataSetContainer.AddModelObject(boBrowserArg);
    }

    protected override void OnComponentDragOver(DragEventArgs e, ConfigurationExplorerDragEventArgs ceArgs)
    {
      base.OnComponentDragOver(e, ceArgs);
      if (e.Effects == DragDropEffects.None)
        return;
      e.Effects = DragDropEffects.None;
    }

    private void LoadDataSetContainerView()
    {
      this.Grid.Children.Clear();
      if (this.DataSetContainer.Items == null || this.DataSetContainer.Items.Count <= 0)
        return;
      int num = 0;
      foreach (DataSetControlContainerItem controlContainerItem in this.DataSetContainer.Items)
      {
        GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) new VisualDataSetContainerItem((IModelObject) controlContainerItem), num++, 0);
      }
    }

    public void InitializeSpanningDetails()
    {
      this.InitializeSpanningDetails(new SpanningInfo(this.DataSetSection.Columns, this.DataSetSection.Rows, this.DataSetContainer.Column, this.DataSetContainer.Row, this.DataSetContainer.ColumnSpan, this.DataSetContainer.RowSpan), SpanningStyles.both);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (source.Header != (object) "Delete")
        return;
      this.DataSetSection.RemoveModelObject((IModelObject) this.DataSetContainer);
    }
  }
}
