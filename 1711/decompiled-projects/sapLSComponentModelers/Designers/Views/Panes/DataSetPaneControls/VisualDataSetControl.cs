﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls.VisualDataSetControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.DataSetPaneControls
{
  public class VisualDataSetControl : BaseSelectableControl
  {
    private const string ADD = "Add";
    private const string DELETE = "Delete";
    private VisualDataSetSection m_basicSectionControl;
    private VisualDataSetSection m_advancedSectionControl;

    private DataSetControl DataSetControl
    {
      get
      {
        return this.m_ModelObject as DataSetControl;
      }
    }

    public VisualDataSetControl(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.m_basicSectionControl = new VisualDataSetSection((IModelObject) this.DataSetControl.BasicSection);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.Grid, 150.0, 150.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.m_basicSectionControl, 0, 0);
        if (!this.DataSetControl.HasAdvancedSection)
          return;
        GridUtil.AddRowDef(this.Grid, 150.0, 150.0, GridUnitType.Auto);
        this.m_advancedSectionControl = new VisualDataSetSection((IModelObject) this.DataSetControl.AdvancedSection);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.m_advancedSectionControl, 1, 0);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of DataSet Control failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "DataSetControl";
      }
    }

    protected override void OnBOElementDragOver(DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      base.OnBOElementDragOver(e, boBrowserArgs);
      if (e.Effects == DragDropEffects.None)
        return;
      BOElement boElement = (BOElement) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
      if (boElement != null && boElement.ParentQuery != null)
        e.Effects = DragDropEffects.None;
      if (boElement == null || boElement.ParentAction == null)
        return;
      e.Effects = DragDropEffects.None;
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      this.ReloadView();
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == null || !(source.Header.ToString() == "Delete"))
        return;
      AdvancedDataSetPane parentOfType = Utilities.GetParentOfType(this.Model, typeof (AdvancedDataSetPane)) as AdvancedDataSetPane;
      if (parentOfType == null || parentOfType.Parent == null)
        return;
      parentOfType.Parent.RemoveModelObject((IModelObject) parentOfType);
    }
  }
}
