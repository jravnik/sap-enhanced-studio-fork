﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.HGraphPaneControls.VisualCellData
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.HGraphPaneControls
{
  public class VisualCellData : BaseSelectableControl
  {
    private BaseVisualControl parentVisualControl;

    internal CellData CellData
    {
      get
      {
        return this.m_ModelObject as CellData;
      }
    }

    public VisualCellData(IModelObject model, BaseVisualControl parentControl)
      : base(model)
    {
      this.AllowDrop = true;
      this.parentVisualControl = parentControl;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.Grid, 15.0, GridUnitType.Pixel);
      this.Grid.Background = (Brush) new SolidColorBrush(ComponentModelersConstants.NODE_BACKGROUND);
      TextBlock textBlock = new TextBlock();
      textBlock.Margin = new Thickness(5.0, 2.0, 2.0, 2.0);
      textBlock.Text = this.CellData.Item != null ? this.CellData.Item.DisplayType.ToString() : (string) null;
      textBlock.SetValue(FrameworkElement.VerticalAlignmentProperty, (object) VerticalAlignment.Center);
      textBlock.SetValue(FrameworkElement.HorizontalAlignmentProperty, (object) HorizontalAlignment.Left);
      GridUtil.PlaceElement(this.Grid, (UIElement) textBlock, 0, 0);
    }

    public override string SelectionText
    {
      get
      {
        return "CellData";
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is CellData)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (e.PropertyName == "Position")
        {
          if (this.parentVisualControl == null)
            return;
          this.parentVisualControl.ReloadView();
        }
        else
          this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of CellData failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) "Delete")
        return;
      NodeConfiguration parent1 = this.CellData.Parent as NodeConfiguration;
      if (parent1 != null)
      {
        parent1.RemoveCellDataElementFromNode(this.CellData);
      }
      else
      {
        Template parent2 = this.CellData.Parent as Template;
        if (parent2 == null)
          return;
        parent2.RemoveCellDataElementFromTemplate(this.CellData);
      }
    }
  }
}
