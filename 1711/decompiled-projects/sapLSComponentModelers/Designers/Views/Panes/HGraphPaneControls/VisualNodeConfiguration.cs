﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.HGraphPaneControls.VisualNodeConfiguration
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.HGraphPaneControls
{
  public class VisualNodeConfiguration : BaseSelectableControl
  {
    private List<CellData> m_CellData;
    private Grid containerGrid;
    private StackPanel stackPanel;
    private Grid cellDataGrid;
    private Grid buttonGrid;

    internal NodeConfiguration NodeConfiguration
    {
      get
      {
        return this.m_ModelObject as NodeConfiguration;
      }
    }

    public VisualNodeConfiguration(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      this.m_CellData = this.NodeConfiguration.CellData;
      GridUtil.AddColumnDef(this.Grid, 110.0, 110.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.stackPanel = new StackPanel();
      this.stackPanel.Orientation = Orientation.Vertical;
      if (!string.IsNullOrEmpty(this.NodeConfiguration.NodeType))
        this.CreateNodeTypeHeader();
      this.CreateCellDataRegion();
      GridUtil.PlaceElement(this.Grid, (UIElement) this.stackPanel, 0, 0);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is CellData)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of NodeConfiguration failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header != (object) "Delete")
        return;
      HierarchicalGraph parent = this.NodeConfiguration.Parent as HierarchicalGraph;
      if (parent == null)
        return;
      parent.RemoveNodeFromHierarchicalGraph(this.NodeConfiguration);
    }

    protected override void OnToolBoxControlDragOver(DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      base.OnToolBoxControlDragOver(e, toolboxEventArg);
      if (!ValidationManager.GetInstance().ValidateModel(this.Model, toolboxEventArg.Tag.ClassName))
        return;
      if (this.Model is NodeConfiguration)
        e.Effects = DragDropEffects.Copy;
      else
        e.Effects = DragDropEffects.None;
    }

    public override string SelectionText
    {
      get
      {
        return "NodeConfiguration";
      }
    }

    private void CreateNodeTypeHeader()
    {
      TextBlock textBlock = new TextBlock();
      textBlock.Text = this.NodeConfiguration.NodeType;
      textBlock.Margin = new Thickness(30.0, 2.0, 30.0, 2.0);
      textBlock.HorizontalAlignment = HorizontalAlignment.Center;
      textBlock.VerticalAlignment = VerticalAlignment.Center;
      this.stackPanel.Children.Add((UIElement) textBlock);
    }

    private void CreateCellDataRegion()
    {
      this.containerGrid = new Grid();
      GridUtil.AddColumnDef(this.containerGrid, 110.0, 110.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.containerGrid, 60.0, 60.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.containerGrid, 2.0, 2.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.containerGrid, 15.0, 15.0, GridUnitType.Auto);
      this.cellDataGrid = new Grid();
      this.cellDataGrid.Background = (Brush) new SolidColorBrush(ComponentModelersConstants.NODE_BACKGROUND);
      if (this.m_CellData != null && this.m_CellData.Count > 0)
      {
        int row1 = 0;
        int col1 = 0;
        GridUtil.AddColumnDef(this.cellDataGrid, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.cellDataGrid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.cellDataGrid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.cellDataGrid, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.cellDataGrid, 1.0, GridUnitType.Star);
        foreach (CellData cellData in this.m_CellData)
        {
          int row2 = cellData.Position / 2;
          int col2 = cellData.Position % 2 != 0 ? 1 : 0;
          if (col1 == 2)
          {
            ++row1;
            col1 = 0;
          }
          VisualCellData visualCellData = new VisualCellData((IModelObject) cellData, (BaseVisualControl) this);
          if (cellData.Position >= this.m_CellData.Count - 1)
            GridUtil.PlaceElement(this.cellDataGrid, (UIElement) visualCellData, row2, col2);
          else
            GridUtil.PlaceElement(this.cellDataGrid, (UIElement) visualCellData, row1, col1);
          ++col1;
        }
      }
      else
        GridUtil.AddRowDef(this.cellDataGrid, 1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.containerGrid, (UIElement) this.cellDataGrid, 0, 0);
      this.CreateNode();
      this.stackPanel.Children.Add((UIElement) this.containerGrid);
    }

    private void CreateNode()
    {
      Grid grid1 = new Grid();
      GridUtil.AddColumnDef(grid1, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(grid1, 2.0, 2.0, GridUnitType.Auto);
      grid1.Background = (Brush) Brushes.Gray;
      GridUtil.PlaceElement(this.containerGrid, (UIElement) grid1, 1, 0);
      Grid grid2 = new Grid();
      GridUtil.AddColumnDef(grid2, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(grid2, 15.0, 15.0, GridUnitType.Auto);
      grid2.Background = (Brush) new SolidColorBrush(ComponentModelersConstants.NODE_BACKGROUND);
      this.CreateExpandCollapseBlock();
      GridUtil.PlaceElement(grid2, (UIElement) this.buttonGrid, 0, 0);
      GridUtil.PlaceElement(this.containerGrid, (UIElement) grid2, 2, 0);
    }

    private void CreateExpandCollapseBlock()
    {
      this.buttonGrid = new Grid();
      GridUtil.AddColumnDef(this.buttonGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.buttonGrid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.buttonGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.buttonGrid, 1.0, GridUnitType.Auto);
      TextBlock textBlock1 = new TextBlock();
      textBlock1.Text = "+";
      textBlock1.FontWeight = FontWeights.Bold;
      textBlock1.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.buttonGrid, (UIElement) textBlock1, 0, 0);
      TextBlock textBlock2 = new TextBlock();
      textBlock2.Text = "-";
      textBlock2.FontWeight = FontWeights.Bold;
      textBlock2.Margin = new Thickness(0.0, 0.0, 5.0, 0.0);
      GridUtil.PlaceElement(this.buttonGrid, (UIElement) textBlock2, 0, 2);
    }
  }
}
