﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualSplitPaneMasterItem
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualSplitPaneMasterItem : BaseBorderControl
  {
    private Grid splitPaneItem;
    private VisualActionButton actionButtonMenu;
    private WrapPanel sp;
    private VisualTextControl textControl;
    private Grid m_Container;

    internal SplitPaneMasterItem SplitPaneItem
    {
      get
      {
        return this.m_ModelObject as SplitPaneMasterItem;
      }
    }

    public VisualSplitPaneMasterItem(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.m_Container = new Grid();
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.BorderBrush = (Brush) Brushes.Purple;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.splitPaneItem = new Grid();
        this.splitPaneItem.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
        this.m_Container.ColumnDefinitions.Add(new ColumnDefinition());
        this.m_Container.RowDefinitions.Add(new RowDefinition());
        GridUtil.PlaceElement(this.m_Container, (UIElement) this.splitPaneItem, 1, 0);
        GridUtil.AddColumnDef(this.splitPaneItem, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.splitPaneItem, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.splitPaneItem, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.splitPaneItem, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.splitPaneItem, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.splitPaneItem, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.splitPaneItem, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.splitPaneItem, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.splitPaneItem, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.splitPaneItem, 1.0, GridUnitType.Star);
        GridUtil.AddRowDef(this.splitPaneItem, 1.0, GridUnitType.Star);
        this.splitPaneItem.MinWidth = 150.0;
        this.textControl = new VisualTextControl((IModelObject) this.SplitPaneItem.Intro);
        this.textControl.Text = "INTRO";
        GridUtil.PlaceElement(this.splitPaneItem, (UIElement) this.textControl, 0, 0);
        this.textControl = new VisualTextControl((IModelObject) this.SplitPaneItem.Header);
        this.textControl.Text = "HEADER";
        GridUtil.PlaceElement(this.splitPaneItem, (UIElement) this.textControl, 1, 0);
        this.textControl = new VisualTextControl((IModelObject) this.SplitPaneItem.Number);
        this.textControl.Text = "NUMBER";
        GridUtil.PlaceElement(this.splitPaneItem, (UIElement) this.textControl, 0, 3);
        this.textControl = new VisualTextControl((IModelObject) this.SplitPaneItem.NumberUnit);
        this.textControl.Text = "NUMBER UNIT";
        GridUtil.PlaceElement(this.splitPaneItem, (UIElement) this.textControl, 1, 3);
        this.textControl = new VisualTextControl((IModelObject) this.SplitPaneItem.FirstStatus);
        this.textControl.Text = "FIRST STATUS";
        GridUtil.PlaceElement(this.splitPaneItem, (UIElement) this.textControl, 5, 3);
        this.textControl = new VisualTextControl((IModelObject) this.SplitPaneItem.SecondStatus);
        this.textControl.Text = "SECOND STATUS";
        GridUtil.PlaceElement(this.splitPaneItem, (UIElement) this.textControl, 6, 3);
        this.Content = (object) this.m_Container;
      }
      catch
      {
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is FieldGroup)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed because of unknown reasons.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
    }

    protected override void OnBOElementDragOver(DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      base.OnBOElementDragOver(e, boBrowserArgs);
      if (e.Effects == DragDropEffects.None)
        return;
      BOElement boElement = (BOElement) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
      {
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
        if (boElement == null)
        {
          BOElementAttribute draggedElement = boBrowserArgs[0].DraggedElement as BOElementAttribute;
          if (draggedElement != null)
            boElement = draggedElement.ParentElement;
        }
      }
      if (boElement != null && boElement.ParentQuery != null)
        e.Effects = DragDropEffects.None;
      if (boElement == null || boElement.ParentAction == null)
        return;
      PaneContainer parentLayoutCell = Utilities.GetParentLayoutCell(this.m_ModelObject);
      if (parentLayoutCell == null || parentLayoutCell.Parent is ActionForm)
        return;
      e.Effects = DragDropEffects.None;
    }

    protected override void OnToolBoxControlDragOver(DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      if (toolboxEventArg.Tag.ControlName.Equals("LayoutGridPane") || toolboxEventArg.Tag.ControlName.Equals("LayoutStackPane") || toolboxEventArg.Tag.ControlName.Equals("LayoutBorderPane"))
        e.Effects = DragDropEffects.None;
      else
        base.OnToolBoxControlDragOver(e, toolboxEventArg);
    }

    protected override void OnDataModelElementDragOver(DragEventArgs e, DataModelDragEventArgs dataModelEventArgs)
    {
      if (dataModelEventArgs.DataElement.IsQueryBinding)
        e.Effects = DragDropEffects.None;
      else
        base.OnDataModelElementDragOver(e, dataModelEventArgs);
    }

    protected override void OnBaseVisualControlDragOver(DragEventArgs e, IModelObject droppedModel)
    {
      if (droppedModel is FieldGroup && (droppedModel as FieldGroup).Parent.GetType() != typeof (FormPane))
        e.Effects = DragDropEffects.None;
      else
        base.OnBaseVisualControlDragOver(e, droppedModel);
    }

    public override string SelectionText
    {
      get
      {
        return "SplitPaneMasterItem";
      }
    }

    public override void Terminate()
    {
      base.Terminate();
    }

    public override void ReloadView()
    {
      base.ReloadView();
    }

    internal override void TriggerSelection(BaseSelectableControl vControl, bool isTriggeredByControlAddOperation)
    {
      if (isTriggeredByControlAddOperation)
      {
        if (!(ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) is ComponentDesigner))
          return;
        base.TriggerSelection((BaseSelectableControl) this, isTriggeredByControlAddOperation);
      }
      else
        base.TriggerSelection((BaseSelectableControl) this, isTriggeredByControlAddOperation);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
