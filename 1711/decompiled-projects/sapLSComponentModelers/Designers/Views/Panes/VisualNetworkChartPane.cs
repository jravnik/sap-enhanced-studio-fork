﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualNetworkChartPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.NetworkChartPaneControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualNetworkChartPane : VisualBasePane, IContextMenuContainer
  {
    private VisualToolbar networkChartToolbar;
    private Grid selectionVarGrid;
    private ComboBox m_ShowComboBox;
    private VisualTemplateDefinition m_TemplateDefinition;
    private VisualNodeDefinition m_NodeDefinition;
    private Grid splitterGrid;

    public event ListNodeVariantChanged OnListVariantChanged;

    internal NetworkChartPane NetworkChartPane
    {
      get
      {
        return this.m_ModelObject as NetworkChartPane;
      }
    }

    public VisualNetworkChartPane(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "NetworkChartPane";
      }
    }

    public override void ConstructBodyElement()
    {
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
      if (this.NetworkChartPane.NodeDefinition != null && this.NetworkChartPane.NodeDefinition.ListVariant != null && this.NetworkChartPane.NodeDefinition.ListVariant.Count > 1)
      {
        this.CreateSelectionListVariantArea();
        if (this.NetworkChartPane.Toolbar != null)
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.selectionVarGrid, 0, 0);
        else
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.selectionVarGrid, 0, 0, 1, 2);
      }
      if (this.NetworkChartPane.Toolbar != null)
      {
        this.networkChartToolbar = new VisualToolbar((IModelObject) this.NetworkChartPane.Toolbar);
        this.networkChartToolbar.VerticalAlignment = VerticalAlignment.Top;
        this.networkChartToolbar.HorizontalAlignment = HorizontalAlignment.Stretch;
        this.networkChartToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        this.networkChartToolbar.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
        if (this.NetworkChartPane.NodeDefinition != null && this.NetworkChartPane.NodeDefinition.ListVariant != null && this.NetworkChartPane.NodeDefinition.ListVariant.Count > 1)
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.networkChartToolbar, 0, 1);
        else
          GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.networkChartToolbar, 0, 0, 1, 2);
      }
      else
        this.networkChartToolbar = (VisualToolbar) null;
      if (this.NetworkChartPane.TemplateDefinition != null)
      {
        this.m_TemplateDefinition = new VisualTemplateDefinition((IModelObject) this.NetworkChartPane.TemplateDefinition, (BaseVisualControl) this);
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.m_TemplateDefinition, 1, 0, 1, 1);
      }
      this.CreateSplitter();
      GridUtil.PlaceElement(this.Grid, (UIElement) this.splitterGrid, 1, 1, 1, 1);
      if (this.NetworkChartPane.NodeDefinition == null)
        return;
      this.DisposeEvent();
      this.m_NodeDefinition = new VisualNodeDefinition((IModelObject) this.NetworkChartPane.NodeDefinition, (BaseVisualControl) this);
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.m_NodeDefinition, 1, 2, 1, 1);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        if (e.PropertyName == NetworkChartPane.ToolbarProperty)
        {
          this.networkChartToolbar.Toolbar = (Toolbar) this.NetworkChartPane.Toolbar;
          this.networkChartToolbar.ReloadView();
        }
        else
          this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of GanttChart Pane failed.", ex));
      }
      base.OnModelPropertyChanged(sender, e);
    }

    protected override void AddContextMenuItems()
    {
      if (this.networkChartToolbar != null)
        this.networkChartToolbar.AddContextMenuItems((IContextMenuContainer) this);
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(MenuItem source)
    {
      if (source.Header == (object) "Delete")
      {
        if (this.NetworkChartPane == null)
          return;
        PaneContainerVariant parentOfType1 = Utilities.GetParentOfType(this.Model, typeof (PaneContainerVariant)) as PaneContainerVariant;
        if (parentOfType1 == null)
        {
          PaneContainer parentOfType2 = Utilities.GetParentOfType(this.Model, typeof (PaneContainer)) as PaneContainer;
          if (parentOfType2 == null)
            return;
          parentOfType2.RemoveModelObject((IModelObject) this.NetworkChartPane);
        }
        else
          parentOfType1.RemoveModelObject((IModelObject) this.NetworkChartPane);
      }
      else
      {
        if (this.networkChartToolbar == null)
          return;
        this.networkChartToolbar.HandleContextMenuItemClickWrapper(source);
      }
    }

    private void CreateSelectionListVariantArea()
    {
      this.selectionVarGrid = new Grid();
      this.selectionVarGrid.Background = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
      GridUtil.AddColumnDef(this.selectionVarGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.selectionVarGrid, 1.0, GridUnitType.Auto);
      this.m_ShowComboBox = new ComboBox();
      this.LoadVariantNames();
      this.m_ShowComboBox.SelectionChanged += new SelectionChangedEventHandler(this.OnSelectionChanged);
      this.m_ShowComboBox.HorizontalAlignment = HorizontalAlignment.Left;
      this.m_ShowComboBox.VerticalAlignment = VerticalAlignment.Center;
      this.m_ShowComboBox.Margin = new Thickness(5.0, 5.0, 5.0, 5.0);
      this.m_ShowComboBox.Width = 150.0;
      GridUtil.PlaceElement(this.selectionVarGrid, (UIElement) this.m_ShowComboBox, 0, 1);
    }

    private void LoadVariantNames()
    {
      this.m_ShowComboBox.Items.Clear();
      int count = this.NetworkChartPane.NodeDefinition.ListVariant.Count;
      foreach (NetworkChartNodeVariant chartNodeVariant in this.NetworkChartPane.NodeDefinition.ListVariant)
      {
        if (chartNodeVariant.Title != null && !string.IsNullOrEmpty(chartNodeVariant.Title.Text))
          this.m_ShowComboBox.Items.Add((object) chartNodeVariant.Title.Text);
        else
          this.m_ShowComboBox.Items.Add((object) chartNodeVariant.Name);
      }
      if (this.NetworkChartPane.NodeDefinition.ListVariant.Count <= 0 || this.NetworkChartPane.NodeDefinition.ListVariant[0].Title == null)
        return;
      this.m_ShowComboBox.Text = this.NetworkChartPane.NodeDefinition.ListVariant[0].Title.Text;
    }

    private void CreateSplitter()
    {
      this.splitterGrid = new Grid();
      GridUtil.AddColumnDef(this.splitterGrid, 1.0, GridUnitType.Auto);
      GridUtil.AddRowDef(this.splitterGrid, 1.0, GridUnitType.Star);
      Rectangle rectangle = new Rectangle();
      rectangle.Width = 5.0;
      rectangle.MinHeight = 150.0;
      GridUtil.PlaceElement(this.splitterGrid, (UIElement) rectangle, 0, 0);
    }

    private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.OnListVariantChanged == null || this.m_ShowComboBox.SelectedIndex == -1 || this.NetworkChartPane.NodeDefinition.ListVariant.Count <= this.m_ShowComboBox.SelectedIndex)
        return;
      this.OnListVariantChanged(this.NetworkChartPane.NodeDefinition.ListVariant[this.m_ShowComboBox.SelectedIndex]);
    }

    public void DisposeEvent()
    {
      if (this.OnListVariantChanged == null)
        return;
      this.OnListVariantChanged = (ListNodeVariantChanged) null;
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public MenuItem AddContextMenuItemWrapper(string menuHeader, MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
