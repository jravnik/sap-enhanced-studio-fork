﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls.VisualList
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls
{
  public class VisualList : BaseSelectableControl
  {
    public static readonly Brush BRUSH_TABLE_BORDER = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 135, (byte) 163, (byte) 192));
    public static readonly Brush BRUSH_TABLE_BACKGROUND = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 212, (byte) 217, (byte) 222));
    public static readonly Brush BRUSH_TABLEHEADER_SEPARATOR = (Brush) new SolidColorBrush(Color.FromRgb((byte) 158, (byte) 169, (byte) 183));
    public static readonly Brush BRUSH_TABLE_TOP_HIGHLIGHT = (Brush) new SolidColorBrush(Color.FromRgb((byte) 240, (byte) 241, (byte) 242));
    public static readonly Brush BRUSH_TABLE_GRID = (Brush) new SolidColorBrush(Color.FromRgb((byte) 190, (byte) 201, (byte) 213));
    public static readonly Brush BRUSH_TABLE_CELL = (Brush) new SolidColorBrush(Color.FromRgb(byte.MaxValue, byte.MaxValue, byte.MaxValue));
    public static readonly Brush BRUSH_TABLE_TRANSPARENT = (Brush) Brushes.Transparent;
    public static readonly double WIDTH_SELECTION_BUTTON = 20.0;
    private const string ADD = "Add";
    private const string DELETE = "Delete";
    internal List<ListColumn> m_VisibleHeaders;
    private Border tableBorder;
    private int columnCount;
    private bool hasColumnHeaders;
    private ListDesignType tableDesign;
    public Grid contentGrid;
    private System.Windows.Controls.Primitives.ScrollBar verticalScrollBar;
    private Grid tableContainer;
    private ScrollViewer horizontalScrollViewer;
    private List list;
    private Dictionary<ListColumn, VisualListColumn> m_VisualListColumns;

    private List<ListColumn> VisibleHeaders
    {
      get
      {
        return this.m_VisibleHeaders;
      }
      set
      {
        this.m_VisibleHeaders.Clear();
        foreach (ListColumn header in this.list.Headers)
        {
          if (Converter.ToBoolean((DependentProperty) header.Item.Visible, true))
            this.m_VisibleHeaders.Add(header);
        }
      }
    }

    public VisualList(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.list = model as List;
      this.m_VisualListColumns = new Dictionary<ListColumn, VisualListColumn>();
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.tableBorder = new Border();
        this.tableBorder.BorderBrush = VisualList.BRUSH_TABLE_BORDER;
        this.tableBorder.BorderThickness = new Thickness(1.0);
        this.Content = (object) this.tableBorder;
        this.tableContainer = new Grid();
        GridUtil.AddColumnDef(this.tableContainer, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.tableContainer, 18.0, GridUnitType.Pixel);
        new RowDefinition().Height = new GridLength(1.0, GridUnitType.Auto);
        this.tableBorder.Child = (UIElement) this.tableContainer;
        this.horizontalScrollViewer = new ScrollViewer();
        this.horizontalScrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
        this.horizontalScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
        this.horizontalScrollViewer.Background = VisualList.BRUSH_TABLE_BACKGROUND;
        this.horizontalScrollViewer.BorderThickness = new Thickness(0.0);
        this.tableContainer.Children.Add((UIElement) this.horizontalScrollViewer);
        this.contentGrid = new Grid();
        this.contentGrid.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        this.horizontalScrollViewer.Content = (object) this.contentGrid;
        this.columnCount = this.list.Headers.Count;
        this.tableDesign = this.list.TableDesign;
        this.hasColumnHeaders = this.list.ShowHeaderRow;
        this.m_VisibleHeaders = new List<ListColumn>();
        this.m_VisibleHeaders = this.list.Headers;
        this.InitializeHeaderControls();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of List Control failed.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "List";
      }
    }

    protected override void OnBOElementDragOver(System.Windows.DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      base.OnBOElementDragOver(e, boBrowserArgs);
      if (e.Effects == System.Windows.DragDropEffects.None)
        return;
      BOElement boElement = (BOElement) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
      if (boElement != null && boElement.ParentQuery != null)
        e.Effects = System.Windows.DragDropEffects.None;
      if (boElement == null || boElement.ParentAction == null || this.m_ModelObject.Parent is ActionForm)
        return;
      e.Effects = System.Windows.DragDropEffects.None;
    }

    private void InitializeHeaderControls()
    {
      this.m_VisualListColumns.Clear();
      int column1 = 0;
      this.AddRectangle(0, column1, 1, 1, 1.0, VisualList.BRUSH_TABLE_GRID);
      int col = column1 + 1;
      if (this.list.TableDesign != ListDesignType.transparent && this.list.SelectOption != ListSelectionOptionType.none)
      {
        GridUtil.AddColumnDef(this.contentGrid, VisualList.WIDTH_SELECTION_BUTTON, GridUnitType.Pixel);
        VisualRowSelectorColumn rowSelectorColumn = new VisualRowSelectorColumn((IModelObject) this.list, (BaseSelectableControl) this);
        rowSelectorColumn.Margin = new Thickness(0.0);
        rowSelectorColumn.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        rowSelectorColumn.VerticalAlignment = VerticalAlignment.Top;
        GridUtil.PlaceElement(this.contentGrid, (UIElement) rowSelectorColumn, 0, col);
        ++col;
      }
      for (int index = 0; index < this.list.Headers.Count; ++index)
      {
        GridUtil.AddColumnDef(this.contentGrid, 1.0, GridUnitType.Star);
        VisualListColumn visualListColumn = new VisualListColumn((IModelObject) this.list.Headers[index], false);
        visualListColumn.Margin = new Thickness(0.0);
        visualListColumn.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        visualListColumn.VerticalAlignment = VerticalAlignment.Top;
        if (this.list.Headers[index].Item.GetType() == typeof (LayoutContainerControl))
          visualListColumn.ToolTip = (object) ("Right click to model " + (this.list.Headers[index].Item as LayoutContainerControl).Content.TypeName);
        GridUtil.PlaceElement(this.contentGrid, (UIElement) visualListColumn, 0, col);
        int column2 = col + 1;
        this.m_VisualListColumns.Add(this.list.Headers[index], visualListColumn);
        this.AddRectangle(0, column2, 1, 1, 1.0, VisualList.BRUSH_TABLE_GRID);
        col = column2 + 1;
      }
      this.AddVerticalScrollBar();
    }

    private void AddColumnHeaderButton(int column, int row, int colSpan, ListColumn columnHeaderDefinition)
    {
      string textProperty = string.Empty;
      bool mandatory = false;
      if (columnHeaderDefinition != null && columnHeaderDefinition.Label != null)
      {
        if (columnHeaderDefinition.Label != null)
          textProperty = columnHeaderDefinition.Label.Text;
        if (columnHeaderDefinition.Item != null)
        {
          EditControl editControl = columnHeaderDefinition.Item as EditControl;
          if (editControl != null)
            mandatory = Converter.ToBoolean((DependentProperty) editControl.Mandatory, false);
        }
      }
      System.Windows.Controls.UserControl userControl = (System.Windows.Controls.UserControl) new NonFocusButton(false, textProperty, mandatory);
      this.contentGrid.Children.Add((UIElement) userControl);
      userControl.SetValue(Grid.ColumnProperty, (object) column);
      userControl.SetValue(Grid.RowProperty, (object) row);
      userControl.SetValue(Grid.ColumnSpanProperty, (object) colSpan);
    }

    private void AddRectangle(int row, int column, int rowspan, int colspan, double columnWidth, Brush background)
    {
      GridUtil.AddColumnDef(this.contentGrid, columnWidth, GridUnitType.Pixel);
      Rectangle rectangle = new Rectangle();
      rectangle.Fill = background;
      rectangle.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      rectangle.VerticalAlignment = VerticalAlignment.Stretch;
      rectangle.StrokeThickness = 0.0;
      GridUtil.PlaceElement(this.contentGrid, (UIElement) rectangle, row, column, rowspan, colspan);
    }

    private void InsertRectangle(int column, double columnWidth, Brush background)
    {
      for (int index = column; index < this.contentGrid.Children.Count; ++index)
      {
        UIElement child = this.contentGrid.Children[index];
        int num = (int) child.GetValue(Grid.ColumnProperty);
        child.SetValue(Grid.ColumnProperty, (object) (num + 1));
      }
      this.contentGrid.ColumnDefinitions.Insert(column, new ColumnDefinition()
      {
        Width = new GridLength(columnWidth, GridUnitType.Pixel)
      });
      Rectangle rectangle = new Rectangle();
      rectangle.Fill = background;
      rectangle.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      rectangle.VerticalAlignment = VerticalAlignment.Stretch;
      rectangle.StrokeThickness = 0.0;
      rectangle.SetValue(Grid.ColumnProperty, (object) column);
      this.contentGrid.Children.Insert(column, (UIElement) rectangle);
    }

    private void InsertListColumn(int column, ListColumn columnModel)
    {
      for (int index = column; index < this.contentGrid.Children.Count; ++index)
      {
        UIElement child = this.contentGrid.Children[index];
        int num = (int) child.GetValue(Grid.ColumnProperty);
        child.SetValue(Grid.ColumnProperty, (object) (num + 1));
      }
      this.contentGrid.ColumnDefinitions.Insert(column, new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Star)
      });
      VisualListColumn visualListColumn = new VisualListColumn((IModelObject) columnModel, false);
      if (columnModel.Item.GetType() == typeof (LayoutContainerControl))
        visualListColumn.ToolTip = (object) ("Right click to model " + (columnModel.Item as LayoutContainerControl).Content.TypeName);
      visualListColumn.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      visualListColumn.VerticalAlignment = VerticalAlignment.Top;
      visualListColumn.Margin = new Thickness(0.0);
      visualListColumn.SetValue(Grid.ColumnProperty, (object) column);
      this.contentGrid.Children.Insert(column, (UIElement) visualListColumn);
    }

    private void RemoveControl(int column)
    {
      this.contentGrid.Children.RemoveAt(column);
      this.contentGrid.ColumnDefinitions.RemoveAt(column);
      for (int index = column; index < this.contentGrid.Children.Count; ++index)
      {
        UIElement child = this.contentGrid.Children[index];
        int num = (int) child.GetValue(Grid.ColumnProperty);
        child.SetValue(Grid.ColumnProperty, (object) (num - 1));
      }
    }

    private void AddVerticalScrollBar()
    {
      this.verticalScrollBar = new System.Windows.Controls.Primitives.ScrollBar();
      this.verticalScrollBar.Orientation = System.Windows.Controls.Orientation.Vertical;
      this.verticalScrollBar.SetValue(Grid.RowProperty, (object) 0);
      this.verticalScrollBar.SetValue(Grid.ColumnProperty, (object) 1);
      this.tableContainer.Children.Add((UIElement) this.verticalScrollBar);
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      BaseVisualControl baseVisualControl = ComponentModelerUtilities.Instance.GetBaseVisualControl(this.Parent as FrameworkElement);
      if (baseVisualControl == null)
        return;
      baseVisualControl.ReloadView();
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel != null && e.PositionInParent != -1)
        {
          int column = e.PositionInParent * 2;
          if (this.list.TableDesign != ListDesignType.transparent && this.list.SelectOption != ListSelectionOptionType.none)
            column += 2;
          this.InsertListColumn(column, this.list.Headers[e.PositionInParent]);
          this.InsertRectangle(column + 1, 1.0, VisualList.BRUSH_TABLE_GRID);
        }
        else
        {
          this.ReloadView();
          if (e.AddedModel is ListColumn && this.m_VisualListColumns.ContainsKey(e.AddedModel as ListColumn))
            this.TriggerSelection((BaseSelectableControl) this.m_VisualListColumns[e.AddedModel as ListColumn], true);
        }
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Model object added event handler failed to execute.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        if (e.RemovedModel != null && e.PositionInParent != -1)
        {
          int column = e.PositionInParent * 2;
          if (this.list.TableDesign != ListDesignType.transparent && this.list.SelectOption != ListSelectionOptionType.none)
            column += 2;
          this.RemoveControl(column);
          this.RemoveControl(column);
        }
        else
        {
          this.ReloadView();
          if (e.RemovedModel is ListColumn && this.m_VisualListColumns.ContainsKey(e.RemovedModel as ListColumn))
            this.TriggerSelection((BaseSelectableControl) this.m_VisualListColumns[e.RemovedModel as ListColumn], true);
        }
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Model object removed event handler failed to execute.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == null || source.Header != (object) "Delete")
        return;
      IExtensible model = this.Model as IExtensible;
      if (ValidationManager.GetInstance().IsDeletable(model))
      {
        AdvancedListPane parentOfType1 = Utilities.GetParentOfType(this.Model, typeof (AdvancedListPane)) as AdvancedListPane;
        if (parentOfType1 != null && parentOfType1.Parent != null)
        {
          parentOfType1.Parent.RemoveModelObject((IModelObject) parentOfType1);
        }
        else
        {
          if (!(model.Parent is AvailabilityCalendarPane))
            return;
          AvailabilityCalendarPane parentOfType2 = Utilities.GetParentOfType(this.Model, typeof (AvailabilityCalendarPane)) as AvailabilityCalendarPane;
          if (parentOfType2 == null || parentOfType2.Parent == null)
            return;
          parentOfType2.Parent.RemoveModelObject((IModelObject) parentOfType2);
        }
      }
      else
      {
        int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.ElementNotDeletable, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
      }
    }
  }
}
