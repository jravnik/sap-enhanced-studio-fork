﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.VisualFormPane
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.FormPaneControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.ValidationLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class VisualFormPane : VisualBasePane, IContextMenuContainer
  {
    private VisualToolbar formPaneToolbar;
    private Grid sectionGroupContainer;
    private int desiredRows;

    internal FormPane FormPane
    {
      get
      {
        return this.m_ModelObject as FormPane;
      }
    }

    public int DesiredRows
    {
      get
      {
        return this.desiredRows;
      }
    }

    public VisualFormPane(IModelObject model)
      : base(model)
    {
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void ConstructBodyElement()
    {
      try
      {
        int col = 0;
        int row = 0;
        this.sectionGroupContainer = new Grid();
        this.desiredRows = this.GetDesiredNumberOfRows();
        GridUtil.AddColumnDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
        if (this.FormPane.UseToolbar && !this.FormPane.PlaceToolbarOnHeader)
        {
          this.CreateFormPaneToolBar(this.FormPane);
          row = 1;
        }
        else if (this.FormPane.UseToolbar && this.FormPane.PlaceToolbarOnHeader)
          this.CreateFormPaneToolBarOnHeader(this.FormPane);
        GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Auto);
        GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.sectionGroupContainer, row, col);
        for (int index = 0; index < this.FormPane.Columns; ++index)
          GridUtil.AddColumnDef(this.sectionGroupContainer, 1.0, GridUnitType.Star);
        for (int index = 0; index < this.desiredRows; ++index)
          GridUtil.AddRowDef(this.sectionGroupContainer, 100.0, 100.0, GridUnitType.Auto);
        int desiredRow = 0;
        int desiredColumn = 0;
        foreach (FieldGroup sectionGroup in this.FormPane.SectionGroups)
        {
          if (desiredColumn == this.FormPane.Columns || sectionGroup.ForceNewLine)
          {
            ++desiredRow;
            desiredColumn = 0;
          }
          GridUtil.GetNextAvialableLocation(this.sectionGroupContainer, ref desiredRow, ref desiredColumn, sectionGroup.ColSpan);
          while (desiredColumn != 0 && sectionGroup.ForceNewLine)
          {
            ++desiredRow;
            desiredColumn = 0;
            GridUtil.GetNextAvialableLocation(this.sectionGroupContainer, ref desiredRow, ref desiredColumn, sectionGroup.ColSpan);
          }
          if (this.sectionGroupContainer.RowDefinitions.Count <= desiredRow)
            GridUtil.AddRowDef(this.sectionGroupContainer, 100.0, 100.0, GridUnitType.Auto);
          if (sectionGroup.Row != desiredRow)
            sectionGroup.Row = desiredRow;
          if (sectionGroup.Column != desiredColumn)
            sectionGroup.Column = desiredColumn;
          VisualFieldGroup visualFieldGroup = new VisualFieldGroup((IModelObject) sectionGroup, (BaseVisualControl) this);
          GridUtil.PlaceElement(this.sectionGroupContainer, (UIElement) visualFieldGroup, desiredRow, desiredColumn, sectionGroup.RowSpan, sectionGroup.ColSpan);
          visualFieldGroup.InitializeSpanningDetails(this.desiredRows);
          desiredColumn += sectionGroup.ColSpan;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of Form pane control failed.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is FieldGroup)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed because of unknown reasons.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        base.OnModelObjectRemoved(sender, e);
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      try
      {
        if (e.PropertyName == FormPane.ColumnsProperty || e.PropertyName == FormPane.UseToolbarProperty || (e.PropertyName == FormPane.FieldGroupsProperty || e.PropertyName == FormPane.PlaceToolbarOnHeaderProperty))
          this.ReloadView();
        else if (e.PropertyName == FormPane.ToolbarProperty && this.FormPane.PlaceToolbarOnHeader)
          this.ReloadView();
        else if (e.PropertyName == FormPane.ToolbarProperty)
        {
          this.formPaneToolbar.Toolbar = (Toolbar) this.FormPane.Toolbar;
          this.formPaneToolbar.ReloadView();
        }
        else
        {
          if (!this.FormPane.UseToolbar || !this.FormPane.PlaceToolbarOnHeader)
            return;
          this.CreateFormPaneToolBarOnHeader(this.FormPane);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of Formpane was not reflected in UI because of unknown reasons.", ex));
      }
    }

    protected override void OnBOElementDragOver(System.Windows.DragEventArgs e, BOBrowserDragEventArgs boBrowserArgs)
    {
      base.OnBOElementDragOver(e, boBrowserArgs);
      if (e.Effects == System.Windows.DragDropEffects.None)
        return;
      BOElement boElement = (BOElement) null;
      if (boBrowserArgs != null && boBrowserArgs.Count > 0)
      {
        boElement = boBrowserArgs[0].DraggedElement as BOElement;
        if (boElement == null)
        {
          BOElementAttribute draggedElement = boBrowserArgs[0].DraggedElement as BOElementAttribute;
          if (draggedElement != null)
            boElement = draggedElement.ParentElement;
        }
      }
      if (boElement != null && boElement.ParentQuery != null)
        e.Effects = System.Windows.DragDropEffects.None;
      if (boElement == null || boElement.ParentAction == null)
        return;
      PaneContainer parentLayoutCell = Utilities.GetParentLayoutCell(this.m_ModelObject);
      if (parentLayoutCell == null || parentLayoutCell.Parent is ActionForm)
        return;
      e.Effects = System.Windows.DragDropEffects.None;
    }

    protected override void OnToolBoxControlDragOver(System.Windows.DragEventArgs e, ToolBoxEventArgs toolboxEventArg)
    {
      if (toolboxEventArg.Tag.ControlName.Equals("LayoutGridPane") || toolboxEventArg.Tag.ControlName.Equals("LayoutStackPane") || toolboxEventArg.Tag.ControlName.Equals("LayoutBorderPane"))
        e.Effects = System.Windows.DragDropEffects.None;
      else
        base.OnToolBoxControlDragOver(e, toolboxEventArg);
    }

    protected override void OnDataModelElementDragOver(System.Windows.DragEventArgs e, DataModelDragEventArgs dataModelEventArgs)
    {
      if (dataModelEventArgs.DataElement.IsQueryBinding)
        e.Effects = System.Windows.DragDropEffects.None;
      else
        base.OnDataModelElementDragOver(e, dataModelEventArgs);
    }

    protected override void OnBaseVisualControlDragOver(System.Windows.DragEventArgs e, IModelObject droppedModel)
    {
      if (droppedModel is FieldGroup && (droppedModel as FieldGroup).Parent.GetType() != typeof (FormPane))
        e.Effects = System.Windows.DragDropEffects.None;
      else
        base.OnBaseVisualControlDragOver(e, droppedModel);
    }

    protected override void AddContextMenuItems()
    {
      try
      {
        if (this.formPaneToolbar != null)
          this.formPaneToolbar.AddContextMenuItems((IContextMenuContainer) this);
        this.AddSeperatorToContextMenu();
        this.AddContextMenuItem("Delete");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to initialize the ContextMenu for VisualForm.", ex));
      }
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      try
      {
        bool flag = true;
        if (source.Header == null || source.Header != (object) "Delete")
          return;
        if (this.FormPane.Parent != null)
        {
          foreach (IExtensible sectionGroup in this.FormPane.SectionGroups)
          {
            if (!ValidationManager.GetInstance().IsDeletable(sectionGroup))
            {
              flag = false;
              break;
            }
          }
          if (flag)
          {
            this.FormPane.Parent.RemoveModelObject((IModelObject) this.FormPane);
          }
          else
          {
            int num = (int) DisplayMessage.Show(SAP.BYD.LS.UIDesigner.ComponentModelers.Resource.ElementNotDeletable, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
          }
        }
        else
        {
          if (this.formPaneToolbar == null)
            return;
          this.formPaneToolbar.HandleContextMenuItemClickWrapper(source);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to handle the ContextMenu click event.", ex));
      }
    }

    public override string SelectionText
    {
      get
      {
        return "Formpane";
      }
    }

    public override void Terminate()
    {
      base.Terminate();
      if (this.sectionGroupContainer == null)
        return;
      ComponentModelerUtilities.Instance.TerminatePanelControls((FrameworkElement) this.sectionGroupContainer);
    }

    public override void ReloadView()
    {
      base.ReloadView();
    }

    internal override void TriggerSelection(BaseSelectableControl vControl, bool isTriggeredByControlAddOperation)
    {
      if (isTriggeredByControlAddOperation)
      {
        if (this.FormPane.SectionGroups != null && this.FormPane.SectionGroups.Count > 0)
        {
          BaseSelectableControl vControl1 = (BaseSelectableControl) null;
          FieldGroup sectionGroup = this.FormPane.SectionGroups[0];
          ComponentDesigner parentDesigner = ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this) as ComponentDesigner;
          if (parentDesigner == null)
            return;
          if (sectionGroup.Fields != null && sectionGroup.Fields.Count > 0)
          {
            SectionGroupItem field = sectionGroup.Fields[0];
            if (parentDesigner.DesignerControlDictionary.ContainsKey(field.Id))
              vControl1 = parentDesigner.DesignerControlDictionary[field.Id] as BaseSelectableControl;
          }
          else if (parentDesigner.DesignerControlDictionary.ContainsKey(sectionGroup.Id))
            vControl1 = parentDesigner.DesignerControlDictionary[sectionGroup.Id] as BaseSelectableControl;
          if (vControl1 == null)
            return;
          base.TriggerSelection(vControl1, isTriggeredByControlAddOperation);
        }
        else
          base.TriggerSelection((BaseSelectableControl) this, isTriggeredByControlAddOperation);
      }
      else
        base.TriggerSelection((BaseSelectableControl) this, isTriggeredByControlAddOperation);
    }

    private void CreateFormPaneToolBar(FormPane formPane)
    {
      if (formPane.Toolbar == null)
        return;
      GridUtil.AddRowDef(this.BodyElementGrid, 1.0, GridUnitType.Star);
      this.formPaneToolbar = new VisualToolbar((IModelObject) formPane.Toolbar);
      this.formPaneToolbar.VerticalAlignment = VerticalAlignment.Top;
      this.formPaneToolbar.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.formPaneToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      this.formPaneToolbar.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
      GridUtil.PlaceElement(this.BodyElementGrid, (UIElement) this.formPaneToolbar, 0, 0);
    }

    private VisualToolbar CreateFormPaneToolBarOnHeader(FormPane formPane)
    {
      if (formPane.Toolbar != null)
      {
        this.formPaneToolbar = new VisualToolbar((IModelObject) formPane.Toolbar);
        this.formPaneToolbar.VerticalAlignment = VerticalAlignment.Top;
        this.formPaneToolbar.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        this.formPaneToolbar.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        this.formPaneToolbar.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
        GridUtil.PlaceElement(this.HeaderGrid, (UIElement) this.formPaneToolbar, 0, 1);
      }
      return this.formPaneToolbar;
    }

    private int GetDesiredNumberOfRows()
    {
      int num = 0;
      foreach (FieldGroup sectionGroup in this.FormPane.SectionGroups)
      {
        if (num == sectionGroup.Row || sectionGroup.ForceNewLine)
          ++num;
      }
      return num;
    }

    public System.Windows.Controls.MenuItem AddContextMenuItemWrapper(string menuHeader)
    {
      return this.AddContextMenuItem(menuHeader);
    }

    public System.Windows.Controls.MenuItem AddContextMenuItemWrapper(string menuHeader, System.Windows.Controls.MenuItem parentItem)
    {
      return this.AddContextMenuItem(menuHeader, parentItem);
    }
  }
}
