﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.VisualWorkingTime
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls
{
  public class VisualWorkingTime : BaseSelectableControl
  {
    private Grid m_Container;

    public VisualWorkingTime(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_Container = new Grid();
      TextBlock textBlock = new TextBlock();
      textBlock.Height = 20.0;
      textBlock.Text = "Working Time";
      textBlock.FontSize = 14.0;
      textBlock.TextAlignment = TextAlignment.Center;
      textBlock.Background = (Brush) Brushes.LightGray;
      this.m_Container.Children.Add((UIElement) textBlock);
      this.Content = (object) this.m_Container;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "WorkingTime";
      }
    }
  }
}
