﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarUtils
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Windows;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls
{
  public class CalendarUtils
  {
    public static readonly int CELL_HEIGHT = 20;
    public static readonly double DEFAULT_CONTENT_HEIGHT = 300.0;
    public static readonly double HOUR_CELL_FONT_SIZE = 20.0;
    public static readonly double MINUTE_CELL_FONT_SIZE = 12.0;
    public static readonly int HOURS_PER_DAY = 24;
    public static readonly double WEEK_HEADER_FONT_SIZE = 20.0;
    public static readonly double MONTH_VIEW_DATE_FONT_SIZE = 10.0;
    public static readonly int MONTH_VIEW_MAX_ENTRIES = 5;

    internal static LinearGradientBrush CreateBrush(Color colorStart, Color colorMiddle, Color colorEnd)
    {
      LinearGradientBrush linearGradientBrush = new LinearGradientBrush();
      linearGradientBrush.StartPoint = new Point(0.5, 0.0);
      linearGradientBrush.EndPoint = new Point(0.5, 1.0);
      linearGradientBrush.GradientStops = new GradientStopCollection()
      {
        new GradientStop() { Color = colorStart, Offset = 0.0 },
        new GradientStop() { Color = colorMiddle, Offset = 0.5 },
        new GradientStop() { Color = colorEnd, Offset = 1.0 }
      };
      return linearGradientBrush;
    }

    internal static LinearGradientBrush TodayMarker()
    {
      LinearGradientBrush linearGradientBrush = new LinearGradientBrush();
      linearGradientBrush.StartPoint = new Point(0.5, 0.0);
      linearGradientBrush.EndPoint = new Point(0.5, 1.0);
      linearGradientBrush.GradientStops = new GradientStopCollection()
      {
        new GradientStop()
        {
          Color = Color.FromArgb((byte) 200, (byte) 75, (byte) 135, (byte) 185),
          Offset = 0.0
        },
        new GradientStop()
        {
          Color = Color.FromArgb(byte.MaxValue, (byte) 75, (byte) 135, (byte) 185),
          Offset = 0.5
        },
        new GradientStop()
        {
          Color = Color.FromArgb((byte) 200, (byte) 75, (byte) 135, (byte) 185),
          Offset = 1.0
        }
      };
      return linearGradientBrush;
    }
  }
}
