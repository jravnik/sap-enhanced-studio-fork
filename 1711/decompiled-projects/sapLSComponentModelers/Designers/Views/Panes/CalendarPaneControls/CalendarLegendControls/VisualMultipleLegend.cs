﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarLegendControls.VisualMultipleLegend
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarLegendControls
{
  internal class VisualMultipleLegend : BaseSelectableControl
  {
    private Grid m_Container;

    public VisualMultipleLegend(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_Container = new Grid();
      this.Content = (object) this.m_Container;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "MultipleLegend";
      }
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.m_Container.RowDefinitions.Add(new RowDefinition());
        TextBlock textBlock = new TextBlock();
        textBlock.Height = 20.0;
        textBlock.Text = "LEGEND";
        textBlock.FontSize = 14.0;
        textBlock.TextAlignment = TextAlignment.Center;
        textBlock.Background = (Brush) Brushes.LightGray;
        GridUtil.PlaceElement(this.m_Container, (UIElement) textBlock, 0, 0);
      }
      catch (Exception ex)
      {
      }
    }
  }
}
