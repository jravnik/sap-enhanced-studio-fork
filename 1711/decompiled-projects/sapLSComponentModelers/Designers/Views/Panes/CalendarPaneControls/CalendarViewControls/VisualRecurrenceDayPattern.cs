﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.VisualRecurrenceDayPattern
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls.CalendarViewElements;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls
{
  public class VisualRecurrenceDayPattern : BaseSelectableControl
  {
    private Grid m_Container;

    public VisualRecurrenceDayPattern(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_Container = new Grid();
      this.LoadView();
    }

    protected RecurrenceDayPattern RecurrenceDayPattern
    {
      get
      {
        return this.m_ModelObject as RecurrenceDayPattern;
      }
    }

    public override string SelectionText
    {
      get
      {
        return "RecurrenceDayPattern";
      }
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.m_Container.RowDefinitions.Add(new RowDefinition());
        this.m_Container.ColumnDefinitions.Add(new ColumnDefinition());
        this.m_Container.ColumnDefinitions.Add(new ColumnDefinition());
        this.m_Container.ColumnDefinitions[0].Width = new GridLength(600.0, GridUnitType.Star);
        this.m_Container.ColumnDefinitions[1].Width = new GridLength(105.0);
        TextBlock textBlock = new TextBlock();
        textBlock.Height = 20.0;
        textBlock.Text = "RECURRENCE DAY PATTERN";
        textBlock.FontSize = 14.0;
        textBlock.TextAlignment = TextAlignment.Center;
        textBlock.Background = (Brush) Brushes.LightGray;
        GridUtil.PlaceElement(this.m_Container, (UIElement) textBlock, 0, 0);
        VisualWorkingTime visualWorkingTime = new VisualWorkingTime((IModelObject) this.RecurrenceDayPattern.WorkingTime);
        visualWorkingTime.Width = 100.0;
        GridUtil.PlaceElement(this.m_Container, (UIElement) visualWorkingTime, 0, 1);
        this.Content = (object) this.m_Container;
      }
      catch (Exception ex)
      {
      }
    }
  }
}
