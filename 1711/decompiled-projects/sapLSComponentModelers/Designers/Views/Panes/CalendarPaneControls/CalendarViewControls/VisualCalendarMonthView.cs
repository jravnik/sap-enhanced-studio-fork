﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.VisualCalendarMonthView
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarMonthView;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls
{
  internal class VisualCalendarMonthView : VisualAbstractCalendarView
  {
    private static double WEEK_COLUMN_WIDTH = 50.0;
    private static double COLUMN_MIN_WIDTH = 20.0;
    private static double ROW_MIN_HEIGHT = 50.0;
    private static double HEADER_ROW_HEIGHT = 20.0;
    private static int MAX_COLUMNS = 7;
    private int rowsForMonth = 5;
    private VisualMonthViewCellControl[,] cells;
    private Grid container;
    private Grid parentContainer;
    private VisualMonthViewRowGridControl[] weekRows;
    private List<VisualMonthViewWeekHeaderControl> weekHeaders;
    private List<VisualMonthViewColumnHeaderControl> colHeaders;
    private VisualMonthViewColumnHeaderControl colHeader;

    public VisualCalendarMonthView(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.container = new Grid();
      this.parentContainer = new Grid();
      this.Content = (object) this.parentContainer;
      this.LoadView();
    }

    public void Initialize()
    {
      this.container.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(100.0, GridUnitType.Auto),
        MinWidth = VisualCalendarMonthView.WEEK_COLUMN_WIDTH
      });
      this.container.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(VisualCalendarMonthView.HEADER_ROW_HEIGHT, GridUnitType.Pixel)
      });
      this.colHeader = new VisualMonthViewColumnHeaderControl("Week");
      this.container.Children.Add((UIElement) this.colHeader);
      this.colHeader.SetValue(Grid.RowProperty, (object) 0);
      this.colHeader.SetValue(Grid.ColumnProperty, (object) 0);
      this.colHeaders = new List<VisualMonthViewColumnHeaderControl>();
      string[] strArray1 = new string[7]
      {
        "Sun",
        "Mon",
        "Tue",
        "Wed",
        "Thurs",
        "Fri",
        "Sat"
      };
      string empty1 = string.Empty;
      for (int index = 0; index < VisualCalendarMonthView.MAX_COLUMNS; ++index)
      {
        this.container.ColumnDefinitions.Add(new ColumnDefinition()
        {
          Width = new GridLength(100.0, GridUnitType.Star),
          MinWidth = VisualCalendarMonthView.COLUMN_MIN_WIDTH
        });
        VisualMonthViewColumnHeaderControl columnHeaderControl = new VisualMonthViewColumnHeaderControl(index >= strArray1.Length ? string.Empty : strArray1[index]);
        this.container.Children.Add((UIElement) columnHeaderControl);
        columnHeaderControl.SetValue(Grid.RowProperty, (object) 0);
        columnHeaderControl.SetValue(Grid.ColumnProperty, (object) (index + 1));
        this.colHeaders.Add(columnHeaderControl);
      }
      this.weekHeaders = new List<VisualMonthViewWeekHeaderControl>();
      string[] strArray2 = new string[7]
      {
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7"
      };
      string empty2 = string.Empty;
      for (int index = 0; index < this.rowsForMonth; ++index)
      {
        this.container.RowDefinitions.Add(new RowDefinition()
        {
          Height = new GridLength(VisualCalendarMonthView.ROW_MIN_HEIGHT, GridUnitType.Pixel)
        });
        VisualMonthViewWeekHeaderControl weekHeaderControl = new VisualMonthViewWeekHeaderControl(index >= strArray2.Length ? string.Empty : strArray2[index]);
        this.container.Children.Add((UIElement) weekHeaderControl);
        weekHeaderControl.SetValue(Grid.RowProperty, (object) (index + 1));
        weekHeaderControl.SetValue(Grid.ColumnProperty, (object) 0);
        this.weekHeaders.Add(weekHeaderControl);
      }
      this.cells = new VisualMonthViewCellControl[this.rowsForMonth, VisualCalendarMonthView.MAX_COLUMNS];
      for (int index1 = 0; index1 < this.rowsForMonth; ++index1)
      {
        for (int index2 = 0; index2 < VisualCalendarMonthView.MAX_COLUMNS; ++index2)
        {
          VisualMonthViewCellControl monthViewCellControl = new VisualMonthViewCellControl();
          monthViewCellControl.Row = index1;
          monthViewCellControl.Column = index2;
          this.container.Children.Add((UIElement) monthViewCellControl);
          monthViewCellControl.SetValue(Grid.RowProperty, (object) (index1 + 1));
          monthViewCellControl.SetValue(Grid.ColumnProperty, (object) (index2 + 1));
          this.cells[index1, index2] = monthViewCellControl;
        }
      }
      this.weekRows = new VisualMonthViewRowGridControl[this.rowsForMonth];
      for (int index = 0; index < this.rowsForMonth; ++index)
      {
        this.weekRows[index] = new VisualMonthViewRowGridControl(7);
        this.container.Children.Add((UIElement) this.weekRows[index]);
        this.weekRows[index].SetValue(Grid.ColumnProperty, (object) 1);
        this.weekRows[index].SetValue(Grid.RowProperty, (object) (index + 1));
        this.weekRows[index].SetValue(Grid.ColumnSpanProperty, (object) 7);
      }
      this.parentContainer.RowDefinitions.Add(new RowDefinition()
      {
        Height = new GridLength(VisualCalendarMonthView.HEADER_ROW_HEIGHT, GridUnitType.Star)
      });
      GridUtil.PlaceElement(this.parentContainer, (UIElement) this.container, 0, 0);
    }

    public override void LoadView()
    {
      base.LoadView();
      this.Initialize();
    }

    public override string SelectionText
    {
      get
      {
        return "CalendarMonthView";
      }
    }
  }
}
