﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarDayView.DayGrid
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using System.Collections.Generic;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.CalendarPaneControls.CalendarViewControls.CalendarDayView
{
  public class DayGrid : UserControl
  {
    private Grid container;
    private int numRows;
    private List<DayGrid.DayColumn> entryMarker;

    public DayGrid(int numRows)
    {
      this.container = new Grid();
      this.Content = (object) this.container;
      this.numRows = numRows;
      this.entryMarker = new List<DayGrid.DayColumn>();
      this.container.ColumnDefinitions.Add(new ColumnDefinition());
      this.entryMarker.Add(new DayGrid.DayColumn(numRows));
      for (int index = 0; index < numRows; ++index)
        this.container.RowDefinitions.Add(new RowDefinition());
    }

    internal class DayColumn
    {
      private bool[] cells;

      internal DayColumn(int numRows)
      {
        this.cells = new bool[numRows];
      }

      public bool HasEntry(int startRow, int rowSpan)
      {
        bool flag = false;
        if (this.cells != null)
        {
          for (int index = startRow; index < rowSpan + startRow; ++index)
          {
            if (this.cells[index])
            {
              flag = true;
              break;
            }
          }
        }
        return flag;
      }

      public void MarkEntry(int startRow, int rowSpan)
      {
        if (this.cells == null)
          return;
        for (int index = startRow; index < rowSpan + startRow; ++index)
          this.cells[index] = true;
      }
    }
  }
}
