﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.PaneFactory
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Panes;
using SAP.BYD.LS.UIDesigner.Model.Entities.SpecializedControls;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes
{
  public class PaneFactory
  {
    public static BaseVisualControl GetVisualPaneControl(IModelObject model)
    {
      if (model is FormPane)
        return (BaseVisualControl) new VisualFormPane(model);
      if (model is GridLayout)
        return (BaseVisualControl) new VisualLayout(model);
      if (model is TabStrip)
        return (BaseVisualControl) new VisualTabStrip(model);
      if (model is AdvancedListPane)
      {
        VisualListPane visualListPane = new VisualListPane(model);
        visualListPane.VerticalAlignment = VerticalAlignment.Top;
        return (BaseVisualControl) visualListPane;
      }
      if (model is AdvancedDataSetPane)
      {
        VisualDataSetPane visualDataSetPane = new VisualDataSetPane(model);
        visualDataSetPane.VerticalAlignment = VerticalAlignment.Top;
        return (BaseVisualControl) visualDataSetPane;
      }
      if (model is BrowseAndCollectPane)
        return (BaseVisualControl) new VisualBACPane(model);
      if (model is EmbeddedComponentPane)
        return (BaseVisualControl) new VisualECPane(model);
      if (model is CustomPane)
        return (BaseVisualControl) new VisualCustompane(model);
      if (model is CalendarPane)
        return (BaseVisualControl) new VisualCalendarPane(model);
      if (model is AvailabilityCalendarPane)
        return (BaseVisualControl) new VisualAvailabilityCalendarPane(model);
      if (model is PaneContainerVariant)
        return (BaseVisualControl) new VisualPaneContainerVariant(model);
      if (model is NetworkChartPane)
        return (BaseVisualControl) new VisualNetworkChartPane(model);
      if (model is GanttChartPane)
        return (BaseVisualControl) new VisualGanttChartPane(model);
      if (model is HierarchicalGraphPane)
        return (BaseVisualControl) new VisualHierarchicalGraphPane(model);
      if (model is RowRepeaterPane)
        return (BaseVisualControl) new VisualRowRepeaterPane(model);
      if (model is ServiceMapPane)
        return (BaseVisualControl) new VisualServiceMapPane(model);
      if (model is MatrixLayout)
        return (BaseVisualControl) new VisualMatrixLayout(model);
      if (model is LayoutContainerPane)
        return (BaseVisualControl) new VisualLayoutContainerPane(model);
      if (model is MapPane)
        return (BaseVisualControl) new VisualMapPane(model);
      if (model is SplitPane)
        return (BaseVisualControl) new VisualSplitPane(model);
      if (model is TabContainerPane)
        return (BaseVisualControl) new VisualTabStrip(model);
      return (BaseVisualControl) new VisualUnknownPane(model);
    }
  }
}
