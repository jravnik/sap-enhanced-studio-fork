﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.GanttChartPaneControls.VisualChartItemListVariant
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.ListPaneControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.GanttChartPaneControls
{
  public class VisualChartItemListVariant : BaseSelectableControl
  {
    private static readonly Brush tableBorderBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 135, (byte) 163, (byte) 192));
    private static readonly Brush tableBackgroundBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 212, (byte) 217, (byte) 222));
    private static readonly Brush tableGridBrush = (Brush) new SolidColorBrush(Color.FromRgb((byte) 190, (byte) 201, (byte) 213));
    private static readonly double selectorButtonWidth = 20.0;
    private const string ADD = "Add";
    private const string DELETE = "Delete";
    private const string COLUMN = "Column";
    internal List<ListColumn> m_VisibleHeaders;
    private Border tableBorder;
    private int columnCount;
    private int visibleColumnCount;
    public Grid contentGrid;
    private ScrollBar verticalScrollBar;
    private Grid tableContainer;
    private int firstVisibleRow;
    private ScrollViewer horizontalScrollViewer;
    private GanttChartItemListVariant list;
    private Dictionary<ListColumn, VisualListColumn> m_VisualListColumns;
    private BaseVisualControl parentVisualControl;

    private GanttChartItemListVariant ChartItemListVariant
    {
      get
      {
        return this.m_ModelObject as GanttChartItemListVariant;
      }
    }

    public VisualChartItemListVariant(IModelObject model, BaseVisualControl parentControl)
      : base(model)
    {
      this.AllowDrop = true;
      this.parentVisualControl = parentControl;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.m_VisualListColumns = new Dictionary<ListColumn, VisualListColumn>();
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        this.tableBorder = new Border();
        this.tableBorder.BorderBrush = VisualChartItemListVariant.tableBorderBrush;
        this.tableBorder.BorderThickness = new Thickness(1.0);
        this.Content = (object) this.tableBorder;
        this.tableContainer = new Grid();
        GridUtil.AddColumnDef(this.tableContainer, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.tableContainer, 18.0, GridUnitType.Pixel);
        new RowDefinition().Height = new GridLength(1.0, GridUnitType.Auto);
        this.tableBorder.Child = (UIElement) this.tableContainer;
        this.horizontalScrollViewer = new ScrollViewer();
        this.horizontalScrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
        this.horizontalScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
        this.horizontalScrollViewer.Background = VisualChartItemListVariant.tableBackgroundBrush;
        this.horizontalScrollViewer.BorderThickness = new Thickness(0.0);
        this.tableContainer.Children.Add((UIElement) this.horizontalScrollViewer);
        this.contentGrid = new Grid();
        this.contentGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
        this.horizontalScrollViewer.Content = (object) this.contentGrid;
        this.list = this.ChartItemListVariant;
        this.columnCount = this.list.Column.Count;
        this.m_VisibleHeaders = new List<ListColumn>();
        this.m_VisibleHeaders = this.list.Column;
        this.visibleColumnCount = this.m_VisibleHeaders.Count;
        this.InitializeHeaderControls();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of GanttChartItemList Control failed.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel != null)
        {
          BaseVisualControl baseVisualControl = ComponentModelerUtilities.Instance.GetBaseVisualControl(this.Parent as FrameworkElement);
          if (baseVisualControl != null)
            baseVisualControl.ReloadView();
        }
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Model added of GanttChartItemListVariant failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        if (e.RemovedModel != null)
        {
          BaseVisualControl baseVisualControl = ComponentModelerUtilities.Instance.GetBaseVisualControl(this.Parent as FrameworkElement);
          if (baseVisualControl != null)
            baseVisualControl.ReloadView();
        }
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Model removed of GanttChartItemListVariant failed.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of GanttChartItemListVariant failed.", ex));
      }
      base.OnModelPropertyChanged(sender, e);
    }

    public override string SelectionText
    {
      get
      {
        return "GanttChartItemListVariant";
      }
    }

    private void InitializeHeaderControls()
    {
      ColumnDefinitionCollection columnDefinitions = this.contentGrid.ColumnDefinitions;
      this.m_VisualListColumns.Clear();
      int num1 = 0;
      columnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(1.0, GridUnitType.Pixel)
      });
      int num2 = num1 + 1;
      this.AddRectangle(num2 - 1, 0, 1, 1, VisualChartItemListVariant.tableGridBrush);
      columnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength(VisualChartItemListVariant.selectorButtonWidth, GridUnitType.Pixel)
      });
      int num3 = num2 + 1;
      this.AddRectangle(num3 - 1, 0, 1, 1, VisualChartItemListVariant.tableGridBrush);
      UserControl userControl1 = (UserControl) new VisualRowSelectorColumn((IModelObject) this.list, (BaseSelectableControl) this);
      this.contentGrid.Children.Add((UIElement) userControl1);
      userControl1.HorizontalAlignment = HorizontalAlignment.Stretch;
      userControl1.VerticalAlignment = VerticalAlignment.Top;
      userControl1.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
      userControl1.SetValue(Grid.ColumnProperty, (object) (num3 - 1));
      userControl1.SetValue(Grid.RowProperty, (object) 0);
      for (int index = 0; index < this.list.Column.Count; ++index)
      {
        GridUtil.AddColumnDef(this.contentGrid, 1.0, GridUnitType.Star);
        GridUtil.AddColumnDef(this.contentGrid, 1.0, GridUnitType.Pixel);
        num3 += 2;
        this.AddRectangle(num3 - 2, 0, 1, 1, VisualChartItemListVariant.tableGridBrush);
        this.AddRectangle(num3 - 1, 0, 1, 1, VisualChartItemListVariant.tableGridBrush);
        ListColumn key = this.list.Column[index];
        UserControl userControl2 = (UserControl) new VisualListColumn((IModelObject) key, false);
        this.m_VisualListColumns.Add(key, userControl2 as VisualListColumn);
        this.contentGrid.Children.Add((UIElement) userControl2);
        userControl2.HorizontalAlignment = HorizontalAlignment.Stretch;
        userControl2.VerticalAlignment = VerticalAlignment.Top;
        userControl2.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
        userControl2.SetValue(Grid.ColumnProperty, (object) (num3 - 2));
        userControl2.SetValue(Grid.RowProperty, (object) 0);
      }
      this.AddVerticalScrollBar();
    }

    private void AddRectangle(int column, int row, int colSpan, int rowSpan, Brush brush)
    {
      Rectangle rectangle = new Rectangle();
      rectangle.Fill = brush;
      rectangle.HorizontalAlignment = HorizontalAlignment.Stretch;
      rectangle.VerticalAlignment = VerticalAlignment.Stretch;
      rectangle.StrokeThickness = 0.0;
      rectangle.SetValue(Grid.ColumnProperty, (object) column);
      rectangle.SetValue(Grid.RowProperty, (object) row);
      rectangle.SetValue(Grid.ColumnSpanProperty, (object) colSpan);
      rectangle.SetValue(Grid.RowSpanProperty, (object) rowSpan);
      this.contentGrid.Children.Add((UIElement) rectangle);
    }

    private void AddVerticalScrollBar()
    {
      this.verticalScrollBar = new ScrollBar();
      this.verticalScrollBar.Orientation = Orientation.Vertical;
      this.verticalScrollBar.SetValue(Grid.RowProperty, (object) 0);
      this.verticalScrollBar.SetValue(Grid.ColumnProperty, (object) 1);
      this.verticalScrollBar.ValueChanged += new RoutedPropertyChangedEventHandler<double>(this.verticalScrollBar_ValueChanged);
      this.tableContainer.Children.Add((UIElement) this.verticalScrollBar);
    }

    private void verticalScrollBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      this.firstVisibleRow = (int) this.verticalScrollBar.Value;
    }

    public void ReloadChartItemListVariant(IModelObject model)
    {
      this.m_ModelObject = model;
      this.ReloadView();
      IDesigner parentDesigner = (IDesigner) ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this);
      if (parentDesigner == null)
        return;
      parentDesigner.RaiseSelectionChanged((IDesignerControl) this, model);
    }
  }
}
