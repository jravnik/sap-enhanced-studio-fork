﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.GanttChartPaneControls.VisualGanttChartBar
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.GanttChartPaneControls
{
  public class VisualGanttChartBar : BaseSelectableControl
  {
    private static readonly Brush viewBorderBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 135, (byte) 163, (byte) 192));
    private static readonly Brush tableBackgroundBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 212, (byte) 217, (byte) 222));
    private Border viewBorder;
    private Grid m_Container;
    private ScrollViewer horizontalScrollViewer;
    private ScrollBar verticalScrollBar;

    private GanttChartBar GanttChartBar
    {
      get
      {
        return this.m_ModelObject as GanttChartBar;
      }
    }

    public VisualGanttChartBar(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      this.viewBorder = new Border();
      this.viewBorder.BorderBrush = VisualGanttChartBar.viewBorderBrush;
      this.viewBorder.BorderThickness = new Thickness(1.0);
      this.Content = (object) this.viewBorder;
      this.m_Container = new Grid();
      GridUtil.AddRowDef(this.m_Container, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.m_Container, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.m_Container, 1.0, GridUnitType.Auto);
      this.viewBorder.Child = (UIElement) this.m_Container;
      this.horizontalScrollViewer = new ScrollViewer();
      this.horizontalScrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
      this.horizontalScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
      this.horizontalScrollViewer.SetValue(Grid.RowProperty, (object) 0);
      this.horizontalScrollViewer.SetValue(Grid.ColumnProperty, (object) 0);
      this.horizontalScrollViewer.BorderThickness = new Thickness(0.0);
      this.m_Container.Children.Add((UIElement) this.horizontalScrollViewer);
      this.verticalScrollBar = new ScrollBar();
      this.verticalScrollBar.Orientation = Orientation.Vertical;
      this.verticalScrollBar.SetValue(Grid.RowProperty, (object) 0);
      this.verticalScrollBar.SetValue(Grid.ColumnProperty, (object) 1);
      this.m_Container.Children.Add((UIElement) this.verticalScrollBar);
      this.horizontalScrollViewer.Content = (object) new GanttChartCalender((IModelObject) this.GanttChartBar);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change of GanttChartBar failed.", ex));
      }
      base.OnModelPropertyChanged(sender, e);
    }

    public override string SelectionText
    {
      get
      {
        return "GanttChartBar";
      }
    }
  }
}
