﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls.VisualLayoutBorderControl
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Panes.LayoutPaneControls
{
  public class VisualLayoutBorderControl : BaseSelectableControl
  {
    internal LayoutBorder LayoutBorderControl
    {
      get
      {
        return this.m_ModelObject as LayoutBorder;
      }
    }

    public VisualLayoutBorderControl(IModelObject model)
      : base(model)
    {
      this.IsDraggable = true;
      this.AllowDrop = true;
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      Border border = new Border();
      border.BorderBrush = (Brush) new SolidColorBrush(Colors.Transparent);
      string text = border.BorderBrush.ToString();
      double left1 = 0.0;
      double top1 = 0.0;
      double right1 = 0.0;
      double bottom1 = 0.0;
      this.MinHeight = 20.0;
      if (this.LayoutBorderControl.Content != null)
      {
        UIElement uiElement = ControlFactory.ConstructLayoutElementControl(this.LayoutBorderControl.Content);
        border.Child = uiElement;
      }
      if (this.LayoutBorderControl.Margin != null)
      {
        if (this.LayoutBorderControl.Margin is StandardThickness)
        {
          left1 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.Margin as StandardThickness).Left);
          top1 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.Margin as StandardThickness).Top);
          right1 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.Margin as StandardThickness).Right);
          bottom1 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.Margin as StandardThickness).Bottom);
        }
        this.Margin = new Thickness(left1, top1, right1, bottom1);
      }
      if (this.LayoutBorderControl.BorderThickness != null)
      {
        double num;
        double bottom2 = num = 0.0;
        double right2 = num;
        double top2 = num;
        double left2 = num;
        if (this.LayoutBorderControl.BorderThickness is StandardThickness)
        {
          left2 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.BorderThickness as StandardThickness).Left);
          top2 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.BorderThickness as StandardThickness).Top);
          right2 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.BorderThickness as StandardThickness).Right);
          bottom2 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.BorderThickness as StandardThickness).Bottom);
        }
        border.BorderThickness = new Thickness(left2, top2, right2, bottom2);
      }
      if (this.LayoutBorderControl.Padding != null)
      {
        double num;
        double bottom2 = num = 0.0;
        double right2 = num;
        double top2 = num;
        double left2 = num;
        if (this.LayoutBorderControl.Padding is StandardThickness)
        {
          left2 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.Padding as StandardThickness).Left);
          top2 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.Padding as StandardThickness).Top);
          right2 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.Padding as StandardThickness).Right);
          bottom2 = VisualConfiguration.Instance.GetStandardThicknessValue((this.LayoutBorderControl.Padding as StandardThickness).Bottom);
        }
        border.Padding = new Thickness(left2, top2, right2, bottom2);
      }
      if (this.LayoutBorderControl.Background != null)
      {
        if (this.LayoutBorderControl.Background is StandardBrush)
          text = (this.LayoutBorderControl.Background as StandardBrush).ThemeColor.ToString();
        try
        {
          border.Background = (Brush) (new BrushConverter().ConvertFromString(text) as SolidColorBrush);
        }
        catch (Exception ex)
        {
          int num = (int) DisplayMessage.Show("Incorrect color string. Provide correct value for color", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
      if (this.LayoutBorderControl.BorderBrush != null)
      {
        if (this.LayoutBorderControl.BorderBrush is StandardBrush)
          text = (this.LayoutBorderControl.BorderBrush as StandardBrush).ThemeColor.ToString();
        try
        {
          border.BorderBrush = (Brush) (new BrushConverter().ConvertFromString(text) as SolidColorBrush);
        }
        catch (Exception ex)
        {
          int num = (int) DisplayMessage.Show("Incorrect color string. Provide correct value for color", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.PlaceElement(this.Grid, (UIElement) border, 0, 0);
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      this.ReloadView();
      base.OnModelObjectAdded(sender, e);
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      base.OnModelObjectRemoved(sender, e);
      this.ReloadView();
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      this.ReloadView();
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Paste");
      this.AddContextMenuItem("Cut");
      this.AddContextMenuItem("Copy");
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      base.HandleContextMenuItemClick(source);
      if (source.Header == (object) "Delete")
        this.LayoutBorderControl.Parent.RemoveModelObject((IModelObject) this.LayoutBorderControl, false);
      else if (source.Header == (object) "Paste")
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyPasteOperation();
      else if (source.Header == (object) "Cut")
      {
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCutOperation();
      }
      else
      {
        if (source.Header != (object) "Copy")
          return;
        ComponentModelerUtilities.Instance.GetParentDesigner((IDesignerControl) this).NotifyCopyOperation();
      }
    }

    internal override void TriggerSelection(BaseSelectableControl vControl, bool isTriggeredByControlAddOperation)
    {
      base.TriggerSelection(vControl, isTriggeredByControlAddOperation);
    }

    public override string SelectionText
    {
      get
      {
        return "Layout Border Control";
      }
    }
  }
}
