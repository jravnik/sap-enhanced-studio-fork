﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ThinkHeaderRegion.VisualThingHeaderRegion
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ThinkHeaderRegion
{
  public class VisualThingHeaderRegion : BaseSelectableControl
  {
    private static readonly Brush tableBackgroundBrush = (Brush) new SolidColorBrush(Color.FromArgb(byte.MaxValue, (byte) 212, (byte) 217, (byte) 222));
    private const string RESOURCE_BACKGROUND = "#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0";
    private VisualCardControl m_cardControl;
    private VisualThingHeaderToolBar m_toolbar;
    private DisplayTextControl title;
    private VisualLayout extendedContentRegion;
    private IFloorplan floorplan;
    private Grid thingHeaderGrid;

    public override string SelectionText
    {
      get
      {
        return "Thing Header";
      }
    }

    internal ThingHeaderRegion ThingHeader
    {
      get
      {
        return this.m_ModelObject as ThingHeaderRegion;
      }
    }

    public VisualThingHeaderRegion(IModelObject visualThingHeadermodel)
      : base(visualThingHeadermodel)
    {
      this.LoadView();
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      if (this.ThingHeader == null)
        return;
      this.floorplan = Utilities.GetFloorplanObject((IModelObject) this.ThingHeader);
      if (this.floorplan.Type == FloorplanType.QC)
      {
        this.AddTitle();
        this.AddExtendedContentRegion(1);
        this.AddToolBar(2);
      }
      else if (this.floorplan.Type == FloorplanType.QV)
      {
        this.AddTitle();
        this.AddCardControl();
        this.AddExtendedContentRegion(2);
        this.AddToolBar(3);
      }
      else
      {
        this.AddTitle();
        this.AddCardControl();
        this.AddToolBar(2);
        this.AddExtendedContentRegion(3);
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      base.OnModelPropertyChanged(sender, e);
      ThingHeaderRegion thingHeaderRegion = sender as ThingHeaderRegion;
      if (thingHeaderRegion != null && e.PropertyName == ThingHeaderRegion.ThingHeaderTitle)
        this.title.Text = thingHeaderRegion.Title != null ? thingHeaderRegion.Title.Text : (string) null;
      if (e.PropertyName == "ThingHeaderToolbar")
        this.m_toolbar.CustomToolBar = e.NewValue as ThingHeaderToolBar;
      this.ReloadView();
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      base.OnModelObjectAdded(sender, e);
    }

    private void AddTitle()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.title = new DisplayTextControl(TextViewStyles.PageHeaderTitle, this.ThingHeader.Title != null ? this.ThingHeader.Title.Text : string.Empty);
      this.title.Background = (Brush) ResourceUtil.GetLinearGradientBrush("#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0");
      this.title.Padding = new Thickness(5.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.title, 0, 0);
    }

    private void AddCardControl()
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.m_cardControl = new VisualCardControl((IModelObject) this.ThingHeader.CardControl);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_cardControl, 1, 0);
    }

    private void AddToolBar(int i)
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.m_toolbar = new VisualThingHeaderToolBar((IModelObject) this.ThingHeader.Toolbar);
      this.m_toolbar.Background = (Brush) ResourceUtil.GetLinearGradientBrush("#D1E0EF,0.0,#C3D5E4,0.3,#BACFDF,0.5,#B1C9DD,0.7,#ADC5DB,1.0");
      this.m_toolbar.Padding = new Thickness(5.0, 0.0, 0.0, 0.0);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_toolbar, i, 0);
    }

    private void AddExtendedContentRegion(int i)
    {
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      ScrollViewer scrollViewer = new ScrollViewer();
      scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
      scrollViewer.CanContentScroll = true;
      scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
      scrollViewer.Background = VisualThingHeaderRegion.tableBackgroundBrush;
      scrollViewer.BorderThickness = new Thickness(0.0);
      this.extendedContentRegion = new VisualLayout((IModelObject) this.ThingHeader.ExtendedContentRegion);
      scrollViewer.Content = (object) this.extendedContentRegion;
      GridUtil.PlaceElement(this.Grid, (UIElement) scrollViewer, i, 0);
    }
  }
}
