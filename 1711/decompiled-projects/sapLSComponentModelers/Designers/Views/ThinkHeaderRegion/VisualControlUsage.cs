﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ThinkHeaderRegion.VisualControlUsage
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ThinkHeaderRegion
{
  public class VisualControlUsage : BaseBorderControl
  {
    private UIElement visualControl;
    private VisualLabel labelControl;

    internal ControlUsage controlUsage
    {
      get
      {
        return this.m_ModelObject as ControlUsage;
      }
    }

    public VisualControlUsage(IModelObject controlUsageModel)
      : base(controlUsageModel)
    {
      this.AllowDrop = true;
      this.controlUsage.Item.PropertyChanged += new CustomPropertyChangedEventHandler(this.OnModelPropertyChanged);
      this.LoadView();
    }

    public override void LoadView()
    {
      GridUtil.AddRowDef(this.Grid, 25.0, this.controlUsage.Item is RadioButtonGroup ? GridUnitType.Auto : GridUnitType.Pixel);
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      GridUtil.AddColumnDef(this.Grid, 5.0, GridUnitType.Pixel);
      if (this.controlUsage.Item.CCTSType != UXCCTSTypes.none)
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      else if (this.controlUsage.Item.FieldWidth != null && this.controlUsage.Item.FieldWidth != string.Empty)
        this.SetFieldWidthOfControls();
      else
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      bool showAsterisk = false;
      if (this.controlUsage.Item is EditControl)
        showAsterisk = Converter.ToBoolean((DependentProperty) (this.controlUsage.Item as EditControl).Mandatory, false);
      else if (this.controlUsage.Item is CompoundField)
      {
        CompoundField compoundField = this.controlUsage.Item as CompoundField;
        if (compoundField.Items != null)
        {
          foreach (AbstractControl abstractControl in compoundField.Items)
          {
            if (abstractControl is EditControl)
            {
              EditControl editControl = abstractControl as EditControl;
              showAsterisk |= Converter.ToBoolean((DependentProperty) editControl.Mandatory, false);
            }
          }
        }
      }
      this.labelControl = new VisualLabel(this.controlUsage.Label != null ? this.controlUsage.Label.Text : string.Empty, showAsterisk, false);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.labelControl, 0, 0);
      if (!this.controlUsage.LabelVisible)
        this.labelControl.Visibility = Visibility.Hidden;
      this.visualControl = ControlFactory.ConstructFormpaneControl(this.controlUsage.Item, UXCCTSTypes.none, UsageType.form);
      if (this.controlUsage.Item is TextEditControl)
        this.SetHeightOfTextEditControl();
      else if (this.controlUsage.Item is LayoutContainerControl)
        this.Grid.RowDefinitions[0].Height = new GridLength(1.0, GridUnitType.Auto);
      GridUtil.PlaceElement(this.Grid, this.visualControl, 0, 2);
    }

    public override string SelectionText
    {
      get
      {
        return nameof (VisualControlUsage);
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        this.ReloadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to update UI on Property Changed event.", ex));
      }
    }

    private void SetFieldWidthOfControls()
    {
      double widthValue = ControlFactory.GetWidthValue(this.controlUsage.Item.FieldWidth);
      string nonDigitPart = Utilities.GetNonDigitPart(this.controlUsage.Item.FieldWidth);
      if (nonDigitPart == "%")
      {
        GridUtil.AddColumnDef(this.Grid, widthValue, GridUnitType.Star);
        GridUtil.AddColumnDef(this.Grid, 100.0 - widthValue, GridUnitType.Star);
      }
      else
      {
        if (!(nonDigitPart == "ex") && !(nonDigitPart == "px"))
          return;
        GridUtil.AddColumnDef(this.Grid, widthValue, GridUnitType.Pixel);
      }
    }

    private void SetHeightOfTextEditControl()
    {
      TextEditControl textEditControl = this.controlUsage.Item as TextEditControl;
      VisualTextEditControl visualControl = this.visualControl as VisualTextEditControl;
      if (textEditControl == null || visualControl == null || textEditControl.Rows == 0)
        return;
      visualControl.Rows = textEditControl.Rows;
      this.Grid.RowDefinitions[0].Height = new GridLength((double) textEditControl.Rows * 25.0, GridUnitType.Pixel);
    }
  }
}
