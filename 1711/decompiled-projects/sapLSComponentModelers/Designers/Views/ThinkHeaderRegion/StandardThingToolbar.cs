﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ThinkHeaderRegion.StandardThingToolbar
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ThinkHeaderRegion
{
  public class StandardThingToolbar : BaseVisualControl
  {
    internal Button m_Save;
    internal Button m_Cancel;

    public override string SelectionText
    {
      get
      {
        return string.Empty;
      }
    }

    public StandardThingToolbar(IModelObject model)
      : base(model)
    {
      this.Margin = ComponentModelersConstants.CONTROL_BORDER_MARGIN;
      this.LoadView();
    }

    public override void LoadView()
    {
      try
      {
        base.LoadView();
        this.Content = (object) this.Grid;
        GridUtil.AddRowDef(this.Grid, 30.0, GridUnitType.Pixel);
        this.Grid.HorizontalAlignment = HorizontalAlignment.Stretch;
        this.Grid.VerticalAlignment = VerticalAlignment.Center;
        this.m_Save = new Button();
        this.AddElement((FrameworkElement) this.m_Save);
        this.m_Save.Content = (object) "Save";
        this.m_Save.HorizontalAlignment = HorizontalAlignment.Left;
        this.m_Save.VerticalAlignment = VerticalAlignment.Center;
        this.m_Save.Background = (Brush) SkinConstants.BRUSH_BUTTON_STANDARDBACKCOLOR;
        this.m_Save.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
        this.m_Cancel = new Button();
        this.AddElement((FrameworkElement) this.m_Cancel);
        this.m_Cancel.Content = (object) "Close";
        this.m_Cancel.HorizontalAlignment = HorizontalAlignment.Left;
        this.m_Cancel.VerticalAlignment = VerticalAlignment.Center;
        this.m_Cancel.Background = (Brush) SkinConstants.BRUSH_BUTTON_STANDARDBACKCOLOR;
        this.m_Cancel.Margin = new Thickness(5.0, 0.0, 0.0, 0.0);
        this.Grid.Background = (Brush) SkinConstants.BRUSH_DEFAULTSET_BACKCOLOR;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading of Standard Thing ToolBar control failed.", ex));
      }
    }

    private void AddElement(FrameworkElement element)
    {
      this.Grid.ColumnDefinitions.Add(new ColumnDefinition()
      {
        Width = new GridLength()
      });
      this.Grid.Children.Add((UIElement) element);
      element.SetValue(Grid.ColumnProperty, (object) (this.Grid.ColumnDefinitions.Count - 1));
      element.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
    }
  }
}
