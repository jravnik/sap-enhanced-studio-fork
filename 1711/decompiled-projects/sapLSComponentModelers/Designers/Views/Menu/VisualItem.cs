﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Menu.VisualItem
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Menu
{
  public class VisualItem : BaseSelectableControl
  {
    private VisualLabel labelControl;
    private UIElement visualControl;

    private ControlUsage ControlUsageItem
    {
      get
      {
        return this.m_ModelObject as ControlUsage;
      }
    }

    public VisualItem(IModelObject item)
      : base(item)
    {
      this.AllowDrop = true;
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "Item";
      }
    }

    public override void LoadView()
    {
      try
      {
        this.Margin = new Thickness(2.0, 4.0, 4.0, 2.0);
        this.Grid.Background = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        GridUtil.AddRowDef(this.Grid, 21.0, GridUnitType.Pixel);
        GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        GridUtil.AddColumnDef(this.Grid, 5.0, GridUnitType.Pixel);
        if (this.ControlUsageItem.Item.CCTSType == UXCCTSTypes.amount || this.ControlUsageItem.Item.CCTSType == UXCCTSTypes.date || (this.ControlUsageItem.Item.CCTSType == UXCCTSTypes.description || this.ControlUsageItem.Item.CCTSType == UXCCTSTypes.identifier) || (this.ControlUsageItem.Item.CCTSType == UXCCTSTypes.measure || this.ControlUsageItem.Item.CCTSType == UXCCTSTypes.name || (this.ControlUsageItem.Item.CCTSType == UXCCTSTypes.note || this.ControlUsageItem.Item.CCTSType == UXCCTSTypes.quantity)) || this.ControlUsageItem.Item.CCTSType == UXCCTSTypes.uri)
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
        else
          GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
        this.labelControl = new VisualLabel(this.ControlUsageItem.Label != null ? this.ControlUsageItem.Label.Text : string.Empty, this.ControlUsageItem.Item is EditControl && Converter.ToBoolean((DependentProperty) (this.ControlUsageItem.Item as EditControl).Mandatory, false), false);
        GridUtil.PlaceElement(this.Grid, (UIElement) this.labelControl, 0, 0);
        this.visualControl = ControlFactory.ConstructFormpaneControl(this.ControlUsageItem.Item, UXCCTSTypes.none, UsageType.form);
        (this.visualControl as FrameworkElement).Width = 50.0;
        if (!this.ControlUsageItem.LabelVisible)
        {
          this.labelControl.Visibility = Visibility.Collapsed;
          GridUtil.PlaceElement(this.Grid, this.visualControl, 0, 0);
          this.Grid.Width = (this.visualControl as FrameworkElement).Width;
        }
        else
          GridUtil.PlaceElement(this.Grid, this.visualControl, 0, 2);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("ControlUsage item initialization failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      try
      {
        if (source.Header != (object) "Delete")
          return;
        ItemGroup parent1 = this.ControlUsageItem.Parent as ItemGroup;
        Toolbar parent2 = parent1.Parent as Toolbar;
        if (parent1.IsMandatory)
        {
          int num1 = (int) DisplayMessage.Show("Mandatory item cannot be deleted.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
        else if (parent1 != null && parent1.Item.Count > 1)
        {
          parent2.RemoveItemFromItemGroup(parent1, this.ControlUsageItem);
        }
        else
        {
          int num2 = (int) DisplayMessage.Show("Atleast one item should be present in the Itemgroup", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Item delete Operation failed.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is ControlUsage)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        this.ReloadView();
        base.OnModelPropertyChanged(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change failed.", ex));
      }
    }
  }
}
