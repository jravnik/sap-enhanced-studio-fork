﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Menu.VisualButtonGroup
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Menu;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Menu
{
  internal class VisualButtonGroup : BaseSelectableControl
  {
    private const string DEFAULT_BUTTON_NAME = "My Button Group";
    private ButtonGroup btnGroup;
    private WrapPanel wrapPanel;

    public VisualButtonGroup(ButtonGroup btnGroup)
      : base((IModelObject) btnGroup)
    {
      this.btnGroup = btnGroup;
      this.LoadView();
    }

    public override void LoadView()
    {
      this.wrapPanel = new WrapPanel();
      this.wrapPanel.Background = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Auto);
      foreach (IModelObject button in this.btnGroup.Buttons)
        this.wrapPanel.Children.Add((UIElement) new VisualMenuButton(button));
      GridUtil.PlaceElement(this.Grid, (UIElement) this.wrapPanel, 0, 0);
    }

    protected override void AddContextMenuItems()
    {
      base.AddContextMenuItems();
    }

    public override string SelectionText
    {
      get
      {
        return "Button Group";
      }
    }
  }
}
