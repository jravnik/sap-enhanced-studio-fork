﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Menu.VisualItemGroup
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.Menu
{
  public class VisualItemGroup : BaseSelectableControl
  {
    private BaseVisualControl parentVisualControl;
    private WrapPanel wrapPanel;

    private ItemGroup ItemGrp
    {
      get
      {
        return this.m_ModelObject as ItemGroup;
      }
    }

    public VisualItemGroup(IModelObject item, BaseVisualControl parentControl)
      : base(item)
    {
      this.AllowDrop = true;
      this.parentVisualControl = parentControl;
      this.wrapPanel = new WrapPanel();
      this.LoadView();
    }

    public override string SelectionText
    {
      get
      {
        return "ItemGroup";
      }
    }

    public override void LoadView()
    {
      try
      {
        this.Grid.Children.Add((UIElement) this.wrapPanel);
        this.wrapPanel.ClipToBounds = true;
        this.wrapPanel.Children.Clear();
        this.Margin = new Thickness(2.0, 2.0, 2.0, 2.0);
        this.BorderBrush = (Brush) ComponentModelersConstants.ACTIVE_BORDER_BRUSH;
        this.BorderThickness = ComponentModelersConstants.CONTROL_BORDER_THICKNESS;
        this.Grid.Background = (Brush) SkinConstants.BRUSH_SOLID_BACKCOLOR;
        foreach (IModelObject modelObject in this.ItemGrp.Item)
          this.wrapPanel.Children.Add((UIElement) new VisualItem(modelObject));
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("ControlUsage item initialization failed.", ex));
      }
    }

    protected override void AddContextMenuItems()
    {
      this.AddSeperatorToContextMenu();
      this.AddContextMenuItem("Delete");
    }

    protected override void HandleContextMenuItemClick(System.Windows.Controls.MenuItem source)
    {
      try
      {
        if (source.Header != (object) "Delete")
          return;
        Toolbar parent = this.ItemGrp.Parent as Toolbar;
        if (this.ItemGrp.IsMandatory)
        {
          int num = (int) DisplayMessage.Show("Mandatory item group cannot be deleted.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
        else
        {
          if (parent == null)
            return;
          parent.RemoveItemGroupFromToolbar(this.ItemGrp);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("ItemGroup delete Operation failed.", ex));
      }
    }

    protected override void OnModelObjectAdded(object sender, ModelAddedEventArgs e)
    {
      try
      {
        if (e.AddedModel is ControlUsage)
          this.ReloadView();
        base.OnModelObjectAdded(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing the UI with newly added control failed.", ex));
      }
    }

    protected override void OnModelObjectRemoved(object sender, ModelRemovedEventArgs e)
    {
      try
      {
        this.ReloadView();
        base.OnModelObjectRemoved(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to refresh the UI on ModelRemovedEvent.", ex));
      }
    }

    protected override void OnModelPropertyChanged(object sender, PropertyChangedEventArgument e)
    {
      try
      {
        if (e.PropertyName == ItemGroup.AlignmentProperty)
        {
          if (this.parentVisualControl != null)
            this.parentVisualControl.ReloadView();
        }
        else
          this.ReloadView();
        base.OnModelPropertyChanged(sender, e);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Property change failed.", ex));
      }
    }
  }
}
