﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.OIFDesigner
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.Views.ContextualNavigationRegion;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Designers.ViewSwitchArea;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.Designers
{
  public class OIFDesigner : ComponentDesigner
  {
    private static readonly SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox m_ComponentToolbox = ToolboxUtilities.DeserialzeToolboxComponent(Resource.ObjectInstanceToolbox);
    private VisualIdentificationRegion m_VisualIDR;
    private VisualViewSwitchCNR m_VisualCNR;
    private ViewSwitchNavArea viewSwitchNav;
    private VisualView m_ViewControl;

    public override SAP.BYD.LS.UIDesigner.UICore.Toolwindowcontrols.Toolbox.Toolbox Toolbox
    {
      get
      {
        return OIFDesigner.m_ComponentToolbox;
      }
    }

    internal ObjectInstanceFloorplan Component
    {
      get
      {
        return this.m_ModelObject as ObjectInstanceFloorplan;
      }
    }

    public OIFDesigner(IModelObject rootModel)
      : base(rootModel, true, true)
    {
      this.AllowDrop = true;
    }

    protected override void BringSelectedModelToFront(IModelObject modelToSelect)
    {
      IModelObject modelObject = modelToSelect;
      List<IModelObject> modelObjectList = new List<IModelObject>();
      for (; modelObject != null && !string.IsNullOrEmpty(modelObject.Id); modelObject = modelObject.Parent)
      {
        modelObjectList.Add(modelObject);
        if (modelObject is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.View || modelObject.Parent == null)
          break;
      }
      if (modelObject is SAP.BYD.LS.UIDesigner.Model.Entities.Controls.View)
      {
        if (!this.m_DesignerControlDictionary.ContainsKey((modelObject as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.View).NavigationItem.Id))
          return;
        IDesignerControl designerControl1 = this.m_DesignerControlDictionary[(modelObject as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.View).NavigationItem.Id];
        IDesignerControl designerControl2 = this.m_DesignerControlDictionary[(modelObject as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.View).NavigationItem.Id];
        NavigationItem navigationItem = (modelObject as SAP.BYD.LS.UIDesigner.Model.Entities.Controls.View).NavigationItem;
        if (navigationItem is ViewSwitchNavigationItem && !(navigationItem.Parent is ViewSwitchNavigationItem))
        {
          ViewSwitchNavigationItem switchNavigationItem = navigationItem as ViewSwitchNavigationItem;
          if (this.m_DesignerControlDictionary.ContainsKey(switchNavigationItem.Id))
          {
            IDesignerControl designerControl3 = this.m_DesignerControlDictionary[switchNavigationItem.Id];
            if (designerControl3 is VisualViewTextControl)
              (designerControl3 as VisualViewTextControl).Item.Select();
          }
        }
        else if (navigationItem.Parent is ViewSwitchNavigationItem)
        {
          ViewSwitchNavigationItem parent = navigationItem.Parent as ViewSwitchNavigationItem;
          if (this.m_DesignerControlDictionary.ContainsKey(parent.Id))
          {
            IDesignerControl designerControl3 = this.m_DesignerControlDictionary[parent.Id];
            if (designerControl3 is VisualViewTextControl)
            {
              (designerControl3 as VisualViewTextControl).Item.Select();
              if (this.m_DesignerControlDictionary.ContainsKey(navigationItem.Id))
              {
                IDesignerControl designerControl4 = this.m_DesignerControlDictionary[navigationItem.Id];
                if (designerControl4 is SubViewTextControl)
                  (designerControl4 as SubViewTextControl).SubViewNav.setTextBlockClicked((object) designerControl4);
              }
              (designerControl3 as VisualViewTextControl).Item.ViewSwitchNavigationList.SelectViewNavigation(navigationItem.NavigationItemId);
            }
          }
        }
        if (this.m_DesignerControlDictionary.ContainsKey(modelToSelect.Id))
        {
          this.m_DesignerControlDictionary[modelToSelect.Id].SelectMe();
        }
        else
        {
          if (!this.m_DesignerControlDictionary.ContainsKey(modelToSelect.Parent.Id))
            return;
          this.m_DesignerControlDictionary[modelToSelect.Parent.Id].SelectMe();
        }
      }
      else
        base.BringSelectedModelToFront(modelToSelect);
    }

    private void SelectViewSwitchNavItem(ViewSwitchNavigationItem vsItem)
    {
      if (!(vsItem.Parent is ViewSwitchNavigationItem))
      {
        if (!this.m_DesignerControlDictionary.ContainsKey(vsItem.Id))
          return;
        IDesignerControl designerControl = this.m_DesignerControlDictionary[vsItem.Id];
        if (!(designerControl is VisualViewTextControl))
          return;
        (designerControl as VisualViewTextControl).Item.Select();
      }
      else
      {
        this.SelectViewSwitchNavItem(vsItem.Parent as ViewSwitchNavigationItem);
        if (!this.m_DesignerControlDictionary.ContainsKey(vsItem.Id))
          return;
        SubViewTextControl designerControl = this.m_DesignerControlDictionary[vsItem.Id] as SubViewTextControl;
      }
    }

    public override void LoadView()
    {
      base.LoadView();
      GridUtil.AddColumnDef(this.Grid, 1.0, GridUnitType.Star);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.AddIdentificationRegion();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.AddContextualNavigationRegion();
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Star);
      this.AddContentRegion();
      this.LoadLinkedControlRegion();
    }

    private void AddIdentificationRegion()
    {
      if (this.m_VisualIDR != null)
      {
        this.m_VisualIDR.Terminate();
        this.Grid.Children.Remove((UIElement) this.m_VisualIDR);
      }
      this.m_VisualIDR = new VisualIdentificationRegion((IModelObject) this.Component.IdentificationArea);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_VisualIDR, 0, 0);
    }

    private void AddContextualNavigationRegion()
    {
      if (this.m_VisualCNR != null)
      {
        this.m_VisualCNR.Terminate();
        this.Grid.Children.Remove((UIElement) this.m_VisualCNR);
      }
      this.m_VisualCNR = new VisualViewSwitchCNR((IModelObject) this.Component.ContextNavigationPanel);
      GridUtil.PlaceElement(this.Grid, (UIElement) this.m_VisualCNR, 1, 0);
      GridUtil.AddRowDef(this.Grid, 1.0, GridUnitType.Auto);
      this.viewSwitchNav = new ViewSwitchNavArea((IModelObject) this.Component.ContextNavigationPanel);
      this.viewSwitchNav.AddHandler(VisualViewSwitchNavigation.SelectedEvent, (Delegate) new RoutedEventHandler(this.OnOIFNavItemSelectionChanged));
      GridUtil.PlaceElement(this.Grid, (UIElement) this.viewSwitchNav, 2, 0);
    }

    private void AddContentRegion()
    {
      if (this.m_ViewControl != null)
      {
        this.m_ViewControl.Terminate();
        this.Grid.Children.Remove((UIElement) this.m_ViewControl);
      }
      string selectedNavId = this.viewSwitchNav.SelectedNavID;
      if (selectedNavId == null)
        return;
      foreach (SAP.BYD.LS.UIDesigner.Model.Entities.Controls.View view in this.Component.Views)
      {
        if (view.NavigationItemId == selectedNavId)
        {
          this.m_ViewControl = new VisualView((IModelObject) view);
          GridUtil.PlaceElement(this.Grid, (UIElement) this.m_ViewControl, 3, 0);
          break;
        }
      }
    }

    private void OnOIFNavItemSelectionChanged(object sender, RoutedEventArgs e)
    {
      Cursor current = Cursor.Current;
      Cursor.Current = Cursors.WaitCursor;
      try
      {
        if (!(e.OriginalSource is VisualViewSwitchNavigation))
          return;
        this.AddContentRegion();
      }
      finally
      {
        Cursor.Current = current;
      }
    }
  }
}
