﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.ComponentDesignersProvider
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.Templates;
using SAP.BYD.LS.UIDesigner.PluginModelers;
using SAP.BYD.LS.UIDesigner.UICore.Commands;
using SAP.BYD.LS.UIDesigner.UICore.Core;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers
{
  public class ComponentDesignersProvider : DesignerProviderBase
  {
    private PluginDesignersProvider m_PluginDesignerProvider = new PluginDesignersProvider();
    private const string CATEGORY_FLOORPLAN = "Floorplan";
    private const string CATEGORY_REUSABLE = "Reusable Components";
    private const string CATEGORY_DIALOG = "Dialog Components";
    private const string CATEGORY_THINGTYPE = "ThingType Components";
    private const string CATEGORY_FIORIAPPS = "Fiori App Components";
    private const string CATEGORY_OFFLINE = "Offline Components";
    private const string TYPE_WCF = "Work Center Floorplan";
    private const string TYPE_OIF = "Object Instance Floorplan";
    private const string TYPE_QAF = "Quick Activity Floorplan";
    private const string TYPE_FS = "Fact Sheet Floorplan";
    private const string TYPE_MA = "Mobile App Container";
    private const string TYPE_TL = "Tile";
    private const string TYPE_GAF = "Guided Activity Floorplan";
    private const string TYPE_MD = "Modal Dialog";
    private const string TYPE_EC = "Embedded Component";
    private const string TYPE_WCVIEW = "Work Center View";
    private const string TYPE_OVS = "Object Value Selector";
    private const string TYPE_INHERITED_COMPONENT = "InheritedComponent";
    private const string TYPE_PTP = "Port Type Package";
    private const string TYPE_OWL = "Object Work List";
    private const string TYPE_TOWL = "Task Based OWL";
    private const string TYPE_TASKLISTVIEW = "Task List View";
    private const string TYPE_THINGTYPE = "Thing Type";
    private const string TYPE_OCM = "Offline Component Model";
    private const string TYPE_DATAOBJECT = "Data Object";
    private const string TYPE_QUICKVIEW = "Quick View";
    private const string TYPE_QUICKCREATE = "Quick Create";
    private const string TYPE_THINGINSPECTOR = "Thing Inspector";
    private const string TYPE_PT = "Print Template";
    private const string TYPE_MASHUPPORTBINDING = "Mashup Port Binding";
    private const string WCF_WIZARD_TITLE = "Creation of Overview, MyTasks and Reports View";
    private const string WCF_FORMTITLE = "Save the WCF Component";
    private bool m_IsPluginComponent;

    protected override void AddCategories()
    {
      try
      {
        this.m_Categories.Add("Floorplan");
        this.m_Categories.Add("Reusable Components");
        this.m_Categories.Add("Dialog Components");
        if (LoginManager.Instance.DTMode == DTModes.Partner)
          return;
        this.m_Categories.Add("ThingType Components");
        this.m_Categories.Add("Fiori App Components");
        this.m_Categories.Add("Offline Components");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    protected override void AddCommandItems()
    {
      try
      {
        this.m_CommandItems.Add(new CommandItem()
        {
          Command = (ToolStripMenuItem) OpenCommand.Instance,
          ObjectType = typeof (FloorplanInfo)
        });
        this.m_CommandItems.Add(new CommandItem()
        {
          Command = (ToolStripMenuItem) EditCommand.Instance,
          ObjectType = typeof (FloorplanInfo)
        });
        this.m_CommandItems.Add(new CommandItem()
        {
          Command = (ToolStripMenuItem) SaveCommand.Instance,
          ObjectType = typeof (IFloorplan)
        });
        this.m_CommandItems.Add(new CommandItem()
        {
          Command = (ToolStripMenuItem) PreviewCommand.Instance,
          ObjectType = typeof (IFloorplan)
        });
        this.m_CommandItems.Add(new CommandItem()
        {
          Command = (ToolStripMenuItem) SaveAsCommand.Instance,
          ObjectType = typeof (IFloorplan)
        });
        this.m_CommandItems.Add(new CommandItem()
        {
          Command = (ToolStripMenuItem) DeleteCommand.Instance,
          ObjectType = typeof (FloorplanInfo)
        });
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    protected override void AddTemplateItems()
    {
      List<TemplateInfo> templateInfoCollection1 = ModelLayer.TemplateManager.GetTemplateInfoCollection(FloorplanType.OWL);
      List<TemplateInfo> templateInfoCollection2 = ModelLayer.TemplateManager.GetTemplateInfoCollection(FloorplanType.OVS);
      List<TemplateItem> templateItemList1 = new List<TemplateItem>();
      List<TemplateItem> templateItemList2 = new List<TemplateItem>();
      foreach (TemplateInfo templateInfo in templateInfoCollection1)
      {
        TemplateItem templateItem = new TemplateItem();
        templateItem.ItemText = templateInfo.TemplateName;
        if (templateInfo.TemplateName == "SADL-OWL")
        {
          templateItem.ItemImage = SkinResource.OWL48x48;
          templateItem.Description = "SADL-OWL is a UI pattern which is enabled for UI Controller infrastructure and supports SADL based Queries.";
        }
        else if (templateInfo.TemplateName == "T-OWL")
        {
          templateItem.ItemImage = SkinResource.TOWL48x48;
          templateItem.Description = "T-OWL is a pattern which is pre configured with the different tasks which can be performed within the business context.";
        }
        else if (templateInfo.TemplateName == "DATASET-OWL")
        {
          templateItem.ItemImage = SkinResource.OWL48x48;
          templateItem.Description = "OWL UI pattern is preconfigured with support for DataSet and SADL based Queries.";
        }
        else if (templateInfo.TemplateName == "SPLITPANE-OWL")
        {
          templateItem.ItemImage = SkinResource.OWL48x48;
          templateItem.Description = "SplitPane pattern is enabled for Fiori App Development only.This will not work for Desktop client.";
        }
        templateItem.TemplateInfo = templateInfo;
        templateItemList1.Add(templateItem);
      }
      this.m_TemplateItems.Add("Object Work List", templateItemList1);
      foreach (TemplateInfo templateInfo in templateInfoCollection2)
      {
        TemplateItem templateItem = new TemplateItem();
        templateItem.ItemText = templateInfo.TemplateName;
        if (templateInfo.TemplateName == "SADL-OVS")
        {
          templateItem.ItemImage = SkinResource.OVS48x48;
          templateItem.Description = "SADL-OVS is a UI pattern which is enabled for UI Controller infrastructure.The data retrieval is based on SADL Query.";
        }
        templateItem.TemplateInfo = templateInfo;
        templateItemList2.Add(templateItem);
      }
      this.m_TemplateItems.Add("Object Value Selector", templateItemList2);
    }

    protected override void AddCategoryItems()
    {
      try
      {
        List<ListItem> componentItems = new List<ListItem>();
        List<ListItem> reuseComponentItems = new List<ListItem>();
        List<ListItem> dilogComponentItems = new List<ListItem>();
        List<ListItem> listItemList1 = new List<ListItem>();
        List<ListItem> listItemList2 = new List<ListItem>();
        List<ListItem> listItemList3 = new List<ListItem>();
        componentItems.Add(new ListItem()
        {
          ItemText = "Object Instance Floorplan",
          ItemImage = SkinResource.OIF48x48,
          ComponentType = FloorplanType.OIF,
          Description = "The Object Instance Floorplan (OIF) allows users to create, delete, view, and edit attributes and associations of a business object (e.g. an employee, a purchase order, or a supplier's record)."
        });
        componentItems.Add(new ListItem()
        {
          ItemText = "Quick Activity Floorplan",
          ItemImage = SkinResource.QAF48x48,
          ComponentType = FloorplanType.QA,
          Description = "The Quick Activity Floorplan (QAF) allows the user to quickly perform a specific task. This can be self-contained or a short sub-task within the context of a larger task (e.g. a quick create)."
        });
        componentItems.Add(new ListItem()
        {
          ItemText = "Fact Sheet Floorplan",
          ItemImage = SkinResource.Factsheet48x48,
          ComponentType = FloorplanType.FS,
          Description = "A Fact Sheet (FS) is a one page overview of an object in a display-only presentation."
        });
        componentItems.Add(new ListItem()
        {
          ItemText = "Work Center Floorplan",
          ItemImage = SkinResource.WCF48x48,
          ComponentType = FloorplanType.WCF,
          HasWizard = true,
          HasShortId = true,
          Description = "Work Centers (WoC), together with the Control Center (CC), form the primary work area. WoCs are represented as tabs in the upper navigation bar (located after the Home tab)."
        });
        componentItems.Add(new ListItem()
        {
          ItemText = "Guided Activity Floorplan",
          ItemImage = SkinResource.GAF48x48,
          ComponentType = FloorplanType.GAF,
          Description = "The Guided Activity Floorplan (GAF) is a floorplan for an activity that can be divided in a logical sequence of steps. It consists of a series of screens that guide the user through an activity to achieve a specific goal."
        });
        this.m_PluginDesignerProvider.AddComponentsItems(componentItems);
        dilogComponentItems.Add(new ListItem()
        {
          ItemText = "Modal Dialog",
          ItemImage = SkinResource.MD48x48,
          ComponentType = FloorplanType.MD,
          Description = "Modal Dialogs are small movable and resizable windows that help the user perform a task out of an application context. They will be triggered out of a calling application and have to be completed before the user can continue to work in the calling application."
        });
        dilogComponentItems.Add(new ListItem()
        {
          ItemText = "Object Value Selector",
          ItemImage = SkinResource.OVS48x48,
          ComponentType = FloorplanType.OVS,
          Description = "The Object Value Selector(OVS) is a UI pattern to support users in selecting values for an input field. It helps to select one item from a table of BO instances. It is most commonly called from an input field with a selection dialog icon."
        });
        this.m_PluginDesignerProvider.AddDialogComponentItems(dilogComponentItems);
        reuseComponentItems.Add(new ListItem()
        {
          ItemText = "Work Center View",
          ItemImage = SkinResource.WCVF48x48,
          ComponentType = FloorplanType.WCVIEW,
          HasShortId = true,
          Description = "A container component for a view switch entry in the work center which acts as a metadata container and bundles OWL etc. It can be reused in multiple work centers."
        });
        reuseComponentItems.Add(new ListItem()
        {
          ItemText = "Embedded Component",
          ItemImage = SkinResource.EC48x48,
          ComponentType = FloorplanType.EC,
          Description = "An Embedded Component is a visual reusable component. It can be plugged into other components and also allows to host a Custom Pane."
        });
        reuseComponentItems.Add(new ListItem()
        {
          ItemText = "Port Type Package",
          ItemImage = SkinResource.PTP48x48,
          ComponentType = FloorplanType.PTP,
          Description = "A component which hosts one or more so called Port Type declarations. A PortType is the blueprint for an Inport or Outport. Inports and Outports with the same Port Type identifies components which can be plugged together without any mapping effort."
        });
        reuseComponentItems.Add(new ListItem()
        {
          ItemText = "Mashup Port Binding",
          ItemImage = SkinResource.MashupReg48x48,
          ComponentType = FloorplanType.PB,
          Description = "A component which define inport type and outport type to empower data navigation between a floorplan and a mashup component"
        });
        reuseComponentItems.Add(new ListItem()
        {
          ItemText = "Object Work List",
          ItemImage = SkinResource.OWL48x48,
          ComponentType = FloorplanType.OWL,
          Description = "The Object Worklist (OWL) is a UI pattern which is always embedded in a Work Center view. The OWL contains business objects such as purchase orders or opportunities."
        });
        reuseComponentItems.Add(new ListItem()
        {
          ItemText = "Task List View",
          ItemImage = SkinResource.TLV48x48,
          ComponentType = FloorplanType.TaskListView,
          Description = "A metadata container to define a cluster of selected task types. This can be assigned as a special entity to work center views., which is user later on to assemble the generic My Tasks view in a work center."
        });
        this.m_PluginDesignerProvider.AddReuseComponentItems(reuseComponentItems);
        listItemList1.Add(new ListItem()
        {
          ItemText = "Thing Type",
          ItemImage = SkinResource.ThingType48x48,
          HasShortId = true,
          ComponentType = FloorplanType.TT,
          Description = "Things are self containing objects."
        });
        listItemList1.Add(new ListItem()
        {
          ItemText = "Data Object",
          ItemImage = SkinResource.ThingType48x48,
          HasShortId = true,
          ComponentType = FloorplanType.DO,
          Description = "Data Objects."
        });
        listItemList1.Add(new ListItem()
        {
          ItemText = "Quick View",
          ItemImage = SkinResource.QuickView48x48,
          ComponentType = FloorplanType.QV,
          Description = "The Quickview is a preview on a Thing.It appears wherever the user might want to get some more information on a Thing without navigation. A Quickview cannot be edited."
        });
        listItemList1.Add(new ListItem()
        {
          ItemText = "Quick Create",
          ItemImage = SkinResource.QuickCreate48x48,
          ComponentType = FloorplanType.QC,
          Description = "Quick Create enables users to quickly create an object record,through a slide-in window, without navigating away from the current page"
        });
        listItemList1.Add(new ListItem()
        {
          ItemText = "Thing Inspector",
          ItemImage = SkinResource.ThingInspector48x48,
          ComponentType = FloorplanType.TI,
          Description = "The Thing Inspector can be used to get a 360 view of a Thing.It exposes all aspects of a Thing including a handle on the Things (the embedded Swatch),a compact summary of the primary attributes, Tags and the generic Thing actions."
        });
        listItemList1.Add(new ListItem()
        {
          ItemText = "Print Template",
          ItemImage = SkinResource.QAF48x48,
          ComponentType = FloorplanType.QA,
          Description = "Print Template is a form template which can be used for to define the content and layout of summary documents that can be output from the system.These documents are read-only PDF documents that are generated from data stored in the system and can be printed."
        });
        listItemList2.Add(new ListItem()
        {
          ItemText = "Mobile App Container",
          ItemImage = SkinResource.QAF48x48,
          ComponentType = FloorplanType.MA,
          Description = "This Floorplan should be used for Fiori Application development"
        });
        listItemList2.Add(new ListItem()
        {
          ItemText = "Tile",
          ItemImage = SkinResource.Factsheet48x48,
          ComponentType = FloorplanType.TL,
          Description = "Tile Floorplan for Fiori Application development"
        });
        listItemList2.Add(new ListItem()
        {
          ItemText = "Object Work List",
          ItemImage = SkinResource.OWL48x48,
          ComponentType = FloorplanType.OWL,
          Description = "This Floorplan should be used for Fiori Application development"
        });
        listItemList3.Add(new ListItem()
        {
          ItemText = "Offline Component Model",
          ItemImage = SkinResource.ThingType48x48,
          HasShortId = true,
          ComponentType = FloorplanType.OCM,
          Description = "This is a model used for offline support"
        });
        this.m_ListItems.Add("Floorplan", componentItems);
        this.m_ListItems.Add("Reusable Components", reuseComponentItems);
        this.m_ListItems.Add("Dialog Components", dilogComponentItems);
        this.m_ListItems.Add("ThingType Components", listItemList1);
        this.m_ListItems.Add("Fiori App Components", listItemList2);
        this.m_ListItems.Add("Offline Components", listItemList3);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    public override IWizardStepCollection GetWizardStepsForCategory(string selectedType)
    {
      IWizardStepCollection wizardStepCollection = (IWizardStepCollection) null;
      if (selectedType != string.Empty && selectedType == "Work Center Floorplan")
        wizardStepCollection = (IWizardStepCollection) new WizardStepCollection("Creation of Overview, MyTasks and Reports View", WizardStepConfiguration.GetInstance().CreateWCFWizardSteps());
      return wizardStepCollection;
    }

    public override IDesigner OpenExisting(IPersistable objectToOpen)
    {
      try
      {
        if (objectToOpen is FloorplanInfo)
          return this.OpenExisting(objectToOpen as FloorplanInfo, false);
        if (objectToOpen is IFloorplan)
          return this.OpenDesigner(objectToOpen as IFloorplan);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
      return (IDesigner) null;
    }

    public override IDesigner OpenExistingForEditing(IPersistable objectToOpen)
    {
      try
      {
        FloorplanInfo objectToOpen1 = objectToOpen as FloorplanInfo;
        if (objectToOpen1 != null)
          return this.OpenExisting(objectToOpen1, true);
        IFloorplan fpToOpen = objectToOpen as IFloorplan;
        if (fpToOpen != null)
          return this.OpenDesigner(fpToOpen);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
      return (IDesigner) null;
    }

    public override IDesigner OpenTemplate(TemplateInfo tempInfo)
    {
      IDesigner designer = (IDesigner) null;
      try
      {
        IFloorplan floorplanModel = ModelLayer.ObjectManager.GetFloorplanModel(tempInfo);
        floorplanModel.IsDisplayOnly = false;
        designer = this.OpenDesigner(floorplanModel);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
      return designer;
    }

    public override IDesigner OpenExisting(string projectPath)
    {
      throw new NotImplementedException();
    }

    public override IDesigner OpenExisting(string componentXML, FloorplanInfo componentInfo, bool isDisplayOnly, bool isLatestVersion, string version)
    {
      try
      {
        if (!string.IsNullOrEmpty(componentXML))
          return this.OpenDesigner(ModelLayer.ObjectManager.GetFloorplanModel(componentXML, componentInfo, isDisplayOnly, isLatestVersion, version));
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
      return (IDesigner) null;
    }

    public override IDesigner OpenNew(string strCategory, ListItem contextItem)
    {
      IDesigner designer = (IDesigner) null;
      try
      {
        IFloorplan fpToOpen = (IFloorplan) null;
        bool flag = false;
        if (contextItem.ItemText == "Quick Activity Floorplan")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateQuickActivityFloorplanModel();
        else if (contextItem.ItemText == "Print Template")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreatePrintTemplateFloorplanModel();
        else if (contextItem.ItemText == "Mobile App Container")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateMobileAppContainerModel();
        else if (contextItem.ItemText == "Tile")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateTileComponentModel();
        else if (contextItem.ItemText == "Fact Sheet Floorplan")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateFactSheetComponentModel();
        else if (contextItem.ItemText == "Modal Dialog")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateModalDialogFloorplanModel();
        else if (contextItem.ItemText == "Object Instance Floorplan")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateObjectInstanceFloorplanModel();
        else if (contextItem.ItemText == "Embedded Component")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateEmbeddedComponentModel();
        else if (contextItem.ItemText == "Work Center View")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateWorkCenterViewModel();
        else if (contextItem.ItemText == "Work Center Floorplan")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateWorkCenterFloorplanModel();
        else if (contextItem.ItemText == "Object Value Selector")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateObjectValueSelector();
        else if (contextItem.ItemText == "Port Type Package")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreatePortTypePackage();
        else if (contextItem.ItemText == "Guided Activity Floorplan")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateGuidedActivityFloorplanModel();
        else if (contextItem.ItemText == "InheritedComponent")
          fpToOpen = ModelLayer.ObjectManager.CreateInheritedComponentModel();
        else if (contextItem.ItemText == "Object Work List")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreatObjectWorkList();
        else if (contextItem.ItemText == "Task List View")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateTaskListView();
        else if (contextItem.ItemText == "Thing Type")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateThingTypeComponent();
        else if (contextItem.ItemText == "Offline Component Model")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateOCMComponent();
        else if (contextItem.ItemText == "Data Object")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateDataObjectComponent();
        else if (contextItem.ItemText == "Quick View")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateQuickViewComponent();
        else if (contextItem.ItemText == "Thing Inspector")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateThingInspectorComponent();
        else if (contextItem.ItemText == "Quick Create")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateQuickCreateComponent();
        else if (contextItem.ItemText == "Mashup Port Binding")
          fpToOpen = (IFloorplan) ModelLayer.ObjectManager.CreateMashupCategoryComponent();
        else
          flag = true;
        if (fpToOpen != null && !flag)
        {
          fpToOpen.IsDisplayOnly = false;
          designer = this.OpenDesigner(fpToOpen);
        }
        else
          designer = this.m_PluginDesignerProvider.OpenNew(strCategory, contextItem);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
      return designer;
    }

    private IDesigner OpenExisting(FloorplanInfo objectToOpen, bool WithEditLock)
    {
      IDesigner designer = (IDesigner) null;
      IFloorplan floorplanModel = ModelLayer.ObjectManager.GetFloorplanModel(objectToOpen, WithEditLock);
      if (floorplanModel != null)
      {
        floorplanModel.RepositoryPath = objectToOpen.RepositoryPath;
        designer = this.OpenDesigner(floorplanModel);
      }
      return designer;
    }

    private IDesigner OpenDesigner(IFloorplan fpToOpen)
    {
      BaseDesigner baseDesigner = (BaseDesigner) null;
      try
      {
        baseDesigner = (BaseDesigner) ComponentModelerUtilities.Instance.GetWPFDesigner(fpToOpen);
        baseDesigner.LoadView();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
      return (IDesigner) baseDesigner;
    }
  }
}
