﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls.VisualInputField
// Assembly: sapLSComponentModelers, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: 3E90F49C-BE21-474B-8847-C78F8910FF44
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\sapLSComponentModelers.dll

using SAP.BYD.LS.UIDesigner.ComponentModelers.BaseControls;
using SAP.BYD.LS.UIDesigner.ComponentModelers.Core;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SAP.BYD.LS.UIDesigner.ComponentModelers.GenericControls
{
  public partial class VisualInputField : UserControl, IComponentConnector
  {
    private int rowSpan = 1;
    private FieldStyles style;
    private IconStyles iconStyle;
    private bool hasBorder;
    private static Dictionary<FieldStyles, Brush> borderBrushes;
    private static Dictionary<FieldStyles, Brush> backgroundBrushes;
    private static Dictionary<FieldStyles, Brush> foregroundBrushes;
    //internal Grid LayoutRoot;
    //internal Border elementBorder;
    //internal Grid contentGrid;
    //internal Rectangle inner_shadow_1;
    //internal Rectangle inner_shadow_2;
    //internal TextBox textBox;
    //internal Image iconArrow;
    //internal Image iconF4;
    //internal Image iconCalculator;
    //internal Image iconCalendar;
    //private bool _contentLoaded;

    public event EventHandler<EventArgs> ValueHelpRequested;

    public FieldStyles FieldStyle
    {
      get
      {
        return this.style;
      }
      set
      {
        this.style = value;
      }
    }

    public IconStyles IconStyle
    {
      get
      {
        return this.iconStyle;
      }
      set
      {
        this.iconStyle = value;
        this.ApplyIconStyle();
      }
    }

    public bool ReadOnly
    {
      get
      {
        return this.textBox.IsReadOnly;
      }
      set
      {
        this.textBox.IsReadOnly = value;
      }
    }

    public string Text
    {
      get
      {
        return this.textBox.Text;
      }
      set
      {
        this.textBox.Text = value;
      }
    }

    public bool HasBorder
    {
      get
      {
        return this.hasBorder;
      }
      set
      {
        this.hasBorder = value;
        if (this.hasBorder)
        {
          this.elementBorder.BorderThickness = new Thickness(1.0);
          this.inner_shadow_1.Visibility = Visibility.Visible;
          this.inner_shadow_2.Visibility = Visibility.Visible;
        }
        else
        {
          this.elementBorder.BorderThickness = new Thickness(0.0);
          this.inner_shadow_1.Visibility = Visibility.Collapsed;
          this.inner_shadow_2.Visibility = Visibility.Collapsed;
        }
      }
    }

    public int RowSpan
    {
      get
      {
        return this.rowSpan;
      }
      set
      {
        this.rowSpan = value;
        double num = 25.0 * (double) this.rowSpan;
        this.Height = num;
        this.textBox.Height = num - 5.0;
      }
    }

    public VisualInputField()
      : this(true, false)
    {
    }

    public VisualInputField(bool hasBorder)
      : this(hasBorder, false)
    {
    }

    public VisualInputField(bool hasBorder, bool isTextEditControl)
    {
      this.InitializeComponent();
      this.Height = 20.0;
      this.textBox.BorderThickness = new Thickness(0.0);
      this.hasBorder = hasBorder;
      if (!hasBorder)
      {
        this.elementBorder.BorderThickness = new Thickness(0.0);
        this.inner_shadow_1.Visibility = Visibility.Collapsed;
        this.inner_shadow_2.Visibility = Visibility.Collapsed;
      }
      if (isTextEditControl)
      {
        this.textBox.TextWrapping = TextWrapping.Wrap;
        this.textBox.AcceptsReturn = true;
        this.textBox.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
        this.textBox.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
      }
      this.FieldStyle = FieldStyles.Editable;
      this.IconStyle = IconStyles.None;
      this.iconArrow.MouseLeftButtonUp += new MouseButtonEventHandler(this.icon_MouseLeftButtonDown);
      this.iconCalculator.MouseLeftButtonUp += new MouseButtonEventHandler(this.icon_MouseLeftButtonDown);
      this.iconCalendar.MouseLeftButtonUp += new MouseButtonEventHandler(this.icon_MouseLeftButtonDown);
      this.iconF4.MouseLeftButtonUp += new MouseButtonEventHandler(this.icon_MouseLeftButtonDown);
      this.textBox.GotFocus += new RoutedEventHandler(this.textBox_GotFocus);
    }

    private void InitializeStyles()
    {
      VisualInputField.borderBrushes = new Dictionary<FieldStyles, Brush>();
      VisualInputField.backgroundBrushes = new Dictionary<FieldStyles, Brush>();
      VisualInputField.foregroundBrushes = new Dictionary<FieldStyles, Brush>();
      VisualInputField.borderBrushes[FieldStyles.Editable] = (Brush) this.Resources[(object) "inputfield_not_focused_editable_border"];
      VisualInputField.backgroundBrushes[FieldStyles.Editable] = (Brush) this.Resources[(object) "inputfield_not_focused_editable_background"];
      VisualInputField.foregroundBrushes[FieldStyles.Editable] = (Brush) this.Resources[(object) "inputfield_not_focused_editable_text"];
      VisualInputField.borderBrushes[FieldStyles.Readonly] = (Brush) this.Resources[(object) "inputfield_not_focused_read_only_border"];
      VisualInputField.backgroundBrushes[FieldStyles.Readonly] = (Brush) this.Resources[(object) "inputfield_not_focused_read_only_background"];
      VisualInputField.foregroundBrushes[FieldStyles.Readonly] = (Brush) this.Resources[(object) "inputfield_not_focused_editable_text"];
      VisualInputField.borderBrushes[FieldStyles.Disabled] = (Brush) this.Resources[(object) "inputfield_not_focused_disabled_border"];
      VisualInputField.backgroundBrushes[FieldStyles.Disabled] = (Brush) this.Resources[(object) "inputfield_not_focused_disabled_background"];
      VisualInputField.foregroundBrushes[FieldStyles.Disabled] = (Brush) this.Resources[(object) "inputfield_not_focused_disabled_text"];
      VisualInputField.borderBrushes[FieldStyles.EditableFocused] = (Brush) this.Resources[(object) "inputfield_focused_editable_border"];
      VisualInputField.backgroundBrushes[FieldStyles.EditableFocused] = (Brush) this.Resources[(object) "inputfield_focused_editable_background"];
      VisualInputField.foregroundBrushes[FieldStyles.EditableFocused] = (Brush) this.Resources[(object) "inputfield_focused_editable_text"];
    }

    private void ApplyIconStyle()
    {
      if (this.style != FieldStyles.Editable)
        this.iconStyle = IconStyles.None;
      this.iconArrow.Visibility = this.iconStyle == IconStyles.Arrow ? Visibility.Visible : Visibility.Collapsed;
      this.iconCalculator.Visibility = this.iconStyle == IconStyles.Calculator ? Visibility.Visible : Visibility.Collapsed;
      this.iconCalendar.Visibility = this.iconStyle == IconStyles.Calendar ? Visibility.Visible : Visibility.Collapsed;
      this.iconF4.Visibility = this.iconStyle == IconStyles.F4 ? Visibility.Visible : Visibility.Collapsed;
    }

    private void icon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      if (this.ValueHelpRequested == null)
        return;
      this.ValueHelpRequested((object) this, EventArgs.Empty);
    }

    private void textBox_GotFocus(object sender, RoutedEventArgs e)
    {
      BaseSelectableControl selectableControl = ComponentModelerUtilities.Instance.GetBaseSelectableControl((FrameworkElement) this);
      if (selectableControl == null)
        return;
      selectableControl.TriggerSelection(selectableControl, false);
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSComponentModelers;component/genericcontrols/visualinputfield.xaml", UriKind.Relative));
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.LayoutRoot = (Grid) target;
    //      break;
    //    case 2:
    //      this.elementBorder = (Border) target;
    //      break;
    //    case 3:
    //      this.contentGrid = (Grid) target;
    //      break;
    //    case 4:
    //      this.inner_shadow_1 = (Rectangle) target;
    //      break;
    //    case 5:
    //      this.inner_shadow_2 = (Rectangle) target;
    //      break;
    //    case 6:
    //      this.textBox = (TextBox) target;
    //      break;
    //    case 7:
    //      this.iconArrow = (Image) target;
    //      break;
    //    case 8:
    //      this.iconF4 = (Image) target;
    //      break;
    //    case 9:
    //      this.iconCalculator = (Image) target;
    //      break;
    //    case 10:
    //      this.iconCalendar = (Image) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
