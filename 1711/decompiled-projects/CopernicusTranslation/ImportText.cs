﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Translation.ImportText
// Assembly: CopernicusTranslation, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: CA9B1E10-5FAB-4C52-8A64-B1F675DF4BDA
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusTranslation.dll

using SAP.Copernicus.Core;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Translation.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Translation
{
  public class ImportText : BaseForm
  {
    private List<string> xRepPathlist = new List<string>();
    private new IContainer components;
    private Label ImportLocationTxt;
    private GroupBox ImportBrowsegrp;
    private Button ImportBrowseButton;
    private TextBox ImportPathTextBox;
    private Label label2;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.ImportLocationTxt = new Label();
      this.ImportBrowsegrp = new GroupBox();
      this.label2 = new Label();
      this.ImportBrowseButton = new Button();
      this.ImportPathTextBox = new TextBox();
      this.ImportBrowsegrp.SuspendLayout();
      this.SuspendLayout();
      this.buttonCancel.Location = new Point(410, 164);
      this.buttonOk.Location = new Point(329, 164);
      this.ImportLocationTxt.AutoSize = true;
      this.ImportLocationTxt.Location = new Point(9, 29);
      this.ImportLocationTxt.Name = "ImportLocationTxt";
      this.ImportLocationTxt.Size = new Size(51, 13);
      this.ImportLocationTxt.TabIndex = 1;
      this.ImportLocationTxt.Text = "Location:";
      this.ImportBrowsegrp.Controls.Add((Control) this.label2);
      this.ImportBrowsegrp.Controls.Add((Control) this.ImportLocationTxt);
      this.ImportBrowsegrp.Controls.Add((Control) this.ImportBrowseButton);
      this.ImportBrowsegrp.Controls.Add((Control) this.ImportPathTextBox);
      this.ImportBrowsegrp.Location = new Point(12, 79);
      this.ImportBrowsegrp.Name = "ImportBrowsegrp";
      this.ImportBrowsegrp.Size = new Size(484, 67);
      this.ImportBrowsegrp.TabIndex = 2;
      this.ImportBrowsegrp.TabStop = false;
      this.ImportBrowsegrp.Text = "Select a translated XLIFF file:";
      this.label2.AutoSize = true;
      this.label2.ForeColor = Color.Red;
      this.label2.Location = new Point(56, 33);
      this.label2.Name = "label2";
      this.label2.Size = new Size(11, 13);
      this.label2.TabIndex = 3;
      this.label2.Text = "*";
      this.ImportBrowseButton.Location = new Point(417, 27);
      this.ImportBrowseButton.Name = "ImportBrowseButton";
      this.ImportBrowseButton.Size = new Size(61, 20);
      this.ImportBrowseButton.TabIndex = 1;
      this.ImportBrowseButton.Text = "Browse...";
      this.ImportBrowseButton.UseVisualStyleBackColor = true;
      this.ImportBrowseButton.Click += new EventHandler(this.ImportBrowseButton_Click);
      this.ImportPathTextBox.Location = new Point(77, 27);
      this.ImportPathTextBox.Name = "ImportPathTextBox";
      this.ImportPathTextBox.Size = new Size(334, 20);
      this.ImportPathTextBox.TabIndex = 0;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(508, 202);
      this.Controls.Add((Control) this.ImportBrowsegrp);
      this.MaximizeBox = false;
      this.Name = nameof (ImportText);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Subtitle = "You can import a translated XLIFF file from your local computer.";
      this.Text = "Import Translation";
      this.Load += new EventHandler(this.ImportText_Load);
      this.Controls.SetChildIndex((Control) this.buttonCancel, 0);
      this.Controls.SetChildIndex((Control) this.ImportBrowsegrp, 0);
      this.Controls.SetChildIndex((Control) this.buttonOk, 0);
      this.ImportBrowsegrp.ResumeLayout(false);
      this.ImportBrowsegrp.PerformLayout();
      this.ResumeLayout(false);
    }

    public ImportText(List<string> pathlist)
      : base(HELP_IDS.BDS_TRANSLATION_IMPORT)
    {
      this.InitializeComponent();
      this.xRepPathlist = pathlist;
    }

    private void ImportText_Load(object sender, EventArgs e)
    {
      this.Icon = Resource.SAPBusinessByDesignStudioIcon;
    }

    private void ImportBrowseButton_Click(object sender, EventArgs e)
    {
      OpenFileDialog openFileDialog = new OpenFileDialog();
      openFileDialog.Filter = "Translation file (*.xlf)|*.xlf";
      openFileDialog.Title = "Open";
      if (openFileDialog.ShowDialog() != DialogResult.OK)
        return;
      this.ImportPathTextBox.Text = openFileDialog.FileName;
    }

    protected override void buttonOk_Click(object sender, EventArgs e)
    {
      bool flag = false;
      string text = this.ImportPathTextBox.Text;
      if (string.IsNullOrEmpty(text))
      {
        int num1 = (int) CopernicusMessageBox.Show(TranslationResource.MsgImpFile, "File Path", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else if (!File.Exists(text))
      {
        int num2 = (int) CopernicusMessageBox.Show(TranslationResource.MsgImpPathValid, "File Path", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        try
        {
          Cursor.Current = Cursors.WaitCursor;
          using (StreamReader streamReader = new StreamReader(text, Encoding.UTF8))
            flag = new TranslationHandler().ImportTextHandler(this.xRepPathlist.ToArray(), Convert.ToBase64String(Encoding.UTF8.GetBytes(streamReader.ReadToEnd())));
          Cursor.Current = Cursors.Default;
        }
        catch (IOException ex)
        {
          int num3 = (int) CopernicusMessageBox.Show(TranslationResource.MsgImpErr + " " + ex.Message, "Import error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
        this.Visible = false;
        if (flag)
          CopernicusStatusBar.Instance.ShowMessage(TranslationResource.MsgImpSuccess);
        this.Close();
      }
    }

    private void ImportCancelButton_Click(object sender, EventArgs e)
    {
      this.Close();
    }
  }
}
