﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Translation.CheckTranslation
// Assembly: CopernicusTranslation, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: CA9B1E10-5FAB-4C52-8A64-B1F675DF4BDA
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusTranslation.dll

using SAP.Copernicus.Core;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Translation.Resources;
using SAP.CopernicusProjectView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace SAP.Copernicus.Translation
{
    public class QuickTranslation : BaseForm
    {
        private List<string> _Title = new List<string>();
        private List<string[]> _DataArray = new List<string[]>();
        private List<KeyValuePair<string, string>> LangComboList = new List<KeyValuePair<string, string>>();
        private KeyValuePair<string, string> Masterlang = new KeyValuePair<string, string>("EN", "English");
        private new IContainer components;
        private GroupBox groupBox1;
        private DataGridView dataGridView2;
        private GroupBox ComponentSummaryGrp;
        private Label label1;
        private Label label2;
        private Label label3;
        private ComboBox ExportLangCmbbox;
        private RichTextBox richTextBox3;
        private DataGridViewImageColumn Status;
        private DataGridViewTextBoxColumn SourceString;
        private DataGridViewTextBoxColumn TargetString;
        private DataGridViewTextBoxColumn LoadedTargetString;
        private Button button1;
        private ZLANG_STRUC[] LanguageDesc;
        private ZLANG_STRUC[] LanguageArray;
        private bool isSolution;
        private List<string> XERPPathlist;
        private string exportFileName;
        private string fileName;
        private XmlDocument doc;

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            DataGridViewCellStyle gridViewCellStyle1 = new DataGridViewCellStyle();
            this.groupBox1 = new GroupBox();
            this.label2 = new Label();
            this.label3 = new Label();
            this.label1 = new Label();
            this.dataGridView2 = new DataGridView();
            this.Status = new DataGridViewImageColumn();
            this.SourceString = new DataGridViewTextBoxColumn();
            this.TargetString = new DataGridViewTextBoxColumn();
            this.LoadedTargetString = new DataGridViewTextBoxColumn();
            this.ComponentSummaryGrp = new GroupBox();
            this.richTextBox3 = new RichTextBox();
            this.ExportLangCmbbox = new ComboBox();
            this.button1 = new Button();
            this.groupBox1.SuspendLayout();
            ((ISupportInitialize)this.dataGridView2).BeginInit();
            this.ComponentSummaryGrp.SuspendLayout();
            this.SuspendLayout();
            this.groupBox1.Controls.Add((Control)this.label2);
            this.groupBox1.Controls.Add((Control)this.label3);
            this.groupBox1.Controls.Add((Control)this.ExportLangCmbbox);
            this.groupBox1.Controls.Add((Control)this.label1);
            this.groupBox1.Controls.Add((Control)this.dataGridView2);
            this.groupBox1.Location = new Point(12, 75);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(618, 447);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Translatable Strings";
            this.label2.AutoSize = true;
            this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte)0);
            this.label2.Location = new Point(136, 20);
            this.label2.Name = "label2";
            this.label2.Size = new Size(41, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "English";
            this.label3.AutoSize = true;
            this.label3.Location = new Point(10, 20);
            this.label3.Name = "label3";
            this.label3.Size = new Size(95, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Source Language:";
            this.label1.AutoSize = true;
            this.label1.Location = new Point(10, 40);
            this.label1.Name = "label1";
            this.label1.Size = new Size(121, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Target Language:";
            this.ExportLangCmbbox.FormattingEnabled = true;
            this.ExportLangCmbbox.Location = new Point(135, 38);
            this.ExportLangCmbbox.Name = "ExportLangCmbbox";
            this.ExportLangCmbbox.Size = new Size(179, 21);
            this.ExportLangCmbbox.TabIndex = 0;
            this.ExportLangCmbbox.LostFocus += new EventHandler(this.ExportLangCmbbox_LostFocus);
            gridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleLeft;
            gridViewCellStyle1.BackColor = SystemColors.Control;
            gridViewCellStyle1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte)0);
            gridViewCellStyle1.ForeColor = SystemColors.WindowText;
            gridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = gridViewCellStyle1;
            this.dataGridView2.Columns.AddRange((DataGridViewColumn)this.Status, (DataGridViewColumn)this.SourceString, (DataGridViewColumn)this.TargetString, (DataGridViewColumn)this.LoadedTargetString);
            this.dataGridView2.EditMode = DataGridViewEditMode.EditOnEnter;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new Point(13, 67);
            this.dataGridView2.MaximumSize = new Size(600, 371);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.ScrollBars = ScrollBars.Vertical;
            this.dataGridView2.Size = new Size(599, 370);
            this.dataGridView2.TabIndex = 1;
            this.Status.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.Status.HeaderText = "Completed";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 100;
            this.SourceString.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.SourceString.HeaderText = "Source";
            this.SourceString.Name = "SourceString";
            this.SourceString.ReadOnly = true;
            this.TargetString.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.TargetString.HeaderText = "Target";
            this.TargetString.Name = "TargetString";
            this.TargetString.ReadOnly = false;
            this.LoadedTargetString.Visible = true;
            this.LoadedTargetString.HeaderText = "Target (Loaded)";
            this.LoadedTargetString.Name = "LoadedTargetString";
            this.LoadedTargetString.ReadOnly = true;
            this.ComponentSummaryGrp.Controls.Add((Control)this.richTextBox3);
            this.ComponentSummaryGrp.Location = new Point(12, 465);
            this.ComponentSummaryGrp.Name = "ComponentSummaryGrp";
            this.ComponentSummaryGrp.Size = new Size(618, 328);
            this.ComponentSummaryGrp.TabIndex = 2;
            this.ComponentSummaryGrp.TabStop = false;
            this.ComponentSummaryGrp.Text = "Item Information";
            this.richTextBox3.BackColor = SystemColors.Control;
            this.richTextBox3.BorderStyle = BorderStyle.None;
            this.richTextBox3.Location = new Point(10, 21);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new Size(594, 35);
            this.richTextBox3.TabIndex = 7;
            this.richTextBox3.Text = "This overview displays the translatable text strings as items and the translated text strings for a set language.";
            this.button1.Location = new Point(555, 528);
            this.button1.Name = "button3";
            this.button1.Size = new Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new EventHandler(this.ButtonOk_Click);
            this.buttonCancel.Location = new Point(480, 528);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(644, 838);
            this.Controls.Add((Control)this.button1);
            this.Controls.Add((Control)this.ComponentSummaryGrp);
            this.Controls.Add((Control)this.groupBox1);
            this.MinimumSize = new Size(660, 600);
            this.Name = nameof(CheckTranslation);
            this.StartPosition = FormStartPosition.CenterParent;
            this.Subtitle = "This overview displays the translatable text strings as items and the translated text strings for a set language.";
            this.Text = "Quick Translation";
            this.Load += new EventHandler(this.CheckTranslation_Load);
            this.Controls.SetChildIndex((Control)this.groupBox1, 0);
            this.Controls.SetChildIndex((Control)this.ComponentSummaryGrp, 0);
            this.Controls.SetChildIndex((Control)this.button1, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((ISupportInitialize)this.dataGridView2).EndInit();
            this.ComponentSummaryGrp.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        public string ExportFileName
        {
            get
            {
                return this.exportFileName;
            }
            set
            {
                this.exportFileName = value;
            }
        }

        public string FileName
        {
            get
            {
                return this.fileName;
            }
            set
            {
                this.fileName = value;
            }
        }

        public QuickTranslation(List<string> xRepPathlist, bool isSolutionNode)
          : base(HELP_IDS.BDS_TRANSLATION_CHECK)
        {
            this.InitializeComponent();
            bool flag = false;
            this.ExportLangCmbbox.SelectedIndexChanged += new EventHandler(this.ExportLangCmbbox_SelectedIndexChanged);
            this.LanguageArray = new TranslationHandler().getLanguage();
            if (this.LanguageArray == null)
                return;
            if (this.LanguageArray.Length > 0)
            {
                this.ExportLangCmbbox.Items.Clear();
                int num1 = 0;
                int num2 = 0;
                foreach (ZLANG_STRUC language in this.LanguageArray)
                {
                    if (language.LANGUAGE.ToString() != "EN")
                    {
                        flag = true;
                        this.LangComboList.Add(new KeyValuePair<string, string>(language.LANGUAGE, language.DESCRIPTION));
                        if (language.LANGUAGE.ToString() == "DE")
                            num2 = num1;
                        ++num1;
                    }
                }
                if (flag)
                {
                    this.ExportLangCmbbox.DataSource = (object)null;
                    this.ExportLangCmbbox.DataSource = (object)new BindingSource((object)this.LangComboList, (string)null);
                    this.ExportLangCmbbox.ValueMember = "Key";
                    this.ExportLangCmbbox.DisplayMember = "Value";
                    this.ExportLangCmbbox.SelectedIndex = num2;
                }
                else
                {
                    int num3 = (int)CopernicusMessageBox.Show(TranslationResource.MsgLangError, "Missing Languages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            else
            {
                int num = (int)CopernicusMessageBox.Show(TranslationResource.MsgLangError, "Missing Languages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

            TranslationHandler translationHandler = new TranslationHandler();
            this.LanguageDesc = translationHandler.getLanguage();
            if (this.LanguageDesc == null)
            {
                int num = (int)CopernicusMessageBox.Show(TranslationResource.MsgLangError, "Languages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                this.XERPPathlist = xRepPathlist;
                ZPDI_CHECK_STR[] CheckList = translationHandler.CheckTextHandler(xRepPathlist.ToArray());
                if (CheckList == null)
                    return;
                this.setSolutionNode(isSolutionNode);
                this.fillDataGridview(CheckList);
            }
        }

        private void DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView2.RowCount && e.ColumnIndex == 2)
            {
                DataGridViewRow row = this.dataGridView2.Rows[e.RowIndex];

                if (row != null)
                {
                    object targetString = row.Cells["TargetString"].Value;

                    if (targetString != null)
                    {
                        object loadedTargetString = row.Cells["LoadedTargetString"].Value;

                        if (loadedTargetString == null)
                        {
                            row.Cells["Status"].Value = (object)ActionIcons.YellowOK;
                        }
                        else
                        {
                            if (loadedTargetString.ToString() != targetString.ToString())
                            {
                                row.Cells["Status"].Value = (object)ActionIcons.YellowOK;
                            }
                        }
                    }
                }
            }
        }

        private void setSolutionNode(bool Val)
        {
            this.isSolution = false;
            this.Size = new Size(660, 345);
            this.ComponentSummaryGrp.Visible = false;
            this.button1.Visible = true;
        }

        private void fillDataGridview(ZPDI_CHECK_STR[] CheckList)
        {
            if (this.LanguageDesc.Length <= 0)
                return;
            this._Title.Add(TranslationResource.CheckTableColComp.ToString());
            List<string> stringList1 = new List<string>();
            foreach (ZPDI_CHECK_STR check in CheckList)
            {
                if (!stringList1.Contains(check.UICOMPONENT))
                    stringList1.Add(check.UICOMPONENT);
            }
            this._DataArray.Add(stringList1.ToArray());
            this._Title.Add(this.Masterlang.Value);
            List<string> stringList2 = new List<string>();
            foreach (ZPDI_CHECK_STR check in CheckList)
            {
                if (check.LANGUAGE == this.Masterlang.Value)
                    stringList2.Add(check.TEXTCOUNT);
            }
            this._DataArray.Add(stringList2.ToArray());
            foreach (ZLANG_STRUC zlangStruc in this.LanguageDesc)
            {
                if (zlangStruc.LANGUAGE != this.Masterlang.Key)
                {
                    this._Title.Add(zlangStruc.DESCRIPTION);
                    List<string> stringList3 = new List<string>();
                    foreach (ZPDI_CHECK_STR check in CheckList)
                    {
                        if (check.LANGUAGE == zlangStruc.DESCRIPTION)
                            stringList3.Add(check.TEXTCOUNT);
                    }
                    this._DataArray.Add(stringList3.ToArray());
                }
            }
            List<KeyValuePair<string, int>> keyValuePairList = new List<KeyValuePair<string, int>>();
            foreach (ZLANG_STRUC zlangStruc in this.LanguageDesc)
            {
                if (zlangStruc.LANGUAGE != this.Masterlang.Key)
                    keyValuePairList.Add(new KeyValuePair<string, int>(zlangStruc.DESCRIPTION, 0));
            }

            string text1 = this.ExportLangCmbbox.Text;
            string text2 = this.FileName + "_DE.xlf";
            string text3 = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string str = Path.Combine(text3, text2);
            TranslationHandler translationHandler = new TranslationHandler();
            string language1 = "DE";
            if (this.LanguageArray != null)
            {
                foreach (ZLANG_STRUC language2 in this.LanguageArray)
                {
                    if (language2.DESCRIPTION.Equals(text1))
                        language1 = language2.LANGUAGE;
                }
            }
            string xmlsstring;
            if (translationHandler.ExportTextHandler(this.XERPPathlist.ToArray(), language1, str, "X", out xmlsstring))
            {
                this.doc = new XmlDocument();
                this.doc.LoadXml(xmlsstring);

                XmlNodeList transUnits = this.doc.GetElementsByTagName("trans-unit");
                int i = 0;

                foreach (XmlNode transUnit in transUnits)
                {
                    if (transUnit.HasChildNodes)
                    {
                        this.dataGridView2.Rows.Add();
                        bool translated = false;

                        foreach (XmlNode child in transUnit.ChildNodes)
                        {
                            if (child.Name == "source")
                            {
                                this.dataGridView2.Rows[i].Cells["SourceString"].Value = child.InnerText;
                            }
                            else if (child.Name == "target")
                            {
                                this.dataGridView2.Rows[i].Cells["TargetString"].Value = child.InnerText;
                                this.dataGridView2.Rows[i].Cells["LoadedTargetString"].Value = child.InnerText;
                                translated = true;
                            }
                        }

                        if (translated)
                        {
                            this.dataGridView2.Rows[i].Cells["Status"].Value = (object)ActionIcons.OK;
                            this.dataGridView2.Rows[i].Cells["Status"].ToolTipText = "String is translated";
                        }
                        else
                        {
                            this.dataGridView2.Rows[i].Cells["Status"].Value = (object)ActionIcons.Error;
                            this.dataGridView2.Rows[i].Cells["Status"].ToolTipText = "String is not translated";
                        }

                        i++;
                    }
                }

                this.dataGridView2.CellValueChanged += new DataGridViewCellEventHandler(this.DataGridView_CellValueChanged);
            }
            else
            {
                CopernicusStatusBar.Instance.ShowMessage(TranslationResource.MsgExpErr);
                this.Close();
            }
        }

        private DataTable GetResultsTable()
        {
            DataTable dataTable = new DataTable();
            for (int index1 = 0; index1 < this._DataArray.Count; ++index1)
            {
                string columnName = this._Title[index1];
                dataTable.Columns.Add(columnName);
                List<object> objectList = new List<object>();
                foreach (string str in this._DataArray[index1])
                    objectList.Add((object)str);
                while (dataTable.Rows.Count < objectList.Count)
                    dataTable.Rows.Add();
                for (int index2 = 0; index2 < objectList.Count; ++index2)
                    dataTable.Rows[index2][index1] = objectList[index2];
            }
            return dataTable;
        }

        private void CheckTranslation_Load(object sender, EventArgs e)
        {
            this.Icon = SAP.Copernicus.Resource.SAPBusinessByDesignStudioIcon;
        }

        private void ButtonOk_Click(object sender, EventArgs e)
        {
            XmlNodeList transUnits = this.doc.GetElementsByTagName("trans-unit");
            int i = 0;

            foreach (XmlNode transUnit in transUnits)
            {
                if (transUnit.HasChildNodes)
                {
                    DataGridViewRow row = this.dataGridView2.Rows[i];
                    bool translated = false;

                    foreach (XmlNode child in transUnit.ChildNodes)
                    {
                        if (child.Name == "target")
                        {
                            child.InnerText = row.Cells["TargetString"].Value.ToString();
                            translated = true;
                        }
                    }

                    if (!translated)
                    {
                        String targetText = "";
                        DataGridViewCell targetCell = row.Cells["TargetString"];

                        if (targetCell != null)
                        {
                            if (targetCell.Value != null)
                            {
                                targetText = targetCell.Value.ToString();
                            }
                        }

                        transUnit.InnerXml += ("<target>" + targetText + "</target>");
                    }

                    i++;
                }
            }

            string text = BeautifyXML(this.doc);
            Clipboard.SetText(text);
            bool flag = false;

            if (string.IsNullOrEmpty(text))
            {
                int num1 = (int)CopernicusMessageBox.Show(TranslationResource.MsgImpErr + " XLF is empty or null", "Import error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                flag = new TranslationHandler().ImportTextHandler(this.XERPPathlist.ToArray(), Convert.ToBase64String(Encoding.UTF8.GetBytes(text)));
                this.Visible = false;
                if (flag)
                {
                    CopernicusStatusBar.Instance.ShowMessage(TranslationResource.MsgImpSuccess);
                }
                this.Close();
            }
        }

        private string BeautifyXML(XmlDocument xml)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                xml.Save(writer);
            }
            return sb.ToString();
        }

        private void ExportLangCmbbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (!String.IsNullOrEmpty(this.FileName))
            //{
            //    this.ExportFileName = this.FileName + "_" + this.LangComboList[this.ExportLangCmbbox.SelectedIndex].Key + ".xlf";
            //}
            //else
            //{
            //    string str;
            //    if (this.XERPPathlist.Count > 1)
            //    {
            //        //str = CopernicusProjectSystemUtil.getSelectedProjectName();
            //    }
            //    else
            //    {
            //        str = CopernicusProjectSystemUtil.getSelectedNodeName();
            //        string[] strArray = str.Split('.');
            //        if (!string.IsNullOrEmpty(strArray[0]))
            //            str = strArray[0];
            //    }
            //    this.ExportFileName = str + "_" + this.LangComboList[this.ExportLangCmbbox.SelectedIndex].Key + ".xlf";
            //}
        }

        private void ExportLangCmbbox_LostFocus(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            bool flag = false;
            if (comboBox.SelectedItem != null)
            {
                KeyValuePair<string, string> selectedItem = (KeyValuePair<string, string>)comboBox.SelectedItem;
                foreach (ZLANG_STRUC language in this.LanguageArray)
                {
                    if (selectedItem.Key == language.LANGUAGE)
                    {
                        flag = true;
                        if (selectedItem.Value.ToUpper() != language.DESCRIPTION.ToUpper())
                        {
                            int num = (int)CopernicusMessageBox.Show(TranslationResource.MsgExpLangInvalid, "Export error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            this.ExportLangCmbbox.ResetText();
                            return;
                        }
                    }
                }
                if (flag)
                    return;
                int num1 = (int)CopernicusMessageBox.Show(TranslationResource.MsgExpLangInvalid, "Export error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.ExportLangCmbbox.ResetText();
            }
            else
            {
                int num = (int)CopernicusMessageBox.Show(TranslationResource.MsgExpLangInvalid, "Export error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.ExportLangCmbbox.ResetText();
            }
        }
    }
}
