﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Parser.XBOWalker
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using Antlr.Runtime;
using Antlr.Runtime.Tree;
using Microsoft.VisualStudio.Modeling;
using Microsoft.VisualStudio.Modeling.Validation;
using SAP.Copernicus.BOCompiler.Common;
using SAP.Copernicus.BOCompiler.Common.Annotations;
using SAP.Copernicus.BusinessObjectLanguage;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Repository.DataModel;
using SAP.Copernicus.Core.Scripting;
using SAP.Copernicus.Extension.LanguageService;
using SAP.Copernicus.Model;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace SAP.Copernicus.Extension.Parser
{
  [CLSCompliant(false)]
  [GeneratedCode("ANTLR", "3.4")]
  public class XBOWalker : TreeParser
  {
    internal static readonly string[] tokenNames = new string[84]
    {
      "<invalid>",
      "<EOR>",
      "<DOWN>",
      "<UP>",
      nameof (ACTION),
      nameof (ADD),
      nameof (ALIAS),
      nameof (AND),
      nameof (ANYCHAR),
      nameof (AS),
      nameof (ASSOCIATION),
      nameof (AST_ACTION),
      nameof (AST_ALIAS),
      nameof (AST_ASSOC),
      nameof (AST_BINARY),
      nameof (AST_BO),
      nameof (AST_CALL),
      nameof (AST_DEFAULT),
      nameof (AST_DO_NODE),
      nameof (AST_ELEMENT),
      nameof (AST_EXT_NODE_REF),
      nameof (AST_LAMBDA),
      nameof (AST_MESSAGE),
      nameof (AST_MULT),
      nameof (AST_NAMESPACE),
      nameof (AST_NODE),
      nameof (AST_PATH),
      nameof (AST_QUALID),
      nameof (AST_RAISES),
      nameof (AST_TYPE),
      nameof (AST_UNARY),
      nameof (AST_VAL_PAR),
      nameof (BUSINESSOBJECT),
      nameof (CN),
      nameof (COLON),
      nameof (COMMA),
      nameof (DECLIT),
      nameof (DIV),
      nameof (DOT),
      nameof (Digit),
      nameof (ELEMENT),
      nameof (ELLIPS),
      nameof (EQ),
      nameof (EscapeSequence),
      nameof (ExponentPart),
      nameof (GE),
      nameof (GT),
      nameof (HexDigit),
      nameof (ID),
      nameof (IDENT),
      nameof (IMPORT),
      nameof (LAMBDA_OPS),
      nameof (LBRACK),
      nameof (LCURLYB),
      nameof (LE),
      nameof (LROUNDB),
      nameof (LTT),
      nameof (MESSAGE),
      nameof (ML_COMMENT),
      nameof (MOD),
      nameof (MUL),
      nameof (MULTLIT),
      nameof (NE),
      nameof (NL),
      nameof (NODE),
      nameof (NOT),
      nameof (NUMLIT),
      nameof (OR),
      nameof (RAISES),
      nameof (RBRACK),
      nameof (RCURLYB),
      nameof (RROUNDB),
      nameof (SEMI),
      nameof (SL_COMMENT),
      nameof (STRING),
      nameof (STRING_FRAGMENT),
      nameof (SUB),
      nameof (StringChar),
      nameof (TEXT),
      nameof (TO),
      nameof (USING),
      nameof (UnicodeChar),
      nameof (VALUATION),
      nameof (WS)
    };
    private static readonly bool[] decisionCanBacktrack = new bool[0];
    private List<string> boDefaultsList = new List<string>();
    private List<string> elementDefaults = new List<string>();
    public const string pdiAbslID = "ID";
    public const string pdiAbslIDProxyName = "PDI_ABSL_IDENTIFIER";
    public const string pdiAbslNamespaceESRName = "http://sap.com/xi/AP/PDI/ABSL";
    internal const string TRACE_CATEGORY = "XBOWalker";
    public const int EOF = -1;
    public const int ACTION = 4;
    public const int ADD = 5;
    public const int ALIAS = 6;
    public const int AND = 7;
    public const int ANYCHAR = 8;
    public const int AS = 9;
    public const int ASSOCIATION = 10;
    public const int AST_ACTION = 11;
    public const int AST_ALIAS = 12;
    public const int AST_ASSOC = 13;
    public const int AST_BINARY = 14;
    public const int AST_BO = 15;
    public const int AST_CALL = 16;
    public const int AST_DEFAULT = 17;
    public const int AST_DO_NODE = 18;
    public const int AST_ELEMENT = 19;
    public const int AST_EXT_NODE_REF = 20;
    public const int AST_LAMBDA = 21;
    public const int AST_MESSAGE = 22;
    public const int AST_MULT = 23;
    public const int AST_NAMESPACE = 24;
    public const int AST_NODE = 25;
    public const int AST_PATH = 26;
    public const int AST_QUALID = 27;
    public const int AST_RAISES = 28;
    public const int AST_TYPE = 29;
    public const int AST_UNARY = 30;
    public const int AST_VAL_PAR = 31;
    public const int BUSINESSOBJECT = 32;
    public const int CN = 33;
    public const int COLON = 34;
    public const int COMMA = 35;
    public const int DECLIT = 36;
    public const int DIV = 37;
    public const int DOT = 38;
    public const int Digit = 39;
    public const int ELEMENT = 40;
    public const int ELLIPS = 41;
    public const int EQ = 42;
    public const int EscapeSequence = 43;
    public const int ExponentPart = 44;
    public const int GE = 45;
    public const int GT = 46;
    public const int HexDigit = 47;
    public const int ID = 48;
    public const int IDENT = 49;
    public const int IMPORT = 50;
    public const int LAMBDA_OPS = 51;
    public const int LBRACK = 52;
    public const int LCURLYB = 53;
    public const int LE = 54;
    public const int LROUNDB = 55;
    public const int LTT = 56;
    public const int MESSAGE = 57;
    public const int ML_COMMENT = 58;
    public const int MOD = 59;
    public const int MUL = 60;
    public const int MULTLIT = 61;
    public const int NE = 62;
    public const int NL = 63;
    public const int NODE = 64;
    public const int NOT = 65;
    public const int NUMLIT = 66;
    public const int OR = 67;
    public const int RAISES = 68;
    public const int RBRACK = 69;
    public const int RCURLYB = 70;
    public const int RROUNDB = 71;
    public const int SEMI = 72;
    public const int SL_COMMENT = 73;
    public const int STRING = 74;
    public const int STRING_FRAGMENT = 75;
    public const int SUB = 76;
    public const int StringChar = 77;
    public const int TEXT = 78;
    public const int TO = 79;
    public const int USING = 80;
    public const int UnicodeChar = 81;
    public const int VALUATION = 82;
    public const int WS = 83;
    private readonly BOParser parser;
    private readonly Partition partition;
    private string name;
    private BOCompilerHelper helper;
    private AbstractLanguageService.callingContext callingContext;
    private string namespaceName;
    private string boName;
    private XBOHeader chachedXboHeader;
    private readonly URLStore store;

    public XBOWalker(BOParser parser, CommonTree tree, AbstractLanguageService.callingContext callingContext)
      : this((ITreeNodeStream) new CommonTreeNodeStream((object) tree))
    {
      string filePath = (string) null;
      this.parser = parser;
      this.callingContext = callingContext;
      this.store = filePath == null ? new URLStore() : new URLStore(filePath);
      this.partition = new Partition((Store) this.store);
    }

    public void WalkAST()
    {
      foreach (AnnotatedElement annotatedElement in this.parser.AllFeatures.Where<AnnotatedElement>((Func<AnnotatedElement, bool>) (msg => msg.Scope == Scope.Message)))
        this.AddMessage(annotatedElement.LanguageElementName, (string) null, (string) null, (string) null, (string) null, (string) null, (IToken) null, (IToken) null);
      this.ruleModel();
    }

    private void AddBoName(string NSName, string BOName)
    {
      this.chachedXboHeader = ExtensionDataCache.GetInstance().updateXBOHeaderList(NSName, BOName, this.parser.ErrorHandler.FileName);
      this.namespaceName = NSName;
      this.boName = BOName;
    }

    public bool checkForParserErrors()
    {
      return this.parser.ErrorHandler.NumberOfErrors != 0;
    }

    public string GetNodeName(IToken nodeToken)
    {
      foreach (string boNodeName in ExtensionDataCache.GetInstance().GetBONodeNames(this.chachedXboHeader.getNamespace(), this.chachedXboHeader.getBoName()))
      {
        if (boNodeName == nodeToken.Text.ToString())
          return nodeToken.Text.ToString();
      }
      this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL1_XBODLMSG_NOT_AN_EXTNODE, (object) nodeToken.Text, (object) this.chachedXboHeader.getBoName()), nodeToken.Line - 1, nodeToken.CharPositionInLine, nodeToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
      return nodeToken.Text.ToString();
    }

    private static BOSourceLocation TokenAsLocation(IToken token)
    {
      if (token == null)
        return new BOSourceLocation(0, 0, 0);
      return new BOSourceLocation(token.Line, token.CharPositionInLine, token.Text == null ? 0 : token.Text.Length);
    }

    private SAP.Copernicus.Model.DataType.DataType GetTypeFor(IToken token, bool isAlias, string namespOrAliasName, string typeName)
    {
      string nsName = namespOrAliasName;
      if (isAlias)
        this.parser.ImportPairs.TryGetValue(namespOrAliasName, out nsName);
      return this.GetTypeFor(XBOWalker.TokenAsLocation(token), nsName, typeName, true);
    }

    public SAP.Copernicus.Model.DataType.DataType GetTypeFor(BOSourceLocation location, string nsName, string typeName, bool checkUsageForBO)
    {
      if (nsName != null)
      {
        if (!nsName.StartsWith("http://"))
        {
          try
          {
            nsName = ImportPathUtil.ConvertImportFromInternalToESR(nsName);
          }
          catch (Exception ex)
          {
            this.parser.ErrorHandler.AddError(ex, BOErrorLevel.MIN_LEVEL);
            return (SAP.Copernicus.Model.DataType.DataType) null;
          }
        }
      }
      string proxyName;
      string[] ambigousNamespaces;
      bool usgCatNotAllowed;
      string actualDTNspName;
      this.GetProxyNameForDataType(this.parser.ProjectNamespace, nsName, typeName, out proxyName, out ambigousNamespaces, out usgCatNotAllowed, out actualDTNspName);
      if (ambigousNamespaces != null)
      {
        string str1 = "";
        if (ambigousNamespaces != null)
        {
          foreach (string str2 in ambigousNamespaces)
            str1 = str1 + str2 + Environment.NewLine;
        }
        string str3 = str1;
        this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.BOCompiler.Common.Resources.LEVEL3_BODLMSG_AMBIGUOUS_DEF_P1_MOTYPE_P2_MONAME_P3_NSLIST, (object) SAP.Copernicus.BOCompiler.Common.Resources.BODLMSG_MOTYPE_DATATYPE, (object) typeName, (object) str3), location.Line - 1, location.Column, location.Length, BOErrorLevel.LEVEL2);
        return (SAP.Copernicus.Model.DataType.DataType) null;
      }
      if (proxyName == null)
      {
        this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.BOCompiler.Common.Resources.LEVEL3_BODLMSG_DEF_NOT_FOUND_P1_MOTYPE_P2_MONAME_P3_ERMSG, (object) SAP.Copernicus.BOCompiler.Common.Resources.BODLMSG_MOTYPE_DATATYPE, (object) typeName, nsName == null ? (object) SAP.Copernicus.BOCompiler.Common.Resources.BODLMSG_ADD_IMPORT_STMT_OR_FULLY_QUALIFY : (object) SAP.Copernicus.BOCompiler.Common.Resources.BODLMSG_CONNECTED_TO_DEV_SYS), location.Line - 1, location.Column, location.Length, BOErrorLevel.LEVEL2);
        return (SAP.Copernicus.Model.DataType.DataType) null;
      }
      if (usgCatNotAllowed && checkUsageForBO)
      {
        this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.BOCompiler.Common.Resources.LEVEL3_BODLMSG_DT_USAGE_CAT_INVALID, (object) typeName, (object) actualDTNspName), location.Line - 1, location.Column, location.Length, BOErrorLevel.LEVEL2);
        return (SAP.Copernicus.Model.DataType.DataType) null;
      }
      SerializationResult serializationResult = new SerializationResult();
      SAP.Copernicus.Model.DataType.DataType dataType;
      using (Transaction transaction = this.partition.Store.TransactionManager.BeginTransaction("Read DataType"))
      {
        dataType = MetamodelSerializationHelper.Instance.LoadModelFromUrl<SAP.Copernicus.Model.DataType.DataType>(serializationResult, this.partition, proxyName, (ISchemaResolver) null, (ValidationController) null);
        if (dataType == null || serializationResult.Failed)
        {
          this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.BOCompiler.Common.Resources.LEVEL3_BODLMSG_DEF_READ_UNABLE_P1_MOTYPE_P2_MONAME_P3_ERMSG, (object) SAP.Copernicus.BOCompiler.Common.Resources.BODLMSG_MOTYPE_DATATYPE, (object) typeName, (object) SAP.Copernicus.BOCompiler.Common.Resources.BODLMSG_CONNECTED_TO_DEV_SYS), location.Line - 1, location.Column, location.Length, BOErrorLevel.LEVEL2);
          return (SAP.Copernicus.Model.DataType.DataType) null;
        }
        transaction.Commit();
      }
      if (dataType.DictionaryEntryName.RepresentationTermCode != DataTypeRepresentationTermCodeEnum.Code || !dataType.IdentifyingContextIndicator)
        return dataType;
      this.parser.ErrorHandler.AddError(SAP.Copernicus.Extension.Resources.CodeWithIdentifyingContextIndicator, location.Line - 1, location.Column, location.Length, BOErrorLevel.LEVEL2);
      return (SAP.Copernicus.Model.DataType.DataType) null;
    }

    private void AddMessage(IToken messageToken, IToken messageTextToken, SAP.Copernicus.Model.DataType.DataType firstVariableType, SAP.Copernicus.Model.DataType.DataType secondVariableType, SAP.Copernicus.Model.DataType.DataType thirdVariableType, SAP.Copernicus.Model.DataType.DataType fourthVariableType)
    {
      this.AddMessage(messageToken.Text, messageTextToken.Text, firstVariableType == null ? (string) null : firstVariableType.Key.ProxyName, secondVariableType == null ? (string) null : secondVariableType.Key.ProxyName, thirdVariableType == null ? (string) null : thirdVariableType.Key.ProxyName, fourthVariableType == null ? (string) null : fourthVariableType.Key.ProxyName, messageToken, messageTextToken);
    }

    private void AddMessage(string messageID, string messageText, string first, string second, string third, string fourth, IToken messageToken, IToken messageTextToken)
    {
      SystemMessageDescriptor messageDescriptor = new SystemMessageDescriptor(messageID, messageText, first, second, third, fourth);
      PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE message = new PDI_UTIL_SMTG_GENERATE.PDI_UTIL_T_MESSAGE();
      int num1 = 0;
      ExtensionDataCache.GetInstance().getMessages();
      messageID.Trim();
      if (string.IsNullOrEmpty(messageID))
        throw new ArgumentNullException("messageID must not be null");
      if (messageID.Length > 30)
      {
        this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.BOCompiler.Common.Resources.LEVEL3_BODLMSG_INVALID_LENGTH_MSGID, (object) messageID), messageToken.Line - 1, messageToken.CharPositionInLine, messageToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
      }
      else
      {
        if (messageText != null)
        {
          message.TEXT = XBOWalker.StripQuotes(messageText);
          if (message.TEXT.Length > 73)
          {
            this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.BOCompiler.Common.Resources.LEVEL3_BODLMSG_INVALID_LENGTH_MSGTXT, (object) message.TEXT), messageTextToken.Line - 1, messageTextToken.CharPositionInLine, messageTextToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
            return;
          }
          num1 = Regex.Matches(message.TEXT, "&").Count;
        }
        int num2 = 0;
        if (first != null)
        {
          message.ATTRIBUTE1_TYPE = messageDescriptor.firstVariableType;
          ++num2;
        }
        if (second != null)
        {
          message.ATTRIBUTE2_TYPE = messageDescriptor.secondVariableType;
          ++num2;
        }
        if (third != null)
        {
          message.ATTRIBUTE3_TYPE = messageDescriptor.thirdVariableType;
          ++num2;
        }
        if (fourth != null)
        {
          message.ATTRIBUTE4_TYPE = messageDescriptor.fourthVariableType;
          ++num2;
        }
        if (num2 != num1)
        {
          this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.BOCompiler.Common.Resources.LEVEL3_BODLMSG_INVALID_NO_OF_PARAMETERS, (object) messageID.ToString(), (object) num1.ToString(), (object) num2.ToString()), messageToken.Line - 1, messageToken.CharPositionInLine, messageToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
        }
        else
        {
          message.ID = messageDescriptor.id.Trim();
          message.TEXT = XBOWalker.StripQuotes(messageDescriptor.text);
          ExtensionDataCache.GetInstance().setMessage(message);
        }
      }
    }

    private void AddRaisedMessages(string nodeName, IToken messageTypeGroup, IToken systemMessageType)
    {
      foreach (PDI_EXT_S_EXT_NODE pdiExtSExtNode in ExtensionDataCache.GetInstance().GetBONodesData(this.chachedXboHeader.getNamespace(), this.chachedXboHeader.getBoName()))
      {
        if (nodeName == pdiExtSExtNode.ND_NAME)
          nodeName = pdiExtSExtNode.PRX_ND_NAME;
      }
      if (ExtensionDataCache.GetInstance().addMessageAssignment(nodeName, systemMessageType.Text))
        return;
      this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.MessageIDNotValid, (object) systemMessageType.Text), systemMessageType.Line - 1, systemMessageType.CharPositionInLine, systemMessageType.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
    }

    private void GetProxyNameForDataType(string boNsName, string dtNsName, string dtName, out string proxyName, out string[] ambigousNamespaces, out bool usgCatNotAllowed, out string actualDTNspName)
    {
      usgCatNotAllowed = false;
      actualDTNspName = (string) null;
      proxyName = (string) null;
      ambigousNamespaces = (string[]) null;
      RepositoryDataCache instance = RepositoryDataCache.GetInstance();
      if (dtNsName != null)
      {
        proxyName = instance.GetProxyNameForQualifiedDTName(dtNsName, dtName);
        ambigousNamespaces = (string[]) null;
        foreach (RepositoryDataSet.DataTypesRow queryDataType in instance.QueryDataTypes((string) null, proxyName, (string) null, (string) null, (string) null, (string) null))
        {
          if (queryDataType.UsageCategory != null && BOResolver.ForbiddenDTUsageCategories.Contains<string>(queryDataType.UsageCategory))
          {
            usgCatNotAllowed = true;
            actualDTNspName = dtNsName;
          }
        }
      }
      else
      {
        if (dtName == "ID")
        {
          RepositoryDataSet.DataTypesRow[] matchingTypesForDtName = instance.GetMatchingTypesForDTName(dtName);
          if (((IEnumerable<RepositoryDataSet.DataTypesRow>) matchingTypesForDtName).Count<RepositoryDataSet.DataTypesRow>() > 1)
          {
            foreach (RepositoryDataSet.DataTypesRow dataTypesRow in matchingTypesForDtName)
            {
              if (dataTypesRow.NSName == this.parser.ProjectNamespace)
              {
                proxyName = dataTypesRow.ProxyName;
                ambigousNamespaces = (string[]) null;
                return;
              }
            }
            if (proxyName == null)
            {
              dtNsName = "http://sap.com/xi/AP/PDI/ABSL";
              proxyName = "PDI_ABSL_IDENTIFIER";
              ambigousNamespaces = (string[]) null;
              return;
            }
          }
          else
          {
            dtNsName = "http://sap.com/xi/AP/PDI/ABSL";
            proxyName = "PDI_ABSL_IDENTIFIER";
            ambigousNamespaces = (string[]) null;
            return;
          }
        }
        if (string.IsNullOrEmpty(boNsName))
          throw new ArgumentException("boNsName must not be null or empty");
        if (string.IsNullOrEmpty(dtName))
          throw new ArgumentException("dtName must not be null or empty");
        RepositoryDataSet.DataTypesRow[] matchingTypesForDtName1 = instance.GetMatchingTypesForDTName(dtName);
        List<RepositoryDataSet.DataTypesRow> dataTypesRowList = new List<RepositoryDataSet.DataTypesRow>();
        IList<string> imports = this.parser.Imports;
        foreach (RepositoryDataSet.DataTypesRow dataTypesRow in matchingTypesForDtName1)
        {
          if (dataTypesRow.NSName == boNsName || imports.Contains(dataTypesRow.NSName))
            dataTypesRowList.Add(dataTypesRow);
          if (dataTypesRow.UsageCategory != null && BOResolver.ForbiddenDTUsageCategories.Contains<string>(dataTypesRow.UsageCategory))
          {
            usgCatNotAllowed = true;
            actualDTNspName = dataTypesRow.NSName;
          }
        }
        if (dataTypesRowList.Count == 0)
        {
          proxyName = (string) null;
          ambigousNamespaces = (string[]) null;
        }
        else if (dataTypesRowList.Count == 1)
        {
          proxyName = dataTypesRowList[0].ProxyName;
          ambigousNamespaces = (string[]) null;
        }
        else
        {
          proxyName = (string) null;
          ambigousNamespaces = new string[dataTypesRowList.Count];
          using (List<RepositoryDataSet.DataTypesRow>.Enumerator enumerator = dataTypesRowList.GetEnumerator())
          {
            if (!enumerator.MoveNext())
              return;
            RepositoryDataSet.DataTypesRow current = enumerator.Current;
            proxyName = current.ProxyName;
          }
        }
      }
    }

    private static string StripQuotes(string str)
    {
      if (str == null)
        return (string) null;
      int startIndex = 0;
      int length = str.Length;
      if (length > 0 && (int) str[0] == 34)
      {
        startIndex = 1;
        --length;
      }
      if (length > 0 && (int) str[startIndex + length - 1] == 34)
        --length;
      return str.Substring(startIndex, length);
    }

    private void AddElement(IToken elementToken, string nodeName, string typeName, CustomToken tree)
    {
      this.AddElement(elementToken, nodeName, typeName, tree, (List<string>) null);
    }

    private void AddElement(IToken elementToken, string nodeName, string typeName, CustomToken tree, List<string> elementDefaults)
    {
      if (tree == null)
        throw new ArgumentNullException(nameof (tree));
      string str1 = (string) null;
      string oldValue = "\"";
      string newValue = "";
      string length = (string) null;
      string decimals = (string) null;
      string defaultValue = string.Empty;
      string defaultCode = string.Empty;
      PDI_EXT_S_ID_TARGET_ATTRIBUTE relation = new PDI_EXT_S_ID_TARGET_ATTRIBUTE();
      XBOHeader xboHeader = ExtensionDataCache.GetInstance().getXBOHeader(this.parser.ErrorHandler.FileName);
      if ((long) elementToken.Text.Length > 60L)
        this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL1_XBODLMSG_ELEMENT_NAME_TOO_LONG, (object) elementToken.Text), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
      else if (nodeName.ToUpper().Equals("ROOT") && !ExtensionDataCache.GetInstance().IsRootNodeExtensible(xboHeader.getNamespace(), xboHeader.getBoName()))
        this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL1_XBODLMSG_ROOT_NODE_NOT_EXTENSIBLE, (object) xboHeader.getBoName()), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
      else if (!ExtensionDataCache.GetInstance().IsNodeExtensible(xboHeader.getNamespace(), xboHeader.getBoName(), nodeName))
      {
        this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL1_XBODLMSG_NODE_NOT_EXTENSIBLE, (object) nodeName, (object) xboHeader.getBoName()), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
      }
      else
      {
        bool flag1 = false;
        bool flag2 = tree.Annotations.FirstOrDefault<Annotation>((Func<Annotation, bool>) (anno => anno.Name == "Reference")) != null;
        if (flag2)
          flag1 = true;
        List<string> scenarios = new List<string>();
        Annotation annotation1 = tree.Annotations.FirstOrDefault<Annotation>((Func<Annotation, bool>) (anno => anno.Name == "Scenario"));
        bool flag3 = annotation1 != null;
        if (annotation1 != null && annotation1.Values != null && annotation1.Values.Count > 0)
        {
          for (int index = 0; index < annotation1.Values.Count; ++index)
            scenarios.Add(annotation1.Values[index].Value);
          flag1 = true;
        }
        bool transient = tree.Annotations.FirstOrDefault<Annotation>((Func<Annotation, bool>) (anno => anno.Name == "Transient")) != null;
        if (transient)
          flag1 = true;
        bool inStandard;
        string usedBoNamespace;
        string usedBoName;
        string usedNodeName;
        string usedFieldType;
        bool usedIsReference;
        string boNameRef;
        string nodeNameRef;
        bool defExists;
        string boNameDef;
        string nodeNameDef;
        bool kut;
        bool flag4 = ExtensionDataCache.GetInstance().checkFieldExists(xboHeader.getNamespace(), xboHeader.getBoName(), nodeName, elementToken.Text, out inStandard, out usedBoNamespace, out usedBoName, out usedNodeName, out usedFieldType, out usedIsReference, out boNameRef, out nodeNameRef, out defExists, out boNameDef, out nodeNameDef, out kut);
        Trace.WriteLine(string.Format("Add element: Field {0} in node {1} of BO {2} in use: {3}.", (object) elementToken.Text, (object) nodeName, (object) xboHeader.getBoName(), (object) flag4), nameof (XBOWalker));
        if (kut)
        {
          this.parser.ErrorHandler.AddWarning(string.Format(SAP.Copernicus.Extension.Resources.ExtFieldManagedByKUT, (object) elementToken.Text), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
        }
        else
        {
          if (flag4 && !flag2 && !flag3)
          {
            if (inStandard)
              this.parser.ErrorHandler.AddWarning(string.Format(SAP.Copernicus.Extension.Resources.LEVEL1_XBODLMSG_FIELD_NAME_EXISTS_IN_STANDARD_BO, (object) elementToken.Text, (object) usedBoName, (object) usedNodeName), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
            else if (!defExists || !xboHeader.getBoName().Equals(boNameDef))
            {
              Trace.WriteLine(string.Format("Add element: Def exists for field {0} in node {1} of BO {2}. We are in BO {3}.", (object) elementToken.Text, (object) nodeNameDef, (object) boNameDef, (object) xboHeader.getBoName()), nameof (XBOWalker));
              this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL1_XBODLMSG_EXTENSION_FIELD_NOT_UNIQUE, (object) elementToken.Text, (object) boNameDef, (object) nodeNameDef), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
              return;
            }
          }
          if (flag2)
          {
            if (typeName != null)
            {
              this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL_XBODLMSG_UNSUPPORTED_ANNOTATION_COMBINATION, (object) elementToken.Text.ToString(), (object) "Data Type not allowed for a referenced element"), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
              return;
            }
            typeName = string.Empty;
          }
          else
          {
            string namespaceForFile = ProjectUtil.GetProjectNamespaceForFile(this.parser.ErrorHandler.FileName);
            List<string> nameSpaces = new List<string>((IEnumerable<string>) this.parser.Imports);
            if (!nameSpaces.Contains(namespaceForFile))
              nameSpaces.Add(namespaceForFile);
            if (!XBOLanguageService.GetInstance().checkDataTypeIsValid(nameSpaces, typeName))
            {
              this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL1_XBODLMSG_INVALID_FIELD_TYPE_FOR_EXT, (object) typeName), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
              return;
            }
          }
          string label = (string) null;
          Annotation annotation2 = tree.Annotations.FirstOrDefault<Annotation>((Func<Annotation, bool>) (anno => anno.Name == "Label"));
          if (annotation2 != null && annotation2.Values != null && (annotation2.Values.Count == 1 && annotation2.Values[0] != null))
          {
            string text = annotation2.Values[0].Value;
            if (text == null || text == string.Empty)
              text = elementToken.Text;
            label = text.Replace(oldValue, newValue);
            flag1 = true;
          }
          if (flag2 && label != null)
          {
            this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL_XBODLMSG_UNSUPPORTED_ANNOTATION_COMBINATION, (object) elementToken.Text.ToString(), (object) "Label not allowed for a referenced element"), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
          }
          else
          {
            string tooltip = (string) null;
            Annotation annotation3 = tree.Annotations.FirstOrDefault<Annotation>((Func<Annotation, bool>) (anno => anno.Name == "Tooltip"));
            if (annotation3 != null && annotation3.Values != null && (annotation3.Values.Count == 1 && annotation3.Values[0] != null))
            {
              string str2 = annotation3.Values[0].Value;
              if (str2 == null || str2 == string.Empty)
                str2 = label;
              tooltip = str2.Replace(oldValue, newValue);
              flag1 = true;
            }
            if (flag2 && tooltip != null)
            {
              this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL_XBODLMSG_UNSUPPORTED_ANNOTATION_COMBINATION, (object) elementToken.Text.ToString(), (object) "Tooltip not allowed for a referenced element"), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
            }
            else
            {
              Annotation decimalAnnotation = tree.Annotations.FirstOrDefault<Annotation>((Func<Annotation, bool>) (anno => anno.Name == "Decimal"));
              bool flag5 = decimalAnnotation != null;
              if (decimalAnnotation != null && decimalAnnotation.Values != null && decimalAnnotation.Values.Count > 0)
              {
                if (typeName != "DecimalValue")
                {
                  this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL_XBODLMSG_UNSUPPORTED_ANNOTATION_COMBINATION, (object) elementToken.Text.ToString(), (object) "Element type and element annotation are inconsistent"), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
                  return;
                }
                if (decimalAnnotation.Values.Count == 2)
                {
                  length = decimalAnnotation.Values[0].Value;
                  decimals = decimalAnnotation.Values[1].Value;
                }
                flag1 = true;
              }
              string relationPath = (string) null;
              Annotation annotation4 = tree.Annotations.FirstOrDefault<Annotation>((Func<Annotation, bool>) (anno => anno.Name == "Relation"));
              bool flag6 = annotation4 != null;
              if (annotation4 != null && annotation4.Values != null && annotation4.Values.Count > 0)
              {
                SAP.Copernicus.Model.DataType.DataType dt = (SAP.Copernicus.Model.DataType.DataType) null;
                try
                {
                  this.parser.BOResolver.FindDTElements(typeName, out dt);
                  if (dt.DictionaryEntryName.RepresentationTermCode != DataTypeRepresentationTermCodeEnum.Identifier)
                  {
                    this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL_XBODLMSG_UNSUPPORTED_ANNOTATION_COMBINATION, (object) elementToken.Text.ToString(), (object) "Element type and element annotation are inconsistent"), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
                    return;
                  }
                  relationPath = annotation4.Values[0].Value;
                  relationPath = relationPath.Replace(oldValue, newValue);
                  flag1 = true;
                }
                finally
                {
                  if (dt != null)
                    dt.Store.Dispose();
                }
              }
              if (transient && (flag2 || flag3 || flag6))
                this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL_XBODLMSG_UNSUPPORTED_ANNOTATION_COMBINATION, (object) elementToken.Text.ToString(), (object) "Transient elements are not allowed with a Scenario, Relation or a Reference Annotation"), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
              else if (flag6 && (flag5 || flag2))
                this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL_XBODLMSG_UNSUPPORTED_ANNOTATION_COMBINATION, (object) elementToken.Text.ToString(), (object) "Relation Annotaion not allowed with a Decimal or a Reference Annotation"), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
              else if (elementDefaults != null && elementDefaults.Count != 0 && flag2)
              {
                this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL_XBODLMSG_UNSUPPORTED_ANNOTATION_COMBINATION, (object) elementToken.Text.ToString(), (object) "Default Values not allowed for a Reference Annotation"), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
              }
              else
              {
                if (elementDefaults != null && elementDefaults.Count != 0 && (this.callingContext.Equals((object) AbstractLanguageService.callingContext.Activate) || this.callingContext.Equals((object) AbstractLanguageService.callingContext.Save) || this.callingContext.Equals((object) AbstractLanguageService.callingContext.Check)))
                {
                  string messageText = (string) null;
                  if (!this.checkDefaults(elementToken.Text, typeName, elementDefaults, decimalAnnotation, out messageText, out defaultValue, out defaultCode))
                  {
                    this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.ElementDefaultValueError, (object) elementToken.Text.ToString(), (object) messageText), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
                    return;
                  }
                }
                if (flag1)
                {
                  if (scenarios.Count == 0)
                  {
                    if (flag2)
                    {
                      string error;
                      if (ExtScenDataCache.GetInstance().addReference(xboHeader.getNamespace(), xboHeader.getBoName(), elementToken.Text, nodeName, string.Empty, out error))
                        return;
                      string str2;
                      switch (error)
                      {
                        case "field":
                          str2 = string.Format(SAP.Copernicus.Extension.Resources.ReferencedFieldNotExist, (object) elementToken.Text);
                          break;
                        case "standardfield":
                          str2 = string.Format(SAP.Copernicus.Extension.Resources.RefStandardBOfield, (object) elementToken.Text);
                          break;
                        default:
                          str2 = "unspecified";
                          break;
                      }
                      this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.ReferenceFieldDeclarationError, (object) elementToken.Text, (object) str2), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
                    }
                    else
                    {
                      string BOName = (string) null;
                      string BONode = (string) null;
                      if (flag6)
                      {
                        string errorMessage;
                        relation = this.GetRelationStruct(relationPath, out errorMessage);
                        if (errorMessage != null)
                        {
                          this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.RelationTargetError, (object) elementToken.Text, (object) errorMessage), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
                          return;
                        }
                      }
                      if (ExtensionDataCache.GetInstance().addField(xboHeader.getNamespace(), xboHeader.getBoName(), elementToken.Text, typeName, nodeName, label, tooltip, length, decimals, transient, defaultValue, defaultCode, relation, out BOName, out BONode, out str1) || nodeName.Equals(BONode) && xboHeader.getBoName().Equals(BOName))
                        return;
                      Trace.WriteLine(string.Format("Add element (branch for annotation): AddField failed: FIELD_NOT_UNIQUE: Field {0} in node {1} of BO {2}.", (object) elementToken.Text, (object) nodeName, (object) xboHeader.getBoName(), (object) xboHeader.getBoName()), nameof (XBOWalker));
                      this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.LEVEL1_XBODLMSG_EXTENSION_FIELD_NOT_UNIQUE, (object) elementToken.Text, (object) BOName, (object) BONode), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
                    }
                  }
                  else if (flag2)
                  {
                    string error;
                    string errorScenario;
                    if (ExtScenDataCache.GetInstance().addReference(xboHeader.getNamespace(), xboHeader.getBoName(), elementToken.Text, nodeName, scenarios, string.Empty, out error, out errorScenario))
                      return;
                    string str2;
                    switch (error)
                    {
                      case "scenario":
                        str2 = string.Format(SAP.Copernicus.Extension.Resources.ScenarioNotExist, (object) errorScenario, (object) elementToken.Text);
                        break;
                      case "field":
                        str2 = string.Format(SAP.Copernicus.Extension.Resources.ReferencedFieldNotExist, (object) elementToken.Text);
                        break;
                      case "standardfield":
                        str2 = string.Format(SAP.Copernicus.Extension.Resources.RefStandardBOfield, (object) elementToken.Text);
                        break;
                      default:
                        str2 = "unspecified";
                        break;
                    }
                    this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.ReferenceFieldDeclarationError, (object) elementToken.Text, (object) str2), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
                  }
                  else
                  {
                    string str2;
                    if (flag6)
                    {
                      relation = this.GetRelationStruct(relationPath, out str2);
                      if (str2 != null)
                      {
                        this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.RelationTargetError, (object) elementToken.Text, (object) str2), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
                        return;
                      }
                    }
                    string errorScenario;
                    if (ExtScenDataCache.GetInstance().addField(xboHeader.getNamespace(), xboHeader.getBoName(), elementToken.Text, label, tooltip, typeName, nodeName, scenarios, string.Empty, length, decimals, defaultValue, defaultCode, relation, out str2, out errorScenario, out usedBoNamespace, out usedBoName, out usedNodeName, out str1))
                      return;
                    string str3;
                    switch (str2)
                    {
                      case "node":
                        str3 = string.Format(SAP.Copernicus.Extension.Resources.ScenarioWrongNode, (object) errorScenario, (object) nodeName, (object) elementToken.Text);
                        break;
                      case "scenario":
                        str3 = string.Format(SAP.Copernicus.Extension.Resources.ScenarioNotExist, (object) errorScenario, (object) elementToken.Text);
                        break;
                      case "field":
                        str3 = string.Format(SAP.Copernicus.Extension.Resources.ExtFieldNotMatchScenario, (object) elementToken.Text, (object) errorScenario);
                        break;
                      case "fieldexists":
                        str3 = string.Format(SAP.Copernicus.Extension.Resources.ExtFieldActiveInOtherBO, (object) elementToken.Text, (object) errorScenario);
                        break;
                      default:
                        str3 = "unspecified";
                        break;
                    }
                    this.parser.ErrorHandler.AddError(string.Format(SAP.Copernicus.Extension.Resources.ScenarioFieldDeclarationError, (object) elementToken.Text, (object) errorScenario, (object) str3), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
                  }
                }
                else
                {
                  string errboName = (string) null;
                  string errboNode = (string) null;
                  if (ExtensionDataCache.GetInstance().addField(xboHeader.getNamespace(), xboHeader.getBoName(), elementToken.Text, typeName, nodeName, length, decimals, defaultValue, defaultCode, relation, out errboName, out errboNode, out str1))
                    return;
                  this.parser.ErrorHandler.AddError(!(str1 != string.Empty) ? string.Format(SAP.Copernicus.Extension.Resources.LEVEL1_XBODLMSG_EXTENSION_FIELD_NOT_UNIQUE, (object) elementToken.Text, (object) errboName, (object) errboNode) : string.Format(SAP.Copernicus.Extension.Resources.LEVEL1_XBODLMSG_EXTENSION_DEFAULT_VALUE, (object) str1, (object) elementToken.Text), elementToken.Line - 1, elementToken.CharPositionInLine, elementToken.Text.ToString().Length, BOErrorLevel.MIN_LEVEL);
                  Trace.WriteLine(string.Format("Add element (branch for no annotation at all): AddField failed: FIELD_NOT_UNIQUE: Field {0} in node {1} of BO {2}.", (object) elementToken.Text, (object) nodeName, (object) xboHeader.getBoName(), (object) xboHeader.getBoName(), (object) nameof (XBOWalker)));
                }
              }
            }
          }
        }
      }
    }

    private bool checkDefaults(string elementName, string dataTypeName, List<string> boDefaultsList, Annotation decimalAnnotation, out string messageText, out string defaultValue, out string defaultCode)
    {
      messageText = (string) null;
      defaultValue = string.Empty;
      defaultCode = string.Empty;
      string str1 = elementName + "/content";
      SAP.Copernicus.Model.DataType.DataType type = (SAP.Copernicus.Model.DataType.DataType) null;
      List<BOComponentAssignment> componentAssignmentList = BOCompilerHelper.mapBODefaults(boDefaultsList, elementName);
      if (componentAssignmentList.Count > 2)
      {
        messageText = string.Format(SAP.Copernicus.BOCompiler.Common.Resources.DefaultCheckingNotSupportedByExt, (object) elementName);
        return false;
      }
      foreach (BOComponentAssignment componentAssignment in componentAssignmentList)
      {
        if (componentAssignment.Name == str1 || componentAssignment.Name == elementName)
          defaultValue = componentAssignment.Value;
      }
      string ddicName = string.Empty;
      if (dataTypeName == "Text")
      {
        ddicName = "APC_V_LANGUAGEINDEPENDENT_TEXT";
      }
      else
      {
        if (dataTypeName == "DecimalValue")
        {
          if (componentAssignmentList.Count != 1)
          {
            messageText = string.Format(SAP.Copernicus.BOCompiler.Common.Resources.DefaultCheckingDataTypeFlat, (object) elementName);
            return false;
          }
          if (componentAssignmentList[0].Name != elementName)
          {
            messageText = string.Format(SAP.Copernicus.BOCompiler.Common.Resources.DefaultCheckingDataTypeFlat, (object) elementName);
            return false;
          }
          if (componentAssignmentList[0].Type != "DECLIT" && componentAssignmentList[0].Type != "NUMLIT")
          {
            messageText = string.Format(SAP.Copernicus.BOCompiler.Common.Resources.DefaultValueCheckingWrongType, (object) componentAssignmentList[0].Type, (object) componentAssignmentList[0].Value);
            return false;
          }
          int num1 = 7;
          int num2 = 2;
          if (decimalAnnotation != null && decimalAnnotation.Values.Count == 2)
          {
            num1 = (int) short.Parse(decimalAnnotation.Values[0].Value);
            num2 = (int) short.Parse(decimalAnnotation.Values[1].Value);
          }
          if (componentAssignmentList[0].Value.Contains("."))
          {
            int num3 = int.Parse(componentAssignmentList[0].Value.Split('.')[0]);
            int num4 = int.Parse(componentAssignmentList[0].Value.Split('.')[1]);
            if (num3.ToString().Length > num1 || num4.ToString().Length > num2)
            {
              messageText = string.Format(SAP.Copernicus.BOCompiler.Common.Resources.DefaultCheckingDecOutOfRange, (object) elementName);
              return false;
            }
          }
          else if (componentAssignmentList[0].Value.Length > num1)
          {
            messageText = string.Format(SAP.Copernicus.BOCompiler.Common.Resources.DefaultCheckingDecOutOfRange, (object) elementName);
            return false;
          }
          defaultValue = componentAssignmentList[0].Value;
          return true;
        }
        foreach (ExtensionFieldType extFieldType in ExtensionDataCache.GetInstance().ExtFieldTypes)
        {
          if (extFieldType.Name == dataTypeName)
          {
            ddicName = extFieldType.ProxyName;
            break;
          }
        }
      }
      try
      {
        if (ddicName == string.Empty)
          this.parser.BOResolver.FindDataTypeElements(this.namespaceName, dataTypeName, true, out type);
        else
          this.parser.BOResolver.GetDataTypeInfo(ddicName, out type);
        if (type == null)
        {
          messageText = string.Format(SAP.Copernicus.BOCompiler.Common.Resources.DefaultCheckingInternalError, (object) elementName);
          return false;
        }
        if (!BOCompilerHelper.CheckDefaultValues(elementName, type, boDefaultsList, out messageText))
          return false;
        foreach (BOComponentAssignment componentAssignment in componentAssignmentList)
        {
          if (componentAssignment.Name == str1 || componentAssignment.Name == elementName)
            defaultValue = componentAssignment.Value;
          else if (type.DictionaryEntryName.RepresentationTermCode == DataTypeRepresentationTermCodeEnum.Amount || type.DictionaryEntryName.RepresentationTermCode == DataTypeRepresentationTermCodeEnum.Quantity)
          {
            defaultCode = (componentAssignment.Value.Contains('"') ? componentAssignment.Value.Split(new char[] { '"' })[1] : componentAssignment.Value);
            return true;
          }
          else
          {
            messageText = string.Format(SAP.Copernicus.BOCompiler.Common.Resources.DefaultCheckingNotSupportedByExt, (object) elementName);
            return false;
          }
        }
        string str3;
        if (!defaultValue.Contains<char>('"'))
          str3 = defaultValue;
        else
          str3 = defaultValue.Split('"')[1];
        string str4 = str3;
        defaultValue = str4;
        return true;
      }
      finally
      {
        if (type != null)
          type.Store.Dispose();
      }
    }

    private void addToBODefaultsList(string defaultItem, string defaultType)
    {
      this.boDefaultsList.Add(defaultItem);
      this.boDefaultsList.Add(defaultType);
    }

    private List<string> GetBODefaultsList()
    {
      List<string> boDefaultsList = this.boDefaultsList;
      this.boDefaultsList.Clear();
      return boDefaultsList;
    }

    private PDI_EXT_S_ID_TARGET_ATTRIBUTE GetRelationStruct(string relationPath, out string errorMessage)
    {
      errorMessage = (string) null;
      PDI_EXT_S_ID_TARGET_ATTRIBUTE relStruct = new PDI_EXT_S_ID_TARGET_ATTRIBUTE();
      if (relationPath != null && !(relationPath == string.Empty))
      {
        string[] strArray1 = relationPath.Split(':');
        string[] strArray2;
        if (((IEnumerable<string>) strArray1).Count<string>() <= 1)
          strArray2 = strArray1[0].Split('.');
        else
          strArray2 = strArray1[1].Split('.');
        List<string> list = ((IEnumerable<string>) strArray2).ToList<string>();
        relStruct.BO_NAMESPACE = ((IEnumerable<string>) strArray1).Count<string>() > 1 ? "http://sap.com/xi/" + strArray1[0].Replace(".", "/") : string.Empty;
        relStruct.ESR_BO_NAME = list[0];
        if (list[1].ToUpper() == "ROOT")
          list.RemoveAt(1);
        if (relStruct.BO_NAMESPACE == null || relStruct.BO_NAMESPACE == string.Empty)
          relStruct.BO_NAMESPACE = this.parser.BOResolver.ResolveBusinessObjectNamespace(relStruct.ESR_BO_NAME);
        relStruct = this.findAttributePathAndNodeName(list, relStruct, out errorMessage);
      }
      else
        errorMessage = SAP.Copernicus.Extension.Resources.NoRelationParameter;
      return relStruct;
    }

    private PDI_EXT_S_ID_TARGET_ATTRIBUTE findAttributePathAndNodeName(List<string> boTarget, PDI_EXT_S_ID_TARGET_ATTRIBUTE relStruct, out string errorMessage)
    {
      string str1 = boTarget[0];
      string boNamespace = relStruct.BO_NAMESPACE;
      string lastTargetNodeName = (string) null;
      string str2 = (string) null;
      int index1 = 0;
      List<string> boCompositions;
      do
      {
        boCompositions = this.parser.BOResolver.FindBOCompositions(boNamespace, str1, lastTargetNodeName, out errorMessage);
        if (boCompositions.Count<string>() != 0 && errorMessage == null)
        {
          ++index1;
          if (index1 >= boTarget.Count)
          {
            errorMessage = SAP.Copernicus.Extension.Resources.RelationPathIncomplete;
            break;
          }
          if (!boCompositions.Contains(boTarget[index1]))
          {
            --index1;
            break;
          }
          lastTargetNodeName = boTarget[index1];
        }
        else
          break;
      }
      while (boCompositions.Count<string>() != 0);
      int num = index1 + 1;
      if (boTarget.Count<string>() > index1 && num >= 1)
      {
        List<BODeclaration> boDeclarationList = new List<BODeclaration>();
        bool flag = false;
        do
        {
          boDeclarationList = this.parser.BOResolver.getBusinessObjectNodeElements(boNamespace, str1, lastTargetNodeName == null ? "Root" : lastTargetNodeName, lastTargetNodeName == null ? "Root" : boTarget[index1], DataTypeRepresentationTermCodeEnum.Identifier, false, boDeclarationList);
          if (boDeclarationList != null && boDeclarationList.Count<BODeclaration>() > 0)
          {
            foreach (BODeclaration boDeclaration in boDeclarationList)
            {
              if (boDeclaration.Name == boTarget[index1])
              {
                flag = true;
                break;
              }
            }
          }
          if (!flag && index1 == boTarget.Count<string>() - 1)
          {
            errorMessage = SAP.Copernicus.Extension.Resources.RelationPathIncomplete;
            break;
          }
          ++index1;
        }
        while (index1 <= boTarget.Count<string>() - 1);
      }
      else if (index1 != 0)
        errorMessage = SAP.Copernicus.Extension.Resources.RelationPathIncomplete;
      if (errorMessage == null)
      {
        relStruct.ESR_NODE_NAME = lastTargetNodeName == null ? "Root" : lastTargetNodeName;
        if (index1 == boTarget.Count)
        {
          for (int index2 = num; index2 < boTarget.Count; ++index2)
            str2 = str2 + boTarget[index2] + "/";
          if (str2.EndsWith("/"))
            str2 = str2.TrimEnd('/');
          relStruct.ESR_ATTRIBUTE_PATH = str2;
        }
        else
          errorMessage = SAP.Copernicus.Extension.Resources.RelationAttributePathMissing;
      }
      return relStruct;
    }

    public XBOWalker(ITreeNodeStream input)
      : this(input, new RecognizerSharedState())
    {
    }

    public XBOWalker(ITreeNodeStream input, RecognizerSharedState state)
      : base(input, state)
    {
    }

    public override string[] TokenNames
    {
      get
      {
        return XBOWalker.tokenNames;
      }
    }

    public override string GrammarFileName
    {
      get
      {
        return "C:\\p4sandbox\\client\\LeanStack\\BYDSTUDIO\\lsfrontend_stream\\src\\_copernicus\\sln\\CopernicusExtension\\Parser\\XBOWalker.g";
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void OnCreated()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule(string ruleName, int ruleIndex)
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule(string ruleName, int ruleIndex)
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ruleModel()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ruleModel()
    {
    }

    [GrammarRule("ruleModel")]
    public void ruleModel()
    {
      try
      {
        this.PushFollow(XBOWalker.Follow._ruleBusinessObject_in_ruleModel50);
        this.ruleBusinessObject();
        this.PopFollow();
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ruleBusinessObject()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ruleBusinessObject()
    {
    }

    [GrammarRule("ruleBusinessObject")]
    private void ruleBusinessObject()
    {
      string nodeName = "Root";
      List<string> stringList = new List<string>();
      try
      {
        this.Match((IIntStream) this.input, 15, XBOWalker.Follow._AST_BO_in_ruleBusinessObject64);
        this.Match((IIntStream) this.input, 2, (BitSet) null);
        this.AddBoName(((BaseTree) this.Match((IIntStream) this.input, 24, XBOWalker.Follow._AST_NAMESPACE_in_ruleBusinessObject66)).Text, ((BaseTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_ruleBusinessObject68)).Text);
        while (true)
        {
          int num = 2;
          if (this.input.LA(1) == 28)
            num = 1;
          if (num == 1)
          {
            this.PushFollow(XBOWalker.Follow._ruleNodeRaisesMessage_in_ruleBusinessObject72);
            this.ruleNodeRaisesMessage(nodeName);
            this.PopFollow();
          }
          else
            break;
        }
        while (true)
        {
          int num = 2;
          switch (this.input.LA(1))
          {
            case 11:
            case 19:
            case 22:
            case 25:
              num = 1;
              break;
          }
          if (num == 1)
          {
            this.PushFollow(XBOWalker.Follow._ruleBoNodeFeature_in_ruleBusinessObject76);
            string str;
            this.ruleBoNodeFeature(str = "Root");
            this.PopFollow();
          }
          else
            break;
        }
        this.Match((IIntStream) this.input, 3, (BitSet) null);
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ruleMessage()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ruleMessage()
    {
    }

    [GrammarRule("ruleMessage")]
    private void ruleMessage()
    {
      XBOWalker.ruleDataTypeSymbolicRef_return symbolicRefReturn1 = (XBOWalker.ruleDataTypeSymbolicRef_return) null;
      XBOWalker.ruleDataTypeSymbolicRef_return symbolicRefReturn2 = (XBOWalker.ruleDataTypeSymbolicRef_return) null;
      XBOWalker.ruleDataTypeSymbolicRef_return symbolicRefReturn3 = (XBOWalker.ruleDataTypeSymbolicRef_return) null;
      XBOWalker.ruleDataTypeSymbolicRef_return symbolicRefReturn4 = (XBOWalker.ruleDataTypeSymbolicRef_return) null;
      try
      {
        this.Match((IIntStream) this.input, 22, XBOWalker.Follow._AST_MESSAGE_in_ruleMessage91);
        this.Match((IIntStream) this.input, 2, (BitSet) null);
        CommonTree commonTree1 = (CommonTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_ruleMessage93);
        CommonTree commonTree2 = (CommonTree) this.Match((IIntStream) this.input, 74, XBOWalker.Follow._STRING_in_ruleMessage95);
        int num1 = 2;
        if (this.input.LA(1) == 34)
          num1 = 1;
        if (num1 == 1)
        {
          this.Match((IIntStream) this.input, 34, XBOWalker.Follow._COLON_in_ruleMessage98);
          this.PushFollow(XBOWalker.Follow._ruleDataTypeSymbolicRef_in_ruleMessage102);
          symbolicRefReturn1 = this.ruleDataTypeSymbolicRef();
          this.PopFollow();
          int num2 = 2;
          if (this.input.LA(1) == 29)
            num2 = 1;
          if (num2 == 1)
          {
            this.PushFollow(XBOWalker.Follow._ruleDataTypeSymbolicRef_in_ruleMessage108);
            symbolicRefReturn2 = this.ruleDataTypeSymbolicRef();
            this.PopFollow();
            int num3 = 2;
            if (this.input.LA(1) == 29)
              num3 = 1;
            if (num3 == 1)
            {
              this.PushFollow(XBOWalker.Follow._ruleDataTypeSymbolicRef_in_ruleMessage113);
              symbolicRefReturn3 = this.ruleDataTypeSymbolicRef();
              this.PopFollow();
              int num4 = 2;
              if (this.input.LA(1) == 29)
                num4 = 1;
              if (num4 == 1)
              {
                this.PushFollow(XBOWalker.Follow._ruleDataTypeSymbolicRef_in_ruleMessage117);
                symbolicRefReturn4 = this.ruleDataTypeSymbolicRef();
                this.PopFollow();
              }
            }
          }
        }
        this.Match((IIntStream) this.input, 3, (BitSet) null);
        this.AddMessage(commonTree1.Token, commonTree2.Token, symbolicRefReturn1 != null ? symbolicRefReturn1.Type : (SAP.Copernicus.Model.DataType.DataType) null, symbolicRefReturn2 != null ? symbolicRefReturn2.Type : (SAP.Copernicus.Model.DataType.DataType) null, symbolicRefReturn3 != null ? symbolicRefReturn3.Type : (SAP.Copernicus.Model.DataType.DataType) null, symbolicRefReturn4 != null ? symbolicRefReturn4.Type : (SAP.Copernicus.Model.DataType.DataType) null);
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ruleBoNode()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ruleBoNode()
    {
    }

    [GrammarRule("ruleBoNode")]
    private void ruleBoNode()
    {
      try
      {
        this.Match((IIntStream) this.input, 25, XBOWalker.Follow._AST_NODE_in_ruleBoNode148);
        this.Match((IIntStream) this.input, 2, (BitSet) null);
        string nodeName = this.GetNodeName(((CommonTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_ruleBoNode150)).Token);
        int num1 = 2;
        if (this.input.LA(1) == 23)
          num1 = 1;
        if (num1 == 1)
        {
          this.PushFollow(XBOWalker.Follow._ruleMultiplicity_in_ruleBoNode154);
          this.ruleMultiplicity();
          this.PopFollow();
        }
        while (true)
        {
          int num2 = 2;
          if (this.input.LA(1) == 28)
            num2 = 1;
          if (num2 == 1)
          {
            this.PushFollow(XBOWalker.Follow._ruleNodeRaisesMessage_in_ruleBoNode157);
            this.ruleNodeRaisesMessage(nodeName);
            this.PopFollow();
          }
          else
            break;
        }
        while (true)
        {
          int num2 = 2;
          switch (this.input.LA(1))
          {
            case 11:
            case 19:
            case 22:
            case 25:
              num2 = 1;
              break;
          }
          if (num2 == 1)
          {
            this.PushFollow(XBOWalker.Follow._ruleBoNodeFeature_in_ruleBoNode161);
            this.ruleBoNodeFeature(nodeName);
            this.PopFollow();
          }
          else
            break;
        }
        this.Match((IIntStream) this.input, 3, (BitSet) null);
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ruleNodeRaisesMessage()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ruleNodeRaisesMessage()
    {
    }

    [GrammarRule("ruleNodeRaisesMessage")]
    private void ruleNodeRaisesMessage(string nodeName)
    {
      CommonTree commonTree1 = (CommonTree) null;
      try
      {
        this.Match((IIntStream) this.input, 28, XBOWalker.Follow._AST_RAISES_in_ruleNodeRaisesMessage178);
        this.Match((IIntStream) this.input, 2, (BitSet) null);
        int num = 2;
        if (this.input.LA(1) == 48 && this.input.LA(2) == 48)
          num = 1;
        if (num == 1)
          commonTree1 = (CommonTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_ruleNodeRaisesMessage183);
        CommonTree commonTree2 = (CommonTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_ruleNodeRaisesMessage189);
        this.AddRaisedMessages(nodeName, commonTree1 == null ? (IToken) null : commonTree1.Token, commonTree2.Token);
        this.Match((IIntStream) this.input, 3, (BitSet) null);
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ruleActionRaisesMessage()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ruleActionRaisesMessage()
    {
    }

    [GrammarRule("ruleActionRaisesMessage")]
    private void ruleActionRaisesMessage()
    {
      try
      {
        this.Match((IIntStream) this.input, 28, XBOWalker.Follow._AST_RAISES_in_ruleActionRaisesMessage204);
        this.Match((IIntStream) this.input, 2, (BitSet) null);
        int num = 2;
        if (this.input.LA(1) == 48 && this.input.LA(2) == 48)
          num = 1;
        if (num == 1)
        {
          CommonTree commonTree1 = (CommonTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_ruleActionRaisesMessage209);
        }
        CommonTree commonTree2 = (CommonTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_ruleActionRaisesMessage215);
        this.Match((IIntStream) this.input, 3, (BitSet) null);
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ruleBoNodeFeature()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ruleBoNodeFeature()
    {
    }

    [GrammarRule("ruleBoNodeFeature")]
    private void ruleBoNodeFeature(string nodeName)
    {
      try
      {
        int num;
        switch (this.input.LA(1))
        {
          case 22:
            num = 3;
            break;
          case 25:
            num = 2;
            break;
          case 11:
            num = 1;
            break;
          case 19:
            num = 4;
            break;
          default:
            throw new NoViableAltException("", 12, 0, (IIntStream) this.input);
        }
        switch (num)
        {
          case 1:
            this.PushFollow(XBOWalker.Follow._ruleAction_in_ruleBoNodeFeature226);
            this.ruleAction(nodeName);
            this.PopFollow();
            break;
          case 2:
            this.PushFollow(XBOWalker.Follow._ruleBoNode_in_ruleBoNodeFeature232);
            this.ruleBoNode();
            this.PopFollow();
            break;
          case 3:
            this.PushFollow(XBOWalker.Follow._ruleMessage_in_ruleBoNodeFeature236);
            this.ruleMessage();
            this.PopFollow();
            break;
          case 4:
            this.PushFollow(XBOWalker.Follow._ruleElement_in_ruleBoNodeFeature240);
            this.ruleElement(nodeName);
            this.PopFollow();
            break;
        }
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ruleMultiplicity()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ruleMultiplicity()
    {
    }

    [GrammarRule("ruleMultiplicity")]
    private void ruleMultiplicity()
    {
      try
      {
        this.Match((IIntStream) this.input, 23, XBOWalker.Follow._AST_MULT_in_ruleMultiplicity251);
        this.Match((IIntStream) this.input, 2, (BitSet) null);
        CommonTree commonTree1 = (CommonTree) this.Match((IIntStream) this.input, 36, XBOWalker.Follow._DECLIT_in_ruleMultiplicity255);
        int num;
        switch (this.input.LA(1))
        {
          case 36:
            num = 1;
            break;
          case 33:
            num = 2;
            break;
          default:
            throw new NoViableAltException("", 13, 0, (IIntStream) this.input);
        }
        switch (num)
        {
          case 1:
            CommonTree commonTree2 = (CommonTree) this.Match((IIntStream) this.input, 36, XBOWalker.Follow._DECLIT_in_ruleMultiplicity260);
            break;
          case 2:
            CommonTree commonTree3 = (CommonTree) this.Match((IIntStream) this.input, 33, XBOWalker.Follow._CN_in_ruleMultiplicity264);
            break;
        }
        this.Match((IIntStream) this.input, 3, (BitSet) null);
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ruleAction()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ruleAction()
    {
    }

    [GrammarRule("ruleAction")]
    private void ruleAction(string nodeName)
    {
      try
      {
        this.Match((IIntStream) this.input, 11, XBOWalker.Follow._AST_ACTION_in_ruleAction278);
        this.Match((IIntStream) this.input, 2, (BitSet) null);
        this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_ruleAction280);
        while (true)
        {
          int num = 2;
          if (this.input.LA(1) == 28)
            num = 1;
          if (num == 1)
          {
            this.PushFollow(XBOWalker.Follow._ruleNodeRaisesMessage_in_ruleAction282);
            this.ruleNodeRaisesMessage(nodeName);
            this.PopFollow();
          }
          else
            break;
        }
        this.Match((IIntStream) this.input, 3, (BitSet) null);
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ruleElement()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ruleElement()
    {
    }

    [GrammarRule("ruleElement")]
    private void ruleElement(string nodeName)
    {
      XBOWalker.ruleDataTypeSymbolicRef_return symbolicRefReturn = (XBOWalker.ruleDataTypeSymbolicRef_return) null;
      List<string> elementDefaults = (List<string>) null;
      try
      {
        CommonTree commonTree1 = (CommonTree) this.Match((IIntStream) this.input, 19, XBOWalker.Follow._AST_ELEMENT_in_ruleElement298);
        this.Match((IIntStream) this.input, 2, (BitSet) null);
        CommonTree commonTree2 = (CommonTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_ruleElement300);
        int num1 = 2;
        if (this.input.LA(1) == 23)
          num1 = 1;
        if (num1 == 1)
        {
          this.PushFollow(XBOWalker.Follow._ruleMultiplicity_in_ruleElement302);
          this.ruleMultiplicity();
          this.PopFollow();
        }
        int num2 = 2;
        if (this.input.LA(1) == 29)
          num2 = 1;
        if (num2 == 1)
        {
          this.PushFollow(XBOWalker.Follow._ruleDataTypeSymbolicRef_in_ruleElement309);
          symbolicRefReturn = this.ruleDataTypeSymbolicRef();
          this.PopFollow();
        }
        int num3 = 2;
        switch (this.input.LA(1))
        {
          case 17:
          case 36:
          case 48:
          case 66:
          case 74:
            num3 = 1;
            break;
        }
        if (num3 == 1)
        {
          this.PushFollow(XBOWalker.Follow._boDefaults_in_ruleElement312);
          elementDefaults = this.boDefaults();
          this.PopFollow();
        }
        this.Match((IIntStream) this.input, 3, (BitSet) null);
        this.AddElement(commonTree2.Token, nodeName, (symbolicRefReturn != null ? symbolicRefReturn.Type : (SAP.Copernicus.Model.DataType.DataType) null) != null ? (symbolicRefReturn != null ? symbolicRefReturn.Type : (SAP.Copernicus.Model.DataType.DataType) null).SemanticalName : (string) null, commonTree1 as CustomToken, elementDefaults);
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ruleDataTypeSymbolicRef()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ruleDataTypeSymbolicRef()
    {
    }

    [GrammarRule("ruleDataTypeSymbolicRef")]
    private XBOWalker.ruleDataTypeSymbolicRef_return ruleDataTypeSymbolicRef()
    {
      XBOWalker.ruleDataTypeSymbolicRef_return symbolicRefReturn = new XBOWalker.ruleDataTypeSymbolicRef_return();
      symbolicRefReturn.Start = (CommonTree) this.input.LT(1);
      CommonTree commonTree1 = (CommonTree) null;
      try
      {
        if (this.input.LA(1) != 29)
          throw new NoViableAltException("", 19, 0, (IIntStream) this.input);
        if (this.input.LA(2) != 2)
          throw new NoViableAltException("", 19, 1, (IIntStream) this.input);
        int num1;
        switch (this.input.LA(3))
        {
          case 12:
            num1 = 2;
            break;
          case 24:
          case 48:
            num1 = 1;
            break;
          default:
            throw new NoViableAltException("", 19, 2, (IIntStream) this.input);
        }
        switch (num1)
        {
          case 1:
            this.Match((IIntStream) this.input, 29, XBOWalker.Follow._AST_TYPE_in_ruleDataTypeSymbolicRef344);
            this.Match((IIntStream) this.input, 2, (BitSet) null);
            int num2 = 2;
            if (this.input.LA(1) == 24)
              num2 = 1;
            if (num2 == 1)
              commonTree1 = (CommonTree) this.Match((IIntStream) this.input, 24, XBOWalker.Follow._AST_NAMESPACE_in_ruleDataTypeSymbolicRef346);
            CommonTree commonTree2 = (CommonTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_ruleDataTypeSymbolicRef351);
            this.Match((IIntStream) this.input, 3, (BitSet) null);
            symbolicRefReturn.Type = this.GetTypeFor(commonTree2.Token, false, commonTree1 == null ? (string) null : commonTree1.Text, commonTree2.Text);
            break;
          case 2:
            this.Match((IIntStream) this.input, 29, XBOWalker.Follow._AST_TYPE_in_ruleDataTypeSymbolicRef361);
            this.Match((IIntStream) this.input, 2, (BitSet) null);
            CommonTree commonTree3 = (CommonTree) this.Match((IIntStream) this.input, 12, XBOWalker.Follow._AST_ALIAS_in_ruleDataTypeSymbolicRef363);
            CommonTree commonTree4 = (CommonTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_ruleDataTypeSymbolicRef367);
            this.Match((IIntStream) this.input, 3, (BitSet) null);
            symbolicRefReturn.Type = this.GetTypeFor(commonTree4.Token, true, commonTree3.Text, commonTree4.Text);
            break;
        }
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
      return symbolicRefReturn;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boDefaults()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boDefaults()
    {
    }

    [GrammarRule("boDefaults")]
    private List<string> boDefaults()
    {
      List<string> stringList = (List<string>) null;
      try
      {
        stringList = this.GetBODefaultsList();
        int num1;
        switch (this.input.LA(1))
        {
          case 36:
          case 48:
          case 66:
          case 74:
            num1 = 1;
            break;
          case 17:
            num1 = 2;
            break;
          default:
            throw new NoViableAltException("", 21, 0, (IIntStream) this.input);
        }
        switch (num1)
        {
          case 1:
            this.PushFollow(XBOWalker.Follow._atomExpr_in_boDefaults388);
            XBOWalker.atomExpr_return atomExprReturn = this.atomExpr();
            this.PopFollow();
            this.addToBODefaultsList(atomExprReturn != null ? atomExprReturn.lit : (string) null, atomExprReturn != null ? atomExprReturn.type : (string) null);
            break;
          case 2:
            int num2 = 0;
            while (true)
            {
              int num3 = 2;
              if (this.input.LA(1) == 17)
                num3 = 1;
              if (num3 == 1)
              {
                this.PushFollow(XBOWalker.Follow._boDefault_in_boDefaults407);
                this.boDefault();
                this.PopFollow();
                ++num2;
              }
              else
                break;
            }
            if (num2 < 1)
              throw new EarlyExitException(20, (IIntStream) this.input);
            break;
        }
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
      return stringList;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boDefault()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boDefault()
    {
    }

    [GrammarRule("boDefault")]
    private void boDefault()
    {
      try
      {
        this.Match((IIntStream) this.input, 17, XBOWalker.Follow._AST_DEFAULT_in_boDefault436);
        this.Match((IIntStream) this.input, 2, (BitSet) null);
        this.addToBODefaultsList(((BaseTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_boDefault442)).Text, "ID");
        this.PushFollow(XBOWalker.Follow._atomExpr_in_boDefault446);
        XBOWalker.atomExpr_return atomExprReturn = this.atomExpr();
        this.PopFollow();
        this.addToBODefaultsList(atomExprReturn != null ? atomExprReturn.lit : (string) null, atomExprReturn != null ? atomExprReturn.type : (string) null);
        this.Match((IIntStream) this.input, 3, (BitSet) null);
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_atomExpr()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_atomExpr()
    {
    }

    [GrammarRule("atomExpr")]
    private XBOWalker.atomExpr_return atomExpr()
    {
      XBOWalker.atomExpr_return atomExprReturn = new XBOWalker.atomExpr_return();
      atomExprReturn.Start = (CommonTree) this.input.LT(1);
      try
      {
        int num;
        switch (this.input.LA(1))
        {
          case 66:
          case 36:
            num = 2;
            break;
          case 74:
            num = 1;
            break;
          case 48:
            num = 3;
            break;
          default:
            throw new NoViableAltException("", 22, 0, (IIntStream) this.input);
        }
        switch (num)
        {
          case 1:
            CommonTree commonTree1 = (CommonTree) this.Match((IIntStream) this.input, 74, XBOWalker.Follow._STRING_in_atomExpr473);
            atomExprReturn.lit = commonTree1.Text;
            atomExprReturn.type = "STRING";
            break;
          case 2:
            this.PushFollow(XBOWalker.Follow._numericLiteral_in_atomExpr483);
            XBOWalker.numericLiteral_return numericLiteralReturn = this.numericLiteral();
            this.PopFollow();
            atomExprReturn.lit = numericLiteralReturn != null ? numericLiteralReturn.lit : (string) null;
            atomExprReturn.type = numericLiteralReturn != null ? numericLiteralReturn.type : (string) null;
            break;
          case 3:
            CommonTree commonTree2 = (CommonTree) this.Match((IIntStream) this.input, 48, XBOWalker.Follow._ID_in_atomExpr496);
            atomExprReturn.lit = commonTree2.Text;
            atomExprReturn.type = "ID";
            break;
        }
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
      return atomExprReturn;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_numericLiteral()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_numericLiteral()
    {
    }

    [GrammarRule("numericLiteral")]
    private XBOWalker.numericLiteral_return numericLiteral()
    {
      XBOWalker.numericLiteral_return numericLiteralReturn = new XBOWalker.numericLiteral_return();
      numericLiteralReturn.Start = (CommonTree) this.input.LT(1);
      try
      {
        int num;
        switch (this.input.LA(1))
        {
          case 36:
            num = 1;
            break;
          case 66:
            num = 2;
            break;
          default:
            throw new NoViableAltException("", 23, 0, (IIntStream) this.input);
        }
        switch (num)
        {
          case 1:
            CommonTree commonTree1 = (CommonTree) this.Match((IIntStream) this.input, 36, XBOWalker.Follow._DECLIT_in_numericLiteral525);
            numericLiteralReturn.lit = commonTree1.Text;
            numericLiteralReturn.type = "DECLIT";
            break;
          case 2:
            CommonTree commonTree2 = (CommonTree) this.Match((IIntStream) this.input, 66, XBOWalker.Follow._NUMLIT_in_numericLiteral538);
            numericLiteralReturn.lit = commonTree2.Text;
            numericLiteralReturn.type = "NUMLIT";
            break;
        }
      }
      catch (RecognitionException ex)
      {
        this.ReportError(ex);
        this.Recover((IIntStream) this.input, ex);
      }
      return numericLiteralReturn;
    }

    private sealed class ruleDataTypeSymbolicRef_return : TreeRuleReturnScope<CommonTree>
    {
      public SAP.Copernicus.Model.DataType.DataType Type;
      public bool isAlias;
    }

    private sealed class atomExpr_return : TreeRuleReturnScope<CommonTree>
    {
      public string lit;
      public string type;
    }

    private sealed class numericLiteral_return : TreeRuleReturnScope<CommonTree>
    {
      public string lit;
      public string type;
    }

    private static class Follow
    {
      public static readonly BitSet _ruleBusinessObject_in_ruleModel50 = new BitSet(new ulong[1]
      {
        2UL
      });
      public static readonly BitSet _AST_BO_in_ruleBusinessObject64 = new BitSet(new ulong[1]
      {
        4UL
      });
      public static readonly BitSet _AST_NAMESPACE_in_ruleBusinessObject66 = new BitSet(new ulong[1]
      {
        281474976710656UL
      });
      public static readonly BitSet _ID_in_ruleBusinessObject68 = new BitSet(new ulong[1]
      {
        306710536UL
      });
      public static readonly BitSet _ruleNodeRaisesMessage_in_ruleBusinessObject72 = new BitSet(new ulong[1]
      {
        306710536UL
      });
      public static readonly BitSet _ruleBoNodeFeature_in_ruleBusinessObject76 = new BitSet(new ulong[1]
      {
        38275080UL
      });
      public static readonly BitSet _AST_MESSAGE_in_ruleMessage91 = new BitSet(new ulong[1]
      {
        4UL
      });
      public static readonly BitSet _ID_in_ruleMessage93 = new BitSet(new ulong[2]
      {
        0UL,
        1024UL
      });
      public static readonly BitSet _STRING_in_ruleMessage95 = new BitSet(new ulong[1]
      {
        17179869192UL
      });
      public static readonly BitSet _COLON_in_ruleMessage98 = new BitSet(new ulong[1]
      {
        536870912UL
      });
      public static readonly BitSet _ruleDataTypeSymbolicRef_in_ruleMessage102 = new BitSet(new ulong[1]
      {
        536870920UL
      });
      public static readonly BitSet _ruleDataTypeSymbolicRef_in_ruleMessage108 = new BitSet(new ulong[1]
      {
        536870920UL
      });
      public static readonly BitSet _ruleDataTypeSymbolicRef_in_ruleMessage113 = new BitSet(new ulong[1]
      {
        536870920UL
      });
      public static readonly BitSet _ruleDataTypeSymbolicRef_in_ruleMessage117 = new BitSet(new ulong[1]
      {
        8UL
      });
      public static readonly BitSet _AST_NODE_in_ruleBoNode148 = new BitSet(new ulong[1]
      {
        4UL
      });
      public static readonly BitSet _ID_in_ruleBoNode150 = new BitSet(new ulong[1]
      {
        315099144UL
      });
      public static readonly BitSet _ruleMultiplicity_in_ruleBoNode154 = new BitSet(new ulong[1]
      {
        306710536UL
      });
      public static readonly BitSet _ruleNodeRaisesMessage_in_ruleBoNode157 = new BitSet(new ulong[1]
      {
        306710536UL
      });
      public static readonly BitSet _ruleBoNodeFeature_in_ruleBoNode161 = new BitSet(new ulong[1]
      {
        38275080UL
      });
      public static readonly BitSet _AST_RAISES_in_ruleNodeRaisesMessage178 = new BitSet(new ulong[1]
      {
        4UL
      });
      public static readonly BitSet _ID_in_ruleNodeRaisesMessage183 = new BitSet(new ulong[1]
      {
        281474976710656UL
      });
      public static readonly BitSet _ID_in_ruleNodeRaisesMessage189 = new BitSet(new ulong[1]
      {
        8UL
      });
      public static readonly BitSet _AST_RAISES_in_ruleActionRaisesMessage204 = new BitSet(new ulong[1]
      {
        4UL
      });
      public static readonly BitSet _ID_in_ruleActionRaisesMessage209 = new BitSet(new ulong[1]
      {
        281474976710656UL
      });
      public static readonly BitSet _ID_in_ruleActionRaisesMessage215 = new BitSet(new ulong[1]
      {
        8UL
      });
      public static readonly BitSet _ruleAction_in_ruleBoNodeFeature226 = new BitSet(new ulong[1]
      {
        2UL
      });
      public static readonly BitSet _ruleBoNode_in_ruleBoNodeFeature232 = new BitSet(new ulong[1]
      {
        2UL
      });
      public static readonly BitSet _ruleMessage_in_ruleBoNodeFeature236 = new BitSet(new ulong[1]
      {
        2UL
      });
      public static readonly BitSet _ruleElement_in_ruleBoNodeFeature240 = new BitSet(new ulong[1]
      {
        2UL
      });
      public static readonly BitSet _AST_MULT_in_ruleMultiplicity251 = new BitSet(new ulong[1]
      {
        4UL
      });
      public static readonly BitSet _DECLIT_in_ruleMultiplicity255 = new BitSet(new ulong[1]
      {
        77309411328UL
      });
      public static readonly BitSet _DECLIT_in_ruleMultiplicity260 = new BitSet(new ulong[1]
      {
        8UL
      });
      public static readonly BitSet _CN_in_ruleMultiplicity264 = new BitSet(new ulong[1]
      {
        8UL
      });
      public static readonly BitSet _AST_ACTION_in_ruleAction278 = new BitSet(new ulong[1]
      {
        4UL
      });
      public static readonly BitSet _ID_in_ruleAction280 = new BitSet(new ulong[1]
      {
        268435464UL
      });
      public static readonly BitSet _ruleNodeRaisesMessage_in_ruleAction282 = new BitSet(new ulong[1]
      {
        268435464UL
      });
      public static readonly BitSet _AST_ELEMENT_in_ruleElement298 = new BitSet(new ulong[1]
      {
        4UL
      });
      public static readonly BitSet _ID_in_ruleElement300 = new BitSet(new ulong[2]
      {
        281544241577992UL,
        1028UL
      });
      public static readonly BitSet _ruleMultiplicity_in_ruleElement302 = new BitSet(new ulong[2]
      {
        281544233189384UL,
        1028UL
      });
      public static readonly BitSet _ruleDataTypeSymbolicRef_in_ruleElement309 = new BitSet(new ulong[2]
      {
        281543696318472UL,
        1028UL
      });
      public static readonly BitSet _boDefaults_in_ruleElement312 = new BitSet(new ulong[1]
      {
        8UL
      });
      public static readonly BitSet _AST_TYPE_in_ruleDataTypeSymbolicRef344 = new BitSet(new ulong[1]
      {
        4UL
      });
      public static readonly BitSet _AST_NAMESPACE_in_ruleDataTypeSymbolicRef346 = new BitSet(new ulong[1]
      {
        281474976710656UL
      });
      public static readonly BitSet _ID_in_ruleDataTypeSymbolicRef351 = new BitSet(new ulong[1]
      {
        8UL
      });
      public static readonly BitSet _AST_TYPE_in_ruleDataTypeSymbolicRef361 = new BitSet(new ulong[1]
      {
        4UL
      });
      public static readonly BitSet _AST_ALIAS_in_ruleDataTypeSymbolicRef363 = new BitSet(new ulong[1]
      {
        281474976710656UL
      });
      public static readonly BitSet _ID_in_ruleDataTypeSymbolicRef367 = new BitSet(new ulong[1]
      {
        8UL
      });
      public static readonly BitSet _atomExpr_in_boDefaults388 = new BitSet(new ulong[1]
      {
        2UL
      });
      public static readonly BitSet _boDefault_in_boDefaults407 = new BitSet(new ulong[1]
      {
        131074UL
      });
      public static readonly BitSet _AST_DEFAULT_in_boDefault436 = new BitSet(new ulong[1]
      {
        4UL
      });
      public static readonly BitSet _ID_in_boDefault442 = new BitSet(new ulong[2]
      {
        281543696187392UL,
        1028UL
      });
      public static readonly BitSet _atomExpr_in_boDefault446 = new BitSet(new ulong[1]
      {
        8UL
      });
      public static readonly BitSet _STRING_in_atomExpr473 = new BitSet(new ulong[1]
      {
        2UL
      });
      public static readonly BitSet _numericLiteral_in_atomExpr483 = new BitSet(new ulong[1]
      {
        2UL
      });
      public static readonly BitSet _ID_in_atomExpr496 = new BitSet(new ulong[1]
      {
        2UL
      });
      public static readonly BitSet _DECLIT_in_numericLiteral525 = new BitSet(new ulong[1]
      {
        2UL
      });
      public static readonly BitSet _NUMLIT_in_numericLiteral538 = new BitSet(new ulong[1]
      {
        2UL
      });
    }
  }
}
