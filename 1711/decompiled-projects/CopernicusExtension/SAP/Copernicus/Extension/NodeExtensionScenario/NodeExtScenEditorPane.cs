﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.NodeExtensionScenario.NodeExtScenEditorPane
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.CustomEditor;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Extension.NodeExtensionScenario
{
  public sealed class NodeExtScenEditorPane : CustomEditorPane<NodeExtScenEditorFactory, NodeExtScenEditorControl>
  {
    protected override string GetFileExtension()
    {
      return ".nxs";
    }

    public NodeExtScenEditorPane()
    {
      this.UIControl.ContentChanged += new EventHandler(this.ViewContentChange);
    }

    protected override void SaveFile(string fileName)
    {
      string pathforLocalFile = XRepMapper.GetInstance().GetXrepPathforLocalFile(fileName);
      string content;
      IDictionary<string, string> attribs;
      if (!new XRepHandler().Read(pathforLocalFile, true, out content, out attribs))
      {
        IVsFileChangeEx service = (IVsFileChangeEx) this.GetService(typeof (SVsFileChangeEx));
        service.IgnoreFile(0U, fileName, -1);
        this.UIControl.SaveFile(fileName);
        service.SyncFile(fileName);
        service.IgnoreFile(0U, fileName, 0);
      }
      else if (this.UIControl.CheckShippedDeleted(content))
      {
        int num = (int) CopernicusMessageBox.Show("Node Extension Scenario " + pathforLocalFile + " already shipped. No deletion allowed!");
        this.UIControl.LoadFiles(fileName);
      }
      else
      {
        IVsFileChangeEx service = (IVsFileChangeEx) this.GetService(typeof (SVsFileChangeEx));
        service.IgnoreFile(0U, fileName, -1);
        this.UIControl.SaveFile(fileName);
        service.SyncFile(fileName);
        service.IgnoreFile(0U, fileName, 0);
      }
    }

    protected override void LoadFile(string fileName)
    {
      this.UIControl.LoadFiles(fileName);
    }

    protected override void OnClose()
    {
      this.UIControl.closeEditor();
    }

    private void ViewContentChange(object sender, EventArgs e)
    {
      this.OnContentChanged();
    }

    protected override Guid GetPackageGuid()
    {
      return typeof (CopernicusExtensionPackage).GUID;
    }

    protected override void OnCreate()
    {
      this.InitializeF1Help(HELP_IDS.BDS_PROCESSEXTENSION_EDITOR);
    }
  }
}
