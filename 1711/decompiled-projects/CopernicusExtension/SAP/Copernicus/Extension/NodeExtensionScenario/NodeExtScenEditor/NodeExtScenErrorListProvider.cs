﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.NodeExtensionScenario.NodeExtScenEditor.NodeExtScenErrorListProvider
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.Automation;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Extension.NodeExtensionScenario.NodeExtScenEditor
{
  public class NodeExtScenErrorListProvider : ErrorListProvider
  {
    public NodeExtScenErrorListProvider(IServiceProvider serviceProvider)
      : base(serviceProvider)
    {
      this.Show();
    }

    public void ClearErrorTaskItems()
    {
      this.Tasks.Clear();
    }

    public void ClearErrorTaskItems(string filePath)
    {
      IList<Task> taskList = (IList<Task>) new List<Task>();
      foreach (Task task in this.Tasks)
      {
        if (task.Document.Equals(filePath))
          taskList.Add(task);
      }
      foreach (Task task in (IEnumerable<Task>) taskList)
        this.Tasks.Remove(task);
    }

    public void ClearErrorTaskItems(string filePath, int tab)
    {
      IList<Task> taskList = (IList<Task>) new List<Task>();
      foreach (Task task in this.Tasks)
      {
        if (task.Document.Equals(filePath) && task.Column == tab)
          taskList.Add(task);
      }
      foreach (Task task in (IEnumerable<Task>) taskList)
        this.Tasks.Remove(task);
    }

    public void ClearErrorTaskItems(int tab)
    {
      IList<Task> taskList = (IList<Task>) new List<Task>();
      foreach (Task task in this.Tasks)
      {
        if (task.Column == tab)
          taskList.Add(task);
      }
      foreach (Task task in (IEnumerable<Task>) taskList)
        this.Tasks.Remove(task);
    }

    private ErrorTask CreateErrorTaskItem(NodeExtScenEditorControl editor, string errorMessage, string filePath)
    {
      return this.CreateErrorTaskItem(editor, errorMessage, filePath, 0, 0);
    }

    public ErrorTask CreateErrorTaskItem(NodeExtScenEditorControl editor, string filePath, string errorMessage, TaskErrorCategory taskCategory, int tab, int pos)
    {
      NodeExtScenErrorTask extScenErrorTask = new NodeExtScenErrorTask();
      extScenErrorTask.Editor = editor;
      extScenErrorTask.Document = filePath;
      extScenErrorTask.Text = errorMessage;
      extScenErrorTask.Column = tab;
      extScenErrorTask.Line = pos;
      extScenErrorTask.Category = TaskCategory.BuildCompile;
      extScenErrorTask.ErrorCategory = taskCategory;
      extScenErrorTask.Priority = TaskPriority.Normal;
      extScenErrorTask.Navigate += new EventHandler(this.ErrorTaskItemNavigate);
      IVsHierarchy selectedProject = this.GetSelectedProject();
      if (selectedProject != null)
        extScenErrorTask.HierarchyItem = selectedProject;
      this.Tasks.Add((Task) extScenErrorTask);
      return (ErrorTask) extScenErrorTask;
    }

    public ErrorTask CreateErrorTaskItem(NodeExtScenEditorControl editor, string filePath, string errorMessage, int tab, int pos)
    {
      return this.CreateErrorTaskItem(editor, filePath, errorMessage, TaskErrorCategory.Error, tab, pos);
    }

    public Task CheckForErrorTask(string filePath, string errorMessage, int tab, int pos)
    {
      foreach (Task task in this.Tasks)
      {
        if (task.Document.Equals(filePath) && task.Text.Equals(errorMessage) && (task.Column == tab && task.Line == pos))
          return task;
      }
      return (Task) null;
    }

    public Task CheckForScenarioWarning(string boName, TaskErrorCategory taskCategory)
    {
      foreach (NodeExtScenErrorTask task in this.Tasks)
      {
        if (task.ErrorCategory == taskCategory && task.Text.Contains(boName))
          return (Task) task;
      }
      return (Task) null;
    }

    private void ErrorTaskItemNavigate(object sender, EventArgs e)
    {
      NodeExtScenErrorTask extScenErrorTask = sender as NodeExtScenErrorTask;
    }

    private IVsHierarchy GetSelectedProject()
    {
      IVsHierarchy ppHierarchy = (IVsHierarchy) null;
      Array solutionProjects = DTEUtil.GetDTE().ActiveSolutionProjects as Array;
      if (solutionProjects != null && solutionProjects.Length > 0)
      {
        IVsSolution service = this.GetService(typeof (SVsSolution)) as IVsSolution;
        if (service != null)
          service.GetProjectOfUniqueName(((Project) solutionProjects.GetValue(0)).FullName, out ppHierarchy);
      }
      return ppHierarchy;
    }
  }
}
