﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Resources
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus.Extension
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) SAP.Copernicus.Extension.Resources.resourceMan, (object) null))
          SAP.Copernicus.Extension.Resources.resourceMan = new ResourceManager("SAP.Copernicus.Extension.Resources", typeof (SAP.Copernicus.Extension.Resources).Assembly);
        return SAP.Copernicus.Extension.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.resourceCulture;
      }
      set
      {
        SAP.Copernicus.Extension.Resources.resourceCulture = value;
      }
    }

    internal static string BONodeNotReleased
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (BONodeNotReleased), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string BOSelectionBOexists
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (BOSelectionBOexists), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string BOSelectionNoBOforNS
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (BOSelectionNoBOforNS), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string BOSelectionNoExtallowed
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (BOSelectionNoExtallowed), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string BOSelectionNoExtNodes
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (BOSelectionNoExtNodes), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string BOSelectionNoExtNodesforBO
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (BOSelectionNoExtNodesforBO), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string BOSelectionSubtitle
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (BOSelectionSubtitle), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string BOSelectionTemplateBO
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (BOSelectionTemplateBO), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string BOSelectionTitle
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (BOSelectionTitle), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string CodeWithIdentifyingContextIndicator
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (CodeWithIdentifyingContextIndicator), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string COLLAPSE_ALL_SCENARIOS
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (COLLAPSE_ALL_SCENARIOS), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string CrossXBOError
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (CrossXBOError), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ElementDefaultValueError
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ElementDefaultValueError), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ESEnhance
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ESEnhance), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string EXPAND_ALL_SCENARIOS
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (EXPAND_ALL_SCENARIOS), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtendedBO
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtendedBO), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtendedNode
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtendedNode), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtensionNodeSelectionTitle
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtensionNodeSelectionTitle), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtensionScenarioEditorView_InfoText
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtensionScenarioEditorView_InfoText), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtensionScenarioInUseMsg
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtensionScenarioInUseMsg), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtensionScenarioSelectionView_InfoText
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtensionScenarioSelectionView_InfoText), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtensionScenarioSelectionView_TitleText
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtensionScenarioSelectionView_TitleText), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtFieldActiveInOtherBO
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtFieldActiveInOtherBO), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtFieldDefinitionFailed
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtFieldDefinitionFailed), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtFieldManagedByKUT
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtFieldManagedByKUT), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtFieldManagedByKUTLabel
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtFieldManagedByKUTLabel), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtFieldManagedByKUTRef
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtFieldManagedByKUTRef), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtFieldManagedByKUTScen
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtFieldManagedByKUTScen), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtFieldManagedByKUTTooltip
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtFieldManagedByKUTTooltip), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtFieldManagedByKUTType
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtFieldManagedByKUTType), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtFieldNotMatchScenario
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtFieldNotMatchScenario), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtScenarioSelView_cmdBoxNamspace_Info
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtScenarioSelView_cmdBoxNamspace_Info), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ExtScenarioSelView_ScenarioView_Info
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ExtScenarioSelView_ScenarioView_Info), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string FormSelectionCopyForm
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (FormSelectionCopyForm), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string FormSelectionSubtitle
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (FormSelectionSubtitle), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string FormSelectionTitle
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (FormSelectionTitle), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string FurtherUsageSubtitleES
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (FurtherUsageSubtitleES), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string FurtherUsageSubTitleReport
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (FurtherUsageSubTitleReport), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string FurtherUsageTitleES
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (FurtherUsageTitleES), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string FurtherUsageTitleReport
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (FurtherUsageTitleReport), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static Bitmap GenerateBO
    {
      get
      {
        return (Bitmap) SAP.Copernicus.Extension.Resources.ResourceManager.GetObject(nameof (GenerateBO), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string INCONSISTENT_SCENARIOS
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (INCONSISTENT_SCENARIOS), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL_XBODLMSG_UNSUPPORTED_ANNOTATION_COMBINATION
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL_XBODLMSG_UNSUPPORTED_ANNOTATION_COMBINATION), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_BO_NAME_OR_NAMSPACE_CHANGED
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_BO_NAME_OR_NAMSPACE_CHANGED), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_BONAME_OR_NAMSPACE_CHANGED
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_BONAME_OR_NAMSPACE_CHANGED), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_ELEMENT_NAME_TOO_LONG
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_ELEMENT_NAME_TOO_LONG), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_ERRORS_IN_XBODL
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_ERRORS_IN_XBODL), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_EXF_REG_ERROR
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_EXF_REG_ERROR), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_EXTENSION_DEFAULT_VALUE
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_EXTENSION_DEFAULT_VALUE), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_EXTENSION_FIELD_ALREADY_USED
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_EXTENSION_FIELD_ALREADY_USED), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_EXTENSION_FIELD_NOT_UNIQUE
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_EXTENSION_FIELD_NOT_UNIQUE), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_EXTENSION_MISSING
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_EXTENSION_MISSING), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_FIELD_NAME_EXISTS_IN_STANDARD_BO
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_FIELD_NAME_EXISTS_IN_STANDARD_BO), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_FILE_NOT_SAVED
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_FILE_NOT_SAVED), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_GENERATION_ERROR
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_GENERATION_ERROR), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_INVALID_FIELD_TYPE_FOR_EXT
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_INVALID_FIELD_TYPE_FOR_EXT), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_NO_ANNO_TO_BIG
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_NO_ANNO_TO_BIG), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_NODE_NOT_EXTENSIBLE
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_NODE_NOT_EXTENSIBLE), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_NOT_AN_EXTNODE
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_NOT_AN_EXTNODE), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_PROJ_NS_NOT_FOUND
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_PROJ_NS_NOT_FOUND), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_ROOT_NODE_NOT_EXTENSIBLE
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_ROOT_NODE_NOT_EXTENSIBLE), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL1_XBODLMSG_SCRIPT_FILE_NOT_ACTIVATED
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL1_XBODLMSG_SCRIPT_FILE_NOT_ACTIVATED), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string LEVEL3_XBODLMSG_COMPILERVERSION
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (LEVEL3_XBODLMSG_COMPILERVERSION), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string MessageIDNotValid
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (MessageIDNotValid), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string MsgBYD
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (MsgBYD), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string MsgExceptionOccured
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (MsgExceptionOccured), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string NamespaceSelectionTitle
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (NamespaceSelectionTitle), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string NO_ALTERNATIVE_KEY
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (NO_ALTERNATIVE_KEY), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string NO_BO_FOUND
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (NO_BO_FOUND), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string NO_SCENARIOS_CHECKED
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (NO_SCENARIOS_CHECKED), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string NO_SCENARIOS_FOUND
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (NO_SCENARIOS_FOUND), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string NoRelationParameter
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (NoRelationParameter), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ReferencedFieldNotExist
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ReferencedFieldNotExist), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ReferenceFieldDeclarationError
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ReferenceFieldDeclarationError), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string RefStandardBOfield
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (RefStandardBOfield), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string RelationAttributePathMissing
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (RelationAttributePathMissing), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string RelationPathIncomplete
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (RelationPathIncomplete), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string RelationTargetError
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (RelationTargetError), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ReportEnhance
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ReportEnhance), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ScenarioFieldDeclarationError
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ScenarioFieldDeclarationError), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ScenarioInUseMsg
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ScenarioInUseMsg), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ScenarioIsReferenced
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ScenarioIsReferenced), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ScenarioNotExist
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ScenarioNotExist), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string ScenarioWrongNode
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (ScenarioWrongNode), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string UIDesignerLoad
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (UIDesignerLoad), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string UIDesignerLoadError
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (UIDesignerLoadError), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string UIDesignerOpenUIError
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (UIDesignerOpenUIError), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string UIModelSelectionSubtitle
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (UIModelSelectionSubtitle), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string UIModelSelectionTitle
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (UIModelSelectionTitle), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string WaitBOGenerate
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (WaitBOGenerate), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string WaitCaption
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (WaitCaption), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string WaitLoadingBoData
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (WaitLoadingBoData), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }

    internal static string WaitProcessExtensionScenarioLoad
    {
      get
      {
        return SAP.Copernicus.Extension.Resources.ResourceManager.GetString(nameof (WaitProcessExtensionScenarioLoad), SAP.Copernicus.Extension.Resources.resourceCulture);
      }
    }
  }
}
