﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.CopernicusExtensionPackage
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.Events;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Extension.CustomerObjectReferences;
using SAP.Copernicus.Extension.EmbeddedComponent;
using SAP.Copernicus.Extension.LanguageService;
using SAP.Copernicus.Extension.Mashup.MashupProjectNode;
using SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler;
using SAP.Copernicus.Extension.Mashup.WebBrowser;
using SAP.Copernicus.Extension.NodeExtensionScenario;
using SAP.Copernicus.Extension.NodeExtensionScenario.NodeExtScenEditor;
using SAP.Copernicus.Extension.ProcessExtensionScenario;
using SAP.Copernicus.Extension.ProcessExtensionScenario.ExtScenEditor;
using SAP.CopernicusProjectView;
using System;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension
{
  [ProvideAutoLoad("F1536EF8-92EC-443C-9ED7-FDADF150DA82")]
  [Guid("dd925b8f-54d3-4a44-970a-d290e3703b68")]
  [ProvideService(typeof (SAuthoringMashupFileNodeFactory))]
  [ProvideService(typeof (XBOLanguageService))]
  [ProvideLanguageExtension(typeof (XBOLanguageService), ".xbo")]
  [ProvideEditorExtension(typeof (ExtScenEditorFactory), ".xs", 50, DefaultName = "CopernicusExtension", TemplateDir = "..\\..\\Templates")]
  [ProvideEditorExtension(typeof (NodeExtScenEditorFactory), ".nxs", 50, DefaultName = "CopernicusExtension", TemplateDir = "..\\..\\Templates")]
  [ProvideEditorExtension(typeof (CustomerObjectReferencesEditorFactory), ".ref", 60, DefaultName = "CopernicusExtension", TemplateDir = "..\\..\\Templates")]
  [ProvideLanguageService(typeof (XBOLanguageService), "Business Object Ext.", 0, AutoOutlining = true, CodeSense = true, CodeSenseDelay = 0, EnableAsyncCompletion = true, EnableCommenting = true, EnableLineNumbers = true, MatchBraces = true, ShowCompletion = true, ShowMatchingBrace = true)]
  [ProvideLoadKey("Professional", "1.0", "CopernicusExtension", "SAP", 113)]
  [ProvideService(typeof (SMashupConfigurationNodeFactory))]
  [ProvideService(typeof (SUIComponentFactory))]
  [PackageRegistration(UseManagedResourcesOnly = true)]
  [ProvideMenuResource(4000, 1)]
  [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
  public sealed class CopernicusExtensionPackage : Package, IOleComponent
  {
    private uint componentID;
    private XBOLanguageService languageService;
    private XBOFileHandler xboFileEventHandler;
    private EmbCompFileHandler embCompFileEventHandler;
    private ExtScenFileHandler extscenFileHandler;
    private static CopernicusExtensionPackage instance;
    private NodeExtScenErrorListProvider nodeExterrorListProvider;
    private ExtScenErrorListProvider errorListProvider;
    private int ctCount_new;

    public ExtScenErrorListProvider ErrorListProvider
    {
      get
      {
        return this.errorListProvider;
      }
    }

    public NodeExtScenErrorListProvider NodeErrorListProvider
    {
      get
      {
        return this.nodeExterrorListProvider;
      }
    }

    public CopernicusExtensionPackage()
    {
      Trace.WriteLine(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "Entering constructor for: {0}", new object[1]
      {
        (object) this.ToString()
      }));
      System.ComponentModel.Design.ServiceCreatorCallback callback = new System.ComponentModel.Design.ServiceCreatorCallback(this.CreateService);
      ((IServiceContainer)this).AddService(typeof (XBOLanguageService), callback, true);
      this.EnableMashupLocalFileChange(callback);
    }

    internal void EnableMashupLocalFileChange(System.ComponentModel.Design.ServiceCreatorCallback callback)
    {
      ((IServiceContainer)this).AddService(typeof (SMashupConfigurationNodeFactory), callback, true);
      ((IServiceContainer)this).AddService(typeof (SAuthoringMashupFileNodeFactory), callback, true);
      ((IServiceContainer)this).AddService(typeof (SUIComponentFactory), callback, true);
      SolutionEventHandlerRegistry.INSTANCE.AddOnSolutionOpenEventHandler(new System.Action(this.MonitorMashupFolder));
      SolutionEventHandlerRegistry.INSTANCE.AddOnSolutionOpenEventHandler(new System.Action(this.MonitorServiceFolder));
      SolutionEventHandlerRegistry.INSTANCE.AddOnSolutionOpenEventHandler(new System.Action(this.MonitorPortBindingFolder));
      SolutionEventHandlerRegistry.INSTANCE.AddMashupRootFolderCreatedEventHandler(new System.Action<bool>(this.MonitorMashupFolderWithIndicator));
      SolutionEventHandlerRegistry.INSTANCE.AddServiceRootFolderCreatedEventHandler(new System.Action<bool>(this.MonitorServiceFolderWithIndicator));
      SolutionEventHandlerRegistry.INSTANCE.AddOnSolutionCloseEventHandler(new EventHandlerOnSolutionClose(this.MonitorMashupTerminate));
      SolutionEventHandlerRegistry.INSTANCE.AddPortBindingRootFolderCreatedEventHandler(new System.Action<bool>(this.MonitorServiceFolderWithIndicator));
    }

    internal void MonitorMashupFolder()
    {
      CopernicusExtensionPackage.EstablishMashupNodeFolder();
    }

    internal void MonitorServiceFolder()
    {
      CopernicusExtensionPackage.EstablishWSNodeFolder();
    }

    internal void MonitorPortBindingFolder()
    {
      CopernicusExtensionPackage.EstablishPBNodeFolder();
    }

    internal void MonitorMashupFolderWithIndicator(bool isMashupLoadForFirstTime)
    {
      CopernicusExtensionPackage.EstablishMashupNodeFolder();
      this.UpdateMashupDependentUIbasedOnChangeTransacationCount(MashupHierarchDefNode.MashupAuthoringReferenceNode as MashupHierarchDefNode, isMashupLoadForFirstTime);
    }

    internal void MonitorServiceFolderWithIndicator(bool isServiceLoadForFirstTime)
    {
      CopernicusExtensionPackage.EstablishWSNodeFolder();
      this.ctCount_new = 0;
      this.UpdateMashupDependentUIbasedOnChangeTransacationCount(MashupHierarchDefNode.MashupServiceReferenceNode as MashupHierarchDefNode, isServiceLoadForFirstTime);
    }

    internal void MonitorPortBindingFolderWithIndicator(bool isPortBindingLoadForFirstTime)
    {
      CopernicusExtensionPackage.EstablishPBNodeFolder();
      this.ctCount_new = 0;
      this.UpdateMashupDependentUIbasedOnChangeTransacationCount(MashupHierarchDefNode.MashupPortBindingReferenceNode as MashupHierarchDefNode, isPortBindingLoadForFirstTime);
    }

    internal void UpdateMashupDependentUIbasedOnChangeTransacationCount(MashupHierarchDefNode _rootHierarchyNode, bool isServiceLoadForFirstTime)
    {
      ChangeTransactionHelper instance = ChangeTransactionHelper.Instance;
      if (isServiceLoadForFirstTime)
        return;
      int transactionCount = instance.getChangeTransactionCount();
      if (transactionCount == this.ctCount_new || transactionCount == 0)
        return;
      for (HierarchyNode hierarchyNode = _rootHierarchyNode.FirstChild; hierarchyNode != null; hierarchyNode = hierarchyNode.NextSibling)
        (hierarchyNode as AuthoringMashupFileNode).UpdateDependentUIComponent();
      this.ctCount_new = transactionCount;
    }

    private static void EstablishPBNodeFolder()
    {
      if (MashupHierarchDefNode.MashupPortBindingReferenceNode == null)
        return;
      MashupHierarchDefNode bindingReferenceNode = MashupHierarchDefNode.MashupPortBindingReferenceNode as MashupHierarchDefNode;
      MashupItemSynchronizer.regiterFileChangeMonitor(bindingReferenceNode.RootProjectFolder, bindingReferenceNode.Url, ".PB.uimashup");
    }

    private static void EstablishWSNodeFolder()
    {
      if (MashupHierarchDefNode.MashupServiceReferenceNode == null)
        return;
      MashupHierarchDefNode serviceReferenceNode = MashupHierarchDefNode.MashupServiceReferenceNode as MashupHierarchDefNode;
      MashupItemSynchronizer.regiterFileChangeMonitor(serviceReferenceNode.RootProjectFolder, serviceReferenceNode.Url, ".WS.uimashup");
    }

    private static void EstablishMashupNodeFolder()
    {
      if (MashupHierarchDefNode.MashupAuthoringReferenceNode == null)
        return;
      MashupHierarchDefNode authoringReferenceNode = MashupHierarchDefNode.MashupAuthoringReferenceNode as MashupHierarchDefNode;
      MashupItemSynchronizer.regiterFileChangeMonitor(authoringReferenceNode.RootProjectFolder, authoringReferenceNode.Url, ".MC.uimashup");
    }

    internal void MonitorMashupTerminate()
    {
      MashupItemSynchronizer.Terminate();
      WebBrowserProxy.getInstance().CloseAllRunningBrowser();
    }

    internal static CopernicusExtensionPackage Instance
    {
      get
      {
        if (CopernicusExtensionPackage.instance == null)
          throw new InvalidOperationException("Package CopernicusExtensionPackage not initialized.");
        return CopernicusExtensionPackage.instance;
      }
    }

    private object CreateService(IServiceContainer container, Type serviceType)
    {
      if (serviceType == typeof (XBOLanguageService))
      {
        if (this.languageService == null)
        {
          this.languageService = new XBOLanguageService();
          this.languageService.SetSite((object) this);
          IOleComponentManager service = this.GetService(typeof (SOleComponentManager)) as IOleComponentManager;
          if ((int) this.componentID == 0 && service != null)
          {
            OLECRINFO[] pcrinfo = new OLECRINFO[1];
            pcrinfo[0].cbSize = (uint) Marshal.SizeOf(typeof (OLECRINFO));
            pcrinfo[0].grfcrf = 3U;
            pcrinfo[0].grfcadvf = 7U;
            pcrinfo[0].uIdleTimeInterval = 1000U;
            service.FRegisterComponent((IOleComponent) this, pcrinfo, out this.componentID);
          }
        }
        return (object) this.languageService;
      }
      if (serviceType == typeof (SMashupConfigurationNodeFactory))
        return (object) new MashupHierarchDefNodeFactory();
      if (serviceType == typeof (SAuthoringMashupFileNodeFactory))
        return (object) new AuthoringMashupFileNodeFactory();
      if (serviceType == typeof (SUIComponentFactory))
        return (object) new UIComponentRefWithMashupFactory();
      return (object) null;
    }

    protected override void Initialize()
    {
      Trace.WriteLine(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "Entering Initialize() of: {0}", new object[1]
      {
        (object) this.ToString()
      }));
      base.Initialize();
      this.RegisterEditorFactory((IVsEditorFactory) new ExtScenEditorFactory());
      this.RegisterEditorFactory((IVsEditorFactory) new NodeExtScenEditorFactory());
      this.RegisterEditorFactory((IVsEditorFactory) new CustomerObjectReferencesEditorFactory());
      this.xboFileEventHandler = XBOFileHandler.Instance;
      this.embCompFileEventHandler = EmbCompFileHandler.Instance;
      this.extscenFileHandler = ExtScenFileHandler.Instance;
      FileHandlerRegistry.INSTANCE.AddHandler("xbo", new EventHandlerOnFileDeletion(this.xboFileEventHandler.OnFileDeletion));
      FileHandlerRegistry.INSTANCE.AddHandler("xbo", new EventHandlerOnFileSave(this.xboFileEventHandler.OnFileSave));
      FileHandlerRegistry.INSTANCE.AddHandler("uicomponent", new EventHandlerOnFileDeletion(this.embCompFileEventHandler.OnFileDeletion));
      FileHandlerRegistry.INSTANCE.AddHandler("xs", new EventHandlerOnFileSave(this.extscenFileHandler.OnFileSave));
      FileHandlerRegistry.INSTANCE.AddHandler("xs", new EventHandlerOnFileDeletion(this.extscenFileHandler.OnFileDeletion));
      FileHandlerRegistry.INSTANCE.AddHandler("nxs", new EventHandlerOnFileSave(this.extscenFileHandler.OnFileSave));
      FileHandlerRegistry.INSTANCE.AddHandler("nxs", new EventHandlerOnFileDeletion(this.extscenFileHandler.OnFileDeletion));
      CopernicusExtensionPackage.instance = this;
      this.initErrorListProvider();
    }

    private void initErrorListProvider()
    {
      if (this.nodeExterrorListProvider == null)
        this.nodeExterrorListProvider = new NodeExtScenErrorListProvider((System.IServiceProvider) this);
      if (this.errorListProvider != null)
        return;
      this.errorListProvider = new ExtScenErrorListProvider((System.IServiceProvider) this);
    }

    public int FContinueMessageLoop(uint uReason, IntPtr pvLoopData, MSG[] pMsgPeeked)
    {
      return 1;
    }

    public int FDoIdle(uint grfidlef)
    {
      if (this.languageService != null)
        this.languageService.OnIdle(((int) grfidlef & 1) != 0);
      return 0;
    }

    public int FPreTranslateMessage(MSG[] pMsg)
    {
      return 0;
    }

    public int FQueryTerminate(int fPromptUser)
    {
      return 1;
    }

    public int FReserved1(uint dwReserved, uint message, IntPtr wParam, IntPtr lParam)
    {
      return 1;
    }

    public IntPtr HwndGetWindow(uint dwWhich, uint dwReserved)
    {
      return IntPtr.Zero;
    }

    public void OnActivationChange(IOleComponent pic, int fSameComponent, OLECRINFO[] pcrinfo, int fHostIsActivating, OLECHOSTINFO[] pchostinfo, uint dwReserved)
    {
    }

    public void OnAppActivate(int fActive, uint dwOtherThreadID)
    {
    }

    public void OnEnterState(uint uStateID, int fEnter)
    {
    }

    public void OnLoseActivation()
    {
    }

    public void Terminate()
    {
    }
  }
}
