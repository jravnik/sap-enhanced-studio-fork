﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.CustomerObjectReferences.CustomerObjectReferencesEditorFactory
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.CustomEditor;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension.CustomerObjectReferences
{
  [Guid("97AAA94D-D4AA-4A53-82BF-34C051CD69FA")]
  public sealed class CustomerObjectReferencesEditorFactory : CustomEditorFactory<CustomerObjectReferencesEditorPane>
  {
  }
}
