﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler.DirectoryEvent
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.IO;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler
{
  public class DirectoryEvent
  {
    private FileInfo _localFile;
    private WatcherChangeTypes _type;

    public DirectoryEvent(FileInfo localFile, WatcherChangeTypes type)
    {
      this._localFile = localFile;
      this._type = type;
    }

    public override string ToString()
    {
      return this._localFile.FullName + (object) this._type;
    }

    public override int GetHashCode()
    {
      return this.ToString().GetHashCode();
    }
  }
}
