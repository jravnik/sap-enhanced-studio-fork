﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.INTERNET_PER_CONN_OptionEnum
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

namespace SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper
{
  public enum INTERNET_PER_CONN_OptionEnum
  {
    INTERNET_PER_CONN_FLAGS = 1,
    INTERNET_PER_CONN_PROXY_SERVER = 2,
    INTERNET_PER_CONN_PROXY_BYPASS = 3,
    INTERNET_PER_CONN_AUTOCONFIG_URL = 4,
    INTERNET_PER_CONN_AUTODISCOVERY_FLAGS = 5,
    INTERNET_PER_CONN_AUTOCONFIG_SECONDARY_URL = 6,
    INTERNET_PER_CONN_AUTOCONFIG_RELOAD_DELAY_MINS = 7,
    INTERNET_PER_CONN_AUTOCONFIG_LAST_DETECT_TIME = 8,
    INTERNET_PER_CONN_AUTOCONFIG_LAST_DETECT_URL = 9,
    INTERNET_PER_CONN_FLAGS_UI = 10,
  }
}
