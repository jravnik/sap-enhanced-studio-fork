﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.XRepSynchronizerExtension
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Util;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System.Collections.Generic;
using System.IO;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  public static class XRepSynchronizerExtension
  {
    private static string strUpdateIndicator = "UpdateCase";
    private static string strNewCreationIndicator = "CreationCase";
    public const string strMashup = "Mashup";
    public const string strService = "Service";
    public const string strPortBinding = "PortBinding";

    public static void SyncSingleFile(this SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer proxy, string strXRepPathforFile)
    {
      string xrepProjectEntities = XRepMapper.GetInstance().GetLocalPathforXRepProjectEntities(strXRepPathforFile);
      SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer instance = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance();
      XRepHandler xrepHandler = new XRepHandler();
      string content = (string) null;
      IDictionary<string, string> attribs = (IDictionary<string, string>) null;
      FileInfo fileInfo = new FileInfo(xrepProjectEntities);
      IDictionary<string, string> cachedFileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getMashupCachedFileAttributes(xrepProjectEntities);
      xrepHandler.Read(strXRepPathforFile, out content, out attribs);
      if (attribs.ContainsKey(XRepSynchronizerExtension.strUpdateIndicator))
        attribs.Remove(XRepSynchronizerExtension.strUpdateIndicator);
      if (attribs.ContainsKey(XRepSynchronizerExtension.strNewCreationIndicator))
        attribs.Remove(XRepSynchronizerExtension.strNewCreationIndicator);
      string path = XRepMapper.GetInstance().GetProjectFolderOnDisk() + "/Mashups/".Replace("/", "\\");
      if (!Directory.Exists(path))
        Directory.CreateDirectory(path);
      string rootFolderOnDisk1 = XRepMapper.GetInstance().GetMashupRootFolderOnDisk();
      if (!Directory.Exists(rootFolderOnDisk1))
        Directory.CreateDirectory(rootFolderOnDisk1);
      string rootFolderOnDisk2 = XRepMapper.GetInstance().GetServiceRootFolderOnDisk();
      if (!Directory.Exists(rootFolderOnDisk2))
        Directory.CreateDirectory(rootFolderOnDisk2);
      string rootFolderOnDisk3 = XRepMapper.GetInstance().GetMashupPortBindingRootFolderOnDisk();
      if (!Directory.Exists(rootFolderOnDisk3))
        Directory.CreateDirectory(rootFolderOnDisk3);
      instance.EstablishMashupConfigurationHierarchyNode(false, "Mashup", false);
      instance.EstablishMashupConfigurationHierarchyNode(false, "Service", false);
      instance.EstablishMashupConfigurationHierarchyNode(false, "PortBinding", false);
      CopernicusExtensionPackage.Instance.MonitorMashupFolder();
      CopernicusExtensionPackage.Instance.MonitorServiceFolder();
      CopernicusExtensionPackage.Instance.MonitorPortBindingFolder();
      if (!fileInfo.Exists)
      {
        if (content != null)
        {
          IDictionary<string, string> fileAttributes = (IDictionary<string, string>) new Dictionary<string, string>();
          fileAttributes.Add(XRepSynchronizerExtension.strNewCreationIndicator, bool.TrueString);
          instance.StoreAttributesInmemory(xrepProjectEntities, fileAttributes);
          LocalFileHandler.CreateFileOnDisk(content, xrepProjectEntities, false);
          attribs[XRepSynchronizerExtension.strNewCreationIndicator] = bool.TrueString;
          instance.StoreAttributesInmemory(xrepProjectEntities, attribs);
        }
      }
      else if (!instance.IsFileUptoDate(attribs, cachedFileAttributes))
      {
        IDictionary<string, string> fileAttributes = (IDictionary<string, string>) new Dictionary<string, string>();
        fileAttributes.Add(XRepSynchronizerExtension.strUpdateIndicator, bool.TrueString);
        instance.StoreAttributesInmemory(xrepProjectEntities, fileAttributes);
        LocalFileHandler.CreateOrUpdateFileOnDisk(content, xrepProjectEntities, false);
        attribs[XRepSynchronizerExtension.strUpdateIndicator] = bool.TrueString;
        instance.StoreAttributesInmemory(fileInfo.FullName, attribs);
      }
      if (!fileInfo.Exists)
        return;
      LocalFileHandler.TagCopernicusFile(xrepProjectEntities);
    }
  }
}
