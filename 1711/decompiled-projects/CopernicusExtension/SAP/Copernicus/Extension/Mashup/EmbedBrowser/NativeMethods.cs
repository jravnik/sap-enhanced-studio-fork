﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.EmbedBrowser.NativeMethods
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace SAP.Copernicus.Extension.Mashup.EmbedBrowser
{
  internal static class NativeMethods
  {
    internal const int BN_CLICKED = 245;
    internal const int BM_CLICK = 245;
    internal const int WM_LBUTTONDOWN = 513;
    internal const int WM_LBUTTONUP = 514;
    internal const int QS_KEY = 1;
    internal const int QS_MOUSEMOVE = 2;
    internal const int QS_MOUSEBUTTON = 4;
    internal const int QS_POSTMESSAGE = 8;
    internal const int QS_TIMER = 16;
    internal const int QS_PAINT = 32;
    internal const int QS_SENDMESSAGE = 64;
    internal const int QS_HOTKEY = 128;
    internal const int QS_ALLPOSTMESSAGE = 256;
    internal const int QS_MOUSE = 6;
    internal const int QS_INPUT = 7;
    internal const int QS_ALLEVENTS = 191;
    internal const int QS_ALLINPUT = 255;
    internal const int Facility_Win32 = 7;
    internal const int WM_CLOSE = 16;
    internal const int WM_COMMAND = 273;
    internal const int S_FALSE = 1;
    internal const int S_OK = 0;
    internal const int IDOK = 1;
    internal const int IDCANCEL = 2;
    internal const int IDABORT = 3;
    internal const int IDRETRY = 4;
    internal const int IDIGNORE = 5;
    internal const int IDYES = 6;
    internal const int IDNO = 7;
    internal const int IDCLOSE = 8;
    internal const int IDHELP = 9;
    internal const int IDTRYAGAIN = 10;
    internal const int IDCONTINUE = 11;

    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr SetActiveWindow(IntPtr hwnd);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool SetForegroundWindow(IntPtr hWnd);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool BringWindowToTop([In] IntPtr hWnd);

    [DllImport("kernel32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool CloseHandle([In] IntPtr hObject);

    [DllImport("user32.dll")]
    internal static extern IntPtr GetForegroundWindow();

    [DllImport("user32.dll")]
    internal static extern uint SendMessage(IntPtr hWnd, uint Msg, uint wParam, IntPtr lParam);

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    internal static extern bool AttachThreadInput(uint idAttach, uint idAttachTo, bool attach);

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    internal static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    internal static extern uint GetCurrentThreadId();

    [DllImport("user32")]
    internal static extern int EnumChildWindows(IntPtr hwnd, NativeMethods.CallBack x, IntPtr y);

    [DllImport("user32")]
    internal static extern bool IsWindowVisible(IntPtr hDlg);

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    internal static extern IntPtr SetFocus(IntPtr hWnd);

    [DllImport("user32")]
    internal static extern int GetClassName(IntPtr hWnd, StringBuilder className, int stringLength);

    [DllImport("user32")]
    internal static extern int GetWindowText(IntPtr hWnd, StringBuilder className, int stringLength);

    [DllImport("user32", PreserveSig = false)]
    internal static extern bool EndDialog(IntPtr hDlg, int result);

    [DllImport("user32.dll", PreserveSig = false)]
    public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

    [DllImport("Kernel32")]
    internal static extern long GetLastError();

    internal static long HResultFromWin32(long error)
    {
      if (error <= 0L)
        return error;
      return error & (long) ushort.MaxValue | 458752L | 2147483648L;
    }

    public static bool IsSamePath(string file1, string file2)
    {
      if (file1 == null || file1.Length == 0)
      {
        if (file2 != null)
          return file2.Length == 0;
        return true;
      }
      Uri result1 = (Uri) null;
      Uri result2 = (Uri) null;
      try
      {
        if (!Uri.TryCreate(file1, UriKind.Absolute, out result1) || !Uri.TryCreate(file2, UriKind.Absolute, out result2))
          return false;
        if (result1 != (Uri) null && result1.IsFile && (result2 != (Uri) null && result2.IsFile))
          return 0 == string.Compare(result1.LocalPath, result2.LocalPath, StringComparison.OrdinalIgnoreCase);
        return file1 == file2;
      }
      catch (UriFormatException ex)
      {
        Trace.WriteLine("Exception " + ex.Message);
      }
      return false;
    }

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool CloseWindow([In] IntPtr hWnd);

    public static bool CloseWindowEx(int HWND)
    {
      return NativeMethods.CloseWindow((IntPtr) HWND);
    }

    [DllImport("kernel32.dll")]
    public static extern uint GetProcessId([In] IntPtr Process);

    public static uint GetWindowThreadProcessId(int HWND)
    {
      uint lpdwProcessId = 0;
      int windowThreadProcessId = (int) NativeMethods.GetWindowThreadProcessId((IntPtr) HWND, out lpdwProcessId);
      return lpdwProcessId;
    }

    internal delegate bool CallBack(IntPtr hwnd, IntPtr lParam);
  }
}
