﻿// Decompiled with JetBrains decompiler
// Type: SHDocVw.IShellUIHelper5
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SHDocVw
{
  [Guid("A2A08B09-103D-4D3F-B91C-EA455CA82EFA")]
  [TypeIdentifier]
  [CompilerGenerated]
  [ComImport]
  public interface IShellUIHelper5 : IShellUIHelper4, IShellUIHelper3, IShellUIHelper2, IShellUIHelper
  {
  }
}
