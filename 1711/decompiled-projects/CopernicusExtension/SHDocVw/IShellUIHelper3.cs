﻿// Decompiled with JetBrains decompiler
// Type: SHDocVw.IShellUIHelper3
// Assembly: CopernicusExtension, Version=142.0.3211.35, Culture=neutral, PublicKeyToken=null
// MVID: E47F3D0A-8EA6-4C7C-8863-CB854DA3C1D1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1711\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SHDocVw
{
  [CompilerGenerated]
  [TypeIdentifier]
  [Guid("528DF2EC-D419-40BC-9B6D-DCDBF9C1B25D")]
  [ComImport]
  public interface IShellUIHelper3 : IShellUIHelper2, IShellUIHelper
  {
  }
}
