﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser.HistoryRow
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Model.BusinessObject;
using System.Collections.Generic;

namespace CopernicusBusinessObjectBrowser
{
  public class HistoryRow
  {
    public List<string[]> resultTable { get; set; }

    public BusinessObject_Node actBoNode { get; set; }

    public HistoryRow.NavigateAttributes navigateAttributes { get; set; }

    public struct NavigateAttributes
    {
      public BusinessObject_Node associatedBONode { get; set; }

      public string boName { get; set; }

      public string boNodeName { get; set; }

      public string associationName { get; set; }

      public string[] nodeIDs { get; set; }
    }
  }
}
