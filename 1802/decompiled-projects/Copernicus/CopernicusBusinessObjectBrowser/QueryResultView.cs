﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser.QueryResultView
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using EnvDTE;
using EnvDTE80;
using SAP.Copernicus;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Model.BusinessObject;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace CopernicusBusinessObjectBrowser
{
  public class QueryResultView : UserControl
  {
    private List<HistoryRow> history = new List<HistoryRow>();
    private string lastSearch = "";
    private IContainer components;
    private ToolStrip toolStrip1BO;
    private ToolStripButton toolStripButtonBack;
    private ToolStripSeparator toolStripSeparator1;
    private SplitContainer splitContainer1;
    private Label subtitle;
    private ToolStripButton toolStripButton_Reopen;
    private ToolStripSeparator toolStripSeparator3;
    private ToolStripButton toolStripButton_singleRecordView;
    private ToolStripSeparator toolStripSeparator4;
    private ToolStripSeparator toolStripSeparator5;
    private DataGridView dataGridView1;
    private SplitContainer splitContainerOuter;
    private ToolStripTextBox toolStripComboBox_search;
    private ToolStripSplitButton toolStripSplitButtonNavigate;
    private ToolStripButton toolStripButtonSearch;
    private ToolStripButton toolStripButtonGenerateABSLCode;
    private ToolStripSeparator toolStripSeparator2;
    private CheckBox checkBoxOutputFormattingActive;
    private QuerySelection1 querySelectionScreen;
    private BusinessObject_Node actBoNode;
    private int lastSearchRowIndex;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (QueryResultView));
      DataGridViewCellStyle gridViewCellStyle1 = new DataGridViewCellStyle();
      DataGridViewCellStyle gridViewCellStyle2 = new DataGridViewCellStyle();
      DataGridViewCellStyle gridViewCellStyle3 = new DataGridViewCellStyle();
      this.toolStrip1BO = new ToolStrip();
      this.toolStripButtonBack = new ToolStripButton();
      this.toolStripSplitButtonNavigate = new ToolStripSplitButton();
      this.toolStripSeparator1 = new ToolStripSeparator();
      this.toolStripButton_Reopen = new ToolStripButton();
      this.toolStripSeparator3 = new ToolStripSeparator();
      this.toolStripButton_singleRecordView = new ToolStripButton();
      this.toolStripSeparator4 = new ToolStripSeparator();
      this.toolStripComboBox_search = new ToolStripTextBox();
      this.toolStripButtonSearch = new ToolStripButton();
      this.toolStripSeparator5 = new ToolStripSeparator();
      this.toolStripButtonGenerateABSLCode = new ToolStripButton();
      this.toolStripSeparator2 = new ToolStripSeparator();
      this.dataGridView1 = new DataGridView();
      this.splitContainer1 = new SplitContainer();
      this.subtitle = new Label();
      this.splitContainerOuter = new SplitContainer();
      this.toolStrip1BO.SuspendLayout();
      ((ISupportInitialize) this.dataGridView1).BeginInit();
      this.splitContainer1.BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.splitContainerOuter.BeginInit();
      this.splitContainerOuter.Panel1.SuspendLayout();
      this.splitContainerOuter.Panel2.SuspendLayout();
      this.splitContainerOuter.SuspendLayout();
      this.SuspendLayout();
      this.toolStrip1BO.BackColor = Color.FromArgb(188, 199, 217);
      this.toolStrip1BO.Dock = DockStyle.Fill;
      this.toolStrip1BO.Items.AddRange(new ToolStripItem[12]
      {
        (ToolStripItem) this.toolStripButtonBack,
        (ToolStripItem) this.toolStripSplitButtonNavigate,
        (ToolStripItem) this.toolStripSeparator1,
        (ToolStripItem) this.toolStripButton_Reopen,
        (ToolStripItem) this.toolStripSeparator3,
        (ToolStripItem) this.toolStripButton_singleRecordView,
        (ToolStripItem) this.toolStripSeparator4,
        (ToolStripItem) this.toolStripComboBox_search,
        (ToolStripItem) this.toolStripButtonSearch,
        (ToolStripItem) this.toolStripSeparator5,
        (ToolStripItem) this.toolStripButtonGenerateABSLCode,
        (ToolStripItem) this.toolStripSeparator2
      });
      this.toolStrip1BO.Location = new Point(0, 0);
      this.toolStrip1BO.Name = "toolStrip1BO";
      this.toolStrip1BO.Size = new Size(813, 31);
      this.toolStrip1BO.Stretch = true;
      this.toolStrip1BO.TabIndex = 10;
      this.toolStrip1BO.Text = "toolStrip1";
      this.toolStripButtonBack.BackColor = Color.Transparent;
      this.toolStripButtonBack.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonBack.Enabled = false;
      this.toolStripButtonBack.ForeColor = SystemColors.ControlText;
      this.toolStripButtonBack.Image = (Image) CopernicusResources.GoToPreviousHS;
      this.toolStripButtonBack.ImageTransparentColor = Color.Black;
      this.toolStripButtonBack.Margin = new Padding(0, 1, 1, 2);
      this.toolStripButtonBack.Name = "toolStripButtonBack";
      this.toolStripButtonBack.Size = new Size(23, 28);
      this.toolStripButtonBack.Text = "Back";
      this.toolStripButtonBack.Click += new EventHandler(this.toolStripButtonBack_Click);
      this.toolStripSplitButtonNavigate.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.toolStripSplitButtonNavigate.Enabled = false;
      this.toolStripSplitButtonNavigate.Image = (Image) componentResourceManager.GetObject("toolStripSplitButtonNavigate.Image");
      this.toolStripSplitButtonNavigate.ImageTransparentColor = Color.Magenta;
      this.toolStripSplitButtonNavigate.Name = "toolStripSplitButtonNavigate";
      this.toolStripSplitButtonNavigate.Size = new Size(150, 28);
      this.toolStripSplitButtonNavigate.Text = "Navigate by Association";
      this.toolStripSplitButtonNavigate.ButtonClick += new EventHandler(this.toolStripSplitButtonNavigate_ButtonClick);
      this.toolStripSplitButtonNavigate.DropDownItemClicked += new ToolStripItemClickedEventHandler(this.toolStripSplitButtonNavigate_DropDownItemClicked);
      this.toolStripSeparator1.BackColor = Color.Black;
      this.toolStripSeparator1.ForeColor = SystemColors.Control;
      this.toolStripSeparator1.Margin = new Padding(1, 0, 1, 0);
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new Size(6, 31);
      this.toolStripButton_Reopen.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.toolStripButton_Reopen.Image = (Image) componentResourceManager.GetObject("toolStripButton_Reopen.Image");
      this.toolStripButton_Reopen.ImageTransparentColor = Color.Magenta;
      this.toolStripButton_Reopen.Name = "toolStripButton_Reopen";
      this.toolStripButton_Reopen.Size = new Size(89, 28);
      this.toolStripButton_Reopen.Text = "Reopen Search";
      this.toolStripButton_Reopen.Click += new EventHandler(this.toolStripButton_Reopen_Click);
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new Size(6, 31);
      this.toolStripButton_singleRecordView.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.toolStripButton_singleRecordView.Image = (Image) componentResourceManager.GetObject("toolStripButton_singleRecordView.Image");
      this.toolStripButton_singleRecordView.ImageTransparentColor = Color.Magenta;
      this.toolStripButton_singleRecordView.Name = "toolStripButton_singleRecordView";
      this.toolStripButton_singleRecordView.Size = new Size(113, 28);
      this.toolStripButton_singleRecordView.Text = "Single-Record View";
      this.toolStripButton_singleRecordView.Click += new EventHandler(this.toolStripButton_singleRecordView_Click);
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      this.toolStripSeparator4.Size = new Size(6, 31);
      this.toolStripComboBox_search.Name = "toolStripComboBox_search";
      this.toolStripComboBox_search.Size = new Size(121, 31);
      this.toolStripComboBox_search.Text = "Search";
      this.toolStripComboBox_search.KeyDown += new KeyEventHandler(this.toolStripComboBox_search_KeyDown);
      this.toolStripButtonSearch.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.toolStripButtonSearch.Image = (Image) CopernicusResources.View1;
      this.toolStripButtonSearch.ImageTransparentColor = Color.Black;
      this.toolStripButtonSearch.Margin = new Padding(4, 1, 2, 2);
      this.toolStripButtonSearch.Name = "toolStripButtonSearch";
      this.toolStripButtonSearch.Size = new Size(23, 28);
      this.toolStripButtonSearch.Text = "Search";
      this.toolStripButtonSearch.Click += new EventHandler(this.toolStripButtonSearch_Click);
      this.toolStripSeparator5.Name = "toolStripSeparator5";
      this.toolStripSeparator5.Size = new Size(6, 31);
      this.toolStripButtonGenerateABSLCode.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.toolStripButtonGenerateABSLCode.Image = (Image) componentResourceManager.GetObject("toolStripButtonGenerateABSLCode.Image");
      this.toolStripButtonGenerateABSLCode.ImageTransparentColor = Color.Magenta;
      this.toolStripButtonGenerateABSLCode.Name = "toolStripButtonGenerateABSLCode";
      this.toolStripButtonGenerateABSLCode.Size = new Size(89, 28);
      this.toolStripButtonGenerateABSLCode.Text = "Generate Code";
      this.toolStripButtonGenerateABSLCode.Click += new EventHandler(this.toolStripButtonGenerateABSLCode_Click);
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new Size(6, 31);
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.AllowUserToOrderColumns = true;
      this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
      gridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleLeft;
      gridViewCellStyle1.BackColor = SystemColors.Control;
      gridViewCellStyle1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      gridViewCellStyle1.ForeColor = SystemColors.WindowText;
      gridViewCellStyle1.SelectionBackColor = SystemColors.Highlight;
      gridViewCellStyle1.SelectionForeColor = SystemColors.HighlightText;
      gridViewCellStyle1.WrapMode = DataGridViewTriState.True;
      this.dataGridView1.ColumnHeadersDefaultCellStyle = gridViewCellStyle1;
      this.dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      gridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
      gridViewCellStyle2.BackColor = SystemColors.Window;
      gridViewCellStyle2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      gridViewCellStyle2.ForeColor = SystemColors.ControlText;
      gridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
      gridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
      gridViewCellStyle2.WrapMode = DataGridViewTriState.False;
      this.dataGridView1.DefaultCellStyle = gridViewCellStyle2;
      this.dataGridView1.Dock = DockStyle.Fill;
      this.dataGridView1.EditMode = DataGridViewEditMode.EditProgrammatically;
      this.dataGridView1.Location = new Point(0, 0);
      this.dataGridView1.Name = "dataGridView1";
      gridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
      gridViewCellStyle3.BackColor = SystemColors.Control;
      gridViewCellStyle3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      gridViewCellStyle3.ForeColor = SystemColors.WindowText;
      gridViewCellStyle3.SelectionBackColor = SystemColors.Highlight;
      gridViewCellStyle3.SelectionForeColor = SystemColors.HighlightText;
      gridViewCellStyle3.WrapMode = DataGridViewTriState.True;
      this.dataGridView1.RowHeadersDefaultCellStyle = gridViewCellStyle3;
      this.dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      this.dataGridView1.Size = new Size(813, 486);
      this.dataGridView1.TabIndex = 11;
      this.dataGridView1.CellDoubleClick += new DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
      this.dataGridView1.RowHeaderMouseClick += new DataGridViewCellMouseEventHandler(this.dataGridView1_RowHeaderMouseClick);
      this.splitContainer1.Dock = DockStyle.Fill;
      this.splitContainer1.Location = new Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = Orientation.Horizontal;
      this.splitContainer1.Panel1.Controls.Add((Control) this.subtitle);
      this.splitContainer1.Panel2.Controls.Add((Control) this.toolStrip1BO);
      this.splitContainer1.Size = new Size(813, 80);
      this.splitContainer1.SplitterDistance = 45;
      this.splitContainer1.TabIndex = 12;
      this.subtitle.AutoSize = true;
      this.subtitle.Font = new Font("Microsoft Sans Serif", 14f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.subtitle.Location = new Point(3, 10);
      this.subtitle.Name = "subtitle";
      this.subtitle.Size = new Size(131, 24);
      this.subtitle.TabIndex = 0;
      this.subtitle.Text = "Query Result";
      this.splitContainerOuter.Dock = DockStyle.Fill;
      this.splitContainerOuter.Location = new Point(0, 0);
      this.splitContainerOuter.Name = "splitContainerOuter";
      this.splitContainerOuter.Orientation = Orientation.Horizontal;
      this.splitContainerOuter.Panel1.Controls.Add((Control) this.splitContainer1);
      this.splitContainerOuter.Panel1MinSize = 80;
      this.splitContainerOuter.Panel2.Controls.Add((Control) this.dataGridView1);
      this.splitContainerOuter.Size = new Size(813, 570);
      this.splitContainerOuter.SplitterDistance = 80;
      this.splitContainerOuter.TabIndex = 1;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.splitContainerOuter);
      this.Name = nameof (QueryResultView);
      this.Size = new Size(813, 570);
      this.toolStrip1BO.ResumeLayout(false);
      this.toolStrip1BO.PerformLayout();
      ((ISupportInitialize) this.dataGridView1).EndInit();
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel1.PerformLayout();
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.Panel2.PerformLayout();
      this.splitContainer1.EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.splitContainerOuter.Panel1.ResumeLayout(false);
      this.splitContainerOuter.Panel2.ResumeLayout(false);
      this.splitContainerOuter.EndInit();
      this.splitContainerOuter.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    public QueryResultView(List<string[]> queryResult, BusinessObject_Query boQuery, QuerySelection1 querySelectionScreen)
    {
      this.InitializeComponent();
      this.setupCheckbox();
      this.setupSubTitle(boQuery);
      this.actBoNode = boQuery.ToParent;
      this.setupResult(queryResult);
      this.dataGridView1.ClearSelection();
      this.history.Add(new HistoryRow()
      {
        resultTable = queryResult,
        actBoNode = this.actBoNode
      });
      this.querySelectionScreen = querySelectionScreen;
    }

    private void setupSubTitle(SAP.Copernicus.Model.BusinessObject.BusinessObject bo)
    {
      this.subtitle.Text = bo.SemanticalName;
    }

    private void setupSubTitle(BusinessObject_Node boNode)
    {
      this.subtitle.Text = boNode.ToParent.SemanticalName + ": " + boNode.Name;
    }

    private void setupSubTitle(BusinessObject_Query boQuery)
    {
      this.subtitle.Text = boQuery.ToParent.ToParent.SemanticalName + ": " + boQuery.ToParent.Name + ": " + boQuery.Name;
    }

    private void setupCheckbox()
    {
      this.checkBoxOutputFormattingActive = new CheckBox();
      this.checkBoxOutputFormattingActive.Text = "Output Formatting Active";
      this.checkBoxOutputFormattingActive.Checked = true;
      this.checkBoxOutputFormattingActive.CheckedChanged += new EventHandler(this.outputFormattingActive_CheckedChanged);
      this.toolStrip1BO.Items.Add((ToolStripItem) new ToolStripControlHost((Control) this.checkBoxOutputFormattingActive));
    }

    private void setupDropDownAssociations(BusinessObject_Node boNode)
    {
      this.toolStripSplitButtonNavigate.DropDownItems.Clear();
      List<BusinessObject_Association> list = boNode.Associations.ToList<BusinessObject_Association>();
      list.OrderBy<BusinessObject_Association, int>((Func<BusinessObject_Association, int>) (item => item.OrdinalNumberValue));
      foreach (BusinessObject_Association objectAssociation in list)
        this.toolStripSplitButtonNavigate.DropDownItems.Add((ToolStripItem) new ToolStripButton(objectAssociation.Name));
    }

    private void outputFormattingActive_CheckedChanged(object sender, EventArgs e)
    {
      if (this.history.Count == 1)
      {
        this.setupResult(this.querySelectionScreen.executeQuery(!this.checkBoxOutputFormattingActive.Checked));
      }
      else
      {
        HistoryRow historyRow = this.history.Last<HistoryRow>();
        this.actBoNode = historyRow.actBoNode;
        this.navigateByAssociation(historyRow.navigateAttributes.boName, historyRow.navigateAttributes.boNodeName, historyRow.navigateAttributes.associationName, historyRow.navigateAttributes.nodeIDs);
      }
    }

    private void setupResult(List<string[]> queryResult)
    {
      this.setupDropDownAssociations(this.actBoNode);
      this.dataGridView1.SelectAll();
      this.dataGridView1.ClearSelection();
      this.dataGridView1.Columns.Clear();
      if (queryResult.Count == 0)
        return;
      DataTable dataTable = new DataTable();
      for (int index = 0; index < queryResult[0].Length; ++index)
        dataTable.Columns.Add(queryResult[0][index]);
      for (int index = 1; index < queryResult.Count; ++index)
        dataTable.Rows.Add((object[]) queryResult[index]);
      this.dataGridView1.DataSource = (object) dataTable;
      this.dataGridView1.Columns[0].Visible = false;
      this.dataGridView1.AllowUserToResizeColumns = true;
      foreach (DataGridViewColumn column in (BaseCollection) this.dataGridView1.Columns)
      {
        column.Resizable = DataGridViewTriState.True;
        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
      }
      if (this.dataGridView1.RowCount == 0)
        return;
      this.dataGridView1.Rows[0].Selected = true;
      this.toolStripSplitButtonNavigate.Enabled = true;
    }

    private string[] getColumnsToShow(string[] row)
    {
      string[] strArray = new string[((IEnumerable<string>) row).Count<string>() - 1];
      for (int index = 0; index < ((IEnumerable<string>) row).Count<string>() - 1; ++index)
        strArray[index] = row[index + 1];
      return strArray;
    }

    private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
      this.showSingleRecordView(e.RowIndex);
    }

    private void showSingleRecordView(int rowIndex)
    {
      if (rowIndex == -1)
        return;
      string[][] table = new string[this.dataGridView1.Columns.Count - 1][];
      for (int index = 1; index < this.dataGridView1.Columns.Count; ++index)
      {
        string[] strArray = new string[2]
        {
          this.dataGridView1.Columns[index].Name,
          this.dataGridView1.Rows[rowIndex].Cells[index].Value.ToString()
        };
        table[index - 1] = strArray;
      }
      int num = (int) new SingleRecordView(table).ShowDialog();
    }

    private int getOptimalComboBoxDropDownWidth(ComboBox cbEdit)
    {
      int num1 = cbEdit.DropDownWidth;
      Graphics graphics = cbEdit.CreateGraphics();
      Font font = cbEdit.Font;
      int num2 = cbEdit.Items.Count > cbEdit.MaxDropDownItems ? SystemInformation.VerticalScrollBarWidth : 0;
      foreach (object obj in cbEdit.Items)
      {
        int num3 = (int) graphics.MeasureString(obj.ToString(), font).Width + num2;
        if (num1 < num3)
          num1 = num3;
      }
      return num1;
    }

    private void navigateByAssociation(string boName, string boNodeName, string associationName, string[] nodeIDs)
    {
      RepositoryQueryHandler repositoryQueryHandler = new RepositoryQueryHandler();
      char intern = ' ';
      if (!this.checkBoxOutputFormattingActive.Checked)
        intern = 'X';
      string resultBase64 = repositoryQueryHandler.navigate_by_Association(boName, boNodeName, associationName, nodeIDs, intern);
      BusinessObject_Association objectAssociation = this.actBoNode.Associations.Find((Predicate<BusinessObject_Association>) (item => item.Key.ProxyName == associationName));
      this.setupSubTitle(objectAssociation.TargetNode);
      List<string[]> queryResult = QuerySelection1.parseQueryResult(resultBase64, objectAssociation.TargetNode, !this.checkBoxOutputFormattingActive.Checked);
      this.setupResult(queryResult);
      this.saveState(queryResult, boName, boNodeName, objectAssociation.TargetNode, associationName, nodeIDs);
      this.actBoNode = objectAssociation.TargetNode;
      this.setupDropDownAssociations(this.actBoNode);
    }

    private void saveState(List<string[]> result, string boName, string boNodeName, BusinessObject_Node targetNode, string associationName, string[] nodeIDs)
    {
      if (this.history.Count - this.history.Count<HistoryRow>((Func<HistoryRow, bool>) (item => item.resultTable == null)) > 9)
        this.history.First<HistoryRow>().resultTable = (List<string[]>) null;
      this.history.Add(new HistoryRow()
      {
        resultTable = result,
        actBoNode = this.actBoNode,
        navigateAttributes = new HistoryRow.NavigateAttributes()
        {
          boName = boName,
          boNodeName = boNodeName,
          associatedBONode = targetNode,
          associationName = associationName,
          nodeIDs = nodeIDs
        }
      });
      if (this.history.Count <= 0)
        return;
      this.toolStripButtonBack.Enabled = true;
    }

    private void toolStripButton_singleRecordView_Click(object sender, EventArgs e)
    {
      if (this.dataGridView1.CurrentRow == null)
        return;
      this.showSingleRecordView(this.dataGridView1.CurrentRow.Index);
    }

    private void findInDataGridView(string searchText, int startRow)
    {
      this.dataGridView1.MultiSelect = true;
      this.dataGridView1.ClearSelection();
      bool flag = false;
      for (int index = startRow; index < this.dataGridView1.RowCount; ++index)
      {
        foreach (DataGridViewCell cell in (BaseCollection) this.dataGridView1.Rows[index].Cells)
        {
          if (cell.Value.ToString().ToUpper().Contains(searchText.ToUpper()))
          {
            this.dataGridView1.FirstDisplayedScrollingRowIndex = cell.RowIndex;
            cell.Selected = true;
            flag = true;
            break;
          }
        }
        if (flag)
        {
          this.lastSearch = searchText;
          this.lastSearchRowIndex = index;
          break;
        }
      }
    }

    private void toolStripSplitButtonNavigate_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {
      if (this.dataGridView1.SelectedRows == null)
      {
        int num1 = (int) CopernicusMessageBox.Show("Please select a Row!");
      }
      else if (this.checkIfDebuggerIsActive())
      {
        int num2 = (int) CopernicusMessageBox.Show("The query cannot be executed because the debugger is active.");
      }
      else
      {
        string associationName = e.ClickedItem.Text;
        string proxyName = this.actBoNode.Associations.Find((Predicate<BusinessObject_Association>) (item => item.Name == associationName)).Key.ProxyName;
        List<string> stringList = new List<string>();
        foreach (DataGridViewRow selectedRow in (BaseCollection) this.dataGridView1.SelectedRows)
          stringList.Add(selectedRow.Cells[0].Value.ToString());
        string boNodeName = this.actBoNode.Key.ProxyName;
        if (this.actBoNode.Key.DependentObjectPathPrefixName != null && !this.actBoNode.Key.DependentObjectPathPrefixName.Equals(""))
          boNodeName = this.actBoNode.Key.DependentObjectPathPrefixName + "." + boNodeName;
        this.navigateByAssociation(this.actBoNode.ToParent.Key.ProxyName, boNodeName, proxyName, stringList.ToArray());
      }
    }

    private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      this.toolStripSplitButtonNavigate.Enabled = true;
    }

    private void toolStripButtonSearch_Click(object sender, EventArgs e)
    {
      this.search();
    }

    private void search()
    {
      string text = this.toolStripComboBox_search.Text;
      int startRow = 0;
      if (this.lastSearch == text)
      {
        startRow = this.lastSearchRowIndex + 1;
        if (startRow >= this.dataGridView1.RowCount)
          startRow = 0;
      }
      this.findInDataGridView(text, startRow);
    }

    private void toolStripComboBox_search_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode != Keys.Return)
        return;
      this.search();
    }

    private void toolStripButtonBack_Click(object sender, EventArgs e)
    {
      HistoryRow historyRow = this.history[this.history.Count - 2];
      this.history.RemoveAt(this.history.Count - 1);
      if (this.history.Count == 1)
        this.toolStripButtonBack.Enabled = false;
      if (historyRow.resultTable != null)
      {
        this.splitContainerOuter.Panel2.Controls.Remove((Control) this.dataGridView1);
        this.splitContainerOuter.Panel2.Controls.Add((Control) this.dataGridView1);
        this.dataGridView1.Refresh();
        this.actBoNode = historyRow.navigateAttributes.associatedBONode;
        if (this.history.Count == 1)
        {
          this.actBoNode = historyRow.actBoNode;
          this.setupResult(historyRow.resultTable);
          this.setupSubTitle(this.querySelectionScreen.query);
        }
        else
        {
          this.setupResult(historyRow.resultTable);
          this.setupSubTitle(this.actBoNode);
        }
        this.setupDropDownAssociations(this.actBoNode);
      }
      else
      {
        this.actBoNode = historyRow.navigateAttributes.associatedBONode;
        if (this.history.Count == 1)
        {
          this.actBoNode = historyRow.actBoNode;
          this.setupResult(this.querySelectionScreen.executeQuery(!this.checkBoxOutputFormattingActive.Checked));
          this.toolStripButtonBack.Enabled = false;
          this.setupSubTitle(this.querySelectionScreen.query);
        }
        else
        {
          this.navigateByAssociation(historyRow.navigateAttributes.boName, historyRow.navigateAttributes.boNodeName, historyRow.navigateAttributes.associationName, historyRow.navigateAttributes.nodeIDs);
          this.history.Remove(historyRow);
        }
      }
      this.toolStripSplitButtonNavigate.Enabled = false;
      this.dataGridView1.ClearSelection();
      this.Refresh();
    }

    private void toolStripButton_Reopen_Click(object sender, EventArgs e)
    {
      List<string[]> queryResult = this.querySelectionScreen.ShowDialog();
      if (this.querySelectionScreen.cancelled)
      {
        this.querySelectionScreen.cancelled = false;
      }
      else
      {
        this.history.Clear();
        this.toolStripSplitButtonNavigate.Enabled = false;
        this.toolStripButtonBack.Enabled = false;
        this.setupResult(queryResult);
        this.actBoNode = this.querySelectionScreen.query.ToParent;
        this.setupSubTitle(this.querySelectionScreen.query);
        this.history.Add(new HistoryRow()
        {
          resultTable = queryResult,
          actBoNode = this.actBoNode
        });
      }
    }

    private void toolStripSplitButtonNavigate_ButtonClick(object sender, EventArgs e)
    {
      this.toolStripSplitButtonNavigate.ShowDropDown();
    }

    private bool checkIfDebuggerIsActive()
    {
      return (DTEUtil.GetDTE().Debugger as Debugger2).CurrentMode != dbgDebugMode.dbgDesignMode;
    }

    private void toolStripButtonGenerateABSLCode_Click(object sender, EventArgs e)
    {
      new ABSLCodeView(this.generateABSLQuery(this.actBoNode.ToParent.Key.ProxyName, this.actBoNode.ToParent.NameKey.Name, this.actBoNode.Name, this.querySelectionScreen.query.Name, this.querySelectionScreen.selectionParametersForABSL.ToArray())).Show();
    }

    private string generateABSLQuery(string boProxyName, string boName, string boNodeName, string queryName, SESF_SELECTION_PARAMETER[] selectionParameters)
    {
      string str1 = "import ABSL;\n";
      string namespaceForBoProxyName = RepositoryDataCache.GetInstance().GetNamespaceForBOProxyName(boProxyName);
      string str2 = "";
      if (CopernicusProjectSystemUtil.getSelectedProject() != null)
        str2 = CopernicusProjectSystemUtil.GetNamespaceForSelectedProject();
      if (namespaceForBoProxyName != null && namespaceForBoProxyName != "" && namespaceForBoProxyName != str2)
      {
        string str3 = namespaceForBoProxyName.Replace("http://sap.com/xi/", "").Replace("http://", "").Replace("/", ".");
        str1 = str1 + "import " + str3 + ";\n";
      }
      boNodeName = !boNodeName.Equals("Root", StringComparison.CurrentCultureIgnoreCase) ? boNodeName + "." : "";
      string str4 = str1 + "var query = " + boName + "." + boNodeName + queryName + ";\n" + "var resultData = query.ExecuteDataOnly();\n" + "// 2. Selection\n" + "var selectionParams = query.CreateSelectionParams();\n";
      foreach (SESF_SELECTION_PARAMETER selectionParameter in selectionParameters)
      {
        string attributeName = selectionParameter.ATTRIBUTE_NAME;
        if (selectionParameter.OPTION.Equals("bt", StringComparison.InvariantCultureIgnoreCase))
          str4 = str4 + "selectionParams.Add(query." + attributeName + ", \"" + (object) selectionParameter.SIGN[0] + "\", \"" + selectionParameter.OPTION + "\", \"" + selectionParameter.LOW + "\", \"" + selectionParameter.HIGH + "\");\n";
        else
          str4 = str4 + "selectionParams.Add(query." + attributeName + ", \"" + (object) selectionParameter.SIGN[0] + "\", \"" + selectionParameter.OPTION + "\", \"" + selectionParameter.LOW + "\");\n";
      }
      return str4 + "// Result\n" + " resultData = query.ExecuteDataOnly(selectionParams);\n";
    }
  }
}
