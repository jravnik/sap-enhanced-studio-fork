﻿// Decompiled with JetBrains decompiler
// Type: CompareOperands
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

public class CompareOperands
{
  public const string eq = "Equal to";
  public const string ge = "Greater Than or Equal to";
  public const string gt = "Greater Than";
  public const string le = "Less Than or Equal to";
  public const string lt = "Less Than";
  public const string bt = "Between";
  public const string cp = "Contains Pattern";

  public static string[] getAll()
  {
    return new string[7]
    {
      "Equal to",
      "Between",
      "Contains Pattern",
      "Less Than",
      "Less Than or Equal to",
      "Greater Than",
      "Greater Than or Equal to"
    };
  }

  public static string getAcronym(string txt)
  {
    switch (txt)
    {
      case "Equal to":
        return "EQ";
      case "Greater Than or Equal to":
        return "GE";
      case "Greater Than":
        return "GT";
      case "Less Than or Equal to":
        return "LE";
      case "Less Than":
        return "LT";
      case "Between":
        return "BT";
      case "Contains Pattern":
        return "CP";
      default:
        return "";
    }
  }
}
