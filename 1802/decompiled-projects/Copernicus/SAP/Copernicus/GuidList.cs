﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.GuidList
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using System;

namespace SAP.Copernicus
{
  internal static class GuidList
  {
    public static readonly Guid guidCopernicusCmdSet = new Guid("4b077f76-2fc9-498b-b023-ebb4f61bc505");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetBCONode = new Guid("48FE391B-E64C-49E6-ADBC-7B3CADA872CC");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetBONode = new Guid("9d9046da-94f8-4fd0-8a00-92bf4f6defa8");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetPINode = new Guid("A1BFC054-5938-42c0-98E0-F0BE08B4280C");
    public static readonly Guid UICONTEXT_boFileSelected = new Guid("203116D4-FC70-48d8-A4E8-2467F58B1F65");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetXBONode = new Guid("638B1F16-90F7-4543-AC75-6A09689EC108");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetScriptNode = new Guid("AF01EB15-9782-4F6C-BE8A-0EC999EED0D9");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetXODataNode = new Guid("105CC2F3-1522-4ED7-8408-32F788C128B2");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetXFMTNode = new Guid("26FC47AD-0140-4873-9A9F-441B70B31D44");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetUINode = new Guid("8128809F-8341-452f-8F5B-26929C2A66EA");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetEditScreen = new Guid("18BB5238-EBF5-4ee8-B2F2-455CF13D71A4");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetEmbComponentNode = new Guid("EFDE5F2E-4578-4959-898B-83C438546635");
    public static readonly Guid UICONTEXT_UIFileSelected = new Guid("3180F7A0-E23D-4330-965E-D7EDA36F2215");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetFormNode = new Guid("39D7548B-0184-44be-888D-8F506619F828");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetOpenFormNode = new Guid("894FB38D-8441-4346-9011-5E78514C240A");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetEditableNode = new Guid("D7518087-043C-4050-90F3-D346D7429EB7");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetProjectNode = new Guid("2D9A4FD8-5568-4dda-B43A-8E30B1E1F7A8");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetKeyUser = new Guid("E6551A27-982C-4db0-9FF0-0BAC3E0398D9");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetAccessRights = new Guid("23B73873-2E8D-45E2-8E11-ED35250D922E");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetApprovalNode = new Guid("F2E629DE-721A-45FB-BE4A-67542A61156F");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetMDRONode = new Guid("68fa9e35-1fc3-4158-a117-8106510b68f4");
    public static readonly Guid guidcmdSetBCImplProjectTempl = new Guid("64576903-2003-4A69-A93D-2262A6A32358");
    public static readonly Guid guidcmdSetHelpMenuSCN = new Guid("332C487A-79BA-4DDD-9463-A1190D5D17BE");
    public static readonly Guid guidcmdSetHelpMenuCheckForUpdate = new Guid("d68eccf3-27e0-4d07-ad3b-740645503619");
    public static readonly Guid guidcmdSetHelpMenuAbout = new Guid("478582A8-85CD-4D3B-B6D2-006D9E88A516");
    public static readonly Guid guidcmdSetHelpMenuBydStudio = new Guid("AC9729E9-9AA6-4b6f-A30B-89630BA23E19");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetTranslationNode = new Guid("8dd7bc6f-6f93-4a1d-a363-f8f95e8c724e");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetPCDNode = new Guid("9B3F4240-5472-4808-9E70-6934C45C38F3");
    public static readonly Guid guidSAPMenuEntryCmdSet = new Guid("05952C16-04D0-494D-B9F1-2D49742A65C0");
    public static readonly Guid guidABSLDebuggerCmdSet = new Guid("8F7CABC8-54E8-4edb-AD8E-59EF2E3C97C7");
    public static readonly Guid guidABSLSignatureAdornmentCmdSet = new Guid("FB6C00C5-1C21-4C41-8AB8-DC9654C09AE2");
    public static readonly Guid guidCSaveActivateCmdSet = new Guid("631b5099-7bcc-484b-8e52-08e4a840eb3a");
    public static readonly Guid guidcmdidShowSolExpCmdSet = new Guid("631b5099-7bcc-484b-8e52-08e4a840ea5a");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetCSDNode = new Guid("90300cb5-d578-4a73-88f4-c6b0176213de");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetWSIDNode = new Guid("E09503B1-AFEF-46A7-8BFA-D0811D189101");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetOpenKeyUserUI = new Guid("B179EF19-4E7A-4D9E-9FC3-DC3FA36D5217");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetWEBSRVNode = new Guid("07612E61-976C-4206-9520-6F39B57D419F");
    public static readonly Guid guidcmdSetGenericActionCmdSet = new Guid("4FCD6030-E625-4b74-BEF8-54609B992A30");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetGenericActivate = new Guid("0364B794-DF1B-44b7-B1D4-B6E9C065CABC");
    public static readonly Guid guidDynamicMenuDevelopmentCmdSetCustomReuseLibrary = new Guid("041950B4-EB85-4D72-9090-00F36335D914");
    public const string guidcmdSetBCImplProjectTemplString = "64576903-2003-4A69-A93D-2262A6A32358";
    public const string guidCopernicusPkgString = "a9ad806d-b284-4c78-ac4b-2b7781dd894a";
    public const string guidCopernicusCmdSetString = "4b077f76-2fc9-498b-b023-ebb4f61bc505";
    public const string guidToolWindowPersistanceString = "0128a988-1402-4c49-a09b-df226597dc6c";
    public const string guidDynamicMenuDevelopmentCmdSetBCONodeString = "48FE391B-E64C-49E6-ADBC-7B3CADA872CC";
    public const string guidDynamicMenuDevelopmentCmdSetBONodeString = "9d9046da-94f8-4fd0-8a00-92bf4f6defa8";
    public const string guidDynamicMenuDevelopmentCmdSetPINodeString = "A1BFC054-5938-42c0-98E0-F0BE08B4280C";
    public const string guidDynamicMenuDevelopmentCmdSetXBONodeString = "638B1F16-90F7-4543-AC75-6A09689EC108";
    public const string guidDynamicMenuDevelopmentCmdSetScriptNodeString = "AF01EB15-9782-4F6C-BE8A-0EC999EED0D9";
    public const string guidDynamicMenuDevelopmentCmdSetXODataNodeString = "105CC2F3-1522-4ED7-8408-32F788C128B2";
    public const string guidDynamicMenuDevelopmentCmdSetXFMTNodeString = "26FC47AD-0140-4873-9A9F-441B70B31D44";
    public const string guidDynamicMenuDevelopmentCmdSetEditableNodeString = "D7518087-043C-4050-90F3-D346D7429EB7";
    public const string guidDynamicMenuDevelopmentCmdSetProjectNodeString = "2D9A4FD8-5568-4dda-B43A-8E30B1E1F7A8";
    public const string guidDynamicMenuDevelopmentCmdSetSolutionNodeString = "649A3088-FCF1-4f3d-A949-DBAD67A3D033";
    public const string guidDynamicMenuDevelopmentCmdSetKeyUserString = "E6551A27-982C-4db0-9FF0-0BAC3E0398D9";
    public const string guidDynamicMenuDevelopmentCmdSetAccessRightsString = "23B73873-2E8D-45E2-8E11-ED35250D922E";
    public const string guidDynamicMenuDevelopmentCmdSetEmbComponentNodeString = "EFDE5F2E-4578-4959-898B-83C438546635";
    public const string UICONTEXT_boFileSelectedString = "203116D4-FC70-48d8-A4E8-2467F58B1F65";
    public const string guidDynamicMenuDevelopmentCmdSetUINodeString = "8128809F-8341-452f-8F5B-26929C2A66EA";
    public const string guidDynamicMenuDevelopmentCmdSetEditScreenString = "18BB5238-EBF5-4ee8-B2F2-455CF13D71A4";
    public const string UICONTEXT_UIFileSelectedString = "3180F7A0-E23D-4330-965E-D7EDA36F2215";
    public const string guidDynamicMenuDevelopmentCmdSetFormNodeString = "39D7548B-0184-44be-888D-8F506619F828";
    public const string guidDynamicMenuDevelopmentCmdSetOpenFormNodeString = "894FB38D-8441-4346-9011-5E78514C240A";
    public const string guidDynamicMenuDevelopmentCmdSetOpenApprovalNodeString = "F2E629DE-721A-45FB-BE4A-67542A61156F";
    public const string guidDynamicMenuDevelopmentCmdSetMDRONodeString = "68fa9e35-1fc3-4158-a117-8106510b68f4";
    public const string guidcmdSetHelpMenuSCNString = "332C487A-79BA-4DDD-9463-A1190D5D17BE";
    public const string guidcmdSetHelpMenuCheckForUpdateString = "d68eccf3-27e0-4d07-ad3b-740645503619";
    public const string guidcmdSetHelpMenuAboutString = "478582A8-85CD-4D3B-B6D2-006D9E88A516";
    public const string guidcmdSetHelpMenuBydStudioString = "AC9729E9-9AA6-4b6f-A30B-89630BA23E19";
    public const string guidDynamicMenuDevelopmentCmdSetTranslationString = "8dd7bc6f-6f93-4a1d-a363-f8f95e8c724e";
    public const string guidDynamicMenuDevelopmentCmdSetPCDString = "9B3F4240-5472-4808-9E70-6934C45C38F3";
    public const string guidSAPMenuEntryString = "05952C16-04D0-494D-B9F1-2D49742A65C0";
    public const string guidABSLDebuggerCmdSetString = "8F7CABC8-54E8-4edb-AD8E-59EF2E3C97C7";
    public const string guidABSLSignatureAdornmentCmdSetString = "FB6C00C5-1C21-4C41-8AB8-DC9654C09AE2";
    public const string guidcmdidSaveActString = "631b5099-7bcc-484b-8e52-08e4a840eb3a";
    public const string guidcmdidShowSolExpString = "631b5099-7bcc-484b-8e52-08e4a840ea5a";
    public const string guidCopernicusCSDCmdSetString = "90300cb5-d578-4a73-88f4-c6b0176213de";
    public const string guidCopernicusWEBSRVCmdSetString = "07612E61-976C-4206-9520-6F39B57D419F";
    public const string guidCopernicusWSIDCmdSetString = "E09503B1-AFEF-46A7-8BFA-D0811D189101";
    public const string guidCopernicusOpenKeyUserCmdSetString = "B179EF19-4E7A-4D9E-9FC3-DC3FA36D5217";
    public const string guidcmdSetGenericActionCmdSetString = "4FCD6030-E625-4b74-BEF8-54609B992A30";
    public const string guidDynamicMenuDevelopmentCmdSetGenericActivateString = "0364B794-DF1B-44b7-B1D4-B6E9C065CABC";
    public const string guidDynamicMenuDevelopmentCmdSetCustomReuseLibraryString = "041950B4-EB85-4D72-9090-00F36335D914";
  }
}
