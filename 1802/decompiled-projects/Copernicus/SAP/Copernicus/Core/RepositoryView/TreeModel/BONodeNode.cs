﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BONodeNode
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class BONodeNode : BaseNode
  {
    private string boNodeName;
    private string nodeProxyName;
    private string boProxyName;

    public BONodeNode(RepositoryDataSet.NodesRow nodeRow)
      : base((DataRow) nodeRow)
    {
      this.boNodeName = nodeRow.Name;
      this.nodeProxyName = nodeRow.ProxyName;
      this.boProxyName = nodeRow.BOProxyName;
      this.Text = this.boNodeName;
      this.ImageIndex = 7;
      this.SelectedImageIndex = 7;
    }

    public override string NodeName
    {
      get
      {
        return this.boNodeName;
      }
    }

    public string NodeProxyName
    {
      get
      {
        return this.nodeProxyName;
      }
    }

    public string BOProxyName
    {
      get
      {
        return this.boProxyName;
      }
    }

    public RepositoryDataSet.NodesRow getData()
    {
      return (RepositoryDataSet.NodesRow) this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.BONode;
    }
  }
}
