﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.EnhancementOptionNode
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class EnhancementOptionNode : BaseNode
  {
    private string enhancementOptionName;

    public EnhancementOptionNode(RepositoryDataSet.BAdIsRow dtRow)
      : base((DataRow) dtRow)
    {
      this.enhancementOptionName = dtRow.Name;
      this.Text = this.enhancementOptionName;
      this.ImageIndex = 13;
      this.SelectedImageIndex = 13;
    }

    public RepositoryDataSet.BAdIsRow getData()
    {
      return (RepositoryDataSet.BAdIsRow) this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.EnhancementOption;
    }
  }
}
