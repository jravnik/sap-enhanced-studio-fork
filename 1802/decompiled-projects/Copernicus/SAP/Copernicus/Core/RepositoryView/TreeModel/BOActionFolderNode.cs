﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BOActionFolderNode
// Assembly: Copernicus, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 73555534-EA3D-475A-95B7-056A84F2FF8F
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\Copernicus.dll

using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class BOActionFolderNode : BaseNode
  {
    public string nodeProxyName;
    public string nodeName;

    public BOActionFolderNode(string nodeProxyName, string nodeName)
      : this((DataRow) null)
    {
      this.nodeProxyName = nodeProxyName;
      this.nodeName = nodeName;
      this.ImageIndex = 5;
      this.SelectedImageIndex = 5;
    }

    public BOActionFolderNode(DataRow dataRow)
      : base(dataRow)
    {
      this.Text = CopernicusResources.TreeNodeTextActionFolder;
      this.ImageIndex = 5;
      this.SelectedImageIndex = 5;
    }

    public override NodeType getNodeType()
    {
      return NodeType.ActionFolder;
    }
  }
}
