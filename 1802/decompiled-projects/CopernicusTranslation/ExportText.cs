﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Translation.ExportText
// Assembly: CopernicusTranslation, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 9D6A50C9-F2FE-4B16-A45E-C445F353581B
// Assembly location: G:\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusTranslation.dll

using SAP.Copernicus.Core;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Translation.Resources;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace SAP.Copernicus.Translation
{
  public class ExportText : BaseForm
  {
    private List<string> xRepPathlist = new List<string>();
    private List<KeyValuePair<string, string>> LangComboList = new List<KeyValuePair<string, string>>();
    private new IContainer components;
    private GroupBox ExportSelectLangGrp;
    private Label ExportSelectLbl;
    private ComboBox ExportLangCmbbox;
    private GroupBox ExportBrowsegrp;
    private Button ExportBrowseButton;
    private Label ExportLocationTxt;
    private TextBox ExportPathTextBox;
    private Label label1;
    private TextBox ExportFileNameTextbox;
    private Label label2;
    private Label label4;
    private Label label3;
    private Label label6;
    private Label label7;
    private Label StandardTranslationLbl;
    private CheckBox StandardTranslationCheckBox;
    private Label labelInfoIconAlternativeKey;
    private ImageList imageList;
    private Label labelInfoIconStandardTranslation;
    private ToolTip listToolTip;
    private ZLANG_STRUC[] LanguageArray;
    private string fileName;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (ExportText));
      this.ExportSelectLangGrp = new GroupBox();
      this.labelInfoIconStandardTranslation = new Label();
      this.imageList = new ImageList(this.components);
      this.labelInfoIconAlternativeKey = new Label();
      this.StandardTranslationCheckBox = new CheckBox();
      this.StandardTranslationLbl = new Label();
      this.label7 = new Label();
      this.label6 = new Label();
      this.label2 = new Label();
      this.ExportSelectLbl = new Label();
      this.ExportLangCmbbox = new ComboBox();
      this.ExportBrowsegrp = new GroupBox();
      this.label4 = new Label();
      this.label3 = new Label();
      this.label1 = new Label();
      this.ExportLocationTxt = new Label();
      this.ExportBrowseButton = new Button();
      this.ExportFileNameTextbox = new TextBox();
      this.ExportPathTextBox = new TextBox();
      this.listToolTip = new ToolTip(this.components);
      this.ExportSelectLangGrp.SuspendLayout();
      this.ExportBrowsegrp.SuspendLayout();
      this.SuspendLayout();
      this.buttonCancel.Location = new Point(443, 348);
      this.buttonOk.Location = new Point(362, 348);
      this.ExportSelectLangGrp.Controls.Add((Control) this.labelInfoIconStandardTranslation);
      this.ExportSelectLangGrp.Controls.Add((Control) this.labelInfoIconAlternativeKey);
      this.ExportSelectLangGrp.Controls.Add((Control) this.StandardTranslationCheckBox);
      this.ExportSelectLangGrp.Controls.Add((Control) this.StandardTranslationLbl);
      this.ExportSelectLangGrp.Controls.Add((Control) this.label7);
      this.ExportSelectLangGrp.Controls.Add((Control) this.label6);
      this.ExportSelectLangGrp.Controls.Add((Control) this.label2);
      this.ExportSelectLangGrp.Controls.Add((Control) this.ExportSelectLbl);
      this.ExportSelectLangGrp.Controls.Add((Control) this.ExportLangCmbbox);
      this.ExportSelectLangGrp.Location = new Point(12, 77);
      this.ExportSelectLangGrp.Name = "ExportSelectLangGrp";
      this.ExportSelectLangGrp.Size = new Size(518, 111);
      this.ExportSelectLangGrp.TabIndex = 0;
      this.ExportSelectLangGrp.TabStop = false;
      this.ExportSelectLangGrp.Text = "Languages";
      this.labelInfoIconStandardTranslation.ImageIndex = 0;
      this.labelInfoIconStandardTranslation.ImageList = this.imageList;
      this.labelInfoIconStandardTranslation.ImeMode = ImeMode.NoControl;
      this.labelInfoIconStandardTranslation.Location = new Point(285, 71);
      this.labelInfoIconStandardTranslation.Name = "labelInfoIconStandardTranslation";
      this.labelInfoIconStandardTranslation.Size = new Size(23, 34);
      this.labelInfoIconStandardTranslation.TabIndex = 61;
      this.listToolTip.SetToolTip((Control) this.labelInfoIconStandardTranslation, TranslationResource.MsgStandardTranslationInfo);
      this.labelInfoIconStandardTranslation.Click += new EventHandler(this.infoBtnClick);
      this.imageList.ImageStream = (ImageListStreamer) componentResourceManager.GetObject("imageList.ImageStream");
      this.imageList.TransparentColor = Color.Transparent;
      this.imageList.Images.SetKeyName(0, "information_mark.png");
      this.labelInfoIconAlternativeKey.ImageIndex = 0;
      this.labelInfoIconAlternativeKey.ImeMode = ImeMode.NoControl;
      this.labelInfoIconAlternativeKey.Location = new Point(256, 71);
      this.labelInfoIconAlternativeKey.Name = "labelInfoIconAlternativeKey";
      this.labelInfoIconAlternativeKey.Size = new Size(23, 23);
      this.labelInfoIconAlternativeKey.TabIndex = 61;
      this.StandardTranslationCheckBox.AutoSize = true;
      this.StandardTranslationCheckBox.Checked = true;
      this.StandardTranslationCheckBox.CheckState = CheckState.Checked;
      this.StandardTranslationCheckBox.Location = new Point(162, 80);
      this.StandardTranslationCheckBox.Name = "StandardTranslationCheckBox";
      this.StandardTranslationCheckBox.Size = new Size(15, 14);
      this.StandardTranslationCheckBox.TabIndex = 6;
      this.StandardTranslationCheckBox.UseVisualStyleBackColor = true;
      this.StandardTranslationLbl.AutoSize = true;
      this.StandardTranslationLbl.Location = new Point(14, 80);
      this.StandardTranslationLbl.Name = "StandardTranslationLbl";
      this.StandardTranslationLbl.Size = new Size(130, 13);
      this.StandardTranslationLbl.TabIndex = 5;
      this.StandardTranslationLbl.Text = "Add Standard Translation:";
      this.label7.AutoSize = true;
      this.label7.Location = new Point(85, 25);
      this.label7.Name = "label7";
      this.label7.Size = new Size(41, 13);
      this.label7.TabIndex = 4;
      this.label7.Text = "English";
      this.label6.AutoSize = true;
      this.label6.Location = new Point(14, 24);
      this.label6.Name = "label6";
      this.label6.Size = new Size(44, 13);
      this.label6.TabIndex = 3;
      this.label6.Text = "Source:";
      this.label2.AutoSize = true;
      this.label2.ForeColor = Color.Red;
      this.label2.Location = new Point(50, 53);
      this.label2.Name = "label2";
      this.label2.Size = new Size(11, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "*";
      this.ExportSelectLbl.AutoSize = true;
      this.ExportSelectLbl.Location = new Point(14, 49);
      this.ExportSelectLbl.Name = "ExportSelectLbl";
      this.ExportSelectLbl.Size = new Size(41, 13);
      this.ExportSelectLbl.TabIndex = 1;
      this.ExportSelectLbl.Text = "Target:";
      this.ExportLangCmbbox.FormattingEnabled = true;
      this.ExportLangCmbbox.Location = new Point(87, 46);
      this.ExportLangCmbbox.Name = "ExportLangCmbbox";
      this.ExportLangCmbbox.Size = new Size(179, 21);
      this.ExportLangCmbbox.TabIndex = 0;
      this.ExportLangCmbbox.LostFocus += new EventHandler(this.ExportLangCmbbox_LostFocus);
      this.ExportBrowsegrp.Controls.Add((Control) this.label4);
      this.ExportBrowsegrp.Controls.Add((Control) this.label3);
      this.ExportBrowsegrp.Controls.Add((Control) this.label1);
      this.ExportBrowsegrp.Controls.Add((Control) this.ExportLocationTxt);
      this.ExportBrowsegrp.Controls.Add((Control) this.ExportBrowseButton);
      this.ExportBrowsegrp.Controls.Add((Control) this.ExportFileNameTextbox);
      this.ExportBrowsegrp.Controls.Add((Control) this.ExportPathTextBox);
      this.ExportBrowsegrp.Location = new Point(12, 224);
      this.ExportBrowsegrp.Name = "ExportBrowsegrp";
      this.ExportBrowsegrp.Size = new Size(518, 100);
      this.ExportBrowsegrp.TabIndex = 0;
      this.ExportBrowsegrp.TabStop = false;
      this.ExportBrowsegrp.Text = "File Information";
      this.label4.AutoSize = true;
      this.label4.ForeColor = Color.Red;
      this.label4.Location = new Point(59, 70);
      this.label4.Name = "label4";
      this.label4.Size = new Size(11, 13);
      this.label4.TabIndex = 4;
      this.label4.Text = "*";
      this.label3.AutoSize = true;
      this.label3.ForeColor = Color.Red;
      this.label3.Location = new Point(64, 36);
      this.label3.Name = "label3";
      this.label3.Size = new Size(11, 13);
      this.label3.TabIndex = 3;
      this.label3.Text = "*";
      this.label1.AutoSize = true;
      this.label1.Location = new Point(12, 32);
      this.label1.Name = "label1";
      this.label1.Size = new Size(57, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "File Name:";
      this.ExportLocationTxt.AutoSize = true;
      this.ExportLocationTxt.Location = new Point(13, 66);
      this.ExportLocationTxt.Name = "ExportLocationTxt";
      this.ExportLocationTxt.Size = new Size(51, 13);
      this.ExportLocationTxt.TabIndex = 1;
      this.ExportLocationTxt.Text = "Location:";
      this.ExportBrowseButton.Location = new Point(448, 59);
      this.ExportBrowseButton.Name = "ExportBrowseButton";
      this.ExportBrowseButton.Size = new Size(59, 20);
      this.ExportBrowseButton.TabIndex = 1;
      this.ExportBrowseButton.Text = "Browse...";
      this.ExportBrowseButton.UseVisualStyleBackColor = true;
      this.ExportBrowseButton.Click += new EventHandler(this.ExportBrowseButton_Click);
      this.ExportFileNameTextbox.Location = new Point(88, 25);
      this.ExportFileNameTextbox.Name = "ExportFileNameTextbox";
      this.ExportFileNameTextbox.Size = new Size(178, 20);
      this.ExportFileNameTextbox.TabIndex = 0;
      this.ExportPathTextBox.Location = new Point(88, 59);
      this.ExportPathTextBox.Name = "ExportPathTextBox";
      this.ExportPathTextBox.Size = new Size(340, 20);
      this.ExportPathTextBox.TabIndex = 0;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BannerVisible = true;
      this.ClientSize = new Size(542, 377);
      this.Controls.Add((Control) this.ExportBrowsegrp);
      this.Controls.Add((Control) this.ExportSelectLangGrp);
      this.MaximizeBox = false;
      this.Name = nameof (ExportText);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Subtitle = "To export translatable text strings to XLIFF format, select a target language and specify a file name and location.";
      this.Text = "Export Text for Translation";
      this.Load += new EventHandler(this.ExportText_Load);
      this.Controls.SetChildIndex((Control) this.ExportSelectLangGrp, 0);
      this.Controls.SetChildIndex((Control) this.buttonOk, 0);
      this.Controls.SetChildIndex((Control) this.buttonCancel, 0);
      this.Controls.SetChildIndex((Control) this.ExportBrowsegrp, 0);
      this.ExportSelectLangGrp.ResumeLayout(false);
      this.ExportSelectLangGrp.PerformLayout();
      this.ExportBrowsegrp.ResumeLayout(false);
      this.ExportBrowsegrp.PerformLayout();
      this.ResumeLayout(false);
    }

    public string FileName
    {
      get
      {
        return this.fileName;
      }
      set
      {
        this.fileName = value;
      }
    }

    public ExportText(List<string> pathList)
      : base(HELP_IDS.BDS_TRANSLATION_EXPORT)
    {
      this.InitializeComponent();
      bool flag = false;
      this.xRepPathlist = pathList;
      this.ExportPathTextBox.Text = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
      this.ExportLangCmbbox.SelectedIndexChanged += new EventHandler(this.ExportLangCmbbox_SelectedIndexChanged);
      this.LanguageArray = new TranslationHandler().getLanguage();
      if (this.LanguageArray == null)
        return;
      if (this.LanguageArray.Length > 0)
      {
        this.ExportLangCmbbox.Items.Clear();
        int num1 = 0;
        int num2 = 0;
        foreach (ZLANG_STRUC language in this.LanguageArray)
        {
          if (language.LANGUAGE.ToString() != "EN")
          {
            flag = true;
            this.LangComboList.Add(new KeyValuePair<string, string>(language.LANGUAGE, language.DESCRIPTION));
            if (language.LANGUAGE.ToString() == "DE")
              num2 = num1;
            ++num1;
          }
        }
        if (flag)
        {
          this.ExportLangCmbbox.DataSource = (object) null;
          this.ExportLangCmbbox.DataSource = (object) new BindingSource((object) this.LangComboList, (string) null);
          this.ExportLangCmbbox.ValueMember = "Key";
          this.ExportLangCmbbox.DisplayMember = "Value";
          this.ExportLangCmbbox.SelectedIndex = num2;
        }
        else
        {
          int num3 = (int) CopernicusMessageBox.Show(TranslationResource.MsgLangError, "Missing Languages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
      }
      else
      {
        int num = (int) CopernicusMessageBox.Show(TranslationResource.MsgLangError, "Missing Languages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private void ExportLangCmbbox_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (!string.IsNullOrEmpty(this.FileName))
      {
        this.ExportFileNameTextbox.Text = this.FileName + "_" + this.LangComboList[this.ExportLangCmbbox.SelectedIndex].Key + ".xlf";
      }
      else
      {
        string str;
        if (this.xRepPathlist.Count > 1)
        {
          str = CopernicusProjectSystemUtil.getSelectedProjectName();
        }
        else
        {
          str = CopernicusProjectSystemUtil.getSelectedNodeName();
          string[] strArray = str.Split('.');
          if (!string.IsNullOrEmpty(strArray[0]))
            str = strArray[0];
        }
        this.ExportFileNameTextbox.Text = str + "_" + this.LangComboList[this.ExportLangCmbbox.SelectedIndex].Key + ".xlf";
      }
    }

    private void ExportCancelButton_Click(object sender, EventArgs e)
    {
      this.ExportLangCmbbox.SelectedIndexChanged -= new EventHandler(this.ExportLangCmbbox_SelectedIndexChanged);
      this.Close();
    }

    private void ExportBrowseButton_Click(object sender, EventArgs e)
    {
      FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
      if (folderBrowserDialog.ShowDialog() != DialogResult.OK)
        return;
      this.ExportPathTextBox.Text = folderBrowserDialog.SelectedPath;
    }

    public void setXrepPath(bool isSolution, List<string> xReplist)
    {
      this.xRepPathlist.Clear();
      this.xRepPathlist = xReplist;
    }

    private void ExportText_Load(object sender, EventArgs e)
    {
      this.Icon = SAP.Copernicus.Resource.SAPBusinessByDesignStudioIcon;
    }

    protected override void buttonOk_Click(object sender, EventArgs e)
    {
      string text1 = this.ExportLangCmbbox.Text;
      string text2 = this.ExportFileNameTextbox.Text;
      string text3 = this.ExportPathTextBox.Text;
      if (string.IsNullOrEmpty(text1))
      {
        int num1 = (int) CopernicusMessageBox.Show(TranslationResource.MsgExpLang, "Select Language", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else if (string.IsNullOrEmpty(text2))
      {
        int num2 = (int) CopernicusMessageBox.Show(TranslationResource.MsgExpFile, "File Name", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else if (string.IsNullOrEmpty(text3))
      {
        int num3 = (int) CopernicusMessageBox.Show(TranslationResource.MsgExpPathNull, "File Path", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else if (!Directory.Exists(text3))
      {
        int num4 = (int) CopernicusMessageBox.Show(TranslationResource.MsgExpPathValid, "File Path", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        if (!text2.EndsWith(".xlf", true, (CultureInfo) null))
          text2 += ".xlf";
        string str = Path.Combine(text3, text2);
        TranslationHandler translationHandler = new TranslationHandler();
        string language1 = "DE";
        if (this.LanguageArray != null)
        {
          foreach (ZLANG_STRUC language2 in this.LanguageArray)
          {
            if (language2.DESCRIPTION.Equals(text1))
              language1 = language2.LANGUAGE;
          }
        }
        string addDefaultTrans = "";
        if (this.StandardTranslationCheckBox.Checked.Equals(true))
          addDefaultTrans = "X";
        string xmlsstring;
        if (translationHandler.ExportTextHandler(this.xRepPathlist.ToArray(), language1, str, addDefaultTrans, out xmlsstring))
        {
          try
          {
            Cursor.Current = Cursors.WaitCursor;
            using (XmlTextWriter xmlTextWriter = new XmlTextWriter(str, (Encoding) null))
            {
              xmlTextWriter.WriteRaw(xmlsstring);
              xmlTextWriter.Formatting = Formatting.Indented;
              this.Visible = false;
              CopernicusStatusBar.Instance.ShowMessage(TranslationResource.MsgExpSuccess);
              this.Close();
            }
            Cursor.Current = Cursors.WaitCursor;
          }
          catch (IOException ex)
          {
            int num5 = (int) CopernicusMessageBox.Show(TranslationResource.MsgExpErr + ": " + ex.Message, "Export error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
          }
        }
        else
        {
          CopernicusStatusBar.Instance.ShowMessage(TranslationResource.MsgExpErr);
          this.Close();
        }
      }
    }

    public void setLanguage(string Language)
    {
      if (string.IsNullOrEmpty(Language) || this.LangComboList.Count == 0)
        return;
      foreach (KeyValuePair<string, string> langCombo in this.LangComboList)
      {
        if (langCombo.Value == Language)
          this.ExportLangCmbbox.SelectedItem = (object) langCombo;
      }
    }

    private void ExportLangCmbbox_LostFocus(object sender, EventArgs e)
    {
      ComboBox comboBox = (ComboBox) sender;
      bool flag = false;
      KeyValuePair<string, string> keyValuePair = new KeyValuePair<string, string>();
      if (comboBox.SelectedItem != null)
      {
        KeyValuePair<string, string> selectedItem = (KeyValuePair<string, string>) comboBox.SelectedItem;
        foreach (ZLANG_STRUC language in this.LanguageArray)
        {
          if (selectedItem.Key == language.LANGUAGE)
          {
            flag = true;
            if (selectedItem.Value.ToUpper() != language.DESCRIPTION.ToUpper())
            {
              int num = (int) CopernicusMessageBox.Show(TranslationResource.MsgExpLangInvalid, "Export error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
              this.ExportLangCmbbox.ResetText();
              return;
            }
          }
        }
        if (flag)
          return;
        int num1 = (int) CopernicusMessageBox.Show(TranslationResource.MsgExpLangInvalid, "Export error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        this.ExportLangCmbbox.ResetText();
      }
      else
      {
        int num = (int) CopernicusMessageBox.Show(TranslationResource.MsgExpLangInvalid, "Export error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        this.ExportLangCmbbox.ResetText();
      }
    }

    private void infoBtnClick(object sender, EventArgs e)
    {
      int num = (int) CopernicusMessageBox.Show(TranslationResource.MsgStandardTranslationInfo, TranslationResource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }
  }
}
