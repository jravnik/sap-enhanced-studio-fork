﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Translation.Resources.TranslationResource
// Assembly: CopernicusTranslation, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 9D6A50C9-F2FE-4B16-A45E-C445F353581B
// Assembly location: G:\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusTranslation.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus.Translation.Resources
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  public class TranslationResource
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal TranslationResource()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) TranslationResource.resourceMan, (object) null))
          TranslationResource.resourceMan = new ResourceManager("SAP.Copernicus.Translation.Resources.TranslationResource", typeof (TranslationResource).Assembly);
        return TranslationResource.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return TranslationResource.resourceCulture;
      }
      set
      {
        TranslationResource.resourceCulture = value;
      }
    }

    public static string CheckTableColComp
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (CheckTableColComp), TranslationResource.resourceCulture);
      }
    }

    public static string CheckTableColSrcLang
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (CheckTableColSrcLang), TranslationResource.resourceCulture);
      }
    }

    public static string MsgBYD
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgBYD), TranslationResource.resourceCulture);
      }
    }

    public static string MsgChkExportNoUIComp
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgChkExportNoUIComp), TranslationResource.resourceCulture);
      }
    }

    public static string MsgChkNoUIComp
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgChkNoUIComp), TranslationResource.resourceCulture);
      }
    }

    public static string MsgChkNoUISolComp
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgChkNoUISolComp), TranslationResource.resourceCulture);
      }
    }

    public static string MsgChkUIComp
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgChkUIComp), TranslationResource.resourceCulture);
      }
    }
    
    public static string MsgQuickUIComp
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgQuickUIComp), TranslationResource.resourceCulture);
      }
    }

    public static string MsgExpErr
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgExpErr), TranslationResource.resourceCulture);
      }
    }

    public static string MsgExpFile
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgExpFile), TranslationResource.resourceCulture);
      }
    }

    public static string MsgExpLang
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgExpLang), TranslationResource.resourceCulture);
      }
    }

    public static string MsgExpLangInvalid
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgExpLangInvalid), TranslationResource.resourceCulture);
      }
    }

    public static string MsgExpPathNull
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgExpPathNull), TranslationResource.resourceCulture);
      }
    }

    public static string MsgExpPathValid
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgExpPathValid), TranslationResource.resourceCulture);
      }
    }

    public static string MsgExpSuccess
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgExpSuccess), TranslationResource.resourceCulture);
      }
    }

    public static string MsgHandlerChk
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgHandlerChk), TranslationResource.resourceCulture);
      }
    }

    public static string MsgHandlerExp
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgHandlerExp), TranslationResource.resourceCulture);
      }
    }

    public static string MsgHandlerImp
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgHandlerImp), TranslationResource.resourceCulture);
      }
    }

    public static string MsgHandlerLang
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgHandlerLang), TranslationResource.resourceCulture);
      }
    }

    public static string MsgImpErr
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgImpErr), TranslationResource.resourceCulture);
      }
    }

    public static string MsgImpFile
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgImpFile), TranslationResource.resourceCulture);
      }
    }

    public static string MsgImpPathValid
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgImpPathValid), TranslationResource.resourceCulture);
      }
    }

    public static string MsgImpSuccess
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgImpSuccess), TranslationResource.resourceCulture);
      }
    }

    public static string MsgLangError
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgLangError), TranslationResource.resourceCulture);
      }
    }

    public static string MsgStandardTranslationInfo
    {
      get
      {
        return TranslationResource.ResourceManager.GetString(nameof (MsgStandardTranslationInfo), TranslationResource.resourceCulture);
      }
    }
  }
}
