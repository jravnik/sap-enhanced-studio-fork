﻿// Decompiled with JetBrains decompiler
// Type: SHDocVw.WebBrowser
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SHDocVw
{
  [CoClass(typeof (object))]
  [CompilerGenerated]
  [TypeIdentifier]
  [Guid("D30C1661-CDAF-11D0-8A3E-00C04FC9E26E")]
  [ComImport]
  public interface WebBrowser : IWebBrowser2, IWebBrowserApp, IWebBrowser, DWebBrowserEvents2_Event
  {
  }
}
