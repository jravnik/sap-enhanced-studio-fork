﻿// Decompiled with JetBrains decompiler
// Type: mshtml.IHTMLScriptElement
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace mshtml
{
  [TypeIdentifier]
  [CompilerGenerated]
  [Guid("3050F28B-98B5-11CF-BB82-00AA00BDCE0B")]
  [ComImport]
  public interface IHTMLScriptElement
  {
    [SpecialName]
    void _VtblGap1_6();

    string text { [DispId(1006)] [param: MarshalAs(UnmanagedType.BStr), In] set; [DispId(1006)] [return: MarshalAs(UnmanagedType.BStr)] get; }

    [SpecialName]
    void _VtblGap2_5();

    string type { [DispId(1009)] [param: MarshalAs(UnmanagedType.BStr), In] set; [DispId(1009)] [return: MarshalAs(UnmanagedType.BStr)] get; }
  }
}
