﻿// Decompiled with JetBrains decompiler
// Type: mshtml.HTMLDocumentEvents2_Event
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace mshtml
{
  [TypeIdentifier("3050f1c5-98b5-11cf-bb82-00aa00bdce0b", "mshtml.HTMLDocumentEvents2_Event")]
  [ComEventInterface(typeof (HTMLDocumentEvents2), typeof (HTMLDocumentEvents2))]
  [CompilerGenerated]
  public interface HTMLDocumentEvents2_Event
  {
    [SpecialName]
    void _VtblGap1_2();

    event HTMLDocumentEvents2_onclickEventHandler onclick;
  }
}
