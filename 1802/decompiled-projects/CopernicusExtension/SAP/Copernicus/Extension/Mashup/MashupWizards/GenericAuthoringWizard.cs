﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupWizards.GenericAuthoringWizard
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;
using System.Collections.Generic;

namespace SAP.Copernicus.Extension.Mashup.MashupWizards
{
  public abstract class GenericAuthoringWizard : IWizard
  {
    private static string strMashupServiceItemForWizard = "$rootname$";
    private string strMashupServiceItemName = string.Empty;

    protected abstract void invokeTargetWizard(string strInputNameWizard);

    public void BeforeOpeningFile(ProjectItem projectItem)
    {
    }

    public void ProjectFinishedGenerating(Project project)
    {
    }

    public void ProjectItemFinishedGenerating(ProjectItem projectItem)
    {
    }

    public void RunFinished()
    {
      this.invokeTargetWizard(this.strMashupServiceItemName);
    }

    public void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
    {
      this.strMashupServiceItemName = replacementsDictionary[GenericAuthoringWizard.strMashupServiceItemForWizard];
    }

    public bool ShouldAddProjectItem(string filePath)
    {
      return false;
    }
  }
}
