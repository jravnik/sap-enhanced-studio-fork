﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupWizards.ReferencedUIComponentsForm
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.BYD.LS.UI.Core.Connector.Services.Mashup;
using SAP.Copernicus.Extension.Mashup.HtmlMashup.MashupJsonUtil.MashupHandler;
using SAP.Copernicus.Extension.UIDesignerHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.Mashup.MashupWizards
{
  public class ReferencedUIComponentsForm : Form
  {
    private IContainer components;
    private DataGridView dataGridViewReferencedUIs;
    private Label lblHeader;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private DataGridViewTextBoxColumn colTitle;
    private DataGridViewTextBoxColumn colFullPath;
    private DataGridViewLinkColumn OpenLink;
    private DataGridViewLinkColumn dataGridViewLinkColumn1;
    private Button btnClose;

    public ReferencedUIComponentsForm()
    {
      this.InitializeComponent();
      this.dataGridViewReferencedUIs.CellContentClick += new DataGridViewCellEventHandler(this.dataGridViewReferencedUIs_CellContentClick);
    }

    public void InitReferencedUIComponentsList(string localFilePath)
    {
      FLX_MASH_T_PIPE_USAGE[] referencedUiComponents = new JSONMashupPipeWhereUsedQueryHandler().GetReferencedUIComponents(SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(localFilePath)["KEY"]);
      for (int index = 0; index < ((IEnumerable<FLX_MASH_T_PIPE_USAGE>) referencedUiComponents).Count<FLX_MASH_T_PIPE_USAGE>(); ++index)
      {
        this.dataGridViewReferencedUIs.Rows.Add((object) referencedUiComponents[index].UI_TITLE, (object) referencedUiComponents[index].UI_PATH, (object) "Open In UI Designer");
        this.dataGridViewReferencedUIs.Rows[index].Cells[2].Tag = (object) referencedUiComponents[index].UI_PATH;
      }
      int num = (int) this.ShowDialog();
    }

    private void dataGridViewReferencedUIs_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.ColumnIndex != 2)
        return;
      UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(this.dataGridViewReferencedUIs.Rows[e.RowIndex].Cells[2].Tag.ToString(), false);
    }

    private void btnClose_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (ReferencedUIComponentsForm));
      this.dataGridViewReferencedUIs = new DataGridView();
      this.lblHeader = new Label();
      this.dataGridViewLinkColumn1 = new DataGridViewLinkColumn();
      this.OpenLink = new DataGridViewLinkColumn();
      this.btnClose = new Button();
      this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
      this.colTitle = new DataGridViewTextBoxColumn();
      this.colFullPath = new DataGridViewTextBoxColumn();
      ((ISupportInitialize) this.dataGridViewReferencedUIs).BeginInit();
      this.SuspendLayout();
      this.dataGridViewReferencedUIs.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.dataGridViewReferencedUIs.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridViewReferencedUIs.Columns.AddRange((DataGridViewColumn) this.colTitle, (DataGridViewColumn) this.colFullPath, (DataGridViewColumn) this.OpenLink);
      this.dataGridViewReferencedUIs.Location = new Point(12, 61);
      this.dataGridViewReferencedUIs.Name = "dataGridViewReferencedUIs";
      this.dataGridViewReferencedUIs.ReadOnly = true;
      this.dataGridViewReferencedUIs.Size = new Size(643, 179);
      this.dataGridViewReferencedUIs.TabIndex = 0;
      this.lblHeader.AutoSize = true;
      this.lblHeader.Location = new Point(9, 18);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new Size(618, 26);
      this.lblHeader.TabIndex = 1;
      this.lblHeader.Text = componentResourceManager.GetString("lblHeader.Text");
      this.dataGridViewLinkColumn1.HeaderText = "";
      this.dataGridViewLinkColumn1.Name = "dataGridViewLinkColumn1";
      this.dataGridViewLinkColumn1.ReadOnly = true;
      this.OpenLink.HeaderText = "";
      this.OpenLink.Name = "OpenLink";
      this.OpenLink.ReadOnly = true;
      this.btnClose.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.btnClose.Location = new Point(580, 246);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new Size(75, 23);
      this.btnClose.TabIndex = 2;
      this.btnClose.Text = "Close";
      this.btnClose.UseVisualStyleBackColor = true;
      this.btnClose.Click += new EventHandler(this.btnClose_Click);
      this.dataGridViewTextBoxColumn1.DataPropertyName = "UI_TITLE";
      this.dataGridViewTextBoxColumn1.HeaderText = "Title";
      this.dataGridViewTextBoxColumn1.MinimumWidth = 20;
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.ReadOnly = true;
      this.dataGridViewTextBoxColumn1.Width = 200;
      this.dataGridViewTextBoxColumn2.DataPropertyName = "UI_PATH";
      this.dataGridViewTextBoxColumn2.HeaderText = "Full Path";
      this.dataGridViewTextBoxColumn2.MinimumWidth = 30;
      this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
      this.dataGridViewTextBoxColumn2.ReadOnly = true;
      this.dataGridViewTextBoxColumn2.Width = 300;
      this.colTitle.DataPropertyName = "UI_TITLE";
      this.colTitle.HeaderText = "Title";
      this.colTitle.MinimumWidth = 150;
      this.colTitle.Name = "colTitle";
      this.colTitle.ReadOnly = true;
      this.colTitle.Width = 200;
      this.colFullPath.DataPropertyName = "UI_PATH";
      this.colFullPath.HeaderText = "Full Path";
      this.colFullPath.MinimumWidth = 300;
      this.colFullPath.Name = "colFullPath";
      this.colFullPath.ReadOnly = true;
      this.colFullPath.Width = 300;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(669, 275);
      this.Controls.Add((Control) this.btnClose);
      this.Controls.Add((Control) this.lblHeader);
      this.Controls.Add((Control) this.dataGridViewReferencedUIs);
      this.Name = nameof (ReferencedUIComponentsForm);
      this.Text = nameof (ReferencedUIComponentsForm);
      ((ISupportInitialize) this.dataGridViewReferencedUIs).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
