﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.AdvancedWebBrowser
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  [ComDefaultInterface(typeof (IObjectSafety))]
  public class AdvancedWebBrowser : System.Windows.Forms.WebBrowser, IObjectSafety
  {
    private InternetProxy proxy = InternetProxy.NoProxy;
    private const int INTERFACESAFE_FOR_UNTRUSTED_CALLER = 1;
    private const int INTERFACESAFE_FOR_UNTRUSTED_DATA = 2;
    private const int S_OK = 0;

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public InternetProxy Proxy
    {
      get
      {
        return this.proxy;
      }
      set
      {
        InternetProxy internetProxy = InternetProxy.NoProxy;
        if (value != null)
          internetProxy = value;
        if (this.proxy.Equals((object) internetProxy))
          return;
        this.proxy = internetProxy;
        if (this.proxy != null && !string.IsNullOrEmpty(this.proxy.Address))
          WinINet.SetConnectionProxy(false, this.proxy.Address);
        else
          WinINet.RestoreSystemProxy();
      }
    }

    [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
    public AdvancedWebBrowser()
    {
      this.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.browser_DocumentCompleted);
    }

    private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      this.ScriptErrorsSuppressed = true;
    }

    [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
    public void Goto(string url)
    {
      Uri result = (Uri) null;
      if (!Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out result))
        throw new ArgumentException(string.Format("The url is not valid for AdvancedWebBrowser Navigation"));
      if (this.Proxy != null && !string.IsNullOrEmpty(this.Proxy.UserName) && !string.IsNullOrEmpty(this.Proxy.Password))
      {
        string additionalHeaders = string.Format("Proxy-Authorization: Basic {0}", (object) Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", (object) this.Proxy.UserName, (object) this.Proxy.Password))));
        this.Navigate(result, string.Empty, (byte[]) null, additionalHeaders);
      }
      else
        this.Navigate(result);
    }

    public int GetInterfaceSafetyOptions(ref Guid riid, out int pwdSupportedOptions, out int pwdEnableOptions)
    {
      pwdSupportedOptions = 3;
      pwdEnableOptions = 3;
      return 0;
    }

    public int SetInterfaceSafetyOptions(ref Guid riid, int dwOptionSetMask, int dwEnabledOptions)
    {
      return 0;
    }
  }
}
