﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.WebBrowser.WinINet
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.Extension.Mashup.WebBrowser
{
  public static class WinINet
  {
    private static string agent = Process.GetCurrentProcess().ProcessName;

    public static bool SetConnectionProxy(bool isMachineSetting, string proxyServer)
    {
      if (isMachineSetting)
        return WinINet.SetConnectionProxy((string) null, proxyServer);
      return WinINet.SetConnectionProxy(WinINet.agent, proxyServer);
    }

    public static bool SetConnectionProxy(string agentName, string proxyServer)
    {
      IntPtr hInternet = IntPtr.Zero;
      try
      {
        if (!string.IsNullOrEmpty(agentName))
          hInternet = SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.InternetOpen(agentName, 1, (string) null, (string) null, 0);
        return WinINet.SetConnectionProxyInternal(hInternet, proxyServer);
      }
      finally
      {
        if (hInternet != IntPtr.Zero)
          SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.InternetCloseHandle(hInternet);
      }
    }

    private static bool SetConnectionProxyInternal(IntPtr hInternet, string proxyServer)
    {
      INTERNET_PER_CONN_OPTION[] internetPerConnOptionArray = new INTERNET_PER_CONN_OPTION[3];
      internetPerConnOptionArray[0] = new INTERNET_PER_CONN_OPTION();
      internetPerConnOptionArray[0].dwOption = 1;
      internetPerConnOptionArray[0].Value.dwValue = 2;
      internetPerConnOptionArray[1] = new INTERNET_PER_CONN_OPTION();
      internetPerConnOptionArray[1].dwOption = 2;
      internetPerConnOptionArray[1].Value.pszValue = Marshal.StringToHGlobalAnsi(proxyServer);
      internetPerConnOptionArray[2] = new INTERNET_PER_CONN_OPTION();
      internetPerConnOptionArray[2].dwOption = 3;
      internetPerConnOptionArray[2].Value.pszValue = Marshal.StringToHGlobalAnsi("local");
      IntPtr ptr1 = Marshal.AllocCoTaskMem(Marshal.SizeOf((object) internetPerConnOptionArray[0]) + Marshal.SizeOf((object) internetPerConnOptionArray[1]) + Marshal.SizeOf((object) internetPerConnOptionArray[2]));
      IntPtr ptr2 = ptr1;
      for (int index = 0; index < internetPerConnOptionArray.Length; ++index)
      {
        Marshal.StructureToPtr((object) internetPerConnOptionArray[index], ptr2, false);
        ptr2 = (IntPtr) ((int) ptr2 + Marshal.SizeOf((object) internetPerConnOptionArray[index]));
      }
      INTERNET_PER_CONN_OPTION_LIST perConnOptionList = new INTERNET_PER_CONN_OPTION_LIST();
      perConnOptionList.pOptions = ptr1;
      perConnOptionList.Size = Marshal.SizeOf((object) perConnOptionList);
      perConnOptionList.Connection = IntPtr.Zero;
      perConnOptionList.OptionCount = internetPerConnOptionArray.Length;
      perConnOptionList.OptionError = 0;
      int num1 = Marshal.SizeOf((object) perConnOptionList);
      IntPtr num2 = Marshal.AllocCoTaskMem(num1);
      Marshal.StructureToPtr((object) perConnOptionList, num2, true);
      bool flag = SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.InternetSetOption(hInternet, INTERNET_OPTION.INTERNET_OPTION_PER_CONNECTION_OPTION, num2, num1);
      Marshal.FreeCoTaskMem(ptr1);
      Marshal.FreeCoTaskMem(num2);
      if (!flag)
        throw new ApplicationException(" Set Internet Option Failed!");
      SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.InternetSetOption(hInternet, INTERNET_OPTION.INTERNET_OPTION_SETTINGS_CHANGED, IntPtr.Zero, 0);
      SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.InternetSetOption(hInternet, INTERNET_OPTION.INTERNET_OPTION_REFRESH, IntPtr.Zero, 0);
      return flag;
    }

    public static INTERNET_PER_CONN_OPTION_LIST GetSystemProxy()
    {
      INTERNET_PER_CONN_OPTION[] internetPerConnOptionArray = new INTERNET_PER_CONN_OPTION[3];
      internetPerConnOptionArray[0] = new INTERNET_PER_CONN_OPTION();
      internetPerConnOptionArray[0].dwOption = 1;
      internetPerConnOptionArray[1] = new INTERNET_PER_CONN_OPTION();
      internetPerConnOptionArray[1].dwOption = 2;
      internetPerConnOptionArray[2] = new INTERNET_PER_CONN_OPTION();
      internetPerConnOptionArray[2].dwOption = 3;
      IntPtr num = Marshal.AllocCoTaskMem(Marshal.SizeOf((object) internetPerConnOptionArray[0]) + Marshal.SizeOf((object) internetPerConnOptionArray[1]) + Marshal.SizeOf((object) internetPerConnOptionArray[2]));
      IntPtr ptr = num;
      for (int index = 0; index < internetPerConnOptionArray.Length; ++index)
      {
        Marshal.StructureToPtr((object) internetPerConnOptionArray[index], ptr, false);
        ptr = (IntPtr) ((int) ptr + Marshal.SizeOf((object) internetPerConnOptionArray[index]));
      }
      INTERNET_PER_CONN_OPTION_LIST OptionList = new INTERNET_PER_CONN_OPTION_LIST();
      OptionList.pOptions = num;
      OptionList.Size = Marshal.SizeOf((object) OptionList);
      OptionList.Connection = IntPtr.Zero;
      OptionList.OptionCount = internetPerConnOptionArray.Length;
      OptionList.OptionError = 0;
      int lpdwBufferLength = Marshal.SizeOf((object) OptionList);
      if (!SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.InternetQueryOption(IntPtr.Zero, INTERNET_OPTION.INTERNET_OPTION_PER_CONNECTION_OPTION, ref OptionList, ref lpdwBufferLength))
        throw new ApplicationException("Get System Internet Option Failed! ");
      return OptionList;
    }

    public static bool RestoreSystemProxy()
    {
      return WinINet.RestoreSystemProxy(WinINet.agent);
    }

    public static bool RestoreSystemProxy(string agentName)
    {
      if (string.IsNullOrEmpty(agentName))
        throw new ArgumentNullException("Agent name cannot be null or empty!");
      IntPtr hInternet = IntPtr.Zero;
      try
      {
        if (!string.IsNullOrEmpty(agentName))
          hInternet = SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.InternetOpen(agentName, 1, (string) null, (string) null, 0);
        return WinINet.RestoreSystemProxyInternal(hInternet);
      }
      finally
      {
        if (hInternet != IntPtr.Zero)
          SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.InternetCloseHandle(hInternet);
      }
    }

    private static bool RestoreSystemProxyInternal(IntPtr hInternet)
    {
      INTERNET_PER_CONN_OPTION_LIST systemProxy = WinINet.GetSystemProxy();
      int num1 = Marshal.SizeOf((object) systemProxy);
      IntPtr num2 = Marshal.AllocCoTaskMem(num1);
      Marshal.StructureToPtr((object) systemProxy, num2, true);
      bool flag = SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.InternetSetOption(hInternet, INTERNET_OPTION.INTERNET_OPTION_PER_CONNECTION_OPTION, num2, num1);
      Marshal.FreeCoTaskMem(systemProxy.pOptions);
      Marshal.FreeCoTaskMem(num2);
      if (!flag)
        throw new ApplicationException(" Set Internet Option Failed! ");
      SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.InternetSetOption(hInternet, INTERNET_OPTION.INTERNET_OPTION_SETTINGS_CHANGED, IntPtr.Zero, 0);
      SAP.Copernicus.Extension.Mashup.WebBrowser.NativeWrapper.NativeMethods.InternetSetOption(hInternet, INTERNET_OPTION.INTERNET_OPTION_REFRESH, IntPtr.Zero, 0);
      return flag;
    }
  }
}
