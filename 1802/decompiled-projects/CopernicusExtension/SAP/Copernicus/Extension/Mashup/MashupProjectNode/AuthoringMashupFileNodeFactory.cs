﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.AuthoringMashupFileNodeFactory
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio.Project;
using SAP.CopernicusProjectView;
using System.IO;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode
{
  internal class AuthoringMashupFileNodeFactory : ICopernicusHierarchyFileNodeFactory, SAuthoringMashupFileNodeFactory
  {
    public HierarchyNode CreateNode(CopernicusProjectNode root, FileInfo element)
    {
      return (HierarchyNode) new AuthoringMashupFileNode(root, element);
    }
  }
}
