﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.AuthoringMashupFileNode
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Project.Automation;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.BYD.LS.UI.Core.Connector.Services.Mashup;
using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.Extension.Mashup.HtmlMashup.MashupJsonUtil.MashupHandler;
using SAP.Copernicus.Extension.Mashup.MashupProjectNode.NodeMenusFilter;
using SAP.Copernicus.Extension.Mashup.WebBrowser;
using SAP.Copernicus.Extension.UIDesignerHelper;
using SAP.Copernicus.Translation;
using SAP.Copernicus.uidesigner.integration.model.MashupJsonUtil.UXModelAPI;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode
{
  [ComVisible(true)]
  internal class AuthoringMashupFileNode : HierarchyNode
  {
    private static string strPostfixForMashup = ".MC.uimashup";
    private static string strPostfixForWS = ".WS.uimashup";
    private static string strPostfixForPB = ".PB.uimashup";
    private static bool isDeleteItemInProcess = false;
    private static int iThreadDelay = 500;
    private bool isEnableWhereUsedList;
    private AuthoringType _type;
    private FileInfo element;
    private string strMashupItemId;
    private PDI_S_PRODUCT_V_AND_COMPONENTS productVersion;

    public AuthoringMashupFileNode(CopernicusProjectNode root, FileInfo element)
      : base((ProjectNode) root)
    {
      this.element = element;
      string fullName = element.FullName;
      if (fullName.EndsWith(AuthoringMashupFileNode.strPostfixForMashup))
        this._type = AuthoringType.MashupAuthoring;
      else if (fullName.EndsWith(AuthoringMashupFileNode.strPostfixForWS))
      {
        this._type = AuthoringType.ServiceAuthoring;
      }
      else
      {
        if (!fullName.EndsWith(AuthoringMashupFileNode.strPostfixForPB))
          throw new NotImplementedException("Not Supported Mashup Item");
        this._type = AuthoringType.MashupPortBinding;
      }
      IDictionary<string, string> fileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(fullName);
      if (this._type != AuthoringType.MashupPortBinding)
        MashupMetaDataAccess.UpdateInMemoryAttributeForMashupLocalFile(ref fileAttributes, fullName);
      this.strMashupItemId = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(fullName)["KEY"];
      this.IsExpanded = true;
    }

    public AuthoringType UIType
    {
      get
      {
        return this._type;
      }
    }

    public void AsynUpdateDisplayNameMetaData()
    {
      ThreadPool.QueueUserWorkItem((WaitCallback) (state =>
      {
        string caption = this.Caption;
        IDictionary<string, string> fileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(this.Url);
        MashupMetaDataAccess.UpdateInMemoryAttributeForMashupLocalFile(ref fileAttributes, this.element.FullName);
        if (caption != null && (caption == null || caption.Equals(this.Caption)))
          return;
        this.ReDraw(UIHierarchyElement.Caption);
      }));
    }

    public void SynUpdateDisplayNameMetaData()
    {
      string caption = this.Caption;
      IDictionary<string, string> fileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(this.Url);
      MashupMetaDataAccess.UpdateInMemoryAttributeForMashupLocalFile(ref fileAttributes, this.element.FullName);
      if (caption != null && (caption == null || caption.Equals(this.Caption)))
        return;
      this.ReDraw(UIHierarchyElement.Caption);
    }

    private bool? RetrieveDependentUIMashupComponent(MashupPipeWhereUsedQueryFunction handler, string strMashupId)
    {
      bool flag1 = false;
      MashupPipeWhereUsedQueryFunction usedQueryFunction = handler;
      if (usedQueryFunction != null && usedQueryFunction.Exporting != null && usedQueryFunction.Exporting.EV_RETURN_CODE == 0)
      {
        CopernicusProjectNode projectMgr = this.ProjectMgr as CopernicusProjectNode;
        foreach (FLX_MASH_T_PIPE_USAGE flxMashTPipeUsage in usedQueryFunction.Exporting.ET_PIPE_USAGE)
        {
          bool flag2 = false;
          for (HierarchyNode hierarchyNode = this.FirstChild; hierarchyNode != null; hierarchyNode = hierarchyNode.NextSibling)
          {
            if (hierarchyNode.Url.Equals(flxMashTPipeUsage.UI_PATH))
              flag2 = true;
          }
          if (!flag2)
          {
            HierarchyNode dependentUiComponent = projectMgr.CreateDependentUIComponent(flxMashTPipeUsage.UI_PATH);
            try
            {
              this.AddChild(dependentUiComponent);
            }
            catch (Exception ex)
            {
              Trace.WriteLine(ex.StackTrace);
            }
          }
        }
      }
      return new bool?(flag1);
    }

    private bool? RetrieveDependentServiceUIComponent(MashupServiceWhereUsedQueryFunction handler, string strServiceId)
    {
      bool flag1 = false;
      MashupServiceWhereUsedQueryFunction usedQueryFunction = handler;
      if (usedQueryFunction != null && usedQueryFunction.Exporting != null && usedQueryFunction.Exporting.EV_RETURN_CODE == 0)
      {
        CopernicusProjectNode projectMgr = this.ProjectMgr as CopernicusProjectNode;
        foreach (FLX_MASH_T_PIPE_USAGE flxMashTPipeUsage in usedQueryFunction.Exporting.ET_PIPE_USAGE)
        {
          bool flag2 = false;
          for (HierarchyNode hierarchyNode = this.FirstChild; hierarchyNode != null; hierarchyNode = hierarchyNode.NextSibling)
          {
            if (hierarchyNode.Url.Equals(flxMashTPipeUsage.UI_PATH))
              flag2 = true;
          }
          if (!flag2)
          {
            HierarchyNode dependentUiComponent = projectMgr.CreateDependentUIComponent(flxMashTPipeUsage.UI_PATH);
            try
            {
              this.AddChild(dependentUiComponent);
            }
            catch (Exception ex)
            {
              Trace.WriteLine(ex.StackTrace);
            }
          }
        }
      }
      return new bool?(flag1);
    }

    public override string GetMkDocument()
    {
      if (this._type == AuthoringType.MashupPortBinding)
        return this.Url;
      return string.Empty;
    }

    protected override void DeleteFromStorage(string path)
    {
      if (!new FileInfo(this.element.FullName).Exists)
        return;
      LocalFileHandler.DeleteFileFromDisk(this.element.FullName);
    }

    public override void Remove(bool removeFromStorage)
    {
      for (HierarchyNode hierarchyNode = this.FirstChild; hierarchyNode != null; hierarchyNode = hierarchyNode.NextSibling)
        hierarchyNode.Remove(true);
      if (this.Parent != null)
        this.Parent.RemoveChild((HierarchyNode) this);
      this.ItemNode.RemoveFromProjectFile();
      this.OnInvalidateItems(this.Parent);
      this.Dispose(true);
    }

    public override int SortPriority
    {
      get
      {
        return 300;
      }
    }

    public override int MenuCommandId
    {
      get
      {
        return 1104;
      }
    }

    public override Guid ItemTypeGuid
    {
      get
      {
        return VSConstants.GUID_ItemType_VirtualFolder;
      }
    }

    public override string Caption
    {
      get
      {
        CopernicusMashupSharedFileNodeProperties fileNodeProperties = new CopernicusMashupSharedFileNodeProperties((HierarchyNode) this, this.element.FullName);
        if (this._type != AuthoringType.MashupPortBinding)
          return fileNodeProperties.MashupDisplayName;
        if (fileNodeProperties.MashupXRepId.Contains("/"))
          return fileNodeProperties.MashupXRepId.Substring(fileNodeProperties.MashupXRepId.LastIndexOf("/") + 1);
        return fileNodeProperties.MashupXRepId;
      }
    }

    public override NodeProperties NodeProperties
    {
      get
      {
        if (this._type == AuthoringType.MashupAuthoring)
          return (NodeProperties) new CopernicusMashupFileNodeProperties((HierarchyNode) this, this.element.FullName);
        if (this._type == AuthoringType.ServiceAuthoring)
          return (NodeProperties) new CopernicusServiceFileNodeProperties((HierarchyNode) this, this.element.FullName);
        if (this._type == AuthoringType.MashupPortBinding)
          return (NodeProperties) new CopernicusPortBindingNodeProperties((HierarchyNode) this, this.element.FullName);
        return base.NodeProperties;
      }
    }

    public override string Url
    {
      get
      {
        return new Microsoft.VisualStudio.Shell.Url(this.element.FullName).AbsoluteUrl;
      }
    }

    public override object GetAutomationObject()
    {
      if (this.ProjectMgr == null || this.ProjectMgr.IsClosed)
        return (object) null;
      return (object) new OAProjectItem<AuthoringMashupFileNode>(this.ProjectMgr.GetAutomationObject() as OAProject, this);
    }

    public override object GetIconHandle(bool open)
    {
      IDictionary<string, string> fileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(this.element.FullName);
      switch (fileAttributes["MASHUP_TYPE"])
      {
        case "Html_Mashup":
          return (object) SAP.Copernicus.Extension.Mashup.Resource.CreateHtmlMashup16x16.GetHicon();
        case "Url_Mashup":
          return (object) SAP.Copernicus.Extension.Mashup.Resource.CreateURLMashup16x16.GetHicon();
        case "Data_Mashup":
          return (object) SAP.Copernicus.Extension.Mashup.Resource.CreateDataMashup16x16.GetHicon();
        case "Port_Binding":
          if (this.isActive())
            return (object) SAP.Copernicus.Extension.Mashup.Resource.MashupPortBinding.GetHicon();
          return (object) SAP.Copernicus.Extension.Mashup.Resource.MashupPortBindingInactive.GetHicon();
        default:
          switch (fileAttributes["SERVICE_TYPE"])
          {
            case "Rest_Service":
              return (object) SAP.Copernicus.Extension.Mashup.Resource.CreateRestService16x16.GetHicon();
            case "Feed_Service":
              return (object) SAP.Copernicus.Extension.Mashup.Resource.CreateRssAtomService16x16.GetHicon();
            case "Wsdl_Service":
              return (object) SAP.Copernicus.Extension.Mashup.Resource.CreateSoapService16x16.GetHicon();
            default:
              return (object) SAP.Copernicus.Extension.Mashup.Resource.CreateSoapService16x16.GetHicon();
          }
      }
    }

    private bool isActive()
    {
      CopernicusPortBindingNodeProperties nodeProperties = this.NodeProperties as CopernicusPortBindingNodeProperties;
      return nodeProperties != null && (nodeProperties.ActivationStatus == "Runtime objects up to date" || nodeProperties.ActivationStatus == "Active");
    }

    public override string GetEditLabel()
    {
      return (string) null;
    }

    protected override NodeProperties CreatePropertiesObject()
    {
      return (NodeProperties) new CopernicusFileNodeProperties((HierarchyNode) this);
    }

    public override int SetEditLabel(string label)
    {
      if (!string.IsNullOrEmpty(this.Url))
        return 1;
      return base.SetEditLabel(label);
    }

    public static bool IsDeleteItemInProcess
    {
      get
      {
        return AuthoringMashupFileNode.isDeleteItemInProcess;
      }
      set
      {
        if (value == AuthoringMashupFileNode.isDeleteItemInProcess)
          return;
        AuthoringMashupFileNode.isDeleteItemInProcess = value;
      }
    }

    protected override int ExecCommandOnNode(Guid cmdGroup, uint cmd, uint nCmdexecopt, IntPtr pvaIn, IntPtr pvaOut)
    {
      if (cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupCommandNode && ((int) cmd == 769 || (int) cmd == 770) && this._type != AuthoringType.MashupPortBinding)
      {
        string EV_SUCCESS;
        string EV_ENABLED;
        MashupNodeMenusFilter.isKeyUserEnabled(out EV_SUCCESS, out EV_ENABLED);
        if (EV_SUCCESS != "X" || EV_ENABLED != "X")
        {
          if (!CopernicusMessageBox.Show(SAP.Copernicus.Extension.Mashup.Resource.EnableAdminMode, SAP.Copernicus.Extension.Mashup.Resource.MsgBYD, MessageBoxButtons.YesNo).Equals((object) DialogResult.Yes))
            return 0;
          MashupNodeMenusFilter.setKeyUser("X");
        }
      }
      if (cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupCommandNode)
      {
        if ((int) cmd == 769)
        {
          if (this._type != AuthoringType.MashupPortBinding)
          {
            IDictionary<string, string> fileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(this.element.FullName);
            string strMashupItemId = fileAttributes["KEY"];
            string strMashupItemType = string.Empty;
            if (this._type == AuthoringType.MashupAuthoring)
              strMashupItemType = fileAttributes["MASHUP_TYPE"];
            else if (this._type == AuthoringType.ServiceAuthoring)
              strMashupItemType = fileAttributes["SERVICE_TYPE"];
            if (strMashupItemId == null || strMashupItemType.Equals(string.Empty))
              throw new InvalidOperationException("MashupItemId shouldn't be null");
            ThreadPool.QueueUserWorkItem((WaitCallback) (state => WebBrowserProxy.getInstance().AsynEditMashupItemOnLine(strMashupItemType, strMashupItemId)));
          }
          else
          {
            string empty = string.Empty;
            UIComponentLoaderForUIDesigner.OpenUIFloorPlan_UIDesigner(CopernicusProjectSystemUtil.getSelectedProject().GetXrepPathforMashupNodes(this.element.FullName), false);
          }
        }
        else if ((int) cmd == 770)
        {
          IDictionary<string, string> fileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(this.element.FullName);
          if (MessageBox.Show(string.Format(SAP.Copernicus.Extension.Mashup.Resource.CanDeleteItem, (object) this.Caption), SAP.Copernicus.Extension.Resources.MsgBYD, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.Cancel)
            return 0;
          string strMashupItemId = fileAttributes["KEY"];
          if (this._type == AuthoringType.MashupAuthoring || this._type == AuthoringType.ServiceAuthoring)
          {
            FLX_MASH_T_PIPE_USAGE[] referencedUiComponents = new JSONMashupPipeWhereUsedQueryHandler().GetReferencedUIComponents(strMashupItemId);
            if (referencedUiComponents != null && ((IEnumerable<FLX_MASH_T_PIPE_USAGE>) referencedUiComponents).Count<FLX_MASH_T_PIPE_USAGE>() > 0 && MessageBox.Show(string.Format(SAP.Copernicus.Extension.Mashup.Resource.ReferencedByUIs, (object) this.element.Name), SAP.Copernicus.Extension.Resources.MsgBYD, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.Cancel)
              return 0;
          }
          AuthoringMashupFileNode.isDeleteItemInProcess = true;
          ThreadPool.QueueUserWorkItem((WaitCallback) (state =>
          {
            ErrorSink.Instance.ClearTasksFor(Origin.Unknown);
            if (this._type == AuthoringType.MashupAuthoring)
            {
              JSONMashupPipeDeleteHandler pipeDeleteHandler = new JSONMashupPipeDeleteHandler();
              CopernicusStatusBar.Instance.StartProcess(string.Format("Start Deleting Mashup Item - {0}", (object) strMashupItemId));
              pipeDeleteHandler.BeginInvokeMashupPipeDelete(new Func<MashupPipeDeleteFunction, string, bool?>(this.MashupPipeDeletionCallBack), strMashupItemId);
            }
            else if (this._type == AuthoringType.ServiceAuthoring)
            {
              JSONMashupServiceDeleteHandler serviceDeleteHandler = new JSONMashupServiceDeleteHandler();
              CopernicusStatusBar.Instance.StartProcess(string.Format("Start Deleting Service Item - {0}", (object) strMashupItemId));
              serviceDeleteHandler.BeginInvokeServicePipeDelete(new Func<MashupServiceDeleteFunction, string, bool?>(this.ServicePipeDeletionCallBack), strMashupItemId);
            }
            else
            {
              if (this._type != AuthoringType.MashupPortBinding)
                return;
              base.Remove(true);
            }
          }));
        }
        else
        {
          string selectedNodeName = CopernicusProjectSystemUtil.getSelectedNodeName();
          string pathforLocalFile = XRepMapper.GetInstance().GetXrepPathforLocalFile(this.Url);
          List<string> stringList = new List<string>();
          switch ((int) cmd - 1281)
          {
            case 0:
              stringList.Add(pathforLocalFile);
              int num1 = (int) new ExportText(stringList) { FileName = selectedNodeName }.ShowDialog();
              return 0;
            case 1:
              stringList.Add(pathforLocalFile);
              int num2 = (int) new ImportText(stringList).ShowDialog();
              return 0;
            case 2:
              stringList.Add(pathforLocalFile);
              int num3 = (int) new CheckTranslation(stringList, false).ShowDialog();
              return 0;
          }
        }
        return 0;
      }
      if ((int) cmd != 2)
        return 0;
      if (this._type == AuthoringType.MashupPortBinding)
      {
        if ((Connection.getInstance().UsageMode == UsageMode.OneOff || Connection.getInstance().UsageMode == UsageMode.miltiCustomer) && XRepMapper.GetInstance().GetOpenSolutionStatus() != "In Development")
          return 1;
        if (Connection.getInstance().UsageMode == UsageMode.Scalable)
        {
          if (XRepMapper.GetInstance().GetSolutionEditStatus() == XRepMapper.EditModes.InMaintenance)
            return 0;
          if (XRepMapper.GetInstance().GetOpenSolutionStatus() != "In Development")
            return 1;
        }
        NodeEventHandlerRegistry.NodeDoubleClickDelegate clickDelegateForExtn = NodeEventHandlerRegistry.getDoubleClickDelegateForExtn(".ui");
        if (clickDelegateForExtn != null)
          clickDelegateForExtn((HierarchyNode) this);
        return 1;
      }
      IDictionary<string, string> fileAttributes1 = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(this.element.FullName);
      string strMashupItemId1 = fileAttributes1["KEY"];
      string strMashupItemType1 = string.Empty;
      if (this._type == AuthoringType.MashupAuthoring)
        strMashupItemType1 = fileAttributes1["MASHUP_TYPE"];
      else if (this._type == AuthoringType.ServiceAuthoring)
        strMashupItemType1 = fileAttributes1["SERVICE_TYPE"];
      if (strMashupItemId1 == null || strMashupItemType1.Equals(string.Empty))
        throw new InvalidOperationException("MashupItemId shouldn't be null");
      ThreadPool.QueueUserWorkItem((WaitCallback) (state => WebBrowserProxy.getInstance().AsynEditMashupItemOnLine(strMashupItemType1, strMashupItemId1)));
      return 0;
    }

    protected override int QueryStatusOnNode(Guid cmdGroup, uint cmd, IntPtr pCmdText, ref QueryStatusResult result)
    {
      if (this.productVersion == null)
        this.productVersion = CopernicusProjectSystemUtil.UpdateProductVersion();
      if (cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.cmdSetMashupCommandNode)
      {
        if ((int) cmd == 769)
        {
          result = QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
          if ((int) cmd == 769 && this._type != AuthoringType.MashupPortBinding)
            WebBrowserProxy.getInstance().AsynInvokeSessionValidation();
        }
        else if ((int) cmd == 770)
        {
          if (XRepMapper.GetInstance().IsSolutionEditable() && !SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().GetShippedSolutionFiles().Contains(new FileInfo(this.Url).Name))
            result = QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
          else
            result |= QueryStatusResult.SUPPORTED | QueryStatusResult.NINCHED;
        }
        else
        {
          if ((int) cmd == 1283 || (int) cmd == 1281)
          {
            result = QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
            return 0;
          }
          if ((int) cmd == 1282)
          {
            if (!this.productVersion.PRODUCT_VERSION.EDIT_STATUS.Equals("2"))
            {
              result = QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
              return 0;
            }
            result = QueryStatusResult.SUPPORTED | QueryStatusResult.NINCHED;
            return 0;
          }
        }
        return 0;
      }
      if (cmdGroup == SAP.Copernicus.Extension.PkgCmdIDList.guidCopernicusCmdSet)
      {
        if ((int) cmd == 401)
        {
          if (!AuthoringMashupFileNode.isDeleteItemInProcess)
            result |= QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
          else
            result |= QueryStatusResult.SUPPORTED | QueryStatusResult.INVISIBLE;
        }
        return 0;
      }
      if (!(cmdGroup == Microsoft.VisualStudio.Project.VsMenus.guidStandardCommandSet97))
        return MashupNodeMenusFilter.FilterQueryStatusOnNode(cmdGroup, cmd, pCmdText, ref result);
      if ((int) cmd == 1058)
        result |= QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
      return 0;
    }

    private bool? ServicePipeDeletionCallBack(MashupServiceDeleteFunction servicePipeFunc, string strServicePipeId)
    {
      bool flag = true;
      MashupServiceDeleteFunction serviceDeleteFunction = servicePipeFunc;
      if (serviceDeleteFunction != null && serviceDeleteFunction.Exporting != null)
      {
        if (serviceDeleteFunction.Exporting.EV_RETURN_CODE != 0)
        {
          flag = false;
          IVsStatusbar globalService = (IVsStatusbar) Package.GetGlobalService(typeof (SVsStatusbar));
          globalService.FreezeOutput(0);
          if (serviceDeleteFunction.Exporting.EV_RETURN_CODE == 403)
            ThreadPool.QueueUserWorkItem((WaitCallback) (state =>
            {
              SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().DeleteAttributesInmemory(new List<string>((IEnumerable<string>) new string[1]
              {
                this.Url
              }));
              FileInfo fileInfo = new FileInfo(this.Url);
              if (fileInfo.Attributes != FileAttributes.Normal)
                fileInfo.Attributes = FileAttributes.Normal;
              fileInfo.Delete();
              WebBrowserProxy.getInstance().DeleteMashupItemOnLineByMashupId(strServicePipeId);
            }));
          int pfFrozen;
          globalService.IsFrozen(out pfFrozen);
          if (pfFrozen == 0)
          {
            CopernicusStatusBar.Instance.EndProcess(string.Format("ServicePipe Deletion Return Code - {0}", (object) serviceDeleteFunction.Exporting.EV_RETURN_CODE));
            Thread.Sleep(AuthoringMashupFileNode.iThreadDelay);
            CopernicusStatusBar.Instance.ShowMessage("");
            globalService.FreezeOutput(1);
          }
        }
        else
        {
          IVsStatusbar globalService = (IVsStatusbar) Package.GetGlobalService(typeof (SVsStatusbar));
          globalService.FreezeOutput(0);
          int pfFrozen;
          globalService.IsFrozen(out pfFrozen);
          if (pfFrozen == 0)
          {
            CopernicusStatusBar.Instance.EndProcess(string.Format("ServicePipe Deletion Return Code - {0}", (object) serviceDeleteFunction.Exporting.EV_RETURN_CODE));
            Thread.Sleep(AuthoringMashupFileNode.iThreadDelay);
            CopernicusStatusBar.Instance.ShowMessage("");
            globalService.FreezeOutput(1);
          }
          int num = (int) CopernicusMessageBox.Show("Deletion performed partially. Some authorizations are missing");
        }
      }
      return new bool?(flag);
    }

    private bool? MashupPipeDeletionCallBack(MashupPipeDeleteFunction pipeFunc, string strPipeId)
    {
      bool flag = true;
      MashupPipeDeleteFunction pipeDeleteFunction = pipeFunc;
      if (pipeDeleteFunction != null && pipeDeleteFunction.Exporting != null)
      {
        if (pipeDeleteFunction.Exporting.EV_RETURN_CODE != 0)
        {
          flag = false;
          IVsStatusbar globalService = (IVsStatusbar) Package.GetGlobalService(typeof (SVsStatusbar));
          globalService.FreezeOutput(0);
          if (pipeDeleteFunction.Exporting.EV_RETURN_CODE == 403)
          {
            ThreadPool.QueueUserWorkItem((WaitCallback) (state =>
            {
              SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().DeleteAttributesInmemory(new List<string>((IEnumerable<string>) new string[1]
              {
                this.Url
              }));
              FileInfo fileInfo = new FileInfo(this.Url);
              if (fileInfo.Attributes != FileAttributes.Normal)
                fileInfo.Attributes = FileAttributes.Normal;
              fileInfo.Delete();
              WebBrowserProxy.getInstance().DeleteMashupItemOnLineByMashupId(strPipeId);
            }));
          }
          else
          {
            List<SAP.Copernicus.Core.ErrorList.Task> taskList = new List<SAP.Copernicus.Core.ErrorList.Task>();
            foreach (OSLS_XREP_RETURN_MSG oslsXrepReturnMsg in pipeDeleteFunction.Exporting.ET_RETURN_MSG)
              taskList.Add(new SAP.Copernicus.Core.ErrorList.Task(oslsXrepReturnMsg.MESSAGE, Origin.Unknown, Severity.Error, (EventHandler) null));
            ErrorSink.Instance.AddTasks((IEnumerable<SAP.Copernicus.Core.ErrorList.Task>) taskList);
          }
          int pfFrozen;
          globalService.IsFrozen(out pfFrozen);
          if (pfFrozen == 0)
          {
            CopernicusStatusBar.Instance.EndProcess(string.Format("MashupPipe Deletion Return Code - {0}", (object) pipeDeleteFunction.Exporting.EV_RETURN_CODE));
            Thread.Sleep(AuthoringMashupFileNode.iThreadDelay);
            CopernicusStatusBar.Instance.ShowMessage("");
            globalService.FreezeOutput(1);
          }
        }
        else
        {
          IVsStatusbar globalService = (IVsStatusbar) Package.GetGlobalService(typeof (SVsStatusbar));
          globalService.FreezeOutput(0);
          int pfFrozen;
          globalService.IsFrozen(out pfFrozen);
          if (pfFrozen == 0)
          {
            CopernicusStatusBar.Instance.EndProcess(string.Format("MashupPipe Deletion Return Code - {0}", (object) pipeDeleteFunction.Exporting.EV_RETURN_CODE));
            Thread.Sleep(AuthoringMashupFileNode.iThreadDelay);
            CopernicusStatusBar.Instance.ShowMessage("");
            globalService.FreezeOutput(1);
          }
          int num = (int) CopernicusMessageBox.Show("Deletion performed partially. Some authorizations are missing");
        }
      }
      return new bool?(flag);
    }

    protected override void UpdateSccStateIcons()
    {
      if (this.ExcludeNodeFromScc)
        return;
      this.ReDraw(UIHierarchyElement.SccState);
    }

    public void updateSccStateIcons()
    {
      this.UpdateSccStateIcons();
    }

    public override VsStateIcon StateIconIndex
    {
      get
      {
        CopernicusProjectNode projectMgr = this.ProjectMgr as CopernicusProjectNode;
        if (projectMgr != null)
        {
          this.Consistent = projectMgr.IsNodeConsistent(this.Url);
          switch (projectMgr.IsLocked(this.Url))
          {
            case LockStatus.LockedByMe:
              return VsStateIcon.STATEICON_CHECKEDOUT;
            case LockStatus.LockedByOther:
              return VsStateIcon.STATEICON_CHECKEDOUTSHAREDOTHER;
          }
        }
        return VsStateIcon.STATEICON_NOSTATEICON;
      }
    }

    internal void UpdateDependentUIComponent()
    {
      if (this._type == AuthoringType.MashupAuthoring)
      {
        this._type = AuthoringType.MashupAuthoring;
        ThreadPool.QueueUserWorkItem((WaitCallback) (newstate => new JSONMashupPipeWhereUsedQueryHandler().BeginInvokeMashupPipeWhereUsed(new Func<MashupPipeWhereUsedQueryFunction, string, bool?>(this.RetrieveDependentUIMashupComponent), this.strMashupItemId)));
      }
      else
      {
        if (this._type != AuthoringType.ServiceAuthoring)
          return;
        this._type = AuthoringType.ServiceAuthoring;
        ThreadPool.QueueUserWorkItem((WaitCallback) (newstate => new JSONMashupServiceWhereUsedQueryHandler().BeginInvokeMashupServiceWhereUsed(new Func<MashupServiceWhereUsedQueryFunction, string, bool?>(this.RetrieveDependentServiceUIComponent), this.strMashupItemId)));
      }
    }
  }
}
