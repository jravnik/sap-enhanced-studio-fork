﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler.ChangeTransactionHelper
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Extension.Mashup.HtmlMashup.MashupJsonUtil.MashupHandler;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler
{
  public class ChangeTransactionHelper
  {
    private static string strTransactionFolderBar = "/SAP_BYD_UI_FLEX/CHANGE_TRANSACTIONS/Partner/AddMashupButtonIntoFunctionBar/";
    private static string strTransactionPort = "/SAP_BYD_UI_FLEX/CHANGE_TRANSACTIONS/Partner/AddMashupPaneWithoutPort/";
    private static ChangeTransactionHelper _instance;

    private ChangeTransactionHelper()
    {
    }

    public static ChangeTransactionHelper Instance
    {
      get
      {
        if (ChangeTransactionHelper._instance == null)
          ChangeTransactionHelper._instance = new ChangeTransactionHelper();
        return ChangeTransactionHelper._instance;
      }
    }

    public int getChangeTransactionCount()
    {
      return this.ReloadMashupdependency(ChangeTransactionHelper.strTransactionFolderBar) + this.ReloadMashupdependency(ChangeTransactionHelper.strTransactionPort);
    }

    public int ReloadMashupdependency(string xrepPath)
    {
      return new JSONOSLSXREP_LIST_CONTENT().invokeReadChangeTransaction(xrepPath);
    }
  }
}
