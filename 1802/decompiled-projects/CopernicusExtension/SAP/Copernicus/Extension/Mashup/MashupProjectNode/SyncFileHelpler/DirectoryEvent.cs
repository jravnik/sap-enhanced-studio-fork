﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler.DirectoryEvent
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System.IO;

namespace SAP.Copernicus.Extension.Mashup.MashupProjectNode.SyncFileHelpler
{
  public class DirectoryEvent
  {
    private FileInfo _localFile;
    private WatcherChangeTypes _type;

    public DirectoryEvent(FileInfo localFile, WatcherChangeTypes type)
    {
      this._localFile = localFile;
      this._type = type;
    }

    public override string ToString()
    {
      return this._localFile.FullName + (object) this._type;
    }

    public override int GetHashCode()
    {
      return this.ToString().GetHashCode();
    }
  }
}
