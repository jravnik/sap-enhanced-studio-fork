﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.SelectionUtil.FurtherUsageSelectionWizard
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.Core.Wizard;
using SAP.CopernicusProjectView;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.SelectionUtil
{
    public class FurtherUsageSelectionWizard : CopernicusWizardSheet, IWizard
    {
        private readonly string projectNamespace;
        private readonly string BOName;
        public selTabs selTab;

        public FurtherUsageSelectionWizard(selTabs tabMode)
        {
            this.selTab = tabMode;
            this.InitializeComponent();
            this.Text = this.getHeaderText();
            this.projectNamespace = CopernicusProjectSystemUtil.getSelectedProject().RepositoryNamespace;
            this.BOName = CopernicusProjectSystemUtil.getSelectedNodeName();
            this.Pages.Add((object)new FurtherUsageSelectionFirstPage(this));
            this.helpid = HELP_IDS.BDS_BOEXTENSION_CREATE;
        }

        void IWizard.BeforeOpeningFile(ProjectItem projectItem)
        {
            Trace.WriteLine("Enter here");
        }

        void IWizard.ProjectFinishedGenerating(Project project)
        {
            Trace.WriteLine("Enter here");
        }

        void IWizard.ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
            Trace.WriteLine("Enter here");
        }

        void IWizard.RunFinished()
        {
            int num = (int)this.ShowDialog();
        }

        void IWizard.RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
        }

        bool IWizard.ShouldAddProjectItem(string filePath)
        {
            return false;
        }

        private void InitializeComponent()
        {
            this.buttonPanel.SuspendLayout();
            this.SuspendLayout();
            this.AcceptButton = (IButtonControl)this.nextButton;
            this.ClientSize = new Size(384, 141);
            this.Name = nameof(FurtherUsageSelectionWizard);
            this.Text = nameof(FurtherUsageSelectionWizard);
            this.buttonPanel.ResumeLayout(false);
            this.buttonPanel.PerformLayout();
            this.ResumeLayout(false);
        }

        public void FinishWizard(KutFieldMetaInfo fieldInfo)
        {
            ConnectionDataSet.SystemDataRow connectedSystem = Connection.getInstance().getConnectedSystem();
            int client = connectedSystem.Client;
            string str1;
            if (client < 100)
            {
                str1 = "0" + (object)client;
                if (client < 10)
                    str1 = "0" + str1;
            }
            else
                str1 = string.Concat((object)client);
            string language = connectedSystem.Language;
            string str2 = "/sap/public/ap/ui/repository/SAP_UI/HTML5/client.html?";
            string str3 = "sap-client=" + str1;
            string str4 = "&sap-language=" + language;
            string str5 = "&app.component=/SAP_BYD_UI_REUSE/ExtFurtherUsage/ExtensibilityFurtherUsageComponent.QA.uicomponent";
            string str6 = "&app.inport=OpenExtFieldFurtherUsageByBONode";
            string str7 = "&param.BusinessObjectName=" + fieldInfo.businessObjectName;
            string str8 = "&param.BusinessObjectNamespace=" + fieldInfo.businessObjectNamespace;
            string str9 = "&param.BusinessObjectNodeName=" + fieldInfo.businessObjectNodeName;
            string str10 = "&param.EsrFieldName=" + fieldInfo.esrFieldName;
            string str11 = "&param.EsrNamespace=" + fieldInfo.esrFieldNamespace;
            string str12 = "&param.EsrFieldNameUiText=" + fieldInfo.esrFieldUiText;
            string str13 = "&param.BONodeUIText=" + fieldInfo.businessObjectNodeUiText;
            string str14 = "&param.FieldHasReferenceBONode=" + fieldInfo.fieldHasReferenceBoNode;
            string path;
            if (this.selTab == selTabs.EntSearch)
            {
                string str15 = "&param.ShowEntSearch=X&param.ShowReports=&param.ShowForms=";
                path = str2 + str3 + str4 + str5 + str6 + str7 + str8 + str9 + str10 + str11 + str12 + str13 + str15;
            }
            else
            {
                string str15 = "&param.ShowEntSearch=&param.ShowReports=X&param.ShowForms=";
                path = str2 + str3 + str4 + str5 + str6 + str7 + str8 + str9 + str10 + str11 + str12 + str13 + str14 + str15;
            }

            string url = WebUtil.BuildURL(Connection.getInstance().getConnectedSystem(), path);

            Clipboard.SetText(url);

            if (!PropertyAccess.GeneralProps.XBOEnhancementNoBrowser)
            {
                ExternalApplication.LaunchExternalBrowser(url);
            }
        }

        private string getHeaderText()
        {
            return this.selTab != selTabs.EntSearch ? "Enhance Report" : "Enhance Enterprise Search";
        }
    }
}
