﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.SelectionUtil.FormExtFieldsSelectionWizard
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.Core.Wizard;
using SAP.CopernicusProjectView;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.SelectionUtil
{
    public class FormExtFieldsSelectionWizard : CopernicusWizardSheet, IWizard
    {
        private readonly string projectNamespace;
        private readonly string BOName;

        public FormExtFieldsSelectionWizard()
        {
            this.InitializeComponent();
            this.projectNamespace = CopernicusProjectSystemUtil.getSelectedProject().RepositoryNamespace;
            this.BOName = CopernicusProjectSystemUtil.getSelectedNodeName();
            this.Pages.Add((object)new FormExtFieldsSelectionFirstPage(this));
            this.helpid = HELP_IDS.BDS_BOEXTENSION_CREATE;
        }

        void IWizard.BeforeOpeningFile(ProjectItem projectItem)
        {
            Trace.WriteLine("Enter here");
        }

        void IWizard.ProjectFinishedGenerating(Project project)
        {
            Trace.WriteLine("Enter here");
        }

        void IWizard.ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
            Trace.WriteLine("Enter here");
        }

        void IWizard.RunFinished()
        {
            int num = (int)this.ShowDialog();
        }

        void IWizard.RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
        }

        bool IWizard.ShouldAddProjectItem(string filePath)
        {
            return false;
        }

        private void InitializeComponent()
        {
            this.buttonPanel.SuspendLayout();
            this.SuspendLayout();
            this.AcceptButton = (IButtonControl)this.nextButton;
            this.ClientSize = new Size(384, 141);
            this.Name = nameof(FormExtFieldsSelectionWizard);
            this.Text = "Enhance Form";
            this.buttonPanel.ResumeLayout(false);
            this.buttonPanel.PerformLayout();
            this.ResumeLayout(false);
        }

        public void FinishWizard(KutFieldMetaInfo fieldInfo)
        {
            ConnectionDataSet.SystemDataRow connectedSystem = Connection.getInstance().getConnectedSystem();
            int client = connectedSystem.Client;
            string str;
            if (client < 100)
            {
                str = "0" + (object)client;
                if (client < 10)
                    str = "0" + str;
            }
            else
                str = string.Concat((object)client);
            string language = connectedSystem.Language;

            string url = WebUtil.BuildURL(Connection.getInstance().getConnectedSystem(), "/sap/public/ap/ui/repository/SAP_UI/HTML5/client.html?" + ("sap-client=" + str) + ("&sap-language=" + language) + "&app.component=/SAP_BYD_UI_REUSE/ExtFurtherUsage/ExtensibilityFurtherUsageComponent.QA.uicomponent" + "&app.inport=OpenExtFieldFurtherUsageByBONode" + ("&param.BusinessObjectName=" + fieldInfo.businessObjectName) + ("&param.BusinessObjectNamespace=" + fieldInfo.businessObjectNamespace) + ("&param.BusinessObjectNodeName=" + fieldInfo.businessObjectNodeName) + ("&param.EsrFieldName=" + fieldInfo.esrFieldName) + ("&param.EsrNamespace=" + fieldInfo.esrFieldNamespace) + ("&param.EsrFieldNameUiText=" + fieldInfo.esrFieldUiText) + ("&param.BONodeUIText=" + fieldInfo.businessObjectNodeUiText) + "&param.ShowEntSearch=&param.ShowReports=&param.ShowForms=X");

            Clipboard.SetText(url);

            if (!PropertyAccess.GeneralProps.XBOEnhancementNoBrowser)
            {
                ExternalApplication.LaunchExternalBrowser(url);
            }
        }
    }
}
