﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.SelectionUtil.BOSelectionFirstPage
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Repository.DataModel;
using SAP.Copernicus.Core.Wizard;
using SAP.Copernicus.Model;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.SelectionUtil
{
  public class BOSelectionFirstPage : InternalWizardPage
  {
    private static string BONSALL = "";
    private Label lbl_Namespace;
    private Label lbl_BO;
    private ComboBox cmbBox_BO;
    private Label label4;
    private Label label3;
    private Label lbl_error;
    private ComboBox cmbBox_Namespace;
    private BOSelectionWizard parentWizard;
    private List<PDI_EXT_S_EXT_NODE> nodeList;

    private void InitializeComponent()
    {
      this.cmbBox_Namespace = new ComboBox();
      this.lbl_Namespace = new Label();
      this.lbl_BO = new Label();
      this.cmbBox_BO = new ComboBox();
      this.label3 = new Label();
      this.label4 = new Label();
      this.lbl_error = new Label();
      this.SuspendLayout();
      this.Banner.Margin = new Padding(2);
      this.Banner.Size = new Size(564, 50);
      this.Banner.Subtitle = "Select a business object to extend";
      this.Banner.Title = "Select Business Object";
      this.cmbBox_Namespace.AutoCompleteMode = AutoCompleteMode.Append;
      this.cmbBox_Namespace.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.cmbBox_Namespace.FormattingEnabled = true;
      this.cmbBox_Namespace.Location = new Point(136, 63);
      this.cmbBox_Namespace.Margin = new Padding(2);
      this.cmbBox_Namespace.Name = "cmbBox_Namespace";
      this.cmbBox_Namespace.Size = new Size(388, 21);
      this.cmbBox_Namespace.Sorted = true;
      this.cmbBox_Namespace.TabIndex = 1;
      this.cmbBox_Namespace.SelectedIndexChanged += new EventHandler(this.cmbBox_Namespace_SelectedIndexChanged);
      this.lbl_Namespace.AutoSize = true;
      this.lbl_Namespace.Location = new Point(16, 66);
      this.lbl_Namespace.Margin = new Padding(2, 0, 2, 0);
      this.lbl_Namespace.Name = "lbl_Namespace";
      this.lbl_Namespace.Size = new Size(67, 13);
      this.lbl_Namespace.TabIndex = 2;
      this.lbl_Namespace.Text = "Namespace:";
      this.lbl_BO.AutoSize = true;
      this.lbl_BO.Location = new Point(16, 94);
      this.lbl_BO.Margin = new Padding(2, 0, 2, 0);
      this.lbl_BO.Name = "lbl_BO";
      this.lbl_BO.Size = new Size(86, 13);
      this.lbl_BO.TabIndex = 3;
      this.lbl_BO.Text = "Business Object:";
      this.cmbBox_BO.AutoCompleteMode = AutoCompleteMode.Append;
      this.cmbBox_BO.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.cmbBox_BO.BackColor = SystemColors.Window;
      this.cmbBox_BO.ForeColor = SystemColors.WindowText;
      this.cmbBox_BO.FormattingEnabled = true;
      this.cmbBox_BO.Location = new Point(136, 91);
      this.cmbBox_BO.Margin = new Padding(2);
      this.cmbBox_BO.Name = "cmbBox_BO";
      this.cmbBox_BO.Size = new Size(388, 21);
      this.cmbBox_BO.Sorted = true;
      this.cmbBox_BO.TabIndex = 4;
      this.cmbBox_BO.SelectedIndexChanged += new EventHandler(this.cmbBox_BO_SelectedIndexChanged);
      this.label3.AutoSize = true;
      this.label3.BackColor = SystemColors.Control;
      this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.label3.ForeColor = Color.Red;
      this.label3.Location = new Point(87, 66);
      this.label3.Margin = new Padding(2, 0, 2, 0);
      this.label3.Name = "label3";
      this.label3.Size = new Size(12, 13);
      this.label3.TabIndex = 5;
      this.label3.Text = "*";
      this.label4.AutoSize = true;
      this.label4.BackColor = SystemColors.Control;
      this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.label4.ForeColor = Color.Red;
      this.label4.Location = new Point(106, 94);
      this.label4.Margin = new Padding(2, 0, 2, 0);
      this.label4.Name = "label4";
      this.label4.Size = new Size(12, 13);
      this.label4.TabIndex = 6;
      this.label4.Text = "*";
      this.lbl_error.AutoSize = true;
      this.lbl_error.ForeColor = Color.Red;
      this.lbl_error.Location = new Point(16, 125);
      this.lbl_error.Margin = new Padding(2, 0, 2, 0);
      this.lbl_error.Name = "lbl_error";
      this.lbl_error.Size = new Size(35, 13);
      this.lbl_error.TabIndex = 7;
      this.lbl_error.Text = "label1";
      this.lbl_error.Click += new EventHandler(this.lbl_error_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.label4);
      this.Controls.Add((Control) this.lbl_BO);
      this.Controls.Add((Control) this.lbl_Namespace);
      this.Controls.Add((Control) this.lbl_error);
      this.Controls.Add((Control) this.cmbBox_BO);
      this.Controls.Add((Control) this.cmbBox_Namespace);
      this.Margin = new Padding(2);
      this.Name = nameof (BOSelectionFirstPage);
      this.Size = new Size(564, 155);
      this.SetActive += new CancelEventHandler(this.BOSelectionFirstPage_SetActive);
      this.WizardBack += new WizardPageEventHandler(this.BOSelectionFirstPage_WizardBack);
      this.WizardFinish += new CancelEventHandler(this.BOSelectionFirstPage_WizardFinish);
      this.Controls.SetChildIndex((Control) this.cmbBox_Namespace, 0);
      this.Controls.SetChildIndex((Control) this.cmbBox_BO, 0);
      this.Controls.SetChildIndex((Control) this.lbl_error, 0);
      this.Controls.SetChildIndex((Control) this.lbl_Namespace, 0);
      this.Controls.SetChildIndex((Control) this.lbl_BO, 0);
      this.Controls.SetChildIndex((Control) this.label4, 0);
      this.Controls.SetChildIndex((Control) this.label3, 0);
      this.Controls.SetChildIndex((Control) this.Banner, 0);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public BOSelectionFirstPage(BOSelectionWizard p1)
      : base(HELP_IDS.BDS_BOEXTENSION_CREATE)
    {
      this.parentWizard = p1;
      this.InitializeComponent();
      this.FetchNamespaceFromCache();
    }

    private void FetchNamespaceFromCache()
    {
      List<string> namespaces = RepositoryDataCache.GetInstance().GetNamespaces();
      string repositoryNamespace = CopernicusProjectSystemUtil.getSelectedProject().RepositoryNamespace;
      string str = "http://sap.com/xi/AP/FO/Product/Global";
      for (int index = 0; index < namespaces.Count; ++index)
      {
        if (namespaces[index] == repositoryNamespace)
          namespaces.RemoveAt(index);
        else if (namespaces[index] == str)
          namespaces.RemoveAt(index);
      }
      this.cmbBox_Namespace.Items.Add((object) BOSelectionFirstPage.BONSALL);
      this.cmbBox_BO.Items.Add((object) BOSelectionFirstPage.BONSALL);
      using (List<string>.Enumerator enumerator = namespaces.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string NS = enumerator.Current;
          if (RepositoryDataCache.GetInstance().RepositoryDataSet.Tables["BusinessObjects"].AsEnumerable().Where<DataRow>((Func<DataRow, bool>) (businessObject =>
          {
            if (businessObject.Field<string>("NSName") == NS && (businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.BusinessTransactionDocument.GetCodeValue() || businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.MasterDataObject.GetCodeValue() || (businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.MassDataRunObject.GetCodeValue() || businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.BusinessAdministrationObject.GetCodeValue()) || businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.TechnicalObject.GetCodeValue()))
              return businessObject.Field<string>("TechCategory") == BusinessObjectTechnicalCategoryCodeEnum.StandardBusinessObject.GetCodeValue();
            return false;
          })).Count<DataRow>() != 0)
            this.cmbBox_Namespace.Items.Add((object) NS);
        }
      }
    }

    private void BOSelectionFirstPage_SetActive(object sender, CancelEventArgs e)
    {
      this.lbl_BO.ForeColor = Color.Black;
      this.lbl_error.Hide();
      this.SetWizardButtons(WizardPageType.FirstAndLast);
      this.EnableFinishButton(false);
    }

    private void BOSelectionFirstPage_WizardFinish(object sender, CancelEventArgs e)
    {
      string selectedItem1 = this.cmbBox_Namespace.SelectedItem as string;
      if (selectedItem1 == null)
      {
        this.SetWizardButtons(WizardPageType.FirstAndLast);
        e.Cancel = true;
        this.EnableFinishButton(false);
      }
      else
      {
        string str1 = selectedItem1.Replace("http://sap.com/xi/", string.Empty).Replace("/", ".");
        string selectedItem2 = this.cmbBox_BO.SelectedItem as string;
        if (selectedItem2 != null)
        {
          CopernicusProjectSystemUtil.getSelectedProject();
          bool OfflineEnabled;
          string str2 = new ExtensionHandler().checkBOExistence(ProjectUtil.GetOpenSolutionName(), selectedItem2, str1, out OfflineEnabled);
          if (str2 != string.Empty)
          {
            if (str2 == "1")
              this.lbl_error.Text = string.Format(SAP.Copernicus.Extension.Resources.BOSelectionBOexists, (object) selectedItem2);
            if (str2 == "2")
              this.lbl_error.Text = string.Format(SAP.Copernicus.Extension.Resources.BOSelectionTemplateBO, (object) selectedItem2);
            if (str2 == "3")
              this.lbl_error.Text = string.Format(SAP.Copernicus.Extension.Resources.BOSelectionNoExtallowed, (object) selectedItem2);
            this.lbl_error.Show();
            this.lbl_BO.ForeColor = Color.Red;
            this.SetWizardButtons(WizardPageType.FirstAndLast);
            this.EnableFinishButton(false);
            e.Cancel = true;
          }
          else
          {
            this.nodeList = ExtensionDataCache.GetInstance().GetBONodesData(str1, selectedItem2);
            if (this.nodeList.Count < 0)
            {
              this.lbl_error.Text = string.Format(SAP.Copernicus.Extension.Resources.BOSelectionNoExtNodesforBO, (object) selectedItem2);
              this.lbl_error.Show();
              this.lbl_BO.ForeColor = Color.Red;
              this.SetWizardButtons(WizardPageType.FirstAndLast);
              this.EnableFinishButton(false);
              e.Cancel = true;
            }
            else
            {
              this.lbl_error.Hide();
              this.lbl_BO.ForeColor = Color.Black;
              this.cmbBox_BO.BackColor = Color.White;
              this.SetWizardButtons(WizardPageType.FirstAndLast);
              this.EnableFinishButton(true);
              this.parentWizard.FinishWizard(str1, selectedItem2, this.nodeList, OfflineEnabled);
            }
          }
        }
        else
        {
          this.SetWizardButtons(WizardPageType.FirstAndLast);
          this.EnableFinishButton(false);
          e.Cancel = true;
        }
      }
    }

    private void BOSelectionFirstPage_WizardBack(object sender, CopernicusWizardPageEventArgs e)
    {
    }

    private void cmbBox_Namespace_SelectedIndexChanged(object sender, EventArgs e)
    {
      string selBONS = this.cmbBox_Namespace.SelectedItem as string;
      if (!(selBONS != BOSelectionFirstPage.BONSALL))
        return;
      this.cmbBox_BO.Items.Clear();
      IEnumerable<DataRow> source = (IEnumerable<DataRow>) RepositoryDataCache.GetInstance().RepositoryDataSet.Tables["BusinessObjects"].AsEnumerable().Where<DataRow>((Func<DataRow, bool>) (businessObject =>
      {
        if (businessObject.Field<string>("NSName") == selBONS && (businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.BusinessTransactionDocument.GetCodeValue() || businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.MasterDataObject.GetCodeValue() || (businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.MassDataRunObject.GetCodeValue() || businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.BusinessAdministrationObject.GetCodeValue()) || businessObject.Field<string>("ObjectCategory") == BusinessObjectCategoryCodeEnum.TechnicalObject.GetCodeValue()))
          return businessObject.Field<string>("TechCategory") == BusinessObjectTechnicalCategoryCodeEnum.StandardBusinessObject.GetCodeValue();
        return false;
      }));
      if (source.Count<DataRow>() == 0)
      {
        this.lbl_error.Text = string.Format(SAP.Copernicus.Extension.Resources.BOSelectionNoBOforNS);
        this.lbl_error.Show();
        this.lbl_Namespace.ForeColor = Color.Red;
      }
      else
      {
        this.lbl_error.Hide();
        this.lbl_Namespace.ForeColor = Color.Black;
      }
      foreach (RepositoryDataSet.BusinessObjectsRow businessObjectsRow in source)
      {
        if (businessObjectsRow.ProxyName != "ENTITLEMENT_PROD_FINANCIALS" && businessObjectsRow.ProxyName != "ENTITLEMENT_PROD_SALES" && (businessObjectsRow.ProxyName != "INDIVIDUAL_MAT_SERVICE" && businessObjectsRow.ProxyName != "MATERIAL_AVAIL_CONFIRM") && (businessObjectsRow.ProxyName != "MATERIAL_FINANCIALS" && businessObjectsRow.ProxyName != "MATERIAL_INVENTORY" && (businessObjectsRow.ProxyName != "MATERIAL_PROCUREMENT" && businessObjectsRow.ProxyName != "MATERIAL_SALES")) && (businessObjectsRow.ProxyName != "MATERIAL_SUPPLY_PLANNING" && businessObjectsRow.ProxyName != "SERVICE_PROD_FINANCIALS" && (businessObjectsRow.ProxyName != "SERVICE_PROD_PROCUREMENT" && businessObjectsRow.ProxyName != "SERVICE_PROD_SALES") && businessObjectsRow.ProxyName != "WARRANTY_SERVICE"))
          this.cmbBox_BO.Items.Add((object) businessObjectsRow.Name);
      }
      this.cmbBox_Namespace.Items.Remove((object) "");
    }

    private void cmbBox_BO_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.lbl_error.Hide();
      this.lbl_BO.ForeColor = Color.Black;
      this.cmbBox_BO.BackColor = Color.White;
      this.SetWizardButtons(WizardPageType.FirstAndLast);
    }

    private void lbl_error_Click(object sender, EventArgs e)
    {
    }
  }
}
