﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.PkgCmdIDList
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using System;

namespace SAP.Copernicus.Extension
{
  internal class PkgCmdIDList
  {
    public static readonly Guid cmdSetMashupAuthoringNode = new Guid("DFB08056-4C5D-4504-949E-933209AFB79B");
    public static readonly Guid cmdSetMashupServiceNode = new Guid("52F066EA-159D-46A4-8E5C-65B8221B2657");
    public static readonly Guid cmdSetMashupCommandNode = new Guid("91564CAC-F5C6-4EF3-9CAE-17B48B5EC745");
    public static readonly Guid cmdSetMashupPortBindingNode = new Guid("FF3DDEF0-C7DA-4F6B-8F38-67033C86B450");
    public static readonly Guid cmdSetMashupUsage = new Guid("816BF2F5-23A6-4BE0-9115-188E1A4ECEAA");
    public static readonly Guid guidCopernicusCmdSet = new Guid("4b077f76-2fc9-498b-b023-ebb4f61bc505");
    public const uint cmdidRefreshSolution = 401;
    public const uint cmdidCreateHtmlMashup = 256;
    public const uint cmdidCreateURLMashup = 257;
    public const uint cmdidCreateDataMashup = 258;
    public const uint cmdidCreateRssAtomService = 513;
    public const uint cmdidCreateRestService = 514;
    public const uint cmdidCreateSoapService = 515;
    public const uint cmdidCreateMashupPortBinding = 1025;
    public const uint cmdidEditMashupOnQAFBuilder = 769;
    public const uint cmdidDeleteMashupItem = 770;
    public const uint cmdidExportTrans = 1281;
    public const uint cmdidImportTrans = 1282;
    public const uint cmdidCheckTrans = 1283;
    public const uint cmdidOpenFloorPlan = 1285;
  }
}
