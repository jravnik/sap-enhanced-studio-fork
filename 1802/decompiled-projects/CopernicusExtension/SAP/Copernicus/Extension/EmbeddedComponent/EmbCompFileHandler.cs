﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.EmbeddedComponent.EmbCompFileHandler
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Project;

namespace SAP.Copernicus.Extension.EmbeddedComponent
{
  public class EmbCompFileHandler
  {
    private static EmbCompFileHandler instance;

    private EmbCompFileHandler()
    {
    }

    public static EmbCompFileHandler Instance
    {
      get
      {
        if (EmbCompFileHandler.instance == null)
          EmbCompFileHandler.instance = new EmbCompFileHandler();
        return EmbCompFileHandler.instance;
      }
    }

    public void OnFileSave(string filePath, string[] dependentFileNames, string content, ProjectProperties properties)
    {
    }

    public void OnFileDeletion(string filePath)
    {
    }

    public void OnFileActivate(string filepath)
    {
    }
  }
}
