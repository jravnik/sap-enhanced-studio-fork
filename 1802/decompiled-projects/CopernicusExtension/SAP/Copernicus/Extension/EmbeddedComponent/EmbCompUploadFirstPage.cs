﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.EmbeddedComponent.EmbCompUploadFirstPage
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Wizard;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Extension.EmbeddedComponent
{
  public class EmbCompUploadFirstPage : InternalWizardPage
  {
    private Label lbl_EmbCompFSBrowse;
    private Button btn_CPBrowse;
    private TextBox txtCPPath;
    private Label lblAsmName;
    private Label lblAsmType;
    private TextBox txtAsmName;
    private TextBox txtAsmType;
    private Label label1;
    private Label label2;
    private Label label3;
    private EmbCompUploadWizard parentWizard;

    private void InitializeComponent()
    {
      this.lbl_EmbCompFSBrowse = new Label();
      this.label3 = new Label();
      this.btn_CPBrowse = new Button();
      this.txtCPPath = new TextBox();
      this.lblAsmName = new Label();
      this.lblAsmType = new Label();
      this.txtAsmName = new TextBox();
      this.txtAsmType = new TextBox();
      this.label1 = new Label();
      this.label2 = new Label();
      this.SuspendLayout();
      this.Banner.Size = new Size(473, 64);
      this.Banner.Subtitle = "Select the custom pane to upload into your embedded component.";
      this.Banner.Title = "Add Custom Pane to Embedded Component";
      this.Banner.Load += new EventHandler(this.Banner_Load);
      this.lbl_EmbCompFSBrowse.AutoSize = true;
      this.lbl_EmbCompFSBrowse.Location = new Point(33, 77);
      this.lbl_EmbCompFSBrowse.Name = "lbl_EmbCompFSBrowse";
      this.lbl_EmbCompFSBrowse.Size = new Size(73, 13);
      this.lbl_EmbCompFSBrowse.TabIndex = 2;
      this.lbl_EmbCompFSBrowse.Text = "Custom Pane:";
      this.label3.AutoSize = true;
      this.label3.BackColor = SystemColors.Control;
      this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.label3.ForeColor = Color.Red;
      this.label3.Location = new Point(111, 76);
      this.label3.Name = "label3";
      this.label3.Size = new Size(12, 13);
      this.label3.TabIndex = 5;
      this.label3.Text = "*";
      this.btn_CPBrowse.Location = new Point(368, 73);
      this.btn_CPBrowse.Name = "btn_CPBrowse";
      this.btn_CPBrowse.Size = new Size(75, 23);
      this.btn_CPBrowse.TabIndex = 6;
      this.btn_CPBrowse.Text = "Browse";
      this.btn_CPBrowse.UseVisualStyleBackColor = true;
      this.btn_CPBrowse.Click += new EventHandler(this.btn_CPBrowse_Click);
      this.txtCPPath.Location = new Point(130, 74);
      this.txtCPPath.Name = "txtCPPath";
      this.txtCPPath.Size = new Size(232, 20);
      this.txtCPPath.TabIndex = 7;
      this.lblAsmName.AutoSize = true;
      this.lblAsmName.Location = new Point(21, 100);
      this.lblAsmName.Name = "lblAsmName";
      this.lblAsmName.Size = new Size(85, 13);
      this.lblAsmName.TabIndex = 8;
      this.lblAsmName.Text = "Assembly Name:";
      this.lblAsmType.AutoSize = true;
      this.lblAsmType.Location = new Point(25, 125);
      this.lblAsmType.Name = "lblAsmType";
      this.lblAsmType.Size = new Size(81, 13);
      this.lblAsmType.TabIndex = 8;
      this.lblAsmType.Text = "Assembly Type:";
      this.txtAsmName.Location = new Point(130, 97);
      this.txtAsmName.Name = "txtAsmName";
      this.txtAsmName.Size = new Size(232, 20);
      this.txtAsmName.TabIndex = 7;
      this.txtAsmName.Text = "ByDCustomPane.dll";
      this.txtAsmType.Location = new Point(130, 120);
      this.txtAsmType.Name = "txtAsmType";
      this.txtAsmType.Size = new Size(232, 20);
      this.txtAsmType.TabIndex = 7;
      this.txtAsmType.Text = "ByDCustomPane.CustomPane";
      this.label1.AutoSize = true;
      this.label1.BackColor = SystemColors.Control;
      this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.label1.ForeColor = Color.Red;
      this.label1.Location = new Point(112, 100);
      this.label1.Name = "label1";
      this.label1.Size = new Size(12, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "*";
      this.label2.AutoSize = true;
      this.label2.BackColor = SystemColors.Control;
      this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.label2.ForeColor = Color.Red;
      this.label2.Location = new Point(112, 123);
      this.label2.Name = "label2";
      this.label2.Size = new Size(12, 13);
      this.label2.TabIndex = 5;
      this.label2.Text = "*";
      this.Controls.Add((Control) this.lblAsmName);
      this.Controls.Add((Control) this.txtCPPath);
      this.Controls.Add((Control) this.lblAsmType);
      this.Controls.Add((Control) this.btn_CPBrowse);
      this.Controls.Add((Control) this.txtAsmName);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.txtAsmType);
      this.Controls.Add((Control) this.lbl_EmbCompFSBrowse);
      this.Name = nameof (EmbCompUploadFirstPage);
      this.Size = new Size(473, 167);
      this.SetActive += new CancelEventHandler(this.EmbCompUploadFirstPage_SetActive);
      this.WizardBack += new WizardPageEventHandler(this.EmbCompUploadFirstPage_WizardBack);
      this.WizardFinish += new CancelEventHandler(this.EmbCompUploadFirstPage_WizardFinish);
      this.Controls.SetChildIndex((Control) this.lbl_EmbCompFSBrowse, 0);
      this.Controls.SetChildIndex((Control) this.txtAsmType, 0);
      this.Controls.SetChildIndex((Control) this.label2, 0);
      this.Controls.SetChildIndex((Control) this.label1, 0);
      this.Controls.SetChildIndex((Control) this.label3, 0);
      this.Controls.SetChildIndex((Control) this.txtAsmName, 0);
      this.Controls.SetChildIndex((Control) this.btn_CPBrowse, 0);
      this.Controls.SetChildIndex((Control) this.lblAsmType, 0);
      this.Controls.SetChildIndex((Control) this.txtCPPath, 0);
      this.Controls.SetChildIndex((Control) this.lblAsmName, 0);
      this.Controls.SetChildIndex((Control) this.Banner, 0);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public EmbCompUploadFirstPage(EmbCompUploadWizard p1)
      : base(HELP_IDS.BDS_BOEXTENSION_CREATE)
    {
      this.parentWizard = p1;
      this.InitializeComponent();
    }

    private void EmbCompUploadFirstPage_SetActive(object sender, CancelEventArgs e)
    {
      this.SetWizardButtons(WizardPageType.FirstAndLast);
      this.EnableFinishButton(false);
    }

    private void EmbCompUploadFirstPage_WizardFinish(object sender, CancelEventArgs e)
    {
      new EmbeddedComponentHandler(this.txtAsmName.Text, this.txtAsmType.Text, this.txtCPPath.Text).save();
    }

    private void EmbCompUploadFirstPage_WizardBack(object sender, CopernicusWizardPageEventArgs e)
    {
    }

    private void Banner_Load(object sender, EventArgs e)
    {
    }

    private void btn_CPBrowse_Click(object sender, EventArgs e)
    {
      OpenFileDialog openFileDialog = new OpenFileDialog();
      openFileDialog.Title = "Choose Custom Pane";
      openFileDialog.Filter = "Custom Pane (*.xap)|*.xap";
      int num = (int) openFileDialog.ShowDialog();
      this.txtCPPath.Text = openFileDialog.FileName.ToString();
      if (!(this.txtCPPath.Text != string.Empty))
        return;
      this.EnableFinishButton(true);
    }
  }
}
