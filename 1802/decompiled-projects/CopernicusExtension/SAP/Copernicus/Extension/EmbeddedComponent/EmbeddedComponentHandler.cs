﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Extension.EmbeddedComponent.EmbeddedComponentHandler
// Assembly: CopernicusExtension, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 502FBA91-B4A5-42CF-AE72-55C79D2DF132
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusExtension.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace SAP.Copernicus.Extension.EmbeddedComponent
{
  internal class EmbeddedComponentHandler
  {
    private string ecXREPpath;
    private string cpXREPpath;
    private string cpXMLcontent;
    private byte[] xapBytes;
    private string asmName;
    private string asmType;
    private string cpName;

    public EmbeddedComponentHandler(string asmName, string asmType, string localXAPpath)
    {
      Cursor.Current = Cursors.WaitCursor;
      this.ecXREPpath = XRepMapper.GetInstance().GetProjectFolderXRepPath() + "/" + CopernicusProjectSystemUtil.getSelectedNodeHirerachyName();
      this.cpXREPpath = this.ecXREPpath.Replace("EC.uicomponent", "xap");
      this.cpXREPpath = this.cpXREPpath.Replace("/SRC", "/GEN");
      this.asmName = asmName;
      this.asmType = asmType;
      this.cpName = CopernicusProjectSystemUtil.getSelectedNodeName().Replace(".EC.uicomponent", "");
      this.cpXMLcontent = this.genXMLcontent();
      this.xapBytes = this.getXAPcontent(localXAPpath);
      Cursor.Current = Cursors.Default;
    }

    public byte[] getXAPcontent(string localXAPpath)
    {
      FileStream fileStream = new FileStream(localXAPpath, FileMode.Open, FileAccess.Read);
      byte[] buffer = new byte[fileStream.Length];
      fileStream.Read(buffer, 0, (int) fileStream.Length);
      fileStream.Close();
      return buffer;
    }

    public string getCustomPaneXREPPath()
    {
      return this.cpXREPpath;
    }

    public static XmlNamespaceManager PrepareNSManagerForCustomPane(XmlDocument myXmlDocument)
    {
      XmlNamespaceManager namespaceManager = new XmlNamespaceManager(myXmlDocument.NameTable);
      namespaceManager.AddNamespace("uxv", "http://www.sap.com/a1s/cd/oberon/uxview-1.0");
      namespaceManager.AddNamespace("uxc", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      namespaceManager.AddNamespace("base", "http://www.sap.com/a1s/cd/oberon/base-1.0");
      return namespaceManager;
    }

    public string genXMLcontent()
    {
      XmlDocument xmlDocument = new XmlDocument();
      XmlDeclaration xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", (string) null, (string) null);
      xmlDeclaration.Encoding = "utf-8";
      XmlElement documentElement = xmlDocument.DocumentElement;
      xmlDocument.InsertBefore((XmlNode) xmlDeclaration, (XmlNode) documentElement);
      XmlNamespaceManager namespaceManager = new XmlNamespaceManager(xmlDocument.NameTable);
      namespaceManager.AddNamespace("uxv", "http://www.sap.com/a1s/cd/oberon/uxview-1.0");
      namespaceManager.AddNamespace("uxc", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      namespaceManager.AddNamespace("base", "http://www.sap.com/a1s/cd/oberon/base-1.0");
      XmlNode element = (XmlNode) xmlDocument.CreateElement("UXComponent");
      XmlAttribute attribute1 = xmlDocument.CreateAttribute("id");
      attribute1.Value = "dkOdULrBvqQuFOlzseH1dm";
      element.Attributes.Append(attribute1);
      XmlAttribute attribute2 = xmlDocument.CreateAttribute("name");
      attribute2.Value = this.cpName;
      element.Attributes.Append(attribute2);
      XmlAttribute attribute3 = xmlDocument.CreateAttribute("componentType");
      attribute3.Value = "embedded";
      element.Attributes.Append(attribute3);
      XmlAttribute attribute4 = xmlDocument.CreateAttribute("parentDependency");
      attribute4.Value = "yes";
      element.Attributes.Append(attribute4);
      XmlAttribute attribute5 = xmlDocument.CreateAttribute("AuthorizationClassificationCode");
      attribute5.Value = "ReuseComponent";
      element.Attributes.Append(attribute5);
      XmlAttribute attribute6 = xmlDocument.CreateAttribute("useUIController");
      attribute6.Value = "true";
      element.Attributes.Append(attribute6);
      XmlAttribute attribute7 = xmlDocument.CreateAttribute("helpId");
      attribute7.Value = "b2b61556-8fbf-4d48-8ff8-310c6118612e";
      element.Attributes.Append(attribute7);
      XmlAttribute attribute8 = xmlDocument.CreateAttribute("helpId");
      attribute8.Value = "b2b61556-8fbf-4d48-8ff8-310c6118612e";
      element.Attributes.Append(attribute8);
      XmlAttribute attribute9 = xmlDocument.CreateAttribute("designtimeVersion");
      attribute9.Value = "25.0.555.1045";
      element.Attributes.Append(attribute9);
      XmlAttribute attribute10 = xmlDocument.CreateAttribute("modelVersion");
      attribute10.Value = "1.0";
      element.Attributes.Append(attribute10);
      XmlAttribute attribute11 = xmlDocument.CreateAttribute("xmlns");
      attribute11.Value = "http://www.sap.com/a1s/cd/oberon/uxcomponent-1.0";
      element.Attributes.Append(attribute11);
      XmlAttribute attribute12 = xmlDocument.CreateAttribute("xmlns:uxv");
      attribute12.Value = "http://www.sap.com/a1s/cd/oberon/uxview-1.0";
      element.Attributes.Append(attribute12);
      XmlAttribute attribute13 = xmlDocument.CreateAttribute("xmlns:base");
      attribute13.Value = "http://www.sap.com/a1s/cd/oberon/base-1.0";
      element.Attributes.Append(attribute13);
      XmlAttribute attribute14 = xmlDocument.CreateAttribute("xmlns:uxc");
      attribute14.Value = "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0";
      element.Attributes.Append(attribute14);
      XmlNode node1 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:Interface", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute15 = xmlDocument.CreateAttribute("id");
      attribute15.Value = "0sL3N_RbxagHVuDDtId8Ym";
      node1.Attributes.Append(attribute15);
      element.AppendChild(node1);
      XmlNode node2 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:RequiredAssemblies", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute16 = xmlDocument.CreateAttribute("id");
      attribute16.Value = "asf6sa7df9sg5ad6sf";
      node2.Attributes.Append(attribute16);
      node1.AppendChild(node2);
      XmlNode node3 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:Xap", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute17 = xmlDocument.CreateAttribute("id");
      attribute17.Value = Guid.NewGuid().ToString("N").Substring(0, 22);
      node3.Attributes.Append(attribute17);
      XmlAttribute attribute18 = xmlDocument.CreateAttribute("value");
      attribute18.Value = this.cpXREPpath;
      node3.Attributes.Append(attribute18);
      node2.AppendChild(node3);
      XmlNode node4 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:Configuration", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute19 = xmlDocument.CreateAttribute("id");
      attribute19.Value = "OlFWvLDVIa_lOeZg5IFB0W";
      node4.Attributes.Append(attribute19);
      node1.AppendChild(node4);
      XmlNode node5 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:Implementation", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute20 = xmlDocument.CreateAttribute("id");
      attribute20.Value = "mqwI_SC4raEqc_SHF9zgfm";
      node5.Attributes.Append(attribute20);
      element.AppendChild(node5);
      XmlNode node6 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:DataModel", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute21 = xmlDocument.CreateAttribute("id");
      attribute21.Value = "hm$aC9_3h4MFL_jA0KlAgm";
      node6.Attributes.Append(attribute21);
      XmlAttribute attribute22 = xmlDocument.CreateAttribute("name");
      attribute22.Value = "Root";
      node6.Attributes.Append(attribute22);
      node5.AppendChild(node6);
      XmlNode node7 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:DataField", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute23 = xmlDocument.CreateAttribute("id");
      attribute23.Value = "oNRkfdfIh4sMBDq4XuSaYW";
      node7.Attributes.Append(attribute23);
      XmlAttribute attribute24 = xmlDocument.CreateAttribute("name");
      attribute24.Value = "@ErrorOccured";
      node7.Attributes.Append(attribute24);
      XmlAttribute attribute25 = xmlDocument.CreateAttribute("type");
      attribute25.Value = "string";
      node7.Attributes.Append(attribute25);
      node6.AppendChild(node7);
      XmlNode node8 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:DataField", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute26 = xmlDocument.CreateAttribute("id");
      attribute26.Value = "nce5O58AzaAiTDTV6gdeY0";
      node8.Attributes.Append(attribute26);
      XmlAttribute attribute27 = xmlDocument.CreateAttribute("name");
      attribute27.Value = "@TransitionErrorOccurred";
      node8.Attributes.Append(attribute27);
      XmlAttribute attribute28 = xmlDocument.CreateAttribute("type");
      attribute28.Value = "string";
      node8.Attributes.Append(attribute28);
      node6.AppendChild(node8);
      XmlNode node9 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:Structure", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute29 = xmlDocument.CreateAttribute("id");
      attribute29.Value = "Z1ylpdNU$KIq8AHkBTKpd0";
      node9.Attributes.Append(attribute29);
      XmlAttribute attribute30 = xmlDocument.CreateAttribute("name");
      attribute30.Value = "$System";
      node9.Attributes.Append(attribute30);
      node6.AppendChild(node9);
      XmlNode node10 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:DataField", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute31 = xmlDocument.CreateAttribute("id");
      attribute31.Value = "VggnVR8JH4ASL7HMjra5yW";
      node10.Attributes.Append(attribute31);
      XmlAttribute attribute32 = xmlDocument.CreateAttribute("name");
      attribute32.Value = "SessionUrl";
      node10.Attributes.Append(attribute32);
      XmlAttribute attribute33 = xmlDocument.CreateAttribute("type");
      attribute33.Value = "string";
      node10.Attributes.Append(attribute33);
      node9.AppendChild(node10);
      XmlNode node11 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:DataField", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute34 = xmlDocument.CreateAttribute("id");
      attribute34.Value = "tStkfH8zva2m547PIoIB3G";
      node11.Attributes.Append(attribute34);
      XmlAttribute attribute35 = xmlDocument.CreateAttribute("name");
      attribute35.Value = "Saved";
      node11.Attributes.Append(attribute35);
      XmlAttribute attribute36 = xmlDocument.CreateAttribute("type");
      attribute36.Value = "boolean";
      node11.Attributes.Append(attribute36);
      node9.AppendChild(node11);
      XmlNode node12 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:DataField", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute37 = xmlDocument.CreateAttribute("id");
      attribute37.Value = "e62u6JU77aYmunXlJRqzpW";
      node12.Attributes.Append(attribute37);
      XmlAttribute attribute38 = xmlDocument.CreateAttribute("name");
      attribute38.Value = "IsPersistent";
      node12.Attributes.Append(attribute38);
      XmlAttribute attribute39 = xmlDocument.CreateAttribute("type");
      attribute39.Value = "boolean";
      node12.Attributes.Append(attribute39);
      node9.AppendChild(node12);
      XmlNode node13 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:Structure", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute40 = xmlDocument.CreateAttribute("id");
      attribute40.Value = "RJRSZYJpkq_2M1nNTUVWA0";
      node13.Attributes.Append(attribute40);
      XmlAttribute attribute41 = xmlDocument.CreateAttribute("name");
      attribute41.Value = "UIState";
      node13.Attributes.Append(attribute41);
      node6.AppendChild(node13);
      XmlNode node14 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:ModelChanges", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      node5.AppendChild(node14);
      XmlNode node15 = xmlDocument.CreateNode(XmlNodeType.Element, "uxc:Navigation", "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0");
      XmlAttribute attribute42 = xmlDocument.CreateAttribute("id");
      attribute42.Value = "Rv6HZb3G94oPaJGZIVxIvG";
      node15.Attributes.Append(attribute42);
      element.AppendChild(node15);
      XmlNode node16 = xmlDocument.CreateNode(XmlNodeType.Element, "UXView", "");
      XmlAttribute attribute43 = xmlDocument.CreateAttribute("id");
      attribute43.Value = "cpUhEWC5uKE_2Jyev2UrSG";
      node16.Attributes.Append(attribute43);
      XmlAttribute attribute44 = xmlDocument.CreateAttribute("patternType");
      attribute44.Value = "embedded";
      node16.Attributes.Append(attribute44);
      element.AppendChild(node16);
      XmlNode node17 = xmlDocument.CreateNode(XmlNodeType.Element, "uxv:IdentificationRegion", "http://www.sap.com/a1s/cd/oberon/uxview-1.0");
      XmlAttribute attribute45 = xmlDocument.CreateAttribute("id");
      attribute45.Value = "zNHsSWZMIKskKrNv27Rngm";
      node17.Attributes.Append(attribute45);
      node16.AppendChild(node17);
      XmlNode node18 = xmlDocument.CreateNode(XmlNodeType.Element, "uxv:ViewContainerRegion", "http://www.sap.com/a1s/cd/oberon/uxview-1.0");
      XmlAttribute attribute46 = xmlDocument.CreateAttribute("id");
      attribute46.Value = "IfWLQ2zjHKw9kmZEYbAQN0";
      node18.Attributes.Append(attribute46);
      node16.AppendChild(node18);
      XmlNode node19 = xmlDocument.CreateNode(XmlNodeType.Element, "uxv:View", "http://www.sap.com/a1s/cd/oberon/uxview-1.0");
      XmlAttribute attribute47 = xmlDocument.CreateAttribute("id");
      attribute47.Value = "eM3uZdOoO4IOWrElS$MUZm";
      node19.Attributes.Append(attribute47);
      XmlAttribute attribute48 = xmlDocument.CreateAttribute("columns");
      attribute48.Value = "1";
      node19.Attributes.Append(attribute48);
      XmlAttribute attribute49 = xmlDocument.CreateAttribute("rows");
      attribute49.Value = "1";
      node19.Attributes.Append(attribute49);
      node18.AppendChild(node19);
      XmlNode node20 = xmlDocument.CreateNode(XmlNodeType.Element, "uxv:PaneContainer", "http://www.sap.com/a1s/cd/oberon/uxview-1.0");
      XmlAttribute attribute50 = xmlDocument.CreateAttribute("id");
      attribute50.Value = "2QUfX5cA44UT8CUSPos7Nm";
      node20.Attributes.Append(attribute50);
      XmlAttribute attribute51 = xmlDocument.CreateAttribute("column");
      attribute51.Value = "0";
      node20.Attributes.Append(attribute51);
      XmlAttribute attribute52 = xmlDocument.CreateAttribute("rows");
      attribute52.Value = "0";
      node20.Attributes.Append(attribute52);
      node19.AppendChild(node20);
      XmlNode node21 = xmlDocument.CreateNode(XmlNodeType.Element, "uxv:CustomPane", "http://www.sap.com/a1s/cd/oberon/uxview-1.0");
      XmlAttribute attribute53 = xmlDocument.CreateAttribute("id");
      attribute53.Value = "mZ5ZpifmWqwXRNxqydVuIm";
      node21.Attributes.Append(attribute53);
      XmlAttribute attribute54 = xmlDocument.CreateAttribute("assemblyName");
      attribute54.Value = this.asmName;
      node21.Attributes.Append(attribute54);
      XmlAttribute attribute55 = xmlDocument.CreateAttribute("typeName");
      attribute55.Value = this.asmType;
      node21.Attributes.Append(attribute55);
      XmlAttribute attribute56 = xmlDocument.CreateAttribute("xapExtensionMode");
      attribute56.Value = "true";
      node21.Attributes.Append(attribute56);
      node20.AppendChild(node21);
      XmlNode node22 = xmlDocument.CreateNode(XmlNodeType.Element, "uxv:ActionForms", "http://www.sap.com/a1s/cd/oberon/uxview-1.0");
      node16.AppendChild(node22);
      XmlNode node23 = xmlDocument.CreateNode(XmlNodeType.Element, "uxv:ModalDialogs", "http://www.sap.com/a1s/cd/oberon/uxview-1.0");
      XmlAttribute attribute57 = xmlDocument.CreateAttribute("id");
      attribute57.Value = "K6S6F$0wO46GVv0BohitNW";
      node23.Attributes.Append(attribute57);
      node16.AppendChild(node23);
      XmlNode node24 = xmlDocument.CreateNode(XmlNodeType.Element, "uxv:SideCarViewPanels", "http://www.sap.com/a1s/cd/oberon/uxview-1.0");
      XmlAttribute attribute58 = xmlDocument.CreateAttribute("id");
      attribute58.Value = "2sATn5UGiqc_7FnEEErtzm";
      node23.Attributes.Append(attribute58);
      node16.AppendChild(node24);
      XmlNode node25 = xmlDocument.CreateNode(XmlNodeType.Element, "TextPool", "");
      element.AppendChild(node25);
      XmlNode node26 = xmlDocument.CreateNode(XmlNodeType.Element, "base:TextBlock", "http://www.sap.com/a1s/cd/oberon/base-1.0");
      XmlAttribute attribute59 = xmlDocument.CreateAttribute("language");
      attribute59.Value = "EN";
      node26.Attributes.Append(attribute59);
      XmlAttribute attribute60 = xmlDocument.CreateAttribute("masterLanguage");
      attribute60.Value = "true";
      node26.Attributes.Append(attribute60);
      XmlAttribute attribute61 = xmlDocument.CreateAttribute("currentLanguage");
      attribute61.Value = "true";
      node26.Attributes.Append(attribute61);
      node25.AppendChild(node26);
      xmlDocument.AppendChild(element);
      StringWriter stringWriter = new StringWriter();
      XmlTextWriter xmlTextWriter = new XmlTextWriter((TextWriter) stringWriter);
      xmlDocument.WriteTo((XmlWriter) xmlTextWriter);
      return stringWriter.ToString();
    }

    public string checkIfCPexists()
    {
      XRepHandler xrepHandler = new XRepHandler();
      string content = string.Empty;
      IDictionary<string, string> attribs = (IDictionary<string, string>) null;
      xrepHandler.Read(this.ecXREPpath, out content, out attribs);
      XmlDocument myXmlDocument = new XmlDocument();
      XmlNamespaceManager nsmgr = EmbeddedComponentHandler.PrepareNSManagerForCustomPane(myXmlDocument);
      myXmlDocument.LoadXml(content);
      XmlNode xmlNode = myXmlDocument.SelectSingleNode("//uxc:Xap", nsmgr);
      if (xmlNode != null)
        return xmlNode.Attributes["value"].Value;
      return string.Empty;
    }

    private void saveEC()
    {
      XRepHandler xrepHandler = new XRepHandler();
      try
      {
        xrepHandler.CreateOrUpdate(this.ecXREPpath, this.cpXMLcontent, true, (IDictionary<string, string>) null, false, (string) null);
      }
      catch (Exception ex)
      {
        int num = (int) CopernicusMessageBox.Show("Error updating Embedded Component in XREP: " + ex.ToString());
      }
    }

    private void saveCP()
    {
      XRepHandler xrepHandler = new XRepHandler();
      try
      {
        xrepHandler.CreateOrUpdate(this.cpXREPpath, this.xapBytes, true, (IDictionary<string, string>) null, false);
      }
      catch (Exception ex)
      {
        int num = (int) CopernicusMessageBox.Show("Error updating Custom Pane (XAP File) in XREP: " + ex.ToString());
      }
    }

    public void save()
    {
      if (this.checkIfCPexists() != string.Empty)
      {
        if (CopernicusMessageBox.Show("File exists. Do you want to overwrite?", "File exists.", MessageBoxButtons.YesNo) == DialogResult.No)
          return;
        Cursor.Current = Cursors.WaitCursor;
        this.saveCP();
        Cursor.Current = Cursors.Default;
      }
      else
      {
        Cursor.Current = Cursors.WaitCursor;
        this.saveEC();
        this.saveCP();
        Cursor.Current = Cursors.Default;
      }
    }
  }
}
