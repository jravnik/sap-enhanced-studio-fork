﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.EntityIcons
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus
{
  [CompilerGenerated]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  public class EntityIcons
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal EntityIcons()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) EntityIcons.resourceMan, (object) null))
          EntityIcons.resourceMan = new ResourceManager("SAP.Copernicus.EntityIcons", typeof (EntityIcons).Assembly);
        return EntityIcons.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return EntityIcons.resourceCulture;
      }
      set
      {
        EntityIcons.resourceCulture = value;
      }
    }

    public static Bitmap ABSLScriptFile
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ABSLScriptFile), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Action
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Action), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Association
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Association), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap B2BWebServiceIntegration
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (B2BWebServiceIntegration), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BAdIFilter
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BAdIFilter), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BadiFilterInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BadiFilterInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BAdIImplementation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BAdIImplementation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BTM
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BTM), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BTMInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BTMInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessArea
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessArea), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessConfiguration
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessConfiguration), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessConfigurationObject
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessConfigurationObject), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessConfigurationView
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessConfigurationView), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessConfigurationViewInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessConfigurationViewInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessObject
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessObject), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessObjectExtension
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessObjectExtension), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessObjectInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessObjectInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessObjectNode
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessObjectNode), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessOption
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessOption), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessOptionGroup
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessOptionGroup), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessPackage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessPackage), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap BusinessTopic
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (BusinessTopic), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CodeValue
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CodeValue), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CombinedDS
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CombinedDS), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CompositionAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CompositionAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CompositionAssociationWithToNCardinality
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CompositionAssociationWithToNCardinality), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CreateApprovalTask
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CreateApprovalTask), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CreateFloorplan
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CreateFloorplan), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CrossBOAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CrossBOAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CrossBOAssociationWithToNCardinality
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CrossBOAssociationWithToNCardinality), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerObjectReferences
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CustomerObjectReferences), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerObjectReferencesInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CustomerObjectReferencesInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerspecificPatchSolutionIcon
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CustomerspecificPatchSolutionIcon), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerspecificSolutionMultiuserIcon
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CustomerspecificSolutionMultiuserIcon), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerspecificSolutionsIcon
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CustomerspecificSolutionsIcon), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap CustomerspecificSolutionTemplateIcon
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (CustomerspecificSolutionTemplateIcon), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DataDisclosure
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DataDisclosure), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DataDisclosureInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DataDisclosureInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DataSource
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DataSource), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DataSourceInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DataSourceInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DataType
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DataType), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeleteSolution
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeleteSolution), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DependentObject
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DependentObject), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DependentObjectAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DependentObjectAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeploymentUnit
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeploymentUnit), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedAction
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedAction), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedBusinessObject
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedBusinessObject), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedCompositionAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedCompositionAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedCompositionAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedCompositionAssociationN), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedCrossBOAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedCrossBOAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedCrossBOAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedCrossBOAssociationN), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedDataType
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedDataType), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedDependendObjectAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedDependendObjectAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedElement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedElement), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedInternalAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedInternalAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedInternalAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedInternalAssociationN), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedLibrary
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedLibrary), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedLibraryFunction
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedLibraryFunction), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedNode
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedNode), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap DeprecatedQuery
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (DeprecatedQuery), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Element
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Element), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ElementCollection
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ElementCollection), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EmbeddedComponent
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (EmbeddedComponent), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EmbeddedComponentInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (EmbeddedComponentInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventHandlerAction
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (EventHandlerAction), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventHandlerActionInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (EventHandlerActionInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventHandlerEvents
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (EventHandlerEvents), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventhandlerEventsInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (EventhandlerEventsInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventHandlerValidation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (EventHandlerValidation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap EventHandlerValidationInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (EventHandlerValidationInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ExtensionElement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ExtensionElement), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ExtensionElementCollection
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ExtensionElementCollection), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ExternalWebServiceExtension
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ExternalWebServiceExtension), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ExternalWebServiceExtensionInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ExternalWebServiceExtensionInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FactSheet
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (FactSheet), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FactSheetInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (FactSheetInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Floorplan
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Floorplan), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Folder
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Folder), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormMessageTypeExtension
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (FormMessageTypeExtension), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormMsgTypeExtInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (FormMsgTypeExtInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormTemplate
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (FormTemplate), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormTemplateGroup
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (FormTemplateGroup), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormTemplateGroupInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (FormTemplateGroupInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormTemplateVariant
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (FormTemplateVariant), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap FormTemplateVariantInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (FormTemplateVariantInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap GAF
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (GAF), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap GAFInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (GAFInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InboundMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (InboundMessage), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InboundMigrationMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (InboundMigrationMessage), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InboundMixedMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (InboundMixedMessage), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InformationMark
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (InformationMark), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap IntegrationScenario
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (IntegrationScenario), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InternalAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (InternalAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap InternalAssociationWithToNCardinality
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (InternalAssociationWithToNCardinality), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap JoinedDS
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (JoinedDS), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap KeyFigure
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (KeyFigure), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Library
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Library), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap LibraryFunction
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (LibraryFunction), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap LibraryInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (LibraryInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap MDRO
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (MDRO), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap MDROInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (MDROInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Message
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Message), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap MessageGroup
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (MessageGroup), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ModalDialog
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ModalDialog), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap NodeExtensionScenario
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (NodeExtensionScenario), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap NodeExtensionScenarioInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (NodeExtensionScenarioInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OIF
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (OIF), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OIFInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (OIFInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OutboundMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (OutboundMessage), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OutboundMigrationMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (OutboundMigrationMessage), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OutboundMixedMessage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (OutboundMixedMessage), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OVS
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (OVS), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap OWL
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (OWL), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap PortTypePackage
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (PortTypePackage), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap PortTypePackageInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (PortTypePackageInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Process
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Process), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Processes
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Processes), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ProcessIntegration
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ProcessIntegration), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ProcessScenarioExtension
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ProcessScenarioExtension), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap PSD
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (PSD), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap PSDInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (PSDInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap QAF
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (QAF), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap QAFInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (QAFInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Query
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Query), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap QueryInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (QueryInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap QuickCreate
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (QuickCreate), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap QuickView
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (QuickView), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyBusinessObject
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyBusinessObject), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyCompositionAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyCompositionAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyCompositionAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyCompositionAssociationN), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyCrossBOAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyCrossBOAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyCrossBOAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyCrossBOAssociationN), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyDependendObjectAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyDependendObjectAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyElement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyElement), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyElementCollection
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyElementCollection), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyInternalAssociation
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyInternalAssociation), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyInternalAssociationN
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyInternalAssociationN), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ReadonlyNode
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ReadonlyNode), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Report
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Report), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ScalableLocalSolutionIcon
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ScalableLocalSolutionIcon), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ServiceIntegration
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ServiceIntegration), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap Solution
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (Solution), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap StatusActionMngmtInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (StatusActionMngmtInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap StatusAndActionManagement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (StatusAndActionManagement), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ThingInspector
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ThingInspector), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap ThingType_new
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (ThingType_new), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap TransientElement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (TransientElement), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap TransientExtensionElement
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (TransientExtensionElement), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap UISwitch
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (UISwitch), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WebService
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (WebService), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WebServiceAuthorization
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (WebServiceAuthorization), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WebServiceInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (WebServiceInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WorkCenter
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (WorkCenter), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WorkCenterInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (WorkCenterInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WorkCenterView
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (WorkCenterView), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap WorkCenterViewInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (WorkCenterViewInactive), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap XODataService
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (XODataService), EntityIcons.resourceCulture);
      }
    }

    public static Bitmap XODataServiceInactive
    {
      get
      {
        return (Bitmap) EntityIcons.ResourceManager.GetObject(nameof (XODataServiceInactive), EntityIcons.resourceCulture);
      }
    }
  }
}
