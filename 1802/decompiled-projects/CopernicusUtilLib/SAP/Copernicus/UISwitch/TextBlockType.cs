﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.TextBlockType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [Serializable]
  public class TextBlockType
  {
    private TextPoolEntryType[] textPoolEntryField;
    private string languageField;
    private bool masterLanguageField;
    private bool currentLanguageField;

    public TextBlockType()
    {
      this.masterLanguageField = false;
      this.currentLanguageField = false;
    }

    [XmlElement("TextPoolEntry")]
    public TextPoolEntryType[] TextPoolEntry
    {
      get
      {
        return this.textPoolEntryField;
      }
      set
      {
        this.textPoolEntryField = value;
      }
    }

    [XmlAttribute]
    public string language
    {
      get
      {
        return this.languageField;
      }
      set
      {
        this.languageField = value;
      }
    }

    [XmlAttribute]
    [DefaultValue(false)]
    public bool masterLanguage
    {
      get
      {
        return this.masterLanguageField;
      }
      set
      {
        this.masterLanguageField = value;
      }
    }

    [DefaultValue(false)]
    [XmlAttribute]
    public bool currentLanguage
    {
      get
      {
        return this.currentLanguageField;
      }
      set
      {
        this.currentLanguageField = value;
      }
    }
  }
}
