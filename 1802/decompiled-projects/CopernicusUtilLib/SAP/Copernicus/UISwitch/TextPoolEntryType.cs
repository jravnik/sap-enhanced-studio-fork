﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.TextPoolEntryType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlType(Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
  [Serializable]
  public class TextPoolEntryType : ModelEntity
  {
    private string textUuidField;
    private string textField;
    private TextCategoryType textCategoryField;

    [XmlAttribute]
    public string textUuid
    {
      get
      {
        return this.textUuidField;
      }
      set
      {
        this.textUuidField = value;
      }
    }

    [XmlAttribute]
    public string text
    {
      get
      {
        return this.textField;
      }
      set
      {
        this.textField = value;
      }
    }

    [XmlAttribute]
    public TextCategoryType textCategory
    {
      get
      {
        return this.textCategoryField;
      }
      set
      {
        this.textCategoryField = value;
      }
    }
  }
}
