﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.UISwitch
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [XmlRoot(IsNullable = false, Namespace = "http://www.sap.com/a1s/cd/oberon/uimodelchange-1.0")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(AnonymousType = true, Namespace = "http://www.sap.com/a1s/cd/oberon/uimodelchange-1.0")]
  [Serializable]
  public class UISwitch
  {
    private TextBlockType[] textPoolField;
    private string idField;
    private string nameTextPoolIdField;
    private string descriptionTextPoolIdField;

    [XmlArray(Form = XmlSchemaForm.Unqualified)]
    [XmlArrayItem("TextBlock", IsNullable = false, Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
    public TextBlockType[] TextPool
    {
      get
      {
        return this.textPoolField;
      }
      set
      {
        this.textPoolField = value;
      }
    }

    [XmlAttribute]
    public string id
    {
      get
      {
        return this.idField;
      }
      set
      {
        this.idField = value;
      }
    }

    [XmlAttribute]
    public string nameTextPoolId
    {
      get
      {
        return this.nameTextPoolIdField;
      }
      set
      {
        this.nameTextPoolIdField = value;
      }
    }

    [XmlAttribute]
    public string descriptionTextPoolId
    {
      get
      {
        return this.descriptionTextPoolIdField;
      }
      set
      {
        this.descriptionTextPoolIdField = value;
      }
    }
  }
}
