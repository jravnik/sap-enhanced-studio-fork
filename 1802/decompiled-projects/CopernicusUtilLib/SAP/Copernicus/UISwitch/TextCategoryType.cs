﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.TextCategoryType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [XmlType(Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [Serializable]
  public enum TextCategoryType
  {
    XTXT,
    XBLI,
    XBUT,
    XCKL,
    XCOL,
    XFLD,
    XGRP,
    XLNK,
    XLOG,
    XLST,
    XMEN,
    XMIT,
    XMSG,
    XRBL,
    XTBS,
    XTIT,
    XTOL,
    XTND,
    XGAF,
    YBLI,
    YDEF,
    YDES,
    YINS,
    YTXT,
    YLOG,
    YTIC,
    YMSG,
    YINF,
    YTEC,
    YEXP,
  }
}
