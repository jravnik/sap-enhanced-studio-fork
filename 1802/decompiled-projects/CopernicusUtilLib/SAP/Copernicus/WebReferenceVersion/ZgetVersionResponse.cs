﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.WebReferenceVersion.ZgetVersionResponse
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.WebReferenceVersion
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Xml", "4.0.30319.17929")]
  [DesignerCategory("code")]
  [XmlType(AnonymousType = true, Namespace = "urn:sap-com:document:sap:soap:functions:mc-style")]
  [Serializable]
  public class ZgetVersionResponse
  {
    private string evObjectIdField;
    private string evPatchLevelField;
    private string evSpLevelField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string EvObjectId
    {
      get
      {
        return this.evObjectIdField;
      }
      set
      {
        this.evObjectIdField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string EvPatchLevel
    {
      get
      {
        return this.evPatchLevelField;
      }
      set
      {
        this.evPatchLevelField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string EvSpLevel
    {
      get
      {
        return this.evSpLevelField;
      }
      set
      {
        this.evSpLevelField = value;
      }
    }
  }
}
