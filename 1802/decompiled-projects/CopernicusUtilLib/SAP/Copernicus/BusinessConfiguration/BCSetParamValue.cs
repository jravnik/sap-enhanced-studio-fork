﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.BCSetParamValue
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.BusinessConfiguration.Model;
using System.Collections.Generic;

namespace SAP.Copernicus.BusinessConfiguration
{
  public class BCSetParamValue : ValueStructureType
  {
    public string ValueID;
    public string ValueDescription;
    public string ParameterID;
    public bool usedOnlyOnce;
    public List<string> BCSetID;

    public BCSetParamValue(string ValueID, string ParameterID, string BCSetID)
    {
      this.BCSetID = new List<string>();
      this.BCSetID.Add(BCSetID);
      this.ValueID = ValueID;
      this.ParameterID = ParameterID;
      this.usedOnlyOnce = true;
    }
  }
}
