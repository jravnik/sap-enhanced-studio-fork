﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.TestUtil.ParserTestData
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.Package;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace SAP.Copernicus.Core.TestUtil
{
  [DataContract]
  public class ParserTestData
  {
    public const string EXTENSION = ".json";
    public const string BODL_ROOT = "..\\CopernicusTest\\Input\\BOParser\\TestData\\";
    private string fileName;

    public ParserTestData(ParseReason reason, string fileName, int line, int column)
    {
      this.ParseReason = reason;
      this.FileName = fileName;
      this.Line = line;
      this.Column = column;
      this.Result = new ParserTestData.Declaration[0];
    }

    [DataMember]
    public string FileName
    {
      get
      {
        return this.fileName;
      }
      private set
      {
        this.fileName = Path.GetFileName(value);
      }
    }

    [DataMember]
    public int Line { get; private set; }

    [DataMember]
    public int Column { get; private set; }

    [DataMember]
    public ParseReason ParseReason { get; private set; }

    [DataMember]
    public ParserTestData.Declaration[] Result { get; private set; }

    public override string ToString()
    {
      return string.Format("{0} for {1}:{2},{3} {4} declarations", (object) Enum.GetName(typeof (ParseReason), (object) this.ParseReason), (object) this.FileName, (object) this.Line, (object) this.Column, (object) this.Result.Length);
    }

    protected void Debug()
    {
      foreach (ParserTestData.Declaration declaration in this.Result)
        ;
    }

    public void SetResult(Declarations declarations)
    {
      this.Result = this.CreateResult(declarations);
    }

    protected ParserTestData.Declaration[] CreateResult(Declarations declarations)
    {
      if (declarations == null)
        return new ParserTestData.Declaration[0];
      List<ParserTestData.Declaration> declarationList = new List<ParserTestData.Declaration>();
      for (int index = 0; index < declarations.GetCount(); ++index)
        declarationList.Add(new ParserTestData.Declaration(declarations.GetGlyph(index), declarations.GetName(index)));
      return declarationList.ToArray();
    }

    public bool CompareResult(Declarations declarations, out string errorMessage)
    {
      ParserTestData.Declaration[] result = this.CreateResult(declarations);
      errorMessage = string.Empty;
      if (this.Result == result)
        return true;
      if (this.Result.Length != result.Length)
      {
        errorMessage = string.Format("Number of declarations {0} is differnt from the expected result {1}", (object) result.Length, (object) this.Result.Length);
        return false;
      }
      for (int index = 0; index < this.Result.Length; ++index)
      {
        if (!this.Result[index].Equals((object) result[index]))
        {
          errorMessage = string.Format("Declaration at position {0} '{1}' is differnt from the expected result '{2}'", (object) index, (object) result[index], (object) this.Result[index]);
          return false;
        }
      }
      return true;
    }

    public static void Save(string name, ParseRequest req, AuthoringScope scope, string directory = "..\\CopernicusTest\\Input\\BOParser\\TestData\\")
    {
      Declarations declarations = scope.GetDeclarations(req.View, req.Line, req.Col, req.TokenInfo, req.Reason);
      ParserTestData parserTestData = new ParserTestData(req.Reason, req.FileName, req.Line, req.Col);
      parserTestData.SetResult(declarations);
      string path2 = name + ".json";
      string path = Path.Combine(directory, path2);
      using (Stream stream = (Stream) File.Create(path))
        new DataContractJsonSerializer(typeof (ParserTestData)).WriteObject(stream, (object) parserTestData);
      parserTestData.Debug();
    }

    public static ParserTestData Load(string filePath)
    {
      using (Stream stream = (Stream) File.OpenRead(filePath))
        return new DataContractJsonSerializer(typeof (ParserTestData)).ReadObject(stream) as ParserTestData;
    }

    [DataContract]
    public class Declaration
    {
      [DataMember]
      private int Glyph;
      [DataMember]
      private string Name;

      public Declaration(int glyph, string name)
      {
        this.Glyph = glyph;
        this.Name = name;
      }

      public override string ToString()
      {
        return string.Format("{0}:{1}", (object) Enum.GetName(typeof (EntityIconImageIndex), (object) this.Glyph), (object) this.Name);
      }

      public override bool Equals(object obj)
      {
        ParserTestData.Declaration declaration = obj as ParserTestData.Declaration;
        return declaration != null && (declaration == this || declaration.Name == this.Name && declaration.Glyph == this.Glyph);
      }

      public override int GetHashCode()
      {
        if (this.Name != null)
          return this.Glyph ^ this.Name.GetHashCode();
        return this.Glyph;
      }
    }
  }
}
