﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.Util
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using EnvDTE;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Util
{
  public class Util
  {
    private Util()
    {
    }

    public static bool hasIDValidSymbols(string id)
    {
      return Regex.IsMatch(id, "^[a-zA-Z0-9_]*$");
    }

    public static DateTime startMeasurement(string text)
    {
      Trace.WriteLine(text);
      return DateTime.Now;
    }

    public static void endMeasurement(DateTime start, string text)
    {
      TimeSpan timeSpan = DateTime.Now.Subtract(start);
      if (text != null)
        Trace.WriteLine(text + (text.EndsWith(" ") ? "" : " ") + timeSpan.ToString());
      else
        Trace.WriteLine(timeSpan.ToString());
    }

    public static string clientIntToString(int client)
    {
      string str = "";
      if (client < 10)
        str = "00";
      else if (client < 100)
        str = "0";
      return str + (object) client;
    }

    public static string ProjectsLocation
    {
      get
      {
        string path = (string) null;
        try
        {
          Property property = DTEUtil.GetDTE().get_Properties("Environment", "ProjectsAndSolution").Item((object) nameof (ProjectsLocation));
          if (property != null)
            path = property.Value as string;
        }
        catch (Exception ex)
        {
        }
        if (string.IsNullOrEmpty(path))
          path = ".";
        try
        {
          return Path.GetFullPath(path);
        }
        catch (Exception ex)
        {
          int num = (int) CopernicusMessageBox.Show(string.Format(Resource.InvalidProjectsLocation_0_reason_1, (object) path, (object) ex.Message), Resource.InvalidToolsOptions, MessageBoxButtons.OK);
          return (string) null;
        }
      }
    }

    public static Array ConcatenateArrays(params Array[] arrays)
    {
      if (arrays == null)
        throw new ArgumentNullException(nameof (arrays));
      if (arrays.Length == 0)
        throw new ArgumentException("No arrays specified");
      Type elementType = arrays[0].GetType().GetElementType();
      int length = arrays[0].Length;
      for (int index = 1; index < arrays.Length; ++index)
      {
        if (arrays[index].GetType().GetElementType() != elementType)
          throw new ArgumentException("Arrays must all be of the same type");
        length += arrays[index].Length;
      }
      Array instance = Array.CreateInstance(elementType, length);
      int destinationIndex = 0;
      foreach (Array array in arrays)
      {
        Array.Copy(array, 0, instance, destinationIndex, array.Length);
        destinationIndex += array.Length;
      }
      return instance;
    }

    public static string FormatResxString(string resxstring)
    {
      return resxstring.Replace("\\n", "\n").Replace("\\r", "\r").Replace("\\t", "\t");
    }

    public static string ValidateEmail(string email)
    {
      if (string.IsNullOrWhiteSpace(email))
        return Resource.EmailEmpty;
      if (email.Length > 200)
        return Resource.ValueExceedLimit;
      if (new Regex("^([\\w\\.\\-]+)@([\\w\\-]+)((\\.(\\w){2,3})+)$").Match(email).Success)
        return string.Empty;
      return Resource.EmailValidationFailed;
    }
  }
}
