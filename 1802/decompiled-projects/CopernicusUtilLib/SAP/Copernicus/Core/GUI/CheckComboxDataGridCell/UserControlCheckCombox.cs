﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CheckComboxDataGridCell.UserControlCheckCombox
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.CheckComboxDataGridCell
{
  public class UserControlCheckCombox : UserControl
  {
    private static string strTooltipTempateRequired = "Required Filter Value from Backend";
    private static string strTooltipTempateOptional = "Optional Filter Value for Backend";
    private static string strGetContent = "GetContent";
    private static string strGetDescription = "GetDescription";
    private static string strGetCodeAndDescription = "GetCodeAndDescription";
    private ToolTip filterToolTip = new ToolTip();
    private string strCallbackFormClass = string.Empty;
    private string strCallbackFormPackageName = string.Empty;
    public List<CopernicusDataGridViewCheckComboxCell> listMonitorInstances = new List<CopernicusDataGridViewCheckComboxCell>();
    private string strTempCellValue = string.Empty;
    private const string strToolTipForF4 = "Press F4 Key to retrieve Value list from backend";
    public Action<EventArgs> OnFilterValueCallback;
    private string selectedCodeValue;
    private bool isUserEdit;
    private bool isFilterBackendMandatory;
    private Action<bool> invokeAction;
    private bool isDisposedInstance;
    private bool isFilterValueSelectable;
    private int irowIndex;
    private int icolumnIndex;
    private bool isCellChecked;
    private bool isEnterForCurrentControl;
    private bool isF4buttonProcessed;
    private CopernicusPopupClassMetaData filterClassMetaData;
    private bool isUserControlLoaded;
    private IContainer components;
    private SplitContainer splitContainer;
    private CheckBox checkBoxRequired;
    private ComboBox CodeListComboBox;

    public string getSelectedCodeValue
    {
      get
      {
        return this.selectedCodeValue.Trim();
      }
    }

    public UserControlCheckCombox()
    {
      this.InitializeComponent();
      this.filterToolTip.AutoPopDelay = 6000;
      this.filterToolTip.InitialDelay = 500;
      this.filterToolTip.ReshowDelay = 500;
      this.filterToolTip.ShowAlways = true;
      this.filterToolTip.UseAnimation = true;
      this.filterToolTip.IsBalloon = true;
      this.filterToolTip.UseFading = true;
      this.CodeListComboBox.ValueMember = UserControlCheckCombox.strGetContent;
      this.CodeListComboBox.DisplayMember = UserControlCheckCombox.strGetCodeAndDescription;
      this.OnFilterValueCallback += new Action<EventArgs>(this.OnValueChanged);
      this.Load += new EventHandler(this.UserControlCheckCombox_Load);
    }

    public string Value
    {
      get
      {
        return this.CodeListComboBox.Text;
      }
      set
      {
        if (value != null && (value == null || value.Equals(this.CodeListComboBox.Text)))
          return;
        this.CodeListComboBox.Text = value;
      }
    }

    public bool UserEdit
    {
      get
      {
        return this.isUserEdit;
      }
      set
      {
        if (value == this.isUserEdit)
          return;
        this.isUserEdit = value;
      }
    }

    public bool IsFilterBackendMandatory
    {
      get
      {
        return this.isFilterBackendMandatory;
      }
      set
      {
        if (value == this.isFilterBackendMandatory)
          return;
        this.isFilterBackendMandatory = value;
      }
    }

    public bool IsUserChecked
    {
      get
      {
        return this.checkBoxRequired.Checked;
      }
      set
      {
        this.UpdateUIStatus(value);
      }
    }

    private void checkBoxRequired_CheckedChanged(object sender, EventArgs e)
    {
      this.FireTagChangeEvent(this.checkBoxRequired.Checked, this.irowIndex, this.icolumnIndex);
      if (this.isFilterBackendMandatory || !this.checkBoxRequired.Enabled)
        return;
      if (!this.checkBoxRequired.Checked)
      {
        this.CodeListComboBox.Text = (string) null;
        this.CodeListComboBox.Enabled = false;
      }
      else
        this.CodeListComboBox.Enabled = true;
    }

    private void UpdateUIStatus(bool isEnableUIStatus)
    {
      if (this.InvokeRequired)
      {
        if (this.invokeAction == null)
          this.invokeAction = new Action<bool>(this.UpdateEnableUIStatus);
        this.BeginInvoke((Delegate) this.invokeAction, (object) isEnableUIStatus, null, null);
      }
      else
        this.UpdateEnableUIStatus(isEnableUIStatus);
    }

    private void UpdateEnableUIStatus(bool isEnableUICheckedStatus)
    {
      if (this.isFilterBackendMandatory)
      {
        if (!this.checkBoxRequired.Checked)
        {
          this.checkBoxRequired.Checked = true;
          this.CodeListComboBox.Enabled = true;
        }
        if (this.checkBoxRequired.Enabled)
          this.checkBoxRequired.Enabled = false;
        this.FireTagChangeEvent(this.checkBoxRequired.Checked, this.irowIndex, this.icolumnIndex);
      }
      else
      {
        if (!this.checkBoxRequired.Enabled)
          this.checkBoxRequired.Enabled = true;
        if (isEnableUICheckedStatus != this.checkBoxRequired.Checked)
          this.checkBoxRequired.Checked = isEnableUICheckedStatus;
        this.CodeListComboBox.Enabled = this.checkBoxRequired.Checked;
      }
      if (this.isFilterBackendMandatory)
        this.UpdateTooltipStatus(UserControlCheckCombox.strTooltipTempateRequired);
      else
        this.UpdateTooltipStatus(UserControlCheckCombox.strTooltipTempateOptional);
    }

    private void UpdateTooltipStatus(string strTooltipTipTitle)
    {
      this.filterToolTip.ToolTipTitle = strTooltipTipTitle;
      string toolTip = this.filterToolTip.GetToolTip((Control) this.CodeListComboBox);
      if (this.isFilterValueSelectable && this.filterClassMetaData != null)
      {
        if (!toolTip.Equals("Press F4 Key to retrieve Value list from backend"))
          this.filterToolTip.SetToolTip((Control) this.CodeListComboBox, "Press F4 Key to retrieve Value list from backend");
      }
      else if (toolTip != null && toolTip.Length > 0)
        this.filterToolTip.SetToolTip((Control) this.CodeListComboBox, string.Empty);
      this.filterToolTip.SetToolTip((Control) this.splitContainer, string.Empty);
    }

    protected virtual void OnValueChanged(EventArgs e)
    {
    }

    private void textBoxFilterValue_TextChanged(object sender, EventArgs e)
    {
      this.OnFilterValueCallback(e);
    }

    public bool IsDisposedAlready
    {
      get
      {
        return this.isDisposedInstance;
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
      this.isDisposedInstance = disposing;
    }

    public string CallbackModelFormClass
    {
      get
      {
        return this.strCallbackFormClass;
      }
      set
      {
        if (value == null || value.Equals(this.strCallbackFormClass))
          return;
        this.strCallbackFormClass = value;
      }
    }

    public string CallbackModelFormPackageName
    {
      get
      {
        return this.strCallbackFormPackageName;
      }
      set
      {
        if (value == null || value.Equals(this.strCallbackFormPackageName))
          return;
        this.strCallbackFormPackageName = value;
      }
    }

    public bool IsFilterValueSelectable
    {
      get
      {
        return this.isFilterValueSelectable;
      }
      set
      {
        if (value == this.isFilterValueSelectable)
          return;
        this.isFilterValueSelectable = value;
      }
    }

    public event Action<bool, int, int> OnTagIsCellCheckedCallback;

    public int IRowIndex
    {
      get
      {
        return this.irowIndex;
      }
      set
      {
        this.irowIndex = value;
      }
    }

    public int IColumnIndex
    {
      get
      {
        return this.icolumnIndex;
      }
      set
      {
        this.icolumnIndex = value;
      }
    }

    public bool IsCellChecked
    {
      get
      {
        return this.isCellChecked;
      }
      set
      {
        if (value == this.isCellChecked)
          return;
        this.isCellChecked = value;
      }
    }

    public void AddTagChangeMonitor(Action<bool, int, int> callback, CopernicusDataGridViewCheckComboxCell _monitorInstance)
    {
      if (this.listMonitorInstances.Contains(_monitorInstance))
        return;
      this.listMonitorInstances.Add(_monitorInstance);
      this.OnTagIsCellCheckedCallback += callback;
    }

    private void textBoxFilterValue_Enter(object sender, EventArgs e)
    {
      this.isEnterForCurrentControl = true;
    }

    private void textBoxFilterValue_Leave(object sender, EventArgs e)
    {
      this.isEnterForCurrentControl = false;
    }

    public void ReceiveWindowPaneMessageInUserControl(Keys keyCode, int iRowIndex, int iColumnInde)
    {
      if (keyCode != Keys.F4 || iRowIndex != this.irowIndex || (iColumnInde != this.icolumnIndex || !this.isEnterForCurrentControl))
        return;
      int num = this.CodeListComboBox.Enabled ? 1 : 0;
    }

    private void FireTagChangeEvent(bool isFilterValueChecked, int iRowIndex, int iColumnIndex)
    {
      if (this.OnTagIsCellCheckedCallback == null)
        return;
      Action<bool, int, int> action = Interlocked.CompareExchange<Action<bool, int, int>>(ref this.OnTagIsCellCheckedCallback, (Action<bool, int, int>) null, (Action<bool, int, int>) null);
      if (action == null)
        return;
      action(isFilterValueChecked, iRowIndex, iColumnIndex);
    }

    public CopernicusPopupClassMetaData FilterClassMetaData
    {
      get
      {
        return this.filterClassMetaData;
      }
      set
      {
        if (value == null || value.Equals((object) this.filterClassMetaData))
          return;
        this.filterClassMetaData = value;
      }
    }

    private void UserControlCheckCombox_Load(object sender, EventArgs args)
    {
      this.isUserControlLoaded = true;
      if (this.Value.Equals(this.strTempCellValue))
        return;
      this.Value = this.strTempCellValue;
    }

    public void UpdateHintsThenControlValue(CopernicusPopupClassMetaData strFilterClassMetaData, string strCellValue)
    {
      this.SetFilterMetaDataAndUpdateComboxEntry(strFilterClassMetaData);
      if (this.isUserControlLoaded)
        this.Value = strCellValue;
      else
        this.strTempCellValue = strCellValue;
    }

    public void UpdateControlValue(string strCellValue)
    {
      if (this.isUserControlLoaded)
        this.Value = strCellValue;
      else
        this.strTempCellValue = strCellValue;
    }

    private void SetFilterMetaDataAndUpdateComboxEntry(CopernicusPopupClassMetaData strFilterClassMetaData)
    {
      this.filterClassMetaData = strFilterClassMetaData;
      if (this.filterClassMetaData == null || this.CodeListComboBox.DataSource == this.filterClassMetaData.FilterCodeListEntry)
        return;
      this.CodeListComboBox.DataSource = (object) null;
      this.CodeListComboBox.ValueMember = UserControlCheckCombox.strGetContent;
      this.CodeListComboBox.DisplayMember = UserControlCheckCombox.strGetCodeAndDescription;
      this.CodeListComboBox.DataSource = (object) this.filterClassMetaData.FilterCodeListEntry;
    }

    private void IndexChange(object sender, EventArgs e)
    {
      this.RefreshComboxWhenSelectionChange(sender, e);
    }

    private void SelectedValueChange(object sender, EventArgs e)
    {
      this.RefreshComboxWhenSelectionChange(sender, e);
    }

    private void RefreshComboxWhenSelectionChange(object sender, EventArgs e)
    {
      if (this.CodeListComboBox.SelectedItem == null)
        return;
      PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC selectedItem = this.CodeListComboBox.SelectedItem as PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC;
      if (selectedItem == null)
        return;
      this.selectedCodeValue = selectedItem.CONTENT;
      this.Invalidate();
      this.OnFilterValueCallback(e);
    }

    private void TextChange(object sender, EventArgs e)
    {
      this.RefreshTextChangeHints(e);
    }

    private void TextUpdate(object sender, EventArgs e)
    {
      this.RefreshTextChangeHints(e);
    }

    private void RefreshTextChangeHints(EventArgs e)
    {
      this.CodeListComboBox.Text.Trim();
      this.Invalidate();
      this.OnFilterValueCallback(e);
    }

    private string getPotentialCodeDescriptionbyValue(string value)
    {
      if (this.FilterClassMetaData == null)
        return (string) null;
      if (this.filterClassMetaData.FilterCodeListEntry == null)
        return (string) null;
      PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC aeSCodeValueWDesc = ((IEnumerable<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>) this.filterClassMetaData.FilterCodeListEntry).ToList<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>().Find((Predicate<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>) (n => n.CONTENT.Trim().Equals(value)));
      if (aeSCodeValueWDesc != null)
        return aeSCodeValueWDesc.DESCRIPTION;
      return string.Empty;
    }

    protected void ProcessKey(Keys key, out bool isProcessedInside)
    {
      isProcessedInside = false;
      if (key == Keys.Left)
      {
        if (this.CodeListComboBox.Focused && (this.RightToLeft == RightToLeft.No && (this.CodeListComboBox.SelectionLength != 0 || this.CodeListComboBox.SelectionStart != 0) || this.RightToLeft == RightToLeft.Yes && (this.CodeListComboBox.SelectionLength != 0 || this.CodeListComboBox.SelectionStart != this.CodeListComboBox.Text.Length)))
          isProcessedInside = true;
        if (!this.checkBoxRequired.Focused)
          return;
        isProcessedInside = false;
      }
      else if (key == Keys.Right)
      {
        if (this.CodeListComboBox.Focused && (this.RightToLeft == RightToLeft.No && (this.CodeListComboBox.SelectionLength != 0 || this.CodeListComboBox.SelectionStart != this.CodeListComboBox.Text.Length) || this.RightToLeft == RightToLeft.Yes && (this.CodeListComboBox.SelectionLength != 0 || this.CodeListComboBox.SelectionStart != 0)))
          isProcessedInside = true;
        if (!this.checkBoxRequired.Focused)
          return;
        this.CodeListComboBox.Focus();
        isProcessedInside = false;
      }
      else if (key == Keys.End)
      {
        if (!this.CodeListComboBox.Focused || this.CodeListComboBox.SelectionLength == this.CodeListComboBox.Text.Length)
          return;
        isProcessedInside = true;
      }
      else
      {
        if (key != Keys.Delete || this.CodeListComboBox.SelectionLength <= 0 && this.CodeListComboBox.SelectionStart >= this.CodeListComboBox.Text.Length)
          return;
        isProcessedInside = true;
      }
    }

    private void CodeListComboBox_DropDown(object sender, EventArgs e)
    {
      if (this.filterClassMetaData == null || !this.filterClassMetaData.refreshCodeList())
        return;
      this.SetFilterMetaDataAndUpdateComboxEntry(this.filterClassMetaData);
    }

    private void InitializeComponent()
    {
      this.splitContainer = new SplitContainer();
      this.checkBoxRequired = new CheckBox();
      this.CodeListComboBox = new ComboBox();
      this.splitContainer.BeginInit();
      this.splitContainer.Panel1.SuspendLayout();
      this.splitContainer.Panel2.SuspendLayout();
      this.splitContainer.SuspendLayout();
      this.SuspendLayout();
      this.splitContainer.Dock = DockStyle.Fill;
      this.splitContainer.Location = new Point(0, 0);
      this.splitContainer.Name = "splitContainer";
      this.splitContainer.Panel1.Controls.Add((Control) this.checkBoxRequired);
      this.splitContainer.Panel2.Controls.Add((Control) this.CodeListComboBox);
      this.splitContainer.Size = new Size(276, 23);
      this.splitContainer.SplitterDistance = 25;
      this.splitContainer.SplitterWidth = 1;
      this.splitContainer.TabIndex = 0;
      this.checkBoxRequired.AutoSize = true;
      this.checkBoxRequired.Dock = DockStyle.Fill;
      this.checkBoxRequired.Location = new Point(0, 0);
      this.checkBoxRequired.Name = "checkBoxRequired";
      this.checkBoxRequired.Size = new Size(25, 23);
      this.checkBoxRequired.TabIndex = 0;
      this.checkBoxRequired.UseVisualStyleBackColor = true;
      this.checkBoxRequired.CheckedChanged += new EventHandler(this.checkBoxRequired_CheckedChanged);
      this.CodeListComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
      this.CodeListComboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.CodeListComboBox.Dock = DockStyle.Fill;
      this.CodeListComboBox.FormattingEnabled = true;
      this.CodeListComboBox.Location = new Point(0, 0);
      this.CodeListComboBox.Margin = new Padding(2);
      this.CodeListComboBox.Name = "CodeListComboBox";
      this.CodeListComboBox.Size = new Size(250, 21);
      this.CodeListComboBox.TabIndex = 0;
      this.CodeListComboBox.DropDown += new EventHandler(this.CodeListComboBox_DropDown);
      this.CodeListComboBox.SelectedIndexChanged += new EventHandler(this.IndexChange);
      this.CodeListComboBox.TextChanged += new EventHandler(this.TextChange);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.splitContainer);
      this.Name = nameof (UserControlCheckCombox);
      this.Size = new Size(276, 23);
      this.splitContainer.Panel1.ResumeLayout(false);
      this.splitContainer.Panel1.PerformLayout();
      this.splitContainer.Panel2.ResumeLayout(false);
      this.splitContainer.EndInit();
      this.splitContainer.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
