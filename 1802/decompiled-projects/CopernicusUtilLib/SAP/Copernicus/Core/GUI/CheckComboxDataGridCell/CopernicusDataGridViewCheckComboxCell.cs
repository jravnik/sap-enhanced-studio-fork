﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CheckComboxDataGridCell.CopernicusDataGridViewCheckComboxCell
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.CheckComboxDataGridCell
{
  public class CopernicusDataGridViewCheckComboxCell : DataGridViewTextBoxCell
  {
    private static readonly DataGridViewContentAlignment anyRight = DataGridViewContentAlignment.TopRight | DataGridViewContentAlignment.MiddleRight | DataGridViewContentAlignment.BottomRight;
    private static readonly DataGridViewContentAlignment anyCenter = DataGridViewContentAlignment.TopCenter | DataGridViewContentAlignment.MiddleCenter | DataGridViewContentAlignment.BottomCenter;
    private static Type defaultEditType = typeof (CopernicusDataGridViewCheckComboxEditingControl);
    private static Type defaultValueType = typeof (string);
    private const int DATAGRIDVIEWSELECTORCELL_defaultRenderingBitmapWidth = 100;
    private const int DATAGRIDVIEWSELECTORCELL_defaultRenderingBitmapHeight = 22;
    internal const bool DATAGRIDVIEWSELECTORCELL_defaultIsEnable = false;
    internal const string DATAGRIDVIEWSELECTORCELL_defaultCallbackHandler = "";
    internal bool isEnableRequiredValue;
    private string strCallbackFormClass;
    private string strCallbackFormPackageName;
    private CopernicusPopupClassMetaData filterClassMetaDataForCell;
    private bool isFilterValueSelectableForCell;
    [ThreadStatic]
    private static Bitmap renderingBitmap;
    [ThreadStatic]
    private static Bitmap emptyBitmap;
    [ThreadStatic]
    private static UserControlCheckCombox paintingContorlforFilterValue;

    [DllImport("USER32.DLL", CharSet = CharSet.Auto)]
    private static extern short VkKeyScan(char key);

    public CopernicusDataGridViewCheckComboxCell()
    {
      if (CopernicusDataGridViewCheckComboxCell.renderingBitmap == null)
        CopernicusDataGridViewCheckComboxCell.renderingBitmap = new Bitmap(100, 22);
      if (CopernicusDataGridViewCheckComboxCell.emptyBitmap == null)
        CopernicusDataGridViewCheckComboxCell.emptyBitmap = new Bitmap(100, 22);
      if (CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue == null || CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.IsDisposedAlready)
      {
        CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue = new UserControlCheckCombox();
        CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.BorderStyle = BorderStyle.None;
      }
      this.isEnableRequiredValue = false;
    }

    [DefaultValue("")]
    public string strCallbackFormHandler
    {
      get
      {
        return this.strCallbackFormClass;
      }
      set
      {
        if (!(this.strCallbackFormClass != value))
          return;
        this.SetCallbackFormHandler(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    internal void SetCallbackFormHandler(int rowIndex, string value)
    {
      this.strCallbackFormClass = value;
      this.OwnsEditingCopernicusFilterValue(rowIndex);
    }

    [DefaultValue("")]
    public string CallbackFormPackageName
    {
      get
      {
        return this.strCallbackFormPackageName;
      }
      set
      {
        if (!(this.strCallbackFormPackageName != value))
          return;
        this.SetCallbackFormPackageName(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    internal void SetCallbackFormPackageName(int rowIndex, string value)
    {
      this.strCallbackFormPackageName = value;
      this.OwnsEditingCopernicusFilterValue(rowIndex);
    }

    [DefaultValue(false)]
    public bool IsFilterValueSelectableForCell
    {
      get
      {
        return this.isFilterValueSelectableForCell;
      }
      set
      {
        if (this.isFilterValueSelectableForCell == value)
          return;
        this.SetCallbackFormIsFilterValueSelectableForCell(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    internal void SetCallbackFormIsFilterValueSelectableForCell(int rowIndex, bool value)
    {
      this.isFilterValueSelectableForCell = value;
      this.OwnsEditingCopernicusFilterValue(rowIndex);
    }

    public CopernicusPopupClassMetaData FilterClassMetaDataForCell
    {
      get
      {
        return this.filterClassMetaDataForCell;
      }
      set
      {
        if (this.filterClassMetaDataForCell == value)
          return;
        this.SetFilterClassMetaDataForCell(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    internal void SetFilterClassMetaDataForCell(int rowIndex, CopernicusPopupClassMetaData value)
    {
      this.filterClassMetaDataForCell = value;
      this.OwnsEditingCopernicusFilterValue(rowIndex);
    }

    [DefaultValue(false)]
    public bool IsEnableRequiredFilterValue
    {
      get
      {
        return this.isEnableRequiredValue;
      }
      set
      {
        if (this.isEnableRequiredValue == value)
          return;
        this.SetIsEnableRequiredValue(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    internal void SetIsEnableRequiredValue(int rowIndex, bool value)
    {
      this.isEnableRequiredValue = value;
      this.OwnsEditingCopernicusFilterValue(rowIndex);
    }

    private void OnCommonChange()
    {
      if (this.DataGridView == null || this.DataGridView.IsDisposed || this.DataGridView.Disposing)
        return;
      if (this.RowIndex == -1)
        this.DataGridView.InvalidateColumn(this.ColumnIndex);
      else
        this.DataGridView.UpdateCellValue(this.ColumnIndex, this.RowIndex);
    }

    private bool OwnsEditingCopernicusFilterValue(int rowIndex)
    {
      if (rowIndex == -1 || this.DataGridView == null)
        return false;
      CopernicusDataGridViewCheckComboxEditingControl editingControl = this.DataGridView.EditingControl as CopernicusDataGridViewCheckComboxEditingControl;
      if (editingControl != null)
        return rowIndex == editingControl.EditingControlRowIndex;
      return false;
    }

    private CopernicusDataGridViewCheckComboxEditingControl EditingSelector
    {
      get
      {
        return this.DataGridView.EditingControl as CopernicusDataGridViewCheckComboxEditingControl;
      }
    }

    public override Type EditType
    {
      get
      {
        return CopernicusDataGridViewCheckComboxCell.defaultEditType;
      }
    }

    public override Type ValueType
    {
      get
      {
        Type valueType = base.ValueType;
        if (valueType != (Type) null)
          return valueType;
        return CopernicusDataGridViewCheckComboxCell.defaultValueType;
      }
    }

    public override object DefaultNewRowValue
    {
      get
      {
        return (object) string.Empty;
      }
    }

    public override object Clone()
    {
      CopernicusDataGridViewCheckComboxCell viewCheckComboxCell = base.Clone() as CopernicusDataGridViewCheckComboxCell;
      if (viewCheckComboxCell != null)
      {
        viewCheckComboxCell.IsEnableRequiredFilterValue = this.isEnableRequiredValue;
        viewCheckComboxCell.strCallbackFormHandler = this.strCallbackFormClass;
        viewCheckComboxCell.strCallbackFormPackageName = this.strCallbackFormPackageName;
        viewCheckComboxCell.IsFilterValueSelectableForCell = this.isFilterValueSelectableForCell;
        viewCheckComboxCell.FilterClassMetaDataForCell = this.filterClassMetaDataForCell;
      }
      return (object) viewCheckComboxCell;
    }

    public bool IsCellChecked
    {
      get
      {
        if (this.Tag == null)
          return false;
        return bool.Parse(this.Tag.ToString());
      }
      set
      {
        bool flag = this.Tag != null && bool.Parse(this.Tag.ToString());
        if (value == flag)
          return;
        this.Tag = (object) value;
      }
    }

    private event Action<Keys> MouseKeyCodeOnCellItem;

    public void RaiseMouseKeyCodeCellForward(Keys keycode)
    {
      if (this.MouseKeyCodeOnCellItem == null)
        return;
      Action<Keys> action = Interlocked.CompareExchange<Action<Keys>>(ref this.MouseKeyCodeOnCellItem, (Action<Keys>) null, (Action<Keys>) null);
      if (action == null)
        return;
      action(keycode);
    }

    public void AddMouseKeyCodeEventHandler(Action<Keys> newKeyCodeEventHandler)
    {
      this.MouseKeyCodeOnCellItem += newKeyCodeEventHandler;
    }

    public void RemoveMouseKeyCodeEventHandler(Action<Keys> newKeyCodeEventHandler)
    {
      this.MouseKeyCodeOnCellItem -= newKeyCodeEventHandler;
    }

    public void ReceiveWindowPaneMessage(Keys keyCode)
    {
      UserControlCheckCombox editingControl = this.DataGridView.EditingControl as UserControlCheckCombox;
      if (editingControl == null)
        return;
      editingControl.ReceiveWindowPaneMessageInUserControl(keyCode, this.RowIndex, this.ColumnIndex);
    }

    private void TagChangeEvent(bool isCellChecked, int iRowIndex, int iColumnIndex)
    {
      if (this.RowIndex != iRowIndex || this.ColumnIndex != iColumnIndex)
        return;
      bool flag = this.Tag != null && bool.Parse(this.Tag.ToString());
      if (isCellChecked == flag)
        return;
      this.Tag = (object) isCellChecked;
    }

    public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
    {
      base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
      UserControlCheckCombox editingControl = this.DataGridView.EditingControl as UserControlCheckCombox;
      if (editingControl == null)
        return;
      editingControl.BorderStyle = BorderStyle.None;
      editingControl.AddTagChangeMonitor(new Action<bool, int, int>(this.TagChangeEvent), this);
      editingControl.IsFilterValueSelectable = this.isFilterValueSelectableForCell;
      editingControl.CallbackModelFormPackageName = this.strCallbackFormPackageName;
      editingControl.CallbackModelFormClass = this.strCallbackFormHandler;
      editingControl.IsFilterBackendMandatory = this.isEnableRequiredValue;
      editingControl.IRowIndex = rowIndex;
      editingControl.IColumnIndex = this.ColumnIndex;
      editingControl.IsUserChecked = this.Tag != null && bool.Parse(this.Tag.ToString());
      editingControl.UpdateHintsThenControlValue(this.filterClassMetaDataForCell, this.Value == null || this.Value.ToString().Length <= 0 ? string.Empty : this.Value.ToString());
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public override void DetachEditingControl()
    {
      DataGridView dataGridView = this.DataGridView;
      if (dataGridView == null || dataGridView.EditingControl == null)
        throw new InvalidOperationException("Cell is detached or its grid has no editing control.");
      UserControlCheckCombox editingControl = dataGridView.EditingControl as UserControlCheckCombox;
      base.DetachEditingControl();
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
      if (this.DataGridView == null)
        return;
      base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts & ~(DataGridViewPaintParts.ContentForeground | DataGridViewPaintParts.ErrorIcon));
      Point currentCellAddress = this.DataGridView.CurrentCellAddress;
      if (currentCellAddress.X == this.ColumnIndex && currentCellAddress.Y == rowIndex && this.DataGridView.EditingControl != null)
        return;
      if (CopernicusDataGridViewCheckComboxCell.PartPainted(paintParts, DataGridViewPaintParts.ContentForeground))
      {
        Rectangle rectangle1 = this.BorderWidths(advancedBorderStyle);
        Rectangle editingControlBounds1 = cellBounds;
        editingControlBounds1.Offset(rectangle1.X, rectangle1.Y);
        editingControlBounds1.Width -= rectangle1.Right;
        editingControlBounds1.Height -= rectangle1.Bottom;
        if (cellStyle.Padding != Padding.Empty)
        {
          if (this.DataGridView.RightToLeft == RightToLeft.Yes)
            editingControlBounds1.Offset(cellStyle.Padding.Right, cellStyle.Padding.Top);
          else
            editingControlBounds1.Offset(cellStyle.Padding.Left, cellStyle.Padding.Top);
          editingControlBounds1.Width -= cellStyle.Padding.Horizontal;
          editingControlBounds1.Height -= cellStyle.Padding.Vertical;
        }
        Rectangle editingControlBounds2 = this.GetAdjustedEditingControlBounds(editingControlBounds1, cellStyle);
        bool flag1 = (cellState & DataGridViewElementStates.Selected) != DataGridViewElementStates.None;
        if (CopernicusDataGridViewCheckComboxCell.renderingBitmap.Width < editingControlBounds2.Width || CopernicusDataGridViewCheckComboxCell.renderingBitmap.Height < editingControlBounds2.Height)
        {
          CopernicusDataGridViewCheckComboxCell.renderingBitmap.Dispose();
          CopernicusDataGridViewCheckComboxCell.renderingBitmap = new Bitmap(editingControlBounds2.Width, editingControlBounds2.Height);
          CopernicusDataGridViewCheckComboxCell.emptyBitmap.Dispose();
          CopernicusDataGridViewCheckComboxCell.emptyBitmap = new Bitmap(editingControlBounds2.Width, editingControlBounds2.Height);
        }
        if (CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.Parent == null || !CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.Parent.Visible)
          CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.Parent = (Control) this.DataGridView;
        CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.Font = cellStyle.Font;
        CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.Width = editingControlBounds2.Width;
        CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.Height = editingControlBounds2.Height;
        CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.RightToLeft = this.DataGridView.RightToLeft;
        CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.Location = new Point(0, -CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.Height);
        Color baseColor = !CopernicusDataGridViewCheckComboxCell.PartPainted(paintParts, DataGridViewPaintParts.SelectionBackground) || !flag1 ? cellStyle.BackColor : cellStyle.SelectionBackColor;
        if (CopernicusDataGridViewCheckComboxCell.PartPainted(paintParts, DataGridViewPaintParts.Background))
        {
          if ((int) baseColor.A < (int) byte.MaxValue)
            baseColor = Color.FromArgb((int) byte.MaxValue, baseColor);
          CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.BackColor = baseColor;
        }
        Rectangle rectangle2 = new Rectangle(0, 0, editingControlBounds2.Width, editingControlBounds2.Height);
        if (rectangle2.Width > 0 && rectangle2.Height > 0)
        {
          CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.CallbackModelFormClass = this.strCallbackFormHandler;
          CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.CallbackModelFormPackageName = this.CallbackFormPackageName;
          CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.IsFilterValueSelectable = this.isFilterValueSelectableForCell;
          CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.IsFilterBackendMandatory = this.isEnableRequiredValue;
          CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.IRowIndex = rowIndex;
          CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.IColumnIndex = this.ColumnIndex;
          bool flag2 = this.Tag != null && bool.Parse(this.Tag.ToString());
          CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.IsUserChecked = flag2;
          CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.UpdateHintsThenControlValue(this.filterClassMetaDataForCell, this.Value == null || this.Value.ToString().Length <= 0 ? string.Empty : this.Value.ToString());
          if (this.Value != null && this.Value.ToString().Length > 0 || flag2)
          {
            CopernicusDataGridViewCheckComboxCell.paintingContorlforFilterValue.DrawToBitmap(CopernicusDataGridViewCheckComboxCell.renderingBitmap, rectangle2);
            graphics.DrawImage((Image) CopernicusDataGridViewCheckComboxCell.renderingBitmap, new Rectangle(editingControlBounds2.Location, editingControlBounds2.Size), rectangle2, GraphicsUnit.Pixel);
          }
          else
            graphics.DrawImage((Image) CopernicusDataGridViewCheckComboxCell.emptyBitmap, new Rectangle(editingControlBounds2.Location, editingControlBounds2.Size), rectangle2, GraphicsUnit.Pixel);
        }
      }
      if (!CopernicusDataGridViewCheckComboxCell.PartPainted(paintParts, DataGridViewPaintParts.ErrorIcon))
        return;
      base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, DataGridViewPaintParts.ErrorIcon);
    }

    private static bool PartPainted(DataGridViewPaintParts paintParts, DataGridViewPaintParts paintPart)
    {
      return (paintParts & paintPart) != DataGridViewPaintParts.None;
    }

    public override void PositionEditingControl(bool setLocation, bool setSize, Rectangle cellBounds, Rectangle cellClip, DataGridViewCellStyle cellStyle, bool singleVerticalBorderAdded, bool singleHorizontalBorderAdded, bool isFirstDisplayedColumn, bool isFirstDisplayedRow)
    {
      Rectangle editingControlBounds = this.GetAdjustedEditingControlBounds(this.PositionEditingPanel(cellBounds, cellClip, cellStyle, singleVerticalBorderAdded, singleHorizontalBorderAdded, isFirstDisplayedColumn, isFirstDisplayedRow), cellStyle);
      this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
      this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
    }

    private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
    {
      ++editingControlBounds.X;
      editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);
      return editingControlBounds;
    }

    protected override Size GetPreferredSize(Graphics graphics, DataGridViewCellStyle cellStyle, int rowIndex, Size constraintSize)
    {
      if (this.DataGridView == null)
        return new Size(-1, -1);
      return base.GetPreferredSize(graphics, cellStyle, rowIndex, constraintSize);
    }

    protected override Rectangle GetErrorIconBounds(Graphics graphics, DataGridViewCellStyle cellStyle, int rowIndex)
    {
      Rectangle errorIconBounds = base.GetErrorIconBounds(graphics, cellStyle, rowIndex);
      errorIconBounds.X = this.DataGridView.RightToLeft != RightToLeft.Yes ? errorIconBounds.Left - 16 : errorIconBounds.Left + 16;
      return errorIconBounds;
    }

    protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context)
    {
      return base.GetFormattedValue(value, rowIndex, ref cellStyle, valueTypeConverter, formattedValueTypeConverter, context);
    }

    public override bool KeyEntersEditMode(KeyEventArgs e)
    {
      NumberFormatInfo numberFormat = CultureInfo.CurrentCulture.NumberFormat;
      Keys keys = Keys.None;
      string negativeSign = numberFormat.NegativeSign;
      if (!string.IsNullOrEmpty(negativeSign) && negativeSign.Length == 1)
        keys = (Keys) CopernicusDataGridViewCheckComboxCell.VkKeyScan(negativeSign[0]);
      return (char.IsDigit((char) e.KeyCode) || e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9 || (keys == e.KeyCode || Keys.Subtract == e.KeyCode)) && (!e.Shift && !e.Alt && !e.Control);
    }

    public string getConvertedCodeDescription(string codevalue)
    {
      PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC aeSCodeValueWDesc = ((IEnumerable<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>) this.filterClassMetaDataForCell.FilterCodeListEntry).ToList<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>().Find((Predicate<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>) (n => n.CONTENT.Trim().Equals(codevalue.Trim())));
      if (aeSCodeValueWDesc == null)
        return string.Empty;
      return aeSCodeValueWDesc.GetCodeAndDescription;
    }

    public string getConvertedCodeValue()
    {
      string codeText = this.EditedFormattedValue.ToString().Trim();
      if (this.filterClassMetaDataForCell == null || this.filterClassMetaDataForCell.FilterCodeListEntry == null)
        return codeText;
      codeText = codeText.Split('-')[0].Trim();
      PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC aeSCodeValueWDesc1 = ((IEnumerable<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>) this.filterClassMetaDataForCell.FilterCodeListEntry).ToList<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>().Find((Predicate<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>) (n => n.CONTENT.Trim().Equals(codeText)));
      if (aeSCodeValueWDesc1 != null)
        return aeSCodeValueWDesc1.CONTENT;
      PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC aeSCodeValueWDesc2 = ((IEnumerable<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>) this.filterClassMetaDataForCell.FilterCodeListEntry).ToList<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>().Find((Predicate<PDI_AE_GET_VALUE_HELP.PDI_AE_S_CODE_VALUE_W_DESC>) (n => n.DESCRIPTION.Trim().Equals(codeText)));
      if (aeSCodeValueWDesc2 == null)
        return string.Empty;
      return aeSCodeValueWDesc2.CONTENT;
    }

    public override string ToString()
    {
      return "CopernicusDataGridViewCheckVariableCell { ColumnIndex=" + this.ColumnIndex.ToString((IFormatProvider) CultureInfo.CurrentCulture) + ", RowIndex=" + this.RowIndex.ToString((IFormatProvider) CultureInfo.CurrentCulture) + " }";
    }

    internal static HorizontalAlignment TranslateAlignment(DataGridViewContentAlignment align)
    {
      if ((align & CopernicusDataGridViewCheckComboxCell.anyRight) != DataGridViewContentAlignment.NotSet)
        return HorizontalAlignment.Right;
      return (align & CopernicusDataGridViewCheckComboxCell.anyCenter) != DataGridViewContentAlignment.NotSet ? HorizontalAlignment.Center : HorizontalAlignment.Left;
    }
  }
}
