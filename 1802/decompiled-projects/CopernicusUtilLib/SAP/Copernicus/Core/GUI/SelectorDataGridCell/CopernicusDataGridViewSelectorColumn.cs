﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.SelectorDataGridCell.CopernicusDataGridViewSelectorColumn
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.SelectorDataGridCell
{
  public class CopernicusDataGridViewSelectorColumn : DataGridViewColumn
  {
    private static string strCellInternError = "Value provided for CellTemplate must be of type SAP.Copernicus.Core.GUI.CustomizedDataGridCell.CopernicusDataGridViewSelectorCell or derive from it.";

    public CopernicusDataGridViewSelectorColumn()
      : base((DataGridViewCell) new CopernicusDataGridViewSelectorCell())
    {
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    public override DataGridViewCell CellTemplate
    {
      get
      {
        return base.CellTemplate;
      }
      set
      {
        CopernicusDataGridViewSelectorCell viewSelectorCell = value as CopernicusDataGridViewSelectorCell;
        if (value != null && viewSelectorCell == null)
          throw new InvalidCastException(CopernicusDataGridViewSelectorColumn.strCellInternError);
        base.CellTemplate = value;
      }
    }

    [DefaultValue("")]
    [Description("Callback Form With SAP.Copernicus.Core.GUI.SelectorDataGridCell.CopernicusPopupForm.")]
    [Category("Behavior")]
    public string CallbackFormHandler
    {
      get
      {
        if (this.CopernicusSelectorCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusSelectorCellTemplate.strCallbackFormHandler;
      }
      set
      {
        if (this.CopernicusSelectorCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusSelectorCellTemplate.strCallbackFormHandler = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewSelectorCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewSelectorCell;
          if (cell != null)
            cell.SetCallbackFormHandler(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    [DefaultValue("")]
    [Description("Callback Form Assembly Name.")]
    [Category("Behavior")]
    public string CallbackFormPackageName
    {
      get
      {
        if (this.CopernicusSelectorCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        return this.CopernicusSelectorCellTemplate.CallbackFormPackageName;
      }
      set
      {
        if (this.CopernicusSelectorCellTemplate == null)
          throw new InvalidOperationException("Operation cannot be completed because this DataGridViewColumn does not have a CellTemplate.");
        this.CopernicusSelectorCellTemplate.CallbackFormPackageName = value;
        if (this.DataGridView == null)
          return;
        DataGridViewRowCollection rows = this.DataGridView.Rows;
        int count = rows.Count;
        for (int rowIndex = 0; rowIndex < count; ++rowIndex)
        {
          CopernicusDataGridViewSelectorCell cell = rows.SharedRow(rowIndex).Cells[this.Index] as CopernicusDataGridViewSelectorCell;
          if (cell != null)
            cell.SetCallbackFormPackageName(rowIndex, value);
        }
        this.DataGridView.InvalidateColumn(this.Index);
      }
    }

    private CopernicusDataGridViewSelectorCell CopernicusSelectorCellTemplate
    {
      get
      {
        return (CopernicusDataGridViewSelectorCell) this.CellTemplate;
      }
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder(100);
      stringBuilder.Append("SAP.Copernicus.Core.GUI.CustomizedDataGridCell.CopernicusDataGridViewSelectorColumn { Name=");
      stringBuilder.Append(this.Name);
      stringBuilder.Append(", Index=");
      stringBuilder.Append(this.Index.ToString((IFormatProvider) CultureInfo.CurrentCulture));
      stringBuilder.Append(" }");
      return stringBuilder.ToString();
    }
  }
}
