﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.Update.FormUpdateContainer
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol.Update
{
  public class FormUpdateContainer : Form
  {
    private IContainer components;

    public FormUpdateContainer()
    {
      this.InitializeComponent();
      this.FormBorderStyle = FormBorderStyle.FixedSingle;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormUpdateContainer));
      this.SuspendLayout();
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(424, 101);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.Name = nameof (FormUpdateContainer);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "SAP Cloud Applications Studio";
      this.ResumeLayout(false);
    }
  }
}
