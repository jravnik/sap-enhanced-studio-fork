﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginSession
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public class LoginSession : ILoginSession
  {
    private readonly string _client;
    private readonly string _userId;
    private readonly string _terminal;
    private readonly string _time;

    internal LoginSession(string client, string userId, string terminal, string time)
    {
      this._client = client;
      this._userId = userId;
      this._terminal = terminal;
      this._time = time;
    }

    public string Client
    {
      get
      {
        return this._client;
      }
    }

    public string UserId
    {
      get
      {
        return this._userId;
      }
    }

    public string Terminal
    {
      get
      {
        return this._terminal;
      }
    }

    public string Time
    {
      get
      {
        return this._time;
      }
    }
  }
}
