﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.ET_APPROVAL_STEP_ITEM
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class ET_APPROVAL_STEP_ITEM
  {
    [DataMember]
    public string NAME;
    [DataMember]
    public string TYPE;
    [DataMember]
    public string XREP_PATH;
    [DataMember]
    public string TASK_TYPE;
    [DataMember]
    public string ACTIVE;
  }
}
