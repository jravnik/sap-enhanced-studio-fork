﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.TTE_DT_TREE_GET_PDI
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class TTE_DT_TREE_GET_PDI : AbstractRemoteFunction<TTE_DT_TREE_GET_PDI.ImportingType, TTE_DT_TREE_GET_PDI.ExportingType, TTE_DT_TREE_GET_PDI.ChangingType, TTE_DT_TREE_GET_PDI.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0286601EE2B792FE7DECB0C14F";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string I_TREEID;
      [DataMember]
      public string I_TREETYPE;
      [DataMember]
      public string I_VERSION;
      [DataMember]
      public string I_MODE;
      [DataMember]
      public string IV_XMLDOC;
      [DataMember]
      public TTEC_DT_TEXTS[] I_DT_TEXTS;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public TTE_DT_TREE_GET_PDI.TTET_DT_TABL_NODES[] ET_TREE_NODES;
      [DataMember]
      public TTE_DT_TREE_GET_PDI.TTET_DT_TARGET[] ET_HDR_TARGET;
      [DataMember]
      public TTE_DT_TREE_GET_PDI.TTET_DT_TABL_CTX[] ET_TARGET_CTX;
      [DataMember]
      public TTE_DT_TREE_GET_PDI.TTET_DT_TABL_FLD[] ET_TEST_FIELDS;
      [DataMember]
      public TTE_DT_TREE_GET_PDI.TTET_DT_TABL_ISVALUE[] ET_TEST_VALUES;
      [DataMember]
      public TTE_DT_TREE_GET_PDI.TTET_DT_TABL_CTX[] ET_TEST_CTX;
      [DataMember]
      public TTE_DT_TREE_GET_PDI.TTET_DT_TABL_COMP[] ET_TEST_CMP;
      [DataMember]
      public TTE_DT_TREE_GET_PDI.TTET_DT_TAB_RESULT_L[] ET_RESULT_NODES;
      [DataMember]
      public TTE_DT_TREE_GET_PDI.TTET_DT_TABL_CTX[] ET_RESULT_CTX;
      [DataMember]
      public TTE_DT_TREE_GET_PDI.TTET_DT_TABL_ERR[] ET_ERROR_NODES;
      [DataMember]
      public TTE_DT_TREE_GET_PDI.TTET_DT_NOTE_LINE[] ET_NOTES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }

    [DataContract]
    public class TTET_DT_TABL_NODES
    {
      [DataMember]
      public string NODEKEY;
      [DataMember]
      public string RELATKEY;
      [DataMember]
      public string HEIGHT;
      [DataMember]
      public string TYPE;
      [DataMember]
      public string ANSWER;
      [DataMember]
      public string DESCRIPTION;
      [DataMember]
      public string TEXTID;
      [DataMember]
      public string SUBTYPE;
      [DataMember]
      public string STATE;
    }

    [DataContract]
    public class TTET_DT_TARGET
    {
      [DataMember]
      public string FIELDNAME;
      [DataMember]
      public string DESCRIPTION;
    }

    [DataContract]
    public class TTET_DT_TABL_CTX
    {
      [DataMember]
      public string NODEKEY;
      [DataMember]
      public string RELATKEY;
      [DataMember]
      public string FIELDNAME;
      [DataMember]
      public string CTX_FLDNAME;
      [DataMember]
      public string CTX_VALUE;
      [DataMember]
      public string DESCRIPTION;
    }

    [DataContract]
    public class TTET_DT_TABL_FLD
    {
      [DataMember]
      public string NODEKEY;
      [DataMember]
      public string RELATKEY;
      [DataMember]
      public string FIELDNAME;
      [DataMember]
      public int FLDPOS;
    }

    [DataContract]
    public class TTET_DT_TABL_ISVALUE
    {
      [DataMember]
      public string NODEKEY;
      [DataMember]
      public string RELATKEY;
      [DataMember]
      public string FIELDNAME;
      [DataMember]
      public string VALUE;
      [DataMember]
      public string DESCRIPTION;
    }

    [DataContract]
    public class TTET_DT_TABL_COMP
    {
      [DataMember]
      public string NODEKEY;
      [DataMember]
      public string RELATKEY;
      [DataMember]
      public string FIELDNAME;
      [DataMember]
      public string CTX_FLDNAME;
      [DataMember]
      public string CTX_VALUE;
      [DataMember]
      public string DESCRIPTION;
      [DataMember]
      public int FLDPOS;
    }

    [DataContract]
    public class TTET_DT_TAB_RESULT_L
    {
      [DataMember]
      public string NODEKEY;
      [DataMember]
      public string RELATKEY;
      [DataMember]
      public string FIELDNAME;
      [DataMember]
      public string VALUE;
      [DataMember]
      public string DESCRIPTION;
    }

    [DataContract]
    public class TTET_DT_TABL_ERR
    {
      [DataMember]
      public string NODEKEY;
      [DataMember]
      public string RELATKEY;
      [DataMember]
      public string ERR_CLASS;
      [DataMember]
      public string ERR_NO;
      [DataMember]
      public string ERR_TEXT;
    }

    [DataContract]
    public class TTET_DT_NOTE_LINE
    {
      [DataMember]
      public string NOTE;
    }
  }
}
