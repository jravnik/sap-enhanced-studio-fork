﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.SODcheckHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class SODcheckHandler : JSONHandler
  {
    public bool IsSODactive()
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_UTIL_SOD_CHECK pdiUtilSodCheck = new PDI_UTIL_SOD_CHECK();
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) pdiUtilSodCheck, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (pdiUtilSodCheck.Exporting != null)
      {
        if (pdiUtilSodCheck.Exporting.EV_SUCCESS == "")
          this.reportServerSideProtocolException((SAPFunctionModule) pdiUtilSodCheck, false, false, false);
        else
          return pdiUtilSodCheck.Exporting.EV_SOD_ACTIVE == "X";
      }
      return false;
    }
  }
}
