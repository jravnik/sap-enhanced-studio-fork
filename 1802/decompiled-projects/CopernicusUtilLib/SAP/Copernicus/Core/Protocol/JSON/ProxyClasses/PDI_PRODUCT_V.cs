﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PRODUCT_V
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PRODUCT_V
  {
    [DataMember]
    public string PRODUCT;
    [DataMember]
    public string VERSION;
    [DataMember]
    public string VERSION_ID;
    [DataMember]
    public string SAP_PRODUCT;
    [DataMember]
    public string SAP_PROD_RELEASE;
    [DataMember]
    public string STATUS;
    [DataMember]
    public string CERTI_STATUS;
    [DataMember]
    public string ASSEMBLY_STATUS;
    [DataMember]
    public string PREVIOUS_VERSION;
    [DataMember]
    public string LOCAL_VERSION;
    [DataMember]
    public string STATUS_RUNTIME;
    [DataMember]
    public string PPMS_NR;
    [DataMember]
    public string HAS_ADAPTATIONS;
    [DataMember]
    public string EDIT_STATUS;
  }
}
