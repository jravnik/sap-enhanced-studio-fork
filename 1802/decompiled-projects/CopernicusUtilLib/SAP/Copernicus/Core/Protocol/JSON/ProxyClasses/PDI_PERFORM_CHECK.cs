﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PERFORM_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PERFORM_CHECK : AbstractRemoteFunction<PDI_PERFORM_CHECK.ImportingType, PDI_PERFORM_CHECK.ExportingType, PDI_PERFORM_CHECK.ChangingType, PDI_PERFORM_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01138A1EE0AFEA287164321C26";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PATH;
      [DataMember]
      public string[] IT_PATH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public LXE_PCX_S1[] ET_TEXTS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public ZPDI_CHECK_STR[] ET_CHECK_INFO;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
