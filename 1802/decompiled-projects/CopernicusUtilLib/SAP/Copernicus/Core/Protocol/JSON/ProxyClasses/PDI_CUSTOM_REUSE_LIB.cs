﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_CUSTOM_REUSE_LIB
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_CUSTOM_REUSE_LIB : AbstractRemoteFunction<PDI_CUSTOM_REUSE_LIB.ImportingType, PDI_CUSTOM_REUSE_LIB.ExportingType, PDI_CUSTOM_REUSE_LIB.ChangingType, PDI_CUSTOM_REUSE_LIB.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0383AC1ED29F86415AFBED4CF7";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember(Order = 1)]
      public int IV_OPERATION;
      [DataMember(Order = 2)]
      public string IV_SOLUTION_NAMESPACE;
      [DataMember(Order = 3)]
      public string IV_XML;
      [DataMember(Order = 4)]
      public string IV_XREP_FILE_PATH;
      [DataMember(Order = 5)]
      public string IV_REDUCED_CHECKS;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_LM_S_MSG_LIST[] ET_MSG_LIST;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
