﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.BOgetGroupIdHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class BOgetGroupIdHandler : JSONHandler
  {
    public string getGroupId(string BOproxy, string filename)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BO_GET_GROUP_ID pdiBoGetGroupId = new PDI_BO_GET_GROUP_ID();
      pdiBoGetGroupId.Importing = new PDI_BO_GET_GROUP_ID.ImportingType();
      pdiBoGetGroupId.Importing.IV_PROXY = BOproxy;
      pdiBoGetGroupId.Importing.IV_UI_NAME = filename;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) pdiBoGetGroupId, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return "";
      }
      if (pdiBoGetGroupId.Exporting == null)
        return "";
      if (!(pdiBoGetGroupId.Exporting.EV_SUCCESS == ""))
        return pdiBoGetGroupId.Exporting.EV_GROUP_CODE;
      this.reportServerSideProtocolException((SAPFunctionModule) pdiBoGetGroupId, false, false, false);
      return "";
    }
  }
}
