﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_GET_DRC_BO_NODES
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_GET_DRC_BO_NODES : AbstractRemoteFunction<PDI_1O_GET_DRC_BO_NODES.ImportingType, PDI_1O_GET_DRC_BO_NODES.ExportingType, PDI_1O_GET_DRC_BO_NODES.ChangingType, PDI_1O_GET_DRC_BO_NODES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F91ED7AFC2D5F3C5589B55";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_XBO_NAME;
      [DataMember]
      public string IV_XBO_NAMESPACE;
      [DataMember]
      public bool IS_XBO_CUSTOM_NODE;
      [DataMember]
      public bool IS_XBO;
      [DataMember]
      public string[] IT_BO_NODE_NAMES;
      [DataMember]
      public string IV_SOLUTION;
      [DataMember]
      public string IV_XREP_FILE_PATH;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string[] ET_BO_NODE_NAMES;
      [DataMember]
      public string[] ET_BO_PROJECTIONS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
