﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_S_EXT_FIELDS
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_S_EXT_FIELDS
  {
    [DataMember]
    public string BO_ESR_NAMESPACE;
    [DataMember]
    public string BO_ESR_NAME;
    [DataMember]
    public string BO_PRX_NAME;
    [DataMember]
    public string MAX_BO_PRX_NAME;
    [DataMember]
    public string NODE_ESR_NAME;
    [DataMember]
    public string NODE_PRX_NAME;
    [DataMember]
    public string PARENT_NODE_PRX_NAME;
    [DataMember]
    public string FIELD_ESR_NAME;
    [DataMember]
    public string FIELD_TYPE;
    [DataMember]
    public string FIELD_TYPE_CAT;
    [DataMember]
    public string DATA_TYPE_NAME;
    [DataMember]
    public string DATA_TYPE_NAMESPACE;
    [DataMember]
    public string LENGTH;
    [DataMember]
    public string DECIMALS;
    [DataMember]
    public string EXTENDED;
    [DataMember]
    public string RELEASED;
    [DataMember]
    public string DISABLED;
    [DataMember]
    public string FIELD_LABEL;
    [DataMember]
    public string FIELD_TOOLTIP;
    [DataMember]
    public string FIELD_OWNER_CODE;
    [DataMember]
    public string TRANSIENT;
    [DataMember]
    public string DEFAULT_VALUE;
    [DataMember]
    public string DEFAULT_CODE;
    [DataMember]
    public PDI_EXT_S_ID_TARGET_ATTRIBUTE ID_TARGET_ATTRIBUTE;
    [DataMember]
    public string OFF_ENABLED;
  }
}
