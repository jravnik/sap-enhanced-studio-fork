﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.MDROHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class MDROHandler : JSONHandler
  {
    public int Get_OTC_Counter(string IV_XREP_FILE_PATH)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_MDRO_GET_OTC_COUNTER mdroGetOtcCounter = new PDI_MDRO_GET_OTC_COUNTER();
      mdroGetOtcCounter.Importing = new PDI_MDRO_GET_OTC_COUNTER.ImportingType();
      mdroGetOtcCounter.Importing.IV_XREP_FILE_PATH = IV_XREP_FILE_PATH;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) mdroGetOtcCounter, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (mdroGetOtcCounter.Exporting != null)
      {
        if (!(mdroGetOtcCounter.Exporting.EV_SUCCESS == "") || mdroGetOtcCounter.Exporting.ET_MESSAGES == null || mdroGetOtcCounter.Exporting.ET_MESSAGES.Length <= 0)
          return mdroGetOtcCounter.Exporting.EV_OTC_COUNTER;
        this.reportServerSideProtocolException((SAPFunctionModule) mdroGetOtcCounter, false, false, false);
      }
      return -1;
    }
  }
}
