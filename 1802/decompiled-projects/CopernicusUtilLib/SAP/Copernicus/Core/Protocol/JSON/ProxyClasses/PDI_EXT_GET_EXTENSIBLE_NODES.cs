﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_GET_EXTENSIBLE_NODES
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_GET_EXTENSIBLE_NODES : AbstractRemoteFunction<PDI_EXT_GET_EXTENSIBLE_NODES.ImportingType, PDI_EXT_GET_EXTENSIBLE_NODES.ExportingType, PDI_EXT_GET_EXTENSIBLE_NODES.ChangingType, PDI_EXT_GET_EXTENSIBLE_NODES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DEFB1A948793992D61E";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BO_ESRNAME;
      [DataMember]
      public string IV_BO_ESRNAMESPACE;
      [DataMember]
      public string IV_SOLUTION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_EXT_S_EXT_NODE[] ET_EXT_NODES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_EXT_S_EXT_FIELDS[] ET_NODES_FIELDS;
      [DataMember]
      public PDI_EXT_S_EXT_FIELD_SCENARIOS[] ET_EXT_FIELD_SCENARIOS;
      [DataMember]
      public PDI_EXT_S_EXT_ACTION[] ET_EXT_ACTION;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
