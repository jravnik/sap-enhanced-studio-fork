﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_FMTEXT_SYNTAX_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_RI_FMTEXT_SYNTAX_CHECK : AbstractRemoteFunction<PDI_RI_FMTEXT_SYNTAX_CHECK.ImportingType, PDI_RI_FMTEXT_SYNTAX_CHECK.ExportingType, PDI_RI_FMTEXT_SYNTAX_CHECK.ChangingType, PDI_RI_FMTEXT_SYNTAX_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01151F1EE094DDA7EA9690C29D";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_ESR_NAMESPACE;
      [DataMember]
      public string IV_ESR_FMT_NAME;
      [DataMember]
      public string IV_FMT_PRXNAME;
      [DataMember]
      public string IV_SNIPPET_PATH;
      [DataMember]
      public string IV_PARTNER_NAMESPACE;
      [DataMember]
      public string IV_XREP_SESSION_ID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_SFW_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
