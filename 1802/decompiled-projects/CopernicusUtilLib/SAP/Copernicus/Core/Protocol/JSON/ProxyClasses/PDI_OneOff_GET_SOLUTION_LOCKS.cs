﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_OneOff_GET_SOLUTION_LOCKS
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_OneOff_GET_SOLUTION_LOCKS : AbstractRemoteFunction<PDI_OneOff_GET_SOLUTION_LOCKS.ImportingType, PDI_OneOff_GET_SOLUTION_LOCKS.ExportingType, PDI_OneOff_GET_SOLUTION_LOCKS.ChangingType, PDI_OneOff_GET_SOLUTION_LOCKS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0123D21ED094953839EBDE55E8";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_1O_SOLUTION_LOCK[] ET_SOLUTION_LOCKS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
