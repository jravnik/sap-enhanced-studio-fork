﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_RAL_RELEVANT_BO_NODES
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_GET_RAL_RELEVANT_BO_NODES : AbstractRemoteFunction<PDI_GET_RAL_RELEVANT_BO_NODES.ImportingType, PDI_GET_RAL_RELEVANT_BO_NODES.ExportingType, PDI_GET_RAL_RELEVANT_BO_NODES.ChangingType, PDI_GET_RAL_RELEVANT_BO_NODES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F91EE7B4A0A77B26DFC42B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public IT_BO_NODE[] IT_BO_NODE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public ET_RAL_RELEVANT_FIELD[] ET_RAL_RELEVANT_FIELD;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
