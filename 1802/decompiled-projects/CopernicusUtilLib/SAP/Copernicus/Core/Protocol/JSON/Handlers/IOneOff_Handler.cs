﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.IOneOff_Handler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public interface IOneOff_Handler
  {
    bool CanDeleteSolution(string Name);

    bool PDI_1O_ASSEMBLE_AS_COPY(string IV_SOLUTION_NAME, string IV_COPY_SOLUTION_DESCRIPTION, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SOLUTION_ASSEMBLE, out string EV_COPY_SOLUTION_NAME);

    bool PDI_1O_COPY_SOLUTION(string IV_PRODUCT_NAME, out string EV_SUCCESS, out string EV_NEW_PRODUCT_NAME, out string EV_NEW_PATCH_VERSION, out PDI_RI_S_MESSAGE[] ET_MESSAGES);

    bool PDI_1O_ENABLE(string SolutionName);

    bool PDI_1O_DISABLE(string SolutionName);

    bool PDI_1O_ENABLE_CHECK(string solutionName, out bool revokeIsPossible, out bool isRemoteWSSyncMergeRelevant);

    bool PDI_1O_IS_COPY_REQUIRED(string IV_PRODUCT_NAME, out string EV_SUCCESS, out string EV_COPY_REQUIRED, out PDI_RI_S_MESSAGE[] ET_MESSAGES);

    bool PDI_1O_PRODUCT_DELETE(string IV_PRODUCT_NAME);

    bool PDI_1O_PRODUCT_DELETE_2(string IV_PRODUCT_NAME, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES);

    bool PDI_1O_MC_CREATE_INSTALL_KEY(string IV_GENERATION_NAMESPACE, string IV_CUSTOMER_ID, out string EV_INSTALLATION_KEY, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES);

    bool PDI_1O_MC_GET_QUALITY_INFO(string IV_GENERATION_NAMESPACE, out string EV_VALID_QUALITY_REVIEW_EXISTS, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_QUALITY_IS_VALID_UNTIL);

    bool PDI_OneOff_ACTIVATE(string IV_SOLUTION_NAME, bool isPatchActivation, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES);

    bool PDI_OneOff_ACTIVATE_DELTA(string IV_SOLUTION_NAME, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES);

    bool PDI_OneOff_ASSEMBLE(string IV_SOLUTION_NAME, string IV_MCUST_DOWNLOAD_FOR_DELIVERY, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SOLUTION_ASSEMBLE);

    bool PDI_OneOff_CREATE_PATCH(string IV_PRODUCT_NAME, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES);

    bool PDI_OneOff_CREATE_PATCH_BATCH(string IV_PRODUCT_NAME, bool IV_DELETION_PATCH, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES);

    bool PDI_OneOff_GET_SOLUTION_INFOS(string IV_SOLUTION_NAME, string IV_LOGLEVEL, bool suppressPopup, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out PDI_1O_SOLUTION_HEADER ES_SOLUTION_HEADER);

    bool PDI_OneOff_GET_SOLUTION_LOCKS(string IV_SOLUTION_NAME, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out PDI_1O_SOLUTION_LOCK[] ET_SOLUTION_LOCKS);

    bool PDI_OneOff_POST_PROCESSING(string IV_SOLUTION_NAME, bool isPatchActivation, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES);

    bool PDI_OneOff_REACTIVATE(string IV_SOLUTION_NAME, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES);

    bool PDI_OneOff_SDI_GET_PROGRESS(string IV_PRODUCT_NAME, string IV_MAJOR_VERSION, string IV_PATCH_VERSION, out string EV_FINISHED, out string EV_PROGRESS_TEXT, out string EV_PHASE, out string EV_PHASE_STATUS);

    bool PDI_OneOff_UPLOAD(string IV_SOLUTION_ASSEMBLE, string IV_INSTALLATION_KEY, string IV_IS_UPLOAD_INTO_PATCH_SOL, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SOLUTION_NAME, out string EV_MAJOR_VERSION);

    bool PDIGetSecurityInfo(string IV_KTD_NAME, out string EV_HTML, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS);

    bool PDIGetZipInfo(string IV_SOLUTION_ASSEMBLE, string IV_FILE_NAME, out string EV_SOLUTION_NAME, out string EV_PATCH_VERSION, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_TOBE_ALIASED, out string EV_SUCCESS, out string EV_MAJOR_VERSION, out string EV_CORRECTION_SOLUTION, out string EV_DESCRIPTION, out PDI_1O_S_SDI_FILE[] ET_WEB_SERVICE, out string EV_WRITE_ACCESS_TO_SAP_BO, out string EV_PROJECT_TYPE, out string EV_SAP_ENTITIES_USED, out string EV_SAP_ENTITY_ACCESS, out string EV_USAGE_CATEGORY_OUTBOUND, out string EV_COMPILER_MIG_NEEDED, out string EV_REQ_MULTICUST_INST_KEY, out string EV_EXT_SOLUTION_NAME, out string EV_CAN_IMPORT_ORIGINAL, out string EV_PATCH_SOLUTION_EXISTS);

    bool PDIOneOffAssignUserToPartner(string IV_PARTNER, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS);

    bool PDIOneOffCreatePartner(string IV_PARTNER, string IV_CUSTOMER_ID, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS);

    bool PDIOneOffGetPartner(out PDI_1O_PARTNER_LIST ES_CURRENT_PARTNER, out PDI_1O_PARTNER_LIST[] ET_PARTNER, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS);

    bool PDIOneOffImportTemplate(string IV_SOLUTION_NAME, string IV_SOLUTION_ASSEMBLE, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS);

    bool PdiOneOffProductTenantsStatus(string pstrProductName, out string pstrSuccess, out PDI_RI_S_MESSAGE[] ptblMessages, out PDI_1O_PRODUCT_TENANT_STATUS[] ptblTenantsStatus);
  }
}
