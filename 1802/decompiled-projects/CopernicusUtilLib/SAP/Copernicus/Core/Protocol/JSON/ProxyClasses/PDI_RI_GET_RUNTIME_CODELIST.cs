﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_GET_RUNTIME_CODELIST
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_RI_GET_RUNTIME_CODELIST : AbstractRemoteFunction<PDI_RI_GET_RUNTIME_CODELIST.ImportingType, PDI_RI_GET_RUNTIME_CODELIST.ExportingType, PDI_RI_GET_RUNTIME_CODELIST.ChangingType, PDI_RI_GET_RUNTIME_CODELIST.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194EC88B8F0AC9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PROXY_NAME;
      [DataMember]
      public string[] IT_CODES;
      [DataMember]
      public string IV_LANGUAGE_CODE;
      [DataMember]
      public string IV_LIST_ID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_CODELIST[] ET_CODE_LISTS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
