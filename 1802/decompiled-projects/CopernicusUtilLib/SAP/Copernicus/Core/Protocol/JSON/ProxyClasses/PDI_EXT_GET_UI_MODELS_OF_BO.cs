﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_GET_UI_MODELS_OF_BO
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_GET_UI_MODELS_OF_BO : AbstractRemoteFunction<PDI_EXT_GET_UI_MODELS_OF_BO.ImportingType, PDI_EXT_GET_UI_MODELS_OF_BO.ExportingType, PDI_EXT_GET_UI_MODELS_OF_BO.ChangingType, PDI_EXT_GET_UI_MODELS_OF_BO.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DEFB1A94F2E7D6F963F";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_XBO_FILENAME;
      [DataMember]
      public string IV_PRJ_NAME;
      [DataMember]
      public string IV_PRJ_NAMESPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_EXT_S_REF_FLD[] ET_REFERENCE_FIELDS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
