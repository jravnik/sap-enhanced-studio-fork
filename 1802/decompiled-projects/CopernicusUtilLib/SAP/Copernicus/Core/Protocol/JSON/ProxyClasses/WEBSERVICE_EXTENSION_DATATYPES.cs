﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.WEBSERVICE_EXTENSION_DATATYPES
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class WEBSERVICE_EXTENSION_DATATYPES
  {
    [DataMember]
    public string MANDT;
    [DataMember]
    public string SOLUTION_NAME;
    [DataMember]
    public string EXTERNAL_INTERFACE_NAME;
    [DataMember]
    public string EXTERNAL_OPERATION_NAME;
    [DataMember]
    public string INTERNAL_INTERFACE_NAME;
    [DataMember]
    public string INTERNAL_OPERATION_NAME;
    [DataMember]
    public string TRANSFORMATION_DEF_CLASS_NAME;
    [DataMember]
    public string TRANSFORMATION_DEF_METH_NAME;
    [DataMember]
    public string INPUT_DATA_TYPE_NAME;
    [DataMember]
    public string OUTPUT_DATA_TYPE_NAME;
  }
}
