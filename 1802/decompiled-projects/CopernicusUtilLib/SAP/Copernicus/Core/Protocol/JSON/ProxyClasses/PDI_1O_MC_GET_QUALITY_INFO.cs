﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_MC_GET_QUALITY_INFO
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_1O_MC_GET_QUALITY_INFO : AbstractRemoteFunction<PDI_1O_MC_GET_QUALITY_INFO.ImportingType, PDI_1O_MC_GET_QUALITY_INFO.ExportingType, PDI_1O_MC_GET_QUALITY_INFO.ChangingType, PDI_1O_MC_GET_QUALITY_INFO.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E04DC271EE388A3D9D7803D449F";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_GENERATION_NAMESPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_VALID_QUALITY_REVIEW_EXISTS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_QUALITY_IS_VALID_UNTIL;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
