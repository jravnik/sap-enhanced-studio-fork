﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.ItemFeatureHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.IO;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class ItemFeatureHandler : JSONHandler
  {
    public bool getItemsAndFeatures(string sapSolutionType, string pdiSubType, out PDI_PROJECT_ITEM_SOLUTION[] projectItemSolution, out PDI_PROJECT_ITEM[] projectItem, out PDI_FEATURE[] feature)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_GET_ITEMS_AND_FEATURES itemsAndFeatures = new PDI_GET_ITEMS_AND_FEATURES();
      itemsAndFeatures.Importing = new PDI_GET_ITEMS_AND_FEATURES.ImportingType();
      itemsAndFeatures.Importing.IV_PDI_SUBTYPE = pdiSubType;
      itemsAndFeatures.Importing.IV_SAP_SOLUTION_TYPE = sapSolutionType;
      projectItemSolution = (PDI_PROJECT_ITEM_SOLUTION[]) null;
      projectItem = (PDI_PROJECT_ITEM[]) null;
      feature = (PDI_FEATURE[]) null;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) itemsAndFeatures, false, false, false);
        projectItemSolution = itemsAndFeatures.Exporting.ET_PROJECT_ITEM_SOLUTION;
        projectItem = itemsAndFeatures.Exporting.ET_PROJECT_ITEM;
        feature = itemsAndFeatures.Exporting.ET_FEATURE;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
      if (itemsAndFeatures.Exporting.EV_SUCCESS.Equals("X"))
        return true;
      this.reportServerSideProtocolException((SAPFunctionModule) itemsAndFeatures, false, false, false);
      return false;
    }

    public void getFeatureRestrictions(string[] features, out PDI_FEATURE_RESTRICTION_RES[] featureRes)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      featureRes = (PDI_FEATURE_RESTRICTION_RES[]) null;
      PDI_RESTRICTION_ENTRY_CHECK restrictionEntryCheck = new PDI_RESTRICTION_ENTRY_CHECK();
      restrictionEntryCheck.Importing = new PDI_RESTRICTION_ENTRY_CHECK.ImportingType();
      restrictionEntryCheck.Importing.IT_CFG_KEY = features;
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) restrictionEntryCheck, false, false, false);
        featureRes = restrictionEntryCheck.Exporting.ET_CONF_VALUES;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
    }

    public static string GetItemTypeForLocalizedName(string localizedName)
    {
      switch (localizedName)
      {
        case "Mass Data Run":
          return "MDRO";
        case "Business Object":
          return "BUSINESS_OBJECT";
        case "Data Source":
          return "DATASOURCE";
        case "Query":
          return "QUERY";
        case "Form":
          return "FORMS";
        case "RSS/Atom Mashup Web Service":
          return "MASHUP_RSS_ATOM";
        case "SOAP Mashup Web Service":
          return "MASHUP_SOAP";
        case "REST Mashup Web Service":
          return "MASHUP_REST_SERVICE";
        case "HTML Mashup":
          return "MASHUP_HTML";
        case "Data Mashup":
          return "DATA_MASHUP";
        case "URL Mashup":
          return "MASHUP_URL";
        case "Mashup Port Binding":
          return "MASHUP_PORT_BINDING";
        case "Internal Communication":
          return "INT_COMMUNICATION";
        case "External Web Service Integration":
          return "EXT_WEB_SERVICE";
        case "Communication Scenario":
          return "COMMUNICATION_SCENARO";
        case "XML File Input":
          return "XML_FILE_INPUT";
        case "Web Service":
          return "WEB_SERVICE";
        case "OData Extension":
          return "XSODATA";
        case "Implementation Project Template":
          return "BC_WS_TEMPLATE";
        case "Business Configuration Object":
          return "BCO";
        case "Business Configuration View":
          return "BCVIEW";
        case "Business Configuration Set":
          return "BCSET";
        case "BAC Elements":
          return "BAC";
        case "Business Object Extension":
          return "BO_EXTENSION";
        case "Process Extension Scenario":
          return "PROCESS_EXTENSION";
        case "Node Extension Scenario":
          return "NODE_EXTENSION_SCENARIO";
        case "Embedded Component":
          return "EMBEDDED_COMPONENT";
        case "Enhancement Implementation":
          return "ENHANCEMENT_IMPLEMENTATION";
        case "Form Data Type Extension":
          return "FORM_EXTENSION";
        case "Work Center View":
          return "UI_WOC_VIEW";
        case "Modal Dialog (MD)":
          return "UI_MD";
        case "Work Center (WoC)":
          return "UI_WOC";
        case "Fact Sheet (FS)":
          return "UI_FS";
        case "Quick Activity Floorplan (QAF)":
          return "UI_QAF";
        case "Port Type Package (PTP)":
          return "UI_PTP";
        case "Object Work List(OWL)":
          return "UI_OWL";
        case "Object Value Selector (OVS)":
          return "UI_OVS";
        case "Object Instance Floorplan (OIF)":
          return "UI_OIF";
        case "Guided Activity Floorplan (GAF)":
          return "UI_GAF";
        case "Thing Type Floorplan (TT)":
          return "UI_TT";
        case "Thing Inspector Floorplan (TI)":
          return "UI_TI";
        case "Quick View Floorplan (QV)":
          return "UI_QV";
        case "Quick Create Floorplan (QC)":
          return "UI_QC";
        case "UI Switch":
          return "UI_SWITCH";
        case "Approval Step":
          return "APPROVAL_STEP";
        case "Approval Process":
          return "APPROVAL_PROCESS";
        case "Code List Data Type":
          return "CODE_DATATYPE";
        case "Analytical Report":
          return "ANALYTICAL_REPORT";
        case "Scripting Language":
          return "ABSL";
        case "Reuse Library":
          return "CUSTOM_REUSE_LIBRARY";
        case "Tax Decision Tree":
          return "TAX_DECISION_TREE";
        case "References to Customer-Specific Fields":
          return "CUSTOMER_OBJECT_REFERENCES";
        case "Key Figure":
          return "KEY_FIGURE_REFERENCE";
        case "Joined Data Source":
          return "JOIN_DS_REFERENCE";
        case "Combined Data Source":
          return "COMBINED_DS_REFERENCE";
        case "Sensitive Personal Data":
          return "PERSONAL_SENSITIVE_DATA";
        case "Personal Data Disclosure":
          return "PERSONAL_DATA_DISCLOSURE";
        default:
          return (string) null;
      }
    }

    public static string[] GetItemTypesToRestrict()
    {
      return new List<string>() { "NODE_EXTENSION_SCENARIO" }.ToArray();
    }

    public static string GetItemTypeFromXrepPath(string path)
    {
      switch (Path.GetExtension(path))
      {
        case ".mdr":
          return "MDRO";
        case ".bo":
          return "BUSINESS_OBJECT";
        case ".bott":
          return "BO_THING_TYPE";
        case ".ds":
          return "DATASOURCE";
        case ".qry":
          return "QUERY";
        case ".ftgd":
        case ".fthd":
          return "FORMS";
        case "RSS/Atom Mashup Web Service":
          return "MASHUP_RSS_ATOM";
        case "SOAP Mashup Web Service":
          return "MASHUP_SOAP";
        case "REST Mashup Web Service":
          return "MASHUP_REST_SERVICE";
        case "HTML Mashup":
          return "MASHUP_HTML";
        case "Data Mashup":
          return "DATA_MASHUP";
        case "URL Mashup":
          return "MASHUP_URL";
        case "Mashup Port Binding":
          return "MASHUP_PORT_BINDING";
        case ".pid":
          return "INT_COMMUNICATION";
        case ".wsdl":
        case ".wsid":
          return "EXT_WEB_SERVICE";
        case ".csd":
          return "COMMUNICATION_SCENARO";
        case ".webservice":
          return "WEB_SERVICE";
        case ".xodata":
          return "XSODATA";
        case ".wsauth":
          return "WEB_SERVICE";
        case ".bct":
          return "BC_WS_TEMPLATE";
        case ".bco":
          return "BCO";
        case ".bcc":
          return "BCSET";
        case ".bac":
          return "BAC";
        case ".xbo":
          return "BO_EXTENSION";
        case ".xs":
          return "PROCESS_EXTENSION";
        case ".nxs":
          return "NODE_EXTENSION_SCENARIO";
        case ".enht":
          return "ENHANCEMENT_IMPLEMENTATION";
        case ".xdp":
          return "FORM_EXTENSION";
        case ".uiwocview":
          return "UI_WOC_VIEW";
        case ".uiwoc":
          return "UI_WOC";
        case ".uiswitch":
          return "UI_SWITCH";
        case ".approval":
          return "APPROVAL_STEP";
        case ".approvalprocess":
          return "APPROVAL_PROCESS";
        case ".codelist":
          return "CODE_DATATYPE";
        case ".report":
          return "ANALYTICAL_REPORT";
        case ".absl":
          return "ABSL";
        case ".library":
          return "CUSTOM_REUSE_LIBRARY";
        case ".bcctax":
          return "TAX_DECISION_TREE";
        case ".ref":
          return "CUSTOMER_OBJECT_REFERENCES";
        case ".jds":
          return "JOIN_DS_REFERENCE";
        case ".cds":
          return "COMBINED_DS_REFERENCE";
        case ".kf":
          return "KEY_FIGURE_REFERENCE";
        case ".psd":
          return "PERSONAL_SENSITIVE_DATA";
        case ".pdd":
          return "PERSONAL_DATA_DISCLOSURE";
        case ".uicomponent":
          if (path.Contains(".EC.uicomponent"))
            return "EMBEDDED_COMPONENT";
          if (path.Contains("MD.uicomponent"))
            return "UI_MD";
          if (path.Contains("FS.uicomponent"))
            return "UI_FS";
          if (path.Contains("QA.uicomponent"))
            return "UI_QAF";
          if (path.Contains("PTP.uicomponent"))
            return "UI_PTP";
          if (path.Contains("OWL.uicomponent"))
            return "UI_OWL";
          if (path.Contains("OVS.uicomponent"))
            return "UI_OVS";
          if (path.Contains("OIF.uicomponent"))
            return "UI_OIF";
          if (path.Contains("GAF.uicomponent"))
            return "UI_GAF";
          if (path.Contains("TT.uidataobject"))
            return "UI_TT";
          if (path.Contains("TI.uicomponent"))
            return "UI_TI";
          if (path.Contains("QV.uicomponent"))
            return "UI_QV";
          if (path.Contains("QC.uicomponent"))
            return "UI_QC";
          return (string) null;
        default:
          return (string) null;
      }
    }
  }
}
