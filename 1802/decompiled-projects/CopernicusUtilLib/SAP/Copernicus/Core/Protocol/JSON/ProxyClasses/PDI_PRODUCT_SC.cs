﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PRODUCT_SC
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PRODUCT_SC
  {
    [DataMember]
    public string COMPONENT;
    [DataMember]
    public string XREP_SOLUTION;
    [DataMember]
    public string XREP_BC_SOLUTION;
    [DataMember]
    public string XREP_BC_GEN_SOLUTION;
    [DataMember]
    public string XREP_ROOT_PATH;
    [DataMember]
    public string XREP_BC_ROOT_PAT;
    [DataMember]
    public string MDRS_NAMESPACE;
    [DataMember]
    public string ABAP_NAMESPACE;
    [DataMember]
    public string DEV_LICENSE;
    [DataMember]
    public string REP_LICENSE;
    [DataMember]
    public string STRUCT_PACKAGE;
    [DataMember]
    public string DEV_PACKAGE;
    [DataMember]
    public string SWITCH_ID;
    [DataMember]
    public string DEVCLASS;
    [DataMember]
    public string DEFAULT_PC;
    [DataMember]
    public string BC_PACKAGE;
    [DataMember]
    public string BAC_HEADER;
    [DataMember]
    public string BAC_HEADER_TYPE;
    [DataMember]
    public string BAC_HEADER_P;
    [DataMember]
    public string BAC_HEADER_PAR_T;
    [DataMember]
    public string PPMS_NR;
    [DataMember]
    public string DEFAULT_DU;
    [DataMember]
    public string COMPILER_VERSION;
    [DataMember]
    public string EXTERNAL_NAMESPACE_NAME;
    [DataMember]
    public string EXTERNAL_NAMESPACE_URI;
  }
}
