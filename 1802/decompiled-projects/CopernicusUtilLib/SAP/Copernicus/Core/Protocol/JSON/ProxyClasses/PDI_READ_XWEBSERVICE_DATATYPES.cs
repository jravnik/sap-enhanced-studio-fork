﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_READ_XWEBSERVICE_DATATYPES
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_READ_XWEBSERVICE_DATATYPES : AbstractRemoteFunction<PDI_READ_XWEBSERVICE_DATATYPES.ImportingType, PDI_READ_XWEBSERVICE_DATATYPES.ExportingType, PDI_READ_XWEBSERVICE_DATATYPES.ChangingType, PDI_READ_XWEBSERVICE_DATATYPES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0520391ED386A4CA0AAAB49CF7";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION_NAMESPACE;
      [DataMember]
      public string IV_XWEBSERVICE_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public WEBSERVICE_EXTENSION_DATATYPES ES_WEBSERVICE_EXTENSION;
      [DataMember]
      public string EV_INPUT_DATATYPE_NAME;
      [DataMember]
      public string EV_INPUT_DATATYPE_NAMESPACE;
      [DataMember]
      public string EV_OUTPUT_DATATYPE_NAME;
      [DataMember]
      public string EV_OUTPUT_DATATYPE_NAMESPACE;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
