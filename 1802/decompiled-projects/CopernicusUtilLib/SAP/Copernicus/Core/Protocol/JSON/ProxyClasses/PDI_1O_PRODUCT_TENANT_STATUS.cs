﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_PRODUCT_TENANT_STATUS
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_PRODUCT_TENANT_STATUS
  {
    [DataMember]
    public string PRODUCT_NAMESPACE;
    [DataMember]
    public string STATUS;
    [DataMember]
    public string TENANT_URL;
    [DataMember]
    public string CLIENT;
    [DataMember]
    public string TENANT_ROLE_CODE;
    [DataMember]
    public string VERSION;
    [DataMember]
    public string LAST_CHANGED_DATE_TIME;
    [DataMember]
    public string INFORMATION_TEXT;
    [DataMember]
    public string TENANT_RUNLEVEL;
    [DataMember]
    public string FAILED_JOB_STATUS;
    [DataMember]
    public string BACKUP_TENANT;
  }
}
