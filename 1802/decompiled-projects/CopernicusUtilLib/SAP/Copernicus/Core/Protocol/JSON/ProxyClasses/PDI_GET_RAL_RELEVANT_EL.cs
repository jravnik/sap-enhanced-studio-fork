﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_RAL_RELEVANT_EL
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_GET_RAL_RELEVANT_EL : AbstractRemoteFunction<PDI_GET_RAL_RELEVANT_EL.ImportingType, PDI_GET_RAL_RELEVANT_EL.ExportingType, PDI_GET_RAL_RELEVANT_EL.ChangingType, PDI_GET_RAL_RELEVANT_EL.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F51EE7B1A230986142D4F1";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public IV_BO_NODE_KEY[] IV_BO_NODE_KEY;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public ET_NODE_ELEMENT[] ET_NODE_ELEMENT;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
