﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_S_EXT_NODE
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_S_EXT_NODE
  {
    [DataMember]
    public string BO_ESR_NAMESPACE;
    [DataMember]
    public string BO_ESR_NAME;
    [DataMember]
    public string MAX_BO_PRX_NAME;
    [DataMember]
    public string ND_NAME;
    [DataMember]
    public string PRX_BO_NAME;
    [DataMember]
    public string PRX_ND_NAME;
    [DataMember]
    public string NODE_ID;
    [DataMember]
    public string ND_UUID;
    [DataMember]
    public string PARENT_NODE_NAME;
    [DataMember]
    public string ORDINAL_NUMBER;
    [DataMember]
    public string IS_EXTENSIBLE;
    [DataMember]
    public string HI_LEVEL;
    [DataMember]
    public string PSM_RELEASED;
    [DataMember]
    public string OFF_ENABLED;
  }
}
