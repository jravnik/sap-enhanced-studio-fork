﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_COPY_SOLUTION
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_COPY_SOLUTION : AbstractRemoteFunction<PDI_1O_COPY_SOLUTION.ImportingType, PDI_1O_COPY_SOLUTION.ExportingType, PDI_1O_COPY_SOLUTION.ChangingType, PDI_1O_COPY_SOLUTION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01138A1ED0B0D13B0A92E383D1";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
      [DataMember]
      public string IV_SESSION_ID;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_NEW_PRODUCT_NAME;
      [DataMember]
      public string EV_NEW_PATCH_VERSION;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
