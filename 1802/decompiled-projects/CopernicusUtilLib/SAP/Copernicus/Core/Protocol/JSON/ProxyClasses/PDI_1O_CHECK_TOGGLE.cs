﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_CHECK_TOGGLE
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_CHECK_TOGGLE : AbstractRemoteFunction<PDI_1O_CHECK_TOGGLE.ImportingType, PDI_1O_CHECK_TOGGLE.ExportingType, PDI_1O_CHECK_TOGGLE.ChangingType, PDI_1O_CHECK_TOGGLE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F51EE5B6DCC85B883ED52B";
      }
    }

    [DataContract]
    public class ImportingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_CHK_BATCH_ENABLED;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
