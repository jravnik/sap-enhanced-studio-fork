﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.DataDisclosureHandler
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class DataDisclosureHandler : JSONHandler
  {
    public bool getDisclosureProfiles(out ET_PROFILE[] profileDetails)
    {
      PDI_GDPR_DISCLOSURE_PROFILES disclosureProfiles = new PDI_GDPR_DISCLOSURE_PROFILES();
      profileDetails = (ET_PROFILE[]) null;
      try
      {
        Client.getInstance().getJSONClient(false).callFunctionModule((SAPFunctionModule) disclosureProfiles, false, false, false);
        if (disclosureProfiles.Exporting == null)
          return false;
        profileDetails = disclosureProfiles.Exporting.ET_PROFILE;
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public bool getDisclosureUIs(string solName, string profileName, out ET_DISCLOSURE_UIS[] disclosureUIs)
    {
      PDI_GDPR_DISCLOSURE_UIS gdprDisclosureUis = new PDI_GDPR_DISCLOSURE_UIS();
      disclosureUIs = (ET_DISCLOSURE_UIS[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        gdprDisclosureUis.Importing = new PDI_GDPR_DISCLOSURE_UIS.ImportingType();
        gdprDisclosureUis.Importing.IV_SOLUTION = solName;
        gdprDisclosureUis.Importing.IV_PROFILE_NAME = profileName;
        jsonClient.callFunctionModule((SAPFunctionModule) gdprDisclosureUis, false, false, false);
        if (gdprDisclosureUis.Exporting != null)
          disclosureUIs = gdprDisclosureUis.Exporting.ET_DISCLOSURE_UIS;
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }
  }
}
