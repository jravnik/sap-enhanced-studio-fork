﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_WSDL_PRX_CHILD_TT
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_WSDL_PRX_CHILD_TT
  {
    [DataMember]
    public string UUID;
    [DataMember]
    public string ES_NAME;
    [DataMember]
    public string PRX_COMPONENT_NAME;
    [DataMember]
    public string PRX_COMPONENT_TYPE;
    [DataMember]
    public string XSD_TYPE;
    [DataMember]
    public string MDRS_DATA_TYPE;
    [DataMember]
    public string MDRS_DATA_TYPE_NAME;
  }
}
