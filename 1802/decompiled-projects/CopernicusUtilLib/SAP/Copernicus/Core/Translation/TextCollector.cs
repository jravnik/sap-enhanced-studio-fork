﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Translation.TextCollector
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Translation
{
  public class TextCollector
  {
    private IDictionary<string, string> textfileAttributes = (IDictionary<string, string>) new Dictionary<string, string>();
    public const string AddTextContractName = "TextSink";

    public void AddText(string textID, TextType textType, string value)
    {
      string key = "TRANSEN_" + new TextIDCreator().getXrepTextType(textType) + "_" + textID;
      if (this.textfileAttributes.ContainsKey(key))
        this.textfileAttributes.Remove(key);
      this.textfileAttributes.Add(key, value);
    }

    public IDictionary<string, string> GetAllTextAttributes()
    {
      return this.textfileAttributes;
    }
  }
}
