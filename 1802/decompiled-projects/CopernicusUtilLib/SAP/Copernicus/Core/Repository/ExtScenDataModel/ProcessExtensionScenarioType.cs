﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ExtScenDataModel.ProcessExtensionScenarioType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.ExtScenDataModel
{
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/ProcessExtensionScenarioDefinition")]
  [XmlRoot("ProcessExtensionScenario", IsNullable = false, Namespace = "http://sap.com/ByD/PDI/ProcessExtensionScenarioDefinition")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [Serializable]
  public class ProcessExtensionScenarioType
  {
    private string nameField;
    private string boNameSpaceField;
    private string boNameField;
    private string boNodeNameField;
    private ExtensionScenarioType[] extensionScenarioListField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string Name
    {
      get
      {
        return this.nameField;
      }
      set
      {
        this.nameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string BoNameSpace
    {
      get
      {
        return this.boNameSpaceField;
      }
      set
      {
        this.boNameSpaceField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string BoName
    {
      get
      {
        return this.boNameField;
      }
      set
      {
        this.boNameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string BoNodeName
    {
      get
      {
        return this.boNodeNameField;
      }
      set
      {
        this.boNodeNameField = value;
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified)]
    [XmlArrayItem("ExtensionScenario", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
    public ExtensionScenarioType[] ExtensionScenarioList
    {
      get
      {
        return this.extensionScenarioListField;
      }
      set
      {
        this.extensionScenarioListField = value;
      }
    }
  }
}
