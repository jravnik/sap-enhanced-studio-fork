﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ActionComparer
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Repository
{
  internal class ActionComparer : IComparer<PDI_EXT_S_EXT_ACTION>
  {
    public int Compare(PDI_EXT_S_EXT_ACTION x, PDI_EXT_S_EXT_ACTION y)
    {
      int num1 = x.PRX_BO_NAME.CompareTo(y.PRX_BO_NAME);
      if (num1 != 0)
        return num1;
      int num2 = x.PRX_NODE_NAME.CompareTo(y.PRX_NODE_NAME);
      if (num2 != 0)
        return num2;
      return x.ESR_ACTION_NAME.CompareTo(y.ESR_ACTION_NAME);
    }
  }
}
