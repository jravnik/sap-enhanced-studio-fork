﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ExtScenDataList.ExtensionScenarioType
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.ExtScenDataList
{
  [XmlType(Namespace = "http://sap.com/ByD/PDI/ExtensionScenarioList")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [Serializable]
  public class ExtensionScenarioType
  {
    private string scenario_nameField;
    private string scenario_descriptionField;
    private string service_interface_typeField;
    private FlowType[] bo_connectionsField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string scenario_name
    {
      get
      {
        return this.scenario_nameField;
      }
      set
      {
        this.scenario_nameField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string scenario_description
    {
      get
      {
        return this.scenario_descriptionField;
      }
      set
      {
        this.scenario_descriptionField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string service_interface_type
    {
      get
      {
        return this.service_interface_typeField;
      }
      set
      {
        this.service_interface_typeField = value;
      }
    }

    [XmlArray(Form = XmlSchemaForm.Unqualified)]
    [XmlArrayItem("Flow", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
    public FlowType[] bo_connections
    {
      get
      {
        return this.bo_connectionsField;
      }
      set
      {
        this.bo_connectionsField = value;
      }
    }
  }
}
