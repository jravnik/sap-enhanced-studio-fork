﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeKey
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Repository
{
  internal class NodeKey
  {
    public readonly string boProxyName;
    public readonly string nodeProxyName;

    public NodeKey(string boProxyName, string nodeProxyName)
    {
      this.boProxyName = boProxyName;
      this.nodeProxyName = nodeProxyName;
    }

    public class EqualityComparer : IEqualityComparer<NodeKey>
    {
      public bool Equals(NodeKey x, NodeKey y)
      {
        if (x.boProxyName == y.boProxyName)
          return x.nodeProxyName == y.nodeProxyName;
        return false;
      }

      public int GetHashCode(NodeKey obj)
      {
        return obj.boProxyName.GetHashCode() ^ obj.nodeProxyName.GetHashCode();
      }
    }
  }
}
