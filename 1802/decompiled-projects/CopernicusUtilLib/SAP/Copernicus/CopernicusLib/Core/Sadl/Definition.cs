﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.Definition
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [DebuggerStepThrough]
  [XmlRoot(ElementName = "definition", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [Serializable]
  public class Definition
  {
    private SAP.Copernicus.CopernicusLib.Core.Sadl.Conversions[] conversionsField;
    private Callback[] callbacksField;
    private SAP.Copernicus.CopernicusLib.Core.Sadl.Modes[] modesField;
    private SAP.Copernicus.CopernicusLib.Core.Sadl.DataSource[] dataSourceField;
    private SAP.Copernicus.CopernicusLib.Core.Sadl.ResultSet[] resultSetField;
    private SAP.Copernicus.CopernicusLib.Core.Sadl.Query[] queryField;
    private string dateField;
    private string timeField;
    private string uuidField;

    [XmlElement("conversions")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.Conversions[] Conversions
    {
      get
      {
        return this.conversionsField;
      }
      set
      {
        this.conversionsField = value;
      }
    }

    [XmlElement("modes")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.Modes[] Modes
    {
      get
      {
        return this.modesField;
      }
      set
      {
        this.modesField = value;
      }
    }

    [XmlElement("callback")]
    public Callback[] Callbacks
    {
      get
      {
        return this.callbacksField;
      }
      set
      {
        this.callbacksField = value;
      }
    }

    [XmlElement("dataSource")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.DataSource[] DataSource
    {
      get
      {
        return this.dataSourceField;
      }
      set
      {
        this.dataSourceField = value;
      }
    }

    [XmlElement("resultSet")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.ResultSet[] ResultSet
    {
      get
      {
        return this.resultSetField;
      }
      set
      {
        this.resultSetField = value;
      }
    }

    [XmlElement("query")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.Query[] Query
    {
      get
      {
        return this.queryField;
      }
      set
      {
        this.queryField = value;
      }
    }

    [XmlAttribute]
    public string date
    {
      get
      {
        return this.dateField;
      }
      set
      {
        this.dateField = value;
      }
    }

    [XmlAttribute]
    public string time
    {
      get
      {
        return this.timeField;
      }
      set
      {
        this.timeField = value;
      }
    }

    [XmlAttribute]
    public string uuid
    {
      get
      {
        return this.uuidField;
      }
      set
      {
        this.uuidField = value;
      }
    }
  }
}
