﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.Structure
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [DebuggerStepThrough]
  [XmlRoot(ElementName = "structure", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://sap.com/sap.nw.f.sadl")]
  [Serializable]
  public class Structure
  {
    public const string MAX_EDIT_MODE_READ_ONLY = "RO";
    public const string MAX_EDIT_MODE_EDIT = "ED";
    public const string MAX_EDIT_MODE_EDIT_EXCL = "EX";
    private StructureAttribute[] attributeField;
    private Structure[] subStructureField;
    private string nameField;
    private string bindingField;
    private string dataSourceField;
    private string maxEditModeField;
    private string aclRelavant;
    private string mdrsNameField;
    private string mdrsBindingField;

    [XmlElement(ElementName = "attribute", Namespace = "http://sap.com/sap.nw.f.sadl")]
    public StructureAttribute[] Attribute
    {
      get
      {
        return this.attributeField;
      }
      set
      {
        this.attributeField = value;
      }
    }

    [XmlElement(ElementName = "structure", Namespace = "http://sap.com/sap.nw.f.sadl")]
    public Structure[] subStructure
    {
      get
      {
        return this.subStructureField;
      }
      set
      {
        this.subStructureField = value;
      }
    }

    [XmlAttribute]
    public string name
    {
      get
      {
        return this.nameField;
      }
      set
      {
        this.nameField = value;
      }
    }

    [XmlAttribute]
    public string binding
    {
      get
      {
        return this.bindingField;
      }
      set
      {
        this.bindingField = value;
      }
    }

    [XmlAttribute]
    public string dataSource
    {
      get
      {
        return this.dataSourceField;
      }
      set
      {
        this.dataSourceField = value;
      }
    }

    [XmlAttribute]
    public string maxEditMode
    {
      get
      {
        return this.maxEditModeField;
      }
      set
      {
        this.maxEditModeField = value;
      }
    }

    [XmlAttribute("aclRelevant")]
    public string aclRelevant
    {
      get
      {
        return this.aclRelavant;
      }
      set
      {
        this.aclRelavant = value;
      }
    }

    [XmlAttribute(Namespace = "http://sap.com/xi/AP/PDI")]
    public string mdrsName
    {
      get
      {
        return this.mdrsNameField;
      }
      set
      {
        this.mdrsNameField = value;
      }
    }

    [XmlAttribute(Namespace = "http://sap.com/xi/AP/PDI")]
    public string mdrsBinding
    {
      get
      {
        return this.mdrsBindingField;
      }
      set
      {
        this.mdrsBindingField = value;
      }
    }
  }
}
