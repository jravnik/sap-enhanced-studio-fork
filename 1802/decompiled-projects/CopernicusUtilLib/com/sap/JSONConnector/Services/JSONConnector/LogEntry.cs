﻿// Decompiled with JetBrains decompiler
// Type: com.sap.JSONConnector.Services.JSONConnector.LogEntry
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace com.sap.JSONConnector.Services.JSONConnector
{
  [DataContract]
  public class LogEntry
  {
    [DataMember(Name = "ID")]
    public string Id { get; set; }

    [DataMember(Name = "TYPE")]
    public string Type { get; set; }

    [DataMember(Name = "TIMESTAMPL")]
    public string Timestamp { get; set; }

    [DataMember(Name = "DESCRIPTION")]
    public string Description { get; set; }
  }
}
