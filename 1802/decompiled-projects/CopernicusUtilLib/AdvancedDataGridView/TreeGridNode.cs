﻿// Decompiled with JetBrains decompiler
// Type: AdvancedDataGridView.TreeGridNode
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

namespace AdvancedDataGridView
{
  [ToolboxItem(false)]
  [DesignTimeVisible(false)]
  public class TreeGridNode : DataGridViewRow
  {
    private Random rndSeed = new Random();
    public int UniqueValue = -1;
    internal TreeGridView _grid;
    internal TreeGridNode _parent;
    internal TreeGridNodeCollection _owner;
    internal bool IsExpanded;
    internal bool IsRoot;
    internal bool _isSited;
    internal bool _isFirstSibling;
    internal bool _isLastSibling;
    internal Image _image;
    internal int _imageIndex;
    private TreeGridCell _treeCell;
    private TreeGridNodeCollection childrenNodes;
    private int _index;
    private int _level;
    private bool childCellsCreated;
    private ISite site;
    private EventHandler disposed;

    internal TreeGridNode(TreeGridView owner)
      : this()
    {
      this._grid = owner;
      this.IsExpanded = true;
    }

    public TreeGridNode()
    {
      this._index = -1;
      this._level = -1;
      this.IsExpanded = false;
      this.UniqueValue = this.rndSeed.Next();
      this._isSited = false;
      this._isFirstSibling = false;
      this._isLastSibling = false;
      this._imageIndex = -1;
    }

    public override object Clone()
    {
      TreeGridNode treeGridNode = (TreeGridNode) base.Clone();
      treeGridNode.UniqueValue = -1;
      treeGridNode._level = this._level;
      treeGridNode._grid = this._grid;
      treeGridNode._parent = this.Parent;
      treeGridNode._imageIndex = this._imageIndex;
      if (treeGridNode._imageIndex == -1)
        treeGridNode.Image = this.Image;
      treeGridNode.IsExpanded = this.IsExpanded;
      return (object) treeGridNode;
    }

    protected internal virtual void UnSited()
    {
      foreach (DataGridViewCell cell in (BaseCollection) this.Cells)
      {
        TreeGridCell treeGridCell = cell as TreeGridCell;
        if (treeGridCell != null)
          treeGridCell.UnSited();
      }
      this._isSited = false;
    }

    protected internal virtual void Sited()
    {
      this._isSited = true;
      this.childCellsCreated = true;
      foreach (DataGridViewCell cell in (BaseCollection) this.Cells)
      {
        TreeGridCell treeGridCell = cell as TreeGridCell;
        if (treeGridCell != null)
          treeGridCell.Sited();
      }
    }

    [Browsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [Description("Represents the index of this row in the Grid. Advanced usage.")]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public int RowIndex
    {
      get
      {
        return base.Index;
      }
    }

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public new int Index
    {
      get
      {
        if (this._index == -1)
          this._index = this._owner.IndexOf(this);
        return this._index;
      }
      set
      {
        this._index = value;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Never)]
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public ImageList ImageList
    {
      get
      {
        if (this._grid != null)
          return this._grid.ImageList;
        return (ImageList) null;
      }
    }

    private bool ShouldSerializeImageIndex()
    {
      if (this._imageIndex != -1)
        return this._image == null;
      return false;
    }

    [Editor("System.Windows.Forms.Design.ImageIndexEditor", typeof (UITypeEditor))]
    [DefaultValue(-1)]
    [TypeConverter(typeof (ImageIndexConverter))]
    [Description("...")]
    [Category("Appearance")]
    public int ImageIndex
    {
      get
      {
        return this._imageIndex;
      }
      set
      {
        this._imageIndex = value;
        if (this._imageIndex != -1)
          this._image = (Image) null;
        if (!this._isSited)
          return;
        this._treeCell.UpdateStyle();
        if (!this.Displayed)
          return;
        this._grid.InvalidateRow(this.RowIndex);
      }
    }

    private bool ShouldSerializeImage()
    {
      if (this._imageIndex == -1)
        return this._image != null;
      return false;
    }

    public TreeGridView Grid
    {
      get
      {
        return this._grid;
      }
      set
      {
        this._grid = value;
      }
    }

    public Image Image
    {
      get
      {
        if (this._image != null || this._imageIndex == -1)
          return this._image;
        if (this.ImageList != null && this._imageIndex < this.ImageList.Images.Count)
          return this.ImageList.Images[this._imageIndex];
        return (Image) null;
      }
      set
      {
        this._image = value;
        if (this._image != null)
          this._imageIndex = -1;
        if (!this._isSited)
          return;
        this._treeCell.UpdateStyle();
        if (!this.Displayed)
          return;
        this._grid.InvalidateRow(this.RowIndex);
      }
    }

    protected override DataGridViewCellCollection CreateCellsInstance()
    {
      DataGridViewCellCollection cellsInstance = base.CreateCellsInstance();
      cellsInstance.CollectionChanged += new CollectionChangeEventHandler(this.cells_CollectionChanged);
      return cellsInstance;
    }

    private void cells_CollectionChanged(object sender, CollectionChangeEventArgs e)
    {
      if (this._treeCell != null || e.Action != CollectionChangeAction.Add && e.Action != CollectionChangeAction.Refresh)
        return;
      TreeGridCell treeGridCell = (TreeGridCell) null;
      if (e.Element == null)
      {
        foreach (DataGridViewCell cell in (BaseCollection) base.Cells)
        {
          if (cell.GetType().IsAssignableFrom(typeof (TreeGridCell)))
          {
            treeGridCell = (TreeGridCell) cell;
            break;
          }
        }
      }
      else
        treeGridCell = e.Element as TreeGridCell;
      if (treeGridCell == null)
        return;
      this._treeCell = treeGridCell;
    }

    [Editor(typeof (CollectionEditor), typeof (UITypeEditor))]
    [Category("Data")]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [Description("The collection of root nodes in the treelist.")]
    public TreeGridNodeCollection ChildNodes
    {
      get
      {
        if (this.childrenNodes == null)
          this.childrenNodes = new TreeGridNodeCollection(this);
        return this.childrenNodes;
      }
      set
      {
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    public new DataGridViewCellCollection Cells
    {
      get
      {
        if (!this.childCellsCreated && this.DataGridView == null)
        {
          if (this._grid == null)
            return (DataGridViewCellCollection) null;
          this.CreateCells((DataGridView) this._grid);
          this.childCellsCreated = true;
        }
        return base.Cells;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    public int Level
    {
      get
      {
        if (this._level == -1)
        {
          int num = 0;
          for (TreeGridNode parent = this.Parent; parent != null; parent = parent.Parent)
            ++num;
          this._level = num;
        }
        return this._level;
      }
    }

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public TreeGridNode Parent
    {
      get
      {
        return this._parent;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    public virtual bool HasChildren
    {
      get
      {
        if (this.childrenNodes != null)
          return this.ChildNodes.Count != 0;
        return false;
      }
    }

    [Browsable(false)]
    public bool IsSited
    {
      get
      {
        return this._isSited;
      }
    }

    [Browsable(false)]
    public bool IsFirstSibling
    {
      get
      {
        return this.Index == 0;
      }
    }

    [Browsable(false)]
    public bool IsLastSibling
    {
      get
      {
        TreeGridNode parent = this.Parent;
        if (parent != null && parent.HasChildren)
          return this.Index == parent.ChildNodes.Count - 1;
        return true;
      }
    }

    public virtual bool Collapse()
    {
      return this._grid.CollapseNode(this);
    }

    public virtual bool Expand()
    {
      if (this._grid != null)
        return this._grid.ExpandNode(this);
      this.IsExpanded = true;
      return true;
    }

    protected internal virtual bool InsertChildNode(int index, TreeGridNode node)
    {
      node._parent = this;
      node._grid = this._grid;
      if (this._grid != null)
        this.UpdateChildNodes(node);
      if ((this._isSited || this.IsRoot) && this.IsExpanded)
        this._grid.SiteNode(node);
      return true;
    }

    protected internal virtual bool InsertChildNodes(int index, params TreeGridNode[] nodes)
    {
      foreach (TreeGridNode node in nodes)
        this.InsertChildNode(index, node);
      return true;
    }

    protected internal virtual bool AddChildNode(TreeGridNode node)
    {
      node._parent = this;
      node._grid = this._grid;
      if (this._grid != null)
        this.UpdateChildNodes(node);
      if ((this._isSited || this.IsRoot) && (this.IsExpanded && !node._isSited))
        this._grid.SiteNode(node);
      return true;
    }

    protected internal virtual bool AddChildNodes(params TreeGridNode[] nodes)
    {
      foreach (TreeGridNode node in nodes)
        this.AddChildNode(node);
      return true;
    }

    protected internal virtual bool RemoveChildNode(TreeGridNode node)
    {
      if ((this.IsRoot || this._isSited) && this.IsExpanded)
        this._grid.UnSiteNode(node);
      node._grid = (TreeGridView) null;
      node._parent = (TreeGridNode) null;
      return true;
    }

    protected internal virtual bool ClearNodes()
    {
      if (this.HasChildren)
      {
        for (int index = this.ChildNodes.Count - 1; index >= 0; --index)
          this.ChildNodes.RemoveAt(index);
      }
      return true;
    }

    [Browsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public event EventHandler Disposed
    {
      add
      {
        this.disposed += value;
      }
      remove
      {
        this.disposed -= value;
      }
    }

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public ISite Site
    {
      get
      {
        return this.site;
      }
      set
      {
        this.site = value;
      }
    }

    private void UpdateChildNodes(TreeGridNode node)
    {
      if (!node.HasChildren)
        return;
      foreach (TreeGridNode childNode in node.ChildNodes)
      {
        childNode._grid = node._grid;
        this.UpdateChildNodes(childNode);
      }
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder(36);
      stringBuilder.Append("TreeGridNode { Index=");
      stringBuilder.Append(this.RowIndex.ToString((IFormatProvider) CultureInfo.CurrentCulture));
      stringBuilder.Append(" }");
      return stringBuilder.ToString();
    }
  }
}
