﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("CopernicusUtilLib")]
[assembly: AssemblyDescription("")]
[assembly: CLSCompliant(false)]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SAP AG")]
[assembly: AssemblyProduct("SAP® Business ByDesign™")]
[assembly: AssemblyCopyright("© 2011 SAP AG. All rights reserved.")]
[assembly: AssemblyTrademark("SAP and Business ByDesign are trademark(s) or registered trademark(s) of SAP AG in Germany and in several other countries.")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: ComVisible(false)]
[assembly: Guid("3945fdb1-6f89-4aa6-8d78-54a6cf89af15")]
[assembly: AssemblyFileVersion("142.0.3319.0231")]
[assembly: InternalsVisibleTo("CopernicusTest")]
[assembly: AssemblyVersion("142.0.3319.231")]
