﻿// Decompiled with JetBrains decompiler
// Type: WindowsInstaller.Installer
// Assembly: CopernicusUtilLib, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: 1F543F7D-C6F3-4A0D-9B52-4A3B2D15C8B1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace WindowsInstaller
{
  [Guid("000C1090-0000-0000-C000-000000000046")]
  [TypeIdentifier]
  [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
  [CompilerGenerated]
  [ComImport]
  public interface Installer
  {
    [DispId(4)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.Interface)]
    Database OpenDatabase([MarshalAs(UnmanagedType.BStr), In] string DatabasePath, [MarshalAs(UnmanagedType.Struct), In] object OpenMode);
  }
}
