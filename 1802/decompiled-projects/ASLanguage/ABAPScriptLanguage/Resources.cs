﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Resources
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus.ABAPScriptLanguage
{
  [CompilerGenerated]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) SAP.Copernicus.ABAPScriptLanguage.Resources.resourceMan, (object) null))
          SAP.Copernicus.ABAPScriptLanguage.Resources.resourceMan = new ResourceManager("SAP.Copernicus.ABAPScriptLanguage.Resources", typeof (SAP.Copernicus.ABAPScriptLanguage.Resources).Assembly);
        return SAP.Copernicus.ABAPScriptLanguage.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture;
      }
      set
      {
        SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture = value;
      }
    }

    internal static string ClassificationFormatNameComment
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (ClassificationFormatNameComment), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string ClassificationFormatNameIdentifier
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (ClassificationFormatNameIdentifier), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string ClassificationFormatNameKeyword
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (ClassificationFormatNameKeyword), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string ClassificationFormatNameNumericLiteral
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (ClassificationFormatNameNumericLiteral), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string ClassificationFormatNameStringLiteral
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (ClassificationFormatNameStringLiteral), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string ClassificationFormatNameText
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (ClassificationFormatNameText), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerAttachErrorPopup
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerAttachErrorPopup), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerBreakpointErrorPopup
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerBreakpointErrorPopup), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerConnectionLostOutputWindow
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerConnectionLostOutputWindow), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerConnectionLostPopup
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerConnectionLostPopup), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerDebuggeeResetOutputWindow
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerDebuggeeResetOutputWindow), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerDebuggeeResetPopup
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerDebuggeeResetPopup), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerDecisionAlwaySaveRefreshActivate
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerDecisionAlwaySaveRefreshActivate), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerListenerConnectionLostOuputWindow
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerListenerConnectionLostOuputWindow), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerListenerConnectionLostPopup
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerListenerConnectionLostPopup), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerSaveRefreshActivate
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerSaveRefreshActivate), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerSourceDoesNotFitToRuntimePopup
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerSourceDoesNotFitToRuntimePopup), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerStartedForUserOutputWindow
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerStartedForUserOutputWindow), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerStartedForUserStatusbar
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerStartedForUserStatusbar), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerStartErrorPopup
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerStartErrorPopup), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerStoppedOutputWindow
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerStoppedOutputWindow), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerStoppedStatusbar
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerStoppedStatusbar), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerTracingConnectionLostOutputWindow
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerTracingConnectionLostOutputWindow), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DebuggerUnknownBreakpointErrorTooltip
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DebuggerUnknownBreakpointErrorTooltip), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisSolutionInvalidCharacters
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisSolutionInvalidCharacters), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisSolutionLength
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisSolutionLength), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisSolutionNoAccessDetail
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisSolutionNoAccessDetail), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisSolutionNoAccessHeader
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisSolutionNoAccessHeader), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisSolutionStartEnd
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisSolutionStartEnd), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisSolutionSwitchDetail
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisSolutionSwitchDetail), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisSolutionSwitchHeader
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisSolutionSwitchHeader), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisTooltipNoExistOrActivation
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisTooltipNoExistOrActivation), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisTooltipPositionChanged
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisTooltipPositionChanged), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisTooltipPositionNotDetermined
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisTooltipPositionNotDetermined), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisTooltipPositionUpToDate
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisTooltipPositionUpToDate), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string DumpAnalysisUserInvalidCharacters
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (DumpAnalysisUserInvalidCharacters), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }

    internal static string HELPID_BDS_ABSL
    {
      get
      {
        return SAP.Copernicus.ABAPScriptLanguage.Resources.ResourceManager.GetString(nameof (HELPID_BDS_ABSL), SAP.Copernicus.ABAPScriptLanguage.Resources.resourceCulture);
      }
    }
  }
}
