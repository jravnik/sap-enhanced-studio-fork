﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Formatter.StatementContextInfo
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System.Collections.Generic;

namespace SAP.Copernicus.ABAPScriptLanguage.Formatter
{
  public class StatementContextInfo
  {
    private List<int> marks = new List<int>();
    private int indentOffset;

    public int IndentOffset
    {
      get
      {
        return this.indentOffset;
      }
      set
      {
        this.indentOffset = value;
        this.IndentOffsetSpecified = true;
      }
    }

    public bool IndentOffsetSpecified { get; private set; }

    public int BracesLeftOpen { get; private set; }

    public void OpeningBraceFound()
    {
      ++this.BracesLeftOpen;
    }

    public void ClosingBraceFound()
    {
      --this.BracesLeftOpen;
    }

    public void AddBraceMark()
    {
      this.marks.Add(this.BracesLeftOpen);
    }

    public void RemoveBraceMark()
    {
      if (!this.marks.Remove(this.BracesLeftOpen))
        throw new SourceFormatterCriticalException("No brace mark for \"" + (object) this.BracesLeftOpen + "\" found");
    }

    public bool NextClosingBraceIsUnexpected()
    {
      if (this.BracesLeftOpen > 0)
        return !this.marks.Contains(this.BracesLeftOpen);
      return false;
    }

    public void Close()
    {
      if (this.BracesLeftOpen > 0)
        throw new SourceFormatterCriticalException("Could not match round braces, " + (object) this.BracesLeftOpen + " closing round brace(s) missing");
    }
  }
}
