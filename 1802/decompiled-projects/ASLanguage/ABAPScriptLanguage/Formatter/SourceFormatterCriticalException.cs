﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Formatter.SourceFormatterCriticalException
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System;

namespace SAP.Copernicus.ABAPScriptLanguage.Formatter
{
  public class SourceFormatterCriticalException : Exception
  {
    private const string ErrorMessage = "An internal error occured in the ByD source code formatter.";

    public SourceFormatterCriticalException(string Message)
      : base("An internal error occured in the ByD source code formatter.\r\n\r\n" + Message)
    {
    }
  }
}
