﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Env.TokenStreamHelper
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Antlr.Runtime;
using SAP.Copernicus.ABAPScriptLanguage.IntelliSense;
using System.Collections.Generic;

namespace SAP.Copernicus.ABAPScriptLanguage.Env
{
  public class TokenStreamHelper
  {
    public static int GetLastTokenTypePos(ITokenStream tokenStream, int tokenTypeToCheck, int startIndex)
    {
      int num = -1;
      for (int i = startIndex; i >= 0; --i)
      {
        if (tokenStream.Get(i).Type == tokenTypeToCheck)
        {
          num = i;
          break;
        }
      }
      return num;
    }

    public static bool IsTokenWithin(ITokenStream tokenStream, IToken matchedToken, int tokenTypeToCheck)
    {
      bool flag1 = false;
      int startIndex = matchedToken.Type == -1 ? tokenStream.Count - 1 : matchedToken.TokenIndex;
      int lastTokenTypePos1 = TokenStreamHelper.GetLastTokenTypePos(tokenStream, 35, startIndex);
      int lastTokenTypePos2 = TokenStreamHelper.GetLastTokenTypePos(tokenStream, tokenTypeToCheck, startIndex);
      bool flag2 = lastTokenTypePos1 >= 0;
      if (lastTokenTypePos2 >= 0 && (!flag2 || lastTokenTypePos1 < lastTokenTypePos2))
        flag1 = true;
      return flag1;
    }

    public static IToken GetLastTokenBeforeCursor(string source, CursorPosition cursor)
    {
      IToken token1 = (IToken) null;
      ByDScriptingLanguageLexer dscriptingLanguageLexer = new ByDScriptingLanguageLexer((ICharStream) new ANTLRStringStream(source));
      List<IToken> tokenList = new List<IToken>();
      IToken token2;
      while ((token2 = dscriptingLanguageLexer.NextToken()).Type != -1)
      {
        if (token2.Channel != 99)
          tokenList.Add(token2);
      }
      for (int index = 0; index < tokenList.Count; ++index)
      {
        IToken token3 = tokenList[index];
        IToken token4 = index + 1 == tokenList.Count ? (IToken) null : tokenList[index + 1];
        RelativeCursorLocation locationRelativeToToken1 = cursor.DetermineLocationRelativeToToken(token3);
        RelativeCursorLocation locationRelativeToToken2 = cursor.DetermineLocationRelativeToToken(token4);
        if (locationRelativeToToken1 != RelativeCursorLocation.MoreThanOneCharBehind)
        {
          if (locationRelativeToToken1 == RelativeCursorLocation.RightBefore)
          {
            if (index - 1 < 0)
            {
              IToken token5 = tokenList[index - 1];
            }
            token1 = token3;
            break;
          }
          if (locationRelativeToToken1 != RelativeCursorLocation.Within)
          {
            if (locationRelativeToToken1 == RelativeCursorLocation.RightBehind || (locationRelativeToToken1 == RelativeCursorLocation.OneCharBehind || locationRelativeToToken1 == RelativeCursorLocation.MoreThanOneCharBehind) && locationRelativeToToken2 == RelativeCursorLocation.Before)
            {
              token1 = token3;
              break;
            }
          }
          else
            break;
        }
      }
      return token1;
    }

    public static int GetParamIndexRelativeToTokenPos(int tokenPos, string source, CursorPosition cursor)
    {
      int num1 = -1;
      if (tokenPos > 0)
      {
        ByDScriptingLanguageLexer dscriptingLanguageLexer = new ByDScriptingLanguageLexer((ICharStream) new ANTLRStringStream(source));
        List<IToken> tokenList = new List<IToken>();
        int num2 = 1;
        for (IToken token = dscriptingLanguageLexer.NextToken(); token.Type != -1 && (token.Type != 35 || num2 < tokenPos); token = dscriptingLanguageLexer.NextToken())
        {
          if (token.Channel != 99 && num2 >= tokenPos)
            tokenList.Add(token);
          ++num2;
        }
        bool flag = false;
        int num3 = 0;
        int num4 = 0;
        int num5 = 0;
        Stack<TokenStreamHelper.LastParenType> lastParenTypeStack = new Stack<TokenStreamHelper.LastParenType>();
        for (int index = 0; index < tokenList.Count && !flag; ++index)
        {
          IToken token1 = tokenList[index];
          IToken token2 = index + 1 == tokenList.Count ? (IToken) null : tokenList[index + 1];
          IToken token3 = (IToken) null;
          if (index >= 1)
            token3 = tokenList[index - 1];
          if (token1.Type == 64)
          {
            if (token3 != null && token3.Type == 5)
            {
              ++num3;
              lastParenTypeStack.Push(TokenStreamHelper.LastParenType.MethodCall);
            }
            else
            {
              ++num4;
              lastParenTypeStack.Push(TokenStreamHelper.LastParenType.Expression);
            }
          }
          if (token1.Type == 84 && lastParenTypeStack.Count > 0)
          {
            if (lastParenTypeStack.Pop() == TokenStreamHelper.LastParenType.MethodCall)
              --num3;
            else
              --num4;
          }
          if (num3 == 1 && token1.Type == 30)
            ++num5;
          RelativeCursorLocation locationRelativeToToken1 = cursor.DetermineLocationRelativeToToken(token1);
          RelativeCursorLocation locationRelativeToToken2 = cursor.DetermineLocationRelativeToToken(token2);
          switch (locationRelativeToToken1)
          {
            case RelativeCursorLocation.RightBefore:
            case RelativeCursorLocation.Within:
            case RelativeCursorLocation.RightBehind:
              flag = true;
              break;
            case RelativeCursorLocation.OneCharBehind:
            case RelativeCursorLocation.MoreThanOneCharBehind:
              if (locationRelativeToToken2 == RelativeCursorLocation.Before || locationRelativeToToken2 == RelativeCursorLocation.RightBefore || locationRelativeToToken2 == RelativeCursorLocation.Unspecified)
              {
                flag = true;
                break;
              }
              break;
          }
        }
        if (num3 == 1)
          num1 = num5;
      }
      return num1;
    }

    private enum LastParenType
    {
      MethodCall,
      Expression,
    }
  }
}
