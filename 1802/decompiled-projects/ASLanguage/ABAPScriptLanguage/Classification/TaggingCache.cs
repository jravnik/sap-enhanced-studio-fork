﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Classification.TaggingCache
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Antlr.Runtime;
using Antlr.Runtime.Tree;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.ABAPScriptLanguage.IntelliSense;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SAP.Copernicus.ABAPScriptLanguage.Classification
{
  public class TaggingCache
  {
    public static TaggingCache Instance = new TaggingCache();
    private Dictionary<string, TaggingCacheEntry> TaggingDictionary;

    private TaggingCache()
    {
      this.TaggingDictionary = new Dictionary<string, TaggingCacheEntry>();
    }

    public void InvalidateForFile(string filePath)
    {
      this.TaggingDictionary.Remove(filePath);
    }

    public CodeCompletionContext GetOrCreateContextForFile(string filePath, string source)
    {
      TaggingCacheEntry taggingCacheEntry = (TaggingCacheEntry) null;
      this.TaggingDictionary.TryGetValue(filePath, out taggingCacheEntry);
      if (taggingCacheEntry == null)
      {
        DateTime now = DateTime.Now;
        CodeCompletionContext completionContext = new CodeCompletionContext();
        taggingCacheEntry = new TaggingCacheEntry(completionContext);
        this.TaggingDictionary.Add(filePath, taggingCacheEntry);
        if (true)
          this.CreateContextFromParser(source, completionContext);
        else
          this.CreateContextFromLexer(source, completionContext);
        TimeSpan timeSpan = DateTime.Now.Subtract(now);
        Trace.WriteLine(string.Format("building new CodeCompletionContext took {1} for file {0}", (object) filePath, (object) timeSpan), "ABSL");
      }
      return taggingCacheEntry.CompletionContext;
    }

    private void CreateContextFromParser(string source, CodeCompletionContext completionContext)
    {
      try
      {
        ByDScriptingLanguageParser dscriptingLanguageParser = new ByDScriptingLanguageParser(new ByDScriptingLanguageLexer((ICharStream) new ANTLRStringStream(source)), completionContext);
        CommonTree tree = dscriptingLanguageParser.program().Tree;
        for (int i = 0; i < dscriptingLanguageParser.TokenStream.Count; ++i)
        {
          IToken token = dscriptingLanguageParser.TokenStream.Get(i);
          if (token.Channel == 99 && token.Type != 101)
            completionContext.AddTokenForTagging(token);
        }
      }
      catch (CodeCompletionMatchException ex)
      {
      }
      catch (Exception ex)
      {
        Trace.WriteLine("ABSL fronend parsing for token tacking failed. Reason: " + (object) ex);
      }
    }

    private void CreateContextFromLexer(string source, CodeCompletionContext completionContext)
    {
      try
      {
        ByDScriptingLanguageLexer dscriptingLanguageLexer = new ByDScriptingLanguageLexer((ICharStream) new ANTLRStringStream(source));
        IToken token;
        while ((token = dscriptingLanguageLexer.NextToken()).Type != -1)
        {
          if (token.Channel != 99 || token.Type != 101)
            completionContext.AddTokenForTagging(token);
        }
      }
      catch (Exception ex)
      {
        Trace.WriteLine("ABSL fronend lexing for token tacking failed. Reason: " + (object) ex);
      }
    }

    internal bool GetBreakpointSpan(string filePath, int line, ref TextSpan span)
    {
      TaggingCacheEntry taggingCacheEntry = (TaggingCacheEntry) null;
      if (!this.TaggingDictionary.TryGetValue(filePath, out taggingCacheEntry))
        return false;
      return taggingCacheEntry.CompletionContext.GetBreakpointSpan(line, ref span);
    }
  }
}
