﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Classification.ClassificationTypeNames
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

namespace SAP.Copernicus.ABAPScriptLanguage.Classification
{
  internal static class ClassificationTypeNames
  {
    internal const string ABSLKeyword = "ABSLKeyword";
    internal const string ABSLIdentifier = "ABSLIdentifier";
    internal const string ABSLStringLiteral = "ABSLStringLiteral";
    internal const string ABSLNumericLiteral = "ABSLNumericLiteral";
    internal const string ABSLText = "ABSLText";
    internal const string ABSLComment = "ABSLComment";
  }
}
