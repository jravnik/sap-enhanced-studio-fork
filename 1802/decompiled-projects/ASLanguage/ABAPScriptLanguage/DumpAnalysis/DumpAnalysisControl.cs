﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis.DumpAnalysisControl
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Repository.DataModel;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Markup;

namespace SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis
{
  [GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
  public partial class DumpAnalysisControl : System.Windows.Controls.UserControl, IComponentConnector
  {
    private ObservableCollection<Dump> dumpCollection = new ObservableCollection<Dump>();
    private ObservableCollection<CallStack> callStackCollection = new ObservableCollection<CallStack>();
    private DateTime dateTimeFrom;
    private DateTime dateTimeTo;
    private string userName;
    private string solutionName;
    private ICollectionView dumpListView;
    //[SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    //internal WindowsFormsHost windowsFormsHost;
    //[SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    //internal DumpAnalysisSelectionControl selectionToolbar;
    //[SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    //internal System.Windows.Controls.DataGrid dataGridDumpList;
    //[SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    //internal System.Windows.Controls.GroupBox groupBoxCallStack;
    //[SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    //internal System.Windows.Controls.DataGrid dataGridCallStack;
    //private bool _contentLoaded;

    public DumpAnalysisControl()
    {
      this.InitializeComponent();
      this.dateTimeFrom = this.selectionToolbar.DateTimeFrom.ToUniversalTime();
      this.dateTimeTo = this.selectionToolbar.DateTimeTo.ToUniversalTime();
      this.solutionName = this.selectionToolbar.Solution;
      this.userName = this.selectionToolbar.User;
      this.updateDumpList();
      this.dumpListView = CollectionViewSource.GetDefaultView((object) this.dumpCollection);
      this.dumpListView.GroupDescriptions.Add((GroupDescription) new PropertyGroupDescription("DateString"));
      this.dumpListView.SortDescriptions.Clear();
      this.dumpListView.SortDescriptions.Add(new SortDescription("Date", ListSortDirection.Descending));
      this.dumpListView.SortDescriptions.Add(new SortDescription("Time", ListSortDirection.Descending));
      this.dataGridDumpList.DataContext = (object) this.dumpListView;
      this.selectionToolbar.NumberOfEntries = this.dumpCollection.Count;
      this.selectionToolbar.updateDisplayValues();
      this.dataGridCallStack.DataContext = (object) CollectionViewSource.GetDefaultView((object) this.callStackCollection);
    }

    private void updateDumpList()
    {
      this.dumpCollection.Clear();
      this.callStackCollection.Clear();
      foreach (PDI_ABSL_S_DUMP queryDump in new ABSLDumpHandler().QueryDumps(this.dateTimeFrom.ToString("yyyy-MM-dd"), this.dateTimeTo.ToString("yyyy-MM-dd"), this.dateTimeFrom.ToString("HHmmss"), this.dateTimeTo.ToString("HHmmss"), this.userName, this.solutionName))
        this.dumpCollection.Add(new Dump(queryDump));
    }

    private void openScriptEditor(string solution, string path, string line, string column)
    {
      if (!(path != "n/a"))
        return;
      try
      {
        string currentSolutionName = CopernicusProjectSystemUtil.GetCurrentSolutionName();
        if (currentSolutionName != null && !currentSolutionName.Equals(solution) && System.Windows.Forms.MessageBox.Show(SAP.Copernicus.ABAPScriptLanguage.Resources.DumpAnalysisSolutionSwitchDetail, SAP.Copernicus.ABAPScriptLanguage.Resources.DumpAnalysisSolutionSwitchHeader, MessageBoxButtons.YesNo) != DialogResult.Yes)
          return;
        if (currentSolutionName == null || !currentSolutionName.Equals(solution))
        {
          RepositoryDataSet.SolutionsRow[] solutionsRowArray = (RepositoryDataSet.SolutionsRow[]) RepositoryDataCache.GetInstance().RepositoryDataSet.Tables["Solutions"].Select();
          XRepMapper.GetInstance().GetSolutionFromXRepository(solution, ((IEnumerable<RepositoryDataSet.SolutionsRow>) solutionsRowArray).Where<RepositoryDataSet.SolutionsRow>((Func<RepositoryDataSet.SolutionsRow, bool>) (s => s.Name == solution)).Single<RepositoryDataSet.SolutionsRow>().Version);
        }
        CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
        CopernicusDependentFileNode nodeObjectForPath = (CopernicusDependentFileNode) selectedProject.GetNodeObjectForPath(XRepMapper.GetInstance().GetLocalPathforXRepProjectEntities(path));
        if (nodeObjectForPath != null)
          CopernicusProjectSystemUtil.openScriptEditor(selectedProject, nodeObjectForPath.ID);
        if (!line.Equals("n/a") && line != string.Empty)
          DTEUtil.GetDTE().ExecuteCommand("Edit.Goto", line);
        if (column.Equals("n/a") || !(column != string.Empty))
          return;
        for (int index = 1; index < Convert.ToInt32(column); ++index)
          DTEUtil.GetDTE().ExecuteCommand("Edit.CharRightExtend", "");
        DTEUtil.GetDTE().ExecuteCommand("Edit.SelectionCancel", "");
      }
      catch (Exception ex)
      {
        int num = (int) System.Windows.Forms.MessageBox.Show(SAP.Copernicus.ABAPScriptLanguage.Resources.DumpAnalysisSolutionNoAccessDetail, SAP.Copernicus.ABAPScriptLanguage.Resources.DumpAnalysisSolutionNoAccessHeader, MessageBoxButtons.OK);
      }
    }

    private void selectionToolbar_SelectionChanged(object sender, EventArgs e)
    {
      this.dateTimeFrom = this.selectionToolbar.DateTimeFrom.ToUniversalTime();
      this.dateTimeTo = this.selectionToolbar.DateTimeTo.ToUniversalTime();
      this.solutionName = this.selectionToolbar.Solution;
      this.userName = this.selectionToolbar.User;
      this.updateDumpList();
      this.selectionToolbar.NumberOfEntries = this.dataGridDumpList.Items.Count;
    }

    private void selectionToolbar_Reset(object sender, EventArgs e)
    {
      this.dateTimeFrom = this.selectionToolbar.DateTimeFrom.ToUniversalTime();
      this.dateTimeTo = this.selectionToolbar.DateTimeTo.ToUniversalTime();
      this.solutionName = this.selectionToolbar.Solution;
      this.userName = this.selectionToolbar.User;
      this.updateDumpList();
      this.selectionToolbar.NumberOfEntries = this.dataGridDumpList.Items.Count;
    }

    private void dataGridDumpList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      Dump mouseSelectedGridRow = this.getMouseSelectedGridRow(sender, e) as Dump;
      if (mouseSelectedGridRow == null)
        return;
      if (mouseSelectedGridRow.Line.Equals("n/a") || mouseSelectedGridRow.Column.Equals("n/a"))
        this.openScriptEditor(mouseSelectedGridRow.Solution, mouseSelectedGridRow.Path, "1", "1");
      else
        this.openScriptEditor(mouseSelectedGridRow.Solution, mouseSelectedGridRow.Path, mouseSelectedGridRow.Line, mouseSelectedGridRow.Column);
    }

    private void dataGridCallStack_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      CallStack mouseSelectedGridRow = this.getMouseSelectedGridRow(sender, e) as CallStack;
      if (mouseSelectedGridRow == null)
        return;
      if (mouseSelectedGridRow.Line.Equals("n/a") || mouseSelectedGridRow.Column.Equals("n/a"))
        this.openScriptEditor(mouseSelectedGridRow.Solution, mouseSelectedGridRow.Path, "1", "1");
      else
        this.openScriptEditor(mouseSelectedGridRow.Solution, mouseSelectedGridRow.Path, mouseSelectedGridRow.Line, mouseSelectedGridRow.Column);
    }

    private object getMouseSelectedGridRow(object sender, MouseButtonEventArgs e)
    {
      IInputElement directlyOver = e.MouseDevice.DirectlyOver;
      if (directlyOver != null && directlyOver is FrameworkElement && ((FrameworkElement) directlyOver).Parent is System.Windows.Controls.DataGridCell)
      {
        System.Windows.Controls.DataGrid dataGrid = sender as System.Windows.Controls.DataGrid;
        if (dataGrid != null && dataGrid.SelectedItems != null && dataGrid.SelectedItems.Count == 1)
          return dataGrid.SelectedItem;
      }
      return (object) null;
    }

    private void dataGridDumpList_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.callStackCollection.Clear();
      System.Windows.Controls.DataGrid dataGrid = sender as System.Windows.Controls.DataGrid;
      if (dataGrid == null || dataGrid.SelectedItems == null || dataGrid.SelectedItems.Count != 1)
        return;
      Dump selectedItem = dataGrid.SelectedItem as Dump;
      if (selectedItem == null)
        return;
      DateTime universalTime = Convert.ToDateTime(selectedItem.Date).Add(Convert.ToDateTime(selectedItem.Time).TimeOfDay).ToUniversalTime();
      foreach (PDI_ABSL_S_STACKS queryStack in new ABSLDumpHandler().QueryStacks(universalTime.ToString("yyyy-MM-dd"), universalTime.ToString("HHmmss"), selectedItem.Transid))
        this.callStackCollection.Add(new CallStack(queryStack));
    }

    private void dataGridDumpList_Sorting(object sender, DataGridSortingEventArgs e)
    {
      DataGridColumn column = e.Column;
      ListSortDirection? sortDirection = e.Column.SortDirection;
      ListSortDirection? nullable = new ListSortDirection?((sortDirection.GetValueOrDefault() != ListSortDirection.Ascending ? 0 : (sortDirection.HasValue ? 1 : 0)) != 0 ? ListSortDirection.Descending : ListSortDirection.Ascending);
      column.SortDirection = nullable;
      this.dumpListView.SortDescriptions.Clear();
      this.dumpListView.SortDescriptions.Add(new SortDescription("Date", ListSortDirection.Descending));
      this.dumpListView.SortDescriptions.Add(new SortDescription(e.Column.SortMemberPath, e.Column.SortDirection.GetValueOrDefault()));
      if (e.Column.SortMemberPath != "Time")
        this.dumpListView.SortDescriptions.Add(new SortDescription("Time", ListSortDirection.Descending));
      e.Handled = true;
    }

    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/ASLanguage;component/dumpanalysis/dumpanalysiscontrol.xaml", UriKind.Relative));
    //}

    //[SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[SuppressMessage("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
    //[SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
    //[SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.windowsFormsHost = (WindowsFormsHost) target;
    //      break;
    //    case 2:
    //      this.selectionToolbar = (DumpAnalysisSelectionControl) target;
    //      break;
    //    case 3:
    //      this.dataGridDumpList = (System.Windows.Controls.DataGrid) target;
    //      this.dataGridDumpList.MouseDoubleClick += new MouseButtonEventHandler(this.dataGridDumpList_MouseDoubleClick);
    //      this.dataGridDumpList.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dataGridDumpList_SelectionChanged);
    //      this.dataGridDumpList.Sorting += new DataGridSortingEventHandler(this.dataGridDumpList_Sorting);
    //      break;
    //    case 4:
    //      this.groupBoxCallStack = (System.Windows.Controls.GroupBox) target;
    //      break;
    //    case 5:
    //      this.dataGridCallStack = (System.Windows.Controls.DataGrid) target;
    //      this.dataGridCallStack.MouseDoubleClick += new MouseButtonEventHandler(this.dataGridCallStack_MouseDoubleClick);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
