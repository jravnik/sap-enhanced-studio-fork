﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis.DumpAnalysisSelectionControl
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using SAP.Copernicus.Core.Automation;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis
{
  public class DumpAnalysisSelectionControl : UserControl
  {
    protected string helpid = HELP_IDS.BDS_DUMPANALYSIS;
    private ToolTip InformationToolTip = new ToolTip();
    private bool dateFromChecked = true;
    private bool timeFromChecked = true;
    private bool dateToChecked = true;
    private bool timeToChecked = true;
    private string solution = string.Empty;
    private string user = string.Empty;
    private IContainer components;
    private ToolStripContainer ToolStripContainer;
    private ToolStrip SelectionToolStrip;
    private ToolStripButton TodayButton;
    private ToolStripButton YesterdayButton;
    private ToolStripButton AllButton;
    private ToolStripButton AdvancedButton;
    private Label InformationLabel;
    private ToolStripButton ResetButton;
    private ToolStripButton RefreshButton;
    private ToolStripButton HelpButton;
    private DateTime dateTimeFrom;
    private DateTime dateTimeTo;
    private int numberOfEntries;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (DumpAnalysisSelectionControl));
      this.ToolStripContainer = new ToolStripContainer();
      this.InformationLabel = new Label();
      this.SelectionToolStrip = new ToolStrip();
      this.TodayButton = new ToolStripButton();
      this.YesterdayButton = new ToolStripButton();
      this.AllButton = new ToolStripButton();
      this.AdvancedButton = new ToolStripButton();
      this.ResetButton = new ToolStripButton();
      this.RefreshButton = new ToolStripButton();
      this.HelpButton = new ToolStripButton();
      this.ToolStripContainer.ContentPanel.SuspendLayout();
      this.ToolStripContainer.TopToolStripPanel.SuspendLayout();
      this.ToolStripContainer.SuspendLayout();
      this.SelectionToolStrip.SuspendLayout();
      this.SuspendLayout();
      this.ToolStripContainer.BottomToolStripPanelVisible = false;
      this.ToolStripContainer.ContentPanel.Controls.Add((Control) this.InformationLabel);
      this.ToolStripContainer.ContentPanel.Size = new Size(549, 30);
      this.ToolStripContainer.Dock = DockStyle.Fill;
      this.ToolStripContainer.LeftToolStripPanelVisible = false;
      this.ToolStripContainer.Location = new Point(0, 0);
      this.ToolStripContainer.Name = "ToolStripContainer";
      this.ToolStripContainer.RightToolStripPanelVisible = false;
      this.ToolStripContainer.Size = new Size(549, 55);
      this.ToolStripContainer.TabIndex = 0;
      this.ToolStripContainer.Text = "toolStripContainer1";
      this.ToolStripContainer.TopToolStripPanel.Controls.Add((Control) this.SelectionToolStrip);
      this.InformationLabel.AutoSize = true;
      this.InformationLabel.Location = new Point(3, 10);
      this.InformationLabel.Name = "InformationLabel";
      this.InformationLabel.Size = new Size(59, 13);
      this.InformationLabel.TabIndex = 1;
      this.InformationLabel.Text = "Information";
      this.SelectionToolStrip.Dock = DockStyle.None;
      this.SelectionToolStrip.GripStyle = ToolStripGripStyle.Hidden;
      this.SelectionToolStrip.Items.AddRange(new ToolStripItem[7]
      {
        (ToolStripItem) this.TodayButton,
        (ToolStripItem) this.YesterdayButton,
        (ToolStripItem) this.AllButton,
        (ToolStripItem) this.AdvancedButton,
        (ToolStripItem) this.ResetButton,
        (ToolStripItem) this.RefreshButton,
        (ToolStripItem) this.HelpButton
      });
      this.SelectionToolStrip.Location = new Point(3, 0);
      this.SelectionToolStrip.Name = "SelectionToolStrip";
      this.SelectionToolStrip.Size = new Size(314, 25);
      this.SelectionToolStrip.TabIndex = 0;
      this.TodayButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.TodayButton.Image = (Image) componentResourceManager.GetObject("TodayButton.Image");
      this.TodayButton.ImageTransparentColor = Color.Magenta;
      this.TodayButton.Name = "TodayButton";
      this.TodayButton.Size = new Size(44, 22);
      this.TodayButton.Text = "Today";
      this.TodayButton.Click += new EventHandler(this.TodayButton_Click);
      this.YesterdayButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.YesterdayButton.Image = (Image) componentResourceManager.GetObject("YesterdayButton.Image");
      this.YesterdayButton.ImageTransparentColor = Color.Magenta;
      this.YesterdayButton.Name = "YesterdayButton";
      this.YesterdayButton.Size = new Size(62, 22);
      this.YesterdayButton.Text = "Yesterday";
      this.YesterdayButton.Click += new EventHandler(this.YesterdayButton_Click);
      this.AllButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.AllButton.Image = (Image) componentResourceManager.GetObject("AllButton.Image");
      this.AllButton.ImageTransparentColor = Color.Magenta;
      this.AllButton.Name = "AllButton";
      this.AllButton.Size = new Size(25, 22);
      this.AllButton.Text = "All";
      this.AllButton.Click += new EventHandler(this.AllButton_Click);
      this.AdvancedButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.AdvancedButton.Image = (Image) componentResourceManager.GetObject("AdvancedButton.Image");
      this.AdvancedButton.ImageTransparentColor = Color.Magenta;
      this.AdvancedButton.Name = "AdvancedButton";
      this.AdvancedButton.Size = new Size(64, 22);
      this.AdvancedButton.Text = "Advanced";
      this.AdvancedButton.Click += new EventHandler(this.AdvancedButton_Click);
      this.ResetButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.ResetButton.Image = (Image) componentResourceManager.GetObject("ResetButton.Image");
      this.ResetButton.ImageTransparentColor = Color.Magenta;
      this.ResetButton.Name = "ResetButton";
      this.ResetButton.Size = new Size(39, 22);
      this.ResetButton.Text = "Reset";
      this.ResetButton.Click += new EventHandler(this.ResetButton_Click);
      this.RefreshButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.RefreshButton.Image = (Image) componentResourceManager.GetObject("RefreshButton.Image");
      this.RefreshButton.ImageTransparentColor = Color.Magenta;
      this.RefreshButton.Name = "RefreshButton";
      this.RefreshButton.Size = new Size(23, 22);
      this.RefreshButton.Text = "Refresh";
      this.RefreshButton.Click += new EventHandler(this.RefreshButton_Click);
      this.HelpButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.HelpButton.Image = (Image) componentResourceManager.GetObject("HelpButton.Image");
      this.HelpButton.ImageTransparentColor = Color.Magenta;
      this.HelpButton.Name = "HelpButton";
      this.HelpButton.Size = new Size(23, 22);
      this.HelpButton.Text = "Help";
      this.HelpButton.Click += new EventHandler(this.HelpButton_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.ToolStripContainer);
      this.Name = nameof (DumpAnalysisSelectionControl);
      this.Size = new Size(549, 55);
      this.ToolStripContainer.ContentPanel.ResumeLayout(false);
      this.ToolStripContainer.ContentPanel.PerformLayout();
      this.ToolStripContainer.TopToolStripPanel.ResumeLayout(false);
      this.ToolStripContainer.TopToolStripPanel.PerformLayout();
      this.ToolStripContainer.ResumeLayout(false);
      this.ToolStripContainer.PerformLayout();
      this.SelectionToolStrip.ResumeLayout(false);
      this.SelectionToolStrip.PerformLayout();
      this.ResumeLayout(false);
    }

    public event SelectionChangedEventHandler SelectionChanged;

    public event ResetEventHandler Reset;

    public DateTime DateTimeFrom
    {
      get
      {
        return this.dateTimeFrom;
      }
      private set
      {
        this.dateTimeFrom = value;
      }
    }

    public DateTime DateTimeTo
    {
      get
      {
        return this.dateTimeTo;
      }
      private set
      {
        this.dateTimeTo = value;
      }
    }

    public string Solution
    {
      get
      {
        return this.solution;
      }
      private set
      {
        this.solution = value;
      }
    }

    public string User
    {
      get
      {
        return this.user;
      }
      private set
      {
        this.user = value;
      }
    }

    public int NumberOfEntries
    {
      get
      {
        return this.numberOfEntries;
      }
      set
      {
        this.numberOfEntries = value;
      }
    }

    public DumpAnalysisSelectionControl()
    {
      this.InitializeComponent();
      this.dateTimeFrom = DateTime.Now.Date;
      this.dateTimeTo = DateTime.Now.Date.AddDays(1.0).AddSeconds(-1.0);
      this.updateDisplayValues();
      this.RefreshButton.Image = (Image) ActionIcons.Refresh;
      this.HelpButton.Image = (Image) ActionIcons.Help;
    }

    public void updateDisplayValues()
    {
      string str1 = "        ";
      string str2 = this.dateTimeFrom == DateTimePicker.MinimumDateTime ? string.Empty : "From: " + this.dateTimeFrom.ToString() + str1;
      if (this.dateTimeFrom == DateTime.MinValue)
        str2 = "";
      string str3 = this.dateTimeTo == DateTimePicker.MaximumDateTime ? string.Empty : "To: " + this.dateTimeTo.ToString() + str1;
      if (this.dateTimeTo == DateTime.MaxValue)
        str3 = "";
      string str4 = "User: " + this.user + str1;
      if (this.user == "")
        str4 = "";
      string str5 = "Solution: " + this.solution + str1;
      if (this.solution == "")
        str5 = "";
      string str6 = "Number of Entries: " + this.numberOfEntries.ToString();
      string caption = str2 + str3 + str4 + str5 + str6;
      this.InformationToolTip.SetToolTip((Control) this.InformationLabel, caption);
      this.InformationLabel.Text = caption;
    }

    private void TodayButton_Click(object sender, EventArgs e)
    {
      this.dateTimeFrom = DateTime.Now.Date;
      this.dateTimeTo = this.dateTimeFrom.AddDays(1.0).AddSeconds(-1.0);
      if (this.SelectionChanged != null)
        this.SelectionChanged(sender, e);
      this.updateDisplayValues();
      this.RefreshButton.Enabled = true;
    }

    private void YesterdayButton_Click(object sender, EventArgs e)
    {
      this.dateTimeFrom = DateTime.Now.Date.AddDays(-1.0);
      this.dateTimeTo = this.dateTimeFrom.AddDays(1.0).AddSeconds(-1.0);
      if (this.SelectionChanged != null)
        this.SelectionChanged(sender, e);
      this.updateDisplayValues();
      this.RefreshButton.Enabled = false;
    }

    private void AllButton_Click(object sender, EventArgs e)
    {
      this.dateTimeFrom = DateTime.Now.Date.AddMonths(-1);
      this.dateTimeTo = DateTime.Now.Date.AddDays(1.0).AddSeconds(-1.0);
      if (this.SelectionChanged != null)
        this.SelectionChanged(sender, e);
      this.updateDisplayValues();
      this.RefreshButton.Enabled = true;
    }

    private void AdvancedButton_Click(object sender, EventArgs e)
    {
      DumpAnalysisSelectionAdvancedDialog selectionAdvancedDialog = new DumpAnalysisSelectionAdvancedDialog();
      selectionAdvancedDialog.StartPosition = FormStartPosition.CenterScreen;
      selectionAdvancedDialog.DateTimeFrom = !(this.dateTimeFrom == DateTime.MinValue) ? this.dateTimeFrom : DateTime.Now.Date;
      selectionAdvancedDialog.DateFromChecked = this.dateFromChecked;
      selectionAdvancedDialog.TimeFromChecked = this.timeFromChecked;
      selectionAdvancedDialog.DateTimeTo = !(this.dateTimeTo == DateTime.MaxValue) ? this.dateTimeTo : DateTime.Now.Date.AddDays(1.0).AddSeconds(-1.0);
      selectionAdvancedDialog.DateToChecked = this.dateToChecked;
      selectionAdvancedDialog.TimeToChecked = this.timeToChecked;
      selectionAdvancedDialog.User = this.user;
      selectionAdvancedDialog.SolutionPrefix = this.solution;
      if (selectionAdvancedDialog.ShowDialog((IWin32Window) this) != DialogResult.OK)
        return;
      this.dateFromChecked = selectionAdvancedDialog.DateFromChecked;
      this.timeFromChecked = selectionAdvancedDialog.TimeFromChecked;
      this.dateTimeFrom = !this.dateFromChecked ? DateTime.MinValue : selectionAdvancedDialog.DateTimeFrom;
      this.dateToChecked = selectionAdvancedDialog.DateToChecked;
      this.timeToChecked = selectionAdvancedDialog.TimeToChecked;
      this.dateTimeTo = !this.dateToChecked ? DateTime.MaxValue : selectionAdvancedDialog.DateTimeTo;
      this.user = selectionAdvancedDialog.User;
      this.solution = selectionAdvancedDialog.SolutionPrefix;
      if (this.SelectionChanged != null)
        this.SelectionChanged(sender, e);
      this.updateDisplayValues();
      this.RefreshButton.Enabled = true;
    }

    private void ResetButton_Click(object sender, EventArgs e)
    {
      this.dateTimeFrom = DateTime.Now.Date;
      this.dateTimeTo = DateTime.Now.Date.AddDays(1.0).AddSeconds(-1.0);
      this.user = string.Empty;
      this.solution = string.Empty;
      this.numberOfEntries = 0;
      if (this.Reset != null)
        this.Reset(sender, e);
      this.updateDisplayValues();
      this.RefreshButton.Enabled = true;
    }

    private void RefreshButton_Click(object sender, EventArgs e)
    {
      if (this.SelectionChanged != null)
        this.SelectionChanged(sender, e);
      this.updateDisplayValues();
    }

    private void ToolStripContainer_ContentPanel_Load(object sender, EventArgs e)
    {
    }

    private void HelpButton_Click(object sender, EventArgs e)
    {
      HelpUtil.DisplayF1Help(this.helpid);
    }
  }
}
