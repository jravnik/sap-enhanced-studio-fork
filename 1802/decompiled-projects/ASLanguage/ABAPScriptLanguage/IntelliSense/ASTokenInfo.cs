﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.ASTokenInfo
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Package;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  public class ASTokenInfo : TokenInfo
  {
    public bool IsCompletorActive { set; get; }

    public ASTokenInfo(TokenInfo tokenInfo)
    {
      this.Color = tokenInfo.Color;
      this.EndIndex = tokenInfo.EndIndex;
      this.StartIndex = tokenInfo.StartIndex;
      this.Token = tokenInfo.Token;
      this.Trigger = tokenInfo.Trigger;
      this.Type = tokenInfo.Type;
    }
  }
}
