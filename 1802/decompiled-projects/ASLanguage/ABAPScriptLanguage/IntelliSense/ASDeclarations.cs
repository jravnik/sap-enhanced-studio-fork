﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.ASDeclarations
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Package;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  internal class ASDeclarations : Declarations
  {
    private bool sorted;
    private List<ASDeclaration> declarations;

    public ASDeclarations()
    {
      this.declarations = new List<ASDeclaration>();
    }

    public ASDeclarations(List<ASDeclaration> declarations)
    {
      this.declarations = declarations;
      this.sortIfNeeded();
    }

    public void AddDeclaration(ASDeclaration declaration)
    {
      this.declarations.Add(declaration);
      this.sorted = false;
    }

    private void sortIfNeeded()
    {
      if (this.sorted)
        return;
      this.declarations.Sort((IComparer<ASDeclaration>) new ASDeclaration.DisplayTextComparator());
      this.sorted = true;
    }

    public override int GetCount()
    {
      return this.declarations.Count;
    }

    public ASDeclaration this[int index]
    {
      get
      {
        if (index < 0 || index >= this.declarations.Count)
          return (ASDeclaration) null;
        this.sortIfNeeded();
        return this.declarations[index];
      }
      set
      {
        if (index < 0 || index >= this.declarations.Count)
          return;
        this.sortIfNeeded();
        this.declarations[index] = value;
      }
    }

    public override string GetDescription(int index)
    {
      ASDeclaration asDeclaration = this[index];
      if (asDeclaration == null)
        return "";
      return asDeclaration.Description;
    }

    public override string GetDisplayText(int index)
    {
      ASDeclaration asDeclaration = this[index];
      if (asDeclaration == null)
        return (string) null;
      return asDeclaration.DisplayText;
    }

    public override int GetGlyph(int index)
    {
      ASDeclaration asDeclaration = this[index];
      if (asDeclaration == null)
        return -1;
      return asDeclaration.GlyphTypeImageIndex;
    }

    public override string GetName(int index)
    {
      ASDeclaration asDeclaration = this[index];
      if (asDeclaration == null)
        return (string) null;
      return asDeclaration.Name;
    }

    public override void GetBestMatch(string value, out int index, out bool uniqueMatch)
    {
      index = 0;
      uniqueMatch = false;
      this.sortIfNeeded();
      for (int index1 = 0; index1 < this.declarations.Count; ++index1)
      {
        if (this.declarations[index1].DisplayText.StartsWith(value, StringComparison.OrdinalIgnoreCase))
        {
          index = index1;
          if (index1 >= this.declarations.Count - 1 || this.declarations[index1 + 1].DisplayText.StartsWith(value, StringComparison.OrdinalIgnoreCase))
            break;
          uniqueMatch = true;
          break;
        }
      }
    }
  }
}
