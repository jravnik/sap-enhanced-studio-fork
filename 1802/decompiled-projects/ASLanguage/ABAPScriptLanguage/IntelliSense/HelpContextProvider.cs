﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.HelpContextProvider
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Editor;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.Core.Util;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  internal class HelpContextProvider
  {
    [Import]
    private IClassifierAggregatorService aggregator;
    [Import]
    private IVsEditorAdaptersFactoryService adapter;
    [Import]
    private IClassificationTypeRegistryService classificationTypeService;
    private IClassificationType abslKeyword;
    private IClassificationType abslIdentifier;

    private IClassificationType ABSLKeyword
    {
      get
      {
        IClassificationType classificationType = this.abslKeyword;
        if (classificationType == null && this.classificationTypeService != null)
          this.abslKeyword = classificationType = this.classificationTypeService.GetClassificationType(nameof (ABSLKeyword));
        return classificationType;
      }
    }

    private IClassificationType ABSLIdentifier
    {
      get
      {
        IClassificationType classificationType = this.abslIdentifier;
        if (classificationType == null && this.classificationTypeService != null)
          this.abslIdentifier = classificationType = this.classificationTypeService.GetClassificationType(nameof (ABSLIdentifier));
        return classificationType;
      }
    }

    public static HelpContextProvider Create()
    {
      HelpContextProvider helpContextProvider = new HelpContextProvider();
      try
      {
        VSPackageUtil.CompositionContainer.ComposeParts((object) helpContextProvider);
      }
      catch (CompositionException ex)
      {
      }
      return helpContextProvider;
    }

    private HelpContextProvider()
    {
    }

    public string GetHelpID(IVsTextLines buffer, TextSpan[] ptsSelection)
    {
      string str = Resources.HELPID_BDS_ABSL;
      if (buffer == null || ptsSelection == null || ptsSelection.Length == 0)
        return str;
      ITextBuffer dataBuffer = this.adapter.GetDataBuffer((IVsTextBuffer) buffer);
      int piLineCount;
      buffer.GetLineCount(out piLineCount);
      if (ptsSelection[0].iStartLine >= piLineCount)
        return str;
      int piPosition1;
      if (buffer.GetPositionOfLineIndex(ptsSelection[0].iStartLine, ptsSelection[0].iStartIndex, out piPosition1) != 0)
      {
        buffer.GetPositionOfLine(ptsSelection[0].iStartLine, out piPosition1);
        int piLength;
        buffer.GetLengthOfLine(ptsSelection[0].iStartLine, out piLength);
        piPosition1 = piPosition1 + piLength - 1;
      }
      int num1 = ptsSelection[0].iEndLine;
      int num2;
      if (buffer.GetPositionOfLineIndex(ptsSelection[0].iEndLine, ptsSelection[0].iEndIndex, out num2) != 0)
      {
        if (ptsSelection[0].iEndLine >= piLineCount)
        {
          buffer.GetSize(out num2);
          num1 = piLineCount - 1;
        }
        else
        {
          buffer.GetPositionOfLine(ptsSelection[0].iEndLine, out num2);
          int piLength;
          buffer.GetLengthOfLine(ptsSelection[0].iEndLine, out piLength);
          num2 = num2 + piLength - 1;
        }
      }
      ITextSnapshot currentSnapshot = dataBuffer.CurrentSnapshot;
      SnapshotSpan snapshotSpan = new SnapshotSpan(currentSnapshot, piPosition1, num2 - piPosition1);
      IClassifier classifier = this.aggregator.GetClassifier(dataBuffer);
      int piPosition2;
      int piLength1;
      for (int iStartLine = ptsSelection[0].iStartLine; iStartLine <= num1 && (buffer.GetPositionOfLine(iStartLine, out piPosition2) == 0 && buffer.GetLengthOfLine(iStartLine, out piLength1) == 0); ++iStartLine)
      {
        SnapshotSpan span = new SnapshotSpan(currentSnapshot, piPosition2, piLength1);
        foreach (ClassificationSpan classificationSpan in (IEnumerable<ClassificationSpan>) classifier.GetClassificationSpans(span))
        {
          if (snapshotSpan.IsEmpty && classificationSpan.Span.Contains(snapshotSpan) || !snapshotSpan.IsEmpty && snapshotSpan.Contains(classificationSpan.Span))
          {
            if (classificationSpan.ClassificationType == this.ABSLKeyword)
            {
              str = Resources.HELPID_BDS_ABSL + (object) '.' + classificationSpan.Span.GetText().ToUpperInvariant();
              return str;
            }
            if (classificationSpan.ClassificationType == this.ABSLIdentifier)
              return str;
          }
        }
      }
      return str;
    }
  }
}
