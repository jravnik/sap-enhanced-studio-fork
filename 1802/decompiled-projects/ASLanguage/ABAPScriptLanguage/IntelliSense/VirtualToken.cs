﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.VirtualToken
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Antlr.Runtime;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  public class VirtualToken
  {
    private IToken Token;

    public int StartIndex { private set; get; }

    public int Length { private set; get; }

    public int Type
    {
      get
      {
        if (this.Token == null)
          return 67;
        return this.Token.Type;
      }
    }

    public int Channel
    {
      get
      {
        if (this.Token == null)
          return 99;
        return this.Token.Channel;
      }
    }

    public VirtualToken(IToken token)
    {
      this.Token = token;
      this.StartIndex = token.StartIndex;
      this.Length = token.Text.Length;
    }

    public VirtualToken(int startIndex, int length)
    {
      this.StartIndex = startIndex;
      this.Length = length;
    }
  }
}
