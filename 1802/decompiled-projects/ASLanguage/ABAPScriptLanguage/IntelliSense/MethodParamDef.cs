﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.MethodParamDef
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  public class MethodParamDef
  {
    public string Name { get; private set; }

    public string DisplayName { get; private set; }

    public string Description { get; private set; }

    public MethodParamDef(string name, string displayName, string description)
    {
      this.Name = name;
      this.DisplayName = displayName;
      this.Description = description;
    }
  }
}
