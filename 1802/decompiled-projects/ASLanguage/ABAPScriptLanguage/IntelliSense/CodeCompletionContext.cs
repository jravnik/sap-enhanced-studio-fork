﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.CodeCompletionContext
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Antlr.Runtime;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.ABAPScriptLanguage.Env;
using System.Collections.Generic;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  public class CodeCompletionContext
  {
    public int[] AllowedTokenTypes = new int[0];
    private Dictionary<int, List<VirtualToken>> TokensOfTagLine = new Dictionary<int, List<VirtualToken>>();
    private List<TextSpan> breakpointSpans = new List<TextSpan>();
    private bool breakpointSpansSorted;
    private bool tooltipPathFound;
    private bool tooltipIdentifierFound;
    private TextSpan tooltipPathSpan;
    private TextSpan tooltipIdentifierSpan;

    public CodeCompletionMode CCMode { set; get; }

    public CursorPosition Cursor { set; get; }

    public CodeCompletionContext.SourceModificationMode SourceModification { set; get; }

    public string PartialTokenText { set; get; }

    public CodeCompletionContextMode ContextMode { private set; get; }

    public bool InsertIdentifier { set; get; }

    public List<VirtualToken> TokensToTagForLine(int line)
    {
      if (!this.TokensOfTagLine.ContainsKey(line))
        return new List<VirtualToken>();
      return this.TokensOfTagLine[line];
    }

    private CodeCompletionContext(CodeCompletionMode ccMode, CursorPosition cursor, CodeCompletionContextMode contextMode)
    {
      this.CCMode = ccMode;
      this.Cursor = cursor;
      this.ContextMode = contextMode;
      this.InsertIdentifier = false;
      this.SourceModification = CodeCompletionContext.SourceModificationMode.None;
    }

    public CodeCompletionContext(CursorPosition cursor, CodeCompletionContextMode contextMode)
      : this(CodeCompletionMode.None, cursor, contextMode)
    {
    }

    public CodeCompletionContext()
    {
      this.ContextMode = CodeCompletionContextMode.Tagging;
    }

    public void AddTokenForTagging(IToken token)
    {
      int line = token.Line - 1;
      if (token.Channel == 99 && token.Text.Contains("\r\n"))
      {
        int startIndex = token.StartIndex;
        string str1 = token.Text.Replace("\r", "");
        char[] chArray = new char[1]{ '\n' };
        foreach (string str2 in str1.Split(chArray))
        {
          this.AddSingleTokenForTagging(line++, new VirtualToken(startIndex, str2.Length));
          startIndex += str2.Length + 2;
        }
      }
      else
        this.AddSingleTokenForTagging(line, new VirtualToken(token));
    }

    private void AddSingleTokenForTagging(int line, VirtualToken vToken)
    {
      if (!this.TokensOfTagLine.ContainsKey(line))
        this.TokensOfTagLine[line] = new List<VirtualToken>();
      this.TokensOfTagLine[line].Add(vToken);
    }

    public override string ToString()
    {
      return string.Format("Mode = '{0}'{1}, CursorPosition = {2}/{3}", (object) this.CCMode.GetStringValue(), string.IsNullOrEmpty(this.PartialTokenText) ? (object) "" : (object) (", Prefix = '" + this.PartialTokenText + "'"), (object) this.Cursor.Line, (object) this.Cursor.Column);
    }

    public string ToTestResultString()
    {
      string[] allowedKeywordStrings = TokenHelper.GetAllowedKeywordStrings(this);
      string str1 = this.ToString();
      if (allowedKeywordStrings.Length > 0)
      {
        str1 += ", Keywords =";
        foreach (string str2 in allowedKeywordStrings)
          str1 = str1 + " " + str2;
      }
      return str1;
    }

    public void AddBreakpointSpan(int startLine, int startCol, int endLine, int endCol)
    {
      this.breakpointSpans.Add(new TextSpan()
      {
        iStartLine = startLine,
        iStartIndex = startCol,
        iEndLine = endLine,
        iEndIndex = endCol
      });
    }

    public void AddTooltipPath(TextSpan span)
    {
      if (!this.tooltipPathFound)
      {
        this.tooltipPathFound = true;
        this.tooltipPathSpan = span;
      }
      else
      {
        if (!Microsoft.VisualStudio.Package.TextSpanHelper.ContainsInclusive(this.tooltipPathSpan, span.iStartLine, span.iStartIndex) || !Microsoft.VisualStudio.Package.TextSpanHelper.ContainsInclusive(this.tooltipPathSpan, span.iEndLine, span.iEndIndex))
          return;
        this.tooltipPathSpan = span;
      }
    }

    public void AddTooltipIdentifier(TextSpan span)
    {
      this.tooltipIdentifierFound = true;
      this.tooltipIdentifierSpan = span;
    }

    public bool GetTooltipSpans(out TextSpan pathSpan, out TextSpan identifierSpan)
    {
      if (!this.tooltipIdentifierFound)
      {
        TextSpan textSpan = default(TextSpan);
		identifierSpan = textSpan;
		pathSpan = (identifierSpan = textSpan);
		return false;
      }
      identifierSpan = this.tooltipIdentifierSpan;
      if (this.tooltipPathFound)
        pathSpan = new TextSpan()
        {
          iStartLine = this.tooltipPathSpan.iStartLine,
          iStartIndex = this.tooltipPathSpan.iStartIndex,
          iEndLine = this.tooltipIdentifierSpan.iEndLine,
          iEndIndex = this.tooltipIdentifierSpan.iEndIndex
        };
      else
        pathSpan = this.tooltipIdentifierSpan;
      return true;
    }

    public bool GetBreakpointSpan(int line, ref TextSpan span)
    {
      if (!this.breakpointSpansSorted)
      {
        this.breakpointSpans.Sort(TextSpanHelper.CompareByStart);
        this.breakpointSpansSorted = true;
      }
      int index1 = this.breakpointSpans.BinarySearch(new TextSpan()
      {
        iStartLine = line,
        iStartIndex = 0
      }, TextSpanHelper.CompareByStart);
      if (index1 >= 0)
      {
        span = this.breakpointSpans[index1];
        return true;
      }
      int index2 = ~index1;
      if (index2 > 0 && TextSpanHelper.Contains(this.breakpointSpans[index2 - 1], line, 0))
      {
        span = this.breakpointSpans[index2 - 1];
        return true;
      }
      if (index2 >= this.breakpointSpans.Count || this.breakpointSpans[index2].iStartLine != line)
        return false;
      span = this.breakpointSpans[index2];
      return true;
    }

    public enum SourceModificationMode
    {
      None,
      ForeachVariable,
      ForeachAfterIn,
      SwitchValue,
    }
  }
}
