﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.IntelliSense.ASDeclaration
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System.Collections.Generic;

namespace SAP.Copernicus.ABAPScriptLanguage.IntelliSense
{
  public class ASDeclaration
  {
    private string name;
    private string description;
    private string displayText;
    private int glyphTypeImageIndex;

    public ASDeclaration(string name, string description, string displayText, int glyphTypeImageIndex)
    {
      this.name = name;
      this.description = description;
      this.displayText = displayText;
      this.glyphTypeImageIndex = glyphTypeImageIndex;
    }

    public ASDeclaration(string name, int glyphTypeImageIndex)
      : this(name, name, name, glyphTypeImageIndex)
    {
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public string Description
    {
      get
      {
        return this.description;
      }
    }

    public string DisplayText
    {
      get
      {
        return this.displayText;
      }
    }

    public int GlyphTypeImageIndex
    {
      get
      {
        return this.glyphTypeImageIndex;
      }
    }

    internal class DefaultComparator : IComparer<ASDeclaration>
    {
      public int Compare(ASDeclaration decl1, ASDeclaration decl2)
      {
        int num = 1;
        if (decl1 != null && decl2 != null)
          num = decl1.name.CompareTo(decl2.name);
        return num;
      }
    }

    internal class DisplayTextComparator : IComparer<ASDeclaration>
    {
      public int Compare(ASDeclaration decl1, ASDeclaration decl2)
      {
        int num = 1;
        if (decl1 != null && decl2 != null)
          num = decl1.displayText.CompareTo(decl2.displayText);
        return num;
      }
    }
  }
}
