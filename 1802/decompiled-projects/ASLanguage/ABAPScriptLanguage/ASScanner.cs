﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.ASScanner
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Antlr.Runtime;
using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.ABAPScriptLanguage.Env;
using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.ABAPScriptLanguage
{
  public class ASScanner : IScanner
  {
    private IVsTextLines textBuffer;
    private string source;
    private int offset;
    private ByDScriptingLanguageLexer lexer;
    private IToken LastToken;
    private int MethodCallCount;

    public ASScanner(IVsTextLines buffer)
    {
      this.textBuffer = buffer;
    }

    public void SetSource(string source, int offset)
    {
      this.offset = offset;
      this.source = source;
      this.lexer = (ByDScriptingLanguageLexer) null;
    }

    public bool ScanTokenAndProvideInfoAboutIt(TokenInfo tokenInfo, ref int state)
    {
      if (this.offset >= this.source.Length)
        return false;
      string str = string.Empty;
      if (this.lexer == null)
        this.lexer = new ByDScriptingLanguageLexer((ICharStream) new ANTLRStringStream(this.source.Substring(this.offset)));
      if (this.lexer != null)
      {
        IToken token = this.lexer.NextToken();
        str = token.Text;
        if (((IEnumerable<int>) TokenHelper.KeywordTokenTypes).Contains<int>(token.Type))
        {
          tokenInfo.Type = TokenType.Keyword;
        }
        else
        {
          tokenInfo.Token = token.Type;
          switch (token.Type)
          {
            case 63:
            case 65:
            case 68:
            case 69:
            case 70:
            case 87:
            case 45:
            case 51:
            case 52:
            case 36:
            case 37:
            case 7:
            case 12:
              tokenInfo.Type = TokenType.Operator;
              tokenInfo.Trigger = TokenTriggers.None;
              if (token.Type == 37)
              {
                tokenInfo.Trigger |= TokenTriggers.MemberSelect;
                break;
              }
              break;
            case 64:
            case 84:
            case 30:
            case 35:
              tokenInfo.Type = TokenType.Delimiter;
              tokenInfo.Trigger = TokenTriggers.None;
              if (token.Type == 64 && this.LastToken != null && this.LastToken.Type == 5)
              {
                ++this.MethodCallCount;
                tokenInfo.Trigger |= TokenTriggers.ParameterStart;
              }
              if (token.Type == 84 && this.MethodCallCount > 0)
              {
                tokenInfo.Trigger |= TokenTriggers.ParameterEnd;
                --this.MethodCallCount;
                break;
              }
              break;
            case 75:
            case 32:
              tokenInfo.Type = TokenType.Literal;
              tokenInfo.Trigger = TokenTriggers.None;
              break;
            case 86:
            case 18:
              tokenInfo.Type = TokenType.String;
              tokenInfo.Trigger = TokenTriggers.None;
              break;
            case 5:
              tokenInfo.Type = TokenType.Identifier;
              tokenInfo.Trigger = TokenTriggers.None;
              break;
            default:
              tokenInfo.Type = TokenType.Text;
              tokenInfo.Trigger = TokenTriggers.None;
              break;
          }
        }
        this.LastToken = token;
      }
      if (string.IsNullOrEmpty(str))
        return false;
      tokenInfo.StartIndex = this.offset;
      this.offset += str.Length;
      tokenInfo.EndIndex = this.offset - 1;
      return true;
    }
  }
}
