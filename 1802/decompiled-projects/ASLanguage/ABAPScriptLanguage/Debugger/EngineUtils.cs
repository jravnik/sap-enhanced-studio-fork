﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.EngineUtils
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Package;
using SAP.Copernicus.ASLanguage;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.CopernicusProjectView;
using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  public static class EngineUtils
  {
    private static ASLanguageService languageService;

    private static ASLanguageService LanguageService
    {
      get
      {
        if (EngineUtils.languageService == null)
          EngineUtils.languageService = ASLanguagePackage.GetLanguageService();
        return EngineUtils.languageService;
      }
    }

    internal static void CheckOk(int hr)
    {
      if (hr == 0)
        return;
      Marshal.ThrowExceptionForHR(hr);
    }

    internal static void RequireOk(int hr)
    {
      if (hr != 0)
        throw new InvalidOperationException();
    }

    internal static bool CheckBackendHashForABSLFile(string backendHash, string filePath)
    {
      if (backendHash != "")
        return backendHash == EngineUtils.GetHashForABSLFile(filePath);
      return false;
    }

    internal static string GetHashForABSLFile(string filePath)
    {
      MD5 md5 = (MD5) new MD5CryptoServiceProvider();
      string s = "";
      bool flag = false;
      Source source = EngineUtils.LanguageService.GetSource(filePath);
      if (source != null)
      {
        s = source.GetText();
        flag = true;
      }
      else
      {
        try
        {
          s = File.ReadAllText(filePath);
          flag = true;
        }
        catch
        {
        }
      }
      return BitConverter.ToString(!flag ? new byte[0] : md5.ComputeHash(Encoding.UTF8.GetBytes(s))).Replace("-", "");
    }

    internal static string PDIMessagesToString(PDI_RI_S_MESSAGE[] msgList)
    {
      StringBuilder stringBuilder = new StringBuilder();
      foreach (PDI_RI_S_MESSAGE msg in msgList)
      {
        if (msg.TEXT.Length > 0)
          stringBuilder.AppendLine(EngineUtils.EnsureDelimiter(msg.TEXT));
      }
      return stringBuilder.ToString();
    }

    internal static string EnsureDelimiter(string text)
    {
      if (text.Length > 0 && !".!?:;,".Contains<char>(text[text.Length - 1]))
        return text + (object) '.';
      return text;
    }

    internal static string GetCurrentSolutionNamespace()
    {
      CopernicusProjectNode selectedProject = CopernicusProjectSystemUtil.getSelectedProject();
      if (selectedProject == null)
        return "";
      return selectedProject.RepositoryNamespace;
    }
  }
}
