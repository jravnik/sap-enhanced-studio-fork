﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.BreakpointUnboundEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class BreakpointUnboundEvent : AsynchronousEvent, IDebugBreakpointUnboundEvent2
  {
    public const string IID = "78D1DB4F-C557-4DC5-A2DD-5369D21B1C8C";
    private IDebugBoundBreakpoint2 boundBreakpoint;

    private BreakpointUnboundEvent(IDebugBoundBreakpoint2 boundBreakpoint)
    {
      this.boundBreakpoint = boundBreakpoint;
    }

    public static void Send(DebugEngine engine, IDebugBoundBreakpoint2 boundBreakpoint)
    {
      BreakpointUnboundEvent breakpointUnboundEvent = new BreakpointUnboundEvent(boundBreakpoint);
      engine.Send((IDebugEvent2) breakpointUnboundEvent, "78D1DB4F-C557-4DC5-A2DD-5369D21B1C8C", (IDebugThread2) null);
    }

    public int GetBreakpoint(out IDebugBoundBreakpoint2 ppBP)
    {
      ppBP = this.boundBreakpoint;
      return 0;
    }

    public int GetReason(enum_BP_UNBOUND_REASON[] pdwUnboundReason)
    {
      pdwUnboundReason[0] = enum_BP_UNBOUND_REASON.BPUR_UNKNOWN;
      return 0;
    }
  }
}
