﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.VariableInformation
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class VariableInformation
  {
    public VariableInformation[] children;
    public string m_name;
    public string m_typeName;
    public string m_value;
  }
}
