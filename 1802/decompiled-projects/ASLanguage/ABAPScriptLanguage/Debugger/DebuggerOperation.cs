﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.DebuggerOperation
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  public enum DebuggerOperation
  {
    NOT_SUPPORTED = -1,
    LOGOFF = 0,
    START_LISTENER = 1,
    ATTACH_DEBUGGEE = 2,
    DETACH_DEBUGGEE = 3,
    STOP_LISTENER = 4,
    INITIALIZE_DEBUGGER = 5,
    PING = 6,
    GET_STACK_INFO = 7,
    SET_BREAKPOINT = 100,
    DELETE_BREAKPOINT = 101,
    SET_DYNAMIC_BREAKPOINT = 102,
    DELETE_DYNAMIC_BREAKPOINT = 103,
    STEP_INTO = 200,
    STEP_OVER = 201,
    CONTINUE = 202,
    STEP_OUT = 203,
    INTERPRET_EXPRESSION = 300,
  }
}
