﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.DebugThread
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using Microsoft.VisualStudio.Debugger.Interop;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class DebugThread : IDebugThread2, IDebugThread100
  {
    private uint m_threadId;
    private DebugEngine m_engine;
    private string m_threadName;
    public ThreadContext threadContext;
    public readonly PDI_ABSL_S_DEBUGGEE m_attachedDebuggee;
    private JSONClient m_statefulJSONClient;
    public bool waitingForInput;
    private FifoExecution fifoExecution;
    private ABAPDebugSession ABAPSession;
    private Semaphore listenerSemaphore;

    public DebugThread(DebugEngine engine, uint threadID, PDI_ABSL_S_DEBUGGEE attachedDebuggee, JSONClient statefulJSONClient, ABAPDebugSession ABAPSession, Semaphore listenerSemaphore)
    {
      this.m_engine = engine;
      this.m_threadId = threadID;
      this.m_attachedDebuggee = attachedDebuggee;
      this.m_statefulJSONClient = statefulJSONClient;
      this.m_threadName = "ABSL Session " + (object) this.m_threadId;
      this.ABAPSession = ABAPSession;
      this.listenerSemaphore = listenerSemaphore;
      this.fifoExecution = new FifoExecution();
    }

    internal void Schedule_StepOrContinue(DebuggerOperation operation)
    {
      this.waitingForInput = false;
      this.fifoExecution.QueueUserWorkItem((WaitCallback) (state => this.StepOrContinue(operation)), (object) null, true);
    }

    private void StepOrContinue(DebuggerOperation operation)
    {
      if (this.m_statefulJSONClient == null)
        return;
      Trace.WriteLine("ABSL Debugger - StepOrContinue begin");
      PDI_ABSL_DBG pdiAbslDbg = new PDI_ABSL_DBG();
      pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
      pdiAbslDbg.Importing.IV_OPERATION = (int) operation;
      pdiAbslDbg.Importing.IT_WAITING_DEBUGGEE = new PDI_ABSL_S_DEBUGGEE[1]
      {
        this.m_attachedDebuggee
      };
      try
      {
        this.m_statefulJSONClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
        pdiAbslDbg.Exporting = (PDI_ABSL_DBG.ExportingType) null;
      }
      if (pdiAbslDbg.Exporting == null || pdiAbslDbg.Exporting.EV_STATUS != 0)
      {
        int num = pdiAbslDbg.Exporting == null ? 2 : pdiAbslDbg.Exporting.EV_STATUS;
        this.m_statefulJSONClient = (JSONClient) null;
        this.listenerSemaphore.Release();
        lock (this.ABAPSession.m_DebugThreadList)
          this.ABAPSession.m_DebugThreadList.Remove(this);
        if (num == 2)
        {
          ErrorEvent.Send(this.m_engine, this, enum_MESSAGETYPE.MT_MESSAGEBOX, string.Format(Resources.DebuggerConnectionLostPopup), 16U);
          OutputStringEvent.Send(this.m_engine, this, string.Format(Resources.DebuggerConnectionLostOutputWindow));
        }
        else if (num == 3)
        {
          ErrorEvent.Send(this.m_engine, this, enum_MESSAGETYPE.MT_MESSAGEBOX, string.Format(Resources.DebuggerDebuggeeResetPopup, (object) EngineUtils.PDIMessagesToString(pdiAbslDbg.Exporting.ET_MESSAGES)), 16U);
          OutputStringEvent.Send(this.m_engine, this, string.Format(Resources.DebuggerDebuggeeResetOutputWindow, (object) EngineUtils.PDIMessagesToString(pdiAbslDbg.Exporting.ET_MESSAGES)));
        }
        ThreadDestroyEvent.Send(this.m_engine, this, 0U);
        foreach (DebugThread debugThread in this.ABAPSession.m_DebugThreadList)
        {
          if (debugThread.waitingForInput)
            BreakpointEvent.Send(this.m_engine, debugThread, (IEnumDebugBoundBreakpoints2) new BoundBreakpointsEnum(new IDebugBoundBreakpoint2[0]));
        }
      }
      else if (pdiAbslDbg.Exporting.EV_SUCCESS != "")
      {
        string xrepProjectEntities = XRepMapper.GetInstance().GetLocalPathforXRepProjectEntities(pdiAbslDbg.Exporting.EV_XREP_FILE_PATH);
        this.threadContext = new ThreadContext(this.m_engine, this, xrepProjectEntities, pdiAbslDbg.Exporting.ES_BREAKPOINT_SPAN.textspan, pdiAbslDbg.Exporting.ET_VARIABLE, pdiAbslDbg.Exporting.ET_CALLSTACK);
        this.waitingForInput = true;
        if (operation == DebuggerOperation.CONTINUE)
        {
          PendingBreakpoint documentNameAndLineSpan = this.m_engine.breakPointManager.GetByDocumentNameAndLineSpan(xrepProjectEntities, pdiAbslDbg.Exporting.ES_BREAKPOINT_SPAN.START_LINE, pdiAbslDbg.Exporting.ES_BREAKPOINT_SPAN.END_LINE);
          if (documentNameAndLineSpan != null)
            documentNameAndLineSpan.RaiseBreakEvent(this);
          else
            SteppingCompleteEvent.Send(this.m_engine, this);
        }
        else
          SteppingCompleteEvent.Send(this.m_engine, this);
        if (!EngineUtils.CheckBackendHashForABSLFile(pdiAbslDbg.Exporting.EV_SOURCE_HASH, xrepProjectEntities))
          this.m_engine.ShowSourceOutOfDatePopup(EngineUtils.PDIMessagesToString(pdiAbslDbg.Exporting.ET_MESSAGES));
      }
      Trace.WriteLine("ABSL Debugger - StepOrContinue end");
    }

    internal void Schedule_AddDynamicBreakpoints(List<PendingBreakpoint> pendingBreakpoints)
    {
      this.fifoExecution.QueueUserWorkItem((WaitCallback) (ignored => this.AddDynamicBreakpoints(pendingBreakpoints)), (object) null, true);
    }

    private void AddDynamicBreakpoints(List<PendingBreakpoint> pendingBreakpoints)
    {
      if (this.m_statefulJSONClient == null)
        return;
      Trace.WriteLine(string.Format("ABSL Debugger - AddDynamicBreakpoints begin {0}", (object) pendingBreakpoints.Count));
      PDI_ABSL_DBG pdiAbslDbg = new PDI_ABSL_DBG();
      PDI_ABSL_S_BREAKPOINT[] pdiAbslSBreakpointArray = new PDI_ABSL_S_BREAKPOINT[pendingBreakpoints.Count];
      Tuple<PDI_ABSL_S_BREAKPOINT, PendingBreakpoint>[] tupleArray = new Tuple<PDI_ABSL_S_BREAKPOINT, PendingBreakpoint>[pendingBreakpoints.Count];
      for (int index = 0; index < pendingBreakpoints.Count; ++index)
      {
        pdiAbslSBreakpointArray[index] = new PDI_ABSL_S_BREAKPOINT();
        pdiAbslSBreakpointArray[index].LINE = pendingBreakpoints[index].line;
        pdiAbslSBreakpointArray[index].XREP_FILE_PATH = XRepMapper.GetInstance().GetXrepPathforLocalFile(pendingBreakpoints[index].documentName);
        pdiAbslSBreakpointArray[index].SOURCE_HASH = EngineUtils.GetHashForABSLFile(pendingBreakpoints[index].documentName);
        tupleArray[index] = Tuple.Create<PDI_ABSL_S_BREAKPOINT, PendingBreakpoint>(pdiAbslSBreakpointArray[index], pendingBreakpoints[index]);
      }
      pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
      pdiAbslDbg.Importing.IV_OPERATION = 102;
      pdiAbslDbg.Importing.IT_WAITING_DEBUGGEE = new PDI_ABSL_S_DEBUGGEE[1]
      {
        this.m_attachedDebuggee
      };
      pdiAbslDbg.Importing.IT_BREAKPOINT = pdiAbslSBreakpointArray;
      try
      {
        this.m_statefulJSONClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
        pdiAbslDbg.Exporting = (PDI_ABSL_DBG.ExportingType) null;
      }
      if (pdiAbslDbg.Exporting != null && pdiAbslDbg.Exporting.EV_SUCCESS != "")
      {
        Array.Sort<PDI_ABSL_S_BREAKPOINT>(pdiAbslDbg.Exporting.ET_BREAKPOINT, PDI_ABSL_S_BREAKPOINT.CompareByFilePathAndLine);
        foreach (Tuple<PDI_ABSL_S_BREAKPOINT, PendingBreakpoint> tuple in tupleArray)
        {
          int index = Array.BinarySearch<PDI_ABSL_S_BREAKPOINT>(pdiAbslDbg.Exporting.ET_BREAKPOINT, tuple.Item1, PDI_ABSL_S_BREAKPOINT.CompareByFilePathAndLine);
          if (index >= 0)
          {
            PDI_ABSL_S_BREAKPOINT pdiAbslSBreakpoint = pdiAbslDbg.Exporting.ET_BREAKPOINT[index];
            if (pdiAbslSBreakpoint.ABAP_LINE.METHOD_INCLUDE.Length > 0)
              tuple.Item2.BoundLocation = pdiAbslSBreakpoint.ABAP_LINE;
          }
        }
      }
      Trace.WriteLine(string.Format("ABSL Debugger - AddDynamicBreakpoints end {0}", (object) pendingBreakpoints.Count));
    }

    internal void Schedule_DeleteDynamicBreakpoints(List<PendingBreakpoint> pendingBreakpoints)
    {
      this.fifoExecution.QueueUserWorkItem((WaitCallback) (ignored => this.DeleteDynamicBreakpoints(pendingBreakpoints)), (object) null, true);
    }

    private void DeleteDynamicBreakpoints(List<PendingBreakpoint> pendingBreakpoints)
    {
      if (this.m_statefulJSONClient == null)
        return;
      Trace.WriteLine(string.Format("ABSL Debugger - DeleteDynamicBreakpoints begin {0}", (object) pendingBreakpoints.Count));
      List<PDI_ABSL_S_BREAKPOINT> pdiAbslSBreakpointList = new List<PDI_ABSL_S_BREAKPOINT>();
      for (int index = 0; index < pendingBreakpoints.Count; ++index)
      {
        PDI_ABSL_S_ABAP_LINE boundLocation = pendingBreakpoints[index].BoundLocation;
        if (boundLocation != null)
          pdiAbslSBreakpointList.Add(new PDI_ABSL_S_BREAKPOINT()
          {
            LINE = pendingBreakpoints[index].line,
            XREP_FILE_PATH = XRepMapper.GetInstance().GetXrepPathforLocalFile(pendingBreakpoints[index].documentName),
            ABAP_LINE = boundLocation
          });
      }
      if (pdiAbslSBreakpointList.Count > 0)
      {
        PDI_ABSL_DBG pdiAbslDbg = new PDI_ABSL_DBG();
        pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
        pdiAbslDbg.Importing.IV_OPERATION = 103;
        pdiAbslDbg.Importing.IT_WAITING_DEBUGGEE = new PDI_ABSL_S_DEBUGGEE[1]
        {
          this.m_attachedDebuggee
        };
        pdiAbslDbg.Importing.IT_BREAKPOINT = pdiAbslSBreakpointList.ToArray();
        try
        {
          this.m_statefulJSONClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
        }
        catch (Exception ex)
        {
          Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
        }
      }
      Trace.WriteLine(string.Format("ABSL Debugger - DeleteDynamicBreakpoints end {0}", (object) pendingBreakpoints.Count));
    }

    internal void Schedule_Detach()
    {
      this.fifoExecution.QueueUserWorkItem((WaitCallback) (state => this.Detach()), (object) null, true);
    }

    private void Detach()
    {
      if (this.m_statefulJSONClient == null)
        return;
      PDI_ABSL_DBG pdiAbslDbg = new PDI_ABSL_DBG();
      pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
      pdiAbslDbg.Importing.IV_OPERATION = 3;
      pdiAbslDbg.Importing.IT_WAITING_DEBUGGEE = new PDI_ABSL_S_DEBUGGEE[1]
      {
        this.m_attachedDebuggee
      };
      try
      {
        this.m_statefulJSONClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
      }
      lock (this.ABAPSession.m_DebugThreadList)
        this.ABAPSession.m_DebugThreadList.Remove(this);
      this.listenerSemaphore.Release();
      this.m_statefulJSONClient = (JSONClient) null;
      ThreadDestroyEvent.Send(this.m_engine, this, 0U);
    }

    internal void Schedule_GetStackFrameInfo(StackFrame stackFrame, ManualResetEvent dataAvailableEvent)
    {
      this.fifoExecution.QueueUserWorkItem((WaitCallback) (state => this.GetStackFrameInfo(stackFrame, dataAvailableEvent)), (object) null, true);
    }

    private void GetStackFrameInfo(StackFrame stackFrame, ManualResetEvent dataAvailableEvent)
    {
      if (this.m_statefulJSONClient == null)
        return;
      PDI_ABSL_DBG pdiAbslDbg = new PDI_ABSL_DBG();
      pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
      pdiAbslDbg.Importing.IV_OPERATION = 7;
      pdiAbslDbg.Importing.IT_WAITING_DEBUGGEE = new PDI_ABSL_S_DEBUGGEE[1]
      {
        this.m_attachedDebuggee
      };
      pdiAbslDbg.Importing.IV_STACK_POSITION = stackFrame.StackPosition;
      try
      {
        this.m_statefulJSONClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
        pdiAbslDbg.Exporting = (PDI_ABSL_DBG.ExportingType) null;
      }
      if (pdiAbslDbg.Exporting != null && pdiAbslDbg.Exporting.EV_SUCCESS != "")
        stackFrame.SetData(pdiAbslDbg.Exporting.ET_VARIABLE);
      dataAvailableEvent.Set();
    }

    internal void Schedule_InterpretExpression(string expression, DebugExpression debugExpression, ManualResetEvent dataAvailableEvent)
    {
      this.fifoExecution.QueueUserWorkItem((WaitCallback) (state => this.InterpretExpression(expression, debugExpression, dataAvailableEvent)), (object) null, true);
    }

    private void InterpretExpression(string expression, DebugExpression debugExpression, ManualResetEvent dataAvailableEvent)
    {
      if (this.m_statefulJSONClient == null)
        return;
      PDI_ABSL_DBG pdiAbslDbg = new PDI_ABSL_DBG();
      pdiAbslDbg.Importing = new PDI_ABSL_DBG.ImportingType();
      pdiAbslDbg.Importing.IV_OPERATION = 300;
      pdiAbslDbg.Importing.IT_WAITING_DEBUGGEE = new PDI_ABSL_S_DEBUGGEE[1]
      {
        this.m_attachedDebuggee
      };
      pdiAbslDbg.Importing.IV_EXPRESSION = expression;
      try
      {
        this.m_statefulJSONClient.callFunctionModule((SAPFunctionModule) pdiAbslDbg, false, false, false);
      }
      catch (Exception ex)
      {
        Trace.WriteLine(string.Format("ABSL Debugger - callFunctionModule returned with exception {0}.", (object) ex.ToString()));
        pdiAbslDbg.Exporting = (PDI_ABSL_DBG.ExportingType) null;
      }
      if (pdiAbslDbg.Exporting != null && pdiAbslDbg.Exporting.EV_SUCCESS != "" && pdiAbslDbg.Exporting.ET_VARIABLE.Length == 1)
        debugExpression.AddInterpretedResult(pdiAbslDbg.Exporting.ET_VARIABLE[0]);
      dataAvailableEvent.Set();
    }

    int IDebugThread2.CanSetNextStatement(IDebugStackFrame2 pStackFrame, IDebugCodeContext2 pCodeContext)
    {
      return 1;
    }

    int IDebugThread2.EnumFrameInfo(enum_FRAMEINFO_FLAGS dwFieldSpec, uint nRadix, out IEnumDebugFrameInfo2 enumObject)
    {
      enumObject = (IEnumDebugFrameInfo2) this.threadContext.frameInfoEnum;
      return 0;
    }

    int IDebugThread2.GetLogicalThread(IDebugStackFrame2 pStackFrame, out IDebugLogicalThread2 ppLogicalThread)
    {
      throw new NotImplementedException();
    }

    int IDebugThread2.GetName(out string pbstrName)
    {
      pbstrName = this.m_threadName;
      return 0;
    }

    int IDebugThread2.GetProgram(out IDebugProgram2 ppProgram)
    {
      throw new NotImplementedException();
    }

    int IDebugThread2.GetThreadId(out uint pdwThreadId)
    {
      pdwThreadId = this.m_threadId;
      return 0;
    }

    int IDebugThread2.GetThreadProperties(enum_THREADPROPERTY_FIELDS dwFields, THREADPROPERTIES[] ptp)
    {
      try
      {
        THREADPROPERTIES threadproperties = new THREADPROPERTIES();
        if ((dwFields & enum_THREADPROPERTY_FIELDS.TPF_ID) != ~enum_THREADPROPERTY_FIELDS.TPF_ALLFIELDS)
        {
          threadproperties.dwThreadId = this.m_threadId;
          threadproperties.dwFields |= enum_THREADPROPERTY_FIELDS.TPF_ID;
        }
        if ((dwFields & enum_THREADPROPERTY_FIELDS.TPF_SUSPENDCOUNT) != ~enum_THREADPROPERTY_FIELDS.TPF_ALLFIELDS)
          threadproperties.dwFields |= enum_THREADPROPERTY_FIELDS.TPF_SUSPENDCOUNT;
        if ((dwFields & enum_THREADPROPERTY_FIELDS.TPF_STATE) != ~enum_THREADPROPERTY_FIELDS.TPF_ALLFIELDS)
        {
          threadproperties.dwThreadState = this.m_statefulJSONClient == null ? 2U : 1U;
          threadproperties.dwFields |= enum_THREADPROPERTY_FIELDS.TPF_STATE;
        }
        if ((dwFields & enum_THREADPROPERTY_FIELDS.TPF_PRIORITY) != ~enum_THREADPROPERTY_FIELDS.TPF_ALLFIELDS)
        {
          threadproperties.bstrPriority = "Normal";
          threadproperties.dwFields |= enum_THREADPROPERTY_FIELDS.TPF_PRIORITY;
        }
        if ((dwFields & enum_THREADPROPERTY_FIELDS.TPF_NAME) != ~enum_THREADPROPERTY_FIELDS.TPF_ALLFIELDS)
        {
          threadproperties.bstrName = this.m_threadName;
          threadproperties.dwFields |= enum_THREADPROPERTY_FIELDS.TPF_NAME;
        }
        if ((dwFields & enum_THREADPROPERTY_FIELDS.TPF_LOCATION) != ~enum_THREADPROPERTY_FIELDS.TPF_ALLFIELDS)
        {
          threadproperties.bstrLocation = this.m_threadName;
          threadproperties.dwFields |= enum_THREADPROPERTY_FIELDS.TPF_LOCATION;
        }
        return 0;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    int IDebugThread2.Resume(out uint pdwSuspendCount)
    {
      throw new NotImplementedException();
    }

    int IDebugThread2.SetNextStatement(IDebugStackFrame2 pStackFrame, IDebugCodeContext2 pCodeContext)
    {
      throw new NotImplementedException();
    }

    int IDebugThread2.SetThreadName(string pszName)
    {
      throw new NotImplementedException();
    }

    int IDebugThread2.Suspend(out uint pdwSuspendCount)
    {
      throw new NotImplementedException();
    }

    int IDebugThread100.CanDoFuncEval()
    {
      throw new NotImplementedException();
    }

    int IDebugThread100.GetFlags(out uint pFlags)
    {
      throw new NotImplementedException();
    }

    int IDebugThread100.GetThreadDisplayName(out string bstrDisplayName)
    {
      throw new NotImplementedException();
    }

    int IDebugThread100.GetThreadProperties100(uint dwFields, THREADPROPERTIES100[] props)
    {
      THREADPROPERTIES[] ptp = new THREADPROPERTIES[1];
      enum_THREADPROPERTY_FIELDS dwFields1 = (enum_THREADPROPERTY_FIELDS) ((int) dwFields & 63);
      int threadProperties = ((IDebugThread2) this).GetThreadProperties(dwFields1, ptp);
      props[0].bstrLocation = ptp[0].bstrLocation;
      props[0].bstrName = ptp[0].bstrName;
      props[0].bstrPriority = ptp[0].bstrPriority;
      props[0].dwFields = (uint) ptp[0].dwFields;
      props[0].dwSuspendCount = ptp[0].dwSuspendCount;
      props[0].dwThreadId = ptp[0].dwThreadId;
      props[0].dwThreadState = ptp[0].dwThreadState;
      if (threadProperties == 0 && (enum_THREADPROPERTY_FIELDS) dwFields != dwFields1)
      {
        if (((int) dwFields & 64) != 0)
        {
          props[0].bstrDisplayName = this.m_threadName;
          props[0].dwFields |= 64U;
          props[0].DisplayNamePriority = 10U;
          props[0].dwFields |= 128U;
        }
        if (((int) dwFields & 256) != 0)
        {
          props[0].dwThreadCategory = 2U;
          props[0].dwFields |= 256U;
        }
        if (((int) dwFields & 512) != 0)
        {
          props[0].AffinityMask = 0UL;
          props[0].dwFields |= 512U;
        }
        if (((int) dwFields & 1024) != 0)
        {
          props[0].priorityId = 0;
          props[0].dwFields |= 1024U;
        }
      }
      return threadProperties;
    }

    int IDebugThread100.SetFlags(uint flags)
    {
      throw new NotImplementedException();
    }

    int IDebugThread100.SetThreadDisplayName(string bstrDisplayName)
    {
      throw new NotImplementedException();
    }
  }
}
