﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.DebugProperty
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using System;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class DebugProperty : IDebugProperty2
  {
    private VariableInformation m_variableInformation;

    public DebugProperty(VariableInformation vi)
    {
      this.m_variableInformation = vi;
    }

    public DEBUG_PROPERTY_INFO ConstructDebugPropertyInfo(enum_DEBUGPROP_INFO_FLAGS dwFields)
    {
      DEBUG_PROPERTY_INFO debugPropertyInfo = new DEBUG_PROPERTY_INFO();
      if ((dwFields & enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_FULLNAME) != enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_NONE)
      {
        debugPropertyInfo.bstrFullName = this.m_variableInformation.m_name;
        debugPropertyInfo.dwFields |= enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_FULLNAME;
      }
      if ((dwFields & enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_NAME) != enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_NONE)
      {
        debugPropertyInfo.bstrName = this.m_variableInformation.m_name;
        debugPropertyInfo.dwFields |= enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_NAME;
      }
      if ((dwFields & enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_TYPE) != enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_NONE)
      {
        debugPropertyInfo.bstrType = this.m_variableInformation.m_typeName;
        debugPropertyInfo.dwFields |= enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_TYPE;
      }
      if ((dwFields & enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_VALUE) != enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_NONE)
      {
        debugPropertyInfo.bstrValue = this.m_variableInformation.m_value;
        debugPropertyInfo.dwFields |= enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_VALUE;
      }
      if ((dwFields & enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_ATTRIB) != enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_NONE)
      {
        debugPropertyInfo.dwAttrib = enum_DBG_ATTRIB_FLAGS.DBG_ATTRIB_VALUE_READONLY;
        if (this.m_variableInformation.children != null)
          debugPropertyInfo.dwAttrib |= enum_DBG_ATTRIB_FLAGS.DBG_ATTRIB_OBJ_IS_EXPANDABLE;
      }
      if ((dwFields & enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_PROP) != enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_NONE || this.m_variableInformation.children != null)
      {
        debugPropertyInfo.pProperty = (IDebugProperty2) this;
        debugPropertyInfo.dwFields |= enum_DEBUGPROP_INFO_FLAGS.DEBUGPROP_INFO_PROP;
      }
      return debugPropertyInfo;
    }

    int IDebugProperty2.EnumChildren(enum_DEBUGPROP_INFO_FLAGS dwFields, uint dwRadix, ref Guid guidFilter, enum_DBG_ATTRIB_FLAGS dwAttribFilter, string pszNameFilter, uint dwTimeout, out IEnumDebugPropertyInfo2 ppEnum)
    {
      ppEnum = (IEnumDebugPropertyInfo2) null;
      if (this.m_variableInformation.children == null)
        return 1;
      DEBUG_PROPERTY_INFO[] properties = new DEBUG_PROPERTY_INFO[this.m_variableInformation.children.Length];
      for (int index = 0; index < this.m_variableInformation.children.Length; ++index)
        properties[index] = new DebugProperty(this.m_variableInformation.children[index]).ConstructDebugPropertyInfo(dwFields);
      ppEnum = (IEnumDebugPropertyInfo2) new DebugPropertyEnum(properties);
      return 0;
    }

    int IDebugProperty2.GetDerivedMostProperty(out IDebugProperty2 ppDerivedMost)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetExtendedInfo(ref Guid guidExtendedInfo, out object pExtendedInfo)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetMemoryBytes(out IDebugMemoryBytes2 ppMemoryBytes)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetMemoryContext(out IDebugMemoryContext2 ppMemory)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetParent(out IDebugProperty2 ppParent)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetPropertyInfo(enum_DEBUGPROP_INFO_FLAGS dwFields, uint dwRadix, uint dwTimeout, IDebugReference2[] rgpArgs, uint dwArgCount, DEBUG_PROPERTY_INFO[] pPropertyInfo)
    {
      pPropertyInfo[0] = new DEBUG_PROPERTY_INFO();
      rgpArgs = (IDebugReference2[]) null;
      pPropertyInfo[0] = this.ConstructDebugPropertyInfo(dwFields);
      return 0;
    }

    int IDebugProperty2.GetReference(out IDebugReference2 ppReference)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.GetSize(out uint pdwSize)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.SetValueAsReference(IDebugReference2[] rgpArgs, uint dwArgCount, IDebugReference2 pValue, uint dwTimeout)
    {
      throw new NotImplementedException();
    }

    int IDebugProperty2.SetValueAsString(string pszValue, uint dwRadix, uint dwTimeout)
    {
      throw new NotImplementedException();
    }
  }
}
