﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.PendingBreakpoint
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class PendingBreakpoint : IDebugPendingBreakpoint2
  {
    private object m_lockToken = new object();
    private IDebugBreakpointRequest2 m_pBPRequest;
    private BP_REQUEST_INFO m_bpRequestInfo;
    private DebugEngine m_engine;
    private BreakpointManager m_bpManager;
    private bool m_enabled;
    private bool m_deleted;
    private ABSLBreakpoint m_abslBP;
    private ABSLErrorBreakpoint m_abslErrorBP;
    public readonly string documentName;
    public readonly int line;

    internal PDI_ABSL_S_ABAP_LINE BoundLocation
    {
      get
      {
        if (this.m_abslBP == null)
          return (PDI_ABSL_S_ABAP_LINE) null;
        return this.m_abslBP.abapBreakpointLocation;
      }
      set
      {
        lock (this.m_lockToken)
        {
          if (this.m_abslBP == null)
            return;
          this.m_abslBP.abapBreakpointLocation = value;
        }
      }
    }

    public PendingBreakpoint(IDebugBreakpointRequest2 pBPRequest, DebugEngine engine, BreakpointManager bpManager)
    {
      this.m_pBPRequest = pBPRequest;
      BP_REQUEST_INFO[] pBPRequestInfo = new BP_REQUEST_INFO[1];
      EngineUtils.CheckOk(this.m_pBPRequest.GetRequestInfo(enum_BPREQI_FIELDS.BPREQI_BPLOCATION, pBPRequestInfo));
      this.m_bpRequestInfo = pBPRequestInfo[0];
      this.m_engine = engine;
      this.m_bpManager = bpManager;
      this.m_enabled = true;
      this.m_deleted = false;
      if ((int) this.m_bpRequestInfo.bpLocation.bpLocationType != 65537)
        return;
      IDebugDocumentPosition2 objectForIunknown = (IDebugDocumentPosition2) Marshal.GetObjectForIUnknown(this.m_bpRequestInfo.bpLocation.unionmember2);
      string pbstrFileName;
      EngineUtils.CheckOk(objectForIunknown.GetFileName(out pbstrFileName));
      string projectFolderOnDisk = XRepMapper.GetInstance().GetProjectFolderOnDisk();
      this.documentName = projectFolderOnDisk.Length <= 0 || !pbstrFileName.ToLower().StartsWith(projectFolderOnDisk.ToLower()) ? pbstrFileName : projectFolderOnDisk + (pbstrFileName.Length > projectFolderOnDisk.Length ? pbstrFileName.Substring(projectFolderOnDisk.Length) : "");
      TEXT_POSITION[] pBegPosition = new TEXT_POSITION[1];
      TEXT_POSITION[] pEndPosition = new TEXT_POSITION[1];
      EngineUtils.CheckOk(objectForIunknown.GetRange(pBegPosition, pEndPosition));
      this.line = (int) pBegPosition[0].dwLine + 1;
    }

    private bool CanBind()
    {
      return !this.m_deleted && (int) this.m_bpRequestInfo.bpLocation.bpLocationType == 65537;
    }

    public void OnBoundBreakpointDeleted()
    {
      lock (this.m_lockToken)
      {
        this.m_abslBP = (ABSLBreakpoint) null;
        this.m_abslErrorBP = (ABSLErrorBreakpoint) null;
      }
    }

    public DebugDocumentContext GetDocumentContext()
    {
      IDebugDocumentPosition2 objectForIunknown = (IDebugDocumentPosition2) Marshal.GetObjectForIUnknown(this.m_bpRequestInfo.bpLocation.unionmember2);
      string pbstrFileName;
      EngineUtils.CheckOk(objectForIunknown.GetFileName(out pbstrFileName));
      TEXT_POSITION[] pBegPosition = new TEXT_POSITION[1];
      TEXT_POSITION[] pEndPosition = new TEXT_POSITION[1];
      EngineUtils.CheckOk(objectForIunknown.GetRange(pBegPosition, pEndPosition));
      DebugCodeContext codeContext = new DebugCodeContext(this.m_engine, pbstrFileName, this.line);
      return new DebugDocumentContext(pbstrFileName, pBegPosition[0], pEndPosition[0], codeContext);
    }

    internal void RaiseBoundEvent(PDI_ABSL_S_ABAP_LINE abapLine)
    {
      lock (this.m_lockToken)
      {
        if (this.m_abslBP != null || this.m_abslErrorBP != null)
          return;
        this.m_abslBP = new ABSLBreakpoint(this.m_engine, this, new BreakpointResolution(this.m_engine, this.GetDocumentContext(), this.documentName, this.line), this.m_enabled);
        this.m_abslBP.abapBreakpointLocation = abapLine;
        BreakpointBoundEvent.Send(this.m_engine, (IDebugPendingBreakpoint2) this, (IDebugBoundBreakpoint2) this.m_abslBP);
      }
    }

    internal void RaiseBreakEvent(DebugThread thread)
    {
      lock (this.m_lockToken)
      {
        if (this.m_abslBP == null)
          return;
        BreakpointEvent.Send(this.m_engine, thread, (IEnumDebugBoundBreakpoints2) new BoundBreakpointsEnum(new IDebugBoundBreakpoint2[1]
        {
          (IDebugBoundBreakpoint2) this.m_abslBP
        }));
      }
    }

    internal void RaiseErrorEvent(string errorMessage)
    {
      lock (this.m_lockToken)
      {
        if (this.m_abslErrorBP != null || this.m_abslBP != null)
          return;
        this.m_abslErrorBP = new ABSLErrorBreakpoint(this.m_engine, this, new ErrorBreakpointResolution(this.m_engine, this.GetDocumentContext(), enum_BP_ERROR_TYPE.BPET_GENERAL_WARNING, errorMessage, this.documentName, this.line));
        BreakpointErrorEvent.Send(this.m_engine, (IDebugErrorBreakpoint2) this.m_abslErrorBP);
        this.m_engine.ShowBreakpointErrorPopup();
      }
    }

    int IDebugPendingBreakpoint2.Bind()
    {
      if (!this.CanBind() || !this.m_enabled)
        return 1;
      this.m_engine.ABAPSession.AddBreakPoint(this);
      return 0;
    }

    int IDebugPendingBreakpoint2.CanBind(out IEnumDebugErrorBreakpoints2 ppErrorEnum)
    {
      ppErrorEnum = (IEnumDebugErrorBreakpoints2) null;
      if (this.CanBind())
        return 0;
      ppErrorEnum = (IEnumDebugErrorBreakpoints2) null;
      return 1;
    }

    int IDebugPendingBreakpoint2.Delete()
    {
      if (!this.m_deleted)
      {
        this.m_deleted = true;
        this.m_engine.ABAPSession.DeleteBreakPoint(this);
      }
      this.m_bpManager.DeletePendingBreakpoint(this);
      return 0;
    }

    int IDebugPendingBreakpoint2.Enable(int fEnable)
    {
      bool flag = fEnable != 0;
      if (this.m_enabled != flag)
      {
        this.m_enabled = flag;
        if (this.m_enabled)
          this.m_engine.ABAPSession.AddBreakPoint(this);
        else
          this.m_engine.ABAPSession.DeleteBreakPoint(this);
      }
      return 0;
    }

    int IDebugPendingBreakpoint2.EnumBoundBreakpoints(out IEnumDebugBoundBreakpoints2 ppEnum)
    {
      IDebugBoundBreakpoint2[] breakpoints;
      lock (this.m_lockToken)
      {
        if (this.m_abslBP != null)
          breakpoints = new IDebugBoundBreakpoint2[1]
          {
            (IDebugBoundBreakpoint2) this.m_abslBP
          };
        else
          breakpoints = new IDebugBoundBreakpoint2[0];
      }
      ppEnum = (IEnumDebugBoundBreakpoints2) new BoundBreakpointsEnum(breakpoints);
      return 0;
    }

    int IDebugPendingBreakpoint2.EnumErrorBreakpoints(enum_BP_ERROR_TYPE bpErrorType, out IEnumDebugErrorBreakpoints2 ppEnum)
    {
      IDebugErrorBreakpoint2[] breakpoints;
      lock (this.m_lockToken)
      {
        if (this.m_abslErrorBP != null)
          breakpoints = new IDebugErrorBreakpoint2[1]
          {
            (IDebugErrorBreakpoint2) this.m_abslErrorBP
          };
        else
          breakpoints = new IDebugErrorBreakpoint2[0];
      }
      ppEnum = (IEnumDebugErrorBreakpoints2) new ErrorBreakpointsEnum(breakpoints);
      return 0;
    }

    int IDebugPendingBreakpoint2.GetBreakpointRequest(out IDebugBreakpointRequest2 ppBPRequest)
    {
      ppBPRequest = this.m_pBPRequest;
      return 0;
    }

    int IDebugPendingBreakpoint2.GetState(PENDING_BP_STATE_INFO[] pState)
    {
      if (this.m_deleted)
        pState[0].state = enum_PENDING_BP_STATE.PBPS_DELETED;
      else if (this.m_enabled)
        pState[0].state = enum_PENDING_BP_STATE.PBPS_ENABLED;
      else if (!this.m_enabled)
        pState[0].state = enum_PENDING_BP_STATE.PBPS_DISABLED;
      return 0;
    }

    int IDebugPendingBreakpoint2.SetCondition(BP_CONDITION bpCondition)
    {
      return -2147467263;
    }

    int IDebugPendingBreakpoint2.SetPassCount(BP_PASSCOUNT bpPassCount)
    {
      return -2147467263;
    }

    int IDebugPendingBreakpoint2.Virtualize(int fVirtualize)
    {
      return 0;
    }
  }
}
