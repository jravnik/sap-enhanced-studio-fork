﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ThreadDestroyEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class ThreadDestroyEvent : AsynchronousEvent, IDebugThreadDestroyEvent2
  {
    public const string IID = "2C3B7532-A36F-4A6E-9072-49BE649B8541";
    private readonly uint m_exitCode;

    public static void Send(DebugEngine engine, DebugThread thread, uint exitCode)
    {
      ThreadDestroyEvent threadDestroyEvent = new ThreadDestroyEvent(exitCode);
      engine.Send((IDebugEvent2) threadDestroyEvent, "2C3B7532-A36F-4A6E-9072-49BE649B8541", (IDebugThread2) thread);
    }

    public ThreadDestroyEvent(uint exitCode)
    {
      this.m_exitCode = exitCode;
    }

    int IDebugThreadDestroyEvent2.GetExitCode(out uint exitCode)
    {
      exitCode = this.m_exitCode;
      return 0;
    }
  }
}
