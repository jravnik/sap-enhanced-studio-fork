﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.BreakpointManager
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class BreakpointManager
  {
    private Dictionary<Tuple<string, int>, PendingBreakpoint> pendingBreakpoints = new Dictionary<Tuple<string, int>, PendingBreakpoint>();
    private DebugEngine engine;

    public BreakpointManager(DebugEngine engine)
    {
      this.engine = engine;
    }

    public void CreatePendingBreakpoint(IDebugBreakpointRequest2 pBPRequest, out IDebugPendingBreakpoint2 ppPendingBP)
    {
      PendingBreakpoint pendingBreakpoint = new PendingBreakpoint(pBPRequest, this.engine, this);
      ppPendingBP = (IDebugPendingBreakpoint2) pendingBreakpoint;
      lock (this.pendingBreakpoints)
        this.pendingBreakpoints.Add(Tuple.Create<string, int>(pendingBreakpoint.documentName, pendingBreakpoint.line), pendingBreakpoint);
    }

    public void DeletePendingBreakpoint(PendingBreakpoint pendingBreakpoint)
    {
      lock (this.pendingBreakpoints)
        this.pendingBreakpoints.Remove(Tuple.Create<string, int>(pendingBreakpoint.documentName, pendingBreakpoint.line));
    }

    public void ClearBoundBreakpoints()
    {
      lock (this.pendingBreakpoints)
      {
        foreach (IDebugPendingBreakpoint2 pendingBreakpoint2 in this.pendingBreakpoints.Values)
          pendingBreakpoint2.Delete();
      }
    }

    public PendingBreakpoint GetByDocumentNameAndLineSpan(string documentName, int startLine, int endLine)
    {
      lock (this.pendingBreakpoints)
      {
        PendingBreakpoint pendingBreakpoint;
        if (this.pendingBreakpoints.TryGetValue(Tuple.Create<string, int>(documentName, startLine), out pendingBreakpoint))
          return pendingBreakpoint;
        return (PendingBreakpoint) null;
      }
    }
  }
}
