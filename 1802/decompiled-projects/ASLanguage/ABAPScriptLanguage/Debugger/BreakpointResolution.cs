﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.BreakpointResolution
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;
using System.Runtime.InteropServices;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class BreakpointResolution : IDebugBreakpointResolution2
  {
    private DebugEngine engine;
    private int lineNumber;
    private string filename;
    private DebugDocumentContext documentContext;

    public BreakpointResolution(DebugEngine engine, DebugDocumentContext documentContext, string filename, int lineNumber)
    {
      this.engine = engine;
      this.lineNumber = lineNumber;
      this.documentContext = documentContext;
      this.filename = filename;
    }

    int IDebugBreakpointResolution2.GetBreakpointType(enum_BP_TYPE[] pBPType)
    {
      pBPType[0] = enum_BP_TYPE.BPT_CODE;
      return 0;
    }

    int IDebugBreakpointResolution2.GetResolutionInfo(enum_BPRESI_FIELDS dwFields, BP_RESOLUTION_INFO[] pBPResolutionInfo)
    {
      if ((dwFields & enum_BPRESI_FIELDS.BPRESI_BPRESLOCATION) != ~enum_BPRESI_FIELDS.BPRESI_ALLFIELDS)
      {
        BP_RESOLUTION_LOCATION resolutionLocation = new BP_RESOLUTION_LOCATION();
        resolutionLocation.bpType = 1U;
        DebugCodeContext debugCodeContext = new DebugCodeContext(this.engine, this.filename, this.lineNumber);
        debugCodeContext.SetDocumentContext((IDebugDocumentContext2) this.documentContext);
        resolutionLocation.unionmember1 = Marshal.GetComInterfaceForObject((object) debugCodeContext, typeof (IDebugCodeContext2));
        pBPResolutionInfo[0].bpResLocation = resolutionLocation;
        pBPResolutionInfo[0].dwFields |= enum_BPRESI_FIELDS.BPRESI_BPRESLOCATION;
      }
      if ((dwFields & enum_BPRESI_FIELDS.BPRESI_PROGRAM) != ~enum_BPRESI_FIELDS.BPRESI_ALLFIELDS)
      {
        pBPResolutionInfo[0].pProgram = (IDebugProgram2) this.engine;
        pBPResolutionInfo[0].dwFields |= enum_BPRESI_FIELDS.BPRESI_PROGRAM;
      }
      return 0;
    }
  }
}
