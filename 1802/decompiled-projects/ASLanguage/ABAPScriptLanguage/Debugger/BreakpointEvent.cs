﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.BreakpointEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class BreakpointEvent : StoppingEvent, IDebugBreakpointEvent2
  {
    public const string IID = "501C1E21-C557-48B8-BA30-A1EAB0BC4A74";
    private IEnumDebugBoundBreakpoints2 m_boundBreakpoints;

    public static void Send(DebugEngine engine, DebugThread thread, IEnumDebugBoundBreakpoints2 boundBreakpoints)
    {
      BreakpointEvent breakpointEvent = new BreakpointEvent(boundBreakpoints);
      engine.Send((IDebugEvent2) breakpointEvent, "501C1E21-C557-48B8-BA30-A1EAB0BC4A74", (IDebugThread2) thread);
    }

    public BreakpointEvent(IEnumDebugBoundBreakpoints2 boundBreakpoints)
    {
      this.m_boundBreakpoints = boundBreakpoints;
    }

    int IDebugBreakpointEvent2.EnumBreakpoints(out IEnumDebugBoundBreakpoints2 ppEnum)
    {
      ppEnum = this.m_boundBreakpoints;
      return 0;
    }
  }
}
