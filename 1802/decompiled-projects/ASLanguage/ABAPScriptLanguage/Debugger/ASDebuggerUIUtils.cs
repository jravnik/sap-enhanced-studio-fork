﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.ASDebuggerUIUtils
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using EnvDTE80;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Util;
using System.Windows.Forms;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal class ASDebuggerUIUtils
  {
    private static IVsStatusbar statusbar;

    internal static string GetUserAliasForDebuggingFromOptionsDialog()
    {
      if (!Connection.getInstance().isConnected() || !PropertyAccess.GeneralProps.TracingActiveForUser)
        return "";
      return PropertyAccess.GeneralProps.TracingUser.ToUpper();
    }

    private static IVsStatusbar Statusbar
    {
      get
      {
        if (ASDebuggerUIUtils.statusbar == null)
          ASDebuggerUIUtils.statusbar = VSPackageUtil.ServiceProvider.GetService(typeof (SVsStatusbar)) as IVsStatusbar;
        return ASDebuggerUIUtils.statusbar;
      }
    }

    internal static void SetStatusbarText(string text)
    {
      ASDebuggerUIUtils.Statusbar.SetText(text);
    }

    internal static void DisplayMessagesInCopernicusMsgBox(string text)
    {
      int num = (int) CopernicusMessageBox.Show(text, (VSPackageUtil.ServiceProvider.GetService(typeof (SDTE)) as DTE2).Name, MessageBoxButtons.OK, MessageBoxIcon.Hand);
    }
  }
}
