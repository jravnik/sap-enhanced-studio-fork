﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ABAPScriptLanguage.Debugger.SteppingCompleteEvent
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using Microsoft.VisualStudio.Debugger.Interop;

namespace SAP.Copernicus.ABAPScriptLanguage.Debugger
{
  internal sealed class SteppingCompleteEvent : StoppingEvent, IDebugStepCompleteEvent2
  {
    public const string IID = "0F7F24C1-74D9-4EA6-A3EA-7EDB2D81441D";

    public static void Send(DebugEngine engine, DebugThread thread)
    {
      SteppingCompleteEvent steppingCompleteEvent = new SteppingCompleteEvent();
      engine.Send((IDebugEvent2) steppingCompleteEvent, "0F7F24C1-74D9-4EA6-A3EA-7EDB2D81441D", (IDebugThread2) thread);
    }
  }
}
