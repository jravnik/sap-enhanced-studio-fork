﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ASLanguage.GuidList
// Assembly: ASLanguage, Version=142.0.3319.231, Culture=neutral, PublicKeyToken=null
// MVID: B2913705-40A9-434D-B9D4-3DAA421257A1
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1802\Extensions\Application\ASLanguage.dll

using System;

namespace SAP.Copernicus.ASLanguage
{
  internal static class GuidList
  {
    public static readonly Guid guidASLanguageCmdSet = new Guid("25b79ca0-bc36-4dd0-ac8a-a944022fb4c3");
    public const string guidASLanguagePkgString = "e1916f8d-ac6d-499d-9efb-4317d7a43619";
    public const string guidASLanguageCmdSetString = "25b79ca0-bc36-4dd0-ac8a-a944022fb4c3";
  }
}
