﻿using Antlr.Runtime;
using Antlr.Runtime.Tree;
using Microsoft.VisualStudio.TextManager.Interop;
using SAP.Copernicus.ABAPScriptLanguage.Env;
using SAP.Copernicus.ABAPScriptLanguage.IntelliSense;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

[CLSCompliant(false), GeneratedCode("ANTLR", "3.4")]
public class ByDScriptingLanguageParser : Parser
{
    private static class Follow
    {
        public static readonly BitSet _impStmt_in_program168 = new BitSet(new ulong[]
        {
            450943303229056096uL,
            91524532357uL
        });

        public static readonly BitSet _sync_in_program171 = new BitSet(new ulong[]
        {
            450943303229056096uL,
            91524532357uL
        });

        public static readonly BitSet _stmt_in_program174 = new BitSet(new ulong[]
        {
            450943303229056096uL,
            91524532357uL
        });

        public static readonly BitSet _sync_in_program176 = new BitSet(new ulong[]
        {
            450943303229056096uL,
            91524532357uL
        });

        public static readonly BitSet _EOF_in_program180 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _LCURLB_in_stmtList223 = new BitSet(new ulong[]
        {
            450943303229056096uL,
            91524794501uL
        });

        public static readonly BitSet _stmt_in_stmtList227 = new BitSet(new ulong[]
        {
            450943303229056096uL,
            91524794501uL
        });

        public static readonly BitSet _RCURLB_in_stmtList242 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _singleStmt_in_stmtList247 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _ifStmt_in_stmt259 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _switchStmt_in_stmt264 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _whileStmt_in_stmt269 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _forEachStmt_in_stmt274 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _emptyStmt_in_stmt279 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _varDeclStmt_in_stmt284 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _singleStmt_in_stmt290 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _methodOrAssignmentStmt_in_singleStmt305 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _raiseStmt_in_singleStmt310 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _contStmt_in_singleStmt315 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _breakStmt_in_singleStmt320 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _returnStmt_in_singleStmt325 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _errorStmts_in_singleStmt330 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _ABSLID_in_singleStmt336 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _unmatchedElse_in_errorStmts348 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _unmatchedCase_in_errorStmts353 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _unmatchedDefault_in_errorStmts358 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _unexpOrErrLiteral_in_errorStmts363 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _DELIM_in_emptyStmt374 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _IMPORT_in_impStmt391 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _impNameSpace_in_impStmt394 = new BitSet(new ulong[]
        {
            34359739392uL
        });

        public static readonly BitSet _AS_in_impStmt397 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _fullId_in_impStmt399 = new BitSet(new ulong[]
        {
            34359738368uL
        });

        public static readonly BitSet _DELIM_in_impStmt403 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _fullId_in_impNameSpace432 = new BitSet(new ulong[]
        {
            137438953474uL
        });

        public static readonly BitSet _DOT_in_impNameSpace459 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _fullId_in_impNameSpace461 = new BitSet(new ulong[]
        {
            137438953474uL
        });

        public static readonly BitSet _VAR_in_varDeclStmt499 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _fullId_in_varDeclStmt501 = new BitSet(new ulong[]
        {
            34628177920uL
        });

        public static readonly BitSet _varTypeSpec_in_varDeclStmt503 = new BitSet(new ulong[]
        {
            34359742464uL
        });

        public static readonly BitSet _ASSIGN_OPS_in_varDeclStmt507 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _expression_in_varDeclStmt511 = new BitSet(new ulong[]
        {
            34359738368uL
        });

        public static readonly BitSet _DELIM_in_varDeclStmt516 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _COLON_in_varTypeSpec546 = new BitSet(new ulong[]
        {
            450943268869317728uL,
            91524532357uL
        });

        public static readonly BitSet _collectionClause_in_varTypeSpec549 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _COLLOF_in_collectionClause576 = new BitSet(new ulong[]
        {
            450943268869317730uL,
            91524532357uL
        });

        public static readonly BitSet _elementsClause_in_collectionClause578 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _elementsClause_in_collectionClause595 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _ELTSOF_in_elementsClause615 = new BitSet(new ulong[]
        {
            450943268869317728uL,
            91524532357uL
        });

        public static readonly BitSet _typeOfClause_in_elementsClause617 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _typeOfClause_in_elementsClause634 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _TYPOF_in_typeOfClause654 = new BitSet(new ulong[]
        {
            450943268869317728uL,
            91524532357uL
        });

        public static readonly BitSet _atomExpr_in_typeOfClause656 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _atomExpr_in_typeOfClause673 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _IF_in_ifStmt696 = new BitSet(new ulong[]
        {
            0uL,
            1uL
        });

        public static readonly BitSet _condition_in_ifStmt700 = new BitSet(new ulong[]
        {
            5062629287296705632uL,
            91524532357uL
        });

        public static readonly BitSet _stmtList_in_ifStmt702 = new BitSet(new ulong[]
        {
            2199023255554uL
        });

        public static readonly BitSet _elseIfClause_in_ifStmt714 = new BitSet(new ulong[]
        {
            2199023255554uL
        });

        public static readonly BitSet _elseClause_in_ifStmt724 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _ELSE_in_elseIfClause773 = new BitSet(new ulong[]
        {
            18014398509481984uL
        });

        public static readonly BitSet _IF_in_elseIfClause775 = new BitSet(new ulong[]
        {
            0uL,
            1uL
        });

        public static readonly BitSet _condition_in_elseIfClause779 = new BitSet(new ulong[]
        {
            5062629287296705632uL,
            91524532357uL
        });

        public static readonly BitSet _stmtList_in_elseIfClause781 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _ELSE_in_elseClause809 = new BitSet(new ulong[]
        {
            5062629287296705632uL,
            91524532357uL
        });

        public static readonly BitSet _stmtList_in_elseClause811 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _SWITCH_in_switchStmt843 = new BitSet(new ulong[]
        {
            0uL,
            1uL
        });

        public static readonly BitSet _condition_in_switchStmt847 = new BitSet(new ulong[]
        {
            4611686018427387904uL
        });

        public static readonly BitSet _LCURLB_in_switchStmt851 = new BitSet(new ulong[]
        {
            8598323200uL,
            262144uL
        });

        public static readonly BitSet _caseClause_in_switchStmt861 = new BitSet(new ulong[]
        {
            8598323200uL,
            262144uL
        });

        public static readonly BitSet _defaultClause_in_switchStmt873 = new BitSet(new ulong[]
        {
            0uL,
            262144uL
        });

        public static readonly BitSet _RCURLB_in_switchStmt879 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _CASE_in_caseClause917 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _caseValues_in_caseClause921 = new BitSet(new ulong[]
        {
            4611686018427387904uL
        });

        public static readonly BitSet _LCURLB_in_caseClause923 = new BitSet(new ulong[]
        {
            450943303229056096uL,
            91524794501uL
        });

        public static readonly BitSet _stmt_in_caseClause925 = new BitSet(new ulong[]
        {
            450943303229056096uL,
            91524794501uL
        });

        public static readonly BitSet _RCURLB_in_caseClause928 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _expression_in_caseValues955 = new BitSet(new ulong[]
        {
            1073741826uL
        });

        public static readonly BitSet _COMMA_in_caseValues958 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _expression_in_caseValues960 = new BitSet(new ulong[]
        {
            1073741826uL
        });

        public static readonly BitSet _DEFAULT_in_defaultClause984 = new BitSet(new ulong[]
        {
            5062629287296705632uL,
            91524532357uL
        });

        public static readonly BitSet _stmtList_in_defaultClause986 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _WHILE_in_whileStmt1016 = new BitSet(new ulong[]
        {
            0uL,
            1uL
        });

        public static readonly BitSet _condition_in_whileStmt1020 = new BitSet(new ulong[]
        {
            5062629287296705632uL,
            91524532357uL
        });

        public static readonly BitSet _stmtList_in_whileStmt1022 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _FOREACH_in_forEachStmt1056 = new BitSet(new ulong[]
        {
            0uL,
            1uL
        });

        public static readonly BitSet _LPAREN_in_forEachStmt1058 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _VAR_in_forEachStmt1075 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _fullId_in_forEachStmt1077 = new BitSet(new ulong[]
        {
            288230376151711744uL
        });

        public static readonly BitSet _INN_in_forEachStmt1079 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _expression_in_forEachStmt1081 = new BitSet(new ulong[]
        {
            0uL,
            1048576uL
        });

        public static readonly BitSet _RPAREN_in_forEachStmt1085 = new BitSet(new ulong[]
        {
            5062629287296705632uL,
            91524532357uL
        });

        public static readonly BitSet _stmtList_in_forEachStmt1087 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _fullId_in_forEachStmt1110 = new BitSet(new ulong[]
        {
            288230376151711744uL
        });

        public static readonly BitSet _INN_in_forEachStmt1112 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _expression_in_forEachStmt1114 = new BitSet(new ulong[]
        {
            0uL,
            1048576uL
        });

        public static readonly BitSet _RPAREN_in_forEachStmt1118 = new BitSet(new ulong[]
        {
            5062629287296705632uL,
            91524532357uL
        });

        public static readonly BitSet _stmtList_in_forEachStmt1120 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _LPAREN_in_condition1172 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _expression_in_condition1175 = new BitSet(new ulong[]
        {
            0uL,
            1048576uL
        });

        public static readonly BitSet _RPAREN_in_condition1177 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _TRY_in_tryBlock1191 = new BitSet(new ulong[]
        {
            5062629287296705632uL,
            91524532357uL
        });

        public static readonly BitSet _stmtList_in_tryBlock1193 = new BitSet(new ulong[]
        {
            33554434uL
        });

        public static readonly BitSet _catchBlock_in_tryBlock1195 = new BitSet(new ulong[]
        {
            33554434uL
        });

        public static readonly BitSet _CATCH_in_catchBlock1230 = new BitSet(new ulong[]
        {
            0uL,
            1uL
        });

        public static readonly BitSet _LPAREN_in_catchBlock1232 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _fullId_in_catchBlock1234 = new BitSet(new ulong[]
        {
            268435456uL,
            1048576uL
        });

        public static readonly BitSet _catchTypeSpec_in_catchBlock1236 = new BitSet(new ulong[]
        {
            0uL,
            1048576uL
        });

        public static readonly BitSet _RPAREN_in_catchBlock1238 = new BitSet(new ulong[]
        {
            5062629287296705632uL,
            91524532357uL
        });

        public static readonly BitSet _stmtList_in_catchBlock1240 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _COLON_in_catchTypeSpec1266 = new BitSet(new ulong[]
        {
            450943268869317728uL,
            91524532357uL
        });

        public static readonly BitSet _atomExpr_in_catchTypeSpec1269 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _CONTINUE_in_contStmt1294 = new BitSet(new ulong[]
        {
            34359738368uL
        });

        public static readonly BitSet _DELIM_in_contStmt1296 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _BREAK_in_breakStmt1312 = new BitSet(new ulong[]
        {
            34359738368uL
        });

        public static readonly BitSet _DELIM_in_breakStmt1314 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _RAISE_in_raiseStmt1334 = new BitSet(new ulong[]
        {
            450943268869317728uL,
            91524532357uL
        });

        public static readonly BitSet _atomExpr_in_raiseStmt1336 = new BitSet(new ulong[]
        {
            34359738368uL
        });

        public static readonly BitSet _DELIM_in_raiseStmt1338 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _dynPathId_in_methodOrAssignmentStmt1371 = new BitSet(new ulong[]
        {
            34359742464uL
        });

        public static readonly BitSet _DELIM_in_methodOrAssignmentStmt1379 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _ASSIGN_OPS_in_methodOrAssignmentStmt1411 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _expression_in_methodOrAssignmentStmt1413 = new BitSet(new ulong[]
        {
            34359738368uL
        });

        public static readonly BitSet _DELIM_in_methodOrAssignmentStmt1415 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _dynStrLitOrSubExprPath_in_methodOrAssignmentStmt1437 = new BitSet(new ulong[]
        {
            34359742464uL
        });

        public static readonly BitSet _DELIM_in_methodOrAssignmentStmt1445 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _ASSIGN_OPS_in_methodOrAssignmentStmt1479 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _expression_in_methodOrAssignmentStmt1481 = new BitSet(new ulong[]
        {
            34359738368uL
        });

        public static readonly BitSet _DELIM_in_methodOrAssignmentStmt1483 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _RETURN_in_returnStmt1517 = new BitSet(new ulong[]
        {
            450943303229056224uL,
            91532921477uL
        });

        public static readonly BitSet _expression_in_returnStmt1520 = new BitSet(new ulong[]
        {
            34359738368uL
        });

        public static readonly BitSet _DELIM_in_returnStmt1524 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _logicalORExpr_in_expression1550 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _logicalANDExpr_in_logicalORExpr1573 = new BitSet(new ulong[]
        {
            2uL,
            4096uL
        });

        public static readonly BitSet _OR_in_logicalORExpr1585 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _logicalANDExpr_in_logicalORExpr1587 = new BitSet(new ulong[]
        {
            2uL,
            4096uL
        });

        public static readonly BitSet _equalityExpr_in_logicalANDExpr1615 = new BitSet(new ulong[]
        {
            258uL
        });

        public static readonly BitSet _AND_in_logicalANDExpr1627 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _equalityExpr_in_logicalANDExpr1629 = new BitSet(new ulong[]
        {
            258uL
        });

        public static readonly BitSet _relationalExpr_in_equalityExpr1657 = new BitSet(new ulong[]
        {
            35184372088834uL,
            64uL
        });

        public static readonly BitSet _EQ_in_equalityExpr1674 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _relationalExpr_in_equalityExpr1676 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _NE_in_equalityExpr1698 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _relationalExpr_in_equalityExpr1700 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _addExpr_in_relationalExpr1731 = new BitSet(new ulong[]
        {
            9230127436295831554uL,
            2uL
        });

        public static readonly BitSet _GT_in_relationalExpr1748 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _addExpr_in_relationalExpr1751 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _LTT_in_relationalExpr1773 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _addExpr_in_relationalExpr1775 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _GE_in_relationalExpr1797 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _addExpr_in_relationalExpr1800 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _LE_in_relationalExpr1822 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _addExpr_in_relationalExpr1825 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _multExpr_in_addExpr1856 = new BitSet(new ulong[]
        {
            130uL,
            8388608uL
        });

        public static readonly BitSet _ADD_in_addExpr1873 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _multExpr_in_addExpr1875 = new BitSet(new ulong[]
        {
            130uL,
            8388608uL
        });

        public static readonly BitSet _SUB_in_addExpr1897 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _multExpr_in_addExpr1899 = new BitSet(new ulong[]
        {
            130uL,
            8388608uL
        });

        public static readonly BitSet _unaryExpr_in_multExpr1930 = new BitSet(new ulong[]
        {
            68719476738uL,
            48uL
        });

        public static readonly BitSet _MUL_in_multExpr1947 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _unaryExpr_in_multExpr1949 = new BitSet(new ulong[]
        {
            68719476738uL,
            48uL
        });

        public static readonly BitSet _DIV_in_multExpr1971 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _unaryExpr_in_multExpr1973 = new BitSet(new ulong[]
        {
            68719476738uL,
            48uL
        });

        public static readonly BitSet _MOD_in_multExpr1995 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _unaryExpr_in_multExpr1997 = new BitSet(new ulong[]
        {
            68719476738uL,
            48uL
        });

        public static readonly BitSet _NOT_in_unaryExpr2029 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _unaryExpr_in_unaryExpr2035 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _signPrefix_in_unaryExpr2051 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _unaryExpr_in_unaryExpr2053 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _atomExpr_in_unaryExpr2079 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _dynStrLitOrSubExprPath_in_atomExpr2097 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _numericLiteral_in_atomExpr2102 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _dynPathId_in_atomExpr2107 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _stringLiteral_in_dynStrLitOrSubExprPath2131 = new BitSet(new ulong[]
        {
            137438953474uL
        });

        public static readonly BitSet _idxExpr_in_dynStrLitOrSubExprPath2145 = new BitSet(new ulong[]
        {
            137438953474uL
        });

        public static readonly BitSet _DOT_in_dynStrLitOrSubExprPath2173 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _idxModObjId_in_dynStrLitOrSubExprPath2175 = new BitSet(new ulong[]
        {
            137438953474uL,
            1uL
        });

        public static readonly BitSet _LPAREN_in_dynStrLitOrSubExprPath2209 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91533970053uL
        });

        public static readonly BitSet _RPAREN_in_dynStrLitOrSubExprPath2219 = new BitSet(new ulong[]
        {
            137438953474uL
        });

        public static readonly BitSet _funcArgs_in_dynStrLitOrSubExprPath2261 = new BitSet(new ulong[]
        {
            0uL,
            1048576uL
        });

        public static readonly BitSet _RPAREN_in_dynStrLitOrSubExprPath2266 = new BitSet(new ulong[]
        {
            137438953474uL
        });

        public static readonly BitSet _idxModObjId_in_dynPathId2372 = new BitSet(new ulong[]
        {
            137438953474uL
        });

        public static readonly BitSet _DOT_in_dynPathId2390 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _idxModObjId_in_dynPathId2392 = new BitSet(new ulong[]
        {
            137438953474uL,
            1uL
        });

        public static readonly BitSet _LPAREN_in_dynPathId2426 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91533970053uL
        });

        public static readonly BitSet _RPAREN_in_dynPathId2436 = new BitSet(new ulong[]
        {
            137438953474uL
        });

        public static readonly BitSet _funcArgs_in_dynPathId2478 = new BitSet(new ulong[]
        {
            0uL,
            1048576uL
        });

        public static readonly BitSet _RPAREN_in_dynPathId2483 = new BitSet(new ulong[]
        {
            137438953474uL
        });

        public static readonly BitSet _expression_in_funcArgs2584 = new BitSet(new ulong[]
        {
            1073741826uL
        });

        public static readonly BitSet _COMMA_in_funcArgs2587 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _expression_in_funcArgs2589 = new BitSet(new ulong[]
        {
            1073741826uL
        });

        public static readonly BitSet _fullId_in_funcArgs2632 = new BitSet(new ulong[]
        {
            1152921504606846976uL
        });

        public static readonly BitSet _LAMBDA_OPS_in_funcArgs2636 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _expression_in_funcArgs2638 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _expression_in_idxArg2662 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _LPAREN_in_idxExpr2672 = new BitSet(new ulong[]
        {
            450943268869317856uL,
            91532921477uL
        });

        public static readonly BitSet _expression_in_idxExpr2674 = new BitSet(new ulong[]
        {
            0uL,
            1048576uL
        });

        public static readonly BitSet _RPAREN_in_idxExpr2676 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _modObjId_in_idxModObjId2695 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _fullId_in_modObjId2717 = new BitSet(new ulong[]
        {
            536870912uL
        });

        public static readonly BitSet _COLON2_in_modObjId2724 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _absoluteId_in_modObjId2726 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _absoluteId_in_modObjId2742 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _fullId_in_absoluteId2754 = new BitSet(new ulong[]
        {
            268435458uL
        });

        public static readonly BitSet _COLON_in_absoluteId2764 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _fullId_in_absoluteId2766 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _STRLIT_in_stringLiteral2793 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _brokenStrLit_in_stringLiteral2803 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _DECLIT_in_numericLiteral2814 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _NUMLIT_in_numericLiteral2819 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _brokenNumLit_in_numericLiteral2829 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _ELSE_in_unmatchedElse2842 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _CASE_in_unmatchedCase2859 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _DEFAULT_in_unmatchedDefault2876 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _STRLIT_in_unexpOrErrLiteral2893 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _DECLIT_in_unexpOrErrLiteral2904 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _NUMLIT_in_unexpOrErrLiteral2915 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _brokenStrLit_in_unexpOrErrLiteral2924 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _brokenNumLit_in_unexpOrErrLiteral2929 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _BROKEN_STRLIT_in_brokenStrLit2942 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _BROKEN_NUMLIT_in_brokenNumLit2960 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _modObjId_in_debugExpression2977 = new BitSet(new ulong[]
        {
            137438953472uL
        });

        public static readonly BitSet _DOT_in_debugExpression2989 = new BitSet(new ulong[]
        {
            450943264573957216uL,
            91520336004uL
        });

        public static readonly BitSet _modObjId_in_debugExpression2991 = new BitSet(new ulong[]
        {
            137438953472uL
        });

        public static readonly BitSet _EOF_in_debugExpression3013 = new BitSet(new ulong[]
        {
            2uL
        });

        public static readonly BitSet _impStmt_in_boModel3034 = new BitSet(new ulong[]
        {
            2449958197290074112uL
        });

        public static readonly BitSet _boDefinition_in_boModel3037;

        public static readonly BitSet _EOF_in_boModel3039;

        public static readonly BitSet _boAnnotation_in_boDefinition3057;

        public static readonly BitSet _BUSINESSOBJECT_in_boDefinition3060;

        public static readonly BitSet _boName_in_boDefinition3062;

        public static readonly BitSet _boRaises_in_boDefinition3064;

        public static readonly BitSet _LCURLB_in_boDefinition3067;

        public static readonly BitSet _boFeatures_in_boDefinition3069;

        public static readonly BitSet _RCURLB_in_boDefinition3072;

        public static readonly BitSet _RAISES_in_boRaises3103;

        public static readonly BitSet _boPath_in_boRaises3105;

        public static readonly BitSet _COMMA_in_boRaises3108;

        public static readonly BitSet _boPath_in_boRaises3110;

        public static readonly BitSet _boAnnotation_in_boFeatures3139;

        public static readonly BitSet _boMessage_in_boFeatures3163;

        public static readonly BitSet _boNode_in_boFeatures3189;

        public static readonly BitSet _boAssociation_in_boFeatures3208;

        public static readonly BitSet _boAction_in_boFeatures3230;

        public static readonly BitSet _boElement_in_boFeatures3258;

        public static readonly BitSet _MESSAGE_in_boMessage3309;

        public static readonly BitSet _fullId_in_boMessage3311;

        public static readonly BitSet _TEXTT_in_boMessage3313;

        public static readonly BitSet _stringLiteral_in_boMessage3315;

        public static readonly BitSet _COLON_in_boMessage3318;

        public static readonly BitSet _boName_in_boMessage3320;

        public static readonly BitSet _COMMA_in_boMessage3323;

        public static readonly BitSet _boName_in_boMessage3325;

        public static readonly BitSet _DELIM_in_boMessage3331;

        public static readonly BitSet _NODE_in_boNode3353;

        public static readonly BitSet _fullId_in_boNode3355;

        public static readonly BitSet _boCardinality_in_boNode3367;

        public static readonly BitSet _boRaises_in_boNode3370;

        public static readonly BitSet _LCURLB_in_boNode3373;

        public static readonly BitSet _boFeatures_in_boNode3375;

        public static readonly BitSet _RCURLB_in_boNode3378;

        public static readonly BitSet _DELIM_in_boNode3385;

        public static readonly BitSet _ASSOCIATION_in_boAssociation3420;

        public static readonly BitSet _fullId_in_boAssociation3424;

        public static readonly BitSet _boCardinality_in_boAssociation3426;

        public static readonly BitSet _TO_in_boAssociation3429;

        public static readonly BitSet _boName_in_boAssociation3431;

        public static readonly BitSet _USING_in_boAssociation3434;

        public static readonly BitSet _fullId_in_boAssociation3438;

        public static readonly BitSet _DELIM_in_boAssociation3442;

        public static readonly BitSet _ACTION_in_boAction3478;

        public static readonly BitSet _fullId_in_boAction3480;

        public static readonly BitSet _boRaises_in_boAction3482;

        public static readonly BitSet _DELIM_in_boAction3485;

        public static readonly BitSet _ELEMENT_in_boElement3511;

        public static readonly BitSet _fullId_in_boElement3513;

        public static readonly BitSet _boCardinality_in_boElement3515;

        public static readonly BitSet _COLON_in_boElement3519;

        public static readonly BitSet _boName_in_boElement3521;

        public static readonly BitSet _DELIM_in_boElement3525;

        public static readonly BitSet _boPath_in_boName3561;

        public static readonly BitSet _COLON_in_boName3584;

        public static readonly BitSet _boPath_in_boName3586;

        public static readonly BitSet _fullId_in_boPath3632;

        public static readonly BitSet _DOT_in_boPath3643;

        public static readonly BitSet _fullId_in_boPath3645;

        public static readonly BitSet _LBRCKT_in_boAnnotation3673;

        public static readonly BitSet _fullId_in_boAnnotation3675;

        public static readonly BitSet _LPAREN_in_boAnnotation3678;

        public static readonly BitSet _boAnnoArgs_in_boAnnotation3680;

        public static readonly BitSet _RPAREN_in_boAnnotation3683;

        public static readonly BitSet _RBRCKT_in_boAnnotation3688;

        public static readonly BitSet _boAnnoPar_in_boAnnoArgs3716;

        public static readonly BitSet _COMMA_in_boAnnoArgs3719;

        public static readonly BitSet _boAnnoPar_in_boAnnoArgs3721;

        public static readonly BitSet _LBRCKT_in_boCardinality3743;

        public static readonly BitSet _boDecIntLiteral_in_boCardinality3746;

        public static readonly BitSet _COMMA_in_boCardinality3753;

        public static readonly BitSet _boDecIntLiteral_in_boCardinality3759;

        public static readonly BitSet _CARD_N_in_boCardinality3836;

        public static readonly BitSet _RBRCKT_in_boCardinality3867;

        public static readonly BitSet _fullId_in_boAnnoPar3879;

        public static readonly BitSet _stringLiteral_in_boAnnoPar3886;

        public static readonly BitSet _boDecIntLiteral_in_boAnnoPar3893;

        public static readonly BitSet _DECLIT_in_boDecIntLiteral3904;

        public static readonly BitSet _signPrefix_in_boDecIntLiteral3909;

        public static readonly BitSet _DECLIT_in_boDecIntLiteral3911;

        public static readonly BitSet _simpleId_in_fullId3943;

        public static readonly BitSet _abslKeywords_in_fullId3950;

        public static readonly BitSet _boKeywords_in_fullId3955;

        public static readonly BitSet _ABSLID_in_simpleId3966;

        public static readonly BitSet _AS_in_abslKeywords3979;

        public static readonly BitSet _IMPORT_in_abslKeywords4000;

        public static readonly BitSet _IF_in_abslKeywords4017;

        public static readonly BitSet _ELSE_in_abslKeywords4038;

        public static readonly BitSet _SWITCH_in_abslKeywords4057;

        public static readonly BitSet _CASE_in_abslKeywords4074;

        public static readonly BitSet _DEFAULT_in_abslKeywords4093;

        public static readonly BitSet _WHILE_in_abslKeywords4109;

        public static readonly BitSet _FOREACH_in_abslKeywords4127;

        public static readonly BitSet _INN_in_abslKeywords4143;

        public static readonly BitSet _CONTINUE_in_abslKeywords4163;

        public static readonly BitSet _BREAK_in_abslKeywords4178;

        public static readonly BitSet _RAISE_in_abslKeywords4196;

        public static readonly BitSet _VAR_in_abslKeywords4214;

        public static readonly BitSet _COLLOF_in_abslKeywords4234;

        public static readonly BitSet _ELTSOF_in_abslKeywords4251;

        public static readonly BitSet _TYPOF_in_abslKeywords4268;

        public static readonly BitSet _RETURN_in_abslKeywords4286;

        public static readonly BitSet _TRY_in_abslKeywords4303;

        public static readonly BitSet _CATCH_in_abslKeywords4323;

        public static readonly BitSet _BUSINESSOBJECT_in_boKeywords4347;

        public static readonly BitSet _MESSAGE_in_boKeywords4357;

        public static readonly BitSet _NODE_in_boKeywords4374;

        public static readonly BitSet _ELEMENT_in_boKeywords4394;

        public static readonly BitSet _RAISES_in_boKeywords4411;

        public static readonly BitSet _ASSOCIATION_in_boKeywords4429;

        public static readonly BitSet _TO_in_boKeywords4442;

        public static readonly BitSet _ACTION_in_boKeywords4464;

        public static readonly BitSet _CARD_N_in_boKeywords4482;

        public static readonly BitSet _USING_in_boKeywords4500;

        public static readonly BitSet _TEXTT_in_boKeywords4519;

        public static readonly BitSet _COLLOF_in_synpred1_ByDScriptingLanguage568;

        public static readonly BitSet _elementsClause_in_synpred1_ByDScriptingLanguage570;

        public static readonly BitSet _ELTSOF_in_synpred2_ByDScriptingLanguage607;

        public static readonly BitSet _typeOfClause_in_synpred2_ByDScriptingLanguage609;

        public static readonly BitSet _TYPOF_in_synpred3_ByDScriptingLanguage646;

        public static readonly BitSet _fullId_in_synpred3_ByDScriptingLanguage648;

        public static readonly BitSet _ELSE_in_synpred4_ByDScriptingLanguage708;

        public static readonly BitSet _IF_in_synpred4_ByDScriptingLanguage710;

        public static readonly BitSet _CASE_in_synpred5_ByDScriptingLanguage857;

        public static readonly BitSet _DEFAULT_in_synpred6_ByDScriptingLanguage869;

        public static readonly BitSet _VAR_in_synpred7_ByDScriptingLanguage1067;

        public static readonly BitSet _fullId_in_synpred7_ByDScriptingLanguage1069;

        public static readonly BitSet _INN_in_synpred7_ByDScriptingLanguage1071;

        public static readonly BitSet _BROKEN_STRLIT_in_synpred8_ByDScriptingLanguage2799;

        public static readonly BitSet _BROKEN_NUMLIT_in_synpred9_ByDScriptingLanguage2825;

        static Follow()
        {
            // Note: this type is marked as 'beforefieldinit'.
            ulong[] bits = new ulong[1];
            ByDScriptingLanguageParser.Follow._boDefinition_in_boModel3037 = new BitSet(bits);
            ByDScriptingLanguageParser.Follow._EOF_in_boModel3039 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._boAnnotation_in_boDefinition3057 = new BitSet(new ulong[]
            {
                2305843009214218240uL
            });
            ByDScriptingLanguageParser.Follow._BUSINESSOBJECT_in_boDefinition3060 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._boName_in_boDefinition3062 = new BitSet(new ulong[]
            {
                4611686018427387904uL,
                65536uL
            });
            ByDScriptingLanguageParser.Follow._boRaises_in_boDefinition3064 = new BitSet(new ulong[]
            {
                4611686018427387904uL
            });
            ByDScriptingLanguageParser.Follow._LCURLB_in_boDefinition3067 = new BitSet(new ulong[]
            {
                2305843558969516096uL,
                262276uL
            });
            ByDScriptingLanguageParser.Follow._boFeatures_in_boDefinition3069 = new BitSet(new ulong[]
            {
                2305843558969516096uL,
                262276uL
            });
            ByDScriptingLanguageParser.Follow._RCURLB_in_boDefinition3072 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._RAISES_in_boRaises3103 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._boPath_in_boRaises3105 = new BitSet(new ulong[]
            {
                1073741826uL
            });
            ByDScriptingLanguageParser.Follow._COMMA_in_boRaises3108 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._boPath_in_boRaises3110 = new BitSet(new ulong[]
            {
                1073741826uL
            });
            ByDScriptingLanguageParser.Follow._boAnnotation_in_boFeatures3139 = new BitSet(new ulong[]
            {
                2305843558969516096uL,
                132uL
            });
            ByDScriptingLanguageParser.Follow._boMessage_in_boFeatures3163 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._boNode_in_boFeatures3189 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._boAssociation_in_boFeatures3208 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._boAction_in_boFeatures3230 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._boElement_in_boFeatures3258 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._MESSAGE_in_boMessage3309 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_boMessage3311 = new BitSet(new ulong[]
            {
                0uL,
                33554432uL
            });
            ByDScriptingLanguageParser.Follow._TEXTT_in_boMessage3313 = new BitSet(new ulong[]
            {
                262144uL,
                4194304uL
            });
            ByDScriptingLanguageParser.Follow._stringLiteral_in_boMessage3315 = new BitSet(new ulong[]
            {
                34628173824uL
            });
            ByDScriptingLanguageParser.Follow._COLON_in_boMessage3318 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._boName_in_boMessage3320 = new BitSet(new ulong[]
            {
                35433480192uL
            });
            ByDScriptingLanguageParser.Follow._COMMA_in_boMessage3323 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._boName_in_boMessage3325 = new BitSet(new ulong[]
            {
                35433480192uL
            });
            ByDScriptingLanguageParser.Follow._DELIM_in_boMessage3331 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._NODE_in_boNode3353 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_boNode3355 = new BitSet(new ulong[]
            {
                6917529062000820224uL,
                65536uL
            });
            ByDScriptingLanguageParser.Follow._boCardinality_in_boNode3367 = new BitSet(new ulong[]
            {
                4611686018427387904uL,
                65536uL
            });
            ByDScriptingLanguageParser.Follow._boRaises_in_boNode3370 = new BitSet(new ulong[]
            {
                4611686018427387904uL
            });
            ByDScriptingLanguageParser.Follow._LCURLB_in_boNode3373 = new BitSet(new ulong[]
            {
                2305843558969516096uL,
                262276uL
            });
            ByDScriptingLanguageParser.Follow._boFeatures_in_boNode3375 = new BitSet(new ulong[]
            {
                2305843558969516096uL,
                262276uL
            });
            ByDScriptingLanguageParser.Follow._RCURLB_in_boNode3378 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._DELIM_in_boNode3385 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._ASSOCIATION_in_boAssociation3420 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_boAssociation3424 = new BitSet(new ulong[]
            {
                2305843009213693952uL,
                67108864uL
            });
            ByDScriptingLanguageParser.Follow._boCardinality_in_boAssociation3426 = new BitSet(new ulong[]
            {
                0uL,
                67108864uL
            });
            ByDScriptingLanguageParser.Follow._TO_in_boAssociation3429 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._boName_in_boAssociation3431 = new BitSet(new ulong[]
            {
                34359738368uL,
                4294967296uL
            });
            ByDScriptingLanguageParser.Follow._USING_in_boAssociation3434 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_boAssociation3438 = new BitSet(new ulong[]
            {
                34359738368uL
            });
            ByDScriptingLanguageParser.Follow._DELIM_in_boAssociation3442 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._ACTION_in_boAction3478 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_boAction3480 = new BitSet(new ulong[]
            {
                34359738368uL,
                65536uL
            });
            ByDScriptingLanguageParser.Follow._boRaises_in_boAction3482 = new BitSet(new ulong[]
            {
                34359738368uL
            });
            ByDScriptingLanguageParser.Follow._DELIM_in_boAction3485 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._ELEMENT_in_boElement3511 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_boElement3513 = new BitSet(new ulong[]
            {
                2305843043841867776uL
            });
            ByDScriptingLanguageParser.Follow._boCardinality_in_boElement3515 = new BitSet(new ulong[]
            {
                34628173824uL
            });
            ByDScriptingLanguageParser.Follow._COLON_in_boElement3519 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._boName_in_boElement3521 = new BitSet(new ulong[]
            {
                34359738368uL
            });
            ByDScriptingLanguageParser.Follow._DELIM_in_boElement3525 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._boPath_in_boName3561 = new BitSet(new ulong[]
            {
                268435458uL
            });
            ByDScriptingLanguageParser.Follow._COLON_in_boName3584 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._boPath_in_boName3586 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_boPath3632 = new BitSet(new ulong[]
            {
                137438953474uL
            });
            ByDScriptingLanguageParser.Follow._DOT_in_boPath3643 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_boPath3645 = new BitSet(new ulong[]
            {
                137438953474uL
            });
            ByDScriptingLanguageParser.Follow._LBRCKT_in_boAnnotation3673 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_boAnnotation3675 = new BitSet(new ulong[]
            {
                0uL,
                131073uL
            });
            ByDScriptingLanguageParser.Follow._LPAREN_in_boAnnotation3678 = new BitSet(new ulong[]
            {
                450943268869186784uL,
                91532918916uL
            });
            ByDScriptingLanguageParser.Follow._boAnnoArgs_in_boAnnotation3680 = new BitSet(new ulong[]
            {
                0uL,
                1048576uL
            });
            ByDScriptingLanguageParser.Follow._RPAREN_in_boAnnotation3683 = new BitSet(new ulong[]
            {
                0uL,
                131072uL
            });
            ByDScriptingLanguageParser.Follow._RBRCKT_in_boAnnotation3688 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._boAnnoPar_in_boAnnoArgs3716 = new BitSet(new ulong[]
            {
                1073741826uL
            });
            ByDScriptingLanguageParser.Follow._COMMA_in_boAnnoArgs3719 = new BitSet(new ulong[]
            {
                450943268869186784uL,
                91532918916uL
            });
            ByDScriptingLanguageParser.Follow._boAnnoPar_in_boAnnoArgs3721 = new BitSet(new ulong[]
            {
                1073741826uL
            });
            ByDScriptingLanguageParser.Follow._LBRCKT_in_boCardinality3743 = new BitSet(new ulong[]
            {
                4294967424uL,
                8388608uL
            });
            ByDScriptingLanguageParser.Follow._boDecIntLiteral_in_boCardinality3746 = new BitSet(new ulong[]
            {
                1073741824uL
            });
            ByDScriptingLanguageParser.Follow._COMMA_in_boCardinality3753 = new BitSet(new ulong[]
            {
                4299161728uL,
                8388608uL
            });
            ByDScriptingLanguageParser.Follow._boDecIntLiteral_in_boCardinality3759 = new BitSet(new ulong[]
            {
                0uL,
                131072uL
            });
            ByDScriptingLanguageParser.Follow._CARD_N_in_boCardinality3836 = new BitSet(new ulong[]
            {
                0uL,
                131072uL
            });
            ByDScriptingLanguageParser.Follow._RBRCKT_in_boCardinality3867 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_boAnnoPar3879 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._stringLiteral_in_boAnnoPar3886 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._boDecIntLiteral_in_boAnnoPar3893 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._DECLIT_in_boDecIntLiteral3904 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._signPrefix_in_boDecIntLiteral3909 = new BitSet(new ulong[]
            {
                4294967296uL
            });
            ByDScriptingLanguageParser.Follow._DECLIT_in_boDecIntLiteral3911 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._simpleId_in_fullId3943 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._abslKeywords_in_fullId3950 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._boKeywords_in_fullId3955 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._ABSLID_in_simpleId3966 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._AS_in_abslKeywords3979 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._IMPORT_in_abslKeywords4000 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._IF_in_abslKeywords4017 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._ELSE_in_abslKeywords4038 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._SWITCH_in_abslKeywords4057 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._CASE_in_abslKeywords4074 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._DEFAULT_in_abslKeywords4093 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._WHILE_in_abslKeywords4109 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._FOREACH_in_abslKeywords4127 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._INN_in_abslKeywords4143 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._CONTINUE_in_abslKeywords4163 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._BREAK_in_abslKeywords4178 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._RAISE_in_abslKeywords4196 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._VAR_in_abslKeywords4214 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._COLLOF_in_abslKeywords4234 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._ELTSOF_in_abslKeywords4251 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._TYPOF_in_abslKeywords4268 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._RETURN_in_abslKeywords4286 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._TRY_in_abslKeywords4303 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._CATCH_in_abslKeywords4323 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._BUSINESSOBJECT_in_boKeywords4347 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._MESSAGE_in_boKeywords4357 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._NODE_in_boKeywords4374 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._ELEMENT_in_boKeywords4394 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._RAISES_in_boKeywords4411 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._ASSOCIATION_in_boKeywords4429 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._TO_in_boKeywords4442 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._ACTION_in_boKeywords4464 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._CARD_N_in_boKeywords4482 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._USING_in_boKeywords4500 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._TEXTT_in_boKeywords4519 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._COLLOF_in_synpred1_ByDScriptingLanguage568 = new BitSet(new ulong[]
            {
                450943268869317728uL,
                91524532357uL
            });
            ByDScriptingLanguageParser.Follow._elementsClause_in_synpred1_ByDScriptingLanguage570 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._ELTSOF_in_synpred2_ByDScriptingLanguage607 = new BitSet(new ulong[]
            {
                450943268869317728uL,
                91524532357uL
            });
            ByDScriptingLanguageParser.Follow._typeOfClause_in_synpred2_ByDScriptingLanguage609 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._TYPOF_in_synpred3_ByDScriptingLanguage646 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_synpred3_ByDScriptingLanguage648 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._ELSE_in_synpred4_ByDScriptingLanguage708 = new BitSet(new ulong[]
            {
                18014398509481984uL
            });
            ByDScriptingLanguageParser.Follow._IF_in_synpred4_ByDScriptingLanguage710 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._CASE_in_synpred5_ByDScriptingLanguage857 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._DEFAULT_in_synpred6_ByDScriptingLanguage869 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._VAR_in_synpred7_ByDScriptingLanguage1067 = new BitSet(new ulong[]
            {
                450943264573957216uL,
                91520336004uL
            });
            ByDScriptingLanguageParser.Follow._fullId_in_synpred7_ByDScriptingLanguage1069 = new BitSet(new ulong[]
            {
                288230376151711744uL
            });
            ByDScriptingLanguageParser.Follow._INN_in_synpred7_ByDScriptingLanguage1071 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._BROKEN_STRLIT_in_synpred8_ByDScriptingLanguage2799 = new BitSet(new ulong[]
            {
                2uL
            });
            ByDScriptingLanguageParser.Follow._BROKEN_NUMLIT_in_synpred9_ByDScriptingLanguage2825 = new BitSet(new ulong[]
            {
                2uL
            });
        }
    }

    public const int EOF = -1;

    public const int ABSID = 4;

    public const int ABSLID = 5;

    public const int ACTION = 6;

    public const int ADD = 7;

    public const int AND = 8;

    public const int ANNOTATION = 9;

    public const int AS = 10;

    public const int ASSIGNMENT = 11;

    public const int ASSIGN_OPS = 12;

    public const int ASSOCIATION = 13;

    public const int BINARY = 14;

    public const int BONAME = 15;

    public const int BREAK = 16;

    public const int BROKEN_NUMLIT = 17;

    public const int BROKEN_STRLIT = 18;

    public const int BUSINESSOBJECT = 19;

    public const int CALL = 20;

    public const int CARDINALITY = 21;

    public const int CARD_N = 22;

    public const int CASE = 23;

    public const int CASECLAUSE = 24;

    public const int CATCH = 25;

    public const int COLLECTIONOF = 26;

    public const int COLLOF = 27;

    public const int COLON = 28;

    public const int COLON2 = 29;

    public const int COMMA = 30;

    public const int CONTINUE = 31;

    public const int DECLIT = 32;

    public const int DEFAULT = 33;

    public const int DEFAULTCLAUSE = 34;

    public const int DELIM = 35;

    public const int DIV = 36;

    public const int DOT = 37;

    public const int DoubleStringCharacter = 38;

    public const int ELEMENT = 39;

    public const int ELEMENTSOF = 40;

    public const int ELSE = 41;

    public const int ELSECLAUSE = 42;

    public const int ELSEIFCLAUSE = 43;

    public const int ELTSOF = 44;

    public const int EQ = 45;

    public const int EXEC = 46;

    public const int EscapeCharacter = 47;

    public const int ExponentPart = 48;

    public const int FOREACH = 49;

    public const int FOREACHDECL = 50;

    public const int GE = 51;

    public const int GT = 52;

    public const int IDX = 53;

    public const int IF = 54;

    public const int IFCLAUSE = 55;

    public const int ILLEGAL_OPERATOR = 56;

    public const int IMPORT = 57;

    public const int INN = 58;

    public const int LAMBDA = 59;

    public const int LAMBDA_OPS = 60;

    public const int LBRCKT = 61;

    public const int LCURLB = 62;

    public const int LE = 63;

    public const int LPAREN = 64;

    public const int LTT = 65;

    public const int MESSAGE = 66;

    public const int ML_COMMENT = 67;

    public const int MOD = 68;

    public const int MUL = 69;

    public const int NE = 70;

    public const int NODE = 71;

    public const int NONE = 72;

    public const int NOT = 73;

    public const int NSPACE = 74;

    public const int NUMLIT = 75;

    public const int OR = 76;

    public const int PATH = 77;

    public const int QUALID = 78;

    public const int RAISE = 79;

    public const int RAISES = 80;

    public const int RBRCKT = 81;

    public const int RCURLB = 82;

    public const int RETURN = 83;

    public const int RPAREN = 84;

    public const int SL_COMMENT = 85;

    public const int STRLIT = 86;

    public const int SUB = 87;

    public const int SWITCH = 88;

    public const int TEXTT = 89;

    public const int TO = 90;

    public const int TRY = 91;

    public const int TRYBLOCK = 92;

    public const int TYPEOF = 93;

    public const int TYPOF = 94;

    public const int UNARY = 95;

    public const int USING = 96;

    public const int VALUES = 97;

    public const int VAR = 98;

    public const int VARDECL = 99;

    public const int WHILE = 100;

    public const int WS = 101;

    private CodeCompletionContext CompletionContext;

    private int[] NextPossibleTokenTypes = new int[0];

    private int[] PrevNextPossibleTokenTypes = new int[0];

    private bool InPath;

    private bool Done;

    internal static readonly string[] tokenNames = new string[]
    {
        "<invalid>",
        "<EOR>",
        "<DOWN>",
        "<UP>",
        "ABSID",
        "ABSLID",
        "ACTION",
        "ADD",
        "AND",
        "ANNOTATION",
        "AS",
        "ASSIGNMENT",
        "ASSIGN_OPS",
        "ASSOCIATION",
        "BINARY",
        "BONAME",
        "BREAK",
        "BROKEN_NUMLIT",
        "BROKEN_STRLIT",
        "BUSINESSOBJECT",
        "CALL",
        "CARDINALITY",
        "CARD_N",
        "CASE",
        "CASECLAUSE",
        "CATCH",
        "COLLECTIONOF",
        "COLLOF",
        "COLON",
        "COLON2",
        "COMMA",
        "CONTINUE",
        "DECLIT",
        "DEFAULT",
        "DEFAULTCLAUSE",
        "DELIM",
        "DIV",
        "DOT",
        "DoubleStringCharacter",
        "ELEMENT",
        "ELEMENTSOF",
        "ELSE",
        "ELSECLAUSE",
        "ELSEIFCLAUSE",
        "ELTSOF",
        "EQ",
        "EXEC",
        "EscapeCharacter",
        "ExponentPart",
        "FOREACH",
        "FOREACHDECL",
        "GE",
        "GT",
        "IDX",
        "IF",
        "IFCLAUSE",
        "ILLEGAL_OPERATOR",
        "IMPORT",
        "INN",
        "LAMBDA",
        "LAMBDA_OPS",
        "LBRCKT",
        "LCURLB",
        "LE",
        "LPAREN",
        "LTT",
        "MESSAGE",
        "ML_COMMENT",
        "MOD",
        "MUL",
        "NE",
        "NODE",
        "NONE",
        "NOT",
        "NSPACE",
        "NUMLIT",
        "OR",
        "PATH",
        "QUALID",
        "RAISE",
        "RAISES",
        "RBRCKT",
        "RCURLB",
        "RETURN",
        "RPAREN",
        "SL_COMMENT",
        "STRLIT",
        "SUB",
        "SWITCH",
        "TEXTT",
        "TO",
        "TRY",
        "TRYBLOCK",
        "TYPEOF",
        "TYPOF",
        "UNARY",
        "USING",
        "VALUES",
        "VAR",
        "VARDECL",
        "WHILE",
        "WS"
    };

    private static readonly bool[] decisionCanBacktrack = new bool[0];

    private ITreeAdaptor adaptor;

    private IToken CurrentToken
    {
        get;
        set;
    }

    private IToken PrevToken
    {
        get;
        set;
    }

    public ITreeAdaptor TreeAdaptor
    {
        get
        {
            return this.adaptor;
        }
        set
        {
            this.adaptor = value;
        }
    }

    public override string[] TokenNames
    {
        get
        {
            return ByDScriptingLanguageParser.tokenNames;
        }
    }

    public override string GrammarFileName
    {
        get
        {
            return "C:\\P4\\LeanStack\\BYDSTUDIO\\lsfrontend_LE_stream\\src\\_copernicus\\sln\\ASLanguage\\Grammar\\ByDScriptingLanguage.g";
        }
    }

    public ByDScriptingLanguageParser(ByDScriptingLanguageLexer lexer, CodeCompletionContext ccCtx) : this(new CommonTokenStream(lexer))
    {
        this.CompletionContext = ccCtx;
        BitSet impStmt_in_program = ByDScriptingLanguageParser.Follow._impStmt_in_program168;
        impStmt_in_program.Add(57);
        this.NextPossibleTokenTypes = impStmt_in_program.ToArray();
        this.TreeAdaptor = new CommonTreeAdaptor();
    }

    private void Retype(IToken token, int newTokenType)
    {
        token.Type = newTokenType;
    }

    public override object Match(IIntStream input, int ttype, BitSet follow)
    {
        IToken token = base.Match(input, ttype, follow) as IToken;
        if (this.CompletionContext.ContextMode == CodeCompletionContextMode.CodeCompletion)
        {
            this.PrevNextPossibleTokenTypes = this.NextPossibleTokenTypes;
            this.NextPossibleTokenTypes = this.DetermineNextPossibleTokenTypes(follow);
            this.PrevToken = this.CurrentToken;
            this.CurrentToken = token;
            this.HandleCodeCompletion();
        }
        else if (this.CompletionContext.ContextMode == CodeCompletionContextMode.Tagging && token.Type != -1)
        {
            this.CompletionContext.AddTokenForTagging(token);
        }
        return token;
    }

    private void HandleCodeCompletion()
    {
        CursorPosition cursor = this.CompletionContext.Cursor;
        RelativeCursorLocation relativeCursorLocation = cursor.DetermineLocationRelativeToToken(this.CurrentToken);
        if ((this.PrevToken != null && this.PrevToken.Type != 37 && this.PrevToken.Type != 5) || (TokenHelper.PathClosingTokens.Contains(this.CurrentToken.Type) && relativeCursorLocation != RelativeCursorLocation.RightBefore))
        {
            this.InPath = false;
        }
        if (this.CurrentToken.Type == 37)
        {
            this.InPath = true;
        }
        if (relativeCursorLocation == RelativeCursorLocation.OneCharBehind || relativeCursorLocation == RelativeCursorLocation.MoreThanOneCharBehind)
        {
            IToken token = this.input.LT(1);
            RelativeCursorLocation relativeCursorLocation2 = cursor.DetermineLocationRelativeToToken(token);
            if (this.PrevToken == null || (token.Type != -1 && relativeCursorLocation2 != RelativeCursorLocation.Before && relativeCursorLocation2 != RelativeCursorLocation.RightBefore))
            {
                return;
            }
            relativeCursorLocation = RelativeCursorLocation.OneCharBehind;
        }
        this.HandleMethodTipCase(relativeCursorLocation);
        this.HandlePathCase(relativeCursorLocation);
        if (!this.InPath)
        {
            this.HandleTopLevelCase(relativeCursorLocation);
        }
        if (relativeCursorLocation == RelativeCursorLocation.Before || relativeCursorLocation == RelativeCursorLocation.RightBefore)
        {
            this.MarkAsModeAndThrowCCException(CodeCompletionMode.None, null, RelativeCursorLocation.Unspecified);
        }
    }

    private void HandleMethodTipCase(RelativeCursorLocation relativeLocation)
    {
        if (this.CompletionContext.CCMode != CodeCompletionMode.MethodTip)
        {
            return;
        }
        this.MarkAsModeAndThrowCCException(this.CompletionContext.CCMode, null, RelativeCursorLocation.Unspecified);
    }

    private void HandlePathCase(RelativeCursorLocation relativeLocation)
    {
        if (this.CurrentToken.Type == 57 && relativeLocation == RelativeCursorLocation.OneCharBehind)
        {
            this.MarkAsModeAndThrowCCException(CodeCompletionMode.Import, null, RelativeCursorLocation.Unspecified);
        }
        if (this.PrevToken == null)
        {
            return;
        }
        bool flag = this.CurrentToken.Type == 37 && (relativeLocation == RelativeCursorLocation.RightBehind || relativeLocation == RelativeCursorLocation.OneCharBehind);
        if (flag || this.PrevToken.Type == 37)
        {
            bool flag2 = false;
            if (!flag && this.CurrentToken.Type == 5)
            {
                flag2 = (relativeLocation == RelativeCursorLocation.Within || relativeLocation == RelativeCursorLocation.RightBehind);
            }
            if (flag || flag2)
            {
                CodeCompletionMode mode = TokenStreamHelper.IsTokenWithin(base.TokenStream, this.CurrentToken, 57) ? CodeCompletionMode.Import : CodeCompletionMode.Path;
                this.MarkAsModeAndThrowCCException(mode, null, RelativeCursorLocation.Unspecified);
                return;
            }
        }
        else if (this.PrevToken.Type == 57)
        {
            this.MarkAsModeAndThrowCCException(CodeCompletionMode.Import, null, RelativeCursorLocation.Unspecified);
        }
    }

    private void HandleTopLevelCase(RelativeCursorLocation relativeLocation)
    {
        if ((relativeLocation == RelativeCursorLocation.OneCharBehind || relativeLocation == RelativeCursorLocation.RightBehind) && this.NextPossibleTokenTypes.Contains(5))
        {
            if (!this.PrevNextPossibleTokenTypes.Contains(5) && TokenHelper.KeywordTokenTypes.Contains(this.CurrentToken.Type) && relativeLocation == RelativeCursorLocation.RightBehind)
            {
                this.MarkAsModeAndThrowCCException(CodeCompletionMode.None, this.PrevNextPossibleTokenTypes, relativeLocation);
                return;
            }
            this.MarkAsModeAndThrowCCException(CodeCompletionMode.TopLevel, this.NextPossibleTokenTypes, relativeLocation);
            return;
        }
        else
        {
            if ((this.PrevToken == null || this.CurrentToken.Type == -1 || relativeLocation == RelativeCursorLocation.RightBehind || relativeLocation == RelativeCursorLocation.Within || relativeLocation == RelativeCursorLocation.RightBefore) && this.PrevNextPossibleTokenTypes.Contains(5))
            {
                this.MarkAsModeAndThrowCCException(CodeCompletionMode.TopLevel, this.PrevNextPossibleTokenTypes, relativeLocation);
                return;
            }
            if ((relativeLocation == RelativeCursorLocation.Within || relativeLocation == RelativeCursorLocation.RightBefore) && (TokenHelper.KeywordTokenTypes.Contains(this.CurrentToken.Type) || TokenHelper.PossibleTokensContainKeywords(this.PrevNextPossibleTokenTypes)))
            {
                this.MarkAsModeAndThrowCCException(CodeCompletionMode.None, this.PrevNextPossibleTokenTypes, relativeLocation);
            }
            return;
        }
    }

    private CodeCompletionContext.SourceModificationMode DetermineSourceModificationMode(RelativeCursorLocation relativeLocation)
    {
        IList<string> ruleInvocationStack = this.GetRuleInvocationStack();
        int num = ruleInvocationStack.Count<string>();
        for (int i = num - 1; i >= 1; i--)
        {
            if (ruleInvocationStack[i] == "condition" && ruleInvocationStack[i - 1] == "switchStmt")
            {
                return CodeCompletionContext.SourceModificationMode.SwitchValue;
            }
            if (ruleInvocationStack[i] == "forEachStmt")
            {
                if ((this.CurrentToken.Type == 58 && relativeLocation == RelativeCursorLocation.RightBefore) || (this.CurrentToken.Type != 84 && this.CurrentToken.Type != 58))
                {
                    return CodeCompletionContext.SourceModificationMode.ForeachVariable;
                }
                if ((this.CurrentToken.Type == 84 && relativeLocation == RelativeCursorLocation.RightBefore) || (this.CurrentToken.Type == 58 && relativeLocation != RelativeCursorLocation.RightBefore))
                {
                    return CodeCompletionContext.SourceModificationMode.ForeachAfterIn;
                }
            }
            else if (ruleInvocationStack[i - 1] == "forEachStmt")
            {
                if (ruleInvocationStack[i] == "expression")
                {
                    return CodeCompletionContext.SourceModificationMode.ForeachAfterIn;
                }
                if (ruleInvocationStack[i] == "stmtList")
                {
                    return CodeCompletionContext.SourceModificationMode.None;
                }
            }
        }
        return CodeCompletionContext.SourceModificationMode.None;
    }

    private void MarkAsModeAndThrowCCException(CodeCompletionMode mode, int[] followSet = null, RelativeCursorLocation relativeLocation = RelativeCursorLocation.Unspecified)
    {
        this.CompletionContext.CCMode = mode;
        this.CompletionContext.SourceModification = this.DetermineSourceModificationMode(relativeLocation);
        if (this.CurrentToken.Type != -1)
        {
            CursorPosition cursor = this.CompletionContext.Cursor;
            if (this.CurrentToken.Type == 5 || TokenHelper.KeywordTokenTypes.Contains(this.CurrentToken.Type))
            {
                this.CompletionContext.PartialTokenText = cursor.GetPartialToken(this.CurrentToken, true);
            }
            if (mode == CodeCompletionMode.TopLevel && !string.IsNullOrEmpty(this.CompletionContext.PartialTokenText))
            {
                this.CompletionContext.Cursor.Column -= this.CompletionContext.PartialTokenText.Length;
            }
        }
        if ((mode == CodeCompletionMode.TopLevel || mode == CodeCompletionMode.None) && followSet != null)
        {
            this.CompletionContext.AllowedTokenTypes = followSet;
        }
        throw new CodeCompletionMatchException();
    }

    private int[] DetermineNextPossibleTokenTypes(BitSet follow)
    {
        int[] result = new int[0];
        if (!this.state.failed && !this.state.errorRecovery)
        {
            BitSet bitSet = new BitSet();
            if (follow.Member(1) || this.state.backtracking != 0)
            {
                if (this.state.backtracking != 0)
                {
                    bitSet = follow;
                }
                for (int i = this.state._fsp; i >= 0; i--)
                {
                    bitSet.OrInPlace(this.state.following[i]);
                    if (!bitSet.Member(1))
                    {
                        break;
                    }
                    bitSet.Remove(1);
                }
            }
            else
            {
                bitSet = follow;
            }
            result = bitSet.ToArray();
        }
        return result;
    }

    private void CustomSync()
    {
        BitSet follow = this.ComputeContextSensitiveRuleFOLLOW();
        this.CustomSync(follow);
    }

    private void CustomSync(BitSet follow)
    {
        int num = -1;
        try
        {
            num = this.input.Mark();
            while (!follow.Member(this.input.LA(1)))
            {
                if (this.input.LA(1) == -1)
                {
                    this.input.Rewind();
                    num = -1;
                    break;
                }
                this.input.Consume();
            }
        }
        catch (Exception)
        {
        }
        finally
        {
            if (num != -1)
            {
                this.input.Release(num);
            }
        }
    }

    private void collect_breakpoint_span(IToken startToken, IToken stopToken)
    {
        if (this.CompletionContext.ContextMode != CodeCompletionContextMode.Tagging || startToken == null || stopToken == null)
        {
            return;
        }
        this.CompletionContext.AddBreakpointSpan(startToken.Line, startToken.CharPositionInLine, stopToken.Line, stopToken.CharPositionInLine + stopToken.Text.Length);
    }

    private void collect_tooltip_identifier(IToken startToken, IToken stopToken)
    {
        if (this.CompletionContext.ContextMode != CodeCompletionContextMode.Tooltip || startToken == null || stopToken == null)
        {
            return;
        }
        if (this.input.LA(1) == 64)
        {
            return;
        }
        TextSpan textSpan = new TextSpan
        {
            iStartLine = startToken.Line - 1,
            iStartIndex = startToken.CharPositionInLine,
            iEndLine = stopToken.Line - 1,
            iEndIndex = stopToken.CharPositionInLine + stopToken.Text.Length
        };
        if (TextSpanHelper.Contains(textSpan, this.CompletionContext.Cursor.Line, this.CompletionContext.Cursor.Column))
        {
            this.CompletionContext.AddTooltipIdentifier(textSpan);
        }
    }

    private void collect_tooltip_path(IToken startToken, IToken stopToken)
    {
        if (this.CompletionContext.ContextMode != CodeCompletionContextMode.Tooltip || startToken == null || stopToken == null)
        {
            return;
        }
        TextSpan textSpan = new TextSpan
        {
            iStartLine = startToken.Line - 1,
            iStartIndex = startToken.CharPositionInLine,
            iEndLine = stopToken.Line - 1,
            iEndIndex = stopToken.CharPositionInLine + stopToken.Text.Length
        };
        if (TextSpanHelper.Contains(textSpan, this.CompletionContext.Cursor.Line, this.CompletionContext.Cursor.Column))
        {
            this.CompletionContext.AddTooltipPath(textSpan);
        }
    }

    public ByDScriptingLanguageParser(ITokenStream input) : this(input, new RecognizerSharedState())
    {
    }

    public ByDScriptingLanguageParser(ITokenStream input, RecognizerSharedState state) : base(input, state)
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void OnCreated()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule(string ruleName, int ruleIndex)
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule(string ruleName, int ruleIndex)
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_program()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_program()
    {
    }

    [GrammarRule("program")]
    public AstParserRuleReturnScope<CommonTree, IToken> program()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token EOF");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule sync");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule impStmt");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule stmt");
        try
        {
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 57)
                {
                    this.input.LA(2);
                    if (this.input.LA(2) != 29 && this.input.LA(2) != 28 && this.input.LA(2) != 37 && this.input.LA(2) != 12)
                    {
                        num = 1;
                    }
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_12E;
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._impStmt_in_program168);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.impStmt();
                base.PopFollow();
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope2.Tree);
                }
            }
            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
            return result;
            IL_12E:
            base.PushFollow(ByDScriptingLanguageParser.Follow._sync_in_program171);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.sync();
            base.PopFollow();
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
            }
            while (true)
            {
                int num4 = 2;
                int num5 = this.input.LA(1);
                if ((num5 >= 5 && num5 <= 6) || (num5 == 10 || num5 == 13 || (num5 >= 16 && num5 <= 19)) || (num5 >= 22 && num5 <= 23) || (num5 == 25 || num5 == 27 || (num5 >= 31 && num5 <= 33)) || (num5 == 35 || num5 == 39 || num5 == 41 || num5 == 44 || num5 == 49 || num5 == 54 || (num5 >= 57 && num5 <= 58)) || (num5 == 64 || num5 == 66 || num5 == 71 || num5 == 75 || (num5 >= 79 && num5 <= 80)) || (num5 == 83 || num5 == 86 || (num5 >= 88 && num5 <= 91)) || num5 == 94 || num5 == 96 || num5 == 98 || num5 == 100)
                {
                    num4 = 1;
                }
                int num6 = num4;
                if (num6 != 1)
                {
                    goto IL_313;
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._stmt_in_program174);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.stmt();
                base.PopFollow();
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope4.Tree);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._sync_in_program176);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.sync();
                base.PopFollow();
                if (this.state.failed)
                {
                    goto Block_41;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope5.Tree);
                }
            }
            result = astParserRuleReturnScope;
            return result;
            Block_41:
            result = astParserRuleReturnScope;
            return result;
            IL_313:
            IToken el = (IToken)this.Match(this.input, -1, ByDScriptingLanguageParser.Follow._EOF_in_program180);
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                while (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                while (rewriteRuleSubtreeStream3.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream3.NextTree());
                }
                rewriteRuleSubtreeStream3.Reset();
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_sync()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_sync()
    {
    }

    [GrammarRule("sync")]
    private AstParserRuleReturnScope<CommonTree, IToken> sync()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        this.CustomSync();
        CommonTree root = (CommonTree)this.adaptor.Nil();
        astParserRuleReturnScope.Stop = this.input.LT(-1);
        if (this.state.backtracking == 0)
        {
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(root);
            this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_stmtList()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_stmtList()
    {
    }

    [GrammarRule("stmtList")]
    private AstParserRuleReturnScope<CommonTree, IToken> stmtList()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token RCURLB");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token LCURLB");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule stmt");
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num == 62)
            {
                num2 = 1;
            }
            else if ((num >= 5 && num <= 6) || (num == 10 || num == 13 || (num >= 16 && num <= 19)) || (num >= 22 && num <= 23) || (num == 25 || num == 27 || (num >= 31 && num <= 33)) || (num == 39 || num == 41 || num == 44 || num == 49 || num == 54 || (num >= 57 && num <= 58)) || (num == 64 || num == 66 || num == 71 || num == 75 || (num >= 79 && num <= 80)) || (num == 83 || num == 86 || (num >= 88 && num <= 91)) || num == 94 || num == 96 || num == 98 || num == 100)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 5, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        IToken el = (IToken)this.Match(this.input, 62, ByDScriptingLanguageParser.Follow._LCURLB_in_stmtList223);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream2.Add(el);
                        }
                        int num3 = this.input.LA(1);
                        int num4;
                        if ((num3 >= 5 && num3 <= 6) || (num3 == 10 || num3 == 13 || (num3 >= 16 && num3 <= 19)) || (num3 >= 22 && num3 <= 23) || (num3 == 25 || num3 == 27 || (num3 >= 31 && num3 <= 33)) || (num3 == 35 || num3 == 39 || num3 == 41 || num3 == 44 || num3 == 49 || num3 == 54 || (num3 >= 57 && num3 <= 58)) || (num3 == 64 || num3 == 66 || num3 == 71 || num3 == 75 || (num3 >= 79 && num3 <= 80)) || (num3 == 83 || num3 == 86 || (num3 >= 88 && num3 <= 91)) || num3 == 94 || num3 == 96 || num3 == 98 || num3 == 100)
                        {
                            num4 = 1;
                        }
                        else if (num3 == 82)
                        {
                            num4 = 2;
                        }
                        else
                        {
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex2 = new NoViableAltException("", 4, 0, this.input);
                            throw ex2;
                        }
                        switch (num4)
                        {
                            case 1:
                                {
                                    int num5 = 0;
                                    while (true)
                                    {
                                        int num6 = 2;
                                        int num7 = this.input.LA(1);
                                        if ((num7 >= 5 && num7 <= 6) || (num7 == 10 || num7 == 13 || (num7 >= 16 && num7 <= 19)) || (num7 >= 22 && num7 <= 23) || (num7 == 25 || num7 == 27 || (num7 >= 31 && num7 <= 33)) || (num7 == 35 || num7 == 39 || num7 == 41 || num7 == 44 || num7 == 49 || num7 == 54 || (num7 >= 57 && num7 <= 58)) || (num7 == 64 || num7 == 66 || num7 == 71 || num7 == 75 || (num7 >= 79 && num7 <= 80)) || (num7 == 83 || num7 == 86 || (num7 >= 88 && num7 <= 91)) || num7 == 94 || num7 == 96 || num7 == 98 || num7 == 100)
                                        {
                                            num6 = 1;
                                        }
                                        int num8 = num6;
                                        if (num8 != 1)
                                        {
                                            goto IL_490;
                                        }
                                        base.PushFollow(ByDScriptingLanguageParser.Follow._stmt_in_stmtList227);
                                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.stmt();
                                        base.PopFollow();
                                        if (this.state.failed)
                                        {
                                            break;
                                        }
                                        if (this.state.backtracking == 0)
                                        {
                                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
                                        }
                                        num5++;
                                    }
                                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                    return result;
                                    IL_490:
                                    if (num5 < 1)
                                    {
                                        if (this.state.backtracking > 0)
                                        {
                                            this.state.failed = true;
                                            result = astParserRuleReturnScope;
                                            return result;
                                        }
                                        EarlyExitException ex3 = new EarlyExitException(3, this.input);
                                        throw ex3;
                                    }
                                    else if (this.state.backtracking == 0)
                                    {
                                        astParserRuleReturnScope.Tree = commonTree;
                                        new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                        commonTree = (CommonTree)this.adaptor.Nil();
                                        if (!rewriteRuleSubtreeStream.HasNext)
                                        {
                                            throw new RewriteEarlyExitException();
                                        }
                                        while (rewriteRuleSubtreeStream.HasNext)
                                        {
                                            this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                                        }
                                        rewriteRuleSubtreeStream.Reset();
                                        astParserRuleReturnScope.Tree = commonTree;
                                    }
                                    break;
                                }
                            case 2:
                                if (this.state.backtracking == 0)
                                {
                                    astParserRuleReturnScope.Tree = commonTree;
                                    new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                    commonTree = (CommonTree)this.adaptor.Nil();
                                    this.adaptor.AddChild(commonTree, (CommonTree)this.adaptor.Create(72, "NONE"));
                                    astParserRuleReturnScope.Tree = commonTree;
                                }
                                break;
                        }
                        IToken el2 = (IToken)this.Match(this.input, 82, ByDScriptingLanguageParser.Follow._RCURLB_in_stmtList242);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(el2);
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._singleStmt_in_stmtList247);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.singleStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope3.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex4)
        {
            this.ReportError(ex4);
            this.Recover(this.input, ex4);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex4);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_stmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_stmt()
    {
    }

    [GrammarRule("stmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> stmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num <= 49)
            {
                if (num <= 41)
                {
                    switch (num)
                    {
                        case 5:
                        case 6:
                        case 10:
                        case 13:
                        case 16:
                        case 17:
                        case 18:
                        case 19:
                        case 22:
                        case 23:
                        case 25:
                        case 27:
                        case 31:
                        case 32:
                        case 33:
                            break;
                        case 7:
                        case 8:
                        case 9:
                        case 11:
                        case 12:
                        case 14:
                        case 15:
                        case 20:
                        case 21:
                        case 24:
                        case 26:
                        case 28:
                        case 29:
                        case 30:
                        case 34:
                            goto IL_600;
                        case 35:
                            num2 = 5;
                            goto IL_639;
                        default:
                            switch (num)
                            {
                                case 39:
                                case 41:
                                    break;
                                case 40:
                                    goto IL_600;
                                default:
                                    goto IL_600;
                            }
                            break;
                    }
                }
                else if (num != 44)
                {
                    if (num != 49)
                    {
                        goto IL_600;
                    }
                    this.input.LA(2);
                    if (this.input.LA(2) != 29 && this.input.LA(2) != 28 && this.input.LA(2) != 37 && this.input.LA(2) != 12)
                    {
                        num2 = 4;
                        goto IL_639;
                    }
                    if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                    {
                        num2 = 7;
                        goto IL_639;
                    }
                    if (this.state.backtracking > 0)
                    {
                        this.state.failed = true;
                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                        return result;
                    }
                    NoViableAltException ex = new NoViableAltException("", 6, 4, this.input);
                    throw ex;
                }
            }
            else if (num <= 66)
            {
                switch (num)
                {
                    case 54:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 29 && this.input.LA(2) != 28 && this.input.LA(2) != 37 && this.input.LA(2) != 12)
                            {
                                num2 = 1;
                                goto IL_639;
                            }
                            if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                            {
                                num2 = 7;
                                goto IL_639;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex2 = new NoViableAltException("", 6, 1, this.input);
                            throw ex2;
                        }
                    case 55:
                    case 56:
                        goto IL_600;
                    case 57:
                    case 58:
                        break;
                    default:
                        switch (num)
                        {
                            case 64:
                            case 66:
                                break;
                            case 65:
                                goto IL_600;
                            default:
                                goto IL_600;
                        }
                        break;
                }
            }
            else if (num != 71)
            {
                switch (num)
                {
                    case 75:
                    case 79:
                    case 80:
                    case 83:
                    case 86:
                    case 89:
                    case 90:
                    case 91:
                        break;
                    case 76:
                    case 77:
                    case 78:
                    case 81:
                    case 82:
                    case 84:
                    case 85:
                    case 87:
                        goto IL_600;
                    case 88:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 29 && this.input.LA(2) != 28 && this.input.LA(2) != 37 && this.input.LA(2) != 12)
                            {
                                num2 = 2;
                                goto IL_639;
                            }
                            if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                            {
                                num2 = 7;
                                goto IL_639;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex3 = new NoViableAltException("", 6, 2, this.input);
                            throw ex3;
                        }
                    default:
                        switch (num)
                        {
                            case 94:
                            case 96:
                                break;
                            case 95:
                            case 97:
                            case 99:
                                goto IL_600;
                            case 98:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 29 && this.input.LA(2) != 28 && this.input.LA(2) != 37 && this.input.LA(2) != 12)
                                    {
                                        num2 = 6;
                                        goto IL_639;
                                    }
                                    if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                                    {
                                        num2 = 7;
                                        goto IL_639;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex4 = new NoViableAltException("", 6, 6, this.input);
                                    throw ex4;
                                }
                            case 100:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 29 && this.input.LA(2) != 28 && this.input.LA(2) != 37 && this.input.LA(2) != 12)
                                    {
                                        num2 = 3;
                                        goto IL_639;
                                    }
                                    if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                                    {
                                        num2 = 7;
                                        goto IL_639;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex5 = new NoViableAltException("", 6, 3, this.input);
                                    throw ex5;
                                }
                            default:
                                goto IL_600;
                        }
                        break;
                }
            }
            num2 = 7;
            goto IL_639;
            IL_600:
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex6 = new NoViableAltException("", 6, 0, this.input);
            throw ex6;
            IL_639:
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._ifStmt_in_stmt259);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.ifStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._switchStmt_in_stmt264);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.switchStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope3.Tree);
                        }
                        break;
                    }
                case 3:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._whileStmt_in_stmt269);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.whileStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope4.Tree);
                        }
                        break;
                    }
                case 4:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._forEachStmt_in_stmt274);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.forEachStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope5.Tree);
                        }
                        break;
                    }
                case 5:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._emptyStmt_in_stmt279);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope6 = this.emptyStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope6.Tree);
                        }
                        break;
                    }
                case 6:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._varDeclStmt_in_stmt284);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope7 = this.varDeclStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope7.Tree);
                        }
                        break;
                    }
                case 7:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._singleStmt_in_stmt290);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope8 = this.singleStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope8.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex7)
        {
            this.ReportError(ex7);
            this.Recover(this.input, ex7);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex7);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_singleStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_singleStmt()
    {
    }

    [GrammarRule("singleStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> singleStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num <= 49)
            {
                if (num <= 41)
                {
                    switch (num)
                    {
                        case 5:
                            this.input.LA(2);
                            if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                            {
                                num2 = 1;
                                goto IL_5A5;
                            }
                            num2 = 7;
                            goto IL_5A5;
                        case 6:
                        case 10:
                        case 13:
                        case 19:
                        case 22:
                        case 25:
                        case 27:
                            break;
                        case 7:
                        case 8:
                        case 9:
                        case 11:
                        case 12:
                        case 14:
                        case 15:
                        case 20:
                        case 21:
                        case 24:
                        case 26:
                        case 28:
                        case 29:
                        case 30:
                            goto IL_56C;
                        case 16:
                            this.input.LA(2);
                            if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                            {
                                num2 = 1;
                                goto IL_5A5;
                            }
                            num2 = 4;
                            goto IL_5A5;
                        case 17:
                        case 32:
                            goto IL_567;
                        case 18:
                            this.input.LA(2);
                            if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                            {
                                num2 = 1;
                                goto IL_5A5;
                            }
                            num2 = 6;
                            goto IL_5A5;
                        case 23:
                            this.input.LA(2);
                            if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                            {
                                num2 = 1;
                                goto IL_5A5;
                            }
                            num2 = 6;
                            goto IL_5A5;
                        case 31:
                            this.input.LA(2);
                            if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                            {
                                num2 = 1;
                                goto IL_5A5;
                            }
                            num2 = 3;
                            goto IL_5A5;
                        case 33:
                            this.input.LA(2);
                            if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                            {
                                num2 = 1;
                                goto IL_5A5;
                            }
                            num2 = 6;
                            goto IL_5A5;
                        default:
                            switch (num)
                            {
                                case 39:
                                    break;
                                case 40:
                                    goto IL_56C;
                                case 41:
                                    this.input.LA(2);
                                    if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                                    {
                                        num2 = 1;
                                        goto IL_5A5;
                                    }
                                    num2 = 6;
                                    goto IL_5A5;
                                default:
                                    goto IL_56C;
                            }
                            break;
                    }
                }
                else if (num != 44 && num != 49)
                {
                    goto IL_56C;
                }
            }
            else if (num <= 66)
            {
                switch (num)
                {
                    case 54:
                    case 57:
                    case 58:
                        break;
                    case 55:
                    case 56:
                        goto IL_56C;
                    default:
                        switch (num)
                        {
                            case 64:
                            case 66:
                                break;
                            case 65:
                                goto IL_56C;
                            default:
                                goto IL_56C;
                        }
                        break;
                }
            }
            else if (num != 71)
            {
                switch (num)
                {
                    case 75:
                        goto IL_567;
                    case 76:
                    case 77:
                    case 78:
                    case 81:
                    case 82:
                    case 84:
                    case 85:
                    case 87:
                        goto IL_56C;
                    case 79:
                        this.input.LA(2);
                        if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                        {
                            num2 = 1;
                            goto IL_5A5;
                        }
                        num2 = 2;
                        goto IL_5A5;
                    case 80:
                    case 88:
                    case 89:
                    case 90:
                    case 91:
                        break;
                    case 83:
                        this.input.LA(2);
                        if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                        {
                            num2 = 1;
                            goto IL_5A5;
                        }
                        num2 = 5;
                        goto IL_5A5;
                    case 86:
                        this.input.LA(2);
                        if (this.input.LA(2) == 29 || this.input.LA(2) == 28 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                        {
                            num2 = 1;
                            goto IL_5A5;
                        }
                        num2 = 6;
                        goto IL_5A5;
                    default:
                        switch (num)
                        {
                            case 94:
                            case 96:
                            case 98:
                            case 100:
                                break;
                            case 95:
                            case 97:
                            case 99:
                                goto IL_56C;
                            default:
                                goto IL_56C;
                        }
                        break;
                }
            }
            num2 = 1;
            goto IL_5A5;
            IL_567:
            num2 = 6;
            goto IL_5A5;
            IL_56C:
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 7, 0, this.input);
            throw ex;
            IL_5A5:
            switch (num2)
            {
                case 1:
                    commonTree = (CommonTree)this.adaptor.Nil();
                    if (this.input.LA(2) != 29 && this.input.LA(2) != 28 && this.input.LA(2) != 37 && this.input.LA(2) != 12)
                    {
                        if (this.state.backtracking > 0)
                        {
                            this.state.failed = true;
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        throw new FailedPredicateException(this.input, "singleStmt", " ( /*input.LA(2) == LBRCKT || uncomment when indexing is activated */ \r\n\t\t   input.LA(2) == COLON2 ||\r\n\t\t   input.LA(2) == COLON ||\r\n\t\t   input.LA(2) == DOT ||\r\n\t\t   input.LA(2) == ASSIGN_OPS) ");
                    }
                    else
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._methodOrAssignmentStmt_in_singleStmt305);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.methodOrAssignmentStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
                        }
                    }
                    break;
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._raiseStmt_in_singleStmt310);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.raiseStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope3.Tree);
                        }
                        break;
                    }
                case 3:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._contStmt_in_singleStmt315);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.contStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope4.Tree);
                        }
                        break;
                    }
                case 4:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._breakStmt_in_singleStmt320);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.breakStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope5.Tree);
                        }
                        break;
                    }
                case 5:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._returnStmt_in_singleStmt325);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope6 = this.returnStmt();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope6.Tree);
                        }
                        break;
                    }
                case 6:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._errorStmts_in_singleStmt330);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope7 = this.errorStmts();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope7.Tree);
                        }
                        break;
                    }
                case 7:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken payload = (IToken)this.Match(this.input, 5, ByDScriptingLanguageParser.Follow._ABSLID_in_singleStmt336);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(payload);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_errorStmts()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_errorStmts()
    {
    }

    [GrammarRule("errorStmts")]
    private AstParserRuleReturnScope<CommonTree, IToken> errorStmts()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num <= 33)
            {
                switch (num)
                {
                    case 17:
                    case 18:
                        break;
                    default:
                        if (num == 23)
                        {
                            num2 = 2;
                            goto IL_C8;
                        }
                        switch (num)
                        {
                            case 32:
                                break;
                            case 33:
                                num2 = 3;
                                goto IL_C8;
                            default:
                                goto IL_8F;
                        }
                        break;
                }
            }
            else
            {
                if (num == 41)
                {
                    num2 = 1;
                    goto IL_C8;
                }
                if (num != 75 && num != 86)
                {
                    goto IL_8F;
                }
            }
            num2 = 4;
            goto IL_C8;
            IL_8F:
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 8, 0, this.input);
            throw ex;
            IL_C8:
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._unmatchedElse_in_errorStmts348);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.unmatchedElse();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._unmatchedCase_in_errorStmts353);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.unmatchedCase();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope3.Tree);
                        }
                        break;
                    }
                case 3:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._unmatchedDefault_in_errorStmts358);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.unmatchedDefault();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope4.Tree);
                        }
                        break;
                    }
                case 4:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._unexpOrErrLiteral_in_errorStmts363);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.unexpOrErrLiteral();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope5.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_emptyStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_emptyStmt()
    {
    }

    [GrammarRule("emptyStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> emptyStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token DELIM");
        try
        {
            IToken el = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_emptyStmt374);
            if (this.state.failed)
            {
                return astParserRuleReturnScope;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, (CommonTree)this.adaptor.Create(72, "NONE"));
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_impStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_impStmt()
    {
    }

    [GrammarRule("impStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> impStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token AS");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token IMPORT");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token DELIM");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule impNameSpace");
        try
        {
            IToken token = (IToken)this.Match(this.input, 57, ByDScriptingLanguageParser.Follow._IMPORT_in_impStmt391);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(token);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._impNameSpace_in_impStmt394);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.impNameSpace();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope2.Tree);
            }
            int num = 2;
            int num2 = this.input.LA(1);
            if (num2 == 10)
            {
                num = 1;
            }
            int num3 = num;
            if (num3 == 1)
            {
                IToken el = (IToken)this.Match(this.input, 10, ByDScriptingLanguageParser.Follow._AS_in_impStmt397);
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(el);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_impStmt399);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.fullId();
                base.PopFollow();
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                }
            }
            IToken el2 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_impStmt403);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream3.Add(el2);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream2.NextNode(), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream4.NextNode());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                if (rewriteRuleSubtreeStream.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                }
                rewriteRuleSubtreeStream.Reset();
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_impNameSpace()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_impNameSpace()
    {
    }

    [GrammarRule("impNameSpace")]
    private AstParserRuleReturnScope<CommonTree, IToken> impNameSpace()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token DOT");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_impNameSpace432);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            int num = this.input.LA(1);
            int num2;
            if (num == 10 || num == 35)
            {
                num2 = 1;
            }
            else if (num == 37)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 11, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    if (this.state.backtracking == 0)
                    {
                        astParserRuleReturnScope.Tree = commonTree;
                        new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                        commonTree = (CommonTree)this.adaptor.Nil();
                        this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                        astParserRuleReturnScope.Tree = commonTree;
                    }
                    break;
                case 2:
                    {
                        int num3 = 0;
                        while (true)
                        {
                            int num4 = 2;
                            int num5 = this.input.LA(1);
                            if (num5 == 37)
                            {
                                num4 = 1;
                            }
                            int num6 = num4;
                            if (num6 != 1)
                            {
                                goto IL_228;
                            }
                            IToken el = (IToken)this.Match(this.input, 37, ByDScriptingLanguageParser.Follow._DOT_in_impNameSpace459);
                            if (this.state.failed)
                            {
                                break;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleTokenStream.Add(el);
                            }
                            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_impNameSpace461);
                            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.fullId();
                            base.PopFollow();
                            if (this.state.failed)
                            {
                                goto Block_15;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                            }
                            num3++;
                        }
                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                        return result;
                        Block_15:
                        result = astParserRuleReturnScope;
                        return result;
                        IL_228:
                        if (num3 < 1)
                        {
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                result = astParserRuleReturnScope;
                                return result;
                            }
                            EarlyExitException ex2 = new EarlyExitException(10, this.input);
                            throw ex2;
                        }
                        else if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule tok", (astParserRuleReturnScope2 != null) ? astParserRuleReturnScope2.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(74, "NSPACE"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                            if (!rewriteRuleSubtreeStream.HasNext)
                            {
                                throw new RewriteEarlyExitException();
                            }
                            while (rewriteRuleSubtreeStream.HasNext)
                            {
                                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                            }
                            rewriteRuleSubtreeStream.Reset();
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex3)
        {
            this.ReportError(ex3);
            this.Recover(this.input, ex3);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex3);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_varDeclStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_varDeclStmt()
    {
    }

    [GrammarRule("varDeclStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> varDeclStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token VAR");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token DELIM");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token ASSIGN_OPS");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule expression");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule varTypeSpec");
        try
        {
            IToken token = (IToken)this.Match(this.input, 98, ByDScriptingLanguageParser.Follow._VAR_in_varDeclStmt499);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(token);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_varDeclStmt501);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.fullId();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._varTypeSpec_in_varDeclStmt503);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.varTypeSpec();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope4.Tree);
            }
            int num = 2;
            int num2 = this.input.LA(1);
            if (num2 == 12)
            {
                num = 1;
            }
            int num3 = num;
            if (num3 == 1)
            {
                IToken el = (IToken)this.Match(this.input, 12, ByDScriptingLanguageParser.Follow._ASSIGN_OPS_in_varDeclStmt507);
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream3.Add(el);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_varDeclStmt511);
                astParserRuleReturnScope2 = this.expression();
                base.PopFollow();
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope2.Tree);
                }
            }
            IToken el2 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_varDeclStmt516);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el2);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream.NextNode(), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream4.NextNode());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream3.NextTree());
                if (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0 && astParserRuleReturnScope2 != null)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_varTypeSpec()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_varTypeSpec()
    {
    }

    [GrammarRule("varTypeSpec")]
    private AstParserRuleReturnScope<CommonTree, IToken> varTypeSpec()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num == 28)
            {
                num2 = 1;
            }
            else if (num == 12 || num == 35)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 13, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken arg_C2_0 = (IToken)this.Match(this.input, 28, ByDScriptingLanguageParser.Follow._COLON_in_varTypeSpec546);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._collectionClause_in_varTypeSpec549);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.collectionClause();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
                        }
                        break;
                    }
                case 2:
                    if (this.state.backtracking == 0)
                    {
                        astParserRuleReturnScope.Tree = commonTree;
                        new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                        commonTree = (CommonTree)this.adaptor.Nil();
                        this.adaptor.AddChild(commonTree, (CommonTree)this.adaptor.Create(72, "NONE"));
                        astParserRuleReturnScope.Tree = commonTree;
                    }
                    break;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_collectionClause()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_collectionClause()
    {
    }

    [GrammarRule("collectionClause")]
    private AstParserRuleReturnScope<CommonTree, IToken> collectionClause()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token COLLOF");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule elementsClause");
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num == 27)
            {
                this.input.LA(2);
                if (this.EvaluatePredicate(new Action(this.synpred1_ByDScriptingLanguage_fragment)))
                {
                    num2 = 1;
                }
                else
                {
                    num2 = 2;
                }
            }
            else if ((num >= 5 && num <= 6) || (num == 10 || num == 13 || (num >= 16 && num <= 19)) || (num >= 22 && num <= 23) || (num == 25 || (num >= 31 && num <= 33)) || (num == 39 || num == 41 || num == 44 || num == 49 || num == 54 || (num >= 57 && num <= 58)) || (num == 64 || num == 66 || num == 71 || num == 75 || (num >= 79 && num <= 80)) || (num == 83 || num == 86 || (num >= 88 && num <= 91)) || num == 94 || num == 96 || num == 98 || num == 100)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 15, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        IToken token = (IToken)this.Match(this.input, 27, ByDScriptingLanguageParser.Follow._COLLOF_in_collectionClause576);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(token);
                        }
                        int num3 = 2;
                        int num4 = this.input.LA(1);
                        if ((num4 >= 5 && num4 <= 6) || (num4 == 10 || num4 == 13 || (num4 >= 16 && num4 <= 19)) || (num4 >= 22 && num4 <= 23) || (num4 == 25 || num4 == 27 || (num4 >= 31 && num4 <= 33)) || (num4 == 39 || num4 == 41 || num4 == 44 || num4 == 49 || num4 == 54 || (num4 >= 57 && num4 <= 58)) || (num4 == 64 || num4 == 66 || num4 == 71 || num4 == 75 || (num4 >= 79 && num4 <= 80)) || (num4 == 83 || num4 == 86 || (num4 >= 88 && num4 <= 91)) || num4 == 94 || num4 == 96 || num4 == 98 || num4 == 100)
                        {
                            num3 = 1;
                        }
                        int num5 = num3;
                        if (num5 == 1)
                        {
                            base.PushFollow(ByDScriptingLanguageParser.Follow._elementsClause_in_collectionClause578);
                            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.elementsClause();
                            base.PopFollow();
                            if (this.state.failed)
                            {
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
                            }
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(26, "COLLECTIONOF"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._elementsClause_in_collectionClause595);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.elementsClause();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope3.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_elementsClause()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_elementsClause()
    {
    }

    [GrammarRule("elementsClause")]
    private AstParserRuleReturnScope<CommonTree, IToken> elementsClause()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token ELTSOF");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule typeOfClause");
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num == 44)
            {
                this.input.LA(2);
                if (this.EvaluatePredicate(new Action(this.synpred2_ByDScriptingLanguage_fragment)))
                {
                    num2 = 1;
                }
                else
                {
                    num2 = 2;
                }
            }
            else if ((num >= 5 && num <= 6) || (num == 10 || num == 13 || (num >= 16 && num <= 19)) || (num >= 22 && num <= 23) || (num == 25 || num == 27 || (num >= 31 && num <= 33)) || (num == 39 || num == 41 || num == 49 || num == 54 || (num >= 57 && num <= 58)) || (num == 64 || num == 66 || num == 71 || num == 75 || (num >= 79 && num <= 80)) || (num == 83 || num == 86 || (num >= 88 && num <= 91)) || num == 94 || num == 96 || num == 98 || num == 100)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 16, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        IToken token = (IToken)this.Match(this.input, 44, ByDScriptingLanguageParser.Follow._ELTSOF_in_elementsClause615);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._typeOfClause_in_elementsClause617);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.typeOfClause();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(40, "ELEMENTSOF"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._typeOfClause_in_elementsClause634);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.typeOfClause();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope3.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_typeOfClause()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_typeOfClause()
    {
    }

    [GrammarRule("typeOfClause")]
    private AstParserRuleReturnScope<CommonTree, IToken> typeOfClause()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token TYPOF");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule atomExpr");
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num == 94)
            {
                this.input.LA(2);
                if (this.EvaluatePredicate(new Action(this.synpred3_ByDScriptingLanguage_fragment)))
                {
                    num2 = 1;
                }
                else
                {
                    num2 = 2;
                }
            }
            else if ((num >= 5 && num <= 6) || (num == 10 || num == 13 || (num >= 16 && num <= 19)) || (num >= 22 && num <= 23) || (num == 25 || num == 27 || (num >= 31 && num <= 33)) || (num == 39 || num == 41 || num == 44 || num == 49 || num == 54 || (num >= 57 && num <= 58)) || (num == 64 || num == 66 || num == 71 || num == 75 || (num >= 79 && num <= 80)) || (num == 83 || num == 86 || (num >= 88 && num <= 91)) || num == 96 || num == 98 || num == 100)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 17, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        IToken token = (IToken)this.Match(this.input, 94, ByDScriptingLanguageParser.Follow._TYPOF_in_typeOfClause654);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._atomExpr_in_typeOfClause656);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.atomExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(93, "TYPEOF"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 2:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._atomExpr_in_typeOfClause673);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.atomExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_ifStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_ifStmt()
    {
    }

    [GrammarRule("ifStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> ifStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token IF");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule stmtList");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule condition");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule elseIfClause");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream4 = new RewriteRuleSubtreeStream(this.adaptor, "rule elseClause");
        try
        {
            IToken token = (IToken)this.Match(this.input, 54, ByDScriptingLanguageParser.Follow._IF_in_ifStmt696);
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(token);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._condition_in_ifStmt700);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.condition();
            base.PopFollow();
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope2.Tree);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._stmtList_in_ifStmt702);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.stmtList();
            base.PopFollow();
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 41)
                {
                    this.input.LA(2);
                    if (this.EvaluatePredicate(new Action(this.synpred4_ByDScriptingLanguage_fragment)))
                    {
                        num = 1;
                    }
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_1E6;
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._elseIfClause_in_ifStmt714);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.elseIfClause();
                base.PopFollow();
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope4.Tree);
                }
            }
            result = astParserRuleReturnScope;
            return result;
            IL_1E6:
            int num4 = 2;
            int num5 = this.input.LA(1);
            if (num5 == 41)
            {
                this.input.LA(2);
                if (this.input.LA(1) == 41 && this.input.LA(2) != 37 && this.input.LA(2) != 12)
                {
                    num4 = 1;
                }
            }
            int num6 = num4;
            if (num6 == 1)
            {
                if (this.input.LA(1) != 41 || this.input.LA(2) == 37 || this.input.LA(2) == 12)
                {
                    if (this.state.backtracking > 0)
                    {
                        this.state.failed = true;
                        result = astParserRuleReturnScope;
                        return result;
                    }
                    throw new FailedPredicateException(this.input, "ifStmt", "(input.LA(1) == ELSE && (input.LA(2) != DOT && input.LA(2) != ASSIGN_OPS))");
                }
                else
                {
                    base.PushFollow(ByDScriptingLanguageParser.Follow._elseClause_in_ifStmt724);
                    AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.elseClause();
                    base.PopFollow();
                    if (this.state.failed)
                    {
                        result = astParserRuleReturnScope;
                        return result;
                    }
                    if (this.state.backtracking == 0)
                    {
                        rewriteRuleSubtreeStream4.Add(astParserRuleReturnScope5.Tree);
                    }
                }
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream.NextNode(), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(55, "IFCLAUSE"), commonTree3);
                this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream2.NextNode());
                this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream2.NextTree());
                this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream.NextTree());
                this.adaptor.AddChild(commonTree2, commonTree3);
                while (rewriteRuleSubtreeStream3.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream3.NextTree());
                }
                rewriteRuleSubtreeStream3.Reset();
                if (rewriteRuleSubtreeStream4.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream4.NextTree());
                }
                rewriteRuleSubtreeStream4.Reset();
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, (astParserRuleReturnScope2 != null) ? astParserRuleReturnScope2.Stop : null);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_elseIfClause()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_elseIfClause()
    {
    }

    [GrammarRule("elseIfClause")]
    private AstParserRuleReturnScope<CommonTree, IToken> elseIfClause()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token ELSE");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token IF");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule stmtList");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule condition");
        try
        {
            IToken token = (IToken)this.Match(this.input, 41, ByDScriptingLanguageParser.Follow._ELSE_in_elseIfClause773);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(token);
            }
            IToken el = (IToken)this.Match(this.input, 54, ByDScriptingLanguageParser.Follow._IF_in_elseIfClause775);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._condition_in_elseIfClause779);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.condition();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope2.Tree);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._stmtList_in_elseIfClause781);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.stmtList();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(43, "ELSEIFCLAUSE"), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream3.NextNode());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, (astParserRuleReturnScope2 != null) ? astParserRuleReturnScope2.Stop : null);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_elseClause()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_elseClause()
    {
    }

    [GrammarRule("elseClause")]
    private AstParserRuleReturnScope<CommonTree, IToken> elseClause()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token ELSE");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule stmtList");
        try
        {
            IToken token = (IToken)this.Match(this.input, 41, ByDScriptingLanguageParser.Follow._ELSE_in_elseClause809);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(token);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._stmtList_in_elseClause811);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.stmtList();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(42, "ELSECLAUSE"), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_switchStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_switchStmt()
    {
    }

    [GrammarRule("switchStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> switchStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token RCURLB");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token LCURLB");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token SWITCH");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule condition");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule caseClause");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule defaultClause");
        try
        {
            IToken token = (IToken)this.Match(this.input, 88, ByDScriptingLanguageParser.Follow._SWITCH_in_switchStmt843);
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream3.Add(token);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._condition_in_switchStmt847);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.condition();
            base.PopFollow();
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            IToken el = (IToken)this.Match(this.input, 62, ByDScriptingLanguageParser.Follow._LCURLB_in_switchStmt851);
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el);
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 23 && this.EvaluatePredicate(new Action(this.synpred5_ByDScriptingLanguage_fragment)))
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_1EA;
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._caseClause_in_switchStmt861);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.caseClause();
                base.PopFollow();
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
                }
            }
            result = astParserRuleReturnScope;
            return result;
            IL_1EA:
            int num4 = 2;
            int num5 = this.input.LA(1);
            if (num5 == 33 && this.EvaluatePredicate(new Action(this.synpred6_ByDScriptingLanguage_fragment)))
            {
                num4 = 1;
            }
            int num6 = num4;
            if (num6 == 1)
            {
                base.PushFollow(ByDScriptingLanguageParser.Follow._defaultClause_in_switchStmt873);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.defaultClause();
                base.PopFollow();
                if (this.state.failed)
                {
                    result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope4.Tree);
                }
            }
            IToken el2 = (IToken)this.Match(this.input, 82, ByDScriptingLanguageParser.Follow._RCURLB_in_switchStmt879);
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el2);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream3.NextNode(), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream4.NextNode());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                while (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                if (rewriteRuleSubtreeStream3.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream3.NextTree());
                }
                rewriteRuleSubtreeStream3.Reset();
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, (astParserRuleReturnScope2 != null) ? astParserRuleReturnScope2.Stop : null);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_caseClause()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_caseClause()
    {
    }

    [GrammarRule("caseClause")]
    private AstParserRuleReturnScope<CommonTree, IToken> caseClause()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token RCURLB");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token LCURLB");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token CASE");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule caseValues");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule stmt");
        try
        {
            IToken token = (IToken)this.Match(this.input, 23, ByDScriptingLanguageParser.Follow._CASE_in_caseClause917);
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream3.Add(token);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._caseValues_in_caseClause921);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.caseValues();
            base.PopFollow();
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            IToken el = (IToken)this.Match(this.input, 62, ByDScriptingLanguageParser.Follow._LCURLB_in_caseClause923);
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el);
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if ((num2 >= 5 && num2 <= 6) || (num2 == 10 || num2 == 13 || (num2 >= 16 && num2 <= 19)) || (num2 >= 22 && num2 <= 23) || (num2 == 25 || num2 == 27 || (num2 >= 31 && num2 <= 33)) || (num2 == 35 || num2 == 39 || num2 == 41 || num2 == 44 || num2 == 49 || num2 == 54 || (num2 >= 57 && num2 <= 58)) || (num2 == 64 || num2 == 66 || num2 == 71 || num2 == 75 || (num2 >= 79 && num2 <= 80)) || (num2 == 83 || num2 == 86 || (num2 >= 88 && num2 <= 91)) || num2 == 94 || num2 == 96 || num2 == 98 || num2 == 100)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_2A0;
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._stmt_in_caseClause925);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.stmt();
                base.PopFollow();
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
                }
            }
            result = astParserRuleReturnScope;
            return result;
            IL_2A0:
            IToken el2 = (IToken)this.Match(this.input, 82, ByDScriptingLanguageParser.Follow._RCURLB_in_caseClause928);
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el2);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(24, "CASECLAUSE"), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream4.NextNode());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                while (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, (astParserRuleReturnScope2 != null) ? astParserRuleReturnScope2.Stop : null);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_caseValues()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_caseValues()
    {
    }

    [GrammarRule("caseValues")]
    private AstParserRuleReturnScope<CommonTree, IToken> caseValues()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token COMMA");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule expression");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_caseValues955);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.expression();
            base.PopFollow();
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 30)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_146;
                }
                IToken el = (IToken)this.Match(this.input, 30, ByDScriptingLanguageParser.Follow._COMMA_in_caseValues958);
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(el);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_caseValues960);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.expression();
                base.PopFollow();
                if (this.state.failed)
                {
                    goto Block_9;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                }
            }
            result = astParserRuleReturnScope;
            return result;
            Block_9:
            result = astParserRuleReturnScope;
            return result;
            IL_146:
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(97, "VALUES"), commonTree2);
                while (rewriteRuleSubtreeStream.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                }
                rewriteRuleSubtreeStream.Reset();
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_defaultClause()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_defaultClause()
    {
    }

    [GrammarRule("defaultClause")]
    private AstParserRuleReturnScope<CommonTree, IToken> defaultClause()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token DEFAULT");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule stmtList");
        try
        {
            IToken token = (IToken)this.Match(this.input, 33, ByDScriptingLanguageParser.Follow._DEFAULT_in_defaultClause984);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(token);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._stmtList_in_defaultClause986);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.stmtList();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(34, "DEFAULTCLAUSE"), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_whileStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_whileStmt()
    {
    }

    [GrammarRule("whileStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> whileStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token WHILE");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule stmtList");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule condition");
        try
        {
            IToken token = (IToken)this.Match(this.input, 100, ByDScriptingLanguageParser.Follow._WHILE_in_whileStmt1016);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(token);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._condition_in_whileStmt1020);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.condition();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope2.Tree);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._stmtList_in_whileStmt1022);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.stmtList();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream.NextNode(), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, (astParserRuleReturnScope2 != null) ? astParserRuleReturnScope2.Stop : null);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_forEachStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_forEachStmt()
    {
    }

    [GrammarRule("forEachStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> forEachStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        IToken token = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token INN");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token RPAREN");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token FOREACH");
        RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token VAR");
        RewriteRuleTokenStream rewriteRuleTokenStream5 = new RewriteRuleTokenStream(this.adaptor, "token LPAREN");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule expression");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule stmtList");
        try
        {
            IToken token2 = (IToken)this.Match(this.input, 49, ByDScriptingLanguageParser.Follow._FOREACH_in_forEachStmt1056);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream3.Add(token2);
            }
            IToken el = (IToken)this.Match(this.input, 64, ByDScriptingLanguageParser.Follow._LPAREN_in_forEachStmt1058);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream5.Add(el);
            }
            int num = this.input.LA(1);
            int num2;
            if (num == 98)
            {
                this.input.LA(2);
                if (this.EvaluatePredicate(new Action(this.synpred7_ByDScriptingLanguage_fragment)))
                {
                    num2 = 1;
                }
                else
                {
                    num2 = 2;
                }
            }
            else if ((num >= 5 && num <= 6) || (num == 10 || num == 13 || num == 16 || num == 19 || (num >= 22 && num <= 23)) || (num == 25 || num == 27 || num == 31 || num == 33 || num == 39 || num == 41 || num == 44 || num == 49 || num == 54 || (num >= 57 && num <= 58)) || (num == 66 || num == 71 || (num >= 79 && num <= 80)) || (num == 83 || (num >= 88 && num <= 91)) || num == 94 || num == 96 || num == 100)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 24, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        IToken el2 = (IToken)this.Match(this.input, 98, ByDScriptingLanguageParser.Follow._VAR_in_forEachStmt1075);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream4.Add(el2);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_forEachStmt1077);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
                        }
                        IToken el3 = (IToken)this.Match(this.input, 58, ByDScriptingLanguageParser.Follow._INN_in_forEachStmt1079);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(el3);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_forEachStmt1081);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.expression();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
                        }
                        token = (IToken)this.Match(this.input, 84, ByDScriptingLanguageParser.Follow._RPAREN_in_forEachStmt1085);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream2.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._stmtList_in_forEachStmt1087);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.stmtList();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope4.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream6 = new RewriteRuleTokenStream(this.adaptor, "token tok", token2);
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(50, "FOREACHDECL"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream6.NextNode());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream3.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 2:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_forEachStmt1110);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.fullId();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope5.Tree);
                        }
                        IToken el4 = (IToken)this.Match(this.input, 58, ByDScriptingLanguageParser.Follow._INN_in_forEachStmt1112);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(el4);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_forEachStmt1114);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope6 = this.expression();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope6.Tree);
                        }
                        token = (IToken)this.Match(this.input, 84, ByDScriptingLanguageParser.Follow._RPAREN_in_forEachStmt1118);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream2.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._stmtList_in_forEachStmt1120);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope7 = this.stmtList();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope7.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream7 = new RewriteRuleTokenStream(this.adaptor, "token tok", token2);
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                            commonTree3 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream3.NextNode(), commonTree3);
                            this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream7.NextNode());
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream2.NextTree());
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream3.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree3);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, token);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_condition()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_condition()
    {
    }

    [GrammarRule("condition")]
    private AstParserRuleReturnScope<CommonTree, IToken> condition()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        try
        {
            CommonTree commonTree = (CommonTree)this.adaptor.Nil();
            IToken arg_45_0 = (IToken)this.Match(this.input, 64, ByDScriptingLanguageParser.Follow._LPAREN_in_condition1172);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_condition1175);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.expression();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
            }
            IToken arg_BF_0 = (IToken)this.Match(this.input, 84, ByDScriptingLanguageParser.Follow._RPAREN_in_condition1177);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_tryBlock()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_tryBlock()
    {
    }

    [GrammarRule("tryBlock")]
    private AstParserRuleReturnScope<CommonTree, IToken> tryBlock()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token TRY");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule stmtList");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule catchBlock");
        try
        {
            IToken token = (IToken)this.Match(this.input, 91, ByDScriptingLanguageParser.Follow._TRY_in_tryBlock1191);
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(token);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._stmtList_in_tryBlock1193);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.stmtList();
            base.PopFollow();
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 25)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_14F;
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._catchBlock_in_tryBlock1195);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.catchBlock();
                base.PopFollow();
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
                }
            }
            result = astParserRuleReturnScope;
            return result;
            IL_14F:
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream.NextNode(), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(92, "TRYBLOCK"), commonTree3);
                this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream2.NextNode());
                this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream.NextTree());
                this.adaptor.AddChild(commonTree2, commonTree3);
                while (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_catchBlock()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_catchBlock()
    {
    }

    [GrammarRule("catchBlock")]
    private AstParserRuleReturnScope<CommonTree, IToken> catchBlock()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token RPAREN");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token CATCH");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token LPAREN");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule stmtList");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule catchTypeSpec");
        try
        {
            IToken token = (IToken)this.Match(this.input, 25, ByDScriptingLanguageParser.Follow._CATCH_in_catchBlock1230);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(token);
            }
            IToken el = (IToken)this.Match(this.input, 64, ByDScriptingLanguageParser.Follow._LPAREN_in_catchBlock1232);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream3.Add(el);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_catchBlock1234);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._catchTypeSpec_in_catchBlock1236);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.catchTypeSpec();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope3.Tree);
            }
            IToken el2 = (IToken)this.Match(this.input, 84, ByDScriptingLanguageParser.Follow._RPAREN_in_catchBlock1238);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el2);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._stmtList_in_catchBlock1240);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.stmtList();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope4.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream2.NextNode(), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream4.NextNode());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream3.NextTree());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_catchTypeSpec()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_catchTypeSpec()
    {
    }

    [GrammarRule("catchTypeSpec")]
    private AstParserRuleReturnScope<CommonTree, IToken> catchTypeSpec()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num == 28)
            {
                num2 = 1;
            }
            else if (num == 84)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 26, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken arg_BC_0 = (IToken)this.Match(this.input, 28, ByDScriptingLanguageParser.Follow._COLON_in_catchTypeSpec1266);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._atomExpr_in_catchTypeSpec1269);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.atomExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
                        }
                        break;
                    }
                case 2:
                    if (this.state.backtracking == 0)
                    {
                        astParserRuleReturnScope.Tree = commonTree;
                        new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                        commonTree = (CommonTree)this.adaptor.Nil();
                        this.adaptor.AddChild(commonTree, (CommonTree)this.adaptor.Create(72, "NONE"));
                        astParserRuleReturnScope.Tree = commonTree;
                    }
                    break;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_contStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_contStmt()
    {
    }

    [GrammarRule("contStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> contStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        try
        {
            CommonTree commonTree = (CommonTree)this.adaptor.Nil();
            IToken payload = (IToken)this.Match(this.input, 31, ByDScriptingLanguageParser.Follow._CONTINUE_in_contStmt1294);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                CommonTree child = (CommonTree)this.adaptor.Create(payload);
                this.adaptor.AddChild(commonTree, child);
            }
            IToken arg_A1_0 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_contStmt1296);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_breakStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_breakStmt()
    {
    }

    [GrammarRule("breakStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> breakStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        try
        {
            CommonTree commonTree = (CommonTree)this.adaptor.Nil();
            IToken payload = (IToken)this.Match(this.input, 16, ByDScriptingLanguageParser.Follow._BREAK_in_breakStmt1312);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                CommonTree child = (CommonTree)this.adaptor.Create(payload);
                this.adaptor.AddChild(commonTree, child);
            }
            IToken arg_A1_0 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_breakStmt1314);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_raiseStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_raiseStmt()
    {
    }

    [GrammarRule("raiseStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> raiseStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token RAISE");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token DELIM");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule atomExpr");
        try
        {
            IToken token = (IToken)this.Match(this.input, 79, ByDScriptingLanguageParser.Follow._RAISE_in_raiseStmt1334);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(token);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._atomExpr_in_raiseStmt1336);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.atomExpr();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            IToken el = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_raiseStmt1338);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream.NextNode(), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream3.NextNode());
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_methodOrAssignmentStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_methodOrAssignmentStmt()
    {
    }

    [GrammarRule("methodOrAssignmentStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> methodOrAssignmentStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token DELIM");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token ASSIGN_OPS");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule expression");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule dynPathId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule dynStrLitOrSubExprPath");
        try
        {
            int num = this.input.LA(1);
            int num2;
            if ((num >= 5 && num <= 6) || (num == 10 || num == 13 || num == 16 || num == 19 || (num >= 22 && num <= 23)) || (num == 25 || num == 27 || num == 31 || num == 33 || num == 39 || num == 41 || num == 44 || num == 49 || num == 54 || (num >= 57 && num <= 58)) || (num == 66 || num == 71 || (num >= 79 && num <= 80)) || (num == 83 || (num >= 88 && num <= 91)) || num == 94 || num == 96 || num == 98 || num == 100)
            {
                num2 = 1;
            }
            else if (num == 18 || num == 64 || num == 86)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 29, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._dynPathId_in_methodOrAssignmentStmt1371);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.dynPathId();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope2.Tree);
                        }
                        int num3 = this.input.LA(1);
                        int num4;
                        if (num3 == 35)
                        {
                            num4 = 1;
                        }
                        else if (num3 == 12)
                        {
                            num4 = 2;
                        }
                        else
                        {
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex2 = new NoViableAltException("", 27, 0, this.input);
                            throw ex2;
                        }
                        switch (num4)
                        {
                            case 1:
                                {
                                    IToken el = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_methodOrAssignmentStmt1379);
                                    if (this.state.failed)
                                    {
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        rewriteRuleTokenStream.Add(el);
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        astParserRuleReturnScope.Tree = commonTree;
                                        new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                        commonTree = (CommonTree)this.adaptor.Nil();
                                        CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                                        commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(46, "EXEC"), commonTree2);
                                        this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                                        this.adaptor.AddChild(commonTree, commonTree2);
                                        astParserRuleReturnScope.Tree = commonTree;
                                    }
                                    break;
                                }
                            case 2:
                                {
                                    IToken el2 = (IToken)this.Match(this.input, 12, ByDScriptingLanguageParser.Follow._ASSIGN_OPS_in_methodOrAssignmentStmt1411);
                                    if (this.state.failed)
                                    {
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        rewriteRuleTokenStream2.Add(el2);
                                    }
                                    base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_methodOrAssignmentStmt1413);
                                    AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.expression();
                                    base.PopFollow();
                                    if (this.state.failed)
                                    {
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                                    }
                                    IToken el3 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_methodOrAssignmentStmt1415);
                                    if (this.state.failed)
                                    {
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        rewriteRuleTokenStream.Add(el3);
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        astParserRuleReturnScope.Tree = commonTree;
                                        new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                        commonTree = (CommonTree)this.adaptor.Nil();
                                        CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                                        commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(11, "ASSIGNMENT"), commonTree3);
                                        this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream2.NextTree());
                                        this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream.NextTree());
                                        this.adaptor.AddChild(commonTree, commonTree3);
                                        astParserRuleReturnScope.Tree = commonTree;
                                    }
                                    break;
                                }
                        }
                        break;
                    }
                case 2:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._dynStrLitOrSubExprPath_in_methodOrAssignmentStmt1437);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.dynStrLitOrSubExprPath();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope4.Tree);
                        }
                        int num5 = this.input.LA(1);
                        int num6;
                        if (num5 == 35)
                        {
                            num6 = 1;
                        }
                        else if (num5 == 12)
                        {
                            num6 = 2;
                        }
                        else
                        {
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex3 = new NoViableAltException("", 28, 0, this.input);
                            throw ex3;
                        }
                        switch (num6)
                        {
                            case 1:
                                {
                                    IToken el4 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_methodOrAssignmentStmt1445);
                                    if (this.state.failed)
                                    {
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        rewriteRuleTokenStream.Add(el4);
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        astParserRuleReturnScope.Tree = commonTree;
                                        new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                        commonTree = (CommonTree)this.adaptor.Nil();
                                        CommonTree commonTree4 = (CommonTree)this.adaptor.Nil();
                                        commonTree4 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(46, "EXEC"), commonTree4);
                                        this.adaptor.AddChild(commonTree4, rewriteRuleSubtreeStream3.NextTree());
                                        this.adaptor.AddChild(commonTree, commonTree4);
                                        astParserRuleReturnScope.Tree = commonTree;
                                    }
                                    break;
                                }
                            case 2:
                                {
                                    IToken el5 = (IToken)this.Match(this.input, 12, ByDScriptingLanguageParser.Follow._ASSIGN_OPS_in_methodOrAssignmentStmt1479);
                                    if (this.state.failed)
                                    {
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        rewriteRuleTokenStream2.Add(el5);
                                    }
                                    base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_methodOrAssignmentStmt1481);
                                    AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.expression();
                                    base.PopFollow();
                                    if (this.state.failed)
                                    {
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        rewriteRuleSubtreeStream.Add(astParserRuleReturnScope5.Tree);
                                    }
                                    IToken el6 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_methodOrAssignmentStmt1483);
                                    if (this.state.failed)
                                    {
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        rewriteRuleTokenStream.Add(el6);
                                    }
                                    if (this.state.backtracking == 0)
                                    {
                                        astParserRuleReturnScope.Tree = commonTree;
                                        new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                        commonTree = (CommonTree)this.adaptor.Nil();
                                        CommonTree commonTree5 = (CommonTree)this.adaptor.Nil();
                                        commonTree5 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(11, "ASSIGNMENT"), commonTree5);
                                        this.adaptor.AddChild(commonTree5, rewriteRuleSubtreeStream3.NextTree());
                                        this.adaptor.AddChild(commonTree5, rewriteRuleSubtreeStream.NextTree());
                                        this.adaptor.AddChild(commonTree, commonTree5);
                                        astParserRuleReturnScope.Tree = commonTree;
                                    }
                                    break;
                                }
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex4)
        {
            this.ReportError(ex4);
            this.Recover(this.input, ex4);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex4);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_returnStmt()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_returnStmt()
    {
    }

    [GrammarRule("returnStmt")]
    private AstParserRuleReturnScope<CommonTree, IToken> returnStmt()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token DELIM");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token RETURN");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule expression");
        try
        {
            IToken token = (IToken)this.Match(this.input, 83, ByDScriptingLanguageParser.Follow._RETURN_in_returnStmt1517);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(token);
            }
            int num = 2;
            int num2 = this.input.LA(1);
            if ((num2 >= 5 && num2 <= 7) || (num2 == 10 || num2 == 13 || (num2 >= 16 && num2 <= 19)) || (num2 >= 22 && num2 <= 23) || (num2 == 25 || num2 == 27 || (num2 >= 31 && num2 <= 33)) || (num2 == 39 || num2 == 41 || num2 == 44 || num2 == 49 || num2 == 54 || (num2 >= 57 && num2 <= 58)) || (num2 == 64 || num2 == 66 || num2 == 71 || num2 == 73 || num2 == 75 || (num2 >= 79 && num2 <= 80)) || (num2 == 83 || (num2 >= 86 && num2 <= 91)) || num2 == 94 || num2 == 96 || num2 == 98 || num2 == 100)
            {
                num = 1;
            }
            int num3 = num;
            if (num3 == 1)
            {
                base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_returnStmt1520);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.expression();
                base.PopFollow();
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
                }
            }
            IToken el = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_returnStmt1524);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream2.NextNode(), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream3.NextNode());
                if (rewriteRuleSubtreeStream.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                }
                rewriteRuleSubtreeStream.Reset();
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_breakpoint_span(astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_expression()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_expression()
    {
    }

    [GrammarRule("expression")]
    private AstParserRuleReturnScope<CommonTree, IToken> expression()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule logicalORExpr");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._logicalORExpr_in_expression1550);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.logicalORExpr();
            base.PopFollow();
            if (this.state.failed)
            {
                return astParserRuleReturnScope;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_logicalORExpr()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_logicalORExpr()
    {
    }

    [GrammarRule("logicalORExpr")]
    private AstParserRuleReturnScope<CommonTree, IToken> logicalORExpr()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token OR");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule logicalANDExpr");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._logicalANDExpr_in_logicalORExpr1573);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.logicalANDExpr();
            base.PopFollow();
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 76)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_286;
                }
                IToken token = (IToken)this.Match(this.input, 76, ByDScriptingLanguageParser.Follow._OR_in_logicalORExpr1585);
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(token);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._logicalANDExpr_in_logicalORExpr1587);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.logicalANDExpr();
                base.PopFollow();
                if (this.state.failed)
                {
                    goto Block_11;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                }
                if (this.state.backtracking == 0)
                {
                    astParserRuleReturnScope.Tree = commonTree;
                    RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                    commonTree = (CommonTree)this.adaptor.Nil();
                    CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                    commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree2);
                    this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                    this.adaptor.AddChild(commonTree, commonTree2);
                    astParserRuleReturnScope.Tree = commonTree;
                }
            }
            result = astParserRuleReturnScope;
            return result;
            Block_11:
            result = astParserRuleReturnScope;
            return result;
            IL_286:
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_logicalANDExpr()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_logicalANDExpr()
    {
    }

    [GrammarRule("logicalANDExpr")]
    private AstParserRuleReturnScope<CommonTree, IToken> logicalANDExpr()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token AND");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule equalityExpr");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._equalityExpr_in_logicalANDExpr1615);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.equalityExpr();
            base.PopFollow();
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 8)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_284;
                }
                IToken token = (IToken)this.Match(this.input, 8, ByDScriptingLanguageParser.Follow._AND_in_logicalANDExpr1627);
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(token);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._equalityExpr_in_logicalANDExpr1629);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.equalityExpr();
                base.PopFollow();
                if (this.state.failed)
                {
                    goto Block_11;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                }
                if (this.state.backtracking == 0)
                {
                    astParserRuleReturnScope.Tree = commonTree;
                    RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                    commonTree = (CommonTree)this.adaptor.Nil();
                    CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                    commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree2);
                    this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                    this.adaptor.AddChild(commonTree, commonTree2);
                    astParserRuleReturnScope.Tree = commonTree;
                }
            }
            result = astParserRuleReturnScope;
            return result;
            Block_11:
            result = astParserRuleReturnScope;
            return result;
            IL_284:
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_equalityExpr()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_equalityExpr()
    {
    }

    [GrammarRule("equalityExpr")]
    private AstParserRuleReturnScope<CommonTree, IToken> equalityExpr()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token EQ");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token NE");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule relationalExpr");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._relationalExpr_in_equalityExpr1657);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.relationalExpr();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            int num = 3;
            int num2 = this.input.LA(1);
            if (num2 == 45)
            {
                num = 1;
            }
            else if (num2 == 70)
            {
                num = 2;
            }
            switch (num)
            {
                case 1:
                    {
                        IToken token = (IToken)this.Match(this.input, 45, ByDScriptingLanguageParser.Follow._EQ_in_equalityExpr1674);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._relationalExpr_in_equalityExpr1676);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.relationalExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream3.NextNode());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 2:
                    {
                        IToken token = (IToken)this.Match(this.input, 70, ByDScriptingLanguageParser.Follow._NE_in_equalityExpr1698);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream2.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._relationalExpr_in_equalityExpr1700);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.relationalExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope4.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                            commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree3);
                            this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream4.NextNode());
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream3.NextTree());
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree3);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_relationalExpr()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_relationalExpr()
    {
    }

    [GrammarRule("relationalExpr")]
    private AstParserRuleReturnScope<CommonTree, IToken> relationalExpr()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token GT");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token GE");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token LTT");
        RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token LE");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule addExpr");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._addExpr_in_relationalExpr1731);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.addExpr();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            int num = 5;
            int num2 = this.input.LA(1);
            switch (num2)
            {
                case 51:
                    num = 3;
                    break;
                case 52:
                    num = 1;
                    break;
                default:
                    switch (num2)
                    {
                        case 63:
                            num = 4;
                            break;
                        case 65:
                            num = 2;
                            break;
                    }
                    break;
            }
            switch (num)
            {
                case 1:
                    {
                        IToken token = (IToken)this.Match(this.input, 52, ByDScriptingLanguageParser.Follow._GT_in_relationalExpr1748);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._addExpr_in_relationalExpr1751);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.addExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream5 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream5.NextNode());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 2:
                    {
                        IToken token = (IToken)this.Match(this.input, 65, ByDScriptingLanguageParser.Follow._LTT_in_relationalExpr1773);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream3.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._addExpr_in_relationalExpr1775);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.addExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope4.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream6 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                            commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree3);
                            this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream6.NextNode());
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream3.NextTree());
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree3);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 3:
                    {
                        IToken token = (IToken)this.Match(this.input, 51, ByDScriptingLanguageParser.Follow._GE_in_relationalExpr1797);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream2.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._addExpr_in_relationalExpr1800);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.addExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope5.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream7 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream4 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree4 = (CommonTree)this.adaptor.Nil();
                            commonTree4 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree4);
                            this.adaptor.AddChild(commonTree4, rewriteRuleTokenStream7.NextNode());
                            this.adaptor.AddChild(commonTree4, rewriteRuleSubtreeStream4.NextTree());
                            this.adaptor.AddChild(commonTree4, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree4);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 4:
                    {
                        IToken token = (IToken)this.Match(this.input, 63, ByDScriptingLanguageParser.Follow._LE_in_relationalExpr1822);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream4.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._addExpr_in_relationalExpr1825);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope6 = this.addExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope6.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream8 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream5 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree5 = (CommonTree)this.adaptor.Nil();
                            commonTree5 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree5);
                            this.adaptor.AddChild(commonTree5, rewriteRuleTokenStream8.NextNode());
                            this.adaptor.AddChild(commonTree5, rewriteRuleSubtreeStream5.NextTree());
                            this.adaptor.AddChild(commonTree5, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree5);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_addExpr()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_addExpr()
    {
    }

    [GrammarRule("addExpr")]
    private AstParserRuleReturnScope<CommonTree, IToken> addExpr()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token SUB");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token ADD");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule multExpr");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._multExpr_in_addExpr1856);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.multExpr();
            base.PopFollow();
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            while (true)
            {
                int num = 3;
                int num2 = this.input.LA(1);
                if (num2 == 7)
                {
                    num = 1;
                }
                else if (num2 == 87)
                {
                    num = 2;
                }
                switch (num)
                {
                    case 1:
                        {
                            IToken token = (IToken)this.Match(this.input, 7, ByDScriptingLanguageParser.Follow._ADD_in_addExpr1873);
                            if (this.state.failed)
                            {
                                goto Block_10;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleTokenStream2.Add(token);
                            }
                            base.PushFollow(ByDScriptingLanguageParser.Follow._multExpr_in_addExpr1875);
                            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.multExpr();
                            base.PopFollow();
                            if (this.state.failed)
                            {
                                goto Block_12;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                            }
                            if (this.state.backtracking == 0)
                            {
                                astParserRuleReturnScope.Tree = commonTree;
                                RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                                RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                commonTree = (CommonTree)this.adaptor.Nil();
                                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                                commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree2);
                                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream3.NextNode());
                                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                                this.adaptor.AddChild(commonTree, commonTree2);
                                astParserRuleReturnScope.Tree = commonTree;
                                continue;
                            }
                            continue;
                        }
                    case 2:
                        {
                            IToken token = (IToken)this.Match(this.input, 87, ByDScriptingLanguageParser.Follow._SUB_in_addExpr1897);
                            if (this.state.failed)
                            {
                                goto Block_16;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleTokenStream.Add(token);
                            }
                            base.PushFollow(ByDScriptingLanguageParser.Follow._multExpr_in_addExpr1899);
                            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.multExpr();
                            base.PopFollow();
                            if (this.state.failed)
                            {
                                goto Block_18;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope4.Tree);
                            }
                            if (this.state.backtracking == 0)
                            {
                                astParserRuleReturnScope.Tree = commonTree;
                                RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                                RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                commonTree = (CommonTree)this.adaptor.Nil();
                                CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                                commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree3);
                                this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream4.NextNode());
                                this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream3.NextTree());
                                this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream.NextTree());
                                this.adaptor.AddChild(commonTree, commonTree3);
                                astParserRuleReturnScope.Tree = commonTree;
                                continue;
                            }
                            continue;
                        }
                }
                break;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            return astParserRuleReturnScope;
            Block_10:
            result = astParserRuleReturnScope;
            return result;
            Block_12:
            result = astParserRuleReturnScope;
            return result;
            Block_16:
            result = astParserRuleReturnScope;
            return result;
            Block_18:
            result = astParserRuleReturnScope;
            return result;
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_multExpr()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_multExpr()
    {
    }

    [GrammarRule("multExpr")]
    private AstParserRuleReturnScope<CommonTree, IToken> multExpr()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token DIV");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token MUL");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token MOD");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule unaryExpr");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._unaryExpr_in_multExpr1930);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.unaryExpr();
            base.PopFollow();
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            while (true)
            {
                int num = 4;
                int num2 = this.input.LA(1);
                if (num2 != 36)
                {
                    switch (num2)
                    {
                        case 68:
                            num = 3;
                            break;
                        case 69:
                            num = 1;
                            break;
                    }
                }
                else
                {
                    num = 2;
                }
                switch (num)
                {
                    case 1:
                        {
                            IToken token = (IToken)this.Match(this.input, 69, ByDScriptingLanguageParser.Follow._MUL_in_multExpr1947);
                            if (this.state.failed)
                            {
                                goto Block_10;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleTokenStream2.Add(token);
                            }
                            base.PushFollow(ByDScriptingLanguageParser.Follow._unaryExpr_in_multExpr1949);
                            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.unaryExpr();
                            base.PopFollow();
                            if (this.state.failed)
                            {
                                goto Block_12;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                            }
                            if (this.state.backtracking == 0)
                            {
                                astParserRuleReturnScope.Tree = commonTree;
                                RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                                RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                commonTree = (CommonTree)this.adaptor.Nil();
                                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                                commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree2);
                                this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream4.NextNode());
                                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                                this.adaptor.AddChild(commonTree, commonTree2);
                                astParserRuleReturnScope.Tree = commonTree;
                                continue;
                            }
                            continue;
                        }
                    case 2:
                        {
                            IToken token = (IToken)this.Match(this.input, 36, ByDScriptingLanguageParser.Follow._DIV_in_multExpr1971);
                            if (this.state.failed)
                            {
                                goto Block_16;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleTokenStream.Add(token);
                            }
                            base.PushFollow(ByDScriptingLanguageParser.Follow._unaryExpr_in_multExpr1973);
                            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.unaryExpr();
                            base.PopFollow();
                            if (this.state.failed)
                            {
                                goto Block_18;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope4.Tree);
                            }
                            if (this.state.backtracking == 0)
                            {
                                astParserRuleReturnScope.Tree = commonTree;
                                RewriteRuleTokenStream rewriteRuleTokenStream5 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                                RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                commonTree = (CommonTree)this.adaptor.Nil();
                                CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                                commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree3);
                                this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream5.NextNode());
                                this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream3.NextTree());
                                this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream.NextTree());
                                this.adaptor.AddChild(commonTree, commonTree3);
                                astParserRuleReturnScope.Tree = commonTree;
                                continue;
                            }
                            continue;
                        }
                    case 3:
                        {
                            IToken token = (IToken)this.Match(this.input, 68, ByDScriptingLanguageParser.Follow._MOD_in_multExpr1995);
                            if (this.state.failed)
                            {
                                goto Block_22;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleTokenStream3.Add(token);
                            }
                            base.PushFollow(ByDScriptingLanguageParser.Follow._unaryExpr_in_multExpr1997);
                            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.unaryExpr();
                            base.PopFollow();
                            if (this.state.failed)
                            {
                                goto Block_24;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope5.Tree);
                            }
                            if (this.state.backtracking == 0)
                            {
                                astParserRuleReturnScope.Tree = commonTree;
                                RewriteRuleTokenStream rewriteRuleTokenStream6 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                                RewriteRuleSubtreeStream rewriteRuleSubtreeStream4 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                commonTree = (CommonTree)this.adaptor.Nil();
                                CommonTree commonTree4 = (CommonTree)this.adaptor.Nil();
                                commonTree4 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(14, "BINARY"), commonTree4);
                                this.adaptor.AddChild(commonTree4, rewriteRuleTokenStream6.NextNode());
                                this.adaptor.AddChild(commonTree4, rewriteRuleSubtreeStream4.NextTree());
                                this.adaptor.AddChild(commonTree4, rewriteRuleSubtreeStream.NextTree());
                                this.adaptor.AddChild(commonTree, commonTree4);
                                astParserRuleReturnScope.Tree = commonTree;
                                continue;
                            }
                            continue;
                        }
                }
                break;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            return astParserRuleReturnScope;
            Block_10:
            result = astParserRuleReturnScope;
            return result;
            Block_12:
            result = astParserRuleReturnScope;
            return result;
            Block_16:
            result = astParserRuleReturnScope;
            return result;
            Block_18:
            result = astParserRuleReturnScope;
            return result;
            Block_22:
            result = astParserRuleReturnScope;
            return result;
            Block_24:
            result = astParserRuleReturnScope;
            return result;
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_unaryExpr()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_unaryExpr()
    {
    }

    [GrammarRule("unaryExpr")]
    private AstParserRuleReturnScope<CommonTree, IToken> unaryExpr()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token NOT");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule unaryExpr");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule signPrefix");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule atomExpr");
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num <= 44)
            {
                switch (num)
                {
                    case 5:
                    case 6:
                    case 10:
                    case 13:
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 22:
                    case 23:
                    case 25:
                    case 27:
                    case 31:
                    case 32:
                    case 33:
                        goto IL_200;
                    case 7:
                        break;
                    case 8:
                    case 9:
                    case 11:
                    case 12:
                    case 14:
                    case 15:
                    case 20:
                    case 21:
                    case 24:
                    case 26:
                    case 28:
                    case 29:
                    case 30:
                        goto IL_205;
                    default:
                        switch (num)
                        {
                            case 39:
                            case 41:
                                goto IL_200;
                            case 40:
                                goto IL_205;
                            default:
                                if (num != 44)
                                {
                                    goto IL_205;
                                }
                                goto IL_200;
                        }
                        break;
                }
            }
            else if (num <= 58)
            {
                if (num == 49)
                {
                    goto IL_200;
                }
                switch (num)
                {
                    case 54:
                    case 57:
                    case 58:
                        goto IL_200;
                    case 55:
                    case 56:
                        goto IL_205;
                    default:
                        goto IL_205;
                }
            }
            else
            {
                switch (num)
                {
                    case 64:
                    case 66:
                        goto IL_200;
                    case 65:
                        goto IL_205;
                    default:
                        switch (num)
                        {
                            case 71:
                            case 75:
                            case 79:
                            case 80:
                            case 83:
                            case 86:
                            case 88:
                            case 89:
                            case 90:
                            case 91:
                            case 94:
                            case 96:
                            case 98:
                            case 100:
                                goto IL_200;
                            case 72:
                            case 74:
                            case 76:
                            case 77:
                            case 78:
                            case 81:
                            case 82:
                            case 84:
                            case 85:
                            case 92:
                            case 93:
                            case 95:
                            case 97:
                            case 99:
                                goto IL_205;
                            case 73:
                                num2 = 1;
                                goto IL_23F;
                            case 87:
                                break;
                            default:
                                goto IL_205;
                        }
                        break;
                }
            }
            num2 = 2;
            goto IL_23F;
            IL_200:
            num2 = 3;
            goto IL_23F;
            IL_205:
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 37, 0, this.input);
            throw ex;
            IL_23F:
            switch (num2)
            {
                case 1:
                    {
                        IToken token = (IToken)this.Match(this.input, 73, ByDScriptingLanguageParser.Follow._NOT_in_unaryExpr2029);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._unaryExpr_in_unaryExpr2035);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.unaryExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(95, "UNARY"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 2:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._signPrefix_in_unaryExpr2051);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.signPrefix();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._unaryExpr_in_unaryExpr2053);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.unaryExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope4.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                            commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(95, "UNARY"), commonTree3);
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream2.NextTree());
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree3);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 3:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._atomExpr_in_unaryExpr2079);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.atomExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope5.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream3.NextTree());
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_atomExpr()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_atomExpr()
    {
    }

    [GrammarRule("atomExpr")]
    private AstParserRuleReturnScope<CommonTree, IToken> atomExpr()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            if (num <= 49)
            {
                if (num <= 41)
                {
                    switch (num)
                    {
                        case 5:
                        case 6:
                        case 10:
                        case 13:
                        case 16:
                        case 19:
                        case 22:
                        case 23:
                        case 25:
                        case 27:
                        case 31:
                        case 33:
                            goto IL_1BB;
                        case 7:
                        case 8:
                        case 9:
                        case 11:
                        case 12:
                        case 14:
                        case 15:
                        case 20:
                        case 21:
                        case 24:
                        case 26:
                        case 28:
                        case 29:
                        case 30:
                            goto IL_1C0;
                        case 17:
                        case 32:
                            goto IL_1B6;
                        case 18:
                            break;
                        default:
                            switch (num)
                            {
                                case 39:
                                case 41:
                                    goto IL_1BB;
                                case 40:
                                    goto IL_1C0;
                                default:
                                    goto IL_1C0;
                            }
                            break;
                    }
                }
                else
                {
                    if (num != 44 && num != 49)
                    {
                        goto IL_1C0;
                    }
                    goto IL_1BB;
                }
            }
            else if (num <= 66)
            {
                switch (num)
                {
                    case 54:
                    case 57:
                    case 58:
                        goto IL_1BB;
                    case 55:
                    case 56:
                        goto IL_1C0;
                    default:
                        switch (num)
                        {
                            case 64:
                                break;
                            case 65:
                                goto IL_1C0;
                            case 66:
                                goto IL_1BB;
                            default:
                                goto IL_1C0;
                        }
                        break;
                }
            }
            else
            {
                if (num == 71)
                {
                    goto IL_1BB;
                }
                switch (num)
                {
                    case 75:
                        goto IL_1B6;
                    case 76:
                    case 77:
                    case 78:
                    case 81:
                    case 82:
                    case 84:
                    case 85:
                    case 87:
                        goto IL_1C0;
                    case 79:
                    case 80:
                    case 83:
                    case 88:
                    case 89:
                    case 90:
                    case 91:
                        goto IL_1BB;
                    case 86:
                        break;
                    default:
                        switch (num)
                        {
                            case 94:
                            case 96:
                            case 98:
                            case 100:
                                goto IL_1BB;
                            case 95:
                            case 97:
                            case 99:
                                goto IL_1C0;
                            default:
                                goto IL_1C0;
                        }
                        break;
                }
            }
            int num2 = 1;
            goto IL_1FA;
            IL_1B6:
            num2 = 2;
            goto IL_1FA;
            IL_1BB:
            num2 = 3;
            goto IL_1FA;
            IL_1C0:
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 38, 0, this.input);
            throw ex;
            IL_1FA:
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._dynStrLitOrSubExprPath_in_atomExpr2097);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.dynStrLitOrSubExprPath();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._numericLiteral_in_atomExpr2102);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.numericLiteral();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope3.Tree);
                        }
                        break;
                    }
                case 3:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._dynPathId_in_atomExpr2107);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.dynPathId();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope4.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_dynStrLitOrSubExprPath()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_dynStrLitOrSubExprPath()
    {
    }

    [GrammarRule("dynStrLitOrSubExprPath")]
    private AstParserRuleReturnScope<CommonTree, IToken> dynStrLitOrSubExprPath()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token RPAREN");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token DOT");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token LPAREN");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule funcArgs");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule idxModObjId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule stringLiteral");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream4 = new RewriteRuleSubtreeStream(this.adaptor, "rule idxExpr");
        try
        {
            int num = this.input.LA(1);
            int num2;
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (num == 18 || num == 86)
            {
                num2 = 1;
            }
            else if (num == 64)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 39, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._stringLiteral_in_dynStrLitOrSubExprPath2131);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.stringLiteral();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope2.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream3.NextTree());
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 2:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._idxExpr_in_dynStrLitOrSubExprPath2145);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.idxExpr();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream4.Add(astParserRuleReturnScope3.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream4.NextTree());
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
            }
            while (true)
            {
                int num3 = 2;
                int num4 = this.input.LA(1);
                if (num4 == 37)
                {
                    num3 = 1;
                }
                int num5 = num3;
                if (num5 != 1)
                {
                    goto IL_8B8;
                }
                IToken token = (IToken)this.Match(this.input, 37, ByDScriptingLanguageParser.Follow._DOT_in_dynStrLitOrSubExprPath2173);
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream2.Add(token);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._idxModObjId_in_dynStrLitOrSubExprPath2175);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.idxModObjId();
                base.PopFollow();
                if (this.state.failed)
                {
                    goto Block_19;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope4.Tree);
                }
                if (this.state.backtracking == 0)
                {
                    astParserRuleReturnScope.Tree = commonTree;
                    RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream5 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                    commonTree = (CommonTree)this.adaptor.Nil();
                    CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                    commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(77, "PATH"), commonTree2);
                    this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream4.NextNode());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream5.NextTree());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                    this.adaptor.AddChild(commonTree, commonTree2);
                    astParserRuleReturnScope.Tree = commonTree;
                }
                int num6 = 2;
                int num7 = this.input.LA(1);
                if (num7 == 64)
                {
                    num6 = 1;
                }
                int num8 = num6;
                if (num8 == 1)
                {
                    IToken token2 = (IToken)this.Match(this.input, 64, ByDScriptingLanguageParser.Follow._LPAREN_in_dynStrLitOrSubExprPath2209);
                    if (this.state.failed)
                    {
                        goto Block_25;
                    }
                    if (this.state.backtracking == 0)
                    {
                        rewriteRuleTokenStream3.Add(token2);
                    }
                    int num9 = this.input.LA(1);
                    int num10;
                    if (num9 == 84)
                    {
                        num10 = 1;
                    }
                    else
                    {
                        if ((num9 < 5 || num9 > 7) && (num9 != 10 && num9 != 13 && (num9 < 16 || num9 > 19)) && (num9 < 22 || num9 > 23) && (num9 != 25 && num9 != 27 && (num9 < 31 || num9 > 33)) && (num9 != 39 && num9 != 41 && num9 != 44 && num9 != 49 && num9 != 54 && (num9 < 57 || num9 > 58)) && (num9 != 64 && num9 != 66 && num9 != 71 && num9 != 73 && num9 != 75 && (num9 < 79 || num9 > 80)) && (num9 != 83 && (num9 < 86 || num9 > 91)) && num9 != 94 && num9 != 96 && num9 != 98 && num9 != 100)
                        {
                            goto IL_57D;
                        }
                        num10 = 2;
                    }
                    switch (num10)
                    {
                        case 1:
                            {
                                IToken token3 = (IToken)this.Match(this.input, 84, ByDScriptingLanguageParser.Follow._RPAREN_in_dynStrLitOrSubExprPath2219);
                                if (this.state.failed)
                                {
                                    goto Block_55;
                                }
                                if (this.state.backtracking == 0)
                                {
                                    rewriteRuleTokenStream.Add(token3);
                                }
                                if (this.state.backtracking == 0)
                                {
                                    astParserRuleReturnScope.Tree = commonTree;
                                    RewriteRuleTokenStream rewriteRuleTokenStream5 = new RewriteRuleTokenStream(this.adaptor, "token lpar", token2);
                                    RewriteRuleTokenStream rewriteRuleTokenStream6 = new RewriteRuleTokenStream(this.adaptor, "token rpar", token3);
                                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream6 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                    commonTree = (CommonTree)this.adaptor.Nil();
                                    CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                                    commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(20, "CALL"), commonTree3);
                                    this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream5.NextNode());
                                    this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream6.NextNode());
                                    this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream6.NextTree());
                                    this.adaptor.AddChild(commonTree, commonTree3);
                                    astParserRuleReturnScope.Tree = commonTree;
                                }
                                break;
                            }
                        case 2:
                            {
                                base.PushFollow(ByDScriptingLanguageParser.Follow._funcArgs_in_dynStrLitOrSubExprPath2261);
                                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.funcArgs();
                                base.PopFollow();
                                if (this.state.failed)
                                {
                                    goto Block_59;
                                }
                                if (this.state.backtracking == 0)
                                {
                                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope5.Tree);
                                }
                                IToken token3 = (IToken)this.Match(this.input, 84, ByDScriptingLanguageParser.Follow._RPAREN_in_dynStrLitOrSubExprPath2266);
                                if (this.state.failed)
                                {
                                    goto Block_61;
                                }
                                if (this.state.backtracking == 0)
                                {
                                    rewriteRuleTokenStream.Add(token3);
                                }
                                if (this.state.backtracking == 0)
                                {
                                    astParserRuleReturnScope.Tree = commonTree;
                                    RewriteRuleTokenStream rewriteRuleTokenStream7 = new RewriteRuleTokenStream(this.adaptor, "token lpar", token2);
                                    RewriteRuleTokenStream rewriteRuleTokenStream8 = new RewriteRuleTokenStream(this.adaptor, "token rpar", token3);
                                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream7 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                    commonTree = (CommonTree)this.adaptor.Nil();
                                    CommonTree commonTree4 = (CommonTree)this.adaptor.Nil();
                                    commonTree4 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(20, "CALL"), commonTree4);
                                    this.adaptor.AddChild(commonTree4, rewriteRuleTokenStream7.NextNode());
                                    this.adaptor.AddChild(commonTree4, rewriteRuleTokenStream8.NextNode());
                                    this.adaptor.AddChild(commonTree4, rewriteRuleSubtreeStream7.NextTree());
                                    this.adaptor.AddChild(commonTree4, rewriteRuleSubtreeStream.NextTree());
                                    this.adaptor.AddChild(commonTree, commonTree4);
                                    astParserRuleReturnScope.Tree = commonTree;
                                }
                                break;
                            }
                    }
                }
            }
            result = astParserRuleReturnScope;
            return result;
            Block_19:
            result = astParserRuleReturnScope;
            return result;
            Block_25:
            result = astParserRuleReturnScope;
            return result;
            IL_57D:
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex2 = new NoViableAltException("", 40, 0, this.input);
            throw ex2;
            Block_55:
            result = astParserRuleReturnScope;
            return result;
            Block_59:
            result = astParserRuleReturnScope;
            return result;
            Block_61:
            result = astParserRuleReturnScope;
            return result;
            IL_8B8:
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_tooltip_path(astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex3)
        {
            this.ReportError(ex3);
            this.Recover(this.input, ex3);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex3);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_dynPathId()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_dynPathId()
    {
    }

    [GrammarRule("dynPathId")]
    private AstParserRuleReturnScope<CommonTree, IToken> dynPathId()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token RPAREN");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token DOT");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token LPAREN");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule funcArgs");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule idxModObjId");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._idxModObjId_in_dynPathId2372);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.idxModObjId();
            base.PopFollow();
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream2.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 37)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_763;
                }
                IToken token = (IToken)this.Match(this.input, 37, ByDScriptingLanguageParser.Follow._DOT_in_dynPathId2390);
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream2.Add(token);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._idxModObjId_in_dynPathId2392);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.idxModObjId();
                base.PopFollow();
                if (this.state.failed)
                {
                    goto Block_11;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
                }
                if (this.state.backtracking == 0)
                {
                    astParserRuleReturnScope.Tree = commonTree;
                    RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                    commonTree = (CommonTree)this.adaptor.Nil();
                    CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                    commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(77, "PATH"), commonTree2);
                    this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream4.NextNode());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream3.NextTree());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                    this.adaptor.AddChild(commonTree, commonTree2);
                    astParserRuleReturnScope.Tree = commonTree;
                }
                int num4 = 2;
                int num5 = this.input.LA(1);
                if (num5 == 64)
                {
                    num4 = 1;
                }
                int num6 = num4;
                if (num6 == 1)
                {
                    IToken token2 = (IToken)this.Match(this.input, 64, ByDScriptingLanguageParser.Follow._LPAREN_in_dynPathId2426);
                    if (this.state.failed)
                    {
                        goto Block_17;
                    }
                    if (this.state.backtracking == 0)
                    {
                        rewriteRuleTokenStream3.Add(token2);
                    }
                    int num7 = this.input.LA(1);
                    int num8;
                    if (num7 == 84)
                    {
                        num8 = 1;
                    }
                    else
                    {
                        if ((num7 < 5 || num7 > 7) && (num7 != 10 && num7 != 13 && (num7 < 16 || num7 > 19)) && (num7 < 22 || num7 > 23) && (num7 != 25 && num7 != 27 && (num7 < 31 || num7 > 33)) && (num7 != 39 && num7 != 41 && num7 != 44 && num7 != 49 && num7 != 54 && (num7 < 57 || num7 > 58)) && (num7 != 64 && num7 != 66 && num7 != 71 && num7 != 73 && num7 != 75 && (num7 < 79 || num7 > 80)) && (num7 != 83 && (num7 < 86 || num7 > 91)) && num7 != 94 && num7 != 96 && num7 != 98 && num7 != 100)
                        {
                            goto IL_428;
                        }
                        num8 = 2;
                    }
                    switch (num8)
                    {
                        case 1:
                            {
                                IToken token3 = (IToken)this.Match(this.input, 84, ByDScriptingLanguageParser.Follow._RPAREN_in_dynPathId2436);
                                if (this.state.failed)
                                {
                                    goto Block_47;
                                }
                                if (this.state.backtracking == 0)
                                {
                                    rewriteRuleTokenStream.Add(token3);
                                }
                                if (this.state.backtracking == 0)
                                {
                                    astParserRuleReturnScope.Tree = commonTree;
                                    RewriteRuleTokenStream rewriteRuleTokenStream5 = new RewriteRuleTokenStream(this.adaptor, "token lpar", token2);
                                    RewriteRuleTokenStream rewriteRuleTokenStream6 = new RewriteRuleTokenStream(this.adaptor, "token rpar", token3);
                                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream4 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                    commonTree = (CommonTree)this.adaptor.Nil();
                                    CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                                    commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(20, "CALL"), commonTree3);
                                    this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream5.NextNode());
                                    this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream6.NextNode());
                                    this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream4.NextTree());
                                    this.adaptor.AddChild(commonTree, commonTree3);
                                    astParserRuleReturnScope.Tree = commonTree;
                                }
                                break;
                            }
                        case 2:
                            {
                                base.PushFollow(ByDScriptingLanguageParser.Follow._funcArgs_in_dynPathId2478);
                                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.funcArgs();
                                base.PopFollow();
                                if (this.state.failed)
                                {
                                    goto Block_51;
                                }
                                if (this.state.backtracking == 0)
                                {
                                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope4.Tree);
                                }
                                IToken token3 = (IToken)this.Match(this.input, 84, ByDScriptingLanguageParser.Follow._RPAREN_in_dynPathId2483);
                                if (this.state.failed)
                                {
                                    goto Block_53;
                                }
                                if (this.state.backtracking == 0)
                                {
                                    rewriteRuleTokenStream.Add(token3);
                                }
                                if (this.state.backtracking == 0)
                                {
                                    astParserRuleReturnScope.Tree = commonTree;
                                    RewriteRuleTokenStream rewriteRuleTokenStream7 = new RewriteRuleTokenStream(this.adaptor, "token lpar", token2);
                                    RewriteRuleTokenStream rewriteRuleTokenStream8 = new RewriteRuleTokenStream(this.adaptor, "token rpar", token3);
                                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream5 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                                    commonTree = (CommonTree)this.adaptor.Nil();
                                    CommonTree commonTree4 = (CommonTree)this.adaptor.Nil();
                                    commonTree4 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(20, "CALL"), commonTree4);
                                    this.adaptor.AddChild(commonTree4, rewriteRuleTokenStream7.NextNode());
                                    this.adaptor.AddChild(commonTree4, rewriteRuleTokenStream8.NextNode());
                                    this.adaptor.AddChild(commonTree4, rewriteRuleSubtreeStream5.NextTree());
                                    this.adaptor.AddChild(commonTree4, rewriteRuleSubtreeStream.NextTree());
                                    this.adaptor.AddChild(commonTree, commonTree4);
                                    astParserRuleReturnScope.Tree = commonTree;
                                }
                                break;
                            }
                    }
                }
            }
            result = astParserRuleReturnScope;
            return result;
            Block_11:
            result = astParserRuleReturnScope;
            return result;
            Block_17:
            result = astParserRuleReturnScope;
            return result;
            IL_428:
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 43, 0, this.input);
            throw ex;
            Block_47:
            result = astParserRuleReturnScope;
            return result;
            Block_51:
            result = astParserRuleReturnScope;
            return result;
            Block_53:
            result = astParserRuleReturnScope;
            return result;
            IL_763:
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_tooltip_path(astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_funcArgs()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_funcArgs()
    {
    }

    [GrammarRule("funcArgs")]
    private AstParserRuleReturnScope<CommonTree, IToken> funcArgs()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token LAMBDA_OPS");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token COMMA");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule expression");
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num <= 44)
            {
                switch (num)
                {
                    case 5:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex = new NoViableAltException("", 47, 9, this.input);
                            throw ex;
                        }
                    case 6:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex2 = new NoViableAltException("", 47, 37, this.input);
                            throw ex2;
                        }
                    case 7:
                    case 17:
                    case 18:
                    case 32:
                        break;
                    case 8:
                    case 9:
                    case 11:
                    case 12:
                    case 14:
                    case 15:
                    case 20:
                    case 21:
                    case 24:
                    case 26:
                    case 28:
                    case 29:
                    case 30:
                        goto IL_1101;
                    case 10:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex3 = new NoViableAltException("", 47, 10, this.input);
                            throw ex3;
                        }
                    case 13:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex4 = new NoViableAltException("", 47, 35, this.input);
                            throw ex4;
                        }
                    case 16:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex5 = new NoViableAltException("", 47, 21, this.input);
                            throw ex5;
                        }
                    case 19:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex6 = new NoViableAltException("", 47, 30, this.input);
                            throw ex6;
                        }
                    case 22:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex7 = new NoViableAltException("", 47, 38, this.input);
                            throw ex7;
                        }
                    case 23:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex8 = new NoViableAltException("", 47, 15, this.input);
                            throw ex8;
                        }
                    case 25:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex9 = new NoViableAltException("", 47, 29, this.input);
                            throw ex9;
                        }
                    case 27:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex10 = new NoViableAltException("", 47, 24, this.input);
                            throw ex10;
                        }
                    case 31:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex11 = new NoViableAltException("", 47, 20, this.input);
                            throw ex11;
                        }
                    case 33:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex12 = new NoViableAltException("", 47, 16, this.input);
                            throw ex12;
                        }
                    default:
                        switch (num)
                        {
                            case 39:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex13 = new NoViableAltException("", 47, 33, this.input);
                                    throw ex13;
                                }
                            case 40:
                                goto IL_1101;
                            case 41:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex14 = new NoViableAltException("", 47, 13, this.input);
                                    throw ex14;
                                }
                            default:
                                {
                                    if (num != 44)
                                    {
                                        goto IL_1101;
                                    }
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex15 = new NoViableAltException("", 47, 25, this.input);
                                    throw ex15;
                                }
                        }
                        break;
                }
            }
            else if (num <= 58)
            {
                if (num != 49)
                {
                    switch (num)
                    {
                        case 54:
                            {
                                this.input.LA(2);
                                if (this.input.LA(2) != 60)
                                {
                                    num2 = 1;
                                    goto IL_113B;
                                }
                                if (this.input.LA(2) == 60)
                                {
                                    num2 = 2;
                                    goto IL_113B;
                                }
                                if (this.state.backtracking > 0)
                                {
                                    this.state.failed = true;
                                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                    return result;
                                }
                                NoViableAltException ex16 = new NoViableAltException("", 47, 12, this.input);
                                throw ex16;
                            }
                        case 55:
                        case 56:
                            goto IL_1101;
                        case 57:
                            {
                                this.input.LA(2);
                                if (this.input.LA(2) != 60)
                                {
                                    num2 = 1;
                                    goto IL_113B;
                                }
                                if (this.input.LA(2) == 60)
                                {
                                    num2 = 2;
                                    goto IL_113B;
                                }
                                if (this.state.backtracking > 0)
                                {
                                    this.state.failed = true;
                                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                    return result;
                                }
                                NoViableAltException ex17 = new NoViableAltException("", 47, 11, this.input);
                                throw ex17;
                            }
                        case 58:
                            {
                                this.input.LA(2);
                                if (this.input.LA(2) != 60)
                                {
                                    num2 = 1;
                                    goto IL_113B;
                                }
                                if (this.input.LA(2) == 60)
                                {
                                    num2 = 2;
                                    goto IL_113B;
                                }
                                if (this.state.backtracking > 0)
                                {
                                    this.state.failed = true;
                                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                    return result;
                                }
                                NoViableAltException ex18 = new NoViableAltException("", 47, 19, this.input);
                                throw ex18;
                            }
                        default:
                            goto IL_1101;
                    }
                }
                else
                {
                    this.input.LA(2);
                    if (this.input.LA(2) != 60)
                    {
                        num2 = 1;
                        goto IL_113B;
                    }
                    if (this.input.LA(2) == 60)
                    {
                        num2 = 2;
                        goto IL_113B;
                    }
                    if (this.state.backtracking > 0)
                    {
                        this.state.failed = true;
                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                        return result;
                    }
                    NoViableAltException ex19 = new NoViableAltException("", 47, 18, this.input);
                    throw ex19;
                }
            }
            else
            {
                switch (num)
                {
                    case 64:
                        break;
                    case 65:
                        goto IL_1101;
                    case 66:
                        {
                            this.input.LA(2);
                            if (this.input.LA(2) != 60)
                            {
                                num2 = 1;
                                goto IL_113B;
                            }
                            if (this.input.LA(2) == 60)
                            {
                                num2 = 2;
                                goto IL_113B;
                            }
                            if (this.state.backtracking > 0)
                            {
                                this.state.failed = true;
                                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                return result;
                            }
                            NoViableAltException ex20 = new NoViableAltException("", 47, 31, this.input);
                            throw ex20;
                        }
                    default:
                        switch (num)
                        {
                            case 71:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex21 = new NoViableAltException("", 47, 32, this.input);
                                    throw ex21;
                                }
                            case 72:
                            case 74:
                            case 76:
                            case 77:
                            case 78:
                            case 81:
                            case 82:
                            case 84:
                            case 85:
                            case 92:
                            case 93:
                            case 95:
                            case 97:
                            case 99:
                                goto IL_1101;
                            case 73:
                            case 75:
                            case 86:
                            case 87:
                                break;
                            case 79:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex22 = new NoViableAltException("", 47, 22, this.input);
                                    throw ex22;
                                }
                            case 80:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex23 = new NoViableAltException("", 47, 34, this.input);
                                    throw ex23;
                                }
                            case 83:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex24 = new NoViableAltException("", 47, 27, this.input);
                                    throw ex24;
                                }
                            case 88:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex25 = new NoViableAltException("", 47, 14, this.input);
                                    throw ex25;
                                }
                            case 89:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex26 = new NoViableAltException("", 47, 40, this.input);
                                    throw ex26;
                                }
                            case 90:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex27 = new NoViableAltException("", 47, 36, this.input);
                                    throw ex27;
                                }
                            case 91:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex28 = new NoViableAltException("", 47, 28, this.input);
                                    throw ex28;
                                }
                            case 94:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex29 = new NoViableAltException("", 47, 26, this.input);
                                    throw ex29;
                                }
                            case 96:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex30 = new NoViableAltException("", 47, 39, this.input);
                                    throw ex30;
                                }
                            case 98:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex31 = new NoViableAltException("", 47, 23, this.input);
                                    throw ex31;
                                }
                            case 100:
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) != 60)
                                    {
                                        num2 = 1;
                                        goto IL_113B;
                                    }
                                    if (this.input.LA(2) == 60)
                                    {
                                        num2 = 2;
                                        goto IL_113B;
                                    }
                                    if (this.state.backtracking > 0)
                                    {
                                        this.state.failed = true;
                                        AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                                        return result;
                                    }
                                    NoViableAltException ex32 = new NoViableAltException("", 47, 17, this.input);
                                    throw ex32;
                                }
                            default:
                                goto IL_1101;
                        }
                        break;
                }
            }
            num2 = 1;
            goto IL_113B;
            IL_1101:
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex33 = new NoViableAltException("", 47, 0, this.input);
            throw ex33;
            IL_113B:
            switch (num2)
            {
                case 1:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_funcArgs2584);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.expression();
                        base.PopFollow();
                        AstParserRuleReturnScope<CommonTree, IToken> result;
                        if (this.state.failed)
                        {
                            result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope2.Tree);
                        }
                        while (true)
                        {
                            int num3 = 2;
                            int num4 = this.input.LA(1);
                            if (num4 == 30)
                            {
                                num3 = 1;
                            }
                            int num5 = num3;
                            if (num5 != 1)
                            {
                                goto IL_1258;
                            }
                            IToken el = (IToken)this.Match(this.input, 30, ByDScriptingLanguageParser.Follow._COMMA_in_funcArgs2587);
                            if (this.state.failed)
                            {
                                break;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleTokenStream2.Add(el);
                            }
                            base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_funcArgs2589);
                            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.expression();
                            base.PopFollow();
                            if (this.state.failed)
                            {
                                goto Block_116;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
                            }
                        }
                        result = astParserRuleReturnScope;
                        return result;
                        Block_116:
                        result = astParserRuleReturnScope;
                        return result;
                        IL_1258:
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            while (rewriteRuleSubtreeStream2.HasNext)
                            {
                                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream2.NextTree());
                            }
                            rewriteRuleSubtreeStream2.Reset();
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 2:
                    if (this.input.LA(2) != 60)
                    {
                        if (this.state.backtracking > 0)
                        {
                            this.state.failed = true;
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        throw new FailedPredicateException(this.input, "funcArgs", " input.LA(2) == LAMBDA_OPS ");
                    }
                    else
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_funcArgs2632);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.fullId();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope4.Tree);
                        }
                        IToken token = (IToken)this.Match(this.input, 60, ByDScriptingLanguageParser.Follow._LAMBDA_OPS_in_funcArgs2636);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(token);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_funcArgs2638);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.expression();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope5.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token op", token);
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(59, "LAMBDA"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream3.NextNode());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                    }
                    break;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex34)
        {
            this.ReportError(ex34);
            this.Recover(this.input, ex34);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex34);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_idxArg()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_idxArg()
    {
    }

    [GrammarRule("idxArg")]
    private AstParserRuleReturnScope<CommonTree, IToken> idxArg()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        try
        {
            CommonTree commonTree = (CommonTree)this.adaptor.Nil();
            base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_idxArg2662);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.expression();
            base.PopFollow();
            if (this.state.failed)
            {
                return astParserRuleReturnScope;
            }
            if (this.state.backtracking == 0)
            {
                this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_idxExpr()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_idxExpr()
    {
    }

    [GrammarRule("idxExpr")]
    private AstParserRuleReturnScope<CommonTree, IToken> idxExpr()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token RPAREN");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token LPAREN");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule expression");
        try
        {
            IToken el = (IToken)this.Match(this.input, 64, ByDScriptingLanguageParser.Follow._LPAREN_in_idxExpr2672);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._expression_in_idxExpr2674);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.expression();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            IToken el2 = (IToken)this.Match(this.input, 84, ByDScriptingLanguageParser.Follow._RPAREN_in_idxExpr2676);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el2);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_idxModObjId()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_idxModObjId()
    {
    }

    [GrammarRule("idxModObjId")]
    private AstParserRuleReturnScope<CommonTree, IToken> idxModObjId()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule modObjId");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._modObjId_in_idxModObjId2695);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.modObjId();
            base.PopFollow();
            if (this.state.failed)
            {
                return astParserRuleReturnScope;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_modObjId()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_modObjId()
    {
    }

    [GrammarRule("modObjId")]
    private AstParserRuleReturnScope<CommonTree, IToken> modObjId()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token COLON2");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule absoluteId");
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num <= 41)
            {
                if (num <= 13)
                {
                    switch (num)
                    {
                        case 5:
                            this.input.LA(2);
                            if (this.input.LA(2) == 29)
                            {
                                num2 = 1;
                                goto IL_7A4;
                            }
                            num2 = 2;
                            goto IL_7A4;
                        case 6:
                            this.input.LA(2);
                            if (this.input.LA(2) == 29)
                            {
                                num2 = 1;
                                goto IL_7A4;
                            }
                            num2 = 2;
                            goto IL_7A4;
                        default:
                            if (num != 10)
                            {
                                if (num == 13)
                                {
                                    this.input.LA(2);
                                    if (this.input.LA(2) == 29)
                                    {
                                        num2 = 1;
                                        goto IL_7A4;
                                    }
                                    num2 = 2;
                                    goto IL_7A4;
                                }
                            }
                            else
                            {
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                            }
                            break;
                    }
                }
                else if (num <= 27)
                {
                    if (num != 16)
                    {
                        switch (num)
                        {
                            case 19:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                            case 22:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                            case 23:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                            case 25:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                            case 27:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                        }
                    }
                    else
                    {
                        this.input.LA(2);
                        if (this.input.LA(2) == 29)
                        {
                            num2 = 1;
                            goto IL_7A4;
                        }
                        num2 = 2;
                        goto IL_7A4;
                    }
                }
                else
                {
                    switch (num)
                    {
                        case 31:
                            this.input.LA(2);
                            if (this.input.LA(2) == 29)
                            {
                                num2 = 1;
                                goto IL_7A4;
                            }
                            num2 = 2;
                            goto IL_7A4;
                        case 32:
                            break;
                        case 33:
                            this.input.LA(2);
                            if (this.input.LA(2) == 29)
                            {
                                num2 = 1;
                                goto IL_7A4;
                            }
                            num2 = 2;
                            goto IL_7A4;
                        default:
                            switch (num)
                            {
                                case 39:
                                    this.input.LA(2);
                                    if (this.input.LA(2) == 29)
                                    {
                                        num2 = 1;
                                        goto IL_7A4;
                                    }
                                    num2 = 2;
                                    goto IL_7A4;
                                case 41:
                                    this.input.LA(2);
                                    if (this.input.LA(2) == 29)
                                    {
                                        num2 = 1;
                                        goto IL_7A4;
                                    }
                                    num2 = 2;
                                    goto IL_7A4;
                            }
                            break;
                    }
                }
            }
            else if (num <= 58)
            {
                if (num != 44)
                {
                    if (num != 49)
                    {
                        switch (num)
                        {
                            case 54:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                            case 57:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                            case 58:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                        }
                    }
                    else
                    {
                        this.input.LA(2);
                        if (this.input.LA(2) == 29)
                        {
                            num2 = 1;
                            goto IL_7A4;
                        }
                        num2 = 2;
                        goto IL_7A4;
                    }
                }
                else
                {
                    this.input.LA(2);
                    if (this.input.LA(2) == 29)
                    {
                        num2 = 1;
                        goto IL_7A4;
                    }
                    num2 = 2;
                    goto IL_7A4;
                }
            }
            else if (num <= 71)
            {
                if (num != 66)
                {
                    if (num == 71)
                    {
                        this.input.LA(2);
                        if (this.input.LA(2) == 29)
                        {
                            num2 = 1;
                            goto IL_7A4;
                        }
                        num2 = 2;
                        goto IL_7A4;
                    }
                }
                else
                {
                    this.input.LA(2);
                    if (this.input.LA(2) == 29)
                    {
                        num2 = 1;
                        goto IL_7A4;
                    }
                    num2 = 2;
                    goto IL_7A4;
                }
            }
            else
            {
                switch (num)
                {
                    case 79:
                        this.input.LA(2);
                        if (this.input.LA(2) == 29)
                        {
                            num2 = 1;
                            goto IL_7A4;
                        }
                        num2 = 2;
                        goto IL_7A4;
                    case 80:
                        this.input.LA(2);
                        if (this.input.LA(2) == 29)
                        {
                            num2 = 1;
                            goto IL_7A4;
                        }
                        num2 = 2;
                        goto IL_7A4;
                    case 81:
                    case 82:
                    case 84:
                    case 85:
                    case 86:
                    case 87:
                        break;
                    case 83:
                        this.input.LA(2);
                        if (this.input.LA(2) == 29)
                        {
                            num2 = 1;
                            goto IL_7A4;
                        }
                        num2 = 2;
                        goto IL_7A4;
                    case 88:
                        this.input.LA(2);
                        if (this.input.LA(2) == 29)
                        {
                            num2 = 1;
                            goto IL_7A4;
                        }
                        num2 = 2;
                        goto IL_7A4;
                    case 89:
                        this.input.LA(2);
                        if (this.input.LA(2) == 29)
                        {
                            num2 = 1;
                            goto IL_7A4;
                        }
                        num2 = 2;
                        goto IL_7A4;
                    case 90:
                        this.input.LA(2);
                        if (this.input.LA(2) == 29)
                        {
                            num2 = 1;
                            goto IL_7A4;
                        }
                        num2 = 2;
                        goto IL_7A4;
                    case 91:
                        this.input.LA(2);
                        if (this.input.LA(2) == 29)
                        {
                            num2 = 1;
                            goto IL_7A4;
                        }
                        num2 = 2;
                        goto IL_7A4;
                    default:
                        switch (num)
                        {
                            case 94:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                            case 96:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                            case 98:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                            case 100:
                                this.input.LA(2);
                                if (this.input.LA(2) == 29)
                                {
                                    num2 = 1;
                                    goto IL_7A4;
                                }
                                num2 = 2;
                                goto IL_7A4;
                        }
                        break;
                }
            }
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 48, 0, this.input);
            throw ex;
            IL_7A4:
            switch (num2)
            {
                case 1:
                    if (this.input.LA(2) != 29)
                    {
                        if (this.state.backtracking > 0)
                        {
                            this.state.failed = true;
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        throw new FailedPredicateException(this.input, "modObjId", " input.LA(2) == COLON2 ");
                    }
                    else
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_modObjId2717);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        IToken el = (IToken)this.Match(this.input, 29, ByDScriptingLanguageParser.Follow._COLON2_in_modObjId2724);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(el);
                        }
                        base.PushFollow(ByDScriptingLanguageParser.Follow._absoluteId_in_modObjId2726);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.absoluteId();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(78, "QUALID"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream3.NextTree());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                    }
                    break;
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._absoluteId_in_modObjId2742);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.absoluteId();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope4.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_absoluteId()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_absoluteId()
    {
    }

    [GrammarRule("absoluteId")]
    private AstParserRuleReturnScope<CommonTree, IToken> absoluteId()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token COLON");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_absoluteId2754);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            int num = 2;
            int num2 = this.input.LA(1);
            if (num2 == 28)
            {
                num = 1;
            }
            int num3 = num;
            if (num3 == 1)
            {
                IToken token = (IToken)this.Match(this.input, 28, ByDScriptingLanguageParser.Follow._COLON_in_absoluteId2764);
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(token);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_absoluteId2766);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.fullId();
                base.PopFollow();
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                }
                if (this.state.backtracking == 0)
                {
                    astParserRuleReturnScope.Tree = commonTree;
                    RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                    commonTree = (CommonTree)this.adaptor.Nil();
                    CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                    commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(4, "ABSID"), commonTree2);
                    this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream2.NextNode());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                    this.adaptor.AddChild(commonTree, commonTree2);
                    astParserRuleReturnScope.Tree = commonTree;
                }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_stringLiteral()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_stringLiteral()
    {
    }

    [GrammarRule("stringLiteral")]
    private AstParserRuleReturnScope<CommonTree, IToken> stringLiteral()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num == 86)
            {
                num2 = 1;
            }
            else if (num == 18 && this.EvaluatePredicate(new Action(this.synpred8_ByDScriptingLanguage_fragment)))
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 50, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken payload = (IToken)this.Match(this.input, 86, ByDScriptingLanguageParser.Follow._STRLIT_in_stringLiteral2793);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(payload);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._brokenStrLit_in_stringLiteral2803);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.brokenStrLit();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_numericLiteral()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_numericLiteral()
    {
    }

    [GrammarRule("numericLiteral")]
    private AstParserRuleReturnScope<CommonTree, IToken> numericLiteral()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num == 32)
            {
                num2 = 1;
            }
            else if (num == 75)
            {
                num2 = 2;
            }
            else if (num == 17 && this.EvaluatePredicate(new Action(this.synpred9_ByDScriptingLanguage_fragment)))
            {
                num2 = 3;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 51, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken payload = (IToken)this.Match(this.input, 32, ByDScriptingLanguageParser.Follow._DECLIT_in_numericLiteral2814);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(payload);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken payload2 = (IToken)this.Match(this.input, 75, ByDScriptingLanguageParser.Follow._NUMLIT_in_numericLiteral2819);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child2 = (CommonTree)this.adaptor.Create(payload2);
                            this.adaptor.AddChild(commonTree, child2);
                        }
                        break;
                    }
                case 3:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._brokenNumLit_in_numericLiteral2829);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.brokenNumLit();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_unmatchedElse()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_unmatchedElse()
    {
    }

    [GrammarRule("unmatchedElse")]
    private AstParserRuleReturnScope<CommonTree, IToken> unmatchedElse()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        try
        {
            CommonTree commonTree = (CommonTree)this.adaptor.Nil();
            IToken payload = (IToken)this.Match(this.input, 41, ByDScriptingLanguageParser.Follow._ELSE_in_unmatchedElse2842);
            if (this.state.failed)
            {
                return astParserRuleReturnScope;
            }
            if (this.state.backtracking == 0)
            {
                CommonTree child = (CommonTree)this.adaptor.Create(payload);
                this.adaptor.AddChild(commonTree, child);
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_unmatchedCase()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_unmatchedCase()
    {
    }

    [GrammarRule("unmatchedCase")]
    private AstParserRuleReturnScope<CommonTree, IToken> unmatchedCase()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        try
        {
            CommonTree commonTree = (CommonTree)this.adaptor.Nil();
            IToken payload = (IToken)this.Match(this.input, 23, ByDScriptingLanguageParser.Follow._CASE_in_unmatchedCase2859);
            if (this.state.failed)
            {
                return astParserRuleReturnScope;
            }
            if (this.state.backtracking == 0)
            {
                CommonTree child = (CommonTree)this.adaptor.Create(payload);
                this.adaptor.AddChild(commonTree, child);
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_unmatchedDefault()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_unmatchedDefault()
    {
    }

    [GrammarRule("unmatchedDefault")]
    private AstParserRuleReturnScope<CommonTree, IToken> unmatchedDefault()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        try
        {
            CommonTree commonTree = (CommonTree)this.adaptor.Nil();
            IToken payload = (IToken)this.Match(this.input, 33, ByDScriptingLanguageParser.Follow._DEFAULT_in_unmatchedDefault2876);
            if (this.state.failed)
            {
                return astParserRuleReturnScope;
            }
            if (this.state.backtracking == 0)
            {
                CommonTree child = (CommonTree)this.adaptor.Create(payload);
                this.adaptor.AddChild(commonTree, child);
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_unexpOrErrLiteral()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_unexpOrErrLiteral()
    {
    }

    [GrammarRule("unexpOrErrLiteral")]
    private AstParserRuleReturnScope<CommonTree, IToken> unexpOrErrLiteral()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num <= 32)
            {
                switch (num)
                {
                    case 17:
                        num2 = 5;
                        goto IL_B4;
                    case 18:
                        num2 = 4;
                        goto IL_B4;
                    default:
                        if (num == 32)
                        {
                            num2 = 2;
                            goto IL_B4;
                        }
                        break;
                }
            }
            else
            {
                if (num == 75)
                {
                    num2 = 3;
                    goto IL_B4;
                }
                if (num == 86)
                {
                    num2 = 1;
                    goto IL_B4;
                }
            }
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 52, 0, this.input);
            throw ex;
            IL_B4:
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken payload = (IToken)this.Match(this.input, 86, ByDScriptingLanguageParser.Follow._STRLIT_in_unexpOrErrLiteral2893);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(payload);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken payload = (IToken)this.Match(this.input, 32, ByDScriptingLanguageParser.Follow._DECLIT_in_unexpOrErrLiteral2904);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(payload);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        break;
                    }
                case 3:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken payload = (IToken)this.Match(this.input, 75, ByDScriptingLanguageParser.Follow._NUMLIT_in_unexpOrErrLiteral2915);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(payload);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        break;
                    }
                case 4:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._brokenStrLit_in_unexpOrErrLiteral2924);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.brokenStrLit();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
                        }
                        break;
                    }
                case 5:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._brokenNumLit_in_unexpOrErrLiteral2929);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.brokenNumLit();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope3.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_brokenStrLit()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_brokenStrLit()
    {
    }

    [GrammarRule("brokenStrLit")]
    private AstParserRuleReturnScope<CommonTree, IToken> brokenStrLit()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        try
        {
            CommonTree commonTree = (CommonTree)this.adaptor.Nil();
            IToken payload = (IToken)this.Match(this.input, 18, ByDScriptingLanguageParser.Follow._BROKEN_STRLIT_in_brokenStrLit2942);
            if (this.state.failed)
            {
                return astParserRuleReturnScope;
            }
            if (this.state.backtracking == 0)
            {
                CommonTree child = (CommonTree)this.adaptor.Create(payload);
                this.adaptor.AddChild(commonTree, child);
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_brokenNumLit()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_brokenNumLit()
    {
    }

    [GrammarRule("brokenNumLit")]
    private AstParserRuleReturnScope<CommonTree, IToken> brokenNumLit()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        try
        {
            CommonTree commonTree = (CommonTree)this.adaptor.Nil();
            IToken payload = (IToken)this.Match(this.input, 17, ByDScriptingLanguageParser.Follow._BROKEN_NUMLIT_in_brokenNumLit2960);
            if (this.state.failed)
            {
                return astParserRuleReturnScope;
            }
            if (this.state.backtracking == 0)
            {
                CommonTree child = (CommonTree)this.adaptor.Create(payload);
                this.adaptor.AddChild(commonTree, child);
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_debugExpression()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_debugExpression()
    {
    }

    [GrammarRule("debugExpression")]
    private AstParserRuleReturnScope<CommonTree, IToken> debugExpression()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token DOT");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token EOF");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule modObjId");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._modObjId_in_debugExpression2977);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.modObjId();
            base.PopFollow();
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 37)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_29D;
                }
                IToken token = (IToken)this.Match(this.input, 37, ByDScriptingLanguageParser.Follow._DOT_in_debugExpression2989);
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(token);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._modObjId_in_debugExpression2991);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.modObjId();
                base.PopFollow();
                if (this.state.failed)
                {
                    goto Block_11;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                }
                if (this.state.backtracking == 0)
                {
                    astParserRuleReturnScope.Tree = commonTree;
                    RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                    commonTree = (CommonTree)this.adaptor.Nil();
                    CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                    commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(77, "PATH"), commonTree2);
                    this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream3.NextNode());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                    this.adaptor.AddChild(commonTree, commonTree2);
                    astParserRuleReturnScope.Tree = commonTree;
                }
            }
            result = astParserRuleReturnScope;
            return result;
            Block_11:
            result = astParserRuleReturnScope;
            return result;
            IL_29D:
            IToken el = (IToken)this.Match(this.input, -1, ByDScriptingLanguageParser.Follow._EOF_in_debugExpression3013);
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream3.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boModel()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boModel()
    {
    }

    [GrammarRule("boModel")]
    private AstParserRuleReturnScope<CommonTree, IToken> boModel()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token EOF");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule boDefinition");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule impStmt");
        try
        {
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 57)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_C3;
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._impStmt_in_boModel3034);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.impStmt();
                base.PopFollow();
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope2.Tree);
                }
            }
            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
            return result;
            IL_C3:
            base.PushFollow(ByDScriptingLanguageParser.Follow._boDefinition_in_boModel3037);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.boDefinition();
            base.PopFollow();
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
            }
            IToken el = (IToken)this.Match(this.input, -1, ByDScriptingLanguageParser.Follow._EOF_in_boModel3039);
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                while (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boDefinition()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boDefinition()
    {
    }

    [GrammarRule("boDefinition")]
    private AstParserRuleReturnScope<CommonTree, IToken> boDefinition()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token RCURLB");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token BUSINESSOBJECT");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token LCURLB");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule boFeatures");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule boAnnotation");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule boName");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream4 = new RewriteRuleSubtreeStream(this.adaptor, "rule boRaises");
        try
        {
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 61)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_119;
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._boAnnotation_in_boDefinition3057);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.boAnnotation();
                base.PopFollow();
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope2.Tree);
                }
            }
            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
            return result;
            IL_119:
            IToken el = (IToken)this.Match(this.input, 19, ByDScriptingLanguageParser.Follow._BUSINESSOBJECT_in_boDefinition3060);
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._boName_in_boDefinition3062);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.boName();
            base.PopFollow();
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope3.Tree);
            }
            int num4 = 2;
            int num5 = this.input.LA(1);
            if (num5 == 80)
            {
                num4 = 1;
            }
            int num6 = num4;
            if (num6 == 1)
            {
                base.PushFollow(ByDScriptingLanguageParser.Follow._boRaises_in_boDefinition3064);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.boRaises();
                base.PopFollow();
                if (this.state.failed)
                {
                    result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream4.Add(astParserRuleReturnScope4.Tree);
                }
            }
            IToken el2 = (IToken)this.Match(this.input, 62, ByDScriptingLanguageParser.Follow._LCURLB_in_boDefinition3067);
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream3.Add(el2);
            }
            while (true)
            {
                int num7 = 2;
                int num8 = this.input.LA(1);
                if (num8 == 6 || num8 == 13 || num8 == 39 || num8 == 61 || num8 == 66 || num8 == 71)
                {
                    num7 = 1;
                }
                int num9 = num7;
                if (num9 != 1)
                {
                    goto IL_2E2;
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._boFeatures_in_boDefinition3069);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.boFeatures();
                base.PopFollow();
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope5.Tree);
                }
            }
            result = astParserRuleReturnScope;
            return result;
            IL_2E2:
            IToken el3 = (IToken)this.Match(this.input, 82, ByDScriptingLanguageParser.Follow._RCURLB_in_boDefinition3072);
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el3);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream2.NextNode(), commonTree2);
                while (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream3.NextTree());
                if (rewriteRuleSubtreeStream4.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream4.NextTree());
                }
                rewriteRuleSubtreeStream4.Reset();
                while (rewriteRuleSubtreeStream.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                }
                rewriteRuleSubtreeStream.Reset();
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boRaises()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boRaises()
    {
    }

    [GrammarRule("boRaises")]
    private AstParserRuleReturnScope<CommonTree, IToken> boRaises()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token COMMA");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token RAISES");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule boPath");
        try
        {
            IToken el = (IToken)this.Match(this.input, 80, ByDScriptingLanguageParser.Follow._RAISES_in_boRaises3103);
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._boPath_in_boRaises3105);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.boPath();
            base.PopFollow();
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 30)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_1A0;
                }
                IToken el2 = (IToken)this.Match(this.input, 30, ByDScriptingLanguageParser.Follow._COMMA_in_boRaises3108);
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(el2);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._boPath_in_boRaises3110);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.boPath();
                base.PopFollow();
                if (this.state.failed)
                {
                    goto Block_11;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                }
            }
            result = astParserRuleReturnScope;
            return result;
            Block_11:
            result = astParserRuleReturnScope;
            return result;
            IL_1A0:
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot(rewriteRuleTokenStream2.NextNode(), commonTree2);
                while (rewriteRuleSubtreeStream.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                }
                rewriteRuleSubtreeStream.Reset();
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boFeatures()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boFeatures()
    {
    }

    [GrammarRule("boFeatures")]
    private AstParserRuleReturnScope<CommonTree, IToken> boFeatures()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule boNode");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule boAssociation");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule boAction");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream4 = new RewriteRuleSubtreeStream(this.adaptor, "rule boMessage");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream5 = new RewriteRuleSubtreeStream(this.adaptor, "rule boAnnotation");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream6 = new RewriteRuleSubtreeStream(this.adaptor, "rule boElement");
        try
        {
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 61)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_102;
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._boAnnotation_in_boFeatures3139);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.boAnnotation();
                base.PopFollow();
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream5.Add(astParserRuleReturnScope2.Tree);
                }
            }
            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
            return result;
            IL_102:
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                while (rewriteRuleSubtreeStream5.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream5.NextTree());
                }
                rewriteRuleSubtreeStream5.Reset();
                astParserRuleReturnScope.Tree = commonTree;
            }
            int num4 = this.input.LA(1);
            int num5;
            if (num4 <= 13)
            {
                if (num4 == 6)
                {
                    num5 = 4;
                    goto IL_1FB;
                }
                if (num4 == 13)
                {
                    num5 = 3;
                    goto IL_1FB;
                }
            }
            else
            {
                if (num4 == 39)
                {
                    num5 = 5;
                    goto IL_1FB;
                }
                if (num4 == 66)
                {
                    num5 = 1;
                    goto IL_1FB;
                }
                if (num4 == 71)
                {
                    num5 = 2;
                    goto IL_1FB;
                }
            }
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 60, 0, this.input);
            throw ex;
            IL_1FB:
            switch (num5)
            {
                case 1:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._boMessage_in_boFeatures3163);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.boMessage();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream4.Add(astParserRuleReturnScope3.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream7 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(66, "MESSAGE"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream7.NextTree());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream4.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 2:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._boNode_in_boFeatures3189);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.boNode();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope4.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream8 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                            commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(71, "NODE"), commonTree3);
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream8.NextTree());
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree3);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 3:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._boAssociation_in_boFeatures3208);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.boAssociation();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope5.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream9 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree4 = (CommonTree)this.adaptor.Nil();
                            commonTree4 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(13, "ASSOCIATION"), commonTree4);
                            this.adaptor.AddChild(commonTree4, rewriteRuleSubtreeStream9.NextTree());
                            this.adaptor.AddChild(commonTree4, rewriteRuleSubtreeStream2.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree4);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 4:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._boAction_in_boFeatures3230);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope6 = this.boAction();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope6.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream10 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree5 = (CommonTree)this.adaptor.Nil();
                            commonTree5 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(6, "ACTION"), commonTree5);
                            this.adaptor.AddChild(commonTree5, rewriteRuleSubtreeStream10.NextTree());
                            this.adaptor.AddChild(commonTree5, rewriteRuleSubtreeStream3.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree5);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 5:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._boElement_in_boFeatures3258);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope7 = this.boElement();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream6.Add(astParserRuleReturnScope7.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream11 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree6 = (CommonTree)this.adaptor.Nil();
                            commonTree6 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(39, "ELEMENT"), commonTree6);
                            this.adaptor.AddChild(commonTree6, rewriteRuleSubtreeStream11.NextTree());
                            this.adaptor.AddChild(commonTree6, rewriteRuleSubtreeStream6.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree6);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boMessage()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boMessage()
    {
    }

    [GrammarRule("boMessage")]
    private AstParserRuleReturnScope<CommonTree, IToken> boMessage()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token COLON");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token MESSAGE");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token DELIM");
        RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token COMMA");
        RewriteRuleTokenStream rewriteRuleTokenStream5 = new RewriteRuleTokenStream(this.adaptor, "token TEXTT");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule stringLiteral");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule boName");
        try
        {
            IToken el = (IToken)this.Match(this.input, 66, ByDScriptingLanguageParser.Follow._MESSAGE_in_boMessage3309);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_boMessage3311);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            IToken el2 = (IToken)this.Match(this.input, 89, ByDScriptingLanguageParser.Follow._TEXTT_in_boMessage3313);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream5.Add(el2);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._stringLiteral_in_boMessage3315);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.stringLiteral();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
            }
            int num = 2;
            int num2 = this.input.LA(1);
            if (num2 == 28)
            {
                num = 1;
            }
            int num3 = num;
            if (num3 == 1)
            {
                IToken el3 = (IToken)this.Match(this.input, 28, ByDScriptingLanguageParser.Follow._COLON_in_boMessage3318);
                AstParserRuleReturnScope<CommonTree, IToken> result;
                if (this.state.failed)
                {
                    result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(el3);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._boName_in_boMessage3320);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.boName();
                base.PopFollow();
                if (this.state.failed)
                {
                    result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope4.Tree);
                }
                while (true)
                {
                    int num4 = 2;
                    int num5 = this.input.LA(1);
                    if (num5 == 30)
                    {
                        num4 = 1;
                    }
                    int num6 = num4;
                    if (num6 != 1)
                    {
                        goto IL_34B;
                    }
                    IToken el4 = (IToken)this.Match(this.input, 30, ByDScriptingLanguageParser.Follow._COMMA_in_boMessage3323);
                    if (this.state.failed)
                    {
                        break;
                    }
                    if (this.state.backtracking == 0)
                    {
                        rewriteRuleTokenStream4.Add(el4);
                    }
                    base.PushFollow(ByDScriptingLanguageParser.Follow._boName_in_boMessage3325);
                    AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.boName();
                    base.PopFollow();
                    if (this.state.failed)
                    {
                        goto Block_21;
                    }
                    if (this.state.backtracking == 0)
                    {
                        rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope5.Tree);
                    }
                }
                result = astParserRuleReturnScope;
                return result;
                Block_21:
                result = astParserRuleReturnScope;
                return result;
            }
            IL_34B:
            IToken el5 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_boMessage3331);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream3.Add(el5);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream2.NextTree());
                while (rewriteRuleSubtreeStream3.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream3.NextTree());
                }
                rewriteRuleSubtreeStream3.Reset();
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boNode()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boNode()
    {
    }

    [GrammarRule("boNode")]
    private AstParserRuleReturnScope<CommonTree, IToken> boNode()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token RCURLB");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token NODE");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token DELIM");
        RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token LCURLB");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule boFeatures");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule boCardinality");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream4 = new RewriteRuleSubtreeStream(this.adaptor, "rule boRaises");
        try
        {
            IToken el = (IToken)this.Match(this.input, 71, ByDScriptingLanguageParser.Follow._NODE_in_boNode3353);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_boNode3355);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            int num = this.input.LA(1);
            int num2;
            if ((num >= 61 && num <= 62) || num == 80)
            {
                num2 = 1;
            }
            else if (num == 35)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 66, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        int num3 = 2;
                        int num4 = this.input.LA(1);
                        if (num4 == 61)
                        {
                            num3 = 1;
                        }
                        int num5 = num3;
                        AstParserRuleReturnScope<CommonTree, IToken> result;
                        if (num5 == 1)
                        {
                            base.PushFollow(ByDScriptingLanguageParser.Follow._boCardinality_in_boNode3367);
                            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.boCardinality();
                            base.PopFollow();
                            if (this.state.failed)
                            {
                                result = astParserRuleReturnScope;
                                return result;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope3.Tree);
                            }
                        }
                        int num6 = 2;
                        int num7 = this.input.LA(1);
                        if (num7 == 80)
                        {
                            num6 = 1;
                        }
                        int num8 = num6;
                        if (num8 == 1)
                        {
                            base.PushFollow(ByDScriptingLanguageParser.Follow._boRaises_in_boNode3370);
                            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.boRaises();
                            base.PopFollow();
                            if (this.state.failed)
                            {
                                result = astParserRuleReturnScope;
                                return result;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleSubtreeStream4.Add(astParserRuleReturnScope4.Tree);
                            }
                        }
                        IToken el2 = (IToken)this.Match(this.input, 62, ByDScriptingLanguageParser.Follow._LCURLB_in_boNode3373);
                        if (this.state.failed)
                        {
                            result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream4.Add(el2);
                        }
                        while (true)
                        {
                            int num9 = 2;
                            int num10 = this.input.LA(1);
                            if (num10 == 6 || num10 == 13 || num10 == 39 || num10 == 61 || num10 == 66 || num10 == 71)
                            {
                                num9 = 1;
                            }
                            int num11 = num9;
                            if (num11 != 1)
                            {
                                goto IL_37C;
                            }
                            base.PushFollow(ByDScriptingLanguageParser.Follow._boFeatures_in_boNode3375);
                            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.boFeatures();
                            base.PopFollow();
                            if (this.state.failed)
                            {
                                break;
                            }
                            if (this.state.backtracking == 0)
                            {
                                rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope5.Tree);
                            }
                        }
                        result = astParserRuleReturnScope;
                        return result;
                        IL_37C:
                        IToken el3 = (IToken)this.Match(this.input, 82, ByDScriptingLanguageParser.Follow._RCURLB_in_boNode3378);
                        if (this.state.failed)
                        {
                            result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(el3);
                        }
                        break;
                    }
                case 2:
                    {
                        IToken el4 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_boNode3385);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream3.Add(el4);
                        }
                        break;
                    }
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                if (rewriteRuleSubtreeStream3.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream3.NextTree());
                }
                rewriteRuleSubtreeStream3.Reset();
                if (rewriteRuleSubtreeStream4.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream4.NextTree());
                }
                rewriteRuleSubtreeStream4.Reset();
                while (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boAssociation()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boAssociation()
    {
    }

    [GrammarRule("boAssociation")]
    private AstParserRuleReturnScope<CommonTree, IToken> boAssociation()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token USING");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token TO");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token DELIM");
        RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token ASSOCIATION");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule boCardinality");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule boName");
        try
        {
            IToken el = (IToken)this.Match(this.input, 13, ByDScriptingLanguageParser.Follow._ASSOCIATION_in_boAssociation3420);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream4.Add(el);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_boAssociation3424);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.fullId();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
            }
            int num = 2;
            int num2 = this.input.LA(1);
            if (num2 == 61)
            {
                num = 1;
            }
            int num3 = num;
            if (num3 == 1)
            {
                base.PushFollow(ByDScriptingLanguageParser.Follow._boCardinality_in_boAssociation3426);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.boCardinality();
                base.PopFollow();
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope4.Tree);
                }
            }
            IToken el2 = (IToken)this.Match(this.input, 90, ByDScriptingLanguageParser.Follow._TO_in_boAssociation3429);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el2);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._boName_in_boAssociation3431);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope5 = this.boName();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope5.Tree);
            }
            int num4 = 2;
            int num5 = this.input.LA(1);
            if (num5 == 96)
            {
                num4 = 1;
            }
            int num6 = num4;
            if (num6 == 1)
            {
                IToken el3 = (IToken)this.Match(this.input, 96, ByDScriptingLanguageParser.Follow._USING_in_boAssociation3434);
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(el3);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_boAssociation3438);
                astParserRuleReturnScope2 = this.fullId();
                base.PopFollow();
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
                }
            }
            IToken el4 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_boAssociation3442);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream3.Add(el4);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleSubtreeStream rewriteRuleSubtreeStream4 = new RewriteRuleSubtreeStream(this.adaptor, "rule id", (astParserRuleReturnScope3 != null) ? astParserRuleReturnScope3.Tree : null);
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                RewriteRuleSubtreeStream rewriteRuleSubtreeStream5 = new RewriteRuleSubtreeStream(this.adaptor, "rule foreignKey", (astParserRuleReturnScope2 != null) ? astParserRuleReturnScope2.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream4.NextTree());
                if (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream3.NextTree());
                if (rewriteRuleSubtreeStream5.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream5.NextTree());
                }
                rewriteRuleSubtreeStream5.Reset();
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boAction()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boAction()
    {
    }

    [GrammarRule("boAction")]
    private AstParserRuleReturnScope<CommonTree, IToken> boAction()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token DELIM");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token ACTION");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule boRaises");
        try
        {
            IToken el = (IToken)this.Match(this.input, 6, ByDScriptingLanguageParser.Follow._ACTION_in_boAction3478);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_boAction3480);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            int num = 2;
            int num2 = this.input.LA(1);
            if (num2 == 80)
            {
                num = 1;
            }
            int num3 = num;
            if (num3 == 1)
            {
                base.PushFollow(ByDScriptingLanguageParser.Follow._boRaises_in_boAction3482);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.boRaises();
                base.PopFollow();
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
                }
            }
            IToken el2 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_boAction3485);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el2);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                if (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boElement()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boElement()
    {
    }

    [GrammarRule("boElement")]
    private AstParserRuleReturnScope<CommonTree, IToken> boElement()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token ELEMENT");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token COLON");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token DELIM");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule boCardinality");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule boName");
        try
        {
            IToken el = (IToken)this.Match(this.input, 39, ByDScriptingLanguageParser.Follow._ELEMENT_in_boElement3511);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_boElement3513);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            int num = 2;
            int num2 = this.input.LA(1);
            if (num2 == 61)
            {
                num = 1;
            }
            int num3 = num;
            if (num3 == 1)
            {
                base.PushFollow(ByDScriptingLanguageParser.Follow._boCardinality_in_boElement3515);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.boCardinality();
                base.PopFollow();
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
                }
            }
            int num4 = 2;
            int num5 = this.input.LA(1);
            if (num5 == 28)
            {
                num4 = 1;
            }
            int num6 = num4;
            if (num6 == 1)
            {
                IToken el2 = (IToken)this.Match(this.input, 28, ByDScriptingLanguageParser.Follow._COLON_in_boElement3519);
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream2.Add(el2);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._boName_in_boElement3521);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.boName();
                base.PopFollow();
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream3.Add(astParserRuleReturnScope4.Tree);
                }
            }
            IToken el3 = (IToken)this.Match(this.input, 35, ByDScriptingLanguageParser.Follow._DELIM_in_boElement3525);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream3.Add(el3);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                if (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                if (rewriteRuleSubtreeStream3.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream3.NextTree());
                }
                rewriteRuleSubtreeStream3.Reset();
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boName()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boName()
    {
    }

    [GrammarRule("boName")]
    private AstParserRuleReturnScope<CommonTree, IToken> boName()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token COLON");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule boPath");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._boPath_in_boName3561);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.boPath();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            int num = 2;
            int num2 = this.input.LA(1);
            if (num2 == 28)
            {
                num = 1;
            }
            int num3 = num;
            if (num3 == 1)
            {
                IToken el = (IToken)this.Match(this.input, 28, ByDScriptingLanguageParser.Follow._COLON_in_boName3584);
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(el);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._boPath_in_boName3586);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.boPath();
                base.PopFollow();
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                }
                if (this.state.backtracking == 0)
                {
                    astParserRuleReturnScope.Tree = commonTree;
                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                    commonTree = (CommonTree)this.adaptor.Nil();
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream2.NextTree());
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                    astParserRuleReturnScope.Tree = commonTree;
                }
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(15, "BONAME"), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream3.NextTree());
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boPath()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boPath()
    {
    }

    [GrammarRule("boPath")]
    private AstParserRuleReturnScope<CommonTree, IToken> boPath()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token DOT");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_boPath3632);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
            base.PopFollow();
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 37)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_25F;
                }
                IToken el = (IToken)this.Match(this.input, 37, ByDScriptingLanguageParser.Follow._DOT_in_boPath3643);
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(el);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_boPath3645);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.fullId();
                base.PopFollow();
                if (this.state.failed)
                {
                    goto Block_11;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                }
                if (this.state.backtracking == 0)
                {
                    astParserRuleReturnScope.Tree = commonTree;
                    RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                    commonTree = (CommonTree)this.adaptor.Nil();
                    CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                    commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(77, "PATH"), commonTree2);
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                    this.adaptor.AddChild(commonTree, commonTree2);
                    astParserRuleReturnScope.Tree = commonTree;
                }
            }
            result = astParserRuleReturnScope;
            return result;
            Block_11:
            result = astParserRuleReturnScope;
            return result;
            IL_25F:
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boAnnotation()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boAnnotation()
    {
    }

    [GrammarRule("boAnnotation")]
    private AstParserRuleReturnScope<CommonTree, IToken> boAnnotation()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token LBRCKT");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token RPAREN");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token RBRCKT");
        RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token LPAREN");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule fullId");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule boAnnoArgs");
        try
        {
            IToken el = (IToken)this.Match(this.input, 61, ByDScriptingLanguageParser.Follow._LBRCKT_in_boAnnotation3673);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(el);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_boAnnotation3675);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            int num = 2;
            int num2 = this.input.LA(1);
            if (num2 == 64)
            {
                num = 1;
            }
            int num3 = num;
            if (num3 == 1)
            {
                IToken el2 = (IToken)this.Match(this.input, 64, ByDScriptingLanguageParser.Follow._LPAREN_in_boAnnotation3678);
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream4.Add(el2);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._boAnnoArgs_in_boAnnotation3680);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.boAnnoArgs();
                base.PopFollow();
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream2.Add(astParserRuleReturnScope3.Tree);
                }
                IToken el3 = (IToken)this.Match(this.input, 84, ByDScriptingLanguageParser.Follow._RPAREN_in_boAnnotation3683);
                if (this.state.failed)
                {
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream2.Add(el3);
                }
            }
            IToken el4 = (IToken)this.Match(this.input, 81, ByDScriptingLanguageParser.Follow._RBRCKT_in_boAnnotation3688);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream3.Add(el4);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(9, "ANNOTATION"), commonTree2);
                this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                if (rewriteRuleSubtreeStream2.HasNext)
                {
                    this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                }
                rewriteRuleSubtreeStream2.Reset();
                this.adaptor.AddChild(commonTree, commonTree2);
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boAnnoArgs()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boAnnoArgs()
    {
    }

    [GrammarRule("boAnnoArgs")]
    private AstParserRuleReturnScope<CommonTree, IToken> boAnnoArgs()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token COMMA");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule boAnnoPar");
        try
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._boAnnoPar_in_boAnnoArgs3716);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.boAnnoPar();
            base.PopFollow();
            AstParserRuleReturnScope<CommonTree, IToken> result;
            if (this.state.failed)
            {
                result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            while (true)
            {
                int num = 2;
                int num2 = this.input.LA(1);
                if (num2 == 30)
                {
                    num = 1;
                }
                int num3 = num;
                if (num3 != 1)
                {
                    goto IL_146;
                }
                IToken el = (IToken)this.Match(this.input, 30, ByDScriptingLanguageParser.Follow._COMMA_in_boAnnoArgs3719);
                if (this.state.failed)
                {
                    break;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleTokenStream.Add(el);
                }
                base.PushFollow(ByDScriptingLanguageParser.Follow._boAnnoPar_in_boAnnoArgs3721);
                AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.boAnnoPar();
                base.PopFollow();
                if (this.state.failed)
                {
                    goto Block_9;
                }
                if (this.state.backtracking == 0)
                {
                    rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                }
            }
            result = astParserRuleReturnScope;
            return result;
            Block_9:
            result = astParserRuleReturnScope;
            return result;
            IL_146:
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                while (rewriteRuleSubtreeStream.HasNext)
                {
                    this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                }
                rewriteRuleSubtreeStream.Reset();
                astParserRuleReturnScope.Tree = commonTree;
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boCardinality()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boCardinality()
    {
    }

    [GrammarRule("boCardinality")]
    private AstParserRuleReturnScope<CommonTree, IToken> boCardinality()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token LBRCKT");
        RewriteRuleTokenStream rewriteRuleTokenStream2 = new RewriteRuleTokenStream(this.adaptor, "token RBRCKT");
        RewriteRuleTokenStream rewriteRuleTokenStream3 = new RewriteRuleTokenStream(this.adaptor, "token COMMA");
        RewriteRuleTokenStream rewriteRuleTokenStream4 = new RewriteRuleTokenStream(this.adaptor, "token CARD_N");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule boDecIntLiteral");
        try
        {
            IToken token = (IToken)this.Match(this.input, 61, ByDScriptingLanguageParser.Follow._LBRCKT_in_boCardinality3743);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream.Add(token);
            }
            base.PushFollow(ByDScriptingLanguageParser.Follow._boDecIntLiteral_in_boCardinality3746);
            AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.boDecIntLiteral();
            base.PopFollow();
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
            }
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = commonTree;
                new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                commonTree = (CommonTree)this.adaptor.Nil();
                this.adaptor.AddChild(commonTree, rewriteRuleSubtreeStream.NextTree());
                astParserRuleReturnScope.Tree = commonTree;
            }
            IToken el = (IToken)this.Match(this.input, 30, ByDScriptingLanguageParser.Follow._COMMA_in_boCardinality3753);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream3.Add(el);
            }
            int num = this.input.LA(1);
            int num2;
            if (num == 7 || num == 32 || num == 87)
            {
                num2 = 1;
            }
            else if (num == 22)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 76, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._boDecIntLiteral_in_boCardinality3759);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.boDecIntLiteral();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope3.Tree);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream5 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream2 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(21, "CARDINALITY"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream5.NextNode());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream2.NextTree());
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
                case 2:
                    {
                        IToken el2 = (IToken)this.Match(this.input, 22, ByDScriptingLanguageParser.Follow._CARD_N_in_boCardinality3836);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream4.Add(el2);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            RewriteRuleTokenStream rewriteRuleTokenStream6 = new RewriteRuleTokenStream(this.adaptor, "token tok", token);
                            RewriteRuleSubtreeStream rewriteRuleSubtreeStream3 = new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree3 = (CommonTree)this.adaptor.Nil();
                            commonTree3 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(21, "CARDINALITY"), commonTree3);
                            this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream6.NextNode());
                            this.adaptor.AddChild(commonTree3, rewriteRuleSubtreeStream3.NextTree());
                            this.adaptor.AddChild(commonTree3, rewriteRuleTokenStream4.NextNode());
                            this.adaptor.AddChild(commonTree, commonTree3);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
            }
            IToken el3 = (IToken)this.Match(this.input, 81, ByDScriptingLanguageParser.Follow._RBRCKT_in_boCardinality3867);
            if (this.state.failed)
            {
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            if (this.state.backtracking == 0)
            {
                rewriteRuleTokenStream2.Add(el3);
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boAnnoPar()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boAnnoPar()
    {
    }

    [GrammarRule("boAnnoPar")]
    private AstParserRuleReturnScope<CommonTree, IToken> boAnnoPar()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            if (num <= 49)
            {
                if (num <= 41)
                {
                    switch (num)
                    {
                        case 5:
                        case 6:
                        case 10:
                        case 13:
                        case 16:
                        case 19:
                        case 22:
                        case 23:
                        case 25:
                        case 27:
                        case 31:
                        case 33:
                            break;
                        case 7:
                        case 32:
                            goto IL_193;
                        case 8:
                        case 9:
                        case 11:
                        case 12:
                        case 14:
                        case 15:
                        case 17:
                        case 20:
                        case 21:
                        case 24:
                        case 26:
                        case 28:
                        case 29:
                        case 30:
                            goto IL_198;
                        case 18:
                            goto IL_18E;
                        default:
                            switch (num)
                            {
                                case 39:
                                case 41:
                                    break;
                                case 40:
                                    goto IL_198;
                                default:
                                    goto IL_198;
                            }
                            break;
                    }
                }
                else if (num != 44 && num != 49)
                {
                    goto IL_198;
                }
            }
            else if (num <= 66)
            {
                switch (num)
                {
                    case 54:
                    case 57:
                    case 58:
                        break;
                    case 55:
                    case 56:
                        goto IL_198;
                    default:
                        if (num != 66)
                        {
                            goto IL_198;
                        }
                        break;
                }
            }
            else if (num != 71)
            {
                switch (num)
                {
                    case 79:
                    case 80:
                    case 83:
                    case 88:
                    case 89:
                    case 90:
                    case 91:
                    case 94:
                    case 96:
                    case 98:
                    case 100:
                        break;
                    case 81:
                    case 82:
                    case 84:
                    case 85:
                    case 92:
                    case 93:
                    case 95:
                    case 97:
                    case 99:
                        goto IL_198;
                    case 86:
                        goto IL_18E;
                    case 87:
                        goto IL_193;
                    default:
                        goto IL_198;
                }
            }
            int num2 = 1;
            goto IL_1D2;
            IL_18E:
            num2 = 2;
            goto IL_1D2;
            IL_193:
            num2 = 3;
            goto IL_1D2;
            IL_198:
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 77, 0, this.input);
            throw ex;
            IL_1D2:
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_boAnnoPar3879);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.fullId();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._stringLiteral_in_boAnnoPar3886);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.stringLiteral();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope3.Tree);
                        }
                        break;
                    }
                case 3:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._boDecIntLiteral_in_boAnnoPar3893);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.boDecIntLiteral();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope4.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boDecIntLiteral()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boDecIntLiteral()
    {
    }

    [GrammarRule("boDecIntLiteral")]
    private AstParserRuleReturnScope<CommonTree, IToken> boDecIntLiteral()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        RewriteRuleTokenStream rewriteRuleTokenStream = new RewriteRuleTokenStream(this.adaptor, "token DECLIT");
        RewriteRuleSubtreeStream rewriteRuleSubtreeStream = new RewriteRuleSubtreeStream(this.adaptor, "rule signPrefix");
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num == 32)
            {
                num2 = 1;
            }
            else if (num == 7 || num == 87)
            {
                num2 = 2;
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                    return result;
                }
                NoViableAltException ex = new NoViableAltException("", 78, 0, this.input);
                throw ex;
            }
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken payload = (IToken)this.Match(this.input, 32, ByDScriptingLanguageParser.Follow._DECLIT_in_boDecIntLiteral3904);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(payload);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        break;
                    }
                case 2:
                    {
                        base.PushFollow(ByDScriptingLanguageParser.Follow._signPrefix_in_boDecIntLiteral3909);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.signPrefix();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleSubtreeStream.Add(astParserRuleReturnScope2.Tree);
                        }
                        IToken el = (IToken)this.Match(this.input, 32, ByDScriptingLanguageParser.Follow._DECLIT_in_boDecIntLiteral3911);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            rewriteRuleTokenStream.Add(el);
                        }
                        if (this.state.backtracking == 0)
                        {
                            astParserRuleReturnScope.Tree = commonTree;
                            new RewriteRuleSubtreeStream(this.adaptor, "rule retval", (astParserRuleReturnScope != null) ? astParserRuleReturnScope.Tree : null);
                            commonTree = (CommonTree)this.adaptor.Nil();
                            CommonTree commonTree2 = (CommonTree)this.adaptor.Nil();
                            commonTree2 = (CommonTree)this.adaptor.BecomeRoot((CommonTree)this.adaptor.Create(95, "UNARY"), commonTree2);
                            this.adaptor.AddChild(commonTree2, rewriteRuleSubtreeStream.NextTree());
                            this.adaptor.AddChild(commonTree2, rewriteRuleTokenStream.NextNode());
                            this.adaptor.AddChild(commonTree, commonTree2);
                            astParserRuleReturnScope.Tree = commonTree;
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_fullId()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_fullId()
    {
    }

    [GrammarRule("fullId")]
    private AstParserRuleReturnScope<CommonTree, IToken> fullId()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num <= 41)
            {
                if (num <= 13)
                {
                    switch (num)
                    {
                        case 5:
                            num2 = 1;
                            goto IL_1DA;
                        case 6:
                            goto IL_19B;
                        default:
                            if (num != 10)
                            {
                                if (num != 13)
                                {
                                    goto IL_1A0;
                                }
                                goto IL_19B;
                            }
                            break;
                    }
                }
                else if (num <= 27)
                {
                    if (num != 16)
                    {
                        switch (num)
                        {
                            case 19:
                            case 22:
                                goto IL_19B;
                            case 20:
                            case 21:
                            case 24:
                            case 26:
                                goto IL_1A0;
                            case 23:
                            case 25:
                            case 27:
                                break;
                            default:
                                goto IL_1A0;
                        }
                    }
                }
                else
                {
                    switch (num)
                    {
                        case 31:
                        case 33:
                            break;
                        case 32:
                            goto IL_1A0;
                        default:
                            switch (num)
                            {
                                case 39:
                                    goto IL_19B;
                                case 40:
                                    goto IL_1A0;
                                case 41:
                                    break;
                                default:
                                    goto IL_1A0;
                            }
                            break;
                    }
                }
            }
            else if (num <= 58)
            {
                if (num != 44 && num != 49)
                {
                    switch (num)
                    {
                        case 54:
                        case 57:
                        case 58:
                            break;
                        case 55:
                        case 56:
                            goto IL_1A0;
                        default:
                            goto IL_1A0;
                    }
                }
            }
            else if (num <= 71)
            {
                if (num != 66 && num != 71)
                {
                    goto IL_1A0;
                }
                goto IL_19B;
            }
            else
            {
                switch (num)
                {
                    case 79:
                    case 83:
                    case 88:
                    case 91:
                        break;
                    case 80:
                    case 89:
                    case 90:
                        goto IL_19B;
                    case 81:
                    case 82:
                    case 84:
                    case 85:
                    case 86:
                    case 87:
                        goto IL_1A0;
                    default:
                        switch (num)
                        {
                            case 94:
                            case 98:
                            case 100:
                                break;
                            case 95:
                            case 97:
                            case 99:
                                goto IL_1A0;
                            case 96:
                                goto IL_19B;
                            default:
                                goto IL_1A0;
                        }
                        break;
                }
            }
            num2 = 2;
            goto IL_1DA;
            IL_19B:
            num2 = 3;
            goto IL_1DA;
            IL_1A0:
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 79, 0, this.input);
            throw ex;
            IL_1DA:
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._simpleId_in_fullId3943);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope2 = this.simpleId();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope2.Tree);
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._abslKeywords_in_fullId3950);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope3 = this.abslKeywords();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope3.Tree);
                        }
                        break;
                    }
                case 3:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        base.PushFollow(ByDScriptingLanguageParser.Follow._boKeywords_in_fullId3955);
                        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope4 = this.boKeywords();
                        base.PopFollow();
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.adaptor.AddChild(commonTree, astParserRuleReturnScope4.Tree);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
            if (this.state.backtracking == 0)
            {
                this.collect_tooltip_identifier(astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_simpleId()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_simpleId()
    {
    }

    [GrammarRule("simpleId")]
    private AstParserRuleReturnScope<CommonTree, IToken> simpleId()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        try
        {
            CommonTree commonTree = (CommonTree)this.adaptor.Nil();
            IToken payload = (IToken)this.Match(this.input, 5, ByDScriptingLanguageParser.Follow._ABSLID_in_simpleId3966);
            if (this.state.failed)
            {
                return astParserRuleReturnScope;
            }
            if (this.state.backtracking == 0)
            {
                CommonTree child = (CommonTree)this.adaptor.Create(payload);
                this.adaptor.AddChild(commonTree, child);
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex)
        {
            this.ReportError(ex);
            this.Recover(this.input, ex);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_abslKeywords()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_abslKeywords()
    {
    }

    [GrammarRule("abslKeywords")]
    private AstParserRuleReturnScope<CommonTree, IToken> abslKeywords()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num <= 49)
            {
                if (num <= 27)
                {
                    if (num == 10)
                    {
                        num2 = 1;
                        goto IL_1ED;
                    }
                    if (num == 16)
                    {
                        num2 = 12;
                        goto IL_1ED;
                    }
                    switch (num)
                    {
                        case 23:
                            num2 = 6;
                            goto IL_1ED;
                        case 25:
                            num2 = 20;
                            goto IL_1ED;
                        case 27:
                            num2 = 15;
                            goto IL_1ED;
                    }
                }
                else if (num <= 41)
                {
                    switch (num)
                    {
                        case 31:
                            num2 = 11;
                            goto IL_1ED;
                        case 32:
                            break;
                        case 33:
                            num2 = 7;
                            goto IL_1ED;
                        default:
                            if (num == 41)
                            {
                                num2 = 4;
                                goto IL_1ED;
                            }
                            break;
                    }
                }
                else
                {
                    if (num == 44)
                    {
                        num2 = 16;
                        goto IL_1ED;
                    }
                    if (num == 49)
                    {
                        num2 = 9;
                        goto IL_1ED;
                    }
                }
            }
            else if (num <= 83)
            {
                switch (num)
                {
                    case 54:
                        num2 = 3;
                        goto IL_1ED;
                    case 55:
                    case 56:
                        break;
                    case 57:
                        num2 = 2;
                        goto IL_1ED;
                    case 58:
                        num2 = 10;
                        goto IL_1ED;
                    default:
                        if (num == 79)
                        {
                            num2 = 13;
                            goto IL_1ED;
                        }
                        if (num == 83)
                        {
                            num2 = 18;
                            goto IL_1ED;
                        }
                        break;
                }
            }
            else if (num <= 91)
            {
                if (num == 88)
                {
                    num2 = 5;
                    goto IL_1ED;
                }
                if (num == 91)
                {
                    num2 = 19;
                    goto IL_1ED;
                }
            }
            else
            {
                if (num == 94)
                {
                    num2 = 17;
                    goto IL_1ED;
                }
                switch (num)
                {
                    case 98:
                        num2 = 14;
                        goto IL_1ED;
                    case 100:
                        num2 = 8;
                        goto IL_1ED;
                }
            }
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 80, 0, this.input);
            throw ex;
            IL_1ED:
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 10, ByDScriptingLanguageParser.Follow._AS_in_abslKeywords3979);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 57, ByDScriptingLanguageParser.Follow._IMPORT_in_abslKeywords4000);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 3:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 54, ByDScriptingLanguageParser.Follow._IF_in_abslKeywords4017);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 4:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 41, ByDScriptingLanguageParser.Follow._ELSE_in_abslKeywords4038);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 5:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 88, ByDScriptingLanguageParser.Follow._SWITCH_in_abslKeywords4057);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 6:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 23, ByDScriptingLanguageParser.Follow._CASE_in_abslKeywords4074);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 7:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 33, ByDScriptingLanguageParser.Follow._DEFAULT_in_abslKeywords4093);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 8:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 100, ByDScriptingLanguageParser.Follow._WHILE_in_abslKeywords4109);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 9:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 49, ByDScriptingLanguageParser.Follow._FOREACH_in_abslKeywords4127);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 10:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 58, ByDScriptingLanguageParser.Follow._INN_in_abslKeywords4143);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 11:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 31, ByDScriptingLanguageParser.Follow._CONTINUE_in_abslKeywords4163);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 12:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 16, ByDScriptingLanguageParser.Follow._BREAK_in_abslKeywords4178);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 13:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 79, ByDScriptingLanguageParser.Follow._RAISE_in_abslKeywords4196);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 14:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 98, ByDScriptingLanguageParser.Follow._VAR_in_abslKeywords4214);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 15:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 27, ByDScriptingLanguageParser.Follow._COLLOF_in_abslKeywords4234);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 16:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 44, ByDScriptingLanguageParser.Follow._ELTSOF_in_abslKeywords4251);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 17:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 94, ByDScriptingLanguageParser.Follow._TYPOF_in_abslKeywords4268);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 18:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 83, ByDScriptingLanguageParser.Follow._RETURN_in_abslKeywords4286);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 19:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 91, ByDScriptingLanguageParser.Follow._TRY_in_abslKeywords4303);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 20:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 25, ByDScriptingLanguageParser.Follow._CATCH_in_abslKeywords4323);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_boKeywords()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_boKeywords()
    {
    }

    [GrammarRule("boKeywords")]
    private AstParserRuleReturnScope<CommonTree, IToken> boKeywords()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        CommonTree commonTree = null;
        try
        {
            int num = this.input.LA(1);
            int num2;
            if (num <= 39)
            {
                if (num <= 13)
                {
                    if (num == 6)
                    {
                        num2 = 8;
                        goto IL_108;
                    }
                    if (num == 13)
                    {
                        num2 = 6;
                        goto IL_108;
                    }
                }
                else
                {
                    if (num == 19)
                    {
                        num2 = 1;
                        goto IL_108;
                    }
                    if (num == 22)
                    {
                        num2 = 9;
                        goto IL_108;
                    }
                    if (num == 39)
                    {
                        num2 = 4;
                        goto IL_108;
                    }
                }
            }
            else if (num <= 71)
            {
                if (num == 66)
                {
                    num2 = 2;
                    goto IL_108;
                }
                if (num == 71)
                {
                    num2 = 3;
                    goto IL_108;
                }
            }
            else
            {
                if (num == 80)
                {
                    num2 = 5;
                    goto IL_108;
                }
                switch (num)
                {
                    case 89:
                        num2 = 11;
                        goto IL_108;
                    case 90:
                        num2 = 7;
                        goto IL_108;
                    default:
                        if (num == 96)
                        {
                            num2 = 10;
                            goto IL_108;
                        }
                        break;
                }
            }
            if (this.state.backtracking > 0)
            {
                this.state.failed = true;
                AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                return result;
            }
            NoViableAltException ex = new NoViableAltException("", 81, 0, this.input);
            throw ex;
            IL_108:
            switch (num2)
            {
                case 1:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 19, ByDScriptingLanguageParser.Follow._BUSINESSOBJECT_in_boKeywords4347);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 2:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 66, ByDScriptingLanguageParser.Follow._MESSAGE_in_boKeywords4357);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 3:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 71, ByDScriptingLanguageParser.Follow._NODE_in_boKeywords4374);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 4:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 39, ByDScriptingLanguageParser.Follow._ELEMENT_in_boKeywords4394);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 5:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 80, ByDScriptingLanguageParser.Follow._RAISES_in_boKeywords4411);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 6:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 13, ByDScriptingLanguageParser.Follow._ASSOCIATION_in_boKeywords4429);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 7:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 90, ByDScriptingLanguageParser.Follow._TO_in_boKeywords4442);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 8:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 6, ByDScriptingLanguageParser.Follow._ACTION_in_boKeywords4464);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 9:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 22, ByDScriptingLanguageParser.Follow._CARD_N_in_boKeywords4482);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 10:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 96, ByDScriptingLanguageParser.Follow._USING_in_boKeywords4500);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
                case 11:
                    {
                        commonTree = (CommonTree)this.adaptor.Nil();
                        IToken token = (IToken)this.Match(this.input, 89, ByDScriptingLanguageParser.Follow._TEXTT_in_boKeywords4519);
                        if (this.state.failed)
                        {
                            AstParserRuleReturnScope<CommonTree, IToken> result = astParserRuleReturnScope;
                            return result;
                        }
                        if (this.state.backtracking == 0)
                        {
                            CommonTree child = (CommonTree)this.adaptor.Create(token);
                            this.adaptor.AddChild(commonTree, child);
                        }
                        if (this.state.backtracking == 0)
                        {
                            this.Retype(token, 5);
                        }
                        break;
                    }
            }
            astParserRuleReturnScope.Stop = this.input.LT(-1);
            if (this.state.backtracking == 0)
            {
                astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_signPrefix()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_signPrefix()
    {
    }

    [GrammarRule("signPrefix")]
    private AstParserRuleReturnScope<CommonTree, IToken> signPrefix()
    {
        AstParserRuleReturnScope<CommonTree, IToken> astParserRuleReturnScope = new AstParserRuleReturnScope<CommonTree, IToken>();
        astParserRuleReturnScope.Start = this.input.LT(1);
        try
        {
            CommonTree commonTree = (CommonTree)this.adaptor.Nil();
            IToken payload = this.input.LT(1);
            if (this.input.LA(1) == 7 || this.input.LA(1) == 87)
            {
                this.input.Consume();
                if (this.state.backtracking == 0)
                {
                    this.adaptor.AddChild(commonTree, (CommonTree)this.adaptor.Create(payload));
                }
                this.state.errorRecovery = false;
                this.state.failed = false;
                astParserRuleReturnScope.Stop = this.input.LT(-1);
                if (this.state.backtracking == 0)
                {
                    astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.RulePostProcessing(commonTree);
                    this.adaptor.SetTokenBoundaries(astParserRuleReturnScope.Tree, astParserRuleReturnScope.Start, astParserRuleReturnScope.Stop);
                }
            }
            else
            {
                if (this.state.backtracking > 0)
                {
                    this.state.failed = true;
                    return astParserRuleReturnScope;
                }
                MismatchedSetException ex = new MismatchedSetException(null, this.input);
                throw ex;
            }
        }
        catch (RecognitionException ex2)
        {
            this.ReportError(ex2);
            this.Recover(this.input, ex2);
            astParserRuleReturnScope.Tree = (CommonTree)this.adaptor.ErrorNode(this.input, astParserRuleReturnScope.Start, this.input.LT(-1), ex2);
        }
        return astParserRuleReturnScope;
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_synpred1_ByDScriptingLanguage_fragment()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_synpred1_ByDScriptingLanguage_fragment()
    {
    }

    public void synpred1_ByDScriptingLanguage_fragment()
    {
        this.Match(this.input, 27, ByDScriptingLanguageParser.Follow._COLLOF_in_synpred1_ByDScriptingLanguage568);
        if (!this.state.failed)
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._elementsClause_in_synpred1_ByDScriptingLanguage570);
            this.elementsClause();
            base.PopFollow();
            if (this.state.failed)
            {
            }
        }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_synpred2_ByDScriptingLanguage_fragment()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_synpred2_ByDScriptingLanguage_fragment()
    {
    }

    public void synpred2_ByDScriptingLanguage_fragment()
    {
        this.Match(this.input, 44, ByDScriptingLanguageParser.Follow._ELTSOF_in_synpred2_ByDScriptingLanguage607);
        if (!this.state.failed)
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._typeOfClause_in_synpred2_ByDScriptingLanguage609);
            this.typeOfClause();
            base.PopFollow();
            if (this.state.failed)
            {
            }
        }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_synpred3_ByDScriptingLanguage_fragment()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_synpred3_ByDScriptingLanguage_fragment()
    {
    }

    public void synpred3_ByDScriptingLanguage_fragment()
    {
        this.Match(this.input, 94, ByDScriptingLanguageParser.Follow._TYPOF_in_synpred3_ByDScriptingLanguage646);
        if (!this.state.failed)
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_synpred3_ByDScriptingLanguage648);
            this.fullId();
            base.PopFollow();
            if (this.state.failed)
            {
            }
        }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_synpred4_ByDScriptingLanguage_fragment()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_synpred4_ByDScriptingLanguage_fragment()
    {
    }

    public void synpred4_ByDScriptingLanguage_fragment()
    {
        this.Match(this.input, 41, ByDScriptingLanguageParser.Follow._ELSE_in_synpred4_ByDScriptingLanguage708);
        if (!this.state.failed)
        {
            this.Match(this.input, 54, ByDScriptingLanguageParser.Follow._IF_in_synpred4_ByDScriptingLanguage710);
            if (this.state.failed)
            {
            }
        }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_synpred5_ByDScriptingLanguage_fragment()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_synpred5_ByDScriptingLanguage_fragment()
    {
    }

    public void synpred5_ByDScriptingLanguage_fragment()
    {
        this.Match(this.input, 23, ByDScriptingLanguageParser.Follow._CASE_in_synpred5_ByDScriptingLanguage857);
        if (this.state.failed)
        {
        }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_synpred6_ByDScriptingLanguage_fragment()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_synpred6_ByDScriptingLanguage_fragment()
    {
    }

    public void synpred6_ByDScriptingLanguage_fragment()
    {
        this.Match(this.input, 33, ByDScriptingLanguageParser.Follow._DEFAULT_in_synpred6_ByDScriptingLanguage869);
        if (this.state.failed)
        {
        }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_synpred7_ByDScriptingLanguage_fragment()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_synpred7_ByDScriptingLanguage_fragment()
    {
    }

    public void synpred7_ByDScriptingLanguage_fragment()
    {
        this.Match(this.input, 98, ByDScriptingLanguageParser.Follow._VAR_in_synpred7_ByDScriptingLanguage1067);
        if (!this.state.failed)
        {
            base.PushFollow(ByDScriptingLanguageParser.Follow._fullId_in_synpred7_ByDScriptingLanguage1069);
            this.fullId();
            base.PopFollow();
            if (!this.state.failed)
            {
                this.Match(this.input, 58, ByDScriptingLanguageParser.Follow._INN_in_synpred7_ByDScriptingLanguage1071);
                if (this.state.failed)
                {
                }
            }
        }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_synpred8_ByDScriptingLanguage_fragment()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_synpred8_ByDScriptingLanguage_fragment()
    {
    }

    public void synpred8_ByDScriptingLanguage_fragment()
    {
        this.Match(this.input, 18, ByDScriptingLanguageParser.Follow._BROKEN_STRLIT_in_synpred8_ByDScriptingLanguage2799);
        if (this.state.failed)
        {
        }
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void EnterRule_synpred9_ByDScriptingLanguage_fragment()
    {
    }

    [Conditional("ANTLR_TRACE")]
    protected virtual void LeaveRule_synpred9_ByDScriptingLanguage_fragment()
    {
    }

    public void synpred9_ByDScriptingLanguage_fragment()
    {
        this.Match(this.input, 17, ByDScriptingLanguageParser.Follow._BROKEN_NUMLIT_in_synpred9_ByDScriptingLanguage2825);
        if (this.state.failed)
        {
        }
    }

    private bool EvaluatePredicate(Action fragment)
    {
        this.state.backtracking++;
        int marker = this.input.Mark();
        try
        {
            fragment();
        }
        catch (RecognitionException arg)
        {
            Console.Error.WriteLine("impossible: " + arg);
        }
        bool result = !this.state.failed;
        this.input.Rewind(marker);
        this.state.backtracking--;
        this.state.failed = false;
        return result;
    }
}