﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Common.SemanticCategory
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Mashup.Resources;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups;
using System.Collections.Generic;

namespace SAP.BYD.LS.UI.Mashup.Common
{
  public sealed class SemanticCategory
  {
    private static readonly Dictionary<SemanticCategories, string> _fallbacks = new Dictionary<SemanticCategories, string>()
    {
      {
        SemanticCategories.BusinessFinance,
        "Business & Finance"
      },
      {
        SemanticCategories.LocationTravel,
        "Location & Travel"
      },
      {
        SemanticCategories.NewsReference,
        "News & Reference"
      },
      {
        SemanticCategories.ProductivityTools,
        "Productivity & Tools"
      },
      {
        SemanticCategories.SocialCommunication,
        "Social & Communication"
      },
      {
        SemanticCategories.ApplicationIntegration,
        "Application Integration"
      }
    };
    private static readonly Dictionary<SemanticCategories, string> _displayNames = new Dictionary<SemanticCategories, string>();
    private static readonly Dictionary<string, string> _portBindingStrings = new Dictionary<string, string>();
    private static readonly SemanticCategory _instance = new SemanticCategory();

    public static SemanticCategory Instance
    {
      get
      {
        return SemanticCategory._instance;
      }
    }

    public string this[SemanticCategories key]
    {
      get
      {
        if (SemanticCategory._displayNames.ContainsKey(key))
          return SemanticCategory._displayNames[key];
        if (!SemanticCategory._fallbacks.ContainsKey(key))
          return string.Empty;
        string str = TextLibrary.Texts.GetString(((int) key).ToString() + "Name", SemanticCategory._fallbacks[key]);
        SemanticCategory._displayNames[key] = str;
        return str;
      }
    }

    public string this[string key]
    {
      get
      {
        SemanticCategories result;
        if (!string.IsNullOrEmpty(key) && key.TryParseEnum<SemanticCategories>(true, out result))
          return this[result];
        return string.Empty;
      }
    }

    public string FromPortBindingAsString(string portBinding)
    {
      if (!string.IsNullOrEmpty(portBinding))
      {
        if (SemanticCategory._portBindingStrings.ContainsKey(portBinding))
          return SemanticCategory._portBindingStrings[portBinding];
        MashupCategoryType mashupCategoryByName = MashupRegistry.GetMashupCategoryByName(portBinding);
        if (mashupCategoryByName != null)
        {
          string semanticCategory = mashupCategoryByName.semanticCategory;
          SemanticCategories result;
          if (!string.IsNullOrEmpty(semanticCategory) && semanticCategory.TryParseEnum<SemanticCategories>(true, out result))
          {
            SemanticCategory._portBindingStrings[portBinding] = semanticCategory;
            return semanticCategory;
          }
        }
      }
      return string.Empty;
    }

    public string FromPortBinding(string portBinding, string semanticCategory = null)
    {
      if (!string.IsNullOrEmpty(semanticCategory))
        return semanticCategory;
      return SemanticCategory.Instance.FromPortBindingAsString(portBinding);
    }
  }
}
