﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Common.MashupRegistry
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base.Exceptions;
using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Mashup.Resources;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAP.BYD.LS.UI.Mashup.Common
{
  public sealed class MashupRegistry
  {
    private static readonly object _semaphore = new object();
    private static MashupRegistryType _instance;
    public static MashupRuntimeMode? RuntimeMode;
    private static LoadMashupRegistryWorker _loader;

    public static MashupRegistryType Instance
    {
      get
      {
        if (MashupRegistry._instance == null)
        {
          lock (MashupRegistry._semaphore)
          {
            if (MashupRegistry._instance == null)
            {
              MashupRegistry._loader = new LoadMashupRegistryWorker();
              MashupRegistry._instance = MashupRegistry._loader.LoadMashupRegistry();
            }
          }
        }
        return MashupRegistry._instance;
      }
    }

    public static bool IsLoaded()
    {
      return MashupRegistry.IsInstanceValid();
    }

    private static bool IsInstanceValid()
    {
      if (MashupRegistry._instance != null && MashupRegistry._instance.MashupTypes != null && MashupRegistry._instance.MashupCategories != null)
        return MashupRegistry.RuntimeMode.HasValue;
      return false;
    }

    private static MashupTypeType ContructMashupType(MashupTypeCategories type)
    {
      MashupTypeType mashupTypeType = new MashupTypeType();
      mashupTypeType.id = string.Empty;
      mashupTypeType.name = type;
      mashupTypeType.Description = new DependentPropertyType()
      {
        fallbackValueType = DataFieldTypes.@string,
        fallbackValue = TextLibrary.Texts.GetString(Enum.GetName(typeof (MashupTypeCategories), (object) type))
      };
      return mashupTypeType;
    }

    public static MashupCategoryType GetMashupCategoryByName(string mashupCategoryName)
    {
      if (string.IsNullOrEmpty(mashupCategoryName))
        return (MashupCategoryType) null;
      return MashupRegistry.GetMashupCategoryByCondition((Func<MashupCategoryType, bool>) (mashupCategory => mashupCategory.name.Equals(mashupCategoryName)));
    }

    public static string GetMashupCategoryDescriptionByName(string mashupCategoryName)
    {
      if (!string.IsNullOrEmpty(mashupCategoryName))
      {
        MashupCategoryType mashupCategoryByName = MashupRegistry.GetMashupCategoryByName(mashupCategoryName);
        if (mashupCategoryByName != null && mashupCategoryByName.Description != null)
          return mashupCategoryByName.Description.fallbackValue;
      }
      return (string) null;
    }

    public static string GetMashupCategoryDisplayNameByName(string mashupCategoryName)
    {
      if (!string.IsNullOrEmpty(mashupCategoryName))
      {
        MashupCategoryType mashupCategoryByName = MashupRegistry.GetMashupCategoryByName(mashupCategoryName);
        if (mashupCategoryByName != null && mashupCategoryByName.DisplayName != null)
          return mashupCategoryByName.DisplayName.fallbackValue;
      }
      return (string) null;
    }

    public static MashupCategoryType GetMashupCategoryByCondition(Func<MashupCategoryType, bool> condition)
    {
      if (MashupRegistry.Instance == null)
        throw new InvalidStateException("MashupRegistry doesn't exist.");
      if (condition == null)
        throw new ArgumentNullException("condition");
      if (MashupRegistry.Instance.MashupCategories == null)
        return (MashupCategoryType) null;
      try
      {
        return MashupRegistry.Instance.MashupCategories.MashupCategory.FirstOrDefault<MashupCategoryType>(condition);
      }
      catch (NullReferenceException ex)
      {
        return (MashupCategoryType) null;
      }
    }

    public static MashupTypeType GetMashupTypeByCondition(Func<MashupTypeType, bool> condition)
    {
      if (MashupRegistry.Instance == null)
        throw new InvalidStateException("MashupRegistry doesn't exist.");
      if (condition == null)
        throw new ArgumentNullException("condition");
      if (MashupRegistry.Instance.MashupTypes == null)
        return (MashupTypeType) null;
      try
      {
        return MashupRegistry.Instance.MashupTypes.MashupType.FirstOrDefault<MashupTypeType>(condition);
      }
      catch (NullReferenceException ex)
      {
        return (MashupTypeType) null;
      }
    }

    public static string GetMashupTypeTextByEnum(MashupTypeCategories type)
    {
      return MashupRegistry.GetMashupTypeByCondition((Func<MashupTypeType, bool>) (mashupType => mashupType.name.Equals((object) type))).Description.fallbackValue;
    }

    public static IEnumerable<MashupCategoryType> GetMashupCategoriesByPorts(List<UXPortType> ports)
    {
      List<MashupCategoryType> first = new List<MashupCategoryType>();
      if (ports != null)
      {
        foreach (UXPortType port in ports)
        {
          if (port != null)
          {
            bool isInPort = port is UXInPortType;
            UXPortType port1 = port;
            IEnumerable<MashupCategoryType> source1 = MashupRegistry.Instance.MashupCategories.MashupCategory.Where<MashupCategoryType>((Func<MashupCategoryType, bool>) (mashupCategory =>
            {
              if (string.IsNullOrEmpty(port1.portTypePackage) || string.IsNullOrEmpty(port1.portTypeReference))
                return false;
              if (!isInPort)
              {
                if (mashupCategory.inPortTypePackage.Equals(port1.portTypePackage))
                  return mashupCategory.inPortTypeReference.Equals(port1.portTypeReference);
                return false;
              }
              if (mashupCategory.outPortTypePackage.Equals(port1.portTypePackage))
                return mashupCategory.outPortTypeReference.Equals(port1.portTypeReference);
              return false;
            }));
            IList<MashupCategoryType> source2 = source1 as IList<MashupCategoryType> ?? (IList<MashupCategoryType>) source1.ToList<MashupCategoryType>();
            if (source2.Any<MashupCategoryType>())
              first = first.Union<MashupCategoryType>((IEnumerable<MashupCategoryType>) source2).ToList<MashupCategoryType>();
          }
        }
        first = first.Union<MashupCategoryType>(MashupRegistry.Instance.MashupCategories.MashupCategory.Where<MashupCategoryType>((Func<MashupCategoryType, bool>) (mashupCategory =>
        {
          if (string.IsNullOrEmpty(mashupCategory.inPortTypeReference))
            return string.IsNullOrEmpty(mashupCategory.outPortTypeReference);
          return false;
        }))).ToList<MashupCategoryType>();
        first.Sort((Comparison<MashupCategoryType>) ((c1, c2) => string.Compare(c1.DisplayName.fallbackValue, c2.DisplayName.fallbackValue, StringComparison.Ordinal)));
      }
      return (IEnumerable<MashupCategoryType>) first;
    }

    public static void GetOutPortInfoByMashupCategory(string mashupCategoryName, out string outPortTypePackage, out string outPortTypeReference)
    {
      if (string.IsNullOrEmpty(mashupCategoryName))
        throw new ArgumentNullException("mashupCategoryName");
      MashupCategoryType mashupCategoryType = MashupRegistry.Instance.MashupCategories.MashupCategory.ToDictionary<MashupCategoryType, string>((Func<MashupCategoryType, string>) (s => s.name))[mashupCategoryName];
      if (mashupCategoryType != null)
      {
        outPortTypePackage = mashupCategoryType.outPortTypePackage;
        outPortTypeReference = mashupCategoryType.outPortTypeReference;
      }
      else
      {
        outPortTypePackage = (string) null;
        outPortTypeReference = (string) null;
      }
    }

    public static void GetInPortInfoByMashupCategory(string mashupCategoryName, out string inPortTypePackage, out string inPortTypeReference)
    {
      if (string.IsNullOrEmpty(mashupCategoryName))
        throw new ArgumentNullException("mashupCategoryName");
      MashupCategoryType mashupCategoryType = MashupRegistry.Instance.MashupCategories.MashupCategory.ToDictionary<MashupCategoryType, string>((Func<MashupCategoryType, string>) (s => s.name))[mashupCategoryName];
      if (mashupCategoryType != null)
      {
        inPortTypePackage = mashupCategoryType.inPortTypePackage;
        inPortTypeReference = mashupCategoryType.inPortTypeReference;
      }
      else
      {
        inPortTypePackage = (string) null;
        inPortTypeReference = (string) null;
      }
    }

    public static void GetPortTypeInfoByMashupCategory(string mashupCategoryName, out string inPortTypePackage, out string inPortTypeReference, out string outPortTypePackage, out string outPortTypeReference)
    {
      if (string.IsNullOrEmpty(mashupCategoryName))
        throw new ArgumentNullException("mashupCategoryName");
      MashupCategoryType mashupCategoryType = MashupRegistry.Instance.MashupCategories.MashupCategory.ToDictionary<MashupCategoryType, string>((Func<MashupCategoryType, string>) (s => s.name))[mashupCategoryName];
      if (mashupCategoryType != null)
      {
        inPortTypePackage = mashupCategoryType.inPortTypePackage;
        inPortTypeReference = mashupCategoryType.inPortTypeReference;
        outPortTypePackage = mashupCategoryType.outPortTypePackage;
        outPortTypeReference = mashupCategoryType.outPortTypeReference;
      }
      else
      {
        inPortTypePackage = (string) null;
        outPortTypePackage = (string) null;
        inPortTypeReference = (string) null;
        outPortTypeReference = (string) null;
      }
    }

    public static bool HasOutPort(string mashupCategoryName)
    {
      if (string.IsNullOrEmpty(mashupCategoryName))
        return false;
      MashupCategoryType mashupCategoryType = MashupRegistry.Instance.MashupCategories.MashupCategory.ToDictionary<MashupCategoryType, string>((Func<MashupCategoryType, string>) (s => s.name))[mashupCategoryName];
      return mashupCategoryType != null && !string.IsNullOrEmpty(mashupCategoryType.outPortTypeReference);
    }
  }
}
