﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord.MashupSerivceAppearance
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

namespace SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord
{
  public enum MashupSerivceAppearance
  {
    FunctionBar,
    NewPaneContainer,
    NearPaneContainer,
    ListToolbar,
    SectionLink,
    ViewSwitch,
  }
}
