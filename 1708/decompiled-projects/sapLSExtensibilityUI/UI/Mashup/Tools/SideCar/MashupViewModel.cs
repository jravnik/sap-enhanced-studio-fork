﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.SideCar.MashupViewModel
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Controls;
using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Mashup.Common;
using SAP.BYD.LS.UI.Mashup.Resources;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;

namespace SAP.BYD.LS.UI.Mashup.Tools.SideCar
{
  public class MashupViewModel : ObservableBase
  {
    private Dictionary<string, string> mandatoryParameters = new Dictionary<string, string>();
    private string componentPath;
    private string mashupType;
    private string semanticCategory;
    private string mashupCategory;
    private string mashupInPlug;
    private string mashupOutPlug;
    private string sourceOutPlug;
    private string targetInPlug;
    private string viewStyle;
    private bool isVisible;
    private string serviceDescription;
    private ObservableCollection<MashupValueHelpListModel> appearanceOptions;
    private ObservableCollection<ExtBindingOption> visibilityExtensionFields;
    private string visibilityBindingText;
    private bool fullColumnSpan;
    private string visibilityBindingExpression;
    private string appearanceKey;
    private string serviceAppearance;
    private string embedName;
    private string displayText;
    private string mandatoryParams;
    private string dynParams;
    private string dynOutputs;
    private bool anyDynParamMandatory;
    private ObservableCollection<DynamicParameter> dynamicParameters;
    private ObservableCollection<DynamicParameter> dynamicOutputs;
    private string iconUrl;
    private string containerID;
    private string refPaneContainerID;
    private string viewSwitchNavigationID;
    private string mashupCategoryDescription;
    private bool isActive;
    private bool canBeAdded;
    private bool isPreconfig;
    private bool isPropertyPanelEnabled;
    private string automationId;
    private string isVisibleAutomationId;
    private string displayName;
    private Dictionary<string, ExtBindingOption> dynamicBindings;
    private string pipeID;
    private bool noDynamicParaBinded;

    public bool IsAdaptation { get; set; }

    public MashupViewModel Self
    {
      get
      {
        return this;
      }
    }

    public string ComponentPath
    {
      get
      {
        return this.componentPath;
      }
      set
      {
        this.componentPath = value;
      }
    }

    public string PipeIDWithoutSlashes
    {
      get
      {
        return this.PipeID.Replace('/', '_');
      }
    }

    public string MashupType
    {
      get
      {
        return this.mashupType;
      }
      set
      {
        this.mashupType = value;
      }
    }

    public string MashupTypeText
    {
      get
      {
        if (!string.IsNullOrEmpty(TextLibrary.Texts.GetString(this.MashupType)))
          return TextLibrary.Texts.GetString(this.MashupType);
        return MashupConstants.MashupDic[this.MashupType];
      }
    }

    public string SemanticCategory
    {
      get
      {
        return this.semanticCategory;
      }
      set
      {
        this.semanticCategory = value;
        this.NotifyPropertyChanged("SemanticCategory");
      }
    }

    public bool IsDynamic
    {
      get
      {
        if (string.IsNullOrEmpty(this.MashupCategory))
          return true;
        if (!(this.MashupCategory == "Web_Widget"))
          return false;
        if (string.IsNullOrEmpty(this.DynParams))
          return !string.IsNullOrEmpty(this.DynOutputs);
        return true;
      }
    }

    public bool IsWebWidget
    {
      get
      {
        if (!(this.MashupCategory == "Web_Widget"))
          return false;
        if (string.IsNullOrEmpty(this.DynParams))
          return !string.IsNullOrEmpty(this.DynOutputs);
        return true;
      }
    }

    public string MashupCategory
    {
      get
      {
        return this.mashupCategory;
      }
      set
      {
        this.mashupCategory = value;
        this.NotifyPropertyChanged("MashupCategory");
      }
    }

    public string MashupInPlug
    {
      get
      {
        return this.mashupInPlug;
      }
      set
      {
        this.mashupInPlug = value;
      }
    }

    public string MashupOutPlug
    {
      get
      {
        return this.mashupOutPlug;
      }
      set
      {
        this.mashupOutPlug = value;
      }
    }

    public string SourceOutPlug
    {
      get
      {
        return this.sourceOutPlug;
      }
      set
      {
        this.sourceOutPlug = value;
      }
    }

    public string SourceOutPlugDescription { get; set; }

    public bool OnlyOneSourceOutPlug { get; set; }

    public Visibility SourceOutPlugVisibility
    {
      get
      {
        return !string.IsNullOrEmpty(this.SourceOutPlug) && !this.OnlyOneSourceOutPlug ? Visibility.Visible : Visibility.Collapsed;
      }
    }

    public string TargetInPlug
    {
      get
      {
        return this.targetInPlug;
      }
      set
      {
        this.targetInPlug = value;
      }
    }

    public Visibility TargetInPlugVisibility
    {
      get
      {
        return !string.IsNullOrEmpty(this.TargetInPlug) && !this.OnlyOneSourceOutPlug ? Visibility.Visible : Visibility.Collapsed;
      }
    }

    public string ViewStyle
    {
      get
      {
        return this.viewStyle;
      }
      set
      {
        this.viewStyle = value;
        this.NotifyPropertyChanged("ViewStyle");
      }
    }

    public bool OldVisibility { get; set; }

    public bool IsVisible
    {
      get
      {
        return this.isVisible;
      }
      set
      {
        this.isVisible = value;
        if (!this.IsVisible)
          this.FullColumnSpan = false;
        this.NotifyPropertyChanged("IsVisible");
      }
    }

    public string ServiceDescription
    {
      get
      {
        return this.serviceDescription;
      }
      set
      {
        this.serviceDescription = value;
        this.NotifyPropertyChanged("ServiceDiscription");
      }
    }

    public ObservableCollection<MashupValueHelpListModel> AppearanceOptions
    {
      get
      {
        return this.appearanceOptions;
      }
      set
      {
        this.appearanceOptions = value;
        this.NotifyPropertyChanged("AppearanceOptions");
      }
    }

    public ObservableCollection<ExtBindingOption> VisibilityExtensionFields
    {
      get
      {
        return this.visibilityExtensionFields;
      }
      set
      {
        this.visibilityExtensionFields = value;
        this.NotifyPropertyChanged("VisibilityExtensionFields");
      }
    }

    public string VisibilityBindingText
    {
      get
      {
        return this.visibilityBindingText;
      }
      set
      {
        this.visibilityBindingText = value;
        this.NotifyPropertyChanged("VisibilityBindingText");
      }
    }

    public bool FullColumnSpan
    {
      get
      {
        return this.fullColumnSpan;
      }
      set
      {
        this.fullColumnSpan = value;
        this.NotifyPropertyChanged("FullColumnSpan");
      }
    }

    public string VisibilityBindingExpression
    {
      get
      {
        return this.visibilityBindingExpression;
      }
      set
      {
        this.visibilityBindingExpression = value;
        if (this.VisibilityExtensionFields != null)
        {
          foreach (ExtBindingOption visibilityExtensionField in (Collection<ExtBindingOption>) this.VisibilityExtensionFields)
          {
            if (visibilityExtensionField.Code.Equals(this.visibilityBindingExpression))
            {
              this.VisibilityBindingText = visibilityExtensionField.Text;
              break;
            }
          }
        }
        this.NotifyPropertyChanged("VisibilityBindingExpression");
      }
    }

    public string OldAppearance { get; set; }

    public string OldAppearanceKey { get; set; }

    public string AppearanceKey
    {
      get
      {
        return this.appearanceKey;
      }
      set
      {
        if (!string.IsNullOrEmpty(value))
        {
          this.appearanceKey = value;
          if (this.AppearanceOptions == null)
            return;
          MashupValueHelpListModel valueHelpListModel = this.AppearanceOptions.FirstOrDefault<MashupValueHelpListModel>((Func<MashupValueHelpListModel, bool>) (o => o.Code == value));
          if (valueHelpListModel == null)
            return;
          string code = valueHelpListModel.Code;
          if (code.StartsWith("S-") || code.StartsWith("T-") || code.StartsWith("V-"))
            this.ContainerID = code.Substring(2);
          else if (code.StartsWith("P-"))
          {
            Match match = Regex.Match(code, "P-(?<PaneContainerID>.+);ViewTypeID-(?<ParentID>.*)");
            if (match.Success)
            {
              string str = match.Groups["PaneContainerID"].ToString();
              this.ContainerID = match.Groups["ParentID"].ToString();
              this.RefPaneContainerID = str;
            }
          }
          if (code.StartsWith("S-"))
            this.Appearance = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.SectionLink);
          else if (code.StartsWith("T-"))
            this.Appearance = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ListToolbar);
          else if (code.StartsWith("P-"))
            this.Appearance = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.NearPaneContainer);
          else if (code.StartsWith("V-"))
            this.Appearance = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.NewPaneContainer);
          else if (code.Equals(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.FunctionBar)))
            this.Appearance = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.FunctionBar);
          else if (code.StartsWith(Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ViewSwitch)))
          {
            this.Appearance = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.ViewSwitch);
            this.ViewSwitchNavitgationID = code.Substring(code.IndexOf("-") + 1);
            this.ContainerID = this.ViewSwitchNavitgationID;
          }
          this.ServiceAppearance = valueHelpListModel.Text;
        }
        else
          this.serviceAppearance = string.Empty;
      }
    }

    public string ServiceAppearance
    {
      get
      {
        return this.serviceAppearance;
      }
      set
      {
        if (string.IsNullOrEmpty(value))
          return;
        this.serviceAppearance = value;
        this.NotifyPropertyChanged("ServiceAppearance");
      }
    }

    public string EmbedName
    {
      get
      {
        return this.embedName;
      }
      set
      {
        if (string.IsNullOrEmpty(value))
          return;
        this.embedName = value;
      }
    }

    public string DisplayText
    {
      get
      {
        return this.displayText;
      }
      set
      {
        this.displayText = value;
        this.NotifyPropertyChanged("DisplayText");
      }
    }

    public string MandatoryParams
    {
      get
      {
        return this.mandatoryParams;
      }
      set
      {
        this.mandatoryParams = value;
        if (string.IsNullOrEmpty(value))
          return;
        string mandatoryParams = this.mandatoryParams;
        char[] chArray = new char[1]{ ';' };
        foreach (string index in mandatoryParams.Split(chArray))
          this.mandatoryParameters[index] = index;
      }
    }

    public string DynParams
    {
      get
      {
        return this.dynParams;
      }
      set
      {
        this.dynParams = value;
        this.DynamicParameters = new ObservableCollection<DynamicParameter>();
        this.ShowExtBinding = !string.IsNullOrEmpty(value);
        if (this.ShowExtBinding && value != null)
        {
          string str = value;
          char[] chArray = new char[1]{ ';' };
          foreach (string key in str.Split(chArray))
          {
            if (key != "$MashupResult")
            {
              bool flag = this.mandatoryParameters.ContainsKey(key);
              DynamicParameter dynamicParameter = new DynamicParameter()
              {
                Name = key,
                BindingOptions = new ObservableCollection<ExtBindingOption>(),
                Binding = string.Empty,
                IsMandatory = flag,
                IsPropertyPanelEnabled = this.IsPropertyPanelEnabled
              };
              this.DynamicParameters.Add(dynamicParameter);
              if (flag)
                this.AnyDynParamMandatory = true;
              else
                dynamicParameter.BindingOptions.Add(ExtBindingOption.EmptyInstance);
            }
          }
        }
        this.NotifyPropertyChanged("DynParams");
      }
    }

    public string DynOutputs
    {
      get
      {
        return this.dynOutputs;
      }
      set
      {
        this.dynOutputs = value;
        this.DynamicOutputs = new ObservableCollection<DynamicParameter>();
        this.ShowDynOutput = !string.IsNullOrEmpty(value);
        if (this.ShowDynOutput && value != null)
        {
          string str1 = value;
          char[] chArray = new char[1]{ ';' };
          foreach (string str2 in str1.Split(chArray))
          {
            DynamicParameter dynamicParameter1 = new DynamicParameter();
            dynamicParameter1.Name = str2;
            DynamicParameter dynamicParameter2 = dynamicParameter1;
            ObservableCollection<ExtBindingOption> observableCollection1 = new ObservableCollection<ExtBindingOption>();
            observableCollection1.Add(ExtBindingOption.EmptyInstance);
            ObservableCollection<ExtBindingOption> observableCollection2 = observableCollection1;
            dynamicParameter2.BindingOptions = observableCollection2;
            dynamicParameter1.Binding = string.Empty;
            dynamicParameter1.IsPropertyPanelEnabled = this.IsPropertyPanelEnabled;
            dynamicParameter1.IsOutput = true;
            this.DynamicOutputs.Add(dynamicParameter1);
          }
        }
        this.NotifyPropertyChanged("DynOutputs");
      }
    }

    public bool ShowDynamicAppearance
    {
      get
      {
        if (!this.ShowExtBinding)
          return this.ShowDynOutput;
        return true;
      }
    }

    public bool ShowExtBinding { get; set; }

    public bool ShowDynOutput { get; set; }

    public bool AnyDynParamMandatory
    {
      get
      {
        return this.anyDynParamMandatory;
      }
      set
      {
        this.anyDynParamMandatory = value;
      }
    }

    public ObservableCollection<DynamicParameter> DynamicParameters
    {
      get
      {
        return this.dynamicParameters;
      }
      set
      {
        this.dynamicParameters = value;
        this.NotifyPropertyChanged("DynamicParameters");
      }
    }

    public ObservableCollection<DynamicParameter> DynamicOutputs
    {
      get
      {
        return this.dynamicOutputs;
      }
      set
      {
        this.dynamicOutputs = value;
        this.NotifyPropertyChanged("DynamicOutputs");
      }
    }

    public string IconUrl
    {
      get
      {
        return this.iconUrl;
      }
      set
      {
        this.iconUrl = value;
        this.NotifyPropertyChanged("IconUrl");
      }
    }

    public string ContainerID
    {
      get
      {
        return this.containerID;
      }
      set
      {
        this.containerID = value;
        this.NotifyPropertyChanged("ContainerID");
      }
    }

    public string RefPaneContainerID
    {
      get
      {
        return this.refPaneContainerID;
      }
      set
      {
        this.refPaneContainerID = value;
      }
    }

    public string ViewSwitchNavitgationID
    {
      get
      {
        return this.viewSwitchNavigationID;
      }
      set
      {
        this.viewSwitchNavigationID = value;
      }
    }

    public string MashupCategoryDescription
    {
      get
      {
        return this.mashupCategoryDescription;
      }
      set
      {
        this.mashupCategoryDescription = value;
        this.NotifyPropertyChanged("MashupCategoryDescription");
      }
    }

    public bool IsActive
    {
      get
      {
        return this.isActive;
      }
      set
      {
        this.isActive = value;
        this.NotifyPropertyChanged("IsActive");
      }
    }

    public bool IsKeyUser { get; set; }

    public bool OutputBound
    {
      get
      {
        if (this.DynamicOutputs != null)
        {
          foreach (DynamicParameter dynamicOutput in (Collection<DynamicParameter>) this.DynamicOutputs)
          {
            if (dynamicOutput.Bound)
              return true;
          }
        }
        return false;
      }
    }

    public bool CanBeAdded
    {
      get
      {
        return this.canBeAdded;
      }
      set
      {
        this.canBeAdded = value;
        if (this.CanBeAddedChanged != null)
          this.CanBeAddedChanged(value);
        this.NotifyPropertyChanged("CanBeAdded");
      }
    }

    public bool IsPreconfig
    {
      get
      {
        return this.isPreconfig;
      }
      set
      {
        this.isPreconfig = value;
      }
    }

    public bool IsPropertyPanelEnabled
    {
      get
      {
        return this.isPropertyPanelEnabled;
      }
      set
      {
        this.isPropertyPanelEnabled = value;
        this.CanBeAdded = value;
        bool flag = this.AppearanceOptions.Count == 0;
        if (this.DynamicParameters != null)
        {
          foreach (DynamicParameter dynamicParameter in (Collection<DynamicParameter>) this.DynamicParameters)
          {
            int count = dynamicParameter.BindingOptions.Count;
            if (value && dynamicParameter.IsMandatory && string.IsNullOrEmpty(dynamicParameter.Binding))
              this.CanBeAdded = false;
            dynamicParameter.IsPropertyPanelEnabled = value;
          }
        }
        if (this.DynamicOutputs != null)
        {
          foreach (DynamicParameter dynamicOutput in (Collection<DynamicParameter>) this.DynamicOutputs)
            dynamicOutput.IsPropertyPanelEnabled = value;
        }
        if (flag)
          this.CanBeAdded = false;
        this.NotifyPropertyChanged("IsPropertyPanelEnabled");
      }
    }

    public string AutomationId
    {
      get
      {
        return this.automationId;
      }
      set
      {
        this.automationId = value;
        this.PipeAutomationId = this.automationId + "_" + this.PipeID;
        this.IsVisibleAutomationId = this.PipeAutomationId + "_IsVisibleCheck";
      }
    }

    public string PipeAutomationId { get; set; }

    public string IsVisibleAutomationId
    {
      get
      {
        return this.isVisibleAutomationId;
      }
      set
      {
        this.isVisibleAutomationId = value;
      }
    }

    public bool AnyDynParamHasBinding { get; set; }

    public ObservableCollection<ByDMessage> ErrorMessages { get; set; }

    public FlexAnchorEnumType AnchorType { get; set; }

    public string AnchorXrepPath { get; set; }

    public bool OneAppearance { get; set; }

    public string Appearance { get; set; }

    public int AppearanceIndex { get; set; }

    public string Description { get; set; }

    public string DisplayName
    {
      get
      {
        return this.displayName;
      }
      set
      {
        this.displayName = value;
        this.DisplayText = value;
      }
    }

    public Dictionary<string, ExtBindingOption> DynamicBindings
    {
      get
      {
        if (this.dynamicBindings == null)
          this.dynamicBindings = new Dictionary<string, ExtBindingOption>();
        return this.dynamicBindings;
      }
      set
      {
        this.dynamicBindings = value;
      }
    }

    public bool MultipleAppearances { get; set; }

    public bool NotExisting { get; set; }

    public string PipeID
    {
      get
      {
        return this.pipeID;
      }
      set
      {
        this.pipeID = value;
        this.NotifyPropertyChanged("PipeID");
      }
    }

    public string ReferencedAnchorXrepPath { get; set; }

    public bool NoDynamicParaBinded
    {
      get
      {
        return this.noDynamicParaBinded;
      }
      set
      {
        this.noDynamicParaBinded = value;
        this.NotifyPropertyChanged("NoDynamicParaBinded");
      }
    }

    public event MashupViewModel.CanBeAddedChangedEventHandler CanBeAddedChanged;

    public void CheckCanBeAdded()
    {
      bool flag1 = this.AppearanceOptions.Count == 0;
      bool flag2 = true;
      if (this.DynamicParameters != null && this.DynamicParameters.Count > 0)
      {
        this.NoDynamicParaBinded = true;
        foreach (DynamicParameter dynamicParameter in (Collection<DynamicParameter>) this.DynamicParameters)
        {
          int count = dynamicParameter.BindingOptions.Count;
          if (dynamicParameter.IsMandatory && string.IsNullOrEmpty(dynamicParameter.Binding))
          {
            flag2 = false;
            this.CanBeAdded = false;
          }
          if (!string.IsNullOrEmpty(dynamicParameter.Binding))
            this.NoDynamicParaBinded = false;
        }
      }
      if (flag2)
        this.CanBeAdded = true;
      if (!flag1)
        return;
      this.CanBeAdded = false;
    }

    public bool CheckAppearanceValidity()
    {
      if (this.AppearanceOptions == null || this.AppearanceOptions.Count <= 0)
        return this.ShowExtBinding;
      return true;
    }

    public bool CheckVisibility(bool isAdaptation)
    {
      if (this.IsVisible)
      {
        if (this.ShowExtBinding || this.ShowDynOutput)
        {
          if (this.AnyDynParamHasBinding)
          {
            this.IsPropertyPanelEnabled = isAdaptation;
          }
          else
          {
            this.IsPropertyPanelEnabled = !this.IsPreconfig;
            this.ShowExtBinding = isAdaptation;
            this.ShowDynOutput = isAdaptation;
          }
          if (this.IsDynamic)
          {
            this.ResetExtensionAppearance();
            foreach (MashupValueHelpListModel appearanceOption in (Collection<MashupValueHelpListModel>) this.AppearanceOptions)
            {
              if (appearanceOption.Code == this.AppearanceKey)
                this.ServiceAppearance = appearanceOption.Text;
            }
          }
        }
        return true;
      }
      if (isAdaptation)
        return true;
      if (this.AnyDynParamMandatory)
        return false;
      this.ShowExtBinding = false;
      this.ShowDynOutput = false;
      return true;
    }

    public void ResetExtensionAppearance()
    {
      ObservableCollection<MashupValueHelpListModel> appearanceOptions = this.AppearanceOptions;
      if (appearanceOptions.Count > 0)
      {
        bool flag1 = false;
        bool flag2 = false;
        bool flag3 = false;
        string str1 = string.Empty;
        string str2 = string.Empty;
        string str3 = string.Empty;
        foreach (MashupValueHelpListModel valueHelpListModel in (Collection<MashupValueHelpListModel>) appearanceOptions)
        {
          if (valueHelpListModel.Code == CommonUtil.FunctionBarAsString)
          {
            flag1 = true;
            break;
          }
          if (valueHelpListModel.Code.StartsWith("V-"))
          {
            flag2 = true;
            str1 = valueHelpListModel.Code;
            str2 = valueHelpListModel.Text;
          }
          else if (valueHelpListModel.Code.StartsWith("ViewSwitch"))
          {
            flag3 = true;
            str3 = valueHelpListModel.Code.Substring(valueHelpListModel.Code.IndexOf("-") + 1);
          }
        }
        appearanceOptions.Clear();
        if (flag1)
          appearanceOptions.Add(new MashupValueHelpListModel()
          {
            Text = TextLibrary.Texts.GetString("Menu_In_Function_Bar"),
            Code = CommonUtil.FunctionBarAsString
          });
        if (flag2)
          appearanceOptions.Add(new MashupValueHelpListModel()
          {
            Text = str2,
            Code = str1
          });
        if (flag3)
          appearanceOptions.Add(new MashupValueHelpListModel()
          {
            Text = TextLibrary.Texts.GetString("ViewSwitch", "ViewSwitch"),
            Code = "ViewSwitch-" + str3
          });
      }
      Dictionary<string, string> appearanceDic = new Dictionary<string, string>();
      if (this.DynamicParameters != null)
      {
        foreach (DynamicParameter dynamicParameter in (Collection<DynamicParameter>) this.DynamicParameters)
          this.ResetAppearanceByDynamicParameter(dynamicParameter, appearanceOptions, appearanceDic);
      }
      if (this.DynamicOutputs == null)
        return;
      foreach (DynamicParameter dynamicOutput in (Collection<DynamicParameter>) this.DynamicOutputs)
        this.ResetAppearanceByDynamicParameter(dynamicOutput, appearanceOptions, appearanceDic);
    }

    private void ResetAppearanceByDynamicParameter(DynamicParameter dynParam, ObservableCollection<MashupValueHelpListModel> options, Dictionary<string, string> appearanceDic)
    {
      string binding = dynParam.Binding;
      if (string.IsNullOrEmpty(binding))
        return;
      foreach (ExtBindingOption bindingOption in (Collection<ExtBindingOption>) dynParam.BindingOptions)
      {
        if (bindingOption.Code == binding && bindingOption.AppearanceInfo != null)
        {
          string appearanceKey = bindingOption.AppearanceInfo[this.ViewStyle].AppearanceKey;
          string appearanceText = bindingOption.AppearanceInfo[this.ViewStyle].AppearanceText;
          if (appearanceDic.ContainsKey(appearanceKey) || appearanceDic.ContainsValue(appearanceText))
            break;
          appearanceDic[appearanceKey] = appearanceText;
          options.Add(new MashupValueHelpListModel()
          {
            Code = appearanceKey,
            Text = appearanceText
          });
          break;
        }
      }
    }

    public void SetEntensionAppearance()
    {
      if (!this.IsVisible || !this.ShowExtBinding && !this.ShowDynOutput || !this.IsDynamic)
        return;
      this.ResetExtensionAppearance();
      foreach (MashupValueHelpListModel appearanceOption in (Collection<MashupValueHelpListModel>) this.AppearanceOptions)
      {
        if (appearanceOption.Code == this.AppearanceKey)
          this.ServiceAppearance = appearanceOption.Text;
      }
    }

    public void Terminate()
    {
      if (this.AppearanceOptions != null)
      {
        this.AppearanceOptions.Clear();
        this.AppearanceOptions = (ObservableCollection<MashupValueHelpListModel>) null;
      }
      if (this.DynamicParameters != null)
      {
        foreach (DynamicParameter dynamicParameter in (Collection<DynamicParameter>) this.DynamicParameters)
          dynamicParameter.Terminate();
        this.DynamicParameters.Clear();
        this.DynamicParameters = (ObservableCollection<DynamicParameter>) null;
      }
      if (this.DynamicOutputs != null)
      {
        foreach (DynamicParameter dynamicOutput in (Collection<DynamicParameter>) this.DynamicOutputs)
          dynamicOutput.Terminate();
        this.DynamicOutputs.Clear();
        this.DynamicOutputs = (ObservableCollection<DynamicParameter>) null;
      }
      if (this.ErrorMessages != null)
      {
        this.ErrorMessages.Clear();
        this.ErrorMessages = (ObservableCollection<ByDMessage>) null;
      }
      if (this.mandatoryParameters != null)
      {
        this.mandatoryParameters.Clear();
        this.mandatoryParameters = (Dictionary<string, string>) null;
      }
      if (this.VisibilityExtensionFields == null)
        return;
      this.VisibilityExtensionFields.Clear();
      this.VisibilityExtensionFields = (ObservableCollection<ExtBindingOption>) null;
    }

    public delegate void CanBeAddedChangedEventHandler(bool canBeAdded);
  }
}
