﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.SideCar.CommonUtil
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Connector.Services.Mashup;
using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Mashup.Resources;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;

namespace SAP.BYD.LS.UI.Mashup.Tools.SideCar
{
  public static class CommonUtil
  {
    public static XNamespace UXC_NAMESPACE = (XNamespace) "http://www.sap.com/a1s/cd/oberon/uxcontroller-1.0";
    public static XNamespace UXV_NAMESPACE = (XNamespace) "http://www.sap.com/a1s/cd/oberon/uxview-1.0";
    public static readonly string FunctionBarAsString = Enum.GetName(typeof (MashupSerivceAppearance), (object) MashupSerivceAppearance.FunctionBar);

    public static string CreateElementID()
    {
      return Guid.NewGuid().ToString("N");
    }

    public static ObservableCollection<ExtBindingOption> GetDynamicBindingOption(this MashupViewModel mashupViewModel, XDocument viewModelDoc, XDocument dataModelDoc, UXOutPortType extensionFieldOutPort, TextPool textPool, string xrepPath = null)
    {
      if (mashupViewModel == null || viewModelDoc == null || (dataModelDoc == null || extensionFieldOutPort == null))
        return (ObservableCollection<ExtBindingOption>) null;
      ObservableCollection<ExtBindingOption> observableCollection = new ObservableCollection<ExtBindingOption>();
      if (dataModelDoc.Root != null && viewModelDoc.Root != null)
      {
        foreach (UXParameterType parameter in extensionFieldOutPort.Parameter)
        {
          if (!string.IsNullOrEmpty(parameter.bind) && parameter.bind.Contains("/"))
          {
            string[] strArray = parameter.bind.Split('/');
            string bind = strArray[strArray.Length - 1];
            IEnumerable<XElement> source = dataModelDoc.Root.Descendants().Where<XElement>((Func<XElement, bool>) (element =>
            {
              if (element.Attribute((XName) "name") != null)
                return element.Attribute((XName) "name").Value == bind;
              return false;
            }));
            if (source != null && source.Count<XElement>() > 0)
            {
              XElement xelement = source.First<XElement>();
              ExtBindingOption extBindingOption = (ExtBindingOption) null;
              if (xelement.Name.LocalName == "DataField")
              {
                string valueType = xelement.Attribute((XName) "type") != null ? xelement.Attribute((XName) "type").Value : string.Empty;
                extBindingOption = mashupViewModel.GetDynamicFieldBinding(textPool, viewModelDoc, bind, parameter, valueType, xrepPath);
              }
              if (extBindingOption != null)
                observableCollection.Add(extBindingOption);
            }
          }
        }
      }
      if (mashupViewModel.VisibilityExtensionFields == null)
        mashupViewModel.VisibilityExtensionFields = new ObservableCollection<ExtBindingOption>();
      foreach (ExtBindingOption extBindingOption in (Collection<ExtBindingOption>) observableCollection)
      {
        if (extBindingOption.ValueType.Equals("boolean"))
          mashupViewModel.VisibilityExtensionFields.Add(extBindingOption);
      }
      return observableCollection;
    }

    internal static ExtBindingOption GetDynamicFieldBinding(this MashupViewModel mashupViewModel, TextPool textPool, XDocument viewModelDoc, string bind, UXParameterType parameter, string valueType, string xrepPath)
    {
      IEnumerable<XElement> source = viewModelDoc.Root.Descendants().Where<XElement>((Func<XElement, bool>) (element =>
      {
        if (element.Attribute((XName) "bindingExpression") != null)
          return element.Attribute((XName) "bindingExpression").Value.EndsWith(bind);
        return false;
      }));
      if (source != null && source.Count<XElement>() > 0)
      {
        foreach (XElement xelement in source.ToList<XElement>())
        {
          if (xelement != null && xelement.Parent != null && xelement.Parent.Parent != null && ((xelement.Name.LocalName.Equals("Value") || xelement.Name.LocalName.Equals("Text")) && xelement.Parent.Parent.Name.LocalName.Equals("SectionGroupItem")))
          {
            XElement parent = xelement.Parent.Parent;
            return mashupViewModel.GetDynamicBindingFromSectionGroupItem(parent, textPool, parameter, xrepPath, valueType);
          }
        }
      }
      return (ExtBindingOption) null;
    }

    internal static ExtBindingOption GetDynamicBindingFromSectionGroupItem(this MashupViewModel mashupViewModel, XElement sectionGroupItem, TextPool textPool, UXParameterType parameter, string xrepPath = null, string valueType = null)
    {
      XAttribute xattribute1 = sectionGroupItem.Attribute((XName) "extendedBy");
      string str1 = xattribute1 != null ? xattribute1.Value : string.Empty;
      XElement xelement = sectionGroupItem.Element(CommonUtil.UXV_NAMESPACE + "Label");
      XElement parent1 = sectionGroupItem.Parent;
      XAttribute xattribute2 = parent1.Attribute((XName) "id");
      foreach (XElement element in parent1.Elements(CommonUtil.UXV_NAMESPACE + "StableAnchor"))
      {
        if (xattribute2 != null && !string.IsNullOrEmpty(xattribute2.Value) && (xelement != null && element != null))
        {
          string str2 = xattribute2.Value;
          XAttribute xattribute3 = xelement.Attribute((XName) "textPoolId");
          string str3 = string.Empty;
          if (xattribute3 != null && !string.IsNullOrEmpty(xattribute3.Value))
            str3 = textPool.Lookup(xattribute3.Value);
          if (string.IsNullOrEmpty(str3))
          {
            XAttribute xattribute4 = xelement.Attribute((XName) "fallbackValue");
            if (xattribute4 != null)
              str3 = xattribute4.Value;
          }
          XAttribute xattribute5 = element.Attribute((XName) "type");
          XAttribute xattribute6 = element.Attribute((XName) "xrepPath");
          bool flag = true;
          if (!string.IsNullOrEmpty(xrepPath))
            flag = xrepPath.Equals(xattribute6.Value);
          FlexAnchorEnumType result;
          if (xattribute5 != null && !string.IsNullOrEmpty(xattribute5.Value) && (xattribute6 != null && !string.IsNullOrEmpty(xattribute6.Value)) && (flag && Enum.TryParse<FlexAnchorEnumType>(xattribute5.Value, out result)))
          {
            ExtBindingOption extBindingOption1 = new ExtBindingOption();
            extBindingOption1.Code = parameter.bind;
            extBindingOption1.Text = string.Format(TextLibrary.Texts.GetString("Ext.Param.Prefix", "Extension Field \"{0}\""), (object) str3);
            extBindingOption1.ParameterName = parameter.name;
            extBindingOption1.ExternalReference = str1;
            extBindingOption1.AppearanceInfo = new Dictionary<string, ExtAppearanceInfo>();
            extBindingOption1.ValueType = valueType;
            ExtBindingOption extBindingOption2 = extBindingOption1;
            ExtAppearanceInfo extAppearanceInfo1 = new ExtAppearanceInfo()
            {
              Type = result,
              XrepPath = xattribute6.Value,
              AppearanceKey = "S-" + str2,
              AppearanceText = string.Format(TextLibrary.Texts.GetString("Link.Near.Field", "Link near {0} Field"), string.IsNullOrEmpty(str3) ? (object) TextLibrary.Texts.GetString("Untitled") : (object) str3)
            };
            ExtAppearanceInfo extAppearanceInfo2 = (ExtAppearanceInfo) null;
            XElement parent2 = parent1.Parent.Parent;
            if (parent2.Name.LocalName.Equals("Member"))
              parent2 = parent2.Parent.Parent;
            XElement parent3 = parent2.Parent;
            XAttribute xattribute4 = parent2.Attribute((XName) "id");
            XAttribute xattribute7 = parent3.Attribute((XName) "id");
            if (xattribute7 != null && xattribute4 != null && (!string.IsNullOrEmpty(xattribute4.Value) && !string.IsNullOrEmpty(xattribute7.Value)))
              extAppearanceInfo2 = new ExtAppearanceInfo()
              {
                Type = result,
                XrepPath = xattribute6.Value,
                AppearanceKey = "P-" + xattribute4.Value + ";ViewTypeID-" + xattribute7.Value,
                AppearanceText = string.Format(TextLibrary.Texts.GetString("Embed.Near.Field", "Embedded near {0} Field"), string.IsNullOrEmpty(str3) ? (object) TextLibrary.Texts.GetString("Untitled") : (object) str3)
              };
            if (mashupViewModel.ShowDynOutput)
            {
              extBindingOption2.AppearanceInfo[MashupViewStyleType.newDialog.ToString()] = extAppearanceInfo1;
              if (extAppearanceInfo2 != null)
                extBindingOption2.AppearanceInfo[MashupViewStyleType.inScreen.ToString()] = extAppearanceInfo2;
            }
            else
            {
              switch ((MashupViewStyleType) Enum.Parse(typeof (MashupViewStyleType), mashupViewModel.ViewStyle, false))
              {
                case MashupViewStyleType.newWindow:
                case MashupViewStyleType.newDialog:
                  extBindingOption2.AppearanceInfo[mashupViewModel.ViewStyle] = extAppearanceInfo1;
                  break;
                case MashupViewStyleType.inScreen:
                  extBindingOption2.AppearanceInfo[mashupViewModel.ViewStyle] = extAppearanceInfo2;
                  break;
              }
            }
            return extBindingOption2;
          }
        }
      }
      return (ExtBindingOption) null;
    }

    public static bool IsFunctionCallSuccess(int returnCode, OSLS_XREP_RETURN_MSG[] messages, out List<string> messageList)
    {
      bool flag = true;
      messageList = new List<string>();
      if (returnCode != 0)
      {
        flag = false;
        if (messages != null)
        {
          int length = messages.Length;
          if (length == 0)
          {
            if (returnCode == 403)
              messageList.Add(TextLibrary.Texts.GetString("403OperationFailedMsg", "Operation not possible; this operation can only be performed by a key user assigned to the Flexibility Change Log view."));
            else
              messageList.Add(string.Format("Return Code is {0}", (object) returnCode));
          }
          else
          {
            int index1 = 0;
            for (int index2 = length; index1 < index2; ++index1)
              messageList.Add(messages[index1].MESSAGE);
          }
        }
      }
      return flag;
    }
  }
}
