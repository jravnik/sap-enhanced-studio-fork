﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Tools.SideCar.ExtBindingOption
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Mashup.Common;
using System.Collections.Generic;

namespace SAP.BYD.LS.UI.Mashup.Tools.SideCar
{
  public class ExtBindingOption : MashupValueHelpListModel
  {
    private const string EMPTY_CODE = "EmptyExtensionField";
    private static ExtBindingOption emptyInstance;

    public bool IsOutput { get; set; }

    public static ExtBindingOption EmptyInstance
    {
      get
      {
        if (ExtBindingOption.emptyInstance == null)
        {
          ExtBindingOption extBindingOption = new ExtBindingOption();
          extBindingOption.Text = string.Empty;
          extBindingOption.Code = "EmptyExtensionField";
          extBindingOption.ParameterName = string.Empty;
          extBindingOption.ExternalReference = string.Empty;
          ExtBindingOption.emptyInstance = extBindingOption;
        }
        return ExtBindingOption.emptyInstance;
      }
    }

    public string ParameterBind
    {
      get
      {
        return this.Code;
      }
    }

    public string ParameterName { get; set; }

    public string ExternalReference { get; set; }

    public Dictionary<string, ExtAppearanceInfo> AppearanceInfo { get; set; }

    public string ValueType { get; set; }
  }
}
