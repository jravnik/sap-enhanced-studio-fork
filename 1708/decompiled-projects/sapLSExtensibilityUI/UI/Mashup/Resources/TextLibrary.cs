﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UI.Mashup.Resources.TextLibrary
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base;

namespace SAP.BYD.LS.UI.Mashup.Resources
{
  public class TextLibrary
  {
    private static ResourceBundle texts;

    public static ResourceBundle Texts
    {
      get
      {
        if (TextLibrary.texts == null)
          TextLibrary.texts = ResourceBundle.GetResourceBundle("sapLSUIMashup;component/Resources/Text");
        return TextLibrary.texts;
      }
    }
  }
}
