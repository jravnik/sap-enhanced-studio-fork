﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("SAP")]
[assembly: Guid("8d5cd0f5-11b5-4b63-9524-ee6eb6358f7a")]
[assembly: AssemblyFileVersion("25.0.555.1045")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("sapLSExtensibilityUI")]
[assembly: AssemblyCopyright("Copyright © SAP 2010")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: AssemblyDescription("")]
[assembly: AssemblyTitle("sapLSExtensibilityUI")]
[assembly: AssemblyVersion("25.0.555.1045")]
