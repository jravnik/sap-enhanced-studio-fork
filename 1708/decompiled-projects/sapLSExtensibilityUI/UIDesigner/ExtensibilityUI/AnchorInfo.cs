﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.AnchorInfo
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI
{
  public class AnchorInfo
  {
    [XmlAttribute]
    public FlexAnchorEnumType AnchorType { get; set; }

    [XmlArray]
    public List<ChangeTransactionInfo> AllowedChangeTransactions { get; set; }

    public AnchorInfo()
    {
      this.AllowedChangeTransactions = new List<ChangeTransactionInfo>();
    }
  }
}
