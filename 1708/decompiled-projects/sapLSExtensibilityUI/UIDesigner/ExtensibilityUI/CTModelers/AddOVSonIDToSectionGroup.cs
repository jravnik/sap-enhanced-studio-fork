﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddOVSonIDToSectionGroup
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using System.Collections.Generic;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  internal class AddOVSonIDToSectionGroup : AddOVSInfoModeler
  {
    protected override List<FlexStableAnchorType> getStableAnchorList()
    {
      return this.flexHandler.GetAllStableAnchorList(this.sectionGroupType);
    }

    protected override void init()
    {
      this.InitializeComponent();
      this.sectionGroupType = this.ExtensibleModel as SectionGroupType;
      if (this.sectionGroupType == null)
        return;
      this.entityHeader.Text = this.getText(this.sectionGroupType.SectionGroupName);
    }
  }
}
