﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.HideWocViewModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.RepositoryLayer;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class HideWocViewModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private const string TITLE_STRING = "Hide Workcenter View";
    private FlexibilityHandler flexHandler;
    private FlexBaseAnchorType stableAnchorType;
    private bool changesApplied;
    private List<string> viewSwitchIDList;
    //internal ListView wocViewListView;
    //internal Button okButton;
    //internal Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public HideWocViewModeler()
    {
      this.Title = "Hide Workcenter View";
      this.InitializeComponent();
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.viewSwitchIDList = new List<string>();
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      this.stableAnchorType = this.getComponentAnchor(this.UIComponent);
      if (this.stableAnchorType == null)
        return;
      this.fillListBox();
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      this.IsChangeApplied = false;
    }

    private void fillListBox()
    {
      foreach (ViewSwitchType viewSwitchType in this.UIComponent.CenterStructure.ViewSwitches.ViewSwitch)
      {
        if ((viewSwitchType.type == ViewSwitchTypes.Application || viewSwitchType.type == ViewSwitchTypes.InvisibleApplication) && viewSwitchType.SubViewSwitches == null)
        {
          Grid grid = new Grid();
          grid.ColumnDefinitions.Add(new ColumnDefinition()
          {
            Width = new GridLength(120.0, GridUnitType.Pixel)
          });
          grid.ColumnDefinitions.Add(new ColumnDefinition()
          {
            Width = new GridLength(550.0, GridUnitType.Pixel)
          });
          Label label1 = new Label();
          label1.Content = (object) this.getText(viewSwitchType.Text);
          label1.HorizontalAlignment = HorizontalAlignment.Left;
          label1.SetValue(Grid.ColumnProperty, (object) 0);
          grid.Children.Add((UIElement) label1);
          Label label2 = new Label();
          label2.Content = (object) this.getTargetCompOfEmbeddedComp(viewSwitchType.id);
          label2.HorizontalAlignment = HorizontalAlignment.Left;
          label2.SetValue(Grid.ColumnProperty, (object) 1);
          grid.Children.Add((UIElement) label2);
          ListViewItem listViewItem = new ListViewItem();
          listViewItem.Content = (object) grid;
          this.wocViewListView.Items.Add((object) listViewItem);
          this.viewSwitchIDList.Add(viewSwitchType.id);
        }
      }
    }

    private string getTargetCompOfEmbeddedComp(string viewSwitchID)
    {
      string str = (string) null;
      foreach (ViewSwitchType viewSwitchType in this.UIComponent.CenterStructure.ViewSwitches.ViewSwitch)
      {
        if (viewSwitchID == viewSwitchType.id)
        {
          foreach (EmbeddedComponentType embeddedComponentType in this.UIComponent.EmbeddedComponents.EmbeddedComponent)
          {
            if (embeddedComponentType.embedName == viewSwitchType.embedName)
              return embeddedComponentType.targetComponentID;
          }
        }
      }
      return str;
    }

    private FlexBaseAnchorType getReferencedAnchor(string viewSwitchID)
    {
      FloorplanInfo componentDetails = ProjectWorkspaceManager.Instance.GetComponentDetails(this.getTargetCompOfEmbeddedComp(viewSwitchID));
      return this.getComponentAnchor(ProjectWorkspaceManager.Instance.GetUXComponent(ref componentDetails, false));
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      foreach (object selectedItem in (IEnumerable) this.wocViewListView.SelectedItems)
      {
        string viewSwitchId = this.viewSwitchIDList[this.wocViewListView.Items.IndexOf(selectedItem)];
        if (!string.IsNullOrEmpty(viewSwitchId))
          this.flexHandler.RemoveWoCView(this.stableAnchorType, this.getReferencedAnchor(viewSwitchId), viewSwitchId);
      }
      this.changesApplied = true;
      this.IsChangeApplied = true;
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/hidewocviewmodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.wocViewListView = (ListView) target;
    //      break;
    //    case 2:
    //      this.okButton = (Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 3:
    //      this.cancelButton = (Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
