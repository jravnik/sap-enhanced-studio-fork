﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.WidthConverter
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public class WidthConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null)
        return (object) null;
      ListView listView = value as ListView;
      GridView view = listView.View as GridView;
      int result = 0;
      if (parameter != null && !int.TryParse(parameter.ToString(), out result))
      {
        string str = parameter.ToString();
        double num = double.Parse(str.Substring(0, str.Length - 1));
        return (object) (listView.ActualWidth * num);
      }
      double num1 = 0.0;
      for (int index = 0; index < view.Columns.Count - 1; ++index)
        num1 += view.Columns[index].ActualWidth;
      double num2 = listView.ActualWidth - num1;
      if (num2 > (double) result)
        return (object) num2;
      return (object) result;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
