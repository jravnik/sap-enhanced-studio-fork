﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddECToScreenModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controller;
using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.Oberon.Controller;
using SAP.BYD.LS.UIDesigner.Model.Oberon.WorkCenterStructure;
using SAP.BYD.LS.UIDesigner.Model.RepositoryLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AddECToScreenModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private const string TITLE_STRING = "Add Embedded Component";
    private const string TXT_ERROR_EC_NOT_SELECTED = "Please select a Embedded Component";
    private const string TXT_ERROR_EC_NO_INPORTS = "Embedded Component doesn't have any inports";
    private const string TXT_ERROR_EC_WRONG_LAYER = "Embedded Component is not from the partner layer";
    private const string TXT_ERROR_EC_NO_REF_ANCHOR = "The selected outport does not reference a PaneContainerAnchor";
    private const string GENERIC_ANA_COMP = "/SAP_BYD_TF/Analytics/AnalysisPattern/ANA_ICP_Embedded.EC.uicomponent";
    private FlexibilityHandler flexHandler;
    private bool changesApplied;
    private AddECToScreenModeler.TargetComponent targetComponent;
    private Form m_Container;
    public bool IsExtendedUIComponent;
    public List<BusinessObject> accessControlledBOList;
    private IOnNavigate m_onNavigateType;
    //internal Grid TabTitle;
    //internal System.Windows.Controls.TextBox tabTitleTextBox;
    //internal System.Windows.Controls.Label SelectECButton;
    //internal System.Windows.Controls.TextBox targetTextBox;
    //internal Grid Title;
    //internal System.Windows.Controls.TextBox titleTextBox;
    //internal System.Windows.Controls.Button DefineMappingButton;
    //internal System.Windows.Controls.CheckBox LazyLoadCheckBox;
    //internal System.Windows.Controls.TextBlock errorMessage;
    //internal System.Windows.Controls.Button AccessControlledBusinessObject;
    //internal System.Windows.Controls.Button okButton;
    //internal System.Windows.Controls.Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public AddECToScreenModeler()
    {
      this.InitializeComponent();
      this.DefineMappingButton.IsEnabled = false;
      this.LazyLoadCheckBox.IsEnabled = false;
      this.Title.Visibility = Visibility.Collapsed;
      this.TabTitle.Visibility = Visibility.Collapsed;
      this.errorMessage.Visibility = Visibility.Collapsed;
      this.m_onNavigateType = (IOnNavigate) null;
      this.accessControlledBOList = new List<BusinessObject>();
      this.AccessControlledBusinessObject.IsEnabled = false;
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.flexHandler = this.getFlexibilityHandler();
      if (this.DTAnchor != null && (this.Anchor.type == FlexAnchorEnumType.TabStripAnchor || this.Anchor.type == FlexAnchorEnumType.ViewSwitchNavigationAnchor))
        this.TabTitle.Visibility = Visibility.Visible;
      this.okButton.IsEnabled = false;
      this.DefineMappingButton.IsEnabled = false;
      this.LazyLoadCheckBox.IsEnabled = false;
      this.AccessControlledBusinessObject.IsEnabled = false;
    }

    protected override void revertChanges()
    {
      int num = this.changesApplied ? 1 : 0;
    }

    private List<FloorplanType> getSelectedFloorType()
    {
      return new List<FloorplanType>() { FloorplanType.EC };
    }

    private string getInportAnchor()
    {
      if (this.targetComponent.Floorplan == null || this.targetComponent.Floorplan.ControllerInterface == null)
        return (string) null;
      List<IInportType> inPorts = this.targetComponent.Floorplan.ControllerInterface.InPorts;
      if (inPorts == null || inPorts.Count < 1)
        return (string) null;
      if (this.m_onNavigateType == null)
        return (string) null;
      foreach (IInportType inportType in inPorts)
      {
        if (inportType.Name == this.m_onNavigateType.InPlug)
        {
          IExtensible extensible = inportType as IExtensible;
          if (extensible != null)
          {
            foreach (FlexBaseAnchor associatedAnchor in extensible.GetAssociatedAnchors())
            {
              if (associatedAnchor.FlexBaseAnchorType.type == FlexAnchorEnumType.InPortAnchor)
                return associatedAnchor.FlexBaseAnchorType.xrepPath;
            }
          }
        }
      }
      return (string) null;
    }

    private string checkDependencies()
    {
      if (this.targetComponent.Path == null)
        return "Please select a Embedded Component";
      IFloorplan floorplan = this.targetComponent.Floorplan;
      if (floorplan.ControllerInterface.InPorts == null || floorplan.ControllerInterface.InPorts.Count < 1)
      {
        this.DefineMappingButton.IsEnabled = false;
        this.LazyLoadCheckBox.IsChecked = new bool?(false);
        this.LazyLoadCheckBox.IsEnabled = false;
      }
      else
      {
        this.DefineMappingButton.IsEnabled = true;
        this.LazyLoadCheckBox.IsEnabled = true;
      }
      return (string) null;
    }

    private void targetCompButton_Click(object sender, RoutedEventArgs e)
    {
      this.m_onNavigateType = (IOnNavigate) null;
      this.errorMessage.Visibility = Visibility.Collapsed;
      this.DefineMappingButton.IsEnabled = false;
      this.LazyLoadCheckBox.IsChecked = new bool?(false);
      this.LazyLoadCheckBox.IsEnabled = false;
      this.AccessControlledBusinessObject.IsEnabled = false;
      this.titleTextBox.Text = "";
      this.targetTextBox.Text = "";
      RepositoryFilterBrowser repositoryFilterBrowser = new RepositoryFilterBrowser(this.getSelectedFloorType(), (FloorplanInfo) null);
      if (repositoryFilterBrowser.ShowDialog() != System.Windows.Forms.DialogResult.OK)
        return;
      FloorplanInfo selectedComponent = repositoryFilterBrowser.SelectedComponent;
      if (repositoryFilterBrowser.SelectedComponent == null)
        return;
      this.targetComponent.Component = ProjectWorkspaceManager.Instance.GetUXComponent(ref selectedComponent, false);
      this.targetComponent.Path = repositoryFilterBrowser.SelectedComponent.UniqueId;
      this.targetComponent.Floorplan = ModelLayer.ObjectManager.GetFloorplanModel(selectedComponent, false);
      string str = this.checkDependencies();
      if (str == null)
      {
        this.targetTextBox.Foreground = (Brush) new SolidColorBrush(Colors.Black);
        this.targetTextBox.Text = repositoryFilterBrowser.SelectedComponent.UniqueId;
        this.okButton.IsEnabled = true;
        this.DefineMappingButton.IsEnabled = true;
        this.LazyLoadCheckBox.IsEnabled = true;
        this.AccessControlledBusinessObject.IsEnabled = true;
        if (this.targetComponent.Path == null)
          return;
        this.Title.Visibility = Visibility.Visible;
      }
      else
      {
        this.targetComponent.Floorplan = (IFloorplan) null;
        this.targetComponent.Component = (UXComponent) null;
        this.targetComponent.Path = (string) null;
        this.targetTextBox.Text = str;
        this.targetTextBox.Foreground = (Brush) new SolidColorBrush(Colors.Red);
        this.okButton.IsEnabled = false;
      }
    }

    private string getOutportAnchor(string outportName)
    {
      foreach (UXOutPortType uxOutPortType in this.UIComponent.Interface.OutPorts.OutPort)
      {
        if (uxOutPortType.name == outportName)
        {
          foreach (FlexBaseAnchorType flexBaseAnchorType in uxOutPortType.StableAnchor)
          {
            if (flexBaseAnchorType.type == FlexAnchorEnumType.OutPortAnchor)
              return flexBaseAnchorType.xrepPath;
          }
        }
      }
      return (string) null;
    }

    private void printError(string errorText)
    {
      this.errorMessage.Visibility = Visibility.Visible;
      this.errorMessage.Text = errorText;
      this.okButton.IsEnabled = false;
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      string inport = (string) null;
      string outportAnchorPath = (string) null;
      Dictionary<string, string> mapping = (Dictionary<string, string>) null;
      if (this.m_onNavigateType != null)
      {
        outportAnchorPath = this.getOutportAnchor(this.m_onNavigateType.OutPlug);
        if (outportAnchorPath == null)
        {
          this.printError("The outport you've choosen, does not contain a stable anchor. Please select a different.");
          return;
        }
        inport = this.m_onNavigateType.InPlug;
        mapping = new Dictionary<string, string>();
        if (this.m_onNavigateType.PassParameters != null)
        {
          foreach (IPassParameter passParameter in this.m_onNavigateType.PassParameters)
            mapping.Add(passParameter.OutParamName, passParameter.InParamName);
        }
      }
      string text = this.titleTextBox.Text;
      string tabTitle = (string) null;
      if (this.TabTitle.Visibility == Visibility.Visible)
        tabTitle = this.tabTitleTextBox.Text;
      string inportAnchor = this.getInportAnchor();
      bool? isChecked = this.LazyLoadCheckBox.IsChecked;
      bool lazyLoad = isChecked.GetValueOrDefault() && isChecked.HasValue;
      bool HasAccessControlledBOMaintained = false;
      if (this.accessControlledBOList.Count > 0)
        HasAccessControlledBOMaintained = true;
      List<BusinessObjectType> accessControlledBOList = new List<BusinessObjectType>(this.accessControlledBOList.Count);
      if (this.accessControlledBOList.Count > 0)
      {
        foreach (BusinessObject accessControlledBo in this.accessControlledBOList)
          accessControlledBOList.Add(accessControlledBo.BusinessObjectType);
      }
      this.flexHandler.AddECtoScreen(this.Anchor, inportAnchor, this.targetComponent.Path, inport, mapping, (string) null, outportAnchorPath, text, tabTitle, lazyLoad, HasAccessControlledBOMaintained, accessControlledBOList);
      this.changesApplied = true;
      this.IsChangeApplied = true;
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
    }

    private void defineMapping_Click(object sender, RoutedEventArgs e)
    {
      this.errorMessage.Visibility = Visibility.Collapsed;
      this.okButton.IsEnabled = true;
      try
      {
        this.m_Container = new Form();
        this.m_Container.Width = 600;
        this.m_Container.Height = 600;
        UCNavigationDesigner navigationDesigner = new UCNavigationDesigner();
        navigationDesigner.Dock = DockStyle.Fill;
        navigationDesigner.SourceFloorPlan = this.DTComponent;
        IOnNavigate onNavigate = (IOnNavigate) new OberonOnNavigate((IModelObject) ModelLayer.ObjectManager.GetFloorplanModel(this.UIComponent, (FloorplanInfo) null, false, true, this.DTComponent.Version).ControllerNavigation);
        if (this.m_onNavigateType == null)
        {
          onNavigate.NavigationStyle = NavigateStyle.inscreen_dataflow;
        }
        else
        {
          onNavigate.InPlug = this.m_onNavigateType.InPlug;
          onNavigate.OutPlug = this.m_onNavigateType.OutPlug;
          onNavigate.PassParameters = this.m_onNavigateType.PassParameters;
          onNavigate.NavigationStyle = this.m_onNavigateType.NavigationStyle;
        }
        onNavigate.IsSourceComponentEC = this.DTComponent is EmbeddComponent;
        onNavigate.IsTargetComponentEC = this.targetComponent.Floorplan is EmbeddComponent;
        onNavigate.SourceComponentID = "this";
        onNavigate.targetComponentID = this.targetComponent.Floorplan.UniqueId;
        navigationDesigner.TargetFloorPlan = this.targetComponent.Floorplan;
        navigationDesigner.TargetComponentId = this.targetTextBox.Text;
        navigationDesigner.CurrentNavigation = onNavigate;
        navigationDesigner.ApplyUICustomizationForCTModeler(true);
        navigationDesigner.OkClicked += new EventHandler(this.OnNavigationDesigner_OkClicked);
        navigationDesigner.CancelClicked += new EventHandler(this.OnnavigationDesigner_CancelClicked);
        this.m_Container.Controls.Add((System.Windows.Forms.Control) navigationDesigner);
        if (this.m_Container.ShowDialog() != System.Windows.Forms.DialogResult.OK)
          return;
        this.m_onNavigateType = onNavigate;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Navigation configuration operation failed.", ex));
      }
    }

    private void OnNavigationDesigner_OkClicked(object sender, EventArgs e)
    {
      if (this.m_Container == null || this.m_Container.IsDisposed)
        return;
      this.m_Container.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.m_Container.Close();
    }

    private void OnnavigationDesigner_CancelClicked(object sender, EventArgs e)
    {
      if (this.m_Container == null || this.m_Container.IsDisposed)
        return;
      this.m_Container.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.m_Container.Close();
    }

    private void accessControlledBOSelectorButton_Click(object sender, EventArgs e)
    {
      this.IsExtendedUIComponent = true;
      UCBusinessObjectEditor businessObjectEditor = new UCBusinessObjectEditor(this.DTExtensibleModel, (object) null, false, this.IsExtendedUIComponent);
      if (businessObjectEditor.ShowDialog() != System.Windows.Forms.DialogResult.OK)
        return;
      this.accessControlledBOList = businessObjectEditor.m_NewValue;
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addectoscreenmodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.TabTitle = (Grid) target;
    //      break;
    //    case 2:
    //      this.tabTitleTextBox = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 3:
    //      this.SelectECButton = (System.Windows.Controls.Label) target;
    //      break;
    //    case 4:
    //      this.targetTextBox = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 5:
    //      ((System.Windows.Controls.Primitives.ButtonBase) target).Click += new RoutedEventHandler(this.targetCompButton_Click);
    //      break;
    //    case 6:
    //      this.Title = (Grid) target;
    //      break;
    //    case 7:
    //      this.titleTextBox = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 8:
    //      this.DefineMappingButton = (System.Windows.Controls.Button) target;
    //      this.DefineMappingButton.Click += new RoutedEventHandler(this.defineMapping_Click);
    //      break;
    //    case 9:
    //      this.LazyLoadCheckBox = (System.Windows.Controls.CheckBox) target;
    //      break;
    //    case 10:
    //      this.errorMessage = (System.Windows.Controls.TextBlock) target;
    //      break;
    //    case 11:
    //      this.AccessControlledBusinessObject = (System.Windows.Controls.Button) target;
    //      this.AccessControlledBusinessObject.Click += new RoutedEventHandler(this.accessControlledBOSelectorButton_Click);
    //      break;
    //    case 12:
    //      this.okButton = (System.Windows.Controls.Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 13:
    //      this.cancelButton = (System.Windows.Controls.Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    internal struct TargetComponent
    {
      public UXComponent Component;
      public string Path;
      public IFloorplan Floorplan;
    }
  }
}
