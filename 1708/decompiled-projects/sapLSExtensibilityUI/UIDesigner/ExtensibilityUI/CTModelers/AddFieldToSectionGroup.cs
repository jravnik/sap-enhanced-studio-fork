﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddFieldToSectionGroup
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility.CoreAPI.Extensibility;
using System.Collections.Generic;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  internal class AddFieldToSectionGroup : AddExtensionFieldModeler
  {
    protected override List<FlexStableAnchorType> getStableAnchorList()
    {
      return this.flexHandler.GetAllStableAnchorList(this.sectionGroupType);
    }

    protected override void init()
    {
      this.InitializeComponent();
      this.sectionGroupType = this.ExtensibleModel as SectionGroupType;
      if (this.sectionGroupType == null)
        return;
      this.entityHeader.Text = this.getText(this.sectionGroupType.SectionGroupName);
      this.processor = (AbstractExtensionProcessor) new SectionGroupExtensionProcessor(this.flexHandler, this.sectionGroupType, false);
    }

    protected override bool isAlreadyAdded(UIExtensionField item)
    {
      item.isSectionGroupExtension = true;
      bool flag = false;
      if (this.sectionGroupType.SectionGroupItem != null)
      {
        foreach (SectionGroupItemType sectionGroupItemType in this.sectionGroupType.SectionGroupItem)
        {
          if (sectionGroupItemType.extendedBy == item.customField.Namespace + item.customField.AppearanceName)
            return true;
        }
      }
      return flag;
    }
  }
}
