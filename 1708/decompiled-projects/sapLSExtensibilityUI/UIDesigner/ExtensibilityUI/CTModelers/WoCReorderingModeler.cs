﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.WoCReorderingModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Model.Core;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class WoCReorderingModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private List<WoCInfo> workCenterInfoCTs = new List<WoCInfo>();
    private Dictionary<string, string> ReorderedWoCDic = new Dictionary<string, string>();
    private Dictionary<string, List<string>> RefComponentsChangeIDs = new Dictionary<string, List<string>>();
    private List<string> changeIds = new List<string>();
    private FlexibilityHandler flexHandler;
    private FlexBaseAnchorType stableAnchor;
    private bool changesApplied;
    private Point startPoint;
    private Point mousePos;
    private string changeId;
    //internal TextBlock AvailableWoc;
    //internal TextBlock CurrentWoc;
    //internal ListBox availableWoC;
    //internal Button rightButton;
    //internal Button leftButton;
    //internal ListBox currentWoC;
    //internal Button Top;
    //internal Button moveUpBtn;
    //internal Button moveDownBtn;
    //internal Button Bottom;
    //internal Button okButton;
    //internal Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return false;
      }
    }

    public WoCReorderingModeler()
    {
      this.InitializeComponent();
      this.Title = "Work Center Reordering";
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      foreach (string changeId in this.changeIds)
        this.flexHandler.RevertChangeTransaction(changeId);
      this.IsChangeApplied = false;
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      if (this.ExtensibleModel == null)
        return;
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      FlexBaseAnchorType centerListAnchor = this.flexHandler.GetWorkCenterListAnchor(this.ExtensibleModel as UXComponent);
      if (this.Anchor.xrepPath == centerListAnchor.xrepPath)
        this.stableAnchor = centerListAnchor;
      if (this.stableAnchor == null)
        return;
      WorkCenterReferencesType centerReferences = (this.ExtensibleModel as UXComponent).WorkCenterReferences;
      this.LoadExistingChangeTransaction(this.UIComponent, this.AnchorPath, ref this.workCenterInfoCTs);
      this.LoadCurrentWorkCenterList(centerReferences);
      this.LoadAvailableWorkCenterList();
      this.ApplyChangeTransaction();
    }

    private void ApplyChangeTransaction()
    {
      foreach (WoCInfo workCenterInfoCt in this.workCenterInfoCTs)
      {
        if (!string.IsNullOrEmpty(workCenterInfoCt.removeComponentId))
        {
          if (!this.availableWoC.Items.Contains((object) workCenterInfoCt.removeComponentId))
          {
            this.availableWoC.Items.Add((object) workCenterInfoCt.removeComponentId);
          }
          else
          {
            this.availableWoC.Items.Remove((object) workCenterInfoCt.removeComponentId);
            this.availableWoC.Items.Add((object) workCenterInfoCt.removeComponentId);
          }
          if (this.currentWoC.Items.Contains((object) workCenterInfoCt.removeComponentId))
            this.currentWoC.Items.Remove((object) workCenterInfoCt.removeComponentId);
        }
        if (workCenterInfoCt.addedWorkCenter != null)
        {
          AddedWoCInfo addedWorkCenter = workCenterInfoCt.addedWorkCenter;
          string refComponetId = addedWorkCenter.refComponetId;
          string targetComponentId = addedWorkCenter.targetComponentId;
          FlexWoCPositionType positionType = addedWorkCenter.positionType;
          string absolutePostion = addedWorkCenter.absolutePostion;
          if (this.availableWoC.Items.Contains((object) targetComponentId))
            this.availableWoC.Items.Remove((object) targetComponentId);
          if (this.currentWoC.Items.Contains((object) targetComponentId))
            this.currentWoC.Items.Remove((object) targetComponentId);
          if (!string.IsNullOrEmpty(refComponetId))
          {
            int num = this.currentWoC.Items.IndexOf((object) refComponetId);
            if (num == -1)
            {
              if (!string.IsNullOrEmpty(absolutePostion) && int.Parse(absolutePostion) - 1 >= 0)
                this.currentWoC.Items.Insert(int.Parse(absolutePostion) - 1, (object) targetComponentId);
              else
                this.currentWoC.Items.Add((object) targetComponentId);
            }
            else
              this.currentWoC.Items.Insert(num + 1, (object) targetComponentId);
          }
          else if (positionType == FlexWoCPositionType.Top)
            this.currentWoC.Items.Insert(0, (object) targetComponentId);
          else if (positionType == FlexWoCPositionType.Bottom)
            this.currentWoC.Items.Add((object) targetComponentId);
        }
      }
      this.currentWoC.Items.Refresh();
      this.currentWoC.SelectedIndex = 0;
      this.availableWoC.Items.Refresh();
      this.availableWoC.SelectedIndex = 0;
    }

    private void LoadExistingChangeTransaction(UXComponent uxComponent, string anchorPath, ref List<WoCInfo> workCenterCTs)
    {
      List<ModeledChangeTransactionInfo> publishedCTList;
      List<ModeledChangeTransactionInfo> savedCTList;
      XRepositoryProxy.Instance.GetChangeTransactionForAnchor(anchorPath, out publishedCTList, out savedCTList);
      if (publishedCTList != null && publishedCTList.Count > 0)
      {
        foreach (ModeledChangeTransactionInfo changeTransactionInfo in publishedCTList)
          this.CheckChangeTransaction(changeTransactionInfo.Content, ref workCenterCTs);
      }
      if (savedCTList != null && savedCTList.Count > 0)
      {
        foreach (ModeledChangeTransactionInfo changeTransactionInfo in savedCTList)
          this.CheckChangeTransaction(changeTransactionInfo.Content, ref workCenterCTs);
      }
      List<ModeledChangeTransactionInfo> changeTransactions = ChangeTransactionManager.Instance.GetUnsavedChangeTransactions(this.UIComponentPath, uxComponent, anchorPath);
      if (changeTransactions == null || changeTransactions.Count <= 0)
        return;
      foreach (ModeledChangeTransactionInfo changeTransactionInfo in changeTransactions)
      {
        this.CheckChangeTransaction(changeTransactionInfo.Content, ref workCenterCTs);
        this.CheckUnSaveChangeTransaction(changeTransactionInfo.Content);
      }
    }

    private void CheckUnSaveChangeTransaction(string content)
    {
      using (MemoryStream memoryStream = new MemoryStream(new UTF8Encoding().GetBytes(content)))
      {
        object deserializedObject;
        if (!XmlHelper.GetInstance().DeserializeObject(typeof (ChangeTransaction), (Stream) memoryStream, out deserializedObject))
          return;
        ChangeTransaction changeTransaction = deserializedObject as ChangeTransaction;
        if (changeTransaction == null || changeTransaction.UsedAnchor == null)
          return;
        if (changeTransaction.UsedAnchor.RemoveWoCOrder != null && changeTransaction.UsedAnchor.RemoveWoCOrder.Count > 0)
          this.ReorderedWoCDic.Add(changeTransaction.UsedAnchor.RemoveWoCOrder[0].targetComponentID, changeTransaction.id);
        if (changeTransaction.UsedAnchor.AddWoCOrder == null || changeTransaction.UsedAnchor.AddWoCOrder.Count <= 0)
          return;
        this.ReorderedWoCDic.Add(changeTransaction.UsedAnchor.AddWoCOrder[0].targetComponentID, changeTransaction.id);
        string refComponentId = changeTransaction.UsedAnchor.AddWoCOrder[0].refComponentID;
        if (!(refComponentId != string.Empty))
          return;
        if (this.RefComponentsChangeIDs.ContainsKey(refComponentId))
          this.RefComponentsChangeIDs[refComponentId].Add(changeTransaction.id);
        else
          this.RefComponentsChangeIDs.Add(refComponentId, new List<string>()
          {
            changeTransaction.id
          });
      }
    }

    private void CheckChangeTransaction(string content, ref List<WoCInfo> workCenterCTs)
    {
      using (MemoryStream memoryStream = new MemoryStream(new UTF8Encoding().GetBytes(content)))
      {
        object deserializedObject;
        if (!XmlHelper.GetInstance().DeserializeObject(typeof (ChangeTransaction), (Stream) memoryStream, out deserializedObject))
          return;
        ChangeTransaction changeTransaction = deserializedObject as ChangeTransaction;
        if (changeTransaction == null || changeTransaction.UsedAnchor == null)
          return;
        if (changeTransaction.UsedAnchor.RemoveWoCOrder != null && changeTransaction.UsedAnchor.RemoveWoCOrder.Count > 0)
          workCenterCTs.Add(new WoCInfo()
          {
            removeComponentId = changeTransaction.UsedAnchor.RemoveWoCOrder[0].targetComponentID
          });
        if (changeTransaction.UsedAnchor.AddWoCOrder == null || changeTransaction.UsedAnchor.AddWoCOrder.Count <= 0)
          return;
        string targetComponentId = changeTransaction.UsedAnchor.AddWoCOrder[0].targetComponentID;
        string refComponentId = changeTransaction.UsedAnchor.AddWoCOrder[0].refComponentID;
        string absolutePosition = changeTransaction.UsedAnchor.AddWoCOrder[0].absolutePosition;
        FlexWoCPositionType position = changeTransaction.UsedAnchor.AddWoCOrder[0].position;
        workCenterCTs.Add(new WoCInfo()
        {
          addedWorkCenter = new AddedWoCInfo()
          {
            targetComponentId = targetComponentId,
            refComponetId = refComponentId,
            positionType = position,
            absolutePostion = absolutePosition
          }
        });
      }
    }

    private void LoadAvailableWorkCenterList()
    {
      foreach (RepositoryElement repositoryElement in XRepositoryProxy.Instance.QueryFolderDeep("/", new List<RepositoryAttributeFilter>()
      {
        new RepositoryAttributeFilter()
        {
          Name = "~ENTITY_TYPE",
          Sign = "I",
          Option = "EQ",
          Low = "uiwoc"
        }
      }))
      {
        if (!this.currentWoC.Items.Contains((object) ("/" + repositoryElement.AbsolutePath)))
          this.availableWoC.Items.Add((object) ("/" + repositoryElement.AbsolutePath));
      }
      this.availableWoC.SelectedIndex = 0;
    }

    private void LoadCurrentWorkCenterList(WorkCenterReferencesType workCenterReference)
    {
      if (workCenterReference != null)
      {
        foreach (WorkCenterReferenceType centerReferenceType in workCenterReference.WorkCenterReference)
          this.currentWoC.Items.Add((object) centerReferenceType.embedName);
      }
      this.currentWoC.SelectedIndex = 0;
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
      this.Terminate();
    }

    private void CurrentWoC_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ListBox listBox = sender as ListBox;
      if (listBox == null || listBox.Items == null)
        return;
      listBox.Items.Refresh();
    }

    private void AvailableWoC_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ListBox listBox = sender as ListBox;
      if (listBox == null || listBox.Items == null)
        return;
      listBox.Items.Refresh();
    }

    private void rightButton_Click(object sender, RoutedEventArgs e)
    {
      string index1 = (string) null;
      if (this.availableWoC.SelectedItem != null)
        index1 = this.availableWoC.SelectedItem.ToString();
      string index2 = this.currentWoC.SelectedItem == null ? string.Empty : this.currentWoC.SelectedItem.ToString();
      int selectedIndex1 = this.currentWoC.SelectedIndex;
      int selectedIndex2 = this.availableWoC.SelectedIndex;
      if (string.IsNullOrEmpty(index1))
        return;
      if (this.ReorderedWoCDic.ContainsKey(index1))
      {
        string oldRefComponentID;
        this.flexHandler.RevertChangeTransaction(this.ReorderedWoCDic[index1], out oldRefComponentID);
        string str = this.ReorderedWoCDic[index1];
        if (!string.IsNullOrEmpty(oldRefComponentID) && this.RefComponentsChangeIDs.ContainsKey(oldRefComponentID) && this.RefComponentsChangeIDs[oldRefComponentID].Contains(str))
        {
          this.RefComponentsChangeIDs[oldRefComponentID].Remove(str);
          if (this.RefComponentsChangeIDs[oldRefComponentID].Count == 0)
            this.RefComponentsChangeIDs.Remove(oldRefComponentID);
        }
        this.changeIds.Remove(str);
        this.ReorderedWoCDic.Remove(index1);
      }
      this.changeId = this.flexHandler.AddWoC(this.stableAnchor, index1, index2, (selectedIndex1 + 1).ToString(), FlexWoCPositionType.Behind, true, this.UIComponentPath);
      if (index2 != string.Empty)
      {
        if (this.RefComponentsChangeIDs.ContainsKey(index2))
          this.RefComponentsChangeIDs[index2].Add(this.changeId);
        else
          this.RefComponentsChangeIDs.Add(index2, new List<string>()
          {
            this.changeId
          });
      }
      this.ReorderedWoCDic.Add(index1, this.changeId);
      this.currentWoC.Items.Insert(selectedIndex1 + 1, (object) index1);
      this.availableWoC.Items.Remove((object) index1);
      this.currentWoC.Items.MoveCurrentTo(this.currentWoC.SelectedItem);
      this.currentWoC.ScrollIntoView(this.currentWoC.SelectedItem);
      this.availableWoC.Items.MoveCurrentTo(this.availableWoC.SelectedItem);
      this.availableWoC.ScrollIntoView(this.availableWoC.SelectedItem);
      if (this.availableWoC.Items != null && this.availableWoC.Items.Count > 0)
      {
        if (selectedIndex2 == 0)
          this.availableWoC.SelectedIndex = 0;
        else
          this.availableWoC.SelectedIndex = selectedIndex2 - 1;
      }
      this.changeIds.Add(this.changeId);
      this.okButton.IsEnabled = true;
    }

    private void leftButton_Click(object sender, RoutedEventArgs e)
    {
      string index1 = (string) null;
      if (this.currentWoC.SelectedItem != null)
        index1 = this.currentWoC.SelectedItem.ToString();
      if (string.IsNullOrEmpty(index1))
        return;
      string index2 = this.currentWoC.SelectedIndex - 1 < 0 ? string.Empty : this.currentWoC.Items[this.currentWoC.SelectedIndex - 1].ToString();
      string oldRefComponentID;
      if (this.RefComponentsChangeIDs.ContainsKey(index1))
      {
        foreach (string changeTransactionID in this.RefComponentsChangeIDs[index1].ToList<string>())
        {
          this.flexHandler.ProtocolHelper.ModifyWoCOrderingChangeTransaction(changeTransactionID, index2, out oldRefComponentID);
          if (!string.IsNullOrEmpty(oldRefComponentID) && this.RefComponentsChangeIDs.ContainsKey(oldRefComponentID) && this.RefComponentsChangeIDs[oldRefComponentID].Contains(changeTransactionID))
            this.RefComponentsChangeIDs[oldRefComponentID].Remove(changeTransactionID);
          if (this.RefComponentsChangeIDs.ContainsKey(index2))
            this.RefComponentsChangeIDs[index2].Add(changeTransactionID);
          else if (index2 != string.Empty)
            this.RefComponentsChangeIDs.Add(index2, new List<string>()
            {
              changeTransactionID
            });
        }
      }
      if (this.ReorderedWoCDic.ContainsKey(index1))
      {
        this.flexHandler.RevertChangeTransaction(this.ReorderedWoCDic[index1], out oldRefComponentID);
        string str = this.ReorderedWoCDic[index1];
        if (!string.IsNullOrEmpty(oldRefComponentID) && this.RefComponentsChangeIDs.ContainsKey(oldRefComponentID) && this.RefComponentsChangeIDs[oldRefComponentID].Contains(str))
        {
          this.RefComponentsChangeIDs[oldRefComponentID].Remove(str);
          if (this.RefComponentsChangeIDs[oldRefComponentID].Count == 0)
            this.RefComponentsChangeIDs.Remove(oldRefComponentID);
        }
        this.changeIds.Remove(str);
        this.ReorderedWoCDic.Remove(index1);
      }
      this.changeId = this.flexHandler.RemoveWoC(this.stableAnchor, index1, this.UIComponentPath);
      this.ReorderedWoCDic.Add(index1, this.changeId);
      this.changeIds.Add(this.changeId);
      int selectedIndex = this.currentWoC.SelectedIndex;
      this.currentWoC.Items.Remove((object) index1);
      this.availableWoC.Items.Add((object) index1);
      if (this.currentWoC.Items != null && this.currentWoC.Items.Count > 0)
      {
        if (selectedIndex == this.currentWoC.Items.Count)
          --selectedIndex;
        this.currentWoC.SelectedIndex = selectedIndex;
      }
      this.availableWoC.SelectedItem = (object) index1;
      this.availableWoC.ScrollIntoView((object) index1);
      this.currentWoC.Items.MoveCurrentTo(this.currentWoC.SelectedItem);
      this.currentWoC.ScrollIntoView(this.currentWoC.SelectedItem);
      this.availableWoC.Items.MoveCurrentTo(this.availableWoC.SelectedItem);
      this.availableWoC.ScrollIntoView(this.availableWoC.SelectedItem);
      this.okButton.IsEnabled = true;
    }

    private void Top_Click(object sender, RoutedEventArgs e)
    {
      string index1 = (string) null;
      if (this.currentWoC.SelectedItem != null)
        index1 = this.currentWoC.SelectedItem.ToString();
      if (string.IsNullOrEmpty(index1))
        return;
      string index2 = this.currentWoC.Items[this.currentWoC.SelectedIndex - 1].ToString();
      string oldRefComponentID;
      if (this.RefComponentsChangeIDs.ContainsKey(index1))
      {
        foreach (string changeTransactionID in this.RefComponentsChangeIDs[index1].ToList<string>())
        {
          this.flexHandler.ProtocolHelper.ModifyWoCOrderingChangeTransaction(changeTransactionID, index2, out oldRefComponentID);
          if (!string.IsNullOrEmpty(oldRefComponentID) && this.RefComponentsChangeIDs.ContainsKey(oldRefComponentID) && this.RefComponentsChangeIDs[oldRefComponentID].Contains(changeTransactionID))
            this.RefComponentsChangeIDs[oldRefComponentID].Remove(changeTransactionID);
          if (this.RefComponentsChangeIDs.ContainsKey(index2))
            this.RefComponentsChangeIDs[index2].Add(changeTransactionID);
          else
            this.RefComponentsChangeIDs.Add(index2, new List<string>()
            {
              changeTransactionID
            });
        }
      }
      if (this.ReorderedWoCDic.ContainsKey(index1))
      {
        this.flexHandler.RevertChangeTransaction(this.ReorderedWoCDic[index1], out oldRefComponentID);
        string str = this.ReorderedWoCDic[index1];
        if (!string.IsNullOrEmpty(oldRefComponentID) && this.RefComponentsChangeIDs.ContainsKey(oldRefComponentID) && this.RefComponentsChangeIDs[oldRefComponentID].Contains(str))
        {
          this.RefComponentsChangeIDs[oldRefComponentID].Remove(str);
          if (this.RefComponentsChangeIDs[oldRefComponentID].Count == 0)
            this.RefComponentsChangeIDs.Remove(oldRefComponentID);
        }
        this.changeIds.Remove(str);
        this.ReorderedWoCDic.Remove(index1);
      }
      this.changeId = this.flexHandler.AddWoC(this.stableAnchor, index1, "", "0", FlexWoCPositionType.Top, true, this.UIComponentPath);
      this.ReorderedWoCDic.Add(index1, this.changeId);
      this.currentWoC.Items.Remove((object) index1);
      this.currentWoC.Items.Insert(0, (object) index1);
      this.currentWoC.SelectedItem = (object) index1;
      this.changeIds.Add(this.changeId);
      this.currentWoC.Items.MoveCurrentTo(this.currentWoC.SelectedItem);
      this.currentWoC.ScrollIntoView(this.currentWoC.SelectedItem);
      this.okButton.IsEnabled = true;
    }

    private void Bottom_Click(object sender, RoutedEventArgs e)
    {
      string index1 = (string) null;
      if (this.currentWoC.SelectedItem != null)
        index1 = this.currentWoC.SelectedItem.ToString();
      if (string.IsNullOrEmpty(index1))
        return;
      string index2 = this.currentWoC.SelectedIndex - 1 < 0 ? string.Empty : this.currentWoC.Items[this.currentWoC.SelectedIndex - 1].ToString();
      string oldRefComponentID;
      if (this.RefComponentsChangeIDs.ContainsKey(index1))
      {
        foreach (string changeTransactionID in this.RefComponentsChangeIDs[index1].ToList<string>())
        {
          this.flexHandler.ProtocolHelper.ModifyWoCOrderingChangeTransaction(changeTransactionID, index2, out oldRefComponentID);
          if (!string.IsNullOrEmpty(oldRefComponentID) && this.RefComponentsChangeIDs.ContainsKey(oldRefComponentID) && this.RefComponentsChangeIDs[oldRefComponentID].Contains(changeTransactionID))
            this.RefComponentsChangeIDs[oldRefComponentID].Remove(changeTransactionID);
          if (this.RefComponentsChangeIDs.ContainsKey(index2))
            this.RefComponentsChangeIDs[index2].Add(changeTransactionID);
          else if (index2 != string.Empty)
            this.RefComponentsChangeIDs.Add(index2, new List<string>()
            {
              changeTransactionID
            });
        }
      }
      if (this.ReorderedWoCDic.ContainsKey(index1))
      {
        this.flexHandler.RevertChangeTransaction(this.ReorderedWoCDic[index1], out oldRefComponentID);
        string str = this.ReorderedWoCDic[index1];
        if (!string.IsNullOrEmpty(oldRefComponentID) && this.RefComponentsChangeIDs.ContainsKey(oldRefComponentID) && this.RefComponentsChangeIDs[oldRefComponentID].Contains(str))
        {
          this.RefComponentsChangeIDs[oldRefComponentID].Remove(str);
          if (this.RefComponentsChangeIDs[oldRefComponentID].Count == 0)
            this.RefComponentsChangeIDs.Remove(oldRefComponentID);
        }
        this.changeIds.Remove(str);
        this.ReorderedWoCDic.Remove(index1);
      }
      this.changeId = this.flexHandler.AddWoC(this.stableAnchor, index1, "", (this.currentWoC.Items.Count - 1).ToString(), FlexWoCPositionType.Bottom, true, this.UIComponentPath);
      this.ReorderedWoCDic.Add(index1, this.changeId);
      this.currentWoC.Items.Remove((object) index1);
      this.currentWoC.Items.Add((object) index1);
      this.currentWoC.SelectedItem = (object) index1;
      this.changeIds.Add(this.changeId);
      this.currentWoC.Items.MoveCurrentTo(this.currentWoC.SelectedItem);
      this.currentWoC.ScrollIntoView(this.currentWoC.SelectedItem);
      this.okButton.IsEnabled = true;
    }

    private void moveUpBtn_Click(object sender, RoutedEventArgs e)
    {
      string index1 = (string) null;
      if (this.currentWoC.SelectedItem != null)
        index1 = this.currentWoC.SelectedItem.ToString();
      if (string.IsNullOrEmpty(index1))
        return;
      int selectedIndex = this.currentWoC.SelectedIndex;
      string oldRefComponentID;
      if (this.RefComponentsChangeIDs.ContainsKey(index1))
      {
        string index2 = this.currentWoC.Items[selectedIndex - 1].ToString();
        foreach (string changeTransactionID in this.RefComponentsChangeIDs[index1].ToList<string>())
        {
          this.flexHandler.ProtocolHelper.ModifyWoCOrderingChangeTransaction(changeTransactionID, index2, out oldRefComponentID);
          if (!string.IsNullOrEmpty(oldRefComponentID) && this.RefComponentsChangeIDs.ContainsKey(oldRefComponentID) && this.RefComponentsChangeIDs[oldRefComponentID].Contains(changeTransactionID))
          {
            this.RefComponentsChangeIDs[oldRefComponentID].Remove(changeTransactionID);
            if (this.RefComponentsChangeIDs[oldRefComponentID].Count == 0)
              this.RefComponentsChangeIDs.Remove(oldRefComponentID);
          }
          if (this.RefComponentsChangeIDs.ContainsKey(index2))
            this.RefComponentsChangeIDs[index2].Add(changeTransactionID);
          else
            this.RefComponentsChangeIDs.Add(index2, new List<string>()
            {
              changeTransactionID
            });
        }
      }
      if (this.ReorderedWoCDic.ContainsKey(index1))
      {
        this.flexHandler.RevertChangeTransaction(this.ReorderedWoCDic[index1], out oldRefComponentID);
        string str = this.ReorderedWoCDic[index1];
        if (!string.IsNullOrEmpty(oldRefComponentID) && this.RefComponentsChangeIDs.ContainsKey(oldRefComponentID) && this.RefComponentsChangeIDs[oldRefComponentID].Contains(str))
        {
          this.RefComponentsChangeIDs[oldRefComponentID].Remove(str);
          if (this.RefComponentsChangeIDs[oldRefComponentID].Count == 0)
            this.RefComponentsChangeIDs.Remove(oldRefComponentID);
        }
        this.changeIds.Remove(str);
        this.ReorderedWoCDic.Remove(index1);
      }
      if (selectedIndex <= 0)
        return;
      this.currentWoC.Items.Remove(this.currentWoC.SelectedItem);
      this.currentWoC.Items.Insert(selectedIndex - 1, (object) index1);
      this.currentWoC.SelectedIndex = selectedIndex - 2;
      object selectedItem = this.currentWoC.SelectedItem;
      string index3 = selectedItem != null ? selectedItem.ToString() : string.Empty;
      if (index3 == string.Empty)
      {
        this.changeId = this.flexHandler.AddWoC(this.stableAnchor, index1, index3, (selectedIndex - 1).ToString(), FlexWoCPositionType.Top, true, this.UIComponentPath);
      }
      else
      {
        this.changeId = this.flexHandler.AddWoC(this.stableAnchor, index1, index3, (selectedIndex - 1).ToString(), FlexWoCPositionType.Behind, true, this.UIComponentPath);
        if (this.RefComponentsChangeIDs.ContainsKey(index3))
          this.RefComponentsChangeIDs[index3].Add(this.changeId);
        else
          this.RefComponentsChangeIDs.Add(index3, new List<string>()
          {
            this.changeId
          });
      }
      this.ReorderedWoCDic.Add(index1, this.changeId);
      this.changeIds.Add(this.changeId);
      this.currentWoC.SelectedIndex = selectedIndex - 1;
      this.currentWoC.Items.MoveCurrentTo(this.currentWoC.SelectedItem);
      this.currentWoC.ScrollIntoView(this.currentWoC.SelectedItem);
      this.okButton.IsEnabled = true;
    }

    private void moveDownBtn_Click(object sender, RoutedEventArgs e)
    {
      string index1 = (string) null;
      if (this.currentWoC.SelectedItem != null)
        index1 = this.currentWoC.SelectedItem.ToString();
      if (string.IsNullOrEmpty(index1))
        return;
      int selectedIndex = this.currentWoC.SelectedIndex;
      string oldRefComponentID;
      if (this.RefComponentsChangeIDs.ContainsKey(index1))
      {
        string empty = string.Empty;
        if (selectedIndex > 0)
          empty = this.currentWoC.Items[selectedIndex - 1].ToString();
        foreach (string changeTransactionID in this.RefComponentsChangeIDs[index1].ToList<string>())
        {
          this.flexHandler.ProtocolHelper.ModifyWoCOrderingChangeTransaction(changeTransactionID, empty, out oldRefComponentID);
          if (!string.IsNullOrEmpty(oldRefComponentID) && this.RefComponentsChangeIDs.ContainsKey(oldRefComponentID) && this.RefComponentsChangeIDs[oldRefComponentID].Contains(changeTransactionID))
          {
            this.RefComponentsChangeIDs[oldRefComponentID].Remove(changeTransactionID);
            if (this.RefComponentsChangeIDs[oldRefComponentID].Count == 0)
              this.RefComponentsChangeIDs.Remove(oldRefComponentID);
          }
          if (this.RefComponentsChangeIDs.ContainsKey(empty))
            this.RefComponentsChangeIDs[empty].Add(changeTransactionID);
          else if (empty != string.Empty)
            this.RefComponentsChangeIDs.Add(empty, new List<string>()
            {
              changeTransactionID
            });
        }
      }
      if (this.ReorderedWoCDic.ContainsKey(index1))
      {
        this.flexHandler.RevertChangeTransaction(this.ReorderedWoCDic[index1], out oldRefComponentID);
        string str = this.ReorderedWoCDic[index1];
        if (!string.IsNullOrEmpty(oldRefComponentID) && this.RefComponentsChangeIDs.ContainsKey(oldRefComponentID) && this.RefComponentsChangeIDs[oldRefComponentID].Contains(str))
        {
          this.RefComponentsChangeIDs[oldRefComponentID].Remove(str);
          if (this.RefComponentsChangeIDs[oldRefComponentID].Count == 0)
            this.RefComponentsChangeIDs.Remove(oldRefComponentID);
        }
        this.changeIds.Remove(str);
        this.ReorderedWoCDic.Remove(index1);
      }
      if (selectedIndex >= this.currentWoC.Items.Count - 1)
        return;
      this.currentWoC.Items.Remove(this.currentWoC.SelectedItem);
      this.currentWoC.Items.Insert(selectedIndex + 1, (object) index1);
      this.currentWoC.SelectedIndex = selectedIndex;
      string index2 = this.currentWoC.SelectedItem != null ? this.currentWoC.SelectedItem.ToString() : string.Empty;
      if (index2 == string.Empty)
      {
        this.changeId = this.flexHandler.AddWoC(this.stableAnchor, index1, index2, (selectedIndex + 1).ToString(), FlexWoCPositionType.Bottom, true, this.UIComponentPath);
      }
      else
      {
        this.changeId = this.flexHandler.AddWoC(this.stableAnchor, index1, index2, (selectedIndex + 1).ToString(), FlexWoCPositionType.Behind, true, this.UIComponentPath);
        if (this.RefComponentsChangeIDs.ContainsKey(index2))
          this.RefComponentsChangeIDs[index2].Add(this.changeId);
        else
          this.RefComponentsChangeIDs.Add(index2, new List<string>()
          {
            this.changeId
          });
      }
      this.ReorderedWoCDic.Add(index1.ToString(), this.changeId);
      this.changeIds.Add(this.changeId);
      this.currentWoC.SelectedIndex = selectedIndex + 1;
      this.currentWoC.Items.MoveCurrentTo(this.currentWoC.SelectedItem);
      this.currentWoC.ScrollIntoView(this.currentWoC.SelectedItem);
      this.okButton.IsEnabled = true;
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      this.IsChangeApplied = true;
      this.changesApplied = true;
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void Terminate()
    {
      if (this.changeIds != null)
      {
        this.changeIds.Clear();
        this.changeIds = (List<string>) null;
      }
      if (this.workCenterInfoCTs != null)
      {
        this.workCenterInfoCTs.Clear();
        this.workCenterInfoCTs = (List<WoCInfo>) null;
      }
      if (this.ReorderedWoCDic != null)
      {
        this.ReorderedWoCDic.Clear();
        this.ReorderedWoCDic = (Dictionary<string, string>) null;
      }
      if (this.RefComponentsChangeIDs != null)
      {
        this.RefComponentsChangeIDs.Clear();
        this.RefComponentsChangeIDs = (Dictionary<string, List<string>>) null;
      }
      if (this.flexHandler != null)
        this.flexHandler = (FlexibilityHandler) null;
      if (this.stableAnchor == null)
        return;
      this.stableAnchor = (FlexBaseAnchorType) null;
    }

    private void BaseChangeTransactionModeler_SizeChanged(object sender, SizeChangedEventArgs e)
    {
      this.availableWoC.Items.MoveCurrentTo(this.availableWoC.SelectedItem);
      this.availableWoC.ScrollIntoView(this.availableWoC.SelectedItem);
      this.currentWoC.Items.MoveCurrentTo(this.currentWoC.SelectedItem);
      this.currentWoC.ScrollIntoView(this.currentWoC.SelectedItem);
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/wocreorderingmodeler.xaml", UriKind.Relative));
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.AvailableWoc = (TextBlock) target;
    //      break;
    //    case 2:
    //      this.CurrentWoc = (TextBlock) target;
    //      break;
    //    case 3:
    //      this.availableWoC = (ListBox) target;
    //      this.availableWoC.SelectionChanged += new SelectionChangedEventHandler(this.AvailableWoC_SelectionChanged);
    //      break;
    //    case 4:
    //      this.rightButton = (Button) target;
    //      this.rightButton.Click += new RoutedEventHandler(this.rightButton_Click);
    //      break;
    //    case 5:
    //      this.leftButton = (Button) target;
    //      this.leftButton.Click += new RoutedEventHandler(this.leftButton_Click);
    //      break;
    //    case 6:
    //      this.currentWoC = (ListBox) target;
    //      this.currentWoC.SelectionChanged += new SelectionChangedEventHandler(this.CurrentWoC_SelectionChanged);
    //      break;
    //    case 7:
    //      this.Top = (Button) target;
    //      this.Top.Click += new RoutedEventHandler(this.Top_Click);
    //      break;
    //    case 8:
    //      this.moveUpBtn = (Button) target;
    //      this.moveUpBtn.Click += new RoutedEventHandler(this.moveUpBtn_Click);
    //      break;
    //    case 9:
    //      this.moveDownBtn = (Button) target;
    //      this.moveDownBtn.Click += new RoutedEventHandler(this.moveDownBtn_Click);
    //      break;
    //    case 10:
    //      this.Bottom = (Button) target;
    //      this.Bottom.Click += new RoutedEventHandler(this.Bottom_Click);
    //      break;
    //    case 11:
    //      this.okButton = (Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 12:
    //      this.cancelButton = (Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
