﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.ExistingMashupsInfo
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Mashup.Tools.SideCar;
using System.Collections.Generic;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public class ExistingMashupsInfo
  {
    public string AppearanceKey { get; set; }

    public bool FullColumnSpan { get; set; }

    public Dictionary<string, ExtBindingOption> DynamicBindings { get; set; }

    public string VisibilityBindingExp { get; set; }

    public string TargetInplug { get; set; }
  }
}
