﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AdjustablePropertiesEntity
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using System.Windows;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public class AdjustablePropertiesEntity
  {
    public string AssociatedControlId { get; set; }

    public ModelEntity AssociatedExtensibleModel { get; set; }

    public string ControlHeader { get; set; }

    public string FieldBackendBindingPath { get; set; }

    public PropertyValueHolder Enabled { get; set; }

    public PropertyValueHolder Visible { get; set; }

    public PropertyValueHolder Mandatory { get; set; }

    public PropertyValueHolder ReadOnly { get; set; }

    public bool HasEnabledProperty { get; set; }

    public bool HasVisibleProperty { get; set; }

    public bool HasMandatoryProperty { get; set; }

    public bool HasReadOnlyProperty { get; set; }

    public Brush VisibleForegroundBrush
    {
      get
      {
        if (this.Visible.Value != PropertyValues.None)
          return (Brush) Brushes.Red;
        return (Brush) Brushes.Black;
      }
    }

    public Brush EnabledForegroundBrush
    {
      get
      {
        if (this.Enabled.Value != PropertyValues.None)
          return (Brush) Brushes.Red;
        return (Brush) Brushes.Black;
      }
    }

    public Brush MandatoryForegroundBrush
    {
      get
      {
        if (this.Mandatory.Value != PropertyValues.None)
          return (Brush) Brushes.Red;
        return (Brush) Brushes.Black;
      }
    }

    public Brush ReadOnlyForegroundBrush
    {
      get
      {
        if (this.ReadOnly.Value != PropertyValues.None)
          return (Brush) Brushes.Red;
        return (Brush) Brushes.Black;
      }
    }

    public Visibility ShowEnabledProperty
    {
      get
      {
        return !this.HasEnabledProperty ? Visibility.Collapsed : Visibility.Visible;
      }
    }

    public Visibility ShowVisibleProperty
    {
      get
      {
        return !this.HasVisibleProperty ? Visibility.Collapsed : Visibility.Visible;
      }
    }

    public Visibility ShowMandatoryProperty
    {
      get
      {
        return !this.HasMandatoryProperty ? Visibility.Collapsed : Visibility.Visible;
      }
    }

    public Visibility ShowReadOnlyProperty
    {
      get
      {
        return !this.HasReadOnlyProperty ? Visibility.Collapsed : Visibility.Visible;
      }
    }

    public Visibility VisibleBindingButton_Visiblity
    {
      get
      {
        return this.Visible.Value == PropertyValues.Bound ? Visibility.Visible : Visibility.Collapsed;
      }
    }

    public Visibility EnabledBindingButton_Visiblity
    {
      get
      {
        return this.Enabled.Value == PropertyValues.Bound ? Visibility.Visible : Visibility.Collapsed;
      }
    }

    public Visibility MandatoryBindingButton_Visiblity
    {
      get
      {
        return this.Mandatory.Value == PropertyValues.Bound ? Visibility.Visible : Visibility.Collapsed;
      }
    }

    public Visibility ReadOnlyBindingButton_Visiblity
    {
      get
      {
        return this.ReadOnly.Value == PropertyValues.Bound ? Visibility.Visible : Visibility.Collapsed;
      }
    }

    public bool IsVisibleDirty { get; set; }

    public bool IsEnabledDirty { get; set; }

    public bool IsMandatoryDirty { get; set; }

    public bool IsReadOnlyDirty { get; set; }

    public bool IsDirty
    {
      get
      {
        if (!this.IsVisibleDirty && !this.IsEnabledDirty && !this.IsMandatoryDirty)
          return this.IsReadOnlyDirty;
        return true;
      }
    }

    public bool IsCompondFieldChildControl { get; set; }

    public AdjustablePropertiesEntity()
    {
      this.Enabled = new PropertyValueHolder()
      {
        Value = PropertyValues.None
      };
      this.Visible = new PropertyValueHolder()
      {
        Value = PropertyValues.None
      };
      this.Mandatory = new PropertyValueHolder()
      {
        Value = PropertyValues.None
      };
      this.ReadOnly = new PropertyValueHolder()
      {
        Value = PropertyValues.None
      };
      this.HasEnabledProperty = true;
      this.HasVisibleProperty = true;
      this.HasMandatoryProperty = true;
      this.HasReadOnlyProperty = true;
    }
  }
}
