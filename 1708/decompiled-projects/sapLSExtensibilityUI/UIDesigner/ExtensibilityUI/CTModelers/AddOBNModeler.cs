﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddOBNModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Flexibility.CoreAPI;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AddOBNModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private FlexibilityHandler flexHandler;
    protected SectionGroupType sectionGroupType;
    private bool changesApplied;
    private OBNConfigHelper OBNHelper;
    //internal TextBlock errorMessage;
    //internal StackPanel UsageAreaToolbar;
    //internal Label lblButton;
    //internal ComboBox comboButtons;
    //internal Label lblTitleTB;
    //internal TextBox txtBoxTitleTB;
    //internal Label lblTooltipTB;
    //internal TextBox txtBoxTooltipTB;
    //internal StackPanel UsageAreaSectionGroup;
    //internal Label lblctrlPosition;
    //internal ComboBox comboPos;
    //internal Label lblctrlType;
    //internal ComboBox comboType;
    //internal Label lblwithLabel;
    //internal CheckBox withLabelCB;
    //internal Label lblLabel;
    //internal TextBox txtBoxLabel;
    //internal Label lblTitleSG;
    //internal TextBox txtBoxTitleSG;
    //internal Label lblTooltipSG;
    //internal TextBox txtBoxTooltipSG;
    //internal Button btnConfigureOBN;
    //internal ScrollViewer OBNArea;
    //internal StackPanel ContentArea;
    //internal Button okButton;
    //internal Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public AddOBNModeler()
    {
      this.InitializeComponent();
    }

    private void fillButtonsList(List<ButtonType> buttonTypes)
    {
      if (buttonTypes == null || buttonTypes.Count == 0)
      {
        this.lblButton.Visibility = Visibility.Collapsed;
        this.comboButtons.Visibility = Visibility.Collapsed;
      }
      else
      {
        this.lblButton.Visibility = Visibility.Visible;
        this.comboButtons.Visibility = Visibility.Visible;
        this.comboButtons.Items.Clear();
        ItemCollection items = this.comboButtons.Items;
        ComboBoxItem comboBoxItem1 = new ComboBoxItem();
        comboBoxItem1.Content = (object) "<First Item>";
        comboBoxItem1.Tag = (object) null;
        ComboBoxItem comboBoxItem2 = comboBoxItem1;
        items.Add((object) comboBoxItem2);
        foreach (ButtonType buttonType in buttonTypes)
        {
          ComboBoxItem comboBoxItem3 = new ComboBoxItem();
          comboBoxItem3.Content = (object) this.getText(buttonType.Text);
          comboBoxItem3.Tag = (object) buttonType.id;
          this.comboButtons.Items.Add((object) comboBoxItem3);
        }
        this.comboButtons.SelectedIndex = buttonTypes.Count - 1;
      }
    }

    private void fillButtonsList(List<ButtonGroupType> buttonGroupTypes)
    {
      List<ButtonType> buttonTypes = (List<ButtonType>) null;
      foreach (ButtonGroupType buttonGroupType in buttonGroupTypes)
      {
        if (buttonGroupType.name.Equals("ApplicationSpecificButtons"))
        {
          buttonTypes = buttonGroupType.Button;
          break;
        }
      }
      this.fillButtonsList(buttonTypes);
    }

    private void InitializeBasedOnToolbarAnchor()
    {
      if (!(this.ExtensibleModel is ToolbarType))
        return;
      this.fillButtonsList((this.ExtensibleModel as ToolbarType).ButtonGroup);
      this.UsageAreaSectionGroup.Visibility = Visibility.Collapsed;
      this.UsageAreaToolbar.Visibility = Visibility.Visible;
      this.Title = "Add OBN Button";
    }

    private void InitializeBasedOnFloorplanAnchor()
    {
      if (this.DTComponent != null && this.DTComponent.UXView != null && (this.DTComponent.UXView.ContextualNavigationRegion != null && this.DTComponent.UXView.ContextualNavigationRegion.Functionbar != null))
        this.InitializeBasedOnToolbarAnchor();
      else
        this.printError("The creation of 'Add OBN' change transaction is not possible for the choosen component");
    }

    private void InitializeBasedOnSectionGroupAnchor()
    {
      this.fillComboBoxes();
      this.UsageAreaSectionGroup.Visibility = Visibility.Visible;
      this.UsageAreaToolbar.Visibility = Visibility.Collapsed;
      this.Title = "Add OBN Button/Link to Form";
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.okButton.IsEnabled = false;
      if (this.Anchor != null)
      {
        if (this.Anchor.type == FlexAnchorEnumType.SectionGroupAnchor)
          this.InitializeBasedOnSectionGroupAnchor();
        else if (this.Anchor.type == FlexAnchorEnumType.FloorPlanAnchor)
          this.InitializeBasedOnFloorplanAnchor();
        else if (this.Anchor.type == FlexAnchorEnumType.ToolbarAnchor)
        {
          this.InitializeBasedOnToolbarAnchor();
        }
        else
        {
          this.printError("The creation of 'Add OBN' change transaction is not possible for the choosen anchor");
          return;
        }
        this.flexHandler = this.getFlexibilityHandler();
        this.OBNHelper = new OBNConfigHelper(this.UIComponent, this.DTComponent);
      }
      else
        this.printError("The creation of 'Add OBN' change transaction is not possible without an anchor");
    }

    protected override void revertChanges()
    {
      int num = this.changesApplied ? 1 : 0;
    }

    private void fillComboBoxes()
    {
      ItemCollection items1 = this.comboPos.Items;
      ComboBoxItem comboBoxItem1 = new ComboBoxItem();
      comboBoxItem1.Content = (object) FlexPositionEnumType.Top;
      ComboBoxItem comboBoxItem2 = comboBoxItem1;
      items1.Add((object) comboBoxItem2);
      ItemCollection items2 = this.comboPos.Items;
      ComboBoxItem comboBoxItem3 = new ComboBoxItem();
      comboBoxItem3.Content = (object) FlexPositionEnumType.Bottom;
      ComboBoxItem comboBoxItem4 = comboBoxItem3;
      items2.Add((object) comboBoxItem4);
      this.comboPos.SelectedIndex = 0;
      ItemCollection items3 = this.comboType.Items;
      ComboBoxItem comboBoxItem5 = new ComboBoxItem();
      comboBoxItem5.Content = (object) "Button";
      ComboBoxItem comboBoxItem6 = comboBoxItem5;
      items3.Add((object) comboBoxItem6);
      ItemCollection items4 = this.comboType.Items;
      ComboBoxItem comboBoxItem7 = new ComboBoxItem();
      comboBoxItem7.Content = (object) "Link";
      ComboBoxItem comboBoxItem8 = comboBoxItem7;
      items4.Add((object) comboBoxItem8);
      this.comboType.SelectedIndex = 0;
    }

    private void checkForApplyEnablement()
    {
      this.okButton.IsEnabled = false;
      if (string.IsNullOrEmpty(this.getTitle()) || this.OBNHelper.m_navigation == null)
        return;
      this.okButton.IsEnabled = true;
    }

    private void printErrorNonBlocking(string errorText)
    {
      this.errorMessage.Visibility = Visibility.Visible;
      this.errorMessage.Text = errorText;
    }

    private void printError(string errorText)
    {
      this.printErrorNonBlocking(errorText);
      this.okButton.IsEnabled = false;
    }

    private FlexPositionEnumType getPosition()
    {
      FlexPositionEnumType positionEnumType = FlexPositionEnumType.Top;
      ComboBoxItem selectedItem = this.comboPos.SelectedItem as ComboBoxItem;
      if (selectedItem != null && selectedItem.Content.ToString() == FlexPositionEnumType.Bottom.ToString())
        positionEnumType = FlexPositionEnumType.Bottom;
      return positionEnumType;
    }

    private bool isTypeButton()
    {
      ComboBoxItem selectedItem = this.comboType.SelectedItem as ComboBoxItem;
      bool flag = true;
      if (selectedItem != null && selectedItem.Content != null && selectedItem.Content.ToString() == "Link")
        flag = false;
      return flag;
    }

    private string getTitle()
    {
      if (this.Anchor.type == FlexAnchorEnumType.SectionGroupAnchor)
        return this.txtBoxTitleSG.Text;
      return this.txtBoxTitleTB.Text;
    }

    private string getTooltip()
    {
      if (this.Anchor.type == FlexAnchorEnumType.SectionGroupAnchor)
        return this.txtBoxTooltipSG.Text;
      return this.txtBoxTooltipTB.Text;
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      string title = this.getTitle();
      string tooltip = this.getTooltip();
      if (this.OBNHelper.m_navigation == null)
      {
        this.printErrorNonBlocking("Please specify the OBN informations");
      }
      else
      {
        string navBons = this.OBNHelper.NavBONS;
        string navBo = this.OBNHelper.NavBO;
        string navBoNode = this.OBNHelper.NavBONode;
        string navOperation = this.OBNHelper.NavOperation;
        string contextAttribute = this.OBNHelper.NavContextAttribute;
        string ctx_const = this.OBNHelper.isConstantNavContext ? "X" : (string) null;
        string portTypePackage = this.OBNHelper.PortTypePackage;
        string portTypeReference = this.OBNHelper.PortTypeReference;
        List<OBNSimpleBindingType> simpleParameters = this.OBNHelper.SimpleParameters;
        List<OBNListBindingType> listParameters = this.OBNHelper.ListParameters;
        if (this.OBNHelper.m_navigation.OutPlug == null)
          this.printErrorNonBlocking("Please specify the PortType Informations");
        else if ((simpleParameters == null || simpleParameters.Count == 0) && (listParameters == null || listParameters.Count == 0))
        {
          this.printErrorNonBlocking("Please specify Parametermapping");
        }
        else
        {
          foreach (OBNListBindingType obnListBindingType in listParameters)
          {
            if (string.IsNullOrEmpty(obnListBindingType.ptpListParamBind))
            {
              this.printErrorNonBlocking("Please specify the list binding");
              return;
            }
          }
          if (string.IsNullOrEmpty(navBons))
            this.printErrorNonBlocking("Please specify the OBN BO-Namespace");
          else if (string.IsNullOrEmpty(navBo))
            this.printErrorNonBlocking("Please specify the OBN Business Object");
          else if (string.IsNullOrEmpty(navBoNode))
            this.printErrorNonBlocking("Please specify the OBN BO-Node");
          else if (string.IsNullOrEmpty(navOperation))
            this.printErrorNonBlocking("Please specify the OBN BO-Operation");
          else if (string.IsNullOrEmpty(portTypePackage))
            this.printErrorNonBlocking("Please specify the PortType Package");
          else if (string.IsNullOrEmpty(portTypeReference))
          {
            this.printErrorNonBlocking("Please specify the PortType Reference");
          }
          else
          {
            if (this.Anchor.type == FlexAnchorEnumType.SectionGroupAnchor)
            {
              string text = this.txtBoxLabel.Text;
              FlexPositionEnumType position = this.getPosition();
              bool isButton = this.isTypeButton();
              this.flexHandler.AddOBNonSection(this.Anchor, title, isButton, position, text, tooltip, navBons, navBo, navBoNode, navOperation, contextAttribute, ctx_const, portTypePackage, portTypeReference, simpleParameters, listParameters);
            }
            else
            {
              string refId = (string) null;
              if (this.comboButtons.SelectedItem != null)
              {
                ComboBoxItem selectedItem = (ComboBoxItem) this.comboButtons.SelectedItem;
                if (selectedItem != null)
                  refId = (string) selectedItem.Tag;
              }
              this.flexHandler.AddOBNInfoOnButton(this.Anchor, title, refId, tooltip, navBons, navBo, navBoNode, navOperation, contextAttribute, ctx_const, portTypePackage, portTypeReference, simpleParameters, listParameters);
            }
            this.changesApplied = true;
            this.IsChangeApplied = true;
            this.Close();
          }
        }
      }
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
    }

    private void btnConfigureButton_Click(object sender, RoutedEventArgs e)
    {
      this.errorMessage.Visibility = Visibility.Collapsed;
    }

    private void btnConfigureOBN_Click(object sender, RoutedEventArgs e)
    {
      this.errorMessage.Visibility = Visibility.Collapsed;
      this.OBNHelper.configureOBN();
      if (this.OBNHelper.m_outportConfig == null)
        this.ContentArea.Children.Clear();
      else
        this.displayOBNParameters();
      this.checkForApplyEnablement();
    }

    private void displayOBNParameters()
    {
      this.errorMessage.Visibility = Visibility.Collapsed;
      if (this.OBNHelper.m_outportConfig == null)
        return;
      this.ContentArea.Children.Clear();
      this.ContentArea.Children.Add((UIElement) this.OBNHelper.m_outportConfig);
      this.OBNHelper.configureParams();
      this.checkForApplyEnablement();
    }

    private void btnConfigureParams_Click(object sender, RoutedEventArgs e)
    {
      this.displayOBNParameters();
    }

    private void comboButtons_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      this.checkForApplyEnablement();
    }

    private void txtBoxTitle_TextChanged(object sender, TextChangedEventArgs e)
    {
      this.checkForApplyEnablement();
    }

    private void withLabelCB_Click(object sender, RoutedEventArgs e)
    {
      if (sender == null)
        return;
      bool? isChecked = (sender as CheckBox).IsChecked;
      if ((!isChecked.GetValueOrDefault() ? 0 : (isChecked.HasValue ? 1 : 0)) != 0)
      {
        this.lblLabel.Visibility = Visibility.Visible;
        this.txtBoxLabel.Visibility = Visibility.Visible;
      }
      else
      {
        this.lblLabel.Visibility = Visibility.Collapsed;
        this.txtBoxLabel.Text = (string) null;
        this.txtBoxLabel.Visibility = Visibility.Collapsed;
      }
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addobnmodeler.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.errorMessage = (TextBlock) target;
    //      break;
    //    case 2:
    //      this.UsageAreaToolbar = (StackPanel) target;
    //      break;
    //    case 3:
    //      this.lblButton = (Label) target;
    //      break;
    //    case 4:
    //      this.comboButtons = (ComboBox) target;
    //      this.comboButtons.SelectionChanged += new SelectionChangedEventHandler(this.comboButtons_SelectionChanged);
    //      break;
    //    case 5:
    //      this.lblTitleTB = (Label) target;
    //      break;
    //    case 6:
    //      this.txtBoxTitleTB = (TextBox) target;
    //      this.txtBoxTitleTB.TextChanged += new TextChangedEventHandler(this.txtBoxTitle_TextChanged);
    //      break;
    //    case 7:
    //      this.lblTooltipTB = (Label) target;
    //      break;
    //    case 8:
    //      this.txtBoxTooltipTB = (TextBox) target;
    //      this.txtBoxTooltipTB.TextChanged += new TextChangedEventHandler(this.txtBoxTitle_TextChanged);
    //      break;
    //    case 9:
    //      this.UsageAreaSectionGroup = (StackPanel) target;
    //      break;
    //    case 10:
    //      this.lblctrlPosition = (Label) target;
    //      break;
    //    case 11:
    //      this.comboPos = (ComboBox) target;
    //      break;
    //    case 12:
    //      this.lblctrlType = (Label) target;
    //      break;
    //    case 13:
    //      this.comboType = (ComboBox) target;
    //      break;
    //    case 14:
    //      this.lblwithLabel = (Label) target;
    //      break;
    //    case 15:
    //      this.withLabelCB = (CheckBox) target;
    //      this.withLabelCB.Click += new RoutedEventHandler(this.withLabelCB_Click);
    //      break;
    //    case 16:
    //      this.lblLabel = (Label) target;
    //      break;
    //    case 17:
    //      this.txtBoxLabel = (TextBox) target;
    //      this.txtBoxLabel.TextChanged += new TextChangedEventHandler(this.txtBoxTitle_TextChanged);
    //      break;
    //    case 18:
    //      this.lblTitleSG = (Label) target;
    //      break;
    //    case 19:
    //      this.txtBoxTitleSG = (TextBox) target;
    //      this.txtBoxTitleSG.TextChanged += new TextChangedEventHandler(this.txtBoxTitle_TextChanged);
    //      break;
    //    case 20:
    //      this.lblTooltipSG = (Label) target;
    //      break;
    //    case 21:
    //      this.txtBoxTooltipSG = (TextBox) target;
    //      this.txtBoxTooltipSG.TextChanged += new TextChangedEventHandler(this.txtBoxTitle_TextChanged);
    //      break;
    //    case 22:
    //      this.btnConfigureOBN = (Button) target;
    //      this.btnConfigureOBN.Click += new RoutedEventHandler(this.btnConfigureOBN_Click);
    //      break;
    //    case 23:
    //      this.OBNArea = (ScrollViewer) target;
    //      break;
    //    case 24:
    //      this.ContentArea = (StackPanel) target;
    //      break;
    //    case 25:
    //      this.okButton = (Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 26:
    //      this.cancelButton = (Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
