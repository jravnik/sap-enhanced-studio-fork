﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddCustomActionModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UIDesigner.BOConnector;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class AddCustomActionModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private VisualBOBrowserWindow browserWindow;
    private BOAction boAction;
    private string esrNamespace;
    private string boName;
    private string actionPath;
    //internal TextBox customActionTextBox;
    //internal Button actionPicker;
    //internal ComboBox ActionTypeCombo;
    //internal TextBox buttonTextTextBox;
    //internal TextBox buttonToolTipTextBox;
    //internal ComboBox InstanceBindCombo;
    //internal CheckBox SaveBeforeCheckBox;
    //internal CheckBox SaveAfterCheckBox;
    //internal CheckBox ContinueOnRejectCheckBox;
    //internal CheckBox SpecialRefActionCheckBox;
    //internal CheckBox ContinueOnConcurrencyBox;
    //internal CheckBox SynchronousCheckBox;
    //internal CheckBox RelevantForWorkProtectCheckBox;
    //internal CheckBox performKUTChecksCheckBox;
    //internal Button okButton;
    //internal Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public AddCustomActionModeler()
    {
      this.InitializeComponent();
    }

    protected override void revertChanges()
    {
    }

    protected override void InitializeModeler()
    {
      try
      {
        base.InitializeModeler();
        this.LoadActionType();
        this.LoadInstanceBindingCombo();
        this.okButton.IsEnabled = false;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("InitializeModeler of AddCustomActionModeler failed to execute", ex));
      }
    }

    private void LoadInstanceBindingCombo()
    {
      IImplementationController implementationController = Model.Utilities.GetImplementationController((IModelObject) this.DTComponent);
      if (implementationController != null)
      {
        this.InstanceBindCombo.Items.Clear();
        IDataStructure dataModel = (IDataStructure) implementationController.DataModel;
        this.InstanceBindCombo.Items.Add((object) ("/" + dataModel.FullPath));
        foreach (IBaseDataElementType dataList in dataModel.GetDataLists())
          this.InstanceBindCombo.Items.Add((object) dataList.FullPath);
        foreach (IBaseDataElementType dataStructure in dataModel.GetDataStructures())
          this.InstanceBindCombo.Items.Add((object) dataStructure.FullPath);
      }
      if (!(this.InstanceBindCombo.Items.Count > 0 & this.ActionTypeCombo.Text == UXBoActionType.@static.ToString()))
        return;
      this.InstanceBindCombo.SelectedIndex = 0;
    }

    private void LoadActionType()
    {
      this.ActionTypeCombo.ItemsSource = (IEnumerable) null;
      this.ActionTypeCombo.Items.Clear();
      this.ActionTypeCombo.ItemsSource = (IEnumerable) Enum.GetValues(typeof (UXBoActionType));
      this.ActionTypeCombo.SelectedIndex = 0;
    }

    private void SetActionType(BOActionCardinality cardinality)
    {
      if (cardinality == BOActionCardinality.actionSingle)
        this.ActionTypeCombo.SelectedItem = (object) UXBoActionType.single;
      else if (cardinality == BOActionCardinality.actionStatic)
        this.ActionTypeCombo.SelectedItem = (object) UXBoActionType.@static;
      else if (cardinality == BOActionCardinality.actionStaticWithoutCreateWithRef)
        this.ActionTypeCombo.SelectedItem = (object) UXBoActionType.staticWithoutCreateWithRef;
      else if (cardinality == BOActionCardinality.actionExactTwo)
        this.ActionTypeCombo.SelectedItem = (object) UXBoActionType.exacttwo;
      else
        this.ActionTypeCombo.SelectedItem = (object) UXBoActionType.single;
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        string str1 = this.buttonTextTextBox.Text.Trim();
        string str2 = str1.Replace(" ", string.Empty);
        string str3 = "On" + str2;
        this.customActionTextBox.Text.Trim();
        AddCustomActionType customActionDetails = new AddCustomActionType();
        customActionDetails.AddButton = new FlexAddButtonType();
        ButtonType buttonType = new ButtonType();
        buttonType.id = Model.Utilities.GetId();
        buttonType.name = "CustomAction_" + str2;
        buttonType.Text = new DependentPropertyType();
        buttonType.Text.id = Model.Utilities.GetId();
        buttonType.Text.fallbackValue = str1;
        buttonType.Text.textPoolId = Model.Utilities.GetId();
        buttonType.onClick = str3;
        customActionDetails.TextPoolEntry = new List<TextPoolEntryType>();
        if (!string.IsNullOrEmpty(str1))
        {
          TextPoolEntryType textPoolEntryType = new TextPoolEntryType();
          textPoolEntryType.id = Model.Utilities.GetId();
          textPoolEntryType.text = buttonType.Text.fallbackValue;
          textPoolEntryType.textUuid = buttonType.Text.textPoolId;
          textPoolEntryType.textCategory = TextCategoryType.XBUT;
          customActionDetails.TextPoolEntry.Add(textPoolEntryType);
        }
        string str4 = this.buttonToolTipTextBox.Text.ToString();
        if (!string.IsNullOrEmpty(str4))
        {
          buttonType.ToolTip = new DependentPropertyType();
          buttonType.ToolTip.id = Model.Utilities.GetId();
          buttonType.ToolTip.fallbackValue = str4;
          buttonType.ToolTip.textPoolId = Model.Utilities.GetId();
          TextPoolEntryType textPoolEntryType = new TextPoolEntryType();
          textPoolEntryType.id = Model.Utilities.GetId();
          textPoolEntryType.text = buttonType.ToolTip.fallbackValue;
          textPoolEntryType.textUuid = buttonType.ToolTip.textPoolId;
          textPoolEntryType.textCategory = TextCategoryType.XTOL;
          customActionDetails.TextPoolEntry.Add(textPoolEntryType);
        }
        if (this.Anchor.type == FlexAnchorEnumType.ToolbarAnchor)
        {
          ButtonGroupType buttonGroupType = new ButtonGroupType();
          buttonGroupType.id = Model.Utilities.GetId();
          buttonGroupType.name = buttonGroupType.id;
          buttonGroupType.Button = new List<ButtonType>();
          buttonGroupType.Button.Add(buttonType);
          customActionDetails.AddButton.Item = (NamedModelEntity) buttonGroupType;
        }
        else
          customActionDetails.AddButton.Item = (NamedModelEntity) buttonType;
        UXEventHandlerType eventHandlerType = new UXEventHandlerType();
        eventHandlerType.id = Model.Utilities.GetId();
        eventHandlerType.name = str3;
        eventHandlerType.Items = new List<UXEventHandlerOperationType>();
        UXEventHandlerOperationTypeBoAction operationTypeBoAction1 = new UXEventHandlerOperationTypeBoAction();
        operationTypeBoAction1.id = Model.Utilities.GetId();
        operationTypeBoAction1.name = "Operation_" + str2;
        operationTypeBoAction1.type = (UXBoActionType) Enum.Parse(typeof (UXBoActionType), this.ActionTypeCombo.SelectedItem.ToString());
        operationTypeBoAction1.instanceBinding = this.InstanceBindCombo.SelectedItem.ToString();
        operationTypeBoAction1.esrNamespace = this.esrNamespace;
        operationTypeBoAction1.proxyName = this.boName;
        operationTypeBoAction1.actionPath = this.actionPath;
        if (!string.IsNullOrEmpty(this.boAction.CustomActionNamespace))
          operationTypeBoAction1.actionNamespace = this.boAction.CustomActionNamespace;
        UXEventHandlerOperationTypeBoAction operationTypeBoAction2 = operationTypeBoAction1;
        bool? isChecked1 = this.ContinueOnConcurrencyBox.IsChecked;
        int num1 = !isChecked1.GetValueOrDefault() ? 0 : (isChecked1.HasValue ? 1 : 0);
        operationTypeBoAction2.continueOnConcurrency = num1 != 0;
        UXEventHandlerOperationTypeBoAction operationTypeBoAction3 = operationTypeBoAction1;
        bool? isChecked2 = this.ContinueOnRejectCheckBox.IsChecked;
        int num2 = !isChecked2.GetValueOrDefault() ? 0 : (isChecked2.HasValue ? 1 : 0);
        operationTypeBoAction3.continueOnReject = num2 != 0;
        UXEventHandlerOperationTypeBoAction operationTypeBoAction4 = operationTypeBoAction1;
        bool? isChecked3 = this.performKUTChecksCheckBox.IsChecked;
        int num3 = !isChecked3.GetValueOrDefault() ? 0 : (isChecked3.HasValue ? 1 : 0);
        operationTypeBoAction4.performClientSideKUTChecks = num3 != 0;
        UXEventHandlerOperationTypeBoAction operationTypeBoAction5 = operationTypeBoAction1;
        bool? isChecked4 = this.RelevantForWorkProtectCheckBox.IsChecked;
        int num4 = !isChecked4.GetValueOrDefault() ? 0 : (isChecked4.HasValue ? 1 : 0);
        operationTypeBoAction5.relevantForWorkProtect = num4 != 0;
        UXEventHandlerOperationTypeBoAction operationTypeBoAction6 = operationTypeBoAction1;
        bool? isChecked5 = this.SpecialRefActionCheckBox.IsChecked;
        int num5 = !isChecked5.GetValueOrDefault() ? 0 : (isChecked5.HasValue ? 1 : 0);
        operationTypeBoAction6.specialReferenceAction = num5 != 0;
        UXEventHandlerOperationTypeBoAction operationTypeBoAction7 = operationTypeBoAction1;
        bool? isChecked6 = this.SynchronousCheckBox.IsChecked;
        int num6 = !isChecked6.GetValueOrDefault() ? 0 : (isChecked6.HasValue ? 1 : 0);
        operationTypeBoAction7.synchronous = num6 != 0;
        UXEventHandlerOperationTypeBoAction operationTypeBoAction8 = operationTypeBoAction1;
        bool? isChecked7 = this.SaveAfterCheckBox.IsChecked;
        int num7 = !isChecked7.GetValueOrDefault() ? 0 : (isChecked7.HasValue ? 1 : 0);
        operationTypeBoAction8.requiresSave = num7 != 0;
        UXEventHandlerOperationTypeBoAction operationTypeBoAction9 = operationTypeBoAction1;
        bool? isChecked8 = this.SaveBeforeCheckBox.IsChecked;
        int num8 = !isChecked8.GetValueOrDefault() ? 0 : (isChecked8.HasValue ? 1 : 0);
        operationTypeBoAction9.requiresSaveBefore = num8 != 0;
        eventHandlerType.Items.Add((UXEventHandlerOperationType) operationTypeBoAction1);
        customActionDetails.AddEventHandler = new FlexAddEventHandlerType();
        customActionDetails.AddEventHandler.EventHandler = eventHandlerType;
        this.getFlexibilityHandler().AddCustomAction(this.Anchor, customActionDetails, this.UIComponentPath);
        this.IsChangeApplied = true;
        this.Close();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("okButton Click event handler of AddCustomActionModeler failed to execute", ex));
      }
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void actionPicker_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        ObservableCollection<BOAction> observableCollection = new ObservableCollection<BOAction>();
        IImplementationController implementationController = Model.Utilities.GetImplementationController((IModelObject) this.DTComponent);
        if (implementationController == null || implementationController.DataModel == null)
          return;
        this.browserWindow = new VisualBOBrowserWindow();
        VisualBOBrowser boBrowser = this.browserWindow.BOBrowser;
        boBrowser.ShowModeSelector = false;
        boBrowser.ShowNSBOSelector = false;
        boBrowser.ShowToolbar = false;
        boBrowser.AllowMultiSelect = false;
        boBrowser.ControlMode = BOBrowserControlMode.actions;
        boBrowser.TreeNodeSelected += new VisualBOBrowser.EventHandler(this.OnBOBrowser_TreeNodeSelected);
        boBrowser.ReselectElement(implementationController.DataModel.RootNameSpace, implementationController.DataModel.RootBOName, string.Empty, true);
        this.browserWindow.DisableOK();
        bool? nullable = this.browserWindow.ShowDialog();
        if ((!nullable.GetValueOrDefault() ? 0 : (nullable.HasValue ? 1 : 0)) == 0 || boBrowser.SelectedNode == null || !(boBrowser.SelectedNode.Tag is BOAction))
          return;
        this.boAction = boBrowser.SelectedNode.Tag as BOAction;
        this.customActionTextBox.Text = this.boAction.Name;
        this.buttonTextTextBox.Text = this.boAction.UIText;
        this.buttonTextTextBox.ToolTip = (object) this.boAction.UITextTooltip;
        this.esrNamespace = boBrowser.ESINamespace;
        this.boName = boBrowser.ESIBO;
        this.actionPath = boBrowser.ESIBindingPath;
        this.okButton.IsEnabled = true;
        if (this.boAction.ActionCardinality == BOActionCardinality.actionMultiple)
        {
          this.ActionTypeCombo.ItemsSource = (IEnumerable) null;
          this.ActionTypeCombo.Items.Clear();
          this.ActionTypeCombo.Items.Add((object) UXBoActionType.single);
          this.ActionTypeCombo.Items.Add((object) UXBoActionType.multiple);
          this.ActionTypeCombo.SelectedIndex = 1;
          this.ActionTypeCombo.IsEnabled = true;
        }
        else
        {
          this.LoadActionType();
          this.ActionTypeCombo.IsEnabled = false;
          this.SetActionType(this.boAction.ActionCardinality);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("actionPicker Click event handler of AddCustomActionModeler failed to execute", ex));
      }
    }

    private void OnBOBrowser_TreeNodeSelected(object sender, EventArgs e)
    {
      try
      {
        if (this.browserWindow.BOBrowser.SelectedElement is BOAction)
          this.browserWindow.EnableOk();
        else
          this.browserWindow.DisableOK();
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("onBOBrowser TreeNodeSelected event handler of AddCustomActionModeler failed to execute", ex));
      }
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addcustomactionmodeler.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.customActionTextBox = (TextBox) target;
    //      break;
    //    case 2:
    //      this.actionPicker = (Button) target;
    //      this.actionPicker.Click += new RoutedEventHandler(this.actionPicker_Click);
    //      break;
    //    case 3:
    //      this.ActionTypeCombo = (ComboBox) target;
    //      break;
    //    case 4:
    //      this.buttonTextTextBox = (TextBox) target;
    //      break;
    //    case 5:
    //      this.buttonToolTipTextBox = (TextBox) target;
    //      break;
    //    case 6:
    //      this.InstanceBindCombo = (ComboBox) target;
    //      break;
    //    case 7:
    //      this.SaveBeforeCheckBox = (CheckBox) target;
    //      break;
    //    case 8:
    //      this.SaveAfterCheckBox = (CheckBox) target;
    //      break;
    //    case 9:
    //      this.ContinueOnRejectCheckBox = (CheckBox) target;
    //      break;
    //    case 10:
    //      this.SpecialRefActionCheckBox = (CheckBox) target;
    //      break;
    //    case 11:
    //      this.ContinueOnConcurrencyBox = (CheckBox) target;
    //      break;
    //    case 12:
    //      this.SynchronousCheckBox = (CheckBox) target;
    //      break;
    //    case 13:
    //      this.RelevantForWorkProtectCheckBox = (CheckBox) target;
    //      break;
    //    case 14:
    //      this.performKUTChecksCheckBox = (CheckBox) target;
    //      break;
    //    case 15:
    //      this.okButton = (Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 16:
    //      this.cancelButton = (Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}
  }
}
