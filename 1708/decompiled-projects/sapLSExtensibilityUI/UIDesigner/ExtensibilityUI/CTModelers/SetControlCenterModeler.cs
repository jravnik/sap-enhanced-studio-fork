﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.SetControlCenterModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.RepositoryLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public partial class SetControlCenterModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private const string TXT_ERROR_TC_NOT_SELECTED = "Please select a Workcenter";
    private const string TXT_ERROR_NO_CC = "This Workcenter cannot be used as Control Center";
    private FlexibilityHandler flexHandler;
    private FlexBaseAnchorType stableAnchorType;
    private bool changesApplied;
    private SetControlCenterModeler.TargetComponent targetComponent;
    //internal System.Windows.Controls.Label componentLabel;
    //internal System.Windows.Controls.TextBox targetTextBox;
    //internal System.Windows.Controls.Button okButton;
    //internal System.Windows.Controls.Button cancelButton;
    //private bool _contentLoaded;

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public SetControlCenterModeler()
    {
      this.InitializeComponent();
      this.okButton.IsEnabled = false;
    }

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      this.stableAnchorType = this.Anchor;
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      this.IsChangeApplied = false;
    }

    private string checkDependencies(string wocPath)
    {
      if (this.targetComponent.Component == null)
        return "Please select a Workcenter";
      FloorplanInfo floorplanInfo = new FloorplanInfo();
      floorplanInfo.UniqueId = wocPath;
      UXComponent uxComponent = ProjectWorkspaceManager.Instance.GetUXComponent(ref floorplanInfo, false);
      if (uxComponent.Tags != null)
      {
        foreach (TagType tagType in uxComponent.Tags.Tag)
        {
          if (tagType.attribute.Equals("SPSM") && tagType.value.Equals("CONTROL_CENTER"))
            return (string) null;
        }
      }
      if (wocPath.EndsWith(".uicc"))
        return (string) null;
      return "This Workcenter cannot be used as Control Center";
    }

    private void targetCompButton_Click(object sender, RoutedEventArgs e)
    {
      RepositoryFilterBrowser repositoryFilterBrowser = new RepositoryFilterBrowser(new List<FloorplanType>()
      {
        FloorplanType.WCF
      }, (FloorplanInfo) null);
      if (repositoryFilterBrowser.ShowDialog() != System.Windows.Forms.DialogResult.OK)
        return;
      FloorplanInfo selectedComponent = repositoryFilterBrowser.SelectedComponent;
      this.targetComponent.Component = ProjectWorkspaceManager.Instance.GetUXComponent(ref selectedComponent, false);
      ModelLayer.ObjectManager.GetFloorplanModel(selectedComponent, false);
      string str = this.checkDependencies(selectedComponent.UniqueId);
      if (str == null)
      {
        this.targetTextBox.Text = repositoryFilterBrowser.SelectedComponent.UniqueId;
        this.targetTextBox.Foreground = (Brush) new SolidColorBrush(Colors.Black);
        this.targetComponent.Path = repositoryFilterBrowser.SelectedComponent.UniqueId;
        this.okButton.IsEnabled = true;
      }
      else
      {
        this.targetTextBox.Text = str;
        this.targetTextBox.Foreground = (Brush) new SolidColorBrush(Colors.Red);
        this.okButton.IsEnabled = false;
      }
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      this.flexHandler.SetControlCenter(this.stableAnchorType, this.getComponentAnchor(this.targetComponent.Component), this.targetComponent.Path);
      this.changesApplied = true;
      this.IsChangeApplied = true;
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void BaseChangeTransactionModeler_Closing(object sender, CancelEventArgs e)
    {
      this.revertChanges();
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/setcontrolcentermodeler.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.componentLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 2:
    //      this.targetTextBox = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 3:
    //      ((System.Windows.Controls.Primitives.ButtonBase) target).Click += new RoutedEventHandler(this.targetCompButton_Click);
    //      break;
    //    case 4:
    //      this.okButton = (System.Windows.Controls.Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 5:
    //      this.cancelButton = (System.Windows.Controls.Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    internal struct TargetComponent
    {
      public UXComponent Component;
      public string Path;
      public string Title;
    }
  }
}
