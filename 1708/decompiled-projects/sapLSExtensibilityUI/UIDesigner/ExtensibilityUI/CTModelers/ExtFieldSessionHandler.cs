﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.ExtFieldSessionHandler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Flexibility.CoreAPI.Extensibility;
using System.Collections.Generic;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  internal class ExtFieldSessionHandler
  {
    private static ExtFieldSessionHandler instance = new ExtFieldSessionHandler();
    private List<AddExtensionFieldModeler.SelectedItem> listSelectedItems;

    public static ExtFieldSessionHandler Instance
    {
      get
      {
        return ExtFieldSessionHandler.instance;
      }
    }

    private ExtFieldSessionHandler()
    {
    }

    public void AddSelectedItem(AddExtensionFieldModeler.SelectedItem item)
    {
      if (this.listSelectedItems == null)
        this.listSelectedItems = new List<AddExtensionFieldModeler.SelectedItem>();
      this.listSelectedItems.Add(item);
    }

    public bool Contains(FlexibilityHandler flexHandler, UIExtensionField field)
    {
      if (flexHandler == null || field == null || (field.customField == null || string.IsNullOrEmpty(field.customField.CoreName)) || this.listSelectedItems == null)
        return false;
      foreach (AddExtensionFieldModeler.SelectedItem listSelectedItem in this.listSelectedItems)
      {
        UIExtensionField extField = listSelectedItem.extField;
        if (extField != null && extField.customField != null && (field.businessContext != null && field.businessContext.Anchor != null) && (extField.businessContext != null && extField.businessContext.Anchor != null && field.businessContext.Anchor.xrepPath == extField.businessContext.Anchor.xrepPath))
        {
          string coreName = extField.customField.CoreName;
          string str = extField.customField.Namespace;
          if (!string.IsNullOrEmpty(coreName) && field.customField.CoreName == coreName && field.customField.Namespace == str)
          {
            string changeTransactionId = listSelectedItem.ChangeTransactionID;
            if (!string.IsNullOrEmpty(changeTransactionId) && this.isAdded(changeTransactionId, flexHandler.GetChangeTransactionsFromProtocol()))
              return true;
          }
        }
      }
      return false;
    }

    private bool isAdded(string ctId, List<ChangeTransaction> ctArray)
    {
      if (string.IsNullOrEmpty(ctId) || ctArray == null)
        return false;
      foreach (ChangeTransaction ct in ctArray)
      {
        if (ctId == ct.id)
          return true;
      }
      return false;
    }
  }
}
