﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddOVSInfoModeler
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Flexibility.CoreAPI;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.Utilities;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.Dialogs;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.CCTS;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.DataModel;
using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.RepositoryLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers
{
  public abstract partial class AddOVSInfoModeler : BaseChangeTransactionModeler, IComponentConnector
  {
    private const string TITLE_STRING = "Identifier Configuration";
    private string m_BONamspace;
    private string m_BOName;
    private string DescriptionNamespace;
    protected FlexibilityHandler flexHandler;
    protected SectionGroupType sectionGroupType;
    protected ListType listType;
    private FlexStableAnchorType stableAnchorType;
    private bool changesApplied;
    private ObservableCollection<AddOVSInfoModeler.SelectedItem> fieldCollection;
    private IImplementationController implementationController;
    private bool incrementOVSAreaOnce;
    private bool incrementOBNArea;
    public PublicDataModel publicModel;
    //internal System.Windows.Controls.TextBox entityHeader;
    //internal System.Windows.Controls.TextBox errorTextBox;
    //internal System.Windows.Controls.ListView itemListView;
    //internal GridView listViewGridView;
    //internal Grid IdentifierArea;
    //internal Grid OVSArea;
    //internal System.Windows.Controls.TextBox ovsComponent;
    //internal System.Windows.Controls.Button selectComponentButton;
    //internal System.Windows.Controls.CheckBox interpretAsInteger;
    //internal System.Windows.Controls.ComboBox presentationMode;
    //internal System.Windows.Controls.Label DescriptionLabel;
    //internal System.Windows.Controls.TextBox DescriptionBox;
    //internal System.Windows.Controls.Button selectDescriptionButton;
    //internal System.Windows.Controls.TextBox explanation;
    //internal System.Windows.Controls.TextBox warningText;
    //internal System.Windows.Controls.Button ConfigureOBNButton;
    //internal Grid OBNArea;
    //internal System.Windows.Controls.TextBox OBN_NS;
    //internal System.Windows.Controls.TextBox OBN_BO;
    //internal System.Windows.Controls.TextBox OBN_Node;
    //internal System.Windows.Controls.TextBox OBN_OP;
    //internal StackPanel ContentArea;
    //internal System.Windows.Controls.Button okButton;
    //internal System.Windows.Controls.Button cancelButton;
    //private bool _contentLoaded;

    public IFloorplan m_editableSourceComponent { get; private set; }

    public IModelObject m_relevantArea { get; private set; }

    public override bool RequiresReload
    {
      get
      {
        return true;
      }
    }

    public AddOVSInfoModeler()
    {
      this.InitializeComponent();
    }

    protected abstract void init();

    protected abstract List<FlexStableAnchorType> getStableAnchorList();

    protected override void InitializeModeler()
    {
      base.InitializeModeler();
      this.m_editableSourceComponent = ModelLayer.ObjectManager.GetFloorplanModel(this.UIComponent, (FloorplanInfo) null, false, true, this.DTComponent.Version);
      this.publicModel = new PublicDataModel(this.UIComponent, this.m_editableSourceComponent);
      this.publicModel.collectAllowedDataModel();
      this.implementationController = SAP.BYD.LS.UIDesigner.Model.Utilities.GetImplementationController(this.DTExtensibleModel);
      if (!this.UIComponent.useUIController)
      {
        this.DescriptionBox.Visibility = Visibility.Collapsed;
        this.DescriptionLabel.Visibility = Visibility.Collapsed;
        this.selectDescriptionButton.Visibility = Visibility.Collapsed;
      }
      this.Title = "Identifier Configuration";
      this.flexHandler = this.getFlexibilityHandler();
      if (this.flexHandler == null)
        return;
      this.init();
      foreach (FlexStableAnchorType stableAnchor in this.getStableAnchorList())
      {
        if (this.Anchor.xrepPath == stableAnchor.xrepPath)
        {
          this.stableAnchorType = stableAnchor;
          break;
        }
      }
      if (this.stableAnchorType == null)
        return;
      this.warningText.Text = "An eventhandler is already configured. It will be overwritten proceeding with OBN configuration.";
      this.warningText.Background = (Brush) new SolidColorBrush(Colors.Yellow);
      this.warningText.IsReadOnlyCaretVisible = false;
      this.fieldCollection = new ObservableCollection<AddOVSInfoModeler.SelectedItem>();
      this.assembleIDFieldInfo();
      if (this.fieldCollection == null || this.fieldCollection.Count == 0)
      {
        this.printError("No editable Identifier available in the selected area.");
        this.itemListView.Visibility = Visibility.Collapsed;
        this.Height = 150.0;
      }
      else
      {
        this.itemListView.DataContext = (object) this.fieldCollection;
        this.fillPresentationModeComboBox();
        this.optimizeControlHeigh();
      }
    }

    private void optimizeControlHeigh()
    {
      int num = this.fieldCollection.Count;
      if (this.fieldCollection.Count > 5)
        num = 5;
      this.itemListView.Height = (double) (40 + num * 30);
      this.Height = this.itemListView.Height + 150.0;
    }

    private IModelObject determinRelevantArea()
    {
      foreach (FlexBaseAnchor stableAnchor in SAP.BYD.LS.UIDesigner.Model.Utilities.GetStableAnchors((IModelObject) this.m_editableSourceComponent))
      {
        if (stableAnchor.XRepPath == this.stableAnchorType.xrepPath)
          return stableAnchor.Parent;
      }
      return (IModelObject) null;
    }

    protected override void revertChanges()
    {
      if (this.changesApplied)
        return;
      this.IsChangeApplied = false;
    }

    private string determinName(AbstractControl ac)
    {
      IModelObject modelObject = (IModelObject) ac;
      while (!(modelObject is List) && !(modelObject is SectionGroupItem))
      {
        modelObject = modelObject.Parent;
        if (!(modelObject is List) && modelObject is SectionGroupItem)
          return this.getText((modelObject as SectionGroupItem).Label.DependentPropertyType);
      }
      return ac.Name;
    }

    private bool isFieldAvailable(string fieldName)
    {
      return this.publicModel.isPublic(fieldName);
    }

    private void addIdentifier(AbstractControl ac, string label)
    {
      if (ac is StaticText || ac.BoundField == null || !this.isFieldAvailable(ac.BoundField.FullPath))
        return;
      AddOVSInfoModeler.SelectedItem selectedItem = new AddOVSInfoModeler.SelectedItem(ac)
      {
        Name = label,
        EsrName = ac.BoundField.FullPath
      };
      if (ac is ObjectValueSelector)
      {
        ObjectValueSelector objectValueSelector = ac as ObjectValueSelector;
        if (objectValueSelector.IsOVSComponentAssigned)
        {
          if (objectValueSelector.OnClick != null)
            selectedItem.IsEventhandlerConfigured = true;
          OVSConsumer ovsComponent = objectValueSelector.OVSComponent;
          selectedItem.setOVSConsumer(ovsComponent);
        }
      }
      if (selectedItem.isExtendedBy)
      {
        DataStructure parent = (ac.BoundField as BaseDataModelElement).Parent as DataStructure;
        selectedItem.parentDataStrcture = parent;
      }
      this.fieldCollection.Add(selectedItem);
    }

    private void assembleID(AbstractControl ac, string label)
    {
      if (ac is CompoundField)
      {
        foreach (AbstractControl ac1 in (ac as CompoundField).Items)
        {
          if (ac1.CCTSType == UXCCTSTypes.identifier)
            this.addIdentifier(ac1, label);
        }
      }
      else
      {
        if (ac.CCTSType != UXCCTSTypes.identifier)
          return;
        this.addIdentifier(ac, label);
      }
    }

    private void assembleIDFromSectionGroup(FieldGroup fg)
    {
      if (fg == null)
        return;
      foreach (SectionGroupItem field in fg.Fields)
      {
        string label = string.Empty;
        if (!field.NoLabel && field.Label == null)
        {
          if (field.Item != null)
            label = field.Item.Name;
        }
        else
          label = field.NoLabel ? field.Item.Name : this.getText(field.Label.DependentPropertyType);
        this.assembleID(field.Item, label);
      }
    }

    private void assembleIDFromList(List list)
    {
      if (list == null)
        return;
      foreach (ListColumn groupByColumns in list.GroupByColumnsList)
      {
        if (groupByColumns.Label != null && groupByColumns.Label.DependentPropertyType != null)
          this.assembleID(groupByColumns.Item, this.getText(groupByColumns.Label.DependentPropertyType));
      }
    }

    private void assembleIDFieldInfo()
    {
      this.m_relevantArea = this.DTAnchor != null ? this.DTAnchor.Parent : (IModelObject) null;
      if (this.m_relevantArea == null)
        return;
      if (this.m_relevantArea is FieldGroup)
      {
        this.assembleIDFromSectionGroup(this.m_relevantArea as FieldGroup);
      }
      else
      {
        if (!(this.m_relevantArea is List))
          return;
        this.assembleIDFromList(this.m_relevantArea as List);
      }
    }

    private void printError(string errorText)
    {
      if (!string.IsNullOrEmpty(errorText))
      {
        this.errorTextBox.Text = errorText;
        this.errorTextBox.Foreground = (Brush) new SolidColorBrush(Colors.Red);
        this.errorTextBox.Visibility = Visibility.Visible;
      }
      else
        this.errorTextBox.Visibility = Visibility.Collapsed;
    }

    private List<UXBaseDataElementType> getList(List<UXBaseDataElementType> dataModelItems, string path)
    {
      foreach (UXBaseDataElementType dataModelItem in dataModelItems)
      {
        if (dataModelItem is UXDataListType && dataModelItem.name.Equals(path))
          return (dataModelItem as UXDataListType).Items;
      }
      return (List<UXBaseDataElementType>) null;
    }

    private bool isAdded(List<UXBaseDataElementType> dataModelItems, UXBaseDataElementType dataType)
    {
      foreach (NamedModelEntity dataModelItem in dataModelItems)
      {
        if (dataModelItem.name == dataType.name)
          return true;
      }
      return false;
    }

    private void initializeOVSContent(AddOVSInfoModeler.SelectedItem selectedItem)
    {
      if (selectedItem == null)
      {
        this.IdentifierArea.Visibility = Visibility.Collapsed;
      }
      else
      {
        if (!this.incrementOVSAreaOnce)
        {
          this.incrementOVSAreaOnce = true;
          this.Height = this.Height + 220.0;
        }
        this.IdentifierArea.DataContext = (object) selectedItem;
        foreach (ComboBoxItem comboBoxItem in (IEnumerable) this.presentationMode.Items)
        {
          if (((IdentifierPresentationModeType) comboBoxItem.Content).ToString() == selectedItem.PresentationMode.ToString())
          {
            this.presentationMode.SelectedItem = (object) comboBoxItem;
            break;
          }
        }
        this.warningText.Visibility = Visibility.Collapsed;
        this.IdentifierArea.Visibility = Visibility.Visible;
        this.OVSArea.Visibility = Visibility.Visible;
        if (selectedItem.OBNHelper == null)
        {
          this.OBNArea.Visibility = Visibility.Collapsed;
        }
        else
        {
          this.OBNArea.Visibility = Visibility.Visible;
          if (this.OBNArea.Visibility == Visibility.Visible)
          {
            this.OBN_NS.Text = selectedItem.OBNHelper.NavBONS;
            this.OBN_BO.Text = selectedItem.OBNHelper.NavBO;
            this.OBN_Node.Text = selectedItem.OBNHelper.NavBONode;
            this.OBN_OP.Text = selectedItem.OBNHelper.NavOperation;
            if (selectedItem.OBNHelper.m_outportConfig != null)
            {
              this.ContentArea.Children.Clear();
              this.ContentArea.Children.Add((UIElement) selectedItem.OBNHelper.m_outportConfig);
              selectedItem.OBNHelper.configureParams();
            }
          }
        }
        if (string.IsNullOrEmpty(selectedItem.OVSName))
        {
          this.ConfigureOBNButton.Visibility = Visibility.Collapsed;
          this.warningText.Visibility = Visibility.Collapsed;
          this.interpretAsInteger.IsEnabled = false;
          this.presentationMode.IsEnabled = false;
          this.explanation.IsEnabled = false;
          this.selectDescriptionButton.IsEnabled = false;
        }
        else
        {
          this.ConfigureOBNButton.Visibility = Visibility.Visible;
          if (selectedItem.IsEventhandlerConfigured && selectedItem.OBNHelper == null)
            this.warningText.Visibility = Visibility.Visible;
          this.interpretAsInteger.IsEnabled = true;
          this.presentationMode.IsEnabled = true;
          this.explanation.IsEnabled = true;
        }
      }
    }

    private void itemListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      AddOVSInfoModeler.SelectedItem selectedItem = (AddOVSInfoModeler.SelectedItem) null;
      if (e != null && e.AddedItems != null && e.AddedItems.Count > 0)
        selectedItem = (AddOVSInfoModeler.SelectedItem) e.AddedItems[0];
      this.initializeOVSContent(selectedItem);
    }

    private string createCT(AddOVSInfoModeler.SelectedItem item)
    {
      OBNInfo obnInfo = (OBNInfo) null;
      if (item == null)
        return (string) null;
      if (string.IsNullOrEmpty(item.OVSName))
      {
        this.printError("Please specify OVS configuration on item with name '" + item.Name + "' before proceeding.");
        return (string) null;
      }
      OVSInfo ovsInfo = item.getOVSInfo();
      if (item.isExtendedBy && this.DescriptionBox.Text != null && this.DescriptionBox.Text != "")
        this._setDescriptionBinding(ovsInfo, item);
      if (item.OBNHelper != null)
        obnInfo = item.getOBNInfo();
      if (obnInfo == null && ovsInfo == null)
        return (string) null;
      return this.flexHandler.ChangingIDConfig(this.Anchor, item.Control.Id, ovsInfo, obnInfo);
    }

    private void _setDescriptionBinding(OVSInfo ovsInfo, AddOVSInfoModeler.SelectedItem item)
    {
      string text = this.DescriptionBox.Text;
      DataStructure parentDataStrcture = item.parentDataStrcture;
      string str1 = text;
      string[] strArray1 = parentDataStrcture.BackendBindingPath.Split(new string[1]
      {
        "-"
      }, StringSplitOptions.RemoveEmptyEntries);
      string[] strArray2 = text.Split(new string[1]{ "-" }, StringSplitOptions.RemoveEmptyEntries);
      for (int index = 0; index < strArray1.Length; ++index)
      {
        string str2 = strArray2[index];
        str1 = !strArray1[index].StartsWith(".") ? str1.Substring(str2.Length) : str1.Substring(str2.Length + 1);
      }
      ovsInfo.descriptionBinding = str1;
      ovsInfo.identifierDescriptionNamespace = this.DescriptionNamespace;
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
      foreach (AddOVSInfoModeler.SelectedItem field in (Collection<AddOVSInfoModeler.SelectedItem>) this.fieldCollection)
      {
        if (field.IsChanged && !string.IsNullOrEmpty(this.createCT(field)))
        {
          this.changesApplied = true;
          this.IsChangeApplied = true;
        }
      }
      this.Close();
    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private bool isAllowedForUsage(string ovsPath)
    {
      if (LoginManager.Instance.DTMode != DTModes.Partner)
        return true;
      FloorplanInfo fp = new FloorplanInfo();
      fp.UniqueId = ovsPath;
      UXComponent uxComponent = ProjectWorkspaceManager.Instance.GetUXComponent(ref fp, false);
      foreach (RepositoryAttribute attribute in fp.Attributes)
      {
        if (attribute.Name.Equals("~SOLUTION") && attribute.Value.Equals(LoginManager.Instance.Solution))
          return true;
      }
      if (uxComponent.Tags != null)
      {
        foreach (TagType tagType in uxComponent.Tags.Tag)
        {
          if (tagType.attribute.Equals("ISVL") && tagType.value.Equals("X"))
            return true;
        }
      }
      return false;
    }

    private void fillPresentationModeComboBox()
    {
      this.presentationMode.Items.Clear();
      ComboBoxItem comboBoxItem1 = new ComboBoxItem();
      comboBoxItem1.Content = (object) IdentifierPresentationModeType.DescriptionOnly;
      this.presentationMode.Items.Add((object) comboBoxItem1);
      ComboBoxItem comboBoxItem2 = new ComboBoxItem();
      comboBoxItem2.Content = (object) IdentifierPresentationModeType.IDAndDescription;
      this.presentationMode.Items.Add((object) comboBoxItem2);
      ComboBoxItem comboBoxItem3 = new ComboBoxItem();
      comboBoxItem3.Content = (object) IdentifierPresentationModeType.IDOnly;
      this.presentationMode.Items.Add((object) comboBoxItem3);
    }

    private void presentationMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      AddOVSInfoModeler.SelectedItem selectedItem = (AddOVSInfoModeler.SelectedItem) this.itemListView.SelectedItem;
      if (selectedItem == null)
        return;
      ComboBoxItem comboBoxItem = (ComboBoxItem) null;
      if (e != null && e.AddedItems != null && e.AddedItems.Count > 0)
        comboBoxItem = (ComboBoxItem) e.AddedItems[0];
      selectedItem.PresentationMode = (IdentifierPresentationModeType) comboBoxItem.Content;
      if ((selectedItem.PresentationMode == IdentifierPresentationModeType.DescriptionOnly || selectedItem.PresentationMode == IdentifierPresentationModeType.IDAndDescription) && selectedItem.isExtendedBy)
        this.selectDescriptionButton.IsEnabled = true;
      else
        this.selectDescriptionButton.IsEnabled = false;
      this.DescriptionBox.Text = string.Empty;
    }

    public void displayOVSEditor(IModelObject parent, AddOVSInfoModeler.SelectedItem item)
    {
      if (parent == null || item == null)
        return;
      OVSConsumer component = item.OVSConsumer == null ? new OVSConsumer(this.m_relevantArea) : item.OVSConsumer;
      try
      {
        OVSComponentSelector componentSelector = new OVSComponentSelector(component, parent, true);
        if (componentSelector.ShowDialog() != System.Windows.Forms.DialogResult.OK)
          return;
        if (this.isAllowedForUsage(componentSelector.NewOVSComponent.OVSComponentName))
        {
          item.setOVSConsumer(componentSelector.NewOVSComponent);
          this.printError((string) null);
          this.ConfigureOBNButton.Visibility = Visibility.Visible;
          if (item.IsEventhandlerConfigured && item.OBNHelper == null)
            this.warningText.Visibility = Visibility.Visible;
          this.interpretAsInteger.IsEnabled = true;
          this.presentationMode.IsEnabled = true;
          this.explanation.IsEnabled = true;
          if (this.presentationMode.Text == "IDAndDescription" || this.presentationMode.Text == "DescriptionOnly")
            this.selectDescriptionButton.IsEnabled = true;
          else
            this.selectDescriptionButton.IsEnabled = false;
        }
        else
          this.printError("The selected OVS may not be used by ISV");
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException(ex));
      }
    }

    private void DescriptionButton_Click(object sender, RoutedEventArgs e)
    {
      if (this.implementationController == null || this.implementationController.DataModel == null)
        return;
      this.m_BOName = this.implementationController.DataModel.RootBOName;
      this.m_BONamspace = this.implementationController.DataModel.RootNameSpace;
      BOFieldSelectorDialog fieldSelectorDialog = new BOFieldSelectorDialog(this.m_BOName, this.m_BONamspace, (string) null, false);
      if (string.IsNullOrEmpty(fieldSelectorDialog.SelectedBO))
        return;
      fieldSelectorDialog.ShowDialog();
      bool? dialogResult = fieldSelectorDialog.DialogResult;
      if ((!dialogResult.GetValueOrDefault() ? 0 : (dialogResult.HasValue ? 1 : 0)) == 0 || !(fieldSelectorDialog.SelectedPath != ""))
        return;
      this.DescriptionBox.Text = fieldSelectorDialog.SelectedPath;
      this.DescriptionNamespace = fieldSelectorDialog.SelectedFieldExtensionNamespace;
    }

    private void ovsButton_Click(object sender, RoutedEventArgs e)
    {
      this.printError((string) null);
      if (this.m_relevantArea == null || sender == null)
        return;
      AddOVSInfoModeler.SelectedItem selectedItem = (AddOVSInfoModeler.SelectedItem) this.itemListView.SelectedItem;
      if (selectedItem == null)
        return;
      this.displayOVSEditor(this.m_relevantArea, selectedItem);
      if (this.itemListView.SelectedItem == selectedItem)
        return;
      this.itemListView.SelectedItem = (object) selectedItem;
    }

    private void selectConfigureOBNButton_Click(object sender, RoutedEventArgs e)
    {
      AddOVSInfoModeler.SelectedItem selectedItem = (AddOVSInfoModeler.SelectedItem) this.itemListView.SelectedItem;
      if (selectedItem == null)
        return;
      this.warningText.Visibility = Visibility.Collapsed;
      if (selectedItem.OBNHelper == null)
        selectedItem.OBNHelper = new OBNConfigHelper(this.m_editableSourceComponent);
      if (selectedItem.OBNHelper == null)
        return;
      selectedItem.OBNHelper.configureOBN();
      if (selectedItem.OBNHelper.m_outportConfig == null)
      {
        this.OBNArea.Visibility = Visibility.Collapsed;
        if (selectedItem.IsEventhandlerConfigured)
          this.warningText.Visibility = Visibility.Visible;
        selectedItem.OBNHelper = (OBNConfigHelper) null;
      }
      else if (selectedItem.OBNHelper.NavBONS == null && selectedItem.OBNHelper.NavBO == null)
      {
        selectedItem.OBNHelper = (OBNConfigHelper) null;
      }
      else
      {
        this.incrementOBNArea = true;
        this.OBNArea.Visibility = Visibility.Visible;
        this.OBN_NS.Text = selectedItem.OBNHelper.NavBONS;
        this.OBN_BO.Text = selectedItem.OBNHelper.NavBO;
        this.OBN_Node.Text = selectedItem.OBNHelper.NavBONode;
        this.OBN_OP.Text = selectedItem.OBNHelper.NavOperation;
        this.ContentArea.Children.Clear();
        this.ContentArea.Children.Add((UIElement) selectedItem.OBNHelper.m_outportConfig);
        selectedItem.OBNHelper.configureParams();
      }
    }

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  System.Windows.Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/ctmodelers/addovsinfomodeler.xaml", UriKind.Relative));
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //internal Delegate _CreateDelegate(Type delegateType, string handler)
    //{
    //  return Delegate.CreateDelegate(delegateType, (object) this, handler);
    //}

    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //[EditorBrowsable(EditorBrowsableState.Never)]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.entityHeader = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 2:
    //      this.errorTextBox = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 3:
    //      this.itemListView = (System.Windows.Controls.ListView) target;
    //      this.itemListView.SelectionChanged += new SelectionChangedEventHandler(this.itemListView_SelectionChanged);
    //      break;
    //    case 4:
    //      this.listViewGridView = (GridView) target;
    //      break;
    //    case 5:
    //      this.IdentifierArea = (Grid) target;
    //      break;
    //    case 6:
    //      this.OVSArea = (Grid) target;
    //      break;
    //    case 7:
    //      this.ovsComponent = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 8:
    //      this.selectComponentButton = (System.Windows.Controls.Button) target;
    //      this.selectComponentButton.Click += new RoutedEventHandler(this.ovsButton_Click);
    //      break;
    //    case 9:
    //      this.interpretAsInteger = (System.Windows.Controls.CheckBox) target;
    //      break;
    //    case 10:
    //      this.presentationMode = (System.Windows.Controls.ComboBox) target;
    //      this.presentationMode.SelectionChanged += new SelectionChangedEventHandler(this.presentationMode_SelectionChanged);
    //      break;
    //    case 11:
    //      this.DescriptionLabel = (System.Windows.Controls.Label) target;
    //      break;
    //    case 12:
    //      this.DescriptionBox = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 13:
    //      this.selectDescriptionButton = (System.Windows.Controls.Button) target;
    //      this.selectDescriptionButton.Click += new RoutedEventHandler(this.DescriptionButton_Click);
    //      break;
    //    case 14:
    //      this.explanation = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 15:
    //      this.warningText = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 16:
    //      this.ConfigureOBNButton = (System.Windows.Controls.Button) target;
    //      this.ConfigureOBNButton.Click += new RoutedEventHandler(this.selectConfigureOBNButton_Click);
    //      break;
    //    case 17:
    //      this.OBNArea = (Grid) target;
    //      break;
    //    case 18:
    //      this.OBN_NS = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 19:
    //      this.OBN_BO = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 20:
    //      this.OBN_Node = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 21:
    //      this.OBN_OP = (System.Windows.Controls.TextBox) target;
    //      break;
    //    case 22:
    //      this.ContentArea = (StackPanel) target;
    //      break;
    //    case 23:
    //      this.okButton = (System.Windows.Controls.Button) target;
    //      this.okButton.Click += new RoutedEventHandler(this.okButton_Click);
    //      break;
    //    case 24:
    //      this.cancelButton = (System.Windows.Controls.Button) target;
    //      this.cancelButton.Click += new RoutedEventHandler(this.cancelButton_Click);
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    public class SelectedItem : DependencyObject
    {
      public static readonly DependencyProperty IsInterpretAsIntegerProperty = DependencyProperty.Register("IsInterpretAsInteger", typeof (bool), typeof (AddOVSInfoModeler.SelectedItem), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddOVSInfoModeler.SelectedItem.isPropertyChangeCallback)));
      public static readonly DependencyProperty PresentationModeProperty = DependencyProperty.Register("PresentationMode", typeof (IdentifierPresentationModeType), typeof (AddOVSInfoModeler.SelectedItem), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddOVSInfoModeler.SelectedItem.isPropertyChangeCallback)));
      public static readonly DependencyProperty DescriptionBindingProperty = DependencyProperty.Register("descriptionBinding", typeof (string), typeof (AddOVSInfoModeler.SelectedItem), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddOVSInfoModeler.SelectedItem.isPropertyChangeCallback)));
      public static readonly DependencyProperty ExplanationProperty = DependencyProperty.Register("Explanation", typeof (string), typeof (AddOVSInfoModeler.SelectedItem), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddOVSInfoModeler.SelectedItem.isPropertyChangeCallback)));
      public static readonly DependencyProperty OVSNameProperty = DependencyProperty.Register("OVSName", typeof (string), typeof (AddOVSInfoModeler.SelectedItem), (PropertyMetadata) new UIPropertyMetadata(new PropertyChangedCallback(AddOVSInfoModeler.SelectedItem.isPropertyChangeCallback)));
      private AddOVSInfoModeler.SelectedItem _self;
      public bool isExtendedBy;
      public string backendBindingPath;
      public DataStructure parentDataStrcture;
      public AddExtensionFieldModeler.OVSInfoModel ovsInfoModel;
      private OVSConsumer _ovsConsumer;
      private AbstractControl _ac;

      public OBNConfigHelper OBNHelper { get; set; }

      public string Name { get; set; }

      public string EsrName { get; set; }

      public bool IsEventhandlerConfigured { get; set; }

      public AddOVSInfoModeler.SelectedItem Self
      {
        get
        {
          return this._self;
        }
      }

      public bool IsChanged
      {
        get
        {
          return this._isChanged();
        }
      }

      public OVSConsumer OVSConsumer
      {
        get
        {
          return this._ovsConsumer;
        }
      }

      public AbstractControl Control
      {
        get
        {
          return this._ac;
        }
      }

      public bool IsInterpretAsInteger
      {
        get
        {
          return (bool) this.GetValue(AddOVSInfoModeler.SelectedItem.IsInterpretAsIntegerProperty);
        }
        set
        {
          this.SetValue(AddOVSInfoModeler.SelectedItem.IsInterpretAsIntegerProperty, (object) value);
        }
      }

      public IdentifierPresentationModeType PresentationMode
      {
        get
        {
          return (IdentifierPresentationModeType) this.GetValue(AddOVSInfoModeler.SelectedItem.PresentationModeProperty);
        }
        set
        {
          this.SetValue(AddOVSInfoModeler.SelectedItem.PresentationModeProperty, (object) value);
        }
      }

      public string DescriptionBind
      {
        get
        {
          return (string) this.GetValue(AddOVSInfoModeler.SelectedItem.DescriptionBindingProperty);
        }
        set
        {
          this.SetValue(AddOVSInfoModeler.SelectedItem.DescriptionBindingProperty, (object) value);
        }
      }

      public string Explanation
      {
        get
        {
          return (string) this.GetValue(AddOVSInfoModeler.SelectedItem.ExplanationProperty);
        }
        set
        {
          this.SetValue(AddOVSInfoModeler.SelectedItem.ExplanationProperty, (object) value);
        }
      }

      public string OVSName
      {
        get
        {
          return (string) this.GetValue(AddOVSInfoModeler.SelectedItem.OVSNameProperty);
        }
        set
        {
          this.SetValue(AddOVSInfoModeler.SelectedItem.OVSNameProperty, (object) value);
        }
      }

      public SelectedItem(AbstractControl ac)
      {
        this._ac = ac;
        this._self = this;
        this._ovsConsumer = (OVSConsumer) null;
        this.OBNHelper = (OBNConfigHelper) null;
        this.IsEventhandlerConfigured = false;
        this.PresentationMode = IdentifierPresentationModeType.IDAndDescription;
        this.isExtendedBy = this._isExtension(ac);
        this.backendBindingPath = ac.BoundField.BackendBindingPath;
        this.parentDataStrcture = (DataStructure) null;
        IdentifierCCTS identifierCcts = this.determinIdentifierCCTS(ac);
        if (identifierCcts != null)
        {
          this.PresentationMode = identifierCcts.PresentationMode;
          this.IsInterpretAsInteger = identifierCcts.InterpretAsInteger;
        }
        if (ac.Explanation == null)
          return;
        this.Explanation = ac.Explanation.Text;
      }

      private bool _isExtension(AbstractControl ac)
      {
        if (ac.Parent != null && ac.Parent is SectionGroupItem)
          return (ac.Parent as SectionGroupItem).isExtendedBy();
        return false;
      }

      private IdentifierCCTS determinIdentifierCCTS(AbstractControl ac)
      {
        IdentifierCCTS identifierCcts = (IdentifierCCTS) null;
        if (ac is InputField)
          identifierCcts = (ac as InputField).CCTSItem as IdentifierCCTS;
        else if (ac is ObjectValueSelector)
          identifierCcts = (ac as ObjectValueSelector).CCTSItem as IdentifierCCTS;
        return identifierCcts;
      }

      public void setOVSConsumer(OVSConsumer ovsConsumer)
      {
        this.OVSName = ovsConsumer == null ? string.Empty : ovsConsumer.OVSComponentName;
        this._ovsConsumer = ovsConsumer;
      }

      private static void isPropertyChangeCallback(DependencyObject dp, DependencyPropertyChangedEventArgs args)
      {
      }

      private bool isContextDifferent(List<ContextParameter> contextParameters)
      {
        if (this._ovsConsumer.ContextParameters == contextParameters)
          return false;
        if (this._ovsConsumer.ContextParameters == null || contextParameters.Count != this._ovsConsumer.ContextParameters.Count)
          return true;
        if (contextParameters.Count == 0)
          return false;
        foreach (ContextParameter contextParameter1 in contextParameters)
        {
          bool flag = false;
          foreach (ContextParameter contextParameter2 in this._ovsConsumer.ContextParameters)
          {
            if (contextParameter1.Name == contextParameter2.Name && contextParameter1.Value == contextParameter2.Value && contextParameter1.Visible == contextParameter2.Visible)
            {
              flag = true;
              break;
            }
          }
          if (!flag)
            return true;
        }
        return false;
      }

      private bool isOVSDifferent(OVSConsumer ovsConsumer)
      {
        if (this._ovsConsumer == ovsConsumer)
          return false;
        if (this._ovsConsumer == null || this._ovsConsumer.OVSComponentName != ovsConsumer.OVSComponentName || (this._ovsConsumer.OVSWindowTitle != ovsConsumer.OVSWindowTitle || this._ovsConsumer.SuggestEnabled != ovsConsumer.SuggestEnabled) || (this._ovsConsumer.TokenEnabled != ovsConsumer.TokenEnabled || this._ovsConsumer.PreventQueryExecution != ovsConsumer.PreventQueryExecution))
          return true;
        return this.isContextDifferent(ovsConsumer.ContextParameters);
      }

      private List<ContextParameter> checkForPublicContext(List<ContextParameter> contextParameters)
      {
        return contextParameters;
      }

      public bool isOVSChanged()
      {
        if (this.Control is ObjectValueSelector)
        {
          if (this.isOVSDifferent((this.Control as ObjectValueSelector).OVSComponent))
            return true;
        }
        else if (this.OVSConsumer != null)
          return true;
        if (this.Explanation != (this.Control.Explanation != null ? this.Control.Explanation.Text : (string) null))
          return true;
        IdentifierCCTS identifierCcts = this.determinIdentifierCCTS(this.Control);
        return identifierCcts != null && (this.PresentationMode != identifierCcts.PresentationMode || this.IsInterpretAsInteger != identifierCcts.InterpretAsInteger);
      }

      private bool _isChanged()
      {
        if (this.OBNHelper != null)
          return true;
        return this.isOVSChanged();
      }

      public OBNInfo getOBNInfo()
      {
        if (this.OBNHelper == null)
          return (OBNInfo) null;
        return new OBNInfo()
        {
          bo_ns = this.OBNHelper.NavBONS,
          bo_name = this.OBNHelper.NavBO,
          bo_node = this.OBNHelper.NavBONode,
          bo_op = this.OBNHelper.NavOperation,
          ctx_attr = this.OBNHelper.NavContextAttribute,
          ctx_const = this.OBNHelper.isConstantNavContext ? "X" : (string) null,
          ptp_package = this.OBNHelper.PortTypePackage,
          ptp_ref = this.OBNHelper.PortTypeReference,
          mapping = this.OBNHelper.SimpleParameters,
          listMapping = this.OBNHelper.ListParameters
        };
      }

      private List<OVSContextParameter> getContextParams()
      {
        if (this._ovsConsumer.ContextParameters == null)
          return (List<OVSContextParameter>) null;
        List<OVSContextParameter> contextParameterList = new List<OVSContextParameter>();
        foreach (ContextParameter contextParameter1 in this._ovsConsumer.ContextParameters)
        {
          OVSContextParameter contextParameter2 = new OVSContextParameter();
          if (contextParameter1.Name.BoundField != null)
          {
            contextParameter2.paramName = contextParameter1.Name.BoundField.FullPath;
            contextParameter2.isNameBound = true;
          }
          else
          {
            contextParameter2.paramName = contextParameter1.Name.Text;
            contextParameter2.isNameBound = false;
          }
          if (contextParameter1.Value.BoundField != null)
          {
            contextParameter2.paramValue = contextParameter1.Value.BoundField.FullPath;
            contextParameter2.isValueBound = true;
          }
          else
          {
            contextParameter2.paramValue = contextParameter1.Value.Text;
            contextParameter2.isValueBound = false;
          }
          contextParameter2.paramVisible = (string) null;
          contextParameter2.isVisibleBound = new bool?(false);
          if (contextParameter1.Visible != null && contextParameter1.Visible.BoundField != null)
          {
            contextParameter2.paramVisible = contextParameter1.Visible.BoundField.FullPath;
            contextParameter2.isVisibleBound = new bool?(true);
          }
          contextParameterList.Add(contextParameter2);
        }
        return contextParameterList;
      }

      public OVSInfo getOVSInfo()
      {
        return new OVSInfo()
        {
          ovsComponentPath = this.OVSName,
          presentationMode = new IdentifierPresentationModeType?(this.PresentationMode),
          interpretAsInteger = new bool?(this.IsInterpretAsInteger),
          preventQueryExecution = new bool?(this._ovsConsumer.PreventQueryExecution),
          suggestEnabled = new bool?(this._ovsConsumer.SuggestEnabled),
          tokenEnabled = new bool?(this._ovsConsumer.TokenEnabled),
          uuidSupport = new bool?(),
          explanationFallback = this.Explanation,
          contextParams = this.getContextParams()
        };
      }
    }
  }
}
