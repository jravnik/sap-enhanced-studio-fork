﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddCondition.PropertyInfo
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddCondition
{
  public class PropertyInfo
  {
    private string name;
    private BindingConditionType binding;
    private bool? fallbackValue;

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public string FallbackValue
    {
      get
      {
        if (!this.fallbackValue.HasValue)
          return string.Empty;
        return this.fallbackValue.Value ? "true" : "false";
      }
      set
      {
        if (value == "x" || value == "X" || (value == "true" || value == "false") || (value == "TRUE" || value == "FALSE"))
          this.fallbackValue = new bool?(true);
        else
          this.fallbackValue = new bool?(false);
      }
    }

    public BindingConditionType Binding
    {
      get
      {
        if (this.binding == null)
          return this.binding = new BindingConditionType();
        return this.binding;
      }
      set
      {
        this.FallbackValue = (string) null;
        this.binding = value;
      }
    }

    public PropertyInfo(string name)
    {
      this.name = name;
      this.fallbackValue = new bool?();
    }
  }
}
