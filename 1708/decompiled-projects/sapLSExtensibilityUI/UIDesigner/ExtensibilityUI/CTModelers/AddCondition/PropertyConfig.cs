﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddCondition.PropertyConfig
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UIDesigner.Model.Core;
using System.Collections.Generic;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers.AddCondition
{
  public class PropertyConfig
  {
    private bool hasOwnAnchor;

    public bool HasOwnAnchor
    {
      get
      {
        return this.hasOwnAnchor;
      }
    }

    public string Name { get; set; }

    public ModelObject ControlModel { get; set; }

    public List<PropertyInfo> Properties { get; set; }

    public PropertyConfig(bool hasOwnAnchor)
    {
      this.hasOwnAnchor = hasOwnAnchor;
    }
  }
}
