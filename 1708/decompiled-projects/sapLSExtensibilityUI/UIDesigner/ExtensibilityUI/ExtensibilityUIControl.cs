﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.ExtensibilityUIControl
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Base;
using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Foundation.Controls;
using SAP.BYD.LS.UI.Skins;
using SAP.BYD.LS.UIDesigner.Connector;
using SAP.BYD.LS.UIDesigner.Connector.Services.XRepository.Designtime;
using SAP.BYD.LS.UIDesigner.Model;
using SAP.BYD.LS.UIDesigner.Model.Core;
using SAP.BYD.LS.UIDesigner.Model.Entities.Controls;
using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;
using SAP.BYD.LS.UIDesigner.Model.Entities.Floorplans;
using SAP.BYD.LS.UIDesigner.Model.ExceptionHandlingLayer;
using SAP.BYD.LS.UIDesigner.Model.UIEditors;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml.Linq;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI
{
  public partial class ExtensibilityUIControl : UserControl, IComponentConnector, IStyleConnector
  {
    private ObservableCollection<ExtensibilityUIBinding> m_extensibilityUIBindingCollection = new ObservableCollection<ExtensibilityUIBinding>();
    private string m_ComponentId = string.Empty;
    private Bitmap GREEN_ICON = SkinResource.GreenIcon;
    private Bitmap YELLOW_ICON = SkinResource.YellowIcon;
    private Bitmap RED_ICON = SkinResource.RedIcon;
    private JSONClient jsonClient;
    private static string currentSelectedNavigationItemID;
    private IExtensibilityExplorer m_ParentExplorer;
    //internal Grid rootGrid;
    //internal System.Windows.Controls.DataGrid anchorDetailsDataGrid;
    //internal DataGridTextColumn IdentifierField;
    //internal TextBlock IdentifierFieldColumnHeader;
    //internal TextBox EntitySearchTxtBox;
    //internal TextBlock EntityColumnHeader;
    //internal TextBox AnchorSearchTxtBox;
    //internal TextBlock navigationTextBlock;
    //internal TreeView m_treeView;
    //internal StackPanel OperationsPanel;
    //private bool _contentLoaded;

    public static string CurrentSelectedNavigationItemID
    {
      get
      {
        return ExtensibilityUIControl.currentSelectedNavigationItemID;
      }
    }

    public ExtensibilityUIControl(IExtensibilityExplorer parentExplorer)
    {
      this.jsonClient = new JSONClient();
      this.m_ParentExplorer = parentExplorer;
      this.InitializeComponent();
      ChangeTransactionManager.Instance.AfterSavedChangeTransaction += new AfterSaveChangeTransactionEventHadler(this.OnAfterSavedChangeTransaction);
    }

    public void NotifySelectionChanged()
    {
      try
      {
        bool flag = false;
        IFloorplan floorplanObject = Utilities.GetFloorplanObject(this.m_ParentExplorer.SelectedObject);
        ViewSwitchNavigationItem selectedObject1 = this.m_ParentExplorer.SelectedObject as ViewSwitchNavigationItem;
        if (selectedObject1 != null)
          ExtensibilityUIControl.currentSelectedNavigationItemID = selectedObject1.NavigationItemId;
        if (floorplanObject != null)
        {
          if (this.IsChangeTransactionCreationAllowed(floorplanObject) || floorplanObject.Changeability == ContentChangeability.ChangeTransaction)
          {
            this.rootGrid.IsEnabled = true;
            if (this.m_ParentExplorer.SelectedObject is IExtensible)
            {
              if (((BaseComponent) floorplanObject).Anchor != null && ((BaseComponent) floorplanObject).Anchor.Count > 0)
                flag = true;
              if (this.m_ComponentId != null && !this.m_ComponentId.Equals(floorplanObject.UniqueId))
              {
                this.m_ParentExplorer.RaiseSelectionChanged((IModelObject) floorplanObject);
                this.m_ComponentId = floorplanObject.UniqueId;
                this.m_extensibilityUIBindingCollection.Clear();
                BindingOperations.ClearAllBindings((DependencyObject) this.anchorDetailsDataGrid);
                this.anchorDetailsDataGrid.ClearValue(ItemsControl.ItemsSourceProperty);
              }
              List<FlexBaseAnchor> supportedAnchors = ChangeTransactionManager.Instance.GetSupportedAnchors(Utilities.GetStableAnchors((IModelObject) floorplanObject));
              if (this.m_extensibilityUIBindingCollection.Count <= 0 && supportedAnchors != null && supportedAnchors.Count > 0)
              {
                int num = -1;
                foreach (FlexBaseAnchor flexBaseAnchor in supportedAnchors)
                {
                  ExtensibilityUIBinding extensibilityUiBinding = new ExtensibilityUIBinding();
                  extensibilityUiBinding.AnchorName = flexBaseAnchor;
                  extensibilityUiBinding.XRepPathName = flexBaseAnchor.RelativeXRepPath;
                  IExtensible parent = flexBaseAnchor.Parent as IExtensible;
                  if (flag && flexBaseAnchor == ((BaseComponent) floorplanObject).Anchor[0])
                    num = this.m_extensibilityUIBindingCollection.Count;
                  extensibilityUiBinding.Image = parent.AnchorImage;
                  extensibilityUiBinding.DisplayText = parent.IdentityField != null ? parent.IdentityField : "EmptyIdentity";
                  extensibilityUiBinding.ExtensibleModelType = parent.TypeName;
                  this.m_extensibilityUIBindingCollection.Add(extensibilityUiBinding);
                }
                this.anchorDetailsDataGrid.ItemsSource = (IEnumerable) this.m_extensibilityUIBindingCollection;
                this.anchorDetailsDataGrid.SelectedIndex = num;
              }
              else
              {
                IExtensible selectedObject2 = this.m_ParentExplorer.SelectedObject as IExtensible;
                this.anchorDetailsDataGrid.SelectedIndex = -1;
                this.m_treeView.Items.Clear();
                foreach (ExtensibilityUIBinding extensibilityUiBinding in (Collection<ExtensibilityUIBinding>) this.m_extensibilityUIBindingCollection)
                {
                  List<FlexBaseAnchor> associatedAnchors = selectedObject2.GetAssociatedAnchors();
                  if (associatedAnchors != null && associatedAnchors.Contains<FlexBaseAnchor>(extensibilityUiBinding.AnchorName))
                  {
                    this.anchorDetailsDataGrid.SelectedItem = (object) extensibilityUiBinding;
                    break;
                  }
                }
              }
            }
            else
            {
              this.anchorDetailsDataGrid.SelectedIndex = -1;
              this.m_treeView.Items.Clear();
            }
          }
          else
          {
            this.anchorDetailsDataGrid.SelectedIndex = -1;
            this.m_treeView.Items.Clear();
            this.rootGrid.IsEnabled = false;
            this.m_extensibilityUIBindingCollection.Clear();
            BindingOperations.ClearAllBindings((DependencyObject) this.anchorDetailsDataGrid);
            this.anchorDetailsDataGrid.ClearValue(ItemsControl.ItemsSourceProperty);
          }
        }
        else
        {
          this.m_treeView.Items.Clear();
          this.m_extensibilityUIBindingCollection.Clear();
          BindingOperations.ClearAllBindings((DependencyObject) this.anchorDetailsDataGrid);
          this.anchorDetailsDataGrid.ClearValue(ItemsControl.ItemsSourceProperty);
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Selection changed notification failed.", ex));
      }
    }

    public void OnExtensibilityExplorerCloseEvent()
    {
      ChangeTransactionManager.Instance.AfterSavedChangeTransaction -= new AfterSaveChangeTransactionEventHadler(this.OnAfterSavedChangeTransaction);
    }

    private void OnAnchorDetailsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      try
      {
        if (!(this.anchorDetailsDataGrid.SelectedItem is ExtensibilityUIBinding))
          return;
        FlexBaseAnchor anchorName = (this.anchorDetailsDataGrid.SelectedItem as ExtensibilityUIBinding).AnchorName;
        IModelObject parent = (IModelObject) (anchorName.Parent as IExtensible);
        List<ChangeTransactionInfo> changeTransactions = ChangeTransactionManager.Instance.GetAllowedChangeTransactions((FlexAnchorEnumType) Enum.Parse(typeof (FlexAnchorEnumType), anchorName.Type.ToString()), anchorName);
        IFloorplan floorplanObject = Utilities.GetFloorplanObject(parent);
        if (floorplanObject.Changeability == ContentChangeability.ChangeTransaction)
        {
          this.m_treeView.Items.Clear();
          foreach (ChangeTransactionInfo changeTransactionInfo in changeTransactions)
          {
            TreeViewItem treeViewItem1 = new TreeViewItem();
            treeViewItem1.Header = (object) this.RenderHeader((Bitmap) null, (object) changeTransactionInfo, false);
            treeViewItem1.Expanded += new RoutedEventHandler(this.OnTreeViewBeforeExpand);
            TreeViewItem treeViewItem2 = new TreeViewItem();
            treeViewItem1.Items.Add((object) treeViewItem2);
            this.m_treeView.Items.Add((object) treeViewItem1);
          }
        }
        else if (this.IsChangeTransactionCreationAllowed(floorplanObject))
        {
          this.m_treeView.Items.Clear();
          foreach (ChangeTransactionInfo changeTransactionInfo in changeTransactions)
          {
            if (changeTransactionInfo.IsAllowedToAddInSameLayer)
            {
              TreeViewItem treeViewItem1 = new TreeViewItem();
              treeViewItem1.Header = (object) this.RenderHeader((Bitmap) null, (object) changeTransactionInfo, false);
              treeViewItem1.Expanded += new RoutedEventHandler(this.OnTreeViewBeforeExpand);
              TreeViewItem treeViewItem2 = new TreeViewItem();
              treeViewItem1.Items.Add((object) treeViewItem2);
              this.m_treeView.Items.Add((object) treeViewItem1);
            }
          }
        }
        if (!(parent.Id != this.m_ParentExplorer.SelectedObject.Id))
          return;
        this.m_ParentExplorer.RaiseSelectionChanged(parent);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Loading context specific information for Anchor failed.", ex));
      }
    }

    private void OnSelectControlClick(object sender, RoutedEventArgs e)
    {
      if (!(this.anchorDetailsDataGrid.SelectedItem is ExtensibilityUIBinding))
        return;
      FlexBaseAnchor anchorName = (this.anchorDetailsDataGrid.SelectedItem as ExtensibilityUIBinding).AnchorName;
      if (anchorName == null)
        return;
      int num = (int) new AnchorEditorFrame(anchorName).ShowDialog();
    }

    private void OnAfterSavedChangeTransaction(SavedChangeTransactionEventArgs e)
    {
      try
      {
        int num = Utilities.GetFloorplanObject(this.m_ParentExplorer.SelectedObject).UniqueId == e.UIComponentId ? 1 : 0;
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Refreshing of Extensibility Explorer failed.", ex));
      }
    }

    private void LoadPublishedAndSavedCT(List<ChangeTransactionType> supportedChangeTransactionTypes)
    {
      if (!(this.anchorDetailsDataGrid.SelectedItem is ExtensibilityUIBinding))
        return;
      List<ModeledChangeTransactionInfo> publishedCTList;
      List<ModeledChangeTransactionInfo> savedCTList;
      XRepositoryProxy.Instance.GetChangeTransactionForAnchor((this.anchorDetailsDataGrid.SelectedItem as ExtensibilityUIBinding).AnchorName.XRepPath, out publishedCTList, out savedCTList);
      TreeViewItem selectedItem = this.m_treeView.SelectedItem as TreeViewItem;
      selectedItem.Items.Clear();
      if (publishedCTList != null && publishedCTList.Count > 0 && (supportedChangeTransactionTypes != null && supportedChangeTransactionTypes.Count > 0))
      {
        foreach (ModeledChangeTransactionInfo changeTransactionInfo in publishedCTList)
        {
          XElement xelement = XElement.Parse(changeTransactionInfo.Content);
          string publishedCTXElementName = xelement.Attribute((XName) "type").Value;
          List<ChangeTransactionType> changeTransactionTypes = supportedChangeTransactionTypes.FindAll((Predicate<ChangeTransactionType>) (type => type.TypeName == publishedCTXElementName));
          if (changeTransactionTypes != null)
          {
            if (changeTransactionTypes.Count > 1)
            {
              foreach (ChangeTransactionType changeTransactionType1 in changeTransactionTypes)
              {
                ChangeTransactionType changeTransactionType = changeTransactionType1;
                if (!string.IsNullOrEmpty(changeTransactionType.SubTypeName) && xelement.Descendants().Where<XElement>((Func<XElement, bool>) (e => e.Name.LocalName == changeTransactionType.SubTypeName)).ToList<XElement>().Count > 0)
                {
                  TreeViewItem treeViewItem = new TreeViewItem();
                  treeViewItem.Header = (object) this.RenderHeader(this.GREEN_ICON, (object) changeTransactionInfo, false);
                  selectedItem.Items.Add((object) treeViewItem);
                  selectedItem.IsExpanded = true;
                  break;
                }
              }
            }
            else if (changeTransactionTypes.Count == 1 && (string.IsNullOrEmpty(changeTransactionTypes[0].SubTypeName) || xelement.Descendants().Where<XElement>((Func<XElement, bool>) (e => e.Name.LocalName == changeTransactionTypes[0].SubTypeName)).ToList<XElement>().Count > 0))
            {
              TreeViewItem treeViewItem = new TreeViewItem();
              treeViewItem.Header = (object) this.RenderHeader(this.GREEN_ICON, (object) changeTransactionInfo, false);
              selectedItem.Items.Add((object) treeViewItem);
              selectedItem.IsExpanded = true;
            }
          }
        }
      }
      if (savedCTList == null || savedCTList.Count <= 0 || (supportedChangeTransactionTypes == null || supportedChangeTransactionTypes.Count <= 0))
        return;
      foreach (ModeledChangeTransactionInfo changeTransactionInfo in savedCTList)
      {
        XElement xelement = XElement.Parse(changeTransactionInfo.Content);
        string savedCTXElementName = xelement.Attribute((XName) "type").Value;
        List<ChangeTransactionType> changeTransactionTypes = supportedChangeTransactionTypes.FindAll((Predicate<ChangeTransactionType>) (type => type.TypeName == savedCTXElementName));
        if (changeTransactionTypes != null)
        {
          if (changeTransactionTypes.Count > 1)
          {
            foreach (ChangeTransactionType changeTransactionType1 in changeTransactionTypes)
            {
              ChangeTransactionType changeTransactionType = changeTransactionType1;
              if (!string.IsNullOrEmpty(changeTransactionType.SubTypeName) && xelement.Descendants().Where<XElement>((Func<XElement, bool>) (e => e.Name.LocalName == changeTransactionType.SubTypeName)).ToList<XElement>().Count > 0)
              {
                TreeViewItem treeViewItem = new TreeViewItem();
                treeViewItem.Header = (object) this.RenderHeader(this.YELLOW_ICON, (object) changeTransactionInfo, false);
                selectedItem.Items.Add((object) treeViewItem);
                selectedItem.IsExpanded = true;
                break;
              }
            }
          }
          else if (changeTransactionTypes.Count == 1 && (string.IsNullOrEmpty(changeTransactionTypes[0].SubTypeName) || xelement.Descendants().Where<XElement>((Func<XElement, bool>) (e => e.Name.LocalName == changeTransactionTypes[0].SubTypeName)).ToList<XElement>().Count > 0))
          {
            TreeViewItem treeViewItem = new TreeViewItem();
            treeViewItem.Header = (object) this.RenderHeader(this.YELLOW_ICON, (object) changeTransactionInfo, false);
            selectedItem.Items.Add((object) treeViewItem);
            selectedItem.IsExpanded = true;
          }
        }
      }
    }

    private void LoadUnsavedCT(List<ChangeTransactionType> supportedChangeTransactionTypes)
    {
      if (!(this.anchorDetailsDataGrid.SelectedItem is ExtensibilityUIBinding))
        return;
      FlexBaseAnchor anchorName = (this.anchorDetailsDataGrid.SelectedItem as ExtensibilityUIBinding).AnchorName;
      IFloorplan floorplanObject = Utilities.GetFloorplanObject(this.m_ParentExplorer.SelectedObject);
      UXComponent oberonModel = floorplanObject.GetOberonModel() as UXComponent;
      List<ModeledChangeTransactionInfo> changeTransactions = ChangeTransactionManager.Instance.GetUnsavedChangeTransactions(floorplanObject.UniqueId, oberonModel, anchorName.XRepPath);
      TreeViewItem selectedItem = this.m_treeView.SelectedItem as TreeViewItem;
      if (changeTransactions == null || changeTransactions.Count <= 0 || (supportedChangeTransactionTypes == null || supportedChangeTransactionTypes.Count <= 0))
        return;
      foreach (ModeledChangeTransactionInfo changeTransactionInfo in changeTransactions)
      {
        XElement xelement = XElement.Parse(changeTransactionInfo.Content);
        string unSavedCTXElementName = xelement.Attribute((XName) "type").Value;
        List<ChangeTransactionType> changeTransactionTypes = supportedChangeTransactionTypes.FindAll((Predicate<ChangeTransactionType>) (type => type.TypeName == unSavedCTXElementName));
        if (changeTransactionTypes != null)
        {
          if (changeTransactionTypes.Count > 1)
          {
            foreach (ChangeTransactionType changeTransactionType1 in changeTransactionTypes)
            {
              ChangeTransactionType changeTransactionType = changeTransactionType1;
              if (!string.IsNullOrEmpty(changeTransactionType.SubTypeName) && xelement.Descendants().Where<XElement>((Func<XElement, bool>) (e => e.Name.LocalName == changeTransactionType.SubTypeName)).ToList<XElement>().Count > 0)
              {
                TreeViewItem treeViewItem = new TreeViewItem();
                treeViewItem.Header = (object) this.RenderHeader(this.RED_ICON, (object) changeTransactionInfo, true);
                selectedItem.Items.Add((object) treeViewItem);
                selectedItem.IsExpanded = true;
                break;
              }
            }
          }
          else if (changeTransactionTypes.Count == 1 && (string.IsNullOrEmpty(changeTransactionTypes[0].SubTypeName) || xelement.Descendants().Where<XElement>((Func<XElement, bool>) (e => e.Name.LocalName == changeTransactionTypes[0].SubTypeName)).ToList<XElement>().Count > 0))
          {
            TreeViewItem treeViewItem = new TreeViewItem();
            treeViewItem.Header = (object) this.RenderHeader(this.RED_ICON, (object) changeTransactionInfo, true);
            selectedItem.Items.Add((object) treeViewItem);
            selectedItem.IsExpanded = true;
          }
        }
      }
    }

    private bool IsChangeTransactionCreationAllowed(IFloorplan component)
    {
      return LoginManager.Instance.DTMode == DTModes.Partner && component.IsDisplayOnly;
    }

    private void OnTreeViewBeforeExpand(object sender, RoutedEventArgs e)
    {
      TreeViewItem treeViewItem = sender as TreeViewItem;
      Cursor cursor = this.Cursor;
      if (treeViewItem == null)
        return;
      this.Cursor = Cursors.Wait;
      this.SetSelectedItem((ItemsControl) this.m_treeView, (object) treeViewItem);
      List<ChangeTransactionType> transactionTypes = ((Utilities.GetVisualChild<StackPanel>((Visual) treeViewItem).Children[0] as System.Windows.Controls.Button).Tag as ChangeTransactionInfo).SupportedChangeTransactionTypes;
      if (transactionTypes != null && transactionTypes.Count > 0)
      {
        this.LoadPublishedAndSavedCT(transactionTypes);
        this.LoadUnsavedCT(transactionTypes);
      }
      this.Cursor = cursor;
    }

    private void DelIcon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      try
      {
        System.Windows.Controls.Image image = sender as System.Windows.Controls.Image;
        object selectedItem = this.m_treeView.SelectedItem;
        if (image == null)
          return;
        StackPanel visualParent = Utilities.FindVisualParent<StackPanel>((UIElement) image);
        System.Windows.Controls.Image child1 = visualParent.Children[0] as System.Windows.Controls.Image;
        TextBlock child2 = visualParent.Children[1] as TextBlock;
        if (child2 == null || !(child2.Tag is ModeledChangeTransactionInfo))
          return;
        if (child1.Tag != null && !child1.Tag.Equals((object) "UnsavedCT"))
        {
          bool isPublishedCTs = !child1.Tag.Equals((object) "SavedNonActivatedCT");
          ModeledChangeTransactionInfo tag = child2.Tag as ModeledChangeTransactionInfo;
          IFloorplan floorplanObject = Utilities.GetFloorplanObject(this.m_ParentExplorer.SelectedObject);
          if (!XRepositoryProxy.Instance.TriggerScopingInCustLayer(this.createCTModel(tag), isPublishedCTs, floorplanObject.UniqueId))
            return;
          this.m_ParentExplorer.RaiseReloadComponentEvent(new ReloadComponentEventArgs()
          {
            MergedComponent = floorplanObject.GetOberonModel() as UXComponent,
            RequiresReload = true,
            HasDirtyChangeTransactions = false
          });
        }
        else
        {
          IFloorplan floorplanObject = Utilities.GetFloorplanObject(this.m_ParentExplorer.SelectedObject);
          UXComponent oberonModel = floorplanObject.GetOberonModel() as UXComponent;
          string chaneTransactionId = XElement.Parse((child2.Tag as ModeledChangeTransactionInfo).Content).Attribute((XName) "id").Value;
          ChangeTransactionManager.Instance.DeleteUnSavedChangeTransaction(floorplanObject.UniqueId, oberonModel, chaneTransactionId);
          this.m_ParentExplorer.RaiseReloadComponentEvent(new ReloadComponentEventArgs()
          {
            MergedComponent = oberonModel,
            RequiresReload = true,
            HasDirtyChangeTransactions = true
          });
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to delete the change transaction.", ex));
      }
    }

    private void ShowXMl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      try
      {
        System.Windows.Controls.Image image = sender as System.Windows.Controls.Image;
        if (image == null || image == null || !(image.Tag is ModeledChangeTransactionInfo))
          return;
        Utilities.ShowXml((image.Tag as ModeledChangeTransactionInfo).Content);
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to show xml.", ex));
      }
    }

    private bool checkForScoping(ModeledChangeTransactionInfo ctInfo)
    {
      XElement xelement = XElement.Parse(ctInfo.Content);
      if (xelement == null || !xelement.Attributes((XName) "type").Any<XAttribute>())
        return false;
      switch (xelement.Attribute((XName) "type").Value)
      {
        case "AddRemoveWocView":
        case "AddRemoveAssignedObject":
          return true;
        default:
          return false;
      }
    }

    private List<ModeledCTScopingInfo> createCTModel(ModeledChangeTransactionInfo ctInfo)
    {
      return new List<ModeledCTScopingInfo>()
      {
        new ModeledCTScopingInfo()
        {
          Path = ctInfo.UniqueId,
          IsScopingRelevant = this.checkForScoping(ctInfo),
          isToBeDeleted = true
        }
      };
    }

    private bool SetSelectedItem(ItemsControl parent, object child)
    {
      if (parent == null || child == null)
        return false;
      TreeViewItem treeViewItem = parent.ItemContainerGenerator.ContainerFromItem(child) as TreeViewItem;
      if (treeViewItem != null)
      {
        treeViewItem.Focus();
        return treeViewItem.IsSelected = true;
      }
      if (parent.Items.Count > 0)
      {
        foreach (object obj in (IEnumerable) parent.Items)
        {
          if (this.SetSelectedItem(parent.ItemContainerGenerator.ContainerFromItem(obj) as ItemsControl, child))
            return true;
        }
      }
      return false;
    }

    private void OpeartionButton_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        if (LoginManager.Instance.DTMode == DTModes.Partner)
        {
          this.jsonClient = LoginManager.Instance.InitJSONClient("/sap/ap/XREP/JSON3", false);
          PDI_LM_PRODUCT_ACTIVATE_LOCK productActivateLock = new PDI_LM_PRODUCT_ACTIVATE_LOCK();
          productActivateLock.Importing = new PDI_LM_PRODUCT_ACTIVATE_LOCK.ImportingType();
          productActivateLock.Importing.IV_USER = LoginManager.Instance.User;
          productActivateLock.Importing.IV_PRODUCT_NAME = LoginManager.Instance.Solution;
          productActivateLock.Importing.IV_CALLER = "A";
          this.jsonClient.callFunctionModule((IRemoteFunction) productActivateLock);
          if (productActivateLock.Exporting.EV_SUCCESS == string.Empty)
            throw new InformationException("Failed to lock the component as solution is being assembled.", productActivateLock.Exporting.ET_MESSAGES);
        }
        System.Windows.Controls.Button button = sender as System.Windows.Controls.Button;
        if (button == null)
          return;
        TreeViewItem visualParent = Utilities.FindVisualParent<TreeViewItem>((UIElement) Utilities.FindVisualParent<StackPanel>((UIElement) button));
        visualParent.IsExpanded = false;
        this.SetSelectedItem((ItemsControl) this.m_treeView, (object) visualParent);
        if (!(this.anchorDetailsDataGrid.SelectedItem is ExtensibilityUIBinding))
          return;
        FlexBaseAnchor anchorName = (this.anchorDetailsDataGrid.SelectedItem as ExtensibilityUIBinding).AnchorName;
        if (button == null || !(button.Tag is ChangeTransactionInfo))
          return;
        object instance = Activator.CreateInstance(Type.GetType((button.Tag as ChangeTransactionInfo).EditorTypeName));
        if (!(instance is BaseChangeTransactionModeler))
          return;
        BaseChangeTransactionModeler transactionModeler = instance as BaseChangeTransactionModeler;
        if (this.m_ParentExplorer.SelectedObject == null)
          return;
        IFloorplan floorplanObject = Utilities.GetFloorplanObject(this.m_ParentExplorer.SelectedObject);
        if (!transactionModeler.CreateChangeTransaction(this.m_ParentExplorer.SelectedObject, floorplanObject, anchorName))
          return;
        this.m_ParentExplorer.RaiseReloadComponentEvent(new ReloadComponentEventArgs()
        {
          MergedComponent = transactionModeler.UIComponent,
          RequiresReload = transactionModeler.RequiresReload,
          HasDirtyChangeTransactions = true
        });
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Failed to create new change transaction.", ex));
      }
    }

    private StackPanel RenderHeader(Bitmap bitmap, object headerContent, bool isUnsavedCT)
    {
      StackPanel stackPanel = new StackPanel();
      stackPanel.Orientation = Orientation.Horizontal;
      stackPanel.HorizontalAlignment = HorizontalAlignment.Stretch;
      if (headerContent is ChangeTransactionInfo)
      {
        System.Windows.Controls.Button button = new System.Windows.Controls.Button();
        button.Margin = new Thickness(0.0, 8.0, 1.0, 8.0);
        button.Background = (System.Windows.Media.Brush) System.Windows.Media.Brushes.White;
        System.Windows.Media.Brush brush = (System.Windows.Media.Brush) new BrushConverter().ConvertFrom((object) "#000000");
        button.Foreground = brush;
        button.FontSize = 11.0;
        button.Height = 25.0;
        button.Width = 300.0;
        button.Tag = (object) (headerContent as ChangeTransactionInfo);
        button.ToolTip = (object) ((ChangeTransactionInfo) headerContent).Name;
        button.Content = (object) ((ChangeTransactionInfo) headerContent).Name;
        button.Click += new RoutedEventHandler(this.OpeartionButton_Click);
        stackPanel.Children.Add((UIElement) button);
      }
      else if (headerContent is ModeledChangeTransactionInfo)
      {
        System.Windows.Controls.Image image1 = new System.Windows.Controls.Image();
        image1.Width = 16.0;
        image1.Height = 16.0;
        image1.Margin = new Thickness(2.0, 1.0, 6.0, 3.0);
        image1.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(bitmap);
        if (bitmap == this.RED_ICON)
        {
          image1.Tag = (object) "UnsavedCT";
          image1.ToolTip = (object) "Unsaved CT";
        }
        else if (bitmap == this.YELLOW_ICON)
        {
          image1.Tag = (object) "SavedNonActivatedCT";
          image1.ToolTip = (object) "Saved CT";
        }
        else
        {
          image1.Tag = (object) string.Empty;
          image1.ToolTip = (object) "Published CT";
        }
        stackPanel.Children.Add((UIElement) image1);
        TextBlock textBlock = new TextBlock();
        textBlock.Foreground = (System.Windows.Media.Brush) new BrushConverter().ConvertFrom((object) "#FF04357B");
        textBlock.VerticalAlignment = VerticalAlignment.Center;
        textBlock.Width = 200.0;
        textBlock.FontSize = 11.0;
        textBlock.Margin = new Thickness(4.0, 1.0, 6.0, 3.0);
        if (((ModeledChangeTransactionInfo) headerContent).UniqueId != null)
        {
          string[] strArray = ((ModeledChangeTransactionInfo) headerContent).UniqueId.Split('/');
          textBlock.Text = strArray[4];
          textBlock.Tag = (object) (headerContent as ModeledChangeTransactionInfo);
          textBlock.ToolTip = (object) ((ModeledChangeTransactionInfo) headerContent).UniqueId;
          stackPanel.Children.Add((UIElement) textBlock);
        }
        System.Windows.Controls.Image image2 = new System.Windows.Controls.Image();
        image2.Width = 12.0;
        image2.Height = 12.0;
        image2.Margin = new Thickness(16.0, 1.0, 6.0, 3.0);
        image2.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.Delete);
        image2.Tag = (object) (headerContent as ModeledChangeTransactionInfo);
        image2.ToolTip = (object) "Delete CT";
        image2.VerticalAlignment = VerticalAlignment.Center;
        image2.MouseLeftButtonDown += new MouseButtonEventHandler(this.DelIcon_MouseLeftButtonDown);
        stackPanel.Children.Add((UIElement) image2);
        System.Windows.Controls.Image image3 = new System.Windows.Controls.Image();
        image3.Width = 12.0;
        image3.Height = 12.0;
        image3.Margin = new Thickness(2.0, 1.0, 2.0, 3.0);
        image3.Source = (ImageSource) ResourceUtil.ConvertToBitMapImage(SkinResource.XmlFile);
        image3.Tag = (object) (headerContent as ModeledChangeTransactionInfo);
        image3.ToolTip = (object) "Show CT Xml";
        image3.VerticalAlignment = VerticalAlignment.Center;
        image3.MouseLeftButtonDown += new MouseButtonEventHandler(this.ShowXMl_MouseLeftButtonDown);
        stackPanel.Children.Add((UIElement) image3);
      }
      return stackPanel;
    }

    private void EntitySearchTxtBox_TextChanged(object sender, TextChangedEventArgs e)
    {
      try
      {
        if (!string.IsNullOrEmpty(this.EntitySearchTxtBox.Text))
        {
          DataSet dataSet1 = new DataSet();
          DataSet dataSet2 = this.ConvertGenericList(this.m_extensibilityUIBindingCollection);
          ObservableCollection<ExtensibilityUIBinding> observableCollection = new ObservableCollection<ExtensibilityUIBinding>();
          DataView defaultView = dataSet2.Tables[0].DefaultView;
          defaultView.RowFilter = " DisplayText like '" + this.EntitySearchTxtBox.Text + "*'";
          foreach (DataRowView dataRowView in defaultView)
          {
            ExtensibilityUIBinding extensibileUiBinding = this.GetExtensibileUIBinding(dataRowView.Row.ItemArray[2].ToString());
            observableCollection.Add(extensibileUiBinding);
          }
          this.anchorDetailsDataGrid.ItemsSource = (IEnumerable) observableCollection;
        }
        else
        {
          if (!(this.EntitySearchTxtBox.Text == string.Empty))
            return;
          this.anchorDetailsDataGrid.ItemsSource = (IEnumerable) this.m_extensibilityUIBindingCollection;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Filtering failed", ex));
      }
    }

    private void AnchorSearchTxtBox_TextChanged(object sender, TextChangedEventArgs e)
    {
      try
      {
        if (!string.IsNullOrEmpty(this.AnchorSearchTxtBox.Text))
        {
          DataSet dataSet1 = new DataSet();
          DataSet dataSet2 = this.ConvertGenericList(this.m_extensibilityUIBindingCollection);
          ObservableCollection<ExtensibilityUIBinding> observableCollection = new ObservableCollection<ExtensibilityUIBinding>();
          DataView defaultView = dataSet2.Tables[0].DefaultView;
          defaultView.RowFilter = " XRepPathName like '" + this.AnchorSearchTxtBox.Text + "*'";
          foreach (DataRowView dataRowView in defaultView)
          {
            ExtensibilityUIBinding extensibileUiBinding = this.GetExtensibileUIBinding(dataRowView.Row.ItemArray[2].ToString());
            observableCollection.Add(extensibileUiBinding);
          }
          this.anchorDetailsDataGrid.ItemsSource = (IEnumerable) observableCollection;
        }
        else
        {
          if (!(this.AnchorSearchTxtBox.Text == string.Empty))
            return;
          this.anchorDetailsDataGrid.ItemsSource = (IEnumerable) this.m_extensibilityUIBindingCollection;
        }
      }
      catch (Exception ex)
      {
        ExceptionManager.GetInstance().HandleException(new UILayerException("Filtering failed", ex));
      }
    }

    private ExtensibilityUIBinding GetExtensibileUIBinding(string text)
    {
      foreach (ExtensibilityUIBinding extensibilityUiBinding in (Collection<ExtensibilityUIBinding>) this.m_extensibilityUIBindingCollection)
      {
        if (extensibilityUiBinding.XRepPathName.Equals(text))
          return extensibilityUiBinding;
      }
      return (ExtensibilityUIBinding) null;
    }

    private DataSet ConvertGenericList(ObservableCollection<ExtensibilityUIBinding> list)
    {
      if (list == null)
        throw new ArgumentNullException("list");
      return new DataSet()
      {
        Tables = {
          this.NewTable(list)
        }
      };
    }

    private DataTable NewTable(ObservableCollection<ExtensibilityUIBinding> list)
    {
      PropertyInfo[] properties = typeof (ExtensibilityUIBinding).GetProperties();
      DataTable dataTable = ExtensibilityUIControl.Table((IEnumerable<ExtensibilityUIBinding>) list, properties);
      IEnumerator<ExtensibilityUIBinding> enumerator = list.GetEnumerator();
      while (enumerator.MoveNext())
        dataTable.Rows.Add(this.NewRow(dataTable.NewRow(), enumerator.Current, properties));
      return dataTable;
    }

    private static DataTable Table(IEnumerable<ExtensibilityUIBinding> list, PropertyInfo[] pi)
    {
      DataTable dataTable = new DataTable();
      foreach (PropertyInfo propertyInfo in pi)
        dataTable.Columns.Add(propertyInfo.Name, propertyInfo.PropertyType);
      return dataTable;
    }

    private DataRow NewRow(DataRow row, ExtensibilityUIBinding listItem, PropertyInfo[] pi)
    {
      foreach (PropertyInfo propertyInfo in pi)
        row[propertyInfo.Name.ToString()] = propertyInfo.GetValue((object) listItem, (object[]) null);
      return row;
    }

    //[DebuggerNonUserCode]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //public void InitializeComponent()
    //{
    //  if (this._contentLoaded)
    //    return;
    //  this._contentLoaded = true;
    //  Application.LoadComponent((object) this, new Uri("/sapLSExtensibilityUI;component/extensibilityuicontrol.xaml", UriKind.Relative));
    //}

    //[EditorBrowsable(EditorBrowsableState.Never)]
    //[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    //[DebuggerNonUserCode]
    //void IComponentConnector.Connect(int connectionId, object target)
    //{
    //  switch (connectionId)
    //  {
    //    case 1:
    //      this.rootGrid = (Grid) target;
    //      break;
    //    case 2:
    //      this.anchorDetailsDataGrid = (System.Windows.Controls.DataGrid) target;
    //      this.anchorDetailsDataGrid.SelectionChanged += new SelectionChangedEventHandler(this.OnAnchorDetailsDataGrid_SelectionChanged);
    //      break;
    //    case 3:
    //      this.IdentifierField = (DataGridTextColumn) target;
    //      break;
    //    case 4:
    //      this.IdentifierFieldColumnHeader = (TextBlock) target;
    //      break;
    //    case 5:
    //      this.EntitySearchTxtBox = (TextBox) target;
    //      this.EntitySearchTxtBox.TextChanged += new TextChangedEventHandler(this.EntitySearchTxtBox_TextChanged);
    //      break;
    //    case 6:
    //      this.EntityColumnHeader = (TextBlock) target;
    //      break;
    //    case 7:
    //      this.AnchorSearchTxtBox = (TextBox) target;
    //      this.AnchorSearchTxtBox.TextChanged += new TextChangedEventHandler(this.AnchorSearchTxtBox_TextChanged);
    //      break;
    //    case 9:
    //      this.navigationTextBlock = (TextBlock) target;
    //      break;
    //    case 10:
    //      this.m_treeView = (TreeView) target;
    //      this.m_treeView.AddHandler(Expander.ExpandedEvent, (Delegate) new RoutedEventHandler(this.OnTreeViewBeforeExpand));
    //      break;
    //    case 11:
    //      this.OperationsPanel = (StackPanel) target;
    //      break;
    //    default:
    //      this._contentLoaded = true;
    //      break;
    //  }
    //}

    [EditorBrowsable(EditorBrowsableState.Never)]
    [DebuggerNonUserCode]
    [GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    void IStyleConnector.Connect(int connectionId, object target)
    {
      if (connectionId != 8)
        return;
      ((SelectorControl) target).Click += new SelectorControl.EventHandler(this.OnSelectControlClick);
    }
  }
}
