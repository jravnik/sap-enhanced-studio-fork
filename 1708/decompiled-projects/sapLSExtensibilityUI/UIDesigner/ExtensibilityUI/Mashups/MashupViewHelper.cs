﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups.MashupViewHelper
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;
using SAP.BYD.LS.UI.Flexibility;
using SAP.BYD.LS.UI.Mashup.Common;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar;
using SAP.BYD.LS.UI.Mashup.Tools.SideCar.ChangeRecord;
using SAP.BYD.LS.UIDesigner.ExtensibilityUI.CTModelers;
using SAP.BYD.LS.UIDesigner.Model.Entities.Extensibility;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups
{
  public class MashupViewHelper
  {
    private AddMashupsBaseControl baseControl;
    private Dictionary<string, string> changeHistory;
    private MashupFlexibilityHandler mashupFlexibilityHandler;
    private List<string> changeIDs;
    private FlexibilityHandler flexHandler;
    private UXComponent uiComponent;
    private string uiComponentPath;
    private FlexBaseAnchorType anchor;
    private UXOutPortType extensionFieldOutPort;

    public MashupViewHelper(AddMashupsBaseControl baseControl, Dictionary<string, string> changeHistory, MashupFlexibilityHandler mashupFlexibilityHandler, List<string> changeIDs, FlexibilityHandler flexHandler, UXComponent uiComponent, string uiComponentPath, FlexBaseAnchorType anchor, UXOutPortType extensionFieldOutPort)
    {
      this.baseControl = baseControl;
      this.changeHistory = changeHistory;
      this.mashupFlexibilityHandler = mashupFlexibilityHandler;
      this.changeIDs = changeIDs;
      this.flexHandler = flexHandler;
      this.uiComponent = uiComponent;
      this.uiComponentPath = uiComponentPath;
      this.anchor = anchor;
      this.extensionFieldOutPort = extensionFieldOutPort;
    }

    public void HandleInsertMashup(MashupViewModel mashupViewModel, List<FlexBaseAnchor> referenceAnchors = null)
    {
      string key = mashupViewModel.PipeID + mashupViewModel.SourceOutPlug;
      if (this.changeHistory.ContainsKey(key))
      {
        this.mashupFlexibilityHandler.RevertChangeTransactionInSession(this.changeHistory[key]);
        this.changeHistory.Remove(key);
        this.InsertMashup(mashupViewModel, referenceAnchors);
      }
      else
        this.InsertMashup(mashupViewModel, referenceAnchors);
    }

    public void InsertMashup(MashupViewModel mashupInsertionInfo, List<FlexBaseAnchor> referencedAnchors = null)
    {
      if (("Web_Widget" == mashupInsertionInfo.MashupCategory || this.extensionFieldOutPort == null && mashupInsertionInfo.MashupCategory == string.Empty && mashupInsertionInfo.ViewStyle == MashupViewStyleType.inScreen.ToString() || string.IsNullOrEmpty(mashupInsertionInfo.MashupCategory) && mashupInsertionInfo.Appearance == MashupSerivceAppearance.ViewSwitch.ToString()) && mashupInsertionInfo.DynamicBindings.Count == 0)
        mashupInsertionInfo.SourceOutPlug = string.Empty;
      if (mashupInsertionInfo.SourceOutPlug == "ExtensionFieldsOutPort" && string.IsNullOrEmpty(mashupInsertionInfo.MashupInPlug))
        mashupInsertionInfo.MashupInPlug = "ExtensionFieldsInPort";
      if (!mashupInsertionInfo.IsDynamic && referencedAnchors != null)
      {
        foreach (FlexBaseAnchor referencedAnchor in referencedAnchors)
        {
          if (referencedAnchor.XRepPath == mashupInsertionInfo.ReferencedAnchorXrepPath)
          {
            string str = AddMashupsBaseControl.InsertMashup(mashupInsertionInfo, this.flexHandler, this.uiComponent, this.uiComponentPath, referencedAnchor.FlexBaseAnchorType);
            if (string.IsNullOrEmpty(str))
              break;
            this.changeIDs.Add(str);
            this.changeHistory.Add(mashupInsertionInfo.PipeID + mashupInsertionInfo.SourceOutPlug, str);
            break;
          }
        }
      }
      else
      {
        string str = AddMashupsBaseControl.InsertMashup(mashupInsertionInfo, this.flexHandler, this.uiComponent, this.uiComponentPath, this.anchor);
        if (string.IsNullOrEmpty(str))
          return;
        this.changeIDs.Add(str);
        this.changeHistory.Add(mashupInsertionInfo.PipeID + mashupInsertionInfo.SourceOutPlug, str);
      }
    }

    public void DynamicParameterChanged(object sender, List<FlexBaseAnchor> referenceAnchors = null, UXComponent uxComponent = null, bool isComponentEmbeded = true, FlexBaseAnchorType stableAnchor = null)
    {
      ComboBox comboBox = sender as ComboBox;
      if (comboBox == null || !(comboBox.SelectedItem is ExtBindingOption))
        return;
      ExtBindingOption selectedItem = (ExtBindingOption) comboBox.SelectedItem;
      string text1 = selectedItem.Text;
      comboBox.Text = text1;
      DynamicParameter dataContext1 = (DynamicParameter) ((FrameworkElement) sender).DataContext;
      bool flag1 = string.IsNullOrEmpty(text1);
      string str = flag1 ? string.Empty : selectedItem.ParameterBind;
      dataContext1.Binding = str;
      MashupViewModel dataContext2 = this.baseControl.DynParamsControl.DataContext as MashupViewModel;
      if (dataContext2 == null)
        return;
      string name = dataContext1.Name;
      if (stableAnchor == null || stableAnchor.type != FlexAnchorEnumType.ViewSwitchNavigationAnchor)
      {
        dataContext2.ResetAppearance(dataContext1, !flag1, uxComponent, isComponentEmbeded, referenceAnchors);
        if (dataContext2.IsDynamic)
        {
          dataContext2.ResetExtensionAppearance();
          ObservableCollection<MashupValueHelpListModel> appearanceOptions = dataContext2.AppearanceOptions;
          string text2 = this.baseControl.Appearances.Text;
          bool flag2 = true;
          int index = -1;
          foreach (MashupValueHelpListModel valueHelpListModel in (Collection<MashupValueHelpListModel>) appearanceOptions)
          {
            if (valueHelpListModel.Value == text2)
            {
              flag2 = false;
              index = appearanceOptions.IndexOf(valueHelpListModel);
              break;
            }
          }
          if (appearanceOptions.Count > 0)
          {
            MashupValueHelpListModel valueHelpListModel = appearanceOptions.FirstOrDefault<MashupValueHelpListModel>();
            if (!flag2)
              valueHelpListModel = appearanceOptions[index];
            else
              index = 0;
            dataContext2.OldAppearanceKey = valueHelpListModel.Key;
            dataContext2.AppearanceKey = valueHelpListModel.Key;
            this.baseControl.Appearances.Text = valueHelpListModel.Value;
            this.baseControl.Appearances.SelectedIndex = index;
          }
        }
      }
      dataContext2.CheckCanBeAdded();
      selectedItem.IsOutput = dataContext1.IsOutput;
      if (string.IsNullOrEmpty(text1) && dataContext2.DynamicBindings.ContainsKey(name))
        dataContext2.DynamicBindings.Remove(name);
      else
        dataContext2.DynamicBindings[name] = selectedItem;
      if (!dataContext2.IsVisible || !dataContext2.NotExisting)
        return;
      this.HandleInsertMashup(dataContext2, referenceAnchors);
    }

    public void FullColumn_Checked(object sender, List<FlexBaseAnchor> referenceAnchors = null)
    {
      CheckBox checkBox = sender as CheckBox;
      if (checkBox == null)
        return;
      MashupViewModel dataContext = checkBox.DataContext as MashupViewModel;
      if (dataContext == null || !dataContext.IsVisible || !dataContext.NotExisting)
        return;
      this.HandleInsertMashup(dataContext, referenceAnchors);
    }

    public void FullColumn_Unchecked(object sender, List<FlexBaseAnchor> referenceAnchors = null)
    {
      CheckBox checkBox = sender as CheckBox;
      if (checkBox == null)
        return;
      MashupViewModel dataContext = checkBox.DataContext as MashupViewModel;
      if (dataContext == null || !dataContext.NotExisting || !dataContext.IsVisible)
        return;
      this.HandleInsertMashup(dataContext, referenceAnchors);
    }

    public void VisibilityCheckbox_Checked(object sender, List<FlexBaseAnchor> referenceAnchors = null)
    {
      CheckBox checkBox = sender as CheckBox;
      if (checkBox == null)
        return;
      MashupViewModel dataContext = checkBox.DataContext as MashupViewModel;
      if (dataContext == null)
        return;
      this.InsertMashup(dataContext, referenceAnchors);
    }

    public void VisibilityCheckbox_Unchecked(object sender)
    {
      CheckBox checkBox = sender as CheckBox;
      if (checkBox == null)
        return;
      MashupViewModel dataContext = checkBox.DataContext as MashupViewModel;
      if (dataContext == null || !dataContext.NotExisting)
        return;
      string key = dataContext.PipeID + dataContext.SourceOutPlug;
      if (!this.changeHistory.ContainsKey(key))
        return;
      this.mashupFlexibilityHandler.RevertChangeTransactionInSession(this.changeHistory[key]);
      this.changeHistory.Remove(key);
    }

    public void VisibilityBindingChanged(object sender, List<FlexBaseAnchor> referenceAnchors = null)
    {
      ComboBox comboBox = sender as ComboBox;
      if (comboBox == null || comboBox.SelectedItem == null)
        return;
      ExtBindingOption selectedItem = comboBox.SelectedItem as ExtBindingOption;
      if (selectedItem == null)
        return;
      comboBox.Text = selectedItem.Text;
      MashupViewModel dataContext = comboBox.DataContext as MashupViewModel;
      dataContext.VisibilityBindingExpression = selectedItem.ParameterBind;
      if (!dataContext.IsVisible || !dataContext.NotExisting)
        return;
      this.HandleInsertMashup(dataContext, referenceAnchors);
    }
  }
}
