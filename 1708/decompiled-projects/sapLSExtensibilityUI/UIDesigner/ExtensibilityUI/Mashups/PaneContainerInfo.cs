﻿// Decompiled with JetBrains decompiler
// Type: SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups.PaneContainerInfo
// Assembly: sapLSExtensibilityUI, Version=25.0.555.1045, Culture=neutral, PublicKeyToken=null
// MVID: D1566CF9-53A2-49E2-A42A-A71974EDFADC
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\sapLSExtensibilityUI.dll

using SAP.BYD.LS.UI.Core.Model;

namespace SAP.BYD.LS.UIDesigner.ExtensibilityUI.Mashups
{
  public class PaneContainerInfo
  {
    public PaneContainerType PaneContainer { get; set; }

    public string ParentID { get; set; }
  }
}
