﻿// Decompiled with JetBrains decompiler
// Type: AdvancedDataGridView.TreeGridView
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Windows.Forms.VisualStyles;

namespace AdvancedDataGridView
{
  [Designer(typeof (ControlDesigner))]
  [ComplexBindingProperties]
  [Docking(DockingBehavior.Ask)]
  [DesignerCategory("code")]
  public class TreeGridView : DataGridView
  {
    private bool _showLines = true;
    private TreeGridNode _root;
    private TreeGridColumn _expandableColumn;
    internal ImageList _imageList;
    private bool _inExpandCollapse;
    internal bool _inExpandCollapseMouseCapture;
    private Control hideScrollBarControl;
    private bool _virtualNodes;
    internal VisualStyleRenderer rOpen;
    internal VisualStyleRenderer rClosed;

    public TreeGridNode RootNode
    {
      get
      {
        return this._root;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Browsable(false)]
    public new object DataSource
    {
      get
      {
        return (object) null;
      }
      set
      {
        throw new NotSupportedException("The TreeGridView does not support databinding");
      }
    }

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public object DataMember
    {
      get
      {
        return (object) null;
      }
      set
      {
        throw new NotSupportedException("The TreeGridView does not support databinding");
      }
    }

    [Browsable(false)]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public new DataGridViewRowCollection Rows
    {
      get
      {
        return base.Rows;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public new bool VirtualMode
    {
      get
      {
        return false;
      }
      set
      {
        throw new NotSupportedException("The TreeGridView does not support virtual mode");
      }
    }

    [EditorBrowsable(EditorBrowsableState.Never)]
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public new DataGridViewRow RowTemplate
    {
      get
      {
        return base.RowTemplate;
      }
      set
      {
        base.RowTemplate = value;
      }
    }

    [Editor(typeof (CollectionEditor), typeof (UITypeEditor))]
    [Description("The collection of root nodes in the treelist.")]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    [Category("Data")]
    public TreeGridNodeCollection Nodes
    {
      get
      {
        return this._root.ChildNodes;
      }
    }

    public TreeGridNode CurrentRow
    {
      get
      {
        return base.CurrentRow as TreeGridNode;
      }
    }

    [Description("Causes nodes to always show as expandable. Use the NodeExpanding event to add nodes.")]
    [DefaultValue(false)]
    public bool VirtualNodes
    {
      get
      {
        return this._virtualNodes;
      }
      set
      {
        this._virtualNodes = value;
      }
    }

    public TreeGridNode CurrentNode
    {
      get
      {
        return this.CurrentRow;
      }
    }

    [DefaultValue(true)]
    public bool ShowLines
    {
      get
      {
        return this._showLines;
      }
      set
      {
        if (value == this._showLines)
          return;
        this._showLines = value;
        this.Invalidate();
      }
    }

    public ImageList ImageList
    {
      get
      {
        return this._imageList;
      }
      set
      {
        this._imageList = value;
      }
    }

    public new int RowCount
    {
      get
      {
        return this.Nodes.Count;
      }
      set
      {
        for (int index = 0; index < value; ++index)
          this.Nodes.Add(new TreeGridNode());
      }
    }

    public event ExpandingEventHandler NodeExpanding;

    public event ExpandedEventHandler NodeExpanded;

    public event CollapsingEventHandler NodeCollapsing;

    public event CollapsedEventHandler NodeCollapsed;

    public TreeGridView()
    {
      this.EditMode = DataGridViewEditMode.EditProgrammatically;
      this.RowTemplate = (DataGridViewRow) new TreeGridNode();
      this.AllowUserToAddRows = false;
      this.AllowUserToDeleteRows = false;
      this._root = new TreeGridNode(this);
      this._root.IsRoot = true;
      base.Rows.CollectionChanged += (CollectionChangeEventHandler) ((sender, e) => {});
      if (!Application.RenderWithVisualStyles)
        return;
      this.rOpen = new VisualStyleRenderer(VisualStyleElement.TreeView.Glyph.Opened);
      this.rClosed = new VisualStyleRenderer(VisualStyleElement.TreeView.Glyph.Closed);
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);
      if (e.Handled)
        return;
      if (e.KeyCode == Keys.F2 && this.CurrentCellAddress.X > -1 && this.CurrentCellAddress.Y > -1)
      {
        if (!this.CurrentCell.Displayed)
          this.FirstDisplayedScrollingRowIndex = this.CurrentCellAddress.Y;
        this.SelectionMode = DataGridViewSelectionMode.CellSelect;
        this.BeginEdit(true);
      }
      else
      {
        if (e.KeyCode != Keys.Return || this.IsCurrentCellInEditMode)
          return;
        this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        this.CurrentCell.OwningRow.Selected = true;
      }
    }

    [Description("Returns the TreeGridNode for the given DataGridViewRow")]
    public TreeGridNode GetNodeForRow(DataGridViewRow row)
    {
      return row as TreeGridNode;
    }

    [Description("Returns the TreeGridNode for the given DataGridViewRow")]
    public TreeGridNode GetNodeForRow(int index)
    {
      return this.GetNodeForRow(base.Rows[index]);
    }

    protected override void OnRowsAdded(DataGridViewRowsAddedEventArgs e)
    {
      base.OnRowsAdded(e);
      for (int index = e.RowCount - 1; index >= 0; --index)
      {
        TreeGridNode row = base.Rows[e.RowIndex + index] as TreeGridNode;
        if (row != null)
          row.Sited();
      }
    }

    protected internal void UnSiteAll()
    {
      this.UnSiteNode(this._root);
    }

    protected internal virtual void UnSiteNode(TreeGridNode node)
    {
      if (!node.IsSited && !node.IsRoot)
        return;
      foreach (TreeGridNode childNode in node.ChildNodes)
        this.UnSiteNode(childNode);
      if (node.IsRoot)
        return;
      base.Rows.Remove((DataGridViewRow) node);
      node.UnSited();
    }

    protected internal virtual bool CollapseNode(TreeGridNode node)
    {
      if (!node.IsExpanded)
        return false;
      CollapsingEventArgs e = new CollapsingEventArgs(node);
      this.OnNodeCollapsing(e);
      if (!e.Cancel)
      {
        this.LockVerticalScrollBarUpdate(true);
        this.SuspendLayout();
        this._inExpandCollapse = true;
        node.IsExpanded = false;
        foreach (TreeGridNode childNode in node.ChildNodes)
          this.UnSiteNode(childNode);
        this.OnNodeCollapsed(new CollapsedEventArgs(node));
        this._inExpandCollapse = false;
        this.LockVerticalScrollBarUpdate(false);
        this.ResumeLayout(true);
        this.InvalidateCell(node.Cells[0]);
      }
      return !e.Cancel;
    }

    protected internal virtual void SiteNode(TreeGridNode node)
    {
      node._grid = this;
      TreeGridNode treeGridNode = node.Parent == null || node.Parent.IsRoot ? (node.Index <= 0 ? (TreeGridNode) null : node.Parent.ChildNodes[node.Index - 1]) : (node.Index <= 0 ? node.Parent : node.Parent.ChildNodes[node.Index - 1]);
      int index;
      if (treeGridNode != null)
      {
        while (treeGridNode.Level >= node.Level && treeGridNode.RowIndex < base.Rows.Count - 1)
          treeGridNode = base.Rows[treeGridNode.RowIndex + 1] as TreeGridNode;
        index = treeGridNode != node.Parent ? (treeGridNode.Level >= node.Level ? treeGridNode.RowIndex + 1 : treeGridNode.RowIndex) : treeGridNode.RowIndex + 1;
      }
      else
        index = 0;
      this.SiteNode(node, index);
      if (!node.IsExpanded)
        return;
      foreach (TreeGridNode childNode in node.ChildNodes)
        this.SiteNode(childNode);
    }

    protected internal virtual void SiteNode(TreeGridNode node, int index)
    {
      if (index < base.Rows.Count)
        base.Rows.Insert(index, (DataGridViewRow) node);
      else
        base.Rows.Add((DataGridViewRow) node);
    }

    protected internal virtual bool ExpandNode(TreeGridNode node)
    {
      if (node.IsExpanded && !this._virtualNodes)
        return false;
      ExpandingEventArgs e = new ExpandingEventArgs(node);
      this.OnNodeExpanding(e);
      if (!e.Cancel)
      {
        this.LockVerticalScrollBarUpdate(true);
        this.SuspendLayout();
        this._inExpandCollapse = true;
        node.IsExpanded = true;
        foreach (TreeGridNode childNode in node.ChildNodes)
          this.SiteNode(childNode);
        this.OnNodeExpanded(new ExpandedEventArgs(node));
        this._inExpandCollapse = false;
        this.LockVerticalScrollBarUpdate(false);
        this.ResumeLayout(true);
        this.InvalidateCell(node.Cells[0]);
      }
      return !e.Cancel;
    }

    public void ExpandAll()
    {
      this.ExpandNode(this._root);
      foreach (TreeGridNode node in this.Nodes)
        this.expandRecursive(node);
    }

    public void CollapseAll()
    {
      this.CollapseNode(this._root.ChildNodes[0]);
    }

    private void expandRecursive(TreeGridNode t)
    {
      this.ExpandNode(t);
      foreach (TreeGridNode childNode in t.ChildNodes)
        this.expandRecursive(childNode);
    }

    protected override void OnMouseUp(MouseEventArgs e)
    {
      base.OnMouseUp(e);
      this._inExpandCollapseMouseCapture = false;
    }

    protected override void OnMouseMove(MouseEventArgs e)
    {
      if (this._inExpandCollapseMouseCapture)
        return;
      base.OnMouseMove(e);
    }

    protected virtual void OnNodeExpanding(ExpandingEventArgs e)
    {
      if (this.NodeExpanding == null)
        return;
      this.NodeExpanding((object) this, e);
    }

    protected virtual void OnNodeExpanded(ExpandedEventArgs e)
    {
      if (this.NodeExpanded == null)
        return;
      this.NodeExpanded((object) this, e);
    }

    protected virtual void OnNodeCollapsing(CollapsingEventArgs e)
    {
      if (this.NodeCollapsing == null)
        return;
      this.NodeCollapsing((object) this, e);
    }

    protected virtual void OnNodeCollapsed(CollapsedEventArgs e)
    {
      if (this.NodeCollapsed == null)
        return;
      this.NodeCollapsed((object) this, e);
    }

    protected override void Dispose(bool disposing)
    {
      base.Dispose(this.Disposing);
      this.UnSiteAll();
    }

    protected override void OnHandleCreated(EventArgs e)
    {
      base.OnHandleCreated(e);
      this.hideScrollBarControl = new Control();
      this.hideScrollBarControl.Visible = false;
      this.hideScrollBarControl.Enabled = false;
      this.hideScrollBarControl.TabStop = false;
      this.Controls.Add(this.hideScrollBarControl);
    }

    protected override void OnRowEnter(DataGridViewCellEventArgs e)
    {
      base.OnRowEnter(e);
      if (this.SelectionMode != DataGridViewSelectionMode.CellSelect && (this.SelectionMode != DataGridViewSelectionMode.FullRowSelect || base.Rows[e.RowIndex].Selected))
        return;
      this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      base.Rows[e.RowIndex].Selected = true;
    }

    private void LockVerticalScrollBarUpdate(bool lockUpdate)
    {
      if (this._inExpandCollapse)
        return;
      if (lockUpdate)
        this.VerticalScrollBar.Parent = this.hideScrollBarControl;
      else
        this.VerticalScrollBar.Parent = (Control) this;
    }

    protected override void OnColumnAdded(DataGridViewColumnEventArgs e)
    {
      if (typeof (TreeGridColumn).IsAssignableFrom(e.Column.GetType()) && this._expandableColumn == null)
        this._expandableColumn = (TreeGridColumn) e.Column;
      e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
      base.OnColumnAdded(e);
    }

    private static class Win32Helper
    {
      public const int WM_SYSKEYDOWN = 260;
      public const int WM_KEYDOWN = 256;
      public const int WM_SETREDRAW = 11;

      [DllImport("USER32.DLL", CharSet = CharSet.Auto)]
      public static extern IntPtr SendMessage(HandleRef hWnd, int msg, IntPtr wParam, IntPtr lParam);

      [DllImport("USER32.DLL", CharSet = CharSet.Auto)]
      public static extern IntPtr SendMessage(HandleRef hWnd, int msg, int wParam, int lParam);

      [DllImport("USER32.DLL", CharSet = CharSet.Auto)]
      public static extern bool PostMessage(HandleRef hwnd, int msg, IntPtr wparam, IntPtr lparam);
    }
  }
}
