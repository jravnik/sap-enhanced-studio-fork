﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.StructureAttribute
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [DesignerCategory("code")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlRoot(ElementName = "structureAttribute", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [DebuggerStepThrough]
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [Serializable]
  public class StructureAttribute
  {
    private string nameField;
    private string bindingField;
    private string typeField;
    private string mdrsNameField;
    private string mdrsBindingField;

    [XmlAttribute]
    public string name
    {
      get
      {
        return this.nameField;
      }
      set
      {
        this.nameField = value;
      }
    }

    [XmlAttribute]
    public string binding
    {
      get
      {
        return this.bindingField;
      }
      set
      {
        this.bindingField = value;
      }
    }

    [XmlAttribute]
    public string type
    {
      get
      {
        return this.typeField;
      }
      set
      {
        this.typeField = value;
      }
    }

    [XmlAttribute(Namespace = "http://sap.com/xi/AP/PDI")]
    public string mdrsName
    {
      get
      {
        return this.mdrsNameField;
      }
      set
      {
        this.mdrsNameField = value;
      }
    }

    [XmlAttribute(Namespace = "http://sap.com/xi/AP/PDI")]
    public string mdrsBinding
    {
      get
      {
        return this.mdrsBindingField;
      }
      set
      {
        this.mdrsBindingField = value;
      }
    }
  }
}
