﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusLib.Core.Sadl.ResultSet
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.CopernicusLib.Core.Sadl
{
  [XmlRoot(ElementName = "resultSet", IsNullable = false, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [DebuggerStepThrough]
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/sap.nw.f.sadl")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [Serializable]
  public class ResultSet
  {
    private SAP.Copernicus.CopernicusLib.Core.Sadl.Structure[] structureField;

    [XmlElement("structure")]
    public SAP.Copernicus.CopernicusLib.Core.Sadl.Structure[] Structure
    {
      get
      {
        return this.structureField;
      }
      set
      {
        this.structureField = value;
      }
    }
  }
}
