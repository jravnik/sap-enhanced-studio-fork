﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Resource
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SAP.Copernicus
{
  [CompilerGenerated]
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  public class Resource
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) Resource.resourceMan, (object) null))
          Resource.resourceMan = new ResourceManager("SAP.Copernicus.Resource", typeof (Resource).Assembly);
        return Resource.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return Resource.resourceCulture;
      }
      set
      {
        Resource.resourceCulture = value;
      }
    }

    public static string BackgroundCheckDisabled
    {
      get
      {
        return Resource.ResourceManager.GetString("BackgroundCheckDisabled", Resource.resourceCulture);
      }
    }

    public static string BackgroundCheckEnabled
    {
      get
      {
        return Resource.ResourceManager.GetString("BackgroundCheckEnabled", Resource.resourceCulture);
      }
    }

    public static string BackgroundCheckfailed
    {
      get
      {
        return Resource.ResourceManager.GetString("BackgroundCheckfailed", Resource.resourceCulture);
      }
    }

    public static string BCDeploymentFailed
    {
      get
      {
        return Resource.ResourceManager.GetString("BCDeploymentFailed", Resource.resourceCulture);
      }
    }

    public static string BCDeploymentFinished
    {
      get
      {
        return Resource.ResourceManager.GetString("BCDeploymentFinished", Resource.resourceCulture);
      }
    }

    public static string BCDeploymentStarted
    {
      get
      {
        return Resource.ResourceManager.GetString("BCDeploymentStarted", Resource.resourceCulture);
      }
    }

    public static string BOCheckTitle
    {
      get
      {
        return Resource.ResourceManager.GetString("BOCheckTitle", Resource.resourceCulture);
      }
    }

    public static string BOSaveMissing
    {
      get
      {
        return Resource.ResourceManager.GetString("BOSaveMissing", Resource.resourceCulture);
      }
    }

    public static string CanNotCreateWindow
    {
      get
      {
        return Resource.ResourceManager.GetString("CanNotCreateWindow", Resource.resourceCulture);
      }
    }

    public static string CheckTriggeredInBackground
    {
      get
      {
        return Resource.ResourceManager.GetString("CheckTriggeredInBackground", Resource.resourceCulture);
      }
    }

    public static string CloseCorrection
    {
      get
      {
        return Resource.ResourceManager.GetString("CloseCorrection", Resource.resourceCulture);
      }
    }

    public static string CloseCorrectionFailed
    {
      get
      {
        return Resource.ResourceManager.GetString("CloseCorrectionFailed", Resource.resourceCulture);
      }
    }

    public static string ContactPersonEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString("ContactPersonEmpty", Resource.resourceCulture);
      }
    }

    public static string CopernicusAppExitFilterValueF4Tip
    {
      get
      {
        return Resource.ResourceManager.GetString("CopernicusAppExitFilterValueF4Tip", Resource.resourceCulture);
      }
    }

    public static string CreateScreenShortIdTextBoxWarningLabel
    {
      get
      {
        return Resource.ResourceManager.GetString("CreateScreenShortIdTextBoxWarningLabel", Resource.resourceCulture);
      }
    }

    public static string CustomerID
    {
      get
      {
        return Resource.ResourceManager.GetString("CustomerID", Resource.resourceCulture);
      }
    }

    public static string CustomerName
    {
      get
      {
        return Resource.ResourceManager.GetString("CustomerName", Resource.resourceCulture);
      }
    }

    public static string DeploymentUnitEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString("DeploymentUnitEmpty", Resource.resourceCulture);
      }
    }

    public static string Deprecated
    {
      get
      {
        return Resource.ResourceManager.GetString("Deprecated", Resource.resourceCulture);
      }
    }

    public static string DevPartnerEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString("DevPartnerEmpty", Resource.resourceCulture);
      }
    }

    public static string Dist_Notpossible
    {
      get
      {
        return Resource.ResourceManager.GetString("Dist_Notpossible", Resource.resourceCulture);
      }
    }

    public static string Dist_Success
    {
      get
      {
        return Resource.ResourceManager.GetString("Dist_Success", Resource.resourceCulture);
      }
    }

    public static string DumpAnalysisLogonRequired
    {
      get
      {
        return Resource.ResourceManager.GetString("DumpAnalysisLogonRequired", Resource.resourceCulture);
      }
    }

    public static string EmailEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString("EmailEmpty", Resource.resourceCulture);
      }
    }

    public static string EmailValidationFailed
    {
      get
      {
        return Resource.ResourceManager.GetString("EmailValidationFailed", Resource.resourceCulture);
      }
    }

    public static string InvalidProjectsLocation_0_reason_1
    {
      get
      {
        return Resource.ResourceManager.GetString("InvalidProjectsLocation_0_reason_1", Resource.resourceCulture);
      }
    }

    public static string InvalidToolsOptions
    {
      get
      {
        return Resource.ResourceManager.GetString("InvalidToolsOptions", Resource.resourceCulture);
      }
    }

    public static string LogonDialog_Error_Auth
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonDialog_Error_Auth", Resource.resourceCulture);
      }
    }

    public static string LogonDialog_Error_Caption
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonDialog_Error_Caption", Resource.resourceCulture);
      }
    }

    public static string LogonDialog_Error_Fatal
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonDialog_Error_Fatal", Resource.resourceCulture);
      }
    }

    public static string LogonDialog_Error_Other
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonDialog_Error_Other", Resource.resourceCulture);
      }
    }

    public static string LogonForm_Btn_Cancel
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonForm_Btn_Cancel", Resource.resourceCulture);
      }
    }

    public static string LogonForm_Btn_Ok
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonForm_Btn_Ok", Resource.resourceCulture);
      }
    }

    public static string LogonForm_Header
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonForm_Header", Resource.resourceCulture);
      }
    }

    public static string LogonForm_Label_Client
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonForm_Label_Client", Resource.resourceCulture);
      }
    }

    public static string LogonForm_Label_Pwd
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonForm_Label_Pwd", Resource.resourceCulture);
      }
    }

    public static string LogonForm_Label_System
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonForm_Label_System", Resource.resourceCulture);
      }
    }

    public static string LogonForm_Label_User
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonForm_Label_User", Resource.resourceCulture);
      }
    }

    public static string LogonForm_Title
    {
      get
      {
        return Resource.ResourceManager.GetString("LogonForm_Title", Resource.resourceCulture);
      }
    }

    public static string MCSDeployToolTip
    {
      get
      {
        return Resource.ResourceManager.GetString("MCSDeployToolTip", Resource.resourceCulture);
      }
    }

    public static string MCSLoadToolTip
    {
      get
      {
        return Resource.ResourceManager.GetString("MCSLoadToolTip", Resource.resourceCulture);
      }
    }

    public static string MessagePrefixActivate
    {
      get
      {
        return Resource.ResourceManager.GetString("MessagePrefixActivate", Resource.resourceCulture);
      }
    }

    public static string MessagePrefixCheck
    {
      get
      {
        return Resource.ResourceManager.GetString("MessagePrefixCheck", Resource.resourceCulture);
      }
    }

    public static string MessagePrefixClean
    {
      get
      {
        return Resource.ResourceManager.GetString("MessagePrefixClean", Resource.resourceCulture);
      }
    }

    public static string MessagePrefixDelete
    {
      get
      {
        return Resource.ResourceManager.GetString("MessagePrefixDelete", Resource.resourceCulture);
      }
    }

    public static string MessageTimeOut
    {
      get
      {
        return Resource.ResourceManager.GetString("MessageTimeOut", Resource.resourceCulture);
      }
    }

    public static string MessageTimeOutActivate
    {
      get
      {
        return Resource.ResourceManager.GetString("MessageTimeOutActivate", Resource.resourceCulture);
      }
    }

    public static string MsgAllreadyLockedOn
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgAllreadyLockedOn", Resource.resourceCulture);
      }
    }

    public static string MsgBrowserLogonToolTip
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgBrowserLogonToolTip", Resource.resourceCulture);
      }
    }

    public static string MsgBYD
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgBYD", Resource.resourceCulture);
      }
    }

    public static string MsgCancel
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgCancel", Resource.resourceCulture);
      }
    }

    public static string MsgCancelSolutionDeletion
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgCancelSolutionDeletion", Resource.resourceCulture);
      }
    }

    public static string MsgChangeInvalidatesEntries
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgChangeInvalidatesEntries", Resource.resourceCulture);
      }
    }

    public static string MsgConenctionHost
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgConenctionHost", Resource.resourceCulture);
      }
    }

    public static string MsgConnectionName
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgConnectionName", Resource.resourceCulture);
      }
    }

    public static string MsgConnectionNameNotUnique
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgConnectionNameNotUnique", Resource.resourceCulture);
      }
    }

    public static string MsgDisableKeyUser
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgDisableKeyUser", Resource.resourceCulture);
      }
    }

    public static string MsgEnableKeyUser
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgEnableKeyUser", Resource.resourceCulture);
      }
    }

    public static string MsgFailedBCNameBO
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgFailedBCNameBO", Resource.resourceCulture);
      }
    }

    public static string MsgInvalidBCSetAssignmentEpilog
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgInvalidBCSetAssignmentEpilog", Resource.resourceCulture);
      }
    }

    public static string MsgInvalidBCSetAssignmentProlog
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgInvalidBCSetAssignmentProlog", Resource.resourceCulture);
      }
    }

    public static string MsgInvalidBCSetAssignmentTitle
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgInvalidBCSetAssignmentTitle", Resource.resourceCulture);
      }
    }

    public static string MsgInvalidConnectionName
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgInvalidConnectionName", Resource.resourceCulture);
      }
    }

    public static string MsgInvalidNameChars
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgInvalidNameChars", Resource.resourceCulture);
      }
    }

    public static string MsgInvalidNameCharsUpper
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgInvalidNameCharsUpper", Resource.resourceCulture);
      }
    }

    public static string MsgMissingAuthorization
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgMissingAuthorization", Resource.resourceCulture);
      }
    }

    public static string MsgNameFirstChar
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgNameFirstChar", Resource.resourceCulture);
      }
    }

    public static string MsgPasswordToolTip
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgPasswordToolTip", Resource.resourceCulture);
      }
    }

    public static string MsgScalableNotAllowed
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgScalableNotAllowed", Resource.resourceCulture);
      }
    }

    public static string MsgSystemToolTip
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgSystemToolTip", Resource.resourceCulture);
      }
    }

    public static string MsgWebException
    {
      get
      {
        return Resource.ResourceManager.GetString("MsgWebException", Resource.resourceCulture);
      }
    }

    public static string NumberTextBox_Validation_Caption
    {
      get
      {
        return Resource.ResourceManager.GetString("NumberTextBox_Validation_Caption", Resource.resourceCulture);
      }
    }

    public static string NumberTextBox_Validation_Error
    {
      get
      {
        return Resource.ResourceManager.GetString("NumberTextBox_Validation_Error", Resource.resourceCulture);
      }
    }

    public static string OpenCorrection
    {
      get
      {
        return Resource.ResourceManager.GetString("OpenCorrection", Resource.resourceCulture);
      }
    }

    public static string OpenCorrectionFailed
    {
      get
      {
        return Resource.ResourceManager.GetString("OpenCorrectionFailed", Resource.resourceCulture);
      }
    }

    public static string OutputWindowPaneTitleSolution
    {
      get
      {
        return Resource.ResourceManager.GetString("OutputWindowPaneTitleSolution", Resource.resourceCulture);
      }
    }

    public static string QBODLMigrationCannotRevert
    {
      get
      {
        return Resource.ResourceManager.GetString("QBODLMigrationCannotRevert", Resource.resourceCulture);
      }
    }

    public static Icon SAPBusinessByDesignStudioIcon
    {
      get
      {
        return (Icon) Resource.ResourceManager.GetObject("SAPBusinessByDesignStudioIcon", Resource.resourceCulture);
      }
    }

    public static string ScriptFileReverFailed
    {
      get
      {
        return Resource.ResourceManager.GetString("ScriptFileReverFailed", Resource.resourceCulture);
      }
    }

    public static string ScriptFileRevertSuccessful
    {
      get
      {
        return Resource.ResourceManager.GetString("ScriptFileRevertSuccessful", Resource.resourceCulture);
      }
    }

    public static string Solution
    {
      get
      {
        return Resource.ResourceManager.GetString("Solution", Resource.resourceCulture);
      }
    }

    public static string SolutioNameEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString("SolutioNameEmpty", Resource.resourceCulture);
      }
    }

    public static string SolutionCtxMenuOpen
    {
      get
      {
        return Resource.ResourceManager.GetString("SolutionCtxMenuOpen", Resource.resourceCulture);
      }
    }

    public static string SolutionCtxMenuSolutionProperties
    {
      get
      {
        return Resource.ResourceManager.GetString("SolutionCtxMenuSolutionProperties", Resource.resourceCulture);
      }
    }

    public static string SolutionDescriptionEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString("SolutionDescriptionEmpty", Resource.resourceCulture);
      }
    }

    public static string SolutionDetailedDescriptionEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString("SolutionDetailedDescriptionEmpty", Resource.resourceCulture);
      }
    }

    public static string SolutionNameInvalid
    {
      get
      {
        return Resource.ResourceManager.GetString("SolutionNameInvalid", Resource.resourceCulture);
      }
    }

    public static string SolutionPropertyTitle
    {
      get
      {
        return Resource.ResourceManager.GetString("SolutionPropertyTitle", Resource.resourceCulture);
      }
    }

    public static string SolutionTypeEmpty
    {
      get
      {
        return Resource.ResourceManager.GetString("SolutionTypeEmpty", Resource.resourceCulture);
      }
    }

    public static string TTDelPending
    {
      get
      {
        return Resource.ResourceManager.GetString("TTDelPending", Resource.resourceCulture);
      }
    }

    public static string TTGlobalSol
    {
      get
      {
        return Resource.ResourceManager.GetString("TTGlobalSol", Resource.resourceCulture);
      }
    }

    public static string TTPatchSolution
    {
      get
      {
        return Resource.ResourceManager.GetString("TTPatchSolution", Resource.resourceCulture);
      }
    }

    public static string TTSandboxSol
    {
      get
      {
        return Resource.ResourceManager.GetString("TTSandboxSol", Resource.resourceCulture);
      }
    }

    public static string TTTemplateSolution
    {
      get
      {
        return Resource.ResourceManager.GetString("TTTemplateSolution", Resource.resourceCulture);
      }
    }

    public static string ValueExceedLimit
    {
      get
      {
        return Resource.ResourceManager.GetString("ValueExceedLimit", Resource.resourceCulture);
      }
    }

    public static string Version
    {
      get
      {
        return Resource.ResourceManager.GetString("Version", Resource.resourceCulture);
      }
    }

    public static string WaitCaption
    {
      get
      {
        return Resource.ResourceManager.GetString("WaitCaption", Resource.resourceCulture);
      }
    }

    public static string WaitDeleteSolution
    {
      get
      {
        return Resource.ResourceManager.GetString("WaitDeleteSolution", Resource.resourceCulture);
      }
    }

    public static string WaitScriptCreate
    {
      get
      {
        return Resource.ResourceManager.GetString("WaitScriptCreate", Resource.resourceCulture);
      }
    }

    public static string WaitScriptCreateCanceled
    {
      get
      {
        return Resource.ResourceManager.GetString("WaitScriptCreateCanceled", Resource.resourceCulture);
      }
    }

    public static string WaitScriptCreateFailed
    {
      get
      {
        return Resource.ResourceManager.GetString("WaitScriptCreateFailed", Resource.resourceCulture);
      }
    }

    public static string WaitScriptCreateSuccess
    {
      get
      {
        return Resource.ResourceManager.GetString("WaitScriptCreateSuccess", Resource.resourceCulture);
      }
    }

    public static Bitmap Warning32x32
    {
      get
      {
        return (Bitmap) Resource.ResourceManager.GetObject("Warning32x32", Resource.resourceCulture);
      }
    }

    internal Resource()
    {
    }
  }
}
