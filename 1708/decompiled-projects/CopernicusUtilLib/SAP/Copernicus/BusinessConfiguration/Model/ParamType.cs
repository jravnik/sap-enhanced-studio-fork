﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.ParamType
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model
{
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCSetDefinition")]
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [Serializable]
  public class ParamType
  {
    private string parameterIDField;
    private string parameterDescriptionField;
    private string parameterTypeField;
    private ValueStructureType[] parameterValueField;
    private bool fineTuningAddField;
    private bool fineTuningDelField;

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ParameterID
    {
      get
      {
        return this.parameterIDField;
      }
      set
      {
        this.parameterIDField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ParameterDescription
    {
      get
      {
        return this.parameterDescriptionField;
      }
      set
      {
        this.parameterDescriptionField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public string ParameterType
    {
      get
      {
        return this.parameterTypeField;
      }
      set
      {
        this.parameterTypeField = value;
      }
    }

    [XmlElement("ParameterValue", Form = XmlSchemaForm.Unqualified)]
    public ValueStructureType[] ParameterValue
    {
      get
      {
        return this.parameterValueField;
      }
      set
      {
        this.parameterValueField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public bool FineTuningAdd
    {
      get
      {
        return this.fineTuningAddField;
      }
      set
      {
        this.fineTuningAddField = value;
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public bool FineTuningDel
    {
      get
      {
        return this.fineTuningDelField;
      }
      set
      {
        this.fineTuningDelField = value;
      }
    }
  }
}
