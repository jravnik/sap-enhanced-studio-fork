﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.BusinessConfiguration.Model.BCO.BCOTypeType
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace SAP.Copernicus.BusinessConfiguration.Model.BCO
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [XmlType(Namespace = "http://sap.com/ByD/PDI/BCODefinition")]
  [Serializable]
  public enum BCOTypeType
  {
    [XmlEnum("1")] Codelist,
    [XmlEnum("2")] Assignment,
  }
}
