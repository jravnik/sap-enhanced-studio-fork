﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Project.ContentTypeMapping
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Project
{
  public class ContentTypeMapping
  {
    public static readonly ContentTypeMapping CSD = new ContentTypeMapping("csd", ContentTypeActivationPriority.CSD);
    public static readonly ContentTypeMapping WSID = new ContentTypeMapping("wsid", ContentTypeActivationPriority.WSID);
    public static readonly ContentTypeMapping BUSINESS_OBJECT = new ContentTypeMapping("bo", ContentTypeActivationPriority.BUSINESS_OBJECT);
    public static readonly ContentTypeMapping MSAPPROVALSTEP = new ContentTypeMapping("approvalprocess", ContentTypeActivationPriority.MSAPPROVALSTEP);
    public static readonly ContentTypeMapping APPROVALSTEP = new ContentTypeMapping("approval", ContentTypeActivationPriority.APPROVALSTEP);
    public static readonly ContentTypeMapping A2X = new ContentTypeMapping("webservice", ContentTypeActivationPriority.A2X);
    public static readonly ContentTypeMapping XSODATA = new ContentTypeMapping("xodata", ContentTypeActivationPriority.A2X);
    public static readonly ContentTypeMapping BUSINESSCONFIGURATION = new ContentTypeMapping("bac", ContentTypeActivationPriority.BUSINESSCONFIGURATION);
    public static readonly ContentTypeMapping BCSET = new ContentTypeMapping("bcc", ContentTypeActivationPriority.BCSET);
    public static readonly ContentTypeMapping EXTENSION_ENTITY = new ContentTypeMapping("xbo", ContentTypeActivationPriority.EXTENSION_ENTITY);
    public static readonly ContentTypeMapping BC_WS_TEMPLATE = new ContentTypeMapping("bct", ContentTypeActivationPriority.BC_WS_TEMPLATE);
    public static readonly ContentTypeMapping BCO = new ContentTypeMapping("bco", ContentTypeActivationPriority.CUSTOMER_OBJECT_REFERENCES);
    public static readonly ContentTypeMapping ABSL = new ContentTypeMapping("absl", ContentTypeActivationPriority.ABSL);
    public static readonly ContentTypeMapping CUSTOM_REUSE_LIBRARY = new ContentTypeMapping("library", ContentTypeActivationPriority.CUSTOM_REUSE_LIBRARY);
    public static readonly ContentTypeMapping FORM_EXTENSION_ENTITY = new ContentTypeMapping("xft", ContentTypeActivationPriority.FORM_EXTENSION_ENTITY);
    public static readonly ContentTypeMapping WSAUTH = new ContentTypeMapping("wsauth", ContentTypeActivationPriority.WSAUTH);
    public static readonly ContentTypeMapping FORMS_XDP = new ContentTypeMapping("xdp", ContentTypeActivationPriority.BCSET_CODELIST);
    public static readonly ContentTypeMapping FORMS_FTHD = new ContentTypeMapping("fthd", ContentTypeActivationPriority.BCSET_CODELIST);
    public static readonly ContentTypeMapping FORMS_FTGD = new ContentTypeMapping("ftgd", ContentTypeActivationPriority.BCSET_CODELIST);
    public static readonly ContentTypeMapping FORMS_XSD = new ContentTypeMapping("xsd", ContentTypeActivationPriority.BCSET_CODELIST);
    public static readonly ContentTypeMapping MDRO = new ContentTypeMapping("run", ContentTypeActivationPriority.MDRO);
    public static readonly ContentTypeMapping PROCESS_INTEGRATION = new ContentTypeMapping("pid", ContentTypeActivationPriority.PROCESS_INTEGRATION);
    public static readonly ContentTypeMapping SERVICE_ADAPT_DEF_LANG = new ContentTypeMapping("sadl", ContentTypeActivationPriority.SERVICE_ADAPT_DEF_LANG);
    public static readonly ContentTypeMapping QUERYDEF = new ContentTypeMapping("qry", ContentTypeActivationPriority.QUERYDEF);
    public static readonly ContentTypeMapping DATASOURCE = new ContentTypeMapping("ds", ContentTypeActivationPriority.DATASOURCE);
    public static readonly ContentTypeMapping MDRS_GENERIC_CONTENT_REPORT = new ContentTypeMapping("report", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping MDRS_GENERIC_CONTENT_KF = new ContentTypeMapping("kf", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping MDRS_GENERIC_CONTENT_CDS = new ContentTypeMapping("cds", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping MDRS_GENERIC_CONTENT_JDS = new ContentTypeMapping("jds", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping BO_THING_TYPE = new ContentTypeMapping("bott", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping XREP_CONTENT = new ContentTypeMapping("None", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping UICOMPONENT = new ContentTypeMapping("uicomponent", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping WORKCENTER = new ContentTypeMapping("uiwoc", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping WORKCENTER_VIEW = new ContentTypeMapping("uiwocview", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping X_UICOMPONENT = new ContentTypeMapping("xuicomponent", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping EXTENSION_SCENARIO_EXTENSION = new ContentTypeMapping("xs", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping NODE_EXTENSION_SCENARIO = new ContentTypeMapping("nxs", ContentTypeActivationPriority.MDRS_GENERIC_CONTENT);
    public static readonly ContentTypeMapping APPLICATION_EXIT = new ContentTypeMapping("enht", ContentTypeActivationPriority.APPLICATION_EXIT);
    public static readonly ContentTypeMapping APPLICATION_EXIT_FILTER = new ContentTypeMapping("fltr", ContentTypeActivationPriority.APPLICATION_EXIT_FILTER);
    public static readonly ContentTypeMapping NOTIFICATION_RULE = new ContentTypeMapping("notification", ContentTypeActivationPriority.NOTIFICATION_RULE);
    public static readonly ContentTypeMapping CODELIST = new ContentTypeMapping("codelist", ContentTypeActivationPriority.BCSET_CODELIST);
    public static readonly ContentTypeMapping CUSTOMER_OBJECT_REFERENCES = new ContentTypeMapping("ref", ContentTypeActivationPriority.CUSTOMER_OBJECT_REFERENCES);
    public static readonly ContentTypeMapping XWEBSERVICE = new ContentTypeMapping("xwebservice", ContentTypeActivationPriority.XWEBSERVICE);
    private readonly string SupportedExtension;
    private readonly ContentTypeActivationPriority activationPriority;

    public static ContentTypeMapping[] Values
    {
      get
      {
        return new ContentTypeMapping[28]{ ContentTypeMapping.BUSINESS_OBJECT, ContentTypeMapping.MSAPPROVALSTEP, ContentTypeMapping.A2X, ContentTypeMapping.XSODATA, ContentTypeMapping.BUSINESSCONFIGURATION, ContentTypeMapping.BCSET, ContentTypeMapping.BCO, ContentTypeMapping.BC_WS_TEMPLATE, ContentTypeMapping.EXTENSION_ENTITY, ContentTypeMapping.FORM_EXTENSION_ENTITY, ContentTypeMapping.WSAUTH, ContentTypeMapping.FORMS_XDP, ContentTypeMapping.FORMS_FTHD, ContentTypeMapping.FORMS_FTGD, ContentTypeMapping.FORMS_XSD, ContentTypeMapping.MDRO, ContentTypeMapping.PROCESS_INTEGRATION, ContentTypeMapping.SERVICE_ADAPT_DEF_LANG, ContentTypeMapping.QUERYDEF, ContentTypeMapping.DATASOURCE, ContentTypeMapping.XREP_CONTENT, ContentTypeMapping.WORKCENTER, ContentTypeMapping.WORKCENTER_VIEW, ContentTypeMapping.X_UICOMPONENT, ContentTypeMapping.APPLICATION_EXIT, ContentTypeMapping.APPLICATION_EXIT_FILTER, ContentTypeMapping.CSD, ContentTypeMapping.CUSTOMER_OBJECT_REFERENCES };
      }
    }

    public static IEnumerable<ContentTypeMapping> ValuesEnumerator
    {
      get
      {
        yield return ContentTypeMapping.BUSINESS_OBJECT;
      }
    }

    public ContentTypeActivationPriority ActivationPriority
    {
      get
      {
        return this.activationPriority;
      }
    }

    private ContentTypeMapping(string SupportedExtension, ContentTypeActivationPriority activationPriority)
    {
      this.SupportedExtension = SupportedExtension;
      this.activationPriority = activationPriority;
    }

    public override string ToString()
    {
      switch (this.SupportedExtension)
      {
        case "bo":
          return "BUSINESS_OBJECT";
        case "approvalprocess":
          return "MSAPPROVALSTEP";
        case "approval":
          return "APPROVALSTEP";
        case "webservice":
          return "A2X";
        case "wsid":
          return "WSID";
        case "csd":
          return "CSD";
        case "bac":
          return "BUSINESSCONFIGURATION";
        case "bcc":
          return "BCSET";
        case "bcctax":
          return "BCSET";
        case "bco":
          return "BCO";
        case "bct":
          return "BC_WS_TEMPLATE";
        case "bott":
          return "BO_THING_TYPE";
        case "xbo":
          return "EXTENSION_ENTITY";
        case "xuicomponent":
          return "EXTENSION_ENTITY";
        case "xs":
          return "EXTENSION_ENTITY";
        case "nxs":
          return "SERVICE_NODE_EXTENSION";
        case "xft":
          return "FORM_EXTENSION_ENTITY";
        case "wsauth":
          return "WSAUTH";
        case "xdp":
          return "FORMS";
        case "fthd":
          return "FORMS";
        case "ftgd":
          return "FORMS";
        case "pid":
          return "PROCESS_INTEGRATION";
        case "sadl":
          return "SERVICE_ADAPT_DEF_LANG";
        case "qry":
          return "QUERYDEF";
        case "ds":
          return "DATASOURCE";
        case "report":
          return "MDRS_GENERIC_CONTENT";
        case "kf":
          return "MDRS_GENERIC_CONTENT";
        case "cds":
          return "MDRS_GENERIC_CONTENT";
        case "jds":
          return "MDRS_GENERIC_CONTENT";
        case "absl":
          return "ABSL";
        case "enht":
          return "APPLICATION_EXIT";
        case "fltr":
          return "APPLICATION_EXIT_FILTER";
        case "uicomponent":
          return "UICOMPONENT";
        case "run":
          return "MDRO";
        case "notification":
          return "NOTIFICATION_RULE";
        case "codelist":
          return "CODE_DATATYPE";
        case "library":
          return "CUSTOM_REUSE_LIBRARY";
        case "ref":
          return "CUSTOMER_OBJECT_REFERENCES";
        case "xwebservice":
          return "WSID_EXTENSION";
        case "xodata":
          return "XSODATA";
        default:
          return "XREP_CONTENT";
      }
    }

    public static ContentTypeMapping fromExtn(string extn)
    {
      switch (extn)
      {
        case "bo":
          return ContentTypeMapping.BUSINESS_OBJECT;
        case "bco":
          return ContentTypeMapping.BCO;
        case "approvalprocess":
          return ContentTypeMapping.MSAPPROVALSTEP;
        case "approval":
          return ContentTypeMapping.APPROVALSTEP;
        case "webservice":
          return ContentTypeMapping.A2X;
        case "wsid":
          return ContentTypeMapping.WSID;
        case "csd":
          return ContentTypeMapping.CSD;
        case "bac":
          return ContentTypeMapping.BUSINESSCONFIGURATION;
        case "bcc":
          return ContentTypeMapping.BCSET;
        case "bcctax":
          return ContentTypeMapping.BCSET;
        case "bct":
          return ContentTypeMapping.BC_WS_TEMPLATE;
        case "xbo":
          return ContentTypeMapping.EXTENSION_ENTITY;
        case "xuicomponent":
          return ContentTypeMapping.X_UICOMPONENT;
        case "xs":
          return ContentTypeMapping.EXTENSION_SCENARIO_EXTENSION;
        case "nxs":
          return ContentTypeMapping.NODE_EXTENSION_SCENARIO;
        case "xft":
          return ContentTypeMapping.FORM_EXTENSION_ENTITY;
        case "wsauth":
          return ContentTypeMapping.WSAUTH;
        case "ftgd":
          return ContentTypeMapping.FORMS_FTGD;
        case "xdp":
          return ContentTypeMapping.FORMS_XDP;
        case "xsd":
          return ContentTypeMapping.FORMS_XSD;
        case "fthd":
          return ContentTypeMapping.FORMS_FTHD;
        case "pid":
          return ContentTypeMapping.PROCESS_INTEGRATION;
        case "sadl":
          return ContentTypeMapping.SERVICE_ADAPT_DEF_LANG;
        case "qry":
          return ContentTypeMapping.QUERYDEF;
        case "ds":
          return ContentTypeMapping.DATASOURCE;
        case "report":
          return ContentTypeMapping.MDRS_GENERIC_CONTENT_REPORT;
        case "kf":
          return ContentTypeMapping.MDRS_GENERIC_CONTENT_KF;
        case "cds":
          return ContentTypeMapping.MDRS_GENERIC_CONTENT_CDS;
        case "jds":
          return ContentTypeMapping.MDRS_GENERIC_CONTENT_JDS;
        case "absl":
          return ContentTypeMapping.ABSL;
        case "enht":
          return ContentTypeMapping.APPLICATION_EXIT;
        case "fltr":
          return ContentTypeMapping.APPLICATION_EXIT_FILTER;
        case "uicomponent":
        case "uiswitch":
          return ContentTypeMapping.UICOMPONENT;
        case "run":
          return ContentTypeMapping.MDRO;
        case "notification":
          return ContentTypeMapping.NOTIFICATION_RULE;
        case "codelist":
          return ContentTypeMapping.CODELIST;
        case "library":
          return ContentTypeMapping.CUSTOM_REUSE_LIBRARY;
        case "ref":
          return ContentTypeMapping.CUSTOMER_OBJECT_REFERENCES;
        case "bott":
          return ContentTypeMapping.BO_THING_TYPE;
        case "xwebservice":
          return ContentTypeMapping.XWEBSERVICE;
        case "xodata":
          return ContentTypeMapping.XSODATA;
        default:
          return ContentTypeMapping.XREP_CONTENT;
      }
    }

    public string GetSupportedExtension()
    {
      return this.SupportedExtension;
    }
  }
}
