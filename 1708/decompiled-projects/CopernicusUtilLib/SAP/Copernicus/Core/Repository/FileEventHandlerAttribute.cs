﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.FileEventHandlerAttribute
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel.Composition;

namespace SAP.Copernicus.Core.Repository
{
  [MetadataAttribute]
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
  public class FileEventHandlerAttribute : ExportAttribute
  {
    public FileEventType EventTypeMask { get; set; }

    public int Priority { get; set; }

    public FileEventHandlerAttribute(FileEventType eventTypeMask, int priority = 100)
      : base(typeof (GenericFileEventHandler))
    {
      this.EventTypeMask = eventTypeMask;
      this.Priority = priority;
    }
  }
}
