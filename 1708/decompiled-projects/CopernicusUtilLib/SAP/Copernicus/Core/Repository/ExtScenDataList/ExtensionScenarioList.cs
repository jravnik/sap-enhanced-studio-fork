﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ExtScenDataList.ExtensionScenarioList
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.ExtScenDataList
{
  [XmlRoot(IsNullable = false, Namespace = "http://sap.com/ByD/PDI/ExtensionScenarioList")]
  [DebuggerStepThrough]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [XmlType(AnonymousType = true, Namespace = "http://sap.com/ByD/PDI/ExtensionScenarioList")]
  [Serializable]
  public class ExtensionScenarioList
  {
    private ExtensionScenarioType[] extensionScenarioField;

    [XmlElement("ExtensionScenario", Form = XmlSchemaForm.Unqualified)]
    public ExtensionScenarioType[] ExtensionScenario
    {
      get
      {
        return this.extensionScenarioField;
      }
      set
      {
        this.extensionScenarioField = value;
      }
    }
  }
}
