﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.Action
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class Action
  {
    public readonly string proxyName;
    public readonly string esrName;
    public readonly bool psmReleased;
    public readonly bool custom_action;
    public readonly bool off_enabled;

    public Action(string proxyName, string esrName, bool psmReleased, bool custom_action, bool offEnabled)
    {
      this.proxyName = proxyName;
      this.esrName = esrName;
      this.psmReleased = psmReleased;
      this.custom_action = custom_action;
      this.off_enabled = offEnabled;
    }
  }
}
