﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.EntityUsageIndex.UsageIndexCollector
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.Core.Repository.EntityUsageIndex
{
  public class UsageIndexCollector
  {
    private readonly IDictionary<string, ICollection<EntityUsageInfo>> entityUsages = (IDictionary<string, ICollection<EntityUsageInfo>>) new Dictionary<string, ICollection<EntityUsageInfo>>();
    private readonly IDictionary<string, ICollection<string>> fileUsages = (IDictionary<string, ICollection<string>>) new Dictionary<string, ICollection<string>>();
    public const string AddEntityUsageInfoContractName = "EntityUsageInfoSink";
    public const string AddFileUsageContractName = "FileUsageSink";

    public IEnumerable<string> GetFilesWithEntityUsageInfo()
    {
      return this.entityUsages.Where<KeyValuePair<string, ICollection<EntityUsageInfo>>>((Func<KeyValuePair<string, ICollection<EntityUsageInfo>>, bool>) (usage => usage.Value.Count > 0)).Select<KeyValuePair<string, ICollection<EntityUsageInfo>>, string>((Func<KeyValuePair<string, ICollection<EntityUsageInfo>>, string>) (usage => usage.Key));
    }

    public IEnumerable<string> GetFilesWithFileUsage()
    {
      return this.fileUsages.Where<KeyValuePair<string, ICollection<string>>>((Func<KeyValuePair<string, ICollection<string>>, bool>) (usage => usage.Value.Count > 0)).Select<KeyValuePair<string, ICollection<string>>, string>((Func<KeyValuePair<string, ICollection<string>>, string>) (usage => usage.Key));
    }

    public ICollection<EntityUsageInfo> GetEntityUsageInfoFor(string fileName)
    {
      if (string.IsNullOrEmpty(fileName))
        throw new ArgumentException("fileName nust not be null or empty");
      ICollection<EntityUsageInfo> entityUsageInfos;
      if (!this.entityUsages.TryGetValue(fileName, out entityUsageInfos))
      {
        entityUsageInfos = (ICollection<EntityUsageInfo>) new HashSet<EntityUsageInfo>();
        this.entityUsages.Add(fileName, entityUsageInfos);
      }
      return entityUsageInfos;
    }

    public IDictionary<string, ICollection<EntityUsageInfo>> GetAllEntityUsageInfos()
    {
      return this.entityUsages;
    }

    public ICollection<string> GetFileUsageFor(string fileName)
    {
      if (string.IsNullOrEmpty(fileName))
        throw new ArgumentException("fileName nust not be null or empty");
      ICollection<string> strings;
      if (!this.fileUsages.TryGetValue(fileName, out strings))
      {
        strings = (ICollection<string>) new HashSet<string>();
        this.fileUsages.Add(fileName, strings);
      }
      return strings;
    }

    public IDictionary<string, ICollection<string>> GetAllFileUsages()
    {
      return this.fileUsages;
    }

    public void AddEntityUsageInfo(string fileName, EntityUsageInfo usageInfo)
    {
      if (string.IsNullOrEmpty(fileName))
        throw new ArgumentException("fileName nust not be null or empty");
      if (usageInfo == null)
        throw new ArgumentNullException("usageInfo");
      this.GetEntityUsageInfoFor(fileName).Add(usageInfo);
    }

    public void AddFileUsage(string fileName, string usedFile)
    {
      if (string.IsNullOrEmpty(fileName))
        throw new ArgumentException("fileName nust not be null or empty");
      if (string.IsNullOrEmpty(usedFile))
        throw new ArgumentException("usedFile nust not be null or empty");
      this.GetFileUsageFor(fileName).Add(usedFile);
    }
  }
}
