﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.UserInterface.BOSelectComboBox
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Repository.UserInterface
{
  public class BOSelectComboBox : UserControl
  {
    private RepositoryDataSet.BusinessObjectsRow[] boRows;
    private SplitContainer splitContainer1;
    private List<BOSelectComboBox.BOstruct> sortedBOList;
    private string overridenNameLabel;
    private string titleText;
    private Button button1;
    private ComboBox comboBox1;
    private bool namespaceShow;
    private bool namespaceEditable;
    private string nameSpacePreset;
    private bool onlySAPNameSpacesShow;
    private bool onlySAPNameSpacesEditable;
    private bool onlySAPNameSpacesPreset;
    private bool writeAccessShow;
    private bool writeAccessEditable;
    private bool writeAccessUse;
    private bool writeAccessPreset;
    private bool deploymentUnitShow;
    private bool deploymentUnitEditable;
    private string deploymentUnitPreset;
    private bool boNameShow;
    private bool boNameEditable;
    private string boNamePreset;
    private bool prxNameShow;
    private bool prxNameEditable;
    private string prxNamePreset;
    private bool categoryShow;
    private bool categoryEditable;
    private BusinessObjectCategory categoryPreset;
    private BusinessObjectCategory[] categoryPresetList;
    private bool techCategoryShow;
    private bool techCategoryEditable;
    private BusinessObjectTechnicalCategory techCategoryPreset;

    public string TitleText
    {
      get
      {
        return this.titleText;
      }
      set
      {
        this.titleText = value;
      }
    }

    public string SelectedItem
    {
      get
      {
        if (this.comboBox1.SelectedItem != null)
          return this.comboBox1.SelectedItem.ToString();
        return (string) null;
      }
      set
      {
        this.comboBox1.SelectedItem = (object) value;
      }
    }

    public ComboBoxStyle DropDownStyle
    {
      get
      {
        return this.comboBox1.DropDownStyle;
      }
      set
      {
        this.comboBox1.DropDownStyle = value;
      }
    }

    public AutoCompleteMode AutoCompleteMode
    {
      get
      {
        return this.comboBox1.AutoCompleteMode;
      }
      set
      {
        this.comboBox1.AutoCompleteMode = value;
      }
    }

    public int ItemCount
    {
      get
      {
        return this.comboBox1.Items.Count;
      }
    }

    public bool SimpleMode
    {
      get
      {
        return this.splitContainer1.Panel2Collapsed;
      }
      set
      {
        this.splitContainer1.Panel2Collapsed = value;
      }
    }

    public bool NamespaceShow
    {
      get
      {
        return this.namespaceShow;
      }
      set
      {
        this.namespaceShow = value;
      }
    }

    public bool NamespaceEditable
    {
      get
      {
        return this.namespaceEditable;
      }
      set
      {
        this.namespaceEditable = value;
      }
    }

    public string NamespacePreset
    {
      get
      {
        return this.nameSpacePreset;
      }
      set
      {
        if (!(this.nameSpacePreset != value))
          return;
        this.nameSpacePreset = value;
        if (!string.IsNullOrWhiteSpace(this.nameSpacePreset))
          RepositoryDataCache.GetInstance().RefreshBOFolderNode(this.nameSpacePreset);
        this.InitializeComboBox();
      }
    }

    public bool OnlySAPNameSpacesShow
    {
      get
      {
        return this.onlySAPNameSpacesShow;
      }
      set
      {
        this.onlySAPNameSpacesShow = value;
      }
    }

    public bool OnlySAPNameSpacesEditable
    {
      get
      {
        return this.onlySAPNameSpacesEditable;
      }
      set
      {
        this.onlySAPNameSpacesEditable = value;
      }
    }

    public bool OnlySAPNameSpacesPreset
    {
      get
      {
        return this.onlySAPNameSpacesPreset;
      }
      set
      {
        this.onlySAPNameSpacesPreset = value;
        this.InitializeComboBox();
      }
    }

    public bool WriteAccessShow
    {
      get
      {
        return this.writeAccessShow;
      }
      set
      {
        this.writeAccessShow = value;
      }
    }

    public bool WriteAccessEditable
    {
      get
      {
        return this.writeAccessEditable;
      }
      set
      {
        this.writeAccessEditable = value;
      }
    }

    public bool WriteAccessUse
    {
      get
      {
        return this.writeAccessUse;
      }
      set
      {
        this.writeAccessUse = value;
        this.InitializeComboBox();
      }
    }

    public bool WriteAccessPreset
    {
      get
      {
        return this.writeAccessPreset;
      }
      set
      {
        this.writeAccessPreset = value;
        this.InitializeComboBox();
      }
    }

    public bool DeploymentUnitShow
    {
      get
      {
        return this.deploymentUnitShow;
      }
      set
      {
        this.deploymentUnitShow = value;
      }
    }

    public bool DeploymentUnitEditable
    {
      get
      {
        return this.deploymentUnitEditable;
      }
      set
      {
        this.deploymentUnitEditable = value;
      }
    }

    public string DeploymentUnitPreset
    {
      get
      {
        return this.deploymentUnitPreset;
      }
      set
      {
        this.deploymentUnitPreset = value;
        this.InitializeComboBox();
      }
    }

    public bool BoNameShow
    {
      get
      {
        return this.boNameShow;
      }
      set
      {
        this.boNameShow = value;
      }
    }

    public bool BoNameEditable
    {
      get
      {
        return this.boNameEditable;
      }
      set
      {
        this.boNameEditable = value;
      }
    }

    public string BONamePreset
    {
      get
      {
        return this.boNamePreset;
      }
      set
      {
        this.boNamePreset = value;
        this.InitializeComboBox();
      }
    }

    public bool ProxyNameShow
    {
      get
      {
        return this.prxNameShow;
      }
      set
      {
        this.prxNameShow = value;
      }
    }

    public bool ProxyNameEditable
    {
      get
      {
        return this.prxNameEditable;
      }
      set
      {
        this.prxNameEditable = value;
      }
    }

    public string ProxyNamePreset
    {
      get
      {
        return this.prxNamePreset;
      }
      set
      {
        this.prxNamePreset = value;
        this.InitializeComboBox();
      }
    }

    public bool ObjectCategoryShow
    {
      get
      {
        return this.categoryShow;
      }
      set
      {
        this.categoryShow = value;
      }
    }

    public bool ObjectCategoryEditable
    {
      get
      {
        return this.categoryEditable;
      }
      set
      {
        this.categoryEditable = value;
      }
    }

    public BusinessObjectCategory ObjectCategoryPreset
    {
      get
      {
        return this.categoryPreset;
      }
      set
      {
        if (this.categoryPreset.Equals((object) value))
          return;
        this.categoryPreset = value;
        this.InitializeComboBox();
      }
    }

    public BusinessObjectCategory[] ObjectCategoryPresetList
    {
      get
      {
        return this.categoryPresetList;
      }
      set
      {
        if (this.categoryPresetList != null && this.categoryPresetList.Equals((object) value))
          return;
        this.categoryPresetList = value;
        this.InitializeComboBox();
      }
    }

    public bool TechnicalCategoryShow
    {
      get
      {
        return this.techCategoryShow;
      }
      set
      {
        this.techCategoryShow = value;
      }
    }

    public bool TechnicalCategoryEditable
    {
      get
      {
        return this.techCategoryEditable;
      }
      set
      {
        this.techCategoryEditable = value;
      }
    }

    public BusinessObjectTechnicalCategory TechnicalCategoryPreset
    {
      get
      {
        return this.techCategoryPreset;
      }
      set
      {
        this.techCategoryPreset = value;
        this.InitializeComboBox();
      }
    }

    public event BOSelectComboBox.BusinessObjectSelectionChangedEventHandler BusinessObjectSelectionChangedEvent;

    public BOSelectComboBox()
    {
      this.InitializeComponent();
      this.categoryShow = true;
      this.categoryEditable = true;
      this.techCategoryShow = true;
      this.techCategoryEditable = true;
      this.namespaceShow = true;
      this.namespaceEditable = true;
      this.boNameShow = true;
      this.boNameEditable = true;
      this.deploymentUnitShow = true;
      this.deploymentUnitEditable = true;
    }

    internal void FireBusinessObjectSelectionChangedEvent(RepositoryDataSet.BusinessObjectsRow selectedRow)
    {
      if (this.BusinessObjectSelectionChangedEvent == null)
        return;
      this.BusinessObjectSelectionChangedEvent((object) this, selectedRow);
    }

    public void OverrideNameLabel(string name)
    {
      this.overridenNameLabel = name;
    }

    private void comboBox_MakeItWhite(object sender, object hlpevent)
    {
      ComboBox comboBox = (ComboBox) sender;
      comboBox.BackColor = Color.White;
      comboBox.AutoCompleteMode = AutoCompleteMode.None;
    }

    private void comboBox_DropDownClosed(object sender, object hlpevent)
    {
      ((ComboBox) sender).AutoCompleteMode = AutoCompleteMode.Suggest;
      this.selectedIndexChangedHandler((object) null, (EventArgs) null);
    }

    public int getOptimalComboBoxDropDownWidth(ComboBox cbEdit)
    {
      int num1 = cbEdit.DropDownWidth;
      Graphics graphics = cbEdit.CreateGraphics();
      Font font = cbEdit.Font;
      int num2 = cbEdit.Items.Count > cbEdit.MaxDropDownItems ? SystemInformation.VerticalScrollBarWidth : 0;
      foreach (string text in cbEdit.Items)
      {
        int num3 = (int) graphics.MeasureString(text, font).Width + num2;
        if (num1 < num3)
          num1 = num3;
      }
      return num1;
    }

    public void PresetFirst()
    {
      if (this.comboBox1.Items.Count <= 0)
        return;
      this.comboBox1.SelectedIndex = 0;
    }

    private void InitializeComboBox()
    {
      this.comboBox1.Items.Clear();
      this.comboBox1.DropDown -= new EventHandler(this.comboBox_MakeItWhite);
      this.comboBox1.DropDownClosed -= new EventHandler(this.comboBox_DropDownClosed);
      string writeaccess = (string) null;
      if (this.writeAccessUse)
        writeaccess = this.writeAccessPreset.ToString().ToLowerInvariant();
      string category = (string) null;
      if (this.categoryPresetList != null)
      {
        for (int index = 0; index < this.categoryPresetList.Length; ++index)
        {
          category += this.categoryPresetList[index].Code;
          if (index < this.categoryPresetList.Length - 1)
            category += "','";
        }
      }
      else
        category = this.categoryPreset.Code;
      this.boRows = RepositoryDataCache.GetInstance().QueryBOs(category, this.techCategoryPreset.Code, this.nameSpacePreset, this.prxNamePreset, this.boNamePreset, this.deploymentUnitPreset, writeaccess);
      List<BOSelectComboBox.BOstruct> source = new List<BOSelectComboBox.BOstruct>();
      for (int index = 0; index < this.boRows.Length; ++index)
      {
        if (!this.onlySAPNameSpacesPreset)
        {
          BOSelectComboBox.BOstruct bostruct = new BOSelectComboBox.BOstruct(this.boRows[index].Name, this.boRows[index].ProxyName);
          source.Add(bostruct);
        }
        else if (this.boRows[index].NSName.StartsWith("http://sap.com"))
        {
          string name = this.boRows[index].Name;
          if (this.boRows[index].Deprecated)
            name = name + "(" + Resource.Deprecated + ")";
          BOSelectComboBox.BOstruct bostruct = new BOSelectComboBox.BOstruct(name, this.boRows[index].ProxyName);
          source.Add(bostruct);
        }
      }
      this.sortedBOList = source.OrderBy<BOSelectComboBox.BOstruct, string>((Func<BOSelectComboBox.BOstruct, string>) (a => a.BoName)).ToList<BOSelectComboBox.BOstruct>();
      foreach (BOSelectComboBox.BOstruct sortedBo in this.sortedBOList)
        this.comboBox1.Items.Add((object) sortedBo.BoName);
      if (this.comboBox1.Items.Count == 1)
        this.comboBox1.SelectedIndex = 0;
      this.comboBox1.DropDown += new EventHandler(this.comboBox_MakeItWhite);
      this.comboBox1.DropDownClosed += new EventHandler(this.comboBox_DropDownClosed);
      this.comboBox1.DropDownWidth = this.getOptimalComboBoxDropDownWidth(this.comboBox1);
    }

    private void InitializeComponent()
    {
      this.button1 = new Button();
      this.comboBox1 = new ComboBox();
      this.splitContainer1 = new SplitContainer();
      this.splitContainer1.BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      this.button1.Dock = DockStyle.Fill;
      this.button1.Location = new Point(0, 0);
      this.button1.Name = "button1";
      this.button1.Size = new Size(90, 23);
      this.button1.TabIndex = 0;
      this.button1.Text = "Search";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.comboBox1.AutoCompleteMode = AutoCompleteMode.Suggest;
      this.comboBox1.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.comboBox1.Dock = DockStyle.Fill;
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new Point(0, 0);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new Size(245, 21);
      this.comboBox1.TabIndex = 1;
      this.comboBox1.SelectedIndexChanged += new EventHandler(this.selectedIndexChangedHandler);
      this.comboBox1.Leave += new EventHandler(this.selectedIndexChangedHandler);
      this.splitContainer1.Dock = DockStyle.Fill;
      this.splitContainer1.Location = new Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Panel1.Controls.Add((Control) this.comboBox1);
      this.splitContainer1.Panel2.Controls.Add((Control) this.button1);
      this.splitContainer1.Size = new Size(339, 23);
      this.splitContainer1.SplitterDistance = 245;
      this.splitContainer1.TabIndex = 2;
      this.Controls.Add((Control) this.splitContainer1);
      this.Name = "BOSelectComboBox";
      this.Size = new Size(339, 23);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    private void button1_Click(object sender, EventArgs e)
    {
      BOSearchForm boSearchForm = new BOSearchForm();
      if (this.overridenNameLabel != null)
        boSearchForm.overrideNameLabel(this.overridenNameLabel);
      boSearchForm.useNamespace(this.namespaceShow, this.namespaceEditable, this.nameSpacePreset);
      boSearchForm.useBOName(this.boNameShow, this.boNameEditable, this.boNamePreset);
      boSearchForm.useBOProxyName(this.prxNameShow, this.prxNameEditable, this.prxNamePreset);
      boSearchForm.useCategory(this.categoryShow, this.categoryEditable, this.categoryPreset);
      boSearchForm.useTechCategory(this.techCategoryShow, this.techCategoryEditable, this.techCategoryPreset);
      boSearchForm.useDeploymentUnit(this.deploymentUnitShow, this.deploymentUnitEditable, this.deploymentUnitPreset);
      boSearchForm.useWriteAccess(this.writeAccessUse, this.WriteAccessShow, this.writeAccessEditable, this.writeAccessPreset);
      boSearchForm.useOnlySAPNameSpaces(this.onlySAPNameSpacesShow, this.onlySAPNameSpacesEditable, this.onlySAPNameSpacesPreset);
      boSearchForm.BusinessObjectSelectionChangedEvent += new BOSearchForm.BusinessObjectSelectionChangedEventHandler(this.searchControl_BOChanged);
      if (this.titleText != null)
        boSearchForm.Text = this.TitleText;
      int num = (int) boSearchForm.ShowDialog();
    }

    private void searchControl_BOChanged(object sender, List<RepositoryDataSet.BusinessObjectsRow> selectedBOs)
    {
      if (selectedBOs == null || selectedBOs.Count <= 0)
        return;
      this.comboBox1.Text = selectedBOs[0].Name;
      this.FireBusinessObjectSelectionChangedEvent(selectedBOs[0]);
    }

    private void selectedIndexChangedHandler(object sender, EventArgs e)
    {
      RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) null;
      if (this.comboBox1.SelectedItem != null)
        businessObjectsRowArray = RepositoryDataCache.GetInstance().QueryBOs((string) null, (string) null, (string) null, this.sortedBOList[this.comboBox1.SelectedIndex].BOProxyName, (string) null, (string) null, (string) null);
      if (businessObjectsRowArray != null)
        this.FireBusinessObjectSelectionChangedEvent(businessObjectsRowArray[0]);
      else
        this.FireBusinessObjectSelectionChangedEvent((RepositoryDataSet.BusinessObjectsRow) null);
    }

    private struct BOstruct
    {
      public string BoName;
      public string BOProxyName;

      public BOstruct(string name, string proxy)
      {
        this.BoName = name;
        this.BOProxyName = proxy;
      }
    }

    public delegate void BusinessObjectSelectionChangedEventHandler(object sender, RepositoryDataSet.BusinessObjectsRow selectedRows);
  }
}
