﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.BOListElement
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Repository
{
  public class BOListElement
  {
    private List<string> nodeNameList = new List<string>();
    private List<PDI_EXT_S_EXT_NODE> nodeDataList = new List<PDI_EXT_S_EXT_NODE>();
    private string namespaceName;
    private string boName;

    public BOListElement(string namespaceName, string boName, List<PDI_EXT_S_EXT_NODE> nodeDataList, List<string> nodeNameList)
    {
      this.namespaceName = namespaceName;
      this.boName = boName;
      this.nodeNameList = nodeNameList;
      this.nodeDataList = nodeDataList;
    }

    public List<string> getNodeNames()
    {
      return this.nodeNameList;
    }

    public List<PDI_EXT_S_EXT_NODE> getNodes()
    {
      return this.nodeDataList;
    }

    public string getNamespace()
    {
      return this.namespaceName;
    }

    public string getBoName()
    {
      return this.boName;
    }
  }
}
