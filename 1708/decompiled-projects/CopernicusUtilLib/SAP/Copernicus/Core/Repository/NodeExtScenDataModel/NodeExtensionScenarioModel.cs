﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeExtScenDataModel.NodeExtensionScenarioModel
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository.NodeExtScenDataList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace SAP.Copernicus.Core.Repository.NodeExtScenDataModel
{
  public class NodeExtensionScenarioModel
  {
    private static int _noCheckes = 0;
    private static readonly Encoding encUTF8 = Encoding.UTF8;
    public NodeExtensionScenarioType xmlModel = new NodeExtensionScenarioType();
    private List<FlowType> flowList = new List<FlowType>();

    public int noChecks
    {
      get
      {
        return NodeExtensionScenarioModel._noCheckes;
      }
      set
      {
        NodeExtensionScenarioModel._noCheckes = value;
      }
    }

    public bool initNewScenarioList(List<PDI_NODE_EXT_S_BO_NODE_KEY> boNodeKeys, string solutionPrefix, out string altKeyExists, out string scenarioUsed)
    {
      NodeExtensionScenarioList extScenarios = this.GetExtScenarios(boNodeKeys, solutionPrefix, out altKeyExists, out scenarioUsed);
      return altKeyExists.Equals("X") && !scenarioUsed.Equals("X") && (extScenarios.ExtensionScenario != null && this.mapScenarioCacheToModel(true, extScenarios));
    }

    public bool updateScenarioList(List<PDI_NODE_EXT_S_BO_NODE_KEY> boNodeKeys, string solutionPrefix, out string altKeyExists, out string scenarioUsed)
    {
      NodeExtensionScenarioList extScenarios = this.GetExtScenarios(boNodeKeys, solutionPrefix, out altKeyExists, out scenarioUsed);
      return extScenarios.ExtensionScenario == null || ((IEnumerable<SAP.Copernicus.Core.Repository.NodeExtScenDataList.ExtensionScenarioType>) extScenarios.ExtensionScenario).Count<SAP.Copernicus.Core.Repository.NodeExtScenDataList.ExtensionScenarioType>() == 0 || this.mapScenarioCacheToModel(false, extScenarios);
    }

    public void loadModelByFilePath(string filePath)
    {
      try
      {
        using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
          this.xmlModel = (NodeExtensionScenarioType) new XmlSerializer(typeof (NodeExtensionScenarioType)).Deserialize((Stream) fileStream);
      }
      catch (Exception ex)
      {
        Console.Out.WriteLine("[ERROR] An exception occurred when trying to deserialize file " + filePath);
        throw new ArgumentException(" An exception occurred when trying to deserialize file" + filePath, ex);
      }
      if (this.xmlModel.ExtensionScenarioList == null)
        return;
      foreach (ExtensionScenarioType extensionScenario in this.xmlModel.ExtensionScenarioList)
      {
        if (extensionScenario.is_selected)
          ++this.noChecks;
      }
    }

    public void loadModelByString(string content)
    {
      string s = this.ConvertBase64ToString(content);
      XmlSerializer xmlSerializer = new XmlSerializer(typeof (NodeExtensionScenarioType));
      StringReader stringReader1 = new StringReader(s);
      XmlTextReader xmlTextReader = new XmlTextReader((TextReader) stringReader1);
      this.xmlModel = (NodeExtensionScenarioType) xmlSerializer.Deserialize((XmlReader) xmlTextReader);
      xmlTextReader.Close();
      stringReader1.Close();
      try
      {
        using (StringReader stringReader2 = new StringReader(s))
          this.xmlModel = (NodeExtensionScenarioType) new XmlSerializer(typeof (NodeExtensionScenarioType)).Deserialize((TextReader) stringReader2);
      }
      catch (Exception ex)
      {
        Console.Out.WriteLine("[ERROR] An exception occurred when trying to deserialize string " + content);
      }
    }

    private string ConvertBase64ToString(string base64)
    {
      return Encoding.UTF8.GetString(Convert.FromBase64String(base64));
    }

    public bool saveXmlModelToFileSystem(NodeExtensionScenarioType model, string filePath)
    {
      try
      {
        using (StreamWriter streamWriter = new StreamWriter(filePath, false, (Encoding) new UTF8Encoding(false)))
        {
          new XmlSerializer(typeof (NodeExtensionScenarioType)).Serialize((TextWriter) streamWriter, (object) model);
          streamWriter.Close();
        }
        return true;
      }
      catch (Exception ex)
      {
        Console.Out.WriteLine("[ERROR] An exception occurred when trying to serialize model");
        return false;
      }
    }

    private string UTF8ByteArrayToString(byte[] characters)
    {
      return new UTF8Encoding().GetString(characters);
    }

    public string getModelAsString()
    {
      XmlSerializer xmlSerializer = new XmlSerializer(typeof (NodeExtensionScenarioType));
      ExtScenStringWriterUTF8 stringWriterUtF8 = new ExtScenStringWriterUTF8();
      new XmlSerializer(typeof (NodeExtensionScenarioType)).Serialize((TextWriter) stringWriterUtF8, (object) this.xmlModel);
      return stringWriterUtF8.ToString();
    }

    public NodeExtensionScenarioList GetExtScenarios(List<PDI_NODE_EXT_S_BO_NODE_KEY> nodeKeys, string solutionPrefix, out string altKeyExists, out string scenarioUsed)
    {
      int index = 0;
      PDI_NODE_EXT_S_BO_NODE_KEY[] IT_BO_NODES = new PDI_NODE_EXT_S_BO_NODE_KEY[nodeKeys.Count<PDI_NODE_EXT_S_BO_NODE_KEY>()];
      foreach (PDI_NODE_EXT_S_BO_NODE_KEY nodeKey in nodeKeys)
      {
        IT_BO_NODES[index] = nodeKey;
        ++index;
      }
      NodeExtensionScenarioList extensionScenarioList = new NodeExtensionScenarioList();
      string resultBase64 = (string) null;
      new ExtensionHandler().GetNodeExtScenarios(IT_BO_NODES, solutionPrefix, out resultBase64, out altKeyExists, out scenarioUsed);
      if (!altKeyExists.Equals("X") || scenarioUsed.Equals("X") || resultBase64 == null)
        return extensionScenarioList;
      byte[] numArray = Convert.FromBase64String(resultBase64);
      MemoryStream memoryStream = new MemoryStream(numArray);
      string s = NodeExtensionScenarioModel.encUTF8.GetString(numArray);
      using (StringReader stringReader = new StringReader(s))
        return (NodeExtensionScenarioList) new XmlSerializer(typeof (NodeExtensionScenarioList)).Deserialize((TextReader) stringReader);
    }

    public static bool isModelConsistent(List<ExtensionScenarioType> extScenList)
    {
      ExtensionScenarioTypeEqualityComparer equalityComparer = new ExtensionScenarioTypeEqualityComparer();
      ICollection<ExtensionScenarioType> source = (ICollection<ExtensionScenarioType>) new HashSet<ExtensionScenarioType>((IEqualityComparer<ExtensionScenarioType>) equalityComparer);
      foreach (ExtensionScenarioType extScen in extScenList)
      {
        if (extScen != null && extScen.is_selected)
          source.Add(extScen);
      }
      if (source.Count < 2)
        return true;
      ICollection<ExtensionScenarioType> extScenColl = (ICollection<ExtensionScenarioType>) new HashSet<ExtensionScenarioType>((IEqualityComparer<ExtensionScenarioType>) equalityComparer);
      if (source.Count > 0)
      {
        ExtensionScenarioType extensionScenarioType = source.First<ExtensionScenarioType>();
        source.Remove(extensionScenarioType);
        extScenColl.Add(extensionScenarioType);
      }
      while (source.Count > 0)
      {
        bool flag = false;
        foreach (ExtensionScenarioType extScen in (IEnumerable<ExtensionScenarioType>) source)
        {
          if (NodeExtensionScenarioModel.getConnectedExtScen(extScen, extScenColl) != null)
          {
            source.Remove(extScen);
            extScenColl.Add(extScen);
            flag = true;
            break;
          }
        }
        if (!flag)
          return false;
      }
      return true;
    }

    private static ExtensionScenarioType getConnectedExtScen(ExtensionScenarioType extScen, ICollection<ExtensionScenarioType> extScenColl)
    {
      foreach (ExtensionScenarioType extScen2 in (IEnumerable<ExtensionScenarioType>) extScenColl)
      {
        if (!extScen2.Equals((object) extScen) && NodeExtensionScenarioModel.areConnected(extScen, extScen2))
          return extScen2;
      }
      return (ExtensionScenarioType) null;
    }

    private static bool areConnected(ExtensionScenarioType extScen1, ExtensionScenarioType extScen2)
    {
      FlowType[] boConnections1 = extScen1.bo_connections;
      FlowType[] boConnections2 = extScen2.bo_connections;
      foreach (FlowType flowType1 in boConnections1)
      {
        string sourceBoName1 = flowType1.source_bo_name;
        string sourceBoNodeName1 = flowType1.source_bo_node_name;
        string targetBoName1 = flowType1.target_bo_name;
        string targetBoNodeName1 = flowType1.target_bo_node_name;
        foreach (FlowType flowType2 in boConnections2)
        {
          string sourceBoName2 = flowType2.source_bo_name;
          string sourceBoNodeName2 = flowType2.source_bo_node_name;
          string targetBoName2 = flowType2.target_bo_name;
          string targetBoNodeName2 = flowType2.target_bo_node_name;
          if (sourceBoName1.Equals(sourceBoName2) && sourceBoNodeName1.Equals(sourceBoNodeName2) || sourceBoName1.Equals(targetBoName2) && sourceBoNodeName1.Equals(targetBoNodeName2) || (targetBoName1.Equals(sourceBoName2) && targetBoNodeName1.Equals(sourceBoNodeName2) || targetBoName1.Equals(targetBoName2) && targetBoNodeName1.Equals(targetBoNodeName2)))
            return true;
        }
      }
      return false;
    }

    private bool mapScenarioCacheToModel(bool create, NodeExtensionScenarioList newExtensionScenarioList)
    {
      List<ExtensionScenarioType> source = new List<ExtensionScenarioType>();
      if (newExtensionScenarioList.ExtensionScenario != null)
      {
        foreach (SAP.Copernicus.Core.Repository.NodeExtScenDataList.ExtensionScenarioType extensionScenarioType1 in newExtensionScenarioList.ExtensionScenario)
        {
          ExtensionScenarioType extensionScenarioType2 = new ExtensionScenarioType();
          List<FlowType> flowTypeList = new List<FlowType>();
          extensionScenarioType2.scenario_description = extensionScenarioType1.scenario_description;
          extensionScenarioType2.scenario_name = extensionScenarioType1.scenario_name;
          extensionScenarioType2.service_interface_type = extensionScenarioType1.service_interface_type;
          if (extensionScenarioType1.bo_connections != null)
          {
            extensionScenarioType2.bo_connections = new FlowType[((IEnumerable<SAP.Copernicus.Core.Repository.NodeExtScenDataList.FlowType>) extensionScenarioType1.bo_connections).Count<SAP.Copernicus.Core.Repository.NodeExtScenDataList.FlowType>()];
            foreach (SAP.Copernicus.Core.Repository.NodeExtScenDataList.FlowType boConnection in extensionScenarioType1.bo_connections)
            {
              List<ReferenceFieldType> referenceFieldTypeList = new List<ReferenceFieldType>();
              FlowType flowType = new FlowType();
              flowType.bo_connection_order = boConnection.bo_connection_order;
              flowType.bo_connection_description = boConnection.bo_connection_description;
              flowType.source_bo_name = boConnection.source_bo_name;
              flowType.source_bo_node_name = boConnection.source_bo_node_name;
              flowType.is_source_hidden = boConnection.is_source_hidden == "X";
              flowType.target_bo_name = boConnection.target_bo_name;
              flowType.target_bo_node_name = boConnection.target_bo_node_name;
              flowType.is_target_hidden = boConnection.is_target_hidden;
              flowType.reference_field_keys = new ReferenceFieldType[((IEnumerable<SAP.Copernicus.Core.Repository.NodeExtScenDataList.ReferenceFieldType>) boConnection.reference_field_keys).Count<SAP.Copernicus.Core.Repository.NodeExtScenDataList.ReferenceFieldType>()];
              flowType.message_type = boConnection.message_type;
              flowType.service_prx = boConnection.service_prx;
              flowType.operation_prx = boConnection.operation_prx;
              flowType.direction = boConnection.direction;
              if (boConnection.reference_field_keys != null)
              {
                flowType.reference_field_keys = new ReferenceFieldType[((IEnumerable<SAP.Copernicus.Core.Repository.NodeExtScenDataList.ReferenceFieldType>) boConnection.reference_field_keys).Count<SAP.Copernicus.Core.Repository.NodeExtScenDataList.ReferenceFieldType>()];
                foreach (SAP.Copernicus.Core.Repository.NodeExtScenDataList.ReferenceFieldType referenceFieldKey in boConnection.reference_field_keys)
                  referenceFieldTypeList.Add(new ReferenceFieldType()
                  {
                    reference_field_bundle_key = referenceFieldKey.reference_field_bundle_key,
                    reference_field_name = referenceFieldKey.reference_field_name
                  });
                referenceFieldTypeList.CopyTo(flowType.reference_field_keys);
                flowTypeList.Add(flowType);
              }
            }
            flowTypeList.CopyTo(extensionScenarioType2.bo_connections);
          }
          source.Add(extensionScenarioType2);
        }
      }
      if (create)
      {
        this.xmlModel.ExtensionScenarioList = new ExtensionScenarioType[source.Count<ExtensionScenarioType>()];
        source.CopyTo(this.xmlModel.ExtensionScenarioList);
        return true;
      }
      List<ExtensionScenarioType> extScenList = new List<ExtensionScenarioType>();
      List<ExtensionScenarioType> extensionScenarioTypeList = new List<ExtensionScenarioType>();
      extensionScenarioTypeList.AddRange((IEnumerable<ExtensionScenarioType>) source);
      if (extensionScenarioTypeList.Count == 1)
      {
        extScenList.Add(extensionScenarioTypeList[0]);
      }
      else
      {
        extensionScenarioTypeList.Sort(new Comparison<ExtensionScenarioType>(NodeExtensionScenarioModel.CompareExtScenListByProxyNameOrder));
        for (int index = 0; index < extensionScenarioTypeList.Count; ++index)
        {
          if (index != extensionScenarioTypeList.Count - 1)
          {
            if (NodeExtensionScenarioModel.CompareExtScenListByProxyNameOrder(extensionScenarioTypeList[index], extensionScenarioTypeList[index + 1]) != 0)
              extScenList.Add(extensionScenarioTypeList[index]);
          }
          else if (NodeExtensionScenarioModel.CompareExtScenListByProxyNameOrder(extensionScenarioTypeList[index], extensionScenarioTypeList[index - 1]) == 0)
            extScenList.Add(extensionScenarioTypeList[index]);
          else
            extScenList.Add(extensionScenarioTypeList[index]);
        }
      }
      List<ExtensionScenarioType> list = ((IEnumerable<ExtensionScenarioType>) this.xmlModel.ExtensionScenarioList).ToList<ExtensionScenarioType>();
      foreach (ExtensionScenarioType extensionScenario in extScenList)
      {
        for (int index = 0; index < list.Count; ++index)
        {
          if (this.isServiceInterface(extensionScenario))
          {
            ReferenceFieldType referenceFieldKey1 = ((IEnumerable<FlowType>) list[index].bo_connections).First<FlowType>().reference_field_keys[0];
            ReferenceFieldType referenceFieldKey2 = ((IEnumerable<FlowType>) extensionScenario.bo_connections).First<FlowType>().reference_field_keys[0];
            if (referenceFieldKey1.reference_field_bundle_key == referenceFieldKey2.reference_field_bundle_key && referenceFieldKey1.reference_field_name == referenceFieldKey2.reference_field_name)
            {
              extensionScenario.is_selected = list[index].is_selected;
              break;
            }
          }
          else if (list[index].scenario_name == extensionScenario.scenario_name)
            extensionScenario.is_selected = list[index].is_selected;
        }
      }
      if (!NodeExtensionScenarioModel.isModelConsistent(extScenList))
        return false;
      this.xmlModel.ExtensionScenarioList = new ExtensionScenarioType[extScenList.Count];
      extScenList.CopyTo(this.xmlModel.ExtensionScenarioList);
      return true;
    }

    private bool isServiceInterface(ExtensionScenarioType extensionScenario)
    {
      return extensionScenario.service_interface_type == "INBOUND" || extensionScenario.service_interface_type == "OUTBOUND" || (extensionScenario.service_interface_type == "INBOUND_EXTEXP_MIG" || extensionScenario.service_interface_type == "INBOUND_MIG") || (extensionScenario.service_interface_type == "OUTBOUND_EXTEXP_MIG" || extensionScenario.service_interface_type == "OUTBOUND_MIG");
    }

    private static int CompareExtScenListByProxyNameOrder(ExtensionScenarioType x, ExtensionScenarioType y)
    {
      if (x.scenario_name == null)
        return y.scenario_name == null ? 0 : -1;
      if (y.scenario_name == null)
        return 1;
      return x.scenario_name.CompareTo(y.scenario_name);
    }

    private static int CompareScenariosByProxyNameOrder(PDI_EXT_S_SCENARIO x, PDI_EXT_S_SCENARIO y)
    {
      if (x.SCEN_NAME_PRX == null)
        return y.SCEN_NAME_PRX == null ? 0 : -1;
      if (y.SCEN_NAME_PRX == null)
        return 1;
      return x.SCEN_NAME_PRX.CompareTo(y.SCEN_NAME_PRX);
    }
  }
}
