﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.NodeExtScenDataModel.ExtScenStringWriterUTF8
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.IO;
using System.Text;

namespace SAP.Copernicus.Core.Repository.NodeExtScenDataModel
{
  public class ExtScenStringWriterUTF8 : StringWriter
  {
    public override Encoding Encoding
    {
      get
      {
        return Encoding.UTF8;
      }
    }
  }
}
