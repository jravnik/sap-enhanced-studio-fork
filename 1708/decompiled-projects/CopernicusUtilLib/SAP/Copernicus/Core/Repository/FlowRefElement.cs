﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.FlowRefElement
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class FlowRefElement
  {
    public string FlowName { get; set; }

    public string RefFieldBundleKey { get; set; }

    public string RefFieldName { get; set; }

    public string ScenarioName { get; set; }

    public FlowRefElement(string flowName, string refFieldBundleKey, string RefFieldName, string scenarioName)
    {
      this.FlowName = flowName;
      this.RefFieldBundleKey = refFieldBundleKey;
      this.RefFieldName = RefFieldName;
      this.ScenarioName = scenarioName;
    }
  }
}
