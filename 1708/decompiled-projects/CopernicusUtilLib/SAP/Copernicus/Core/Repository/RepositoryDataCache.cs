﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.RepositoryDataCache
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.PDI;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using SAP.Copernicus.Core.Repository.DataModel;
using SAP.Copernicus.Core.Scripting;
using SAP.Copernicus.Core.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;

namespace SAP.Copernicus.Core.Repository
{
  public class RepositoryDataCache
  {
    public static readonly string NOT_IN_CACHE = string.Empty;
    protected static RepositoryDataCache instance = new RepositoryDataCache();
    protected RepositoryDataSet repositoryDataSet = new RepositoryDataSet();
    private RepositoryQueryHandler queryHandler = new RepositoryQueryHandler();
    private SolutionHandler solutionHandler = new SolutionHandler();
    public static EventHandlerOnModelChanged ModelChangeEventHandlerRegistry;
    public static EventHandlerOnRepositoryRefresh RefreshEventHandlers;
    private QueryResultSetParser resultSetParser;
    private PDI_S_PRODUCT_VERS_AND_COMPS[] products;

    public PDI_S_PRODUCT_VERS_AND_COMPS[] Products
    {
      get
      {
        return this.products;
      }
      set
      {
        this.products = value;
      }
    }

    public RepositoryDataSet RepositoryDataSet
    {
      get
      {
        return this.repositoryDataSet;
      }
    }

    protected RepositoryDataCache()
    {
      this.queryHandler.BORepositoryDataLoadedEvent += new RepositoryQueryHandler.BORepositoryDataLoadedEventHandler(this.handleBOQueryUpdate);
      this.queryHandler.DTRepositoryDataLoadedEvent += new RepositoryQueryHandler.DTRepositoryDataLoadedEventHandler(this.handleDTQueryUpdate);
      this.queryHandler.LibRepositoryDataLoadedEvent += new RepositoryQueryHandler.LibRepositoryDataLoadedEventHandler(this.handleLibraryQueryUpdate);
      this.queryHandler.MTRepositoryDataLoadedEvent += new RepositoryQueryHandler.MTRepositoryDataLoadedEventHandler(this.handleMTQueryUpdate);
      this.queryHandler.BAdIRepositoryDataLoadedEvent += new RepositoryQueryHandler.BAdIRepositoryDataLoadedEventHandler(this.HandleBAdIQueryUpdate);
      this.resultSetParser = new QueryResultSetParser(this.repositoryDataSet, this.queryHandler);
    }

    public void HandleBAdIQueryUpdate(string newResultXML)
    {
      lock (this.queryHandler.lockObject)
      {
        Dictionary<string, LastChangedItem> dtLastChangedInfo = new Dictionary<string, LastChangedItem>();
        foreach (RepositoryDataSet.BAdIsRow badIsRow in this.repositoryDataSet.BAdIs.Select())
          dtLastChangedInfo.Add(badIsRow.ProxyName, new LastChangedItem(badIsRow.ProxyName, DateTime.MinValue));
        this.resultSetParser.ParseBAdIIntoDataset(newResultXML, dtLastChangedInfo);
      }
    }

    public void handleBOQueryUpdate(string newResultXML)
    {
      lock (this.queryHandler.lockObject)
        this.resultSetParser.RefreshBOs(newResultXML);
    }

    public void handleDTQueryUpdate(string newResultXML)
    {
      lock (this.queryHandler.lockObject)
      {
        Dictionary<string, LastChangedItem> dtLastChangedInfo = new Dictionary<string, LastChangedItem>();
        foreach (RepositoryDataSet.DataTypesRow dataTypesRow in this.repositoryDataSet.DataTypes.Select())
          dtLastChangedInfo.Add(dataTypesRow.ProxyName, new LastChangedItem(dataTypesRow.ProxyName, dataTypesRow.LastChangeAt, dataTypesRow.TransitiveHash));
        this.resultSetParser.ParseDTsIntoDataSet(newResultXML, dtLastChangedInfo);
      }
    }

    public void handleLibraryQueryUpdate(string newResultXML)
    {
      lock (this.queryHandler.lockObject)
      {
        Dictionary<string, LastChangedItem> LibrariesLastChangedInfo = new Dictionary<string, LastChangedItem>();
        foreach (RepositoryDataSet.LibrariesRow librariesRow in this.repositoryDataSet.Libraries.Select())
          LibrariesLastChangedInfo.Add(librariesRow.ProxyName, new LastChangedItem(librariesRow.ProxyName, librariesRow.LastChangeAt, librariesRow.TransitiveHash));
        this.resultSetParser.ParseLibrariesIntoDataSet(newResultXML, LibrariesLastChangedInfo);
      }
    }

    public void handleMTQueryUpdate(string newResultXML)
    {
      lock (this.queryHandler.lockObject)
      {
        Dictionary<string, LastChangedItem> dictionary = new Dictionary<string, LastChangedItem>();
      }
    }

    public static RepositoryDataCache GetInstance()
    {
      return RepositoryDataCache.instance;
    }

    public void Reset()
    {
      lock (this.queryHandler.lockObject)
      {
        this.repositoryDataSet.Clear();
        this.queryHandler.ResetProxy();
      }
    }

    public void ExportContentToFile(string fileName)
    {
      lock (this.queryHandler.lockObject)
      {
        using (TextWriter w = (TextWriter) new StringWriter())
        {
          this.repositoryDataSet.WriteXml((XmlWriter) new XmlTextWriter(w));
          byte[] bytes = new UTF8Encoding().GetBytes(w.ToString());
          using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
            fileStream.Write(bytes, 0, bytes.Length);
        }
      }
    }

    public bool DoesSolutionExists(string SolutionName, string SolutionVersion)
    {
      lock (this.queryHandler.lockObject)
      {
        if (this.repositoryDataSet.Solutions.Rows.Contains(new object[2]{ (object) SolutionName, (object) SolutionVersion }))
          return true;
      }
      return false;
    }

    public void UpdateSolution(PDI_S_PRODUCT_VERS_AND_COMPS product)
    {
      if (product == null)
        return;
      string str1 = (string) null;
      if (product.PRODUCT != null)
        str1 = product.PRODUCT.PRODUCT;
      if (product.VERSIONS == null || product.VERSIONS.Length < 1)
      {
        Trace.WriteLine(string.Format("No product versions found for product {0}", (object) str1));
      }
      else
      {
        bool flag = product.PRODUCT.IS_DELETED.Equals("X");
        foreach (PDI_S_PRODUCT_V_AND_COMPONENTS productVAndComponents in product.VERSIONS)
        {
          if (productVAndComponents != null && productVAndComponents.PRODUCT_VERSION != null)
          {
            string str2 = str1;
            string str3 = string.Empty;
            string empty = string.Empty;
            string version = productVAndComponents.PRODUCT_VERSION.VERSION;
            string productType = product.PRODUCT.PRODUCT_TYPE;
            string pv1OPatchSolution = productVAndComponents.PV_1O_PATCH_SOLUTION;
            string str4 = product.PRODUCT_SUBTYPE;
            if (productType != null && productType.Trim().Length != 0 && SolutionType.fromSolutionTypeCode(productType).ToString().ToString().Equals(SolutionType.MultiCustomer_Solutions.ToString()))
              str3 = productVAndComponents.EXTERNAL_NAMESPACE;
            string str5 = "";
            if (productVAndComponents.COMP_VERSIONS.Length != 0)
              str5 = productVAndComponents.COMP_VERSIONS[0].COMPONENT.DEFAULT_DU;
            string str6 = "";
            if (product.PRODUCT.DEV_PARTNER != null)
              str6 = product.PRODUCT.DEV_PARTNER;
            string str7 = "";
            if (product.PRODUCT.CONTACT_PERSON != null)
              str7 = product.PRODUCT.CONTACT_PERSON;
            string str8 = "";
            if (product.PRODUCT.EMAIL != null)
              str8 = product.PRODUCT.EMAIL;
            if (str4 == null)
              str4 = "";
            string str9 = "";
            if (productVAndComponents.PRODUCT_VERSION_TEXTS != null && ((IEnumerable<PDI_PRODUCT_V_T>) productVAndComponents.PRODUCT_VERSION_TEXTS).FirstOrDefault<PDI_PRODUCT_V_T>() != null)
              str9 = ((IEnumerable<PDI_PRODUCT_V_T>) productVAndComponents.PRODUCT_VERSION_TEXTS).FirstOrDefault<PDI_PRODUCT_V_T>().DDTEXT;
            lock (this.queryHandler.lockObject)
            {
              if (!this.repositoryDataSet.Solutions.Rows.Contains(new object[2]{ (object) str2, (object) version }))
              {
                string status = productVAndComponents.PRODUCT_VERSION.STATUS;
                string certificationStatus = productVAndComponents.PV_CERTIFICATION_STATUS;
                string pvOverallStatus = productVAndComponents.PV_OVERALL_STATUS;
                RepositoryDataSet.SolutionsDataTable solutions = this.repositoryDataSet.Solutions;
                string filterExpression = "Name='" + str2 + "' AND Version='" + version + "'";
                foreach (DataRow dataRow in solutions.Select(filterExpression))
                  dataRow.Delete();
                this.repositoryDataSet.Solutions.Rows.Add((object) str2, (object) version, (object) status, (object) str9, (object) productType, (object) flag, (object) certificationStatus, (object) pvOverallStatus, (object) pv1OPatchSolution, (object) str4, (object) str6, (object) str7, (object) str8, (object) str5, (object) str3);
              }
              if (str6.Length > 0)
              {
                string status = productVAndComponents.PRODUCT_VERSION.STATUS;
                string certificationStatus = productVAndComponents.PV_CERTIFICATION_STATUS;
                string pvOverallStatus = productVAndComponents.PV_OVERALL_STATUS;
                RepositoryDataSet.SolutionsDataTable solutions = this.repositoryDataSet.Solutions;
                string filterExpression = "Name='" + str2 + "' AND Version='" + version + "'";
                foreach (DataRow dataRow in solutions.Select(filterExpression))
                  dataRow.Delete();
                this.repositoryDataSet.Solutions.Rows.Add((object) str2, (object) version, (object) status, (object) str9, (object) productType, (object) flag, (object) certificationStatus, (object) pvOverallStatus, (object) pv1OPatchSolution, (object) str4, (object) str6, (object) str7, (object) str8, (object) str5, (object) str3);
              }
            }
          }
        }
      }
    }

    public void RefreshSolutions()
    {
      lock (this.queryHandler.lockObject)
      {
        this.repositoryDataSet.Tables["Solutions"].Clear();
        this.repositoryDataSet.AcceptChanges();
        Connection instance = Connection.getInstance();
        instance.connect();
        PDI_S_PRODUCT_VERS_AND_COMPS[] solutionsFor = this.solutionHandler.GetSolutionsFor(instance.getLoggedInUser());
        this.Products = solutionsFor;
        foreach (PDI_S_PRODUCT_VERS_AND_COMPS product in solutionsFor)
          this.UpdateSolution(product);
      }
    }

    public string getSolutionStatus(string SolutionName, string SolutionVersion)
    {
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        DataRow[] dataRowArray = this.repositoryDataSet.Solutions.Select("Name='" + SolutionName + "' AND Version='" + SolutionVersion + "'");
        int index = 0;
        if (index < dataRowArray.Length)
          str = ((RepositoryDataSet.SolutionsRow) dataRowArray[index]).Status;
      }
      return str;
    }

    public void RefreshBO(string nsName, string boName)
    {
      string bqlQueryResult = this.queryHandler.RefreshBO(nsName, boName);
      this.resultSetParser.RefreshBO(nsName, boName, bqlQueryResult);
    }

    public void RefreshBOFolderNode(string nsName)
    {
      string bqlQueryResult = this.queryHandler.RefreshBOs(nsName);
      this.resultSetParser.RefreshBOsForNSInDataSet(nsName, bqlQueryResult);
    }

    public void RefreshDTFolderNode(string nsName)
    {
      string bqlQueryResult = this.queryHandler.RefreshDTs(nsName);
      this.resultSetParser.RefreshDTsForNSInDataSet(nsName, bqlQueryResult);
    }

    public void RefreshEnhancementOptionFolderNode(string nsName)
    {
      string bqlQueryResult = this.queryHandler.RefreshEnhancementOptions(nsName);
      this.resultSetParser.RefreshEnhancementOptionsForNSInDataSet(nsName, bqlQueryResult);
    }

    public void RefreshMTFolderNode(string nsName)
    {
      string bqlQueryResult = this.queryHandler.RefreshMTs(nsName);
      this.resultSetParser.RefreshMTsForNSInDataSet(nsName, bqlQueryResult);
    }

    public void RefreshPartnerNamespaces()
    {
      IList<string> fromOpenSolution = ProjectUtil.GetNamespacesFromOpenSolution();
      if (fromOpenSolution == null)
        return;
      foreach (string nsName in (IEnumerable<string>) fromOpenSolution)
      {
        DateTime start = SAP.Copernicus.Core.Util.Util.startMeasurement("Refreshing contents of " + nsName);
        string bqlQueryResult1 = this.queryHandler.RefreshBOs(nsName);
        string bqlQueryResult2 = this.queryHandler.RefreshDTs(nsName);
        this.resultSetParser.RefreshBOsForNSInDataSet(nsName, bqlQueryResult1);
        this.resultSetParser.RefreshDTsForNSInDataSet(nsName, bqlQueryResult2);
        SAP.Copernicus.Core.Util.Util.endMeasurement(start, string.Format("Refresh of {0} contents finished in: ", (object) nsName));
      }
    }

    public void RefreshItemsAndFeatures()
    {
      try
      {
        ItemFeatureHandler itemFeatureHandler = new ItemFeatureHandler();
        PDI_PROJECT_ITEM_SOLUTION[] projectItemSolution;
        PDI_PROJECT_ITEM[] projectItem;
        PDI_FEATURE[] feature;
        itemFeatureHandler.getItemsAndFeatures("", "", out projectItemSolution, out projectItem, out feature);
        string[] restrictedReleaseItems = ItemFeatureHandler.GetItemTypesToRestrict();
        PDI_FEATURE_RESTRICTION_RES[] featureRes;
        itemFeatureHandler.getFeatureRestrictions(restrictedReleaseItems, out featureRes);
        for (int i = 0; i < featureRes.Length; ++i)
        {
          if (!featureRes[i].CFG_VALUE.Equals("X"))
          {
            projectItemSolution = ((IEnumerable<PDI_PROJECT_ITEM_SOLUTION>) projectItemSolution).Where<PDI_PROJECT_ITEM_SOLUTION>((Func<PDI_PROJECT_ITEM_SOLUTION, bool>) (n => !n.PROJ_ITEM_TYPE.Equals(restrictedReleaseItems[i]))).ToArray<PDI_PROJECT_ITEM_SOLUTION>();
            projectItem = ((IEnumerable<PDI_PROJECT_ITEM>) projectItem).Where<PDI_PROJECT_ITEM>((Func<PDI_PROJECT_ITEM, bool>) (val => !val.PROJ_ITEM_TYPE.Equals(restrictedReleaseItems[i]))).ToArray<PDI_PROJECT_ITEM>();
          }
        }
        this.resultSetParser.PutItemsAndFeaturesInDataSet(projectItemSolution, projectItem, feature);
      }
      catch (Exception ex)
      {
      }
    }

    public void RefreshItemsAndFeaturesInThread()
    {
      new Thread(new ThreadStart(this.RefreshItemsAndFeatures)).Start();
    }

    public PDI_PROJECT_ITEM QueryItem(string item)
    {
      RepositoryDataSet.ProjectItemRow[] projectItemRowArray = (RepositoryDataSet.ProjectItemRow[]) null;
      string filterExpression = "ProjectItemType='" + item + "'";
      try
      {
        lock (this.queryHandler.lockObject)
          projectItemRowArray = (RepositoryDataSet.ProjectItemRow[]) this.repositoryDataSet.ProjectItem.Select(filterExpression);
        if (projectItemRowArray.Length > 0)
          return new PDI_PROJECT_ITEM() { PROJECT_ITEM_KEY = projectItemRowArray[0].ProjectItemKey, PROJ_ITEM_TYPE = projectItemRowArray[0].ProjectItemType, DESCRIPTION = projectItemRowArray[0].Description, LM_CONTENT_TYPE = projectItemRowArray[0].LMContentType, KEY_USER_REQUIRED = projectItemRowArray[0].KeyUserRequired, ADD_ITEM_RELEVANT = projectItemRowArray[0].AddItemRelevant, DEL_ALLOWED_IN_MAINT = projectItemRowArray[0].DelAllowedInMaintenance };
      }
      catch (EvaluateException ex)
      {
        Trace.TraceError(ex.Message);
      }
      return (PDI_PROJECT_ITEM) null;
    }

    public PDI_PROJECT_ITEM QueryItemByDescription(string itemDescription)
    {
      RepositoryDataSet.ProjectItemRow[] projectItemRowArray = (RepositoryDataSet.ProjectItemRow[]) null;
      string filterExpression = "Description LIKE'" + itemDescription + "'";
      try
      {
        lock (this.queryHandler.lockObject)
          projectItemRowArray = (RepositoryDataSet.ProjectItemRow[]) this.repositoryDataSet.ProjectItem.Select(filterExpression);
        if (projectItemRowArray.Length > 0)
          return new PDI_PROJECT_ITEM() { PROJECT_ITEM_KEY = projectItemRowArray[0].ProjectItemKey, PROJ_ITEM_TYPE = projectItemRowArray[0].ProjectItemType, DESCRIPTION = projectItemRowArray[0].Description, LM_CONTENT_TYPE = projectItemRowArray[0].LMContentType, KEY_USER_REQUIRED = projectItemRowArray[0].KeyUserRequired, ADD_ITEM_RELEVANT = projectItemRowArray[0].AddItemRelevant };
      }
      catch (EvaluateException ex)
      {
        Trace.TraceError(ex.Message);
      }
      return (PDI_PROJECT_ITEM) null;
    }

    public PDI_PROJECT_ITEM_SOLUTION[] QueryItemSolution(string item, string sapSolutionType = null, string pdiSubType = null)
    {
      string str = "";
      if (sapSolutionType != null && sapSolutionType.Length > 0)
        str = str + "SAPSolutionType='" + sapSolutionType + "' AND ";
      if (pdiSubType != null && pdiSubType.Length > 0)
        str = str + "PDISubtype='" + pdiSubType + "' AND ";
      string filterExpression = str + "ProjectItemType='" + item + "'";
      try
      {
        lock (this.queryHandler.lockObject)
        {
          RepositoryDataSet.ProjectItemSolutionRow[] projectItemSolutionRowArray = (RepositoryDataSet.ProjectItemSolutionRow[]) this.repositoryDataSet.ProjectItemSolution.Select(filterExpression);
          if (projectItemSolutionRowArray.Length > 0)
          {
            PDI_PROJECT_ITEM_SOLUTION[] projectItemSolutionArray = new PDI_PROJECT_ITEM_SOLUTION[projectItemSolutionRowArray.Length];
            for (int index = 0; index < projectItemSolutionRowArray.Length; ++index)
              projectItemSolutionArray[index] = new PDI_PROJECT_ITEM_SOLUTION()
              {
                PROJECT_ITEM_SOLUTION_KEY = projectItemSolutionRowArray[index].ProjectItemSolutionkey,
                PROJ_ITEM_TYPE = projectItemSolutionRowArray[index].ProjectItemType,
                SAP_SOLUTION_TYPE = projectItemSolutionRowArray[index].SAPSolutionType,
                PDI_SOLUTION_SUBTYPE = projectItemSolutionRowArray[index].PDISubtype
              };
            return projectItemSolutionArray;
          }
        }
      }
      catch (EvaluateException ex)
      {
        Trace.TraceError(ex.Message);
      }
      return (PDI_PROJECT_ITEM_SOLUTION[]) null;
    }

    public List<PDI_FEATURE> QueryFeature(string featureName, string sapSolutionType = null, string pdiSubType = null)
    {
      string str = "";
      if (sapSolutionType != null && sapSolutionType.Length > 0)
        str = str + "SAPSolutionType='" + sapSolutionType + "' AND ";
      if (pdiSubType != null && pdiSubType.Length > 0)
        str = str + "PDISubtype='" + pdiSubType + "' AND ";
      string filterExpression = str + "Feature='" + featureName + "'";
      try
      {
        lock (this.queryHandler.lockObject)
        {
          RepositoryDataSet.FeatureRow[] featureRowArray = (RepositoryDataSet.FeatureRow[]) this.repositoryDataSet.Feature.Select(filterExpression);
          if (featureRowArray.Length > 0)
          {
            List<PDI_FEATURE> pdiFeatureList = new List<PDI_FEATURE>();
            foreach (RepositoryDataSet.FeatureRow featureRow in featureRowArray)
              pdiFeatureList.Add(new PDI_FEATURE()
              {
                FEATURE_KEY = featureRow.FeatureKey,
                FEATURE = featureRow.Feature,
                DESCRIPTION = featureRow.Description,
                SAP_SOLUTION_TYPE = featureRow.SAPSolutionType,
                PDI_SOLUTION_SUBTYPE = featureRow.PDISubtype
              });
            return pdiFeatureList;
          }
        }
      }
      catch (EvaluateException ex)
      {
        Trace.TraceError(ex.Message);
      }
      return (List<PDI_FEATURE>) null;
    }

    public void RefreshRoot()
    {
      lock (this.queryHandler.lockObject)
      {
        this.RefreshItemsAndFeaturesInThread();
        Dictionary<string, LastChangedItem> boLastChangedInfo = new Dictionary<string, LastChangedItem>();
        Dictionary<string, LastChangedItem> dtLastChangedInfo1 = new Dictionary<string, LastChangedItem>();
        Dictionary<string, LastChangedItem> mtLastChangedInfo = new Dictionary<string, LastChangedItem>();
        Dictionary<string, LastChangedItem> LibrariesLastChangedInfo = new Dictionary<string, LastChangedItem>();
        Dictionary<string, LastChangedItem> dtLastChangedInfo2 = new Dictionary<string, LastChangedItem>();
        foreach (RepositoryDataSet.LibrariesRow librariesRow in this.repositoryDataSet.Libraries.Select())
          LibrariesLastChangedInfo.Add(librariesRow.ProxyName, new LastChangedItem(librariesRow.ProxyName, librariesRow.LastChangeAt, librariesRow.TransitiveHash));
        foreach (RepositoryDataSet.BusinessObjectsRow businessObjectsRow in this.repositoryDataSet.BusinessObjects.Select())
          boLastChangedInfo.Add(businessObjectsRow.ProxyName, new LastChangedItem(businessObjectsRow.ProxyName, businessObjectsRow.LastChangeAt, businessObjectsRow.TransitiveHash));
        foreach (RepositoryDataSet.DataTypesRow dataTypesRow in this.repositoryDataSet.DataTypes.Select())
          dtLastChangedInfo1.Add(dataTypesRow.ProxyName, new LastChangedItem(dataTypesRow.ProxyName, dataTypesRow.LastChangeAt, dataTypesRow.TransitiveHash));
        foreach (RepositoryDataSet.BAdIsRow badIsRow in this.repositoryDataSet.BAdIs.Select())
          dtLastChangedInfo2.Add(badIsRow.ProxyName, new LastChangedItem(badIsRow.ProxyName, new DateTime()));
        this.repositoryDataSet.Clear();
        DateTime start1 = SAP.Copernicus.Core.Util.Util.startMeasurement("Getting Solutions");
        this.RefreshSolutions();
        SAP.Copernicus.Core.Util.Util.endMeasurement(start1, "Getting Solutions finished in: ");
        this.RefreshSAPNamespaces();
        DateTime start2 = SAP.Copernicus.Core.Util.Util.startMeasurement("Executing  Queries");
        string boResultXML;
        string dtResultXML;
        string mtResultXML;
        string badiResultXML;
        this.queryHandler.QueryAllSAPObjects(out boResultXML, out dtResultXML, out mtResultXML, out badiResultXML);
        string queryResult1 = this.queryHandler.QueryABSLLibraries(true);
        string queryResult2 = this.queryHandler.QueryQDLibraries(true);
        SAP.Copernicus.Core.Util.Util.endMeasurement(start2, "Queries finished in: ");
        DateTime start3 = SAP.Copernicus.Core.Util.Util.startMeasurement("Parsing query result XML.");
        this.resultSetParser.ParseBOsIntoDataSet(boResultXML, boLastChangedInfo);
        this.resultSetParser.ParseDTsIntoDataSet(dtResultXML, dtLastChangedInfo1);
        this.resultSetParser.ParseMTsIntoDataSet(mtResultXML, mtLastChangedInfo);
        this.resultSetParser.ParseLibrariesIntoDataSet(queryResult1, LibrariesLastChangedInfo);
        this.resultSetParser.ParseLibrariesIntoDataSet(queryResult2, LibrariesLastChangedInfo);
        this.resultSetParser.ParseBAdIIntoDataset(badiResultXML, dtLastChangedInfo2);
        SAP.Copernicus.Core.Util.Util.endMeasurement(start3, "Parsed in: ");
        this.RefreshPartnerNamespaces();
        if (RepositoryDataCache.RefreshEventHandlers == null)
          return;
        RepositoryDataCache.RefreshEventHandlers();
      }
    }

    public IEnumerable<DataRow> GetBAdI(string nsName, string boName)
    {
      DataTable badIs = (DataTable) this.RepositoryDataSet.BAdIs;
      return string.IsNullOrEmpty(boName) ? (IEnumerable<DataRow>) badIs.AsEnumerable().Where<DataRow>((Func<DataRow, bool>) (badi => badi.Field<string>("NSName") == nsName)) : (IEnumerable<DataRow>) badIs.AsEnumerable().Where<DataRow>((Func<DataRow, bool>) (badi =>
      {
        if (badi.Field<string>("NSName") == nsName)
          return badi.Field<string>("BusinessObject") == boName;
        return false;
      }));
    }

    public IEnumerable<DataRow> GetNSHavingBAdI()
    {
      DataTable badIs = (DataTable) this.RepositoryDataSet.BAdIs;
      List<string> stringList = new List<string>();
      return (IEnumerable<DataRow>) badIs.DefaultView.ToTable(true, "NSName").Select();
    }

    public IEnumerable<DataRow> GetBAdI(string badiProxyName)
    {
      return (IEnumerable<DataRow>) this.RepositoryDataSet.BAdIs.AsEnumerable().Where<DataRow>((Func<DataRow, bool>) (badi => badi.Field<string>("ProxyName") == badiProxyName));
    }

    public IEnumerable<DataRow> GetBAdINSandName(string badiName, string nameSpace)
    {
      DataTable badIs = (DataTable) this.RepositoryDataSet.BAdIs;
      string filterExpression = "";
      if (nameSpace != null && nameSpace.Length > 0)
        filterExpression = filterExpression + "NSName LIKE '" + nameSpace + "' ";
      if (badiName != null && badiName.Length > 0)
        filterExpression = !(filterExpression != "") ? filterExpression + "Name LIKE '" + badiName + "' " : filterExpression + "AND Name LIKE '" + badiName + "' ";
      return (IEnumerable<DataRow>) badIs.Select(filterExpression);
    }

    public IEnumerable<DataRow> GetOperationNames(string badiProxyName)
    {
      DataTable badImethods = (DataTable) this.RepositoryDataSet.BAdIMethods;
      return string.IsNullOrEmpty(badiProxyName) ? (IEnumerable<DataRow>) null : (IEnumerable<DataRow>) badImethods.AsEnumerable().Where<DataRow>((Func<DataRow, bool>) (badiMethod => badiMethod.Field<string>("BAdIProxyName") == badiProxyName));
    }

    public IEnumerable<DataRow> GetFilters(string badiProxyName)
    {
      DataTable badIfilters = (DataTable) this.RepositoryDataSet.BAdIFilters;
      return string.IsNullOrEmpty(badiProxyName) ? (IEnumerable<DataRow>) null : (IEnumerable<DataRow>) badIfilters.AsEnumerable().Where<DataRow>((Func<DataRow, bool>) (badiFilter => badiFilter.Field<string>("BAdIProxyName") == badiProxyName));
    }

    public List<string> GetAllDataTypes()
    {
      return this.GetDataTypes((string) null);
    }

    public void RefreshSAPNamespaces()
    {
      lock (this.queryHandler.lockObject)
      {
        foreach (string refreshSapNamespace in this.queryHandler.RefreshSAPNamespaces())
          this.repositoryDataSet.Namespaces.AddNamespacesRow(refreshSapNamespace);
      }
    }

    public List<string> GetDataTypes(string nsName)
    {
      List<string> stringList = new List<string>();
      lock (this.queryHandler.lockObject)
      {
        foreach (RepositoryDataSet.DataTypesRow dataTypesRow in nsName == null ? (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select() : (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select("NSName='" + nsName + "'"))
          stringList.Add(dataTypesRow.Name);
      }
      return stringList;
    }

    public List<string> GetMessages(string nsName)
    {
      return new List<string>();
    }

    public RepositoryDataSet.DataTypesRow GetDataTypeByProxyName(string dtPrxName)
    {
      RepositoryDataSet.DataTypesRow dataTypesRow = (RepositoryDataSet.DataTypesRow) null;
      lock (this.queryHandler.lockObject)
      {
        RepositoryDataSet.DataTypesRow[] dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select("ProxyName='" + dtPrxName + "'");
        if (dataTypesRowArray != null)
        {
          if (((IEnumerable<RepositoryDataSet.DataTypesRow>) dataTypesRowArray).Count<RepositoryDataSet.DataTypesRow>() == 1)
            dataTypesRow = dataTypesRowArray[0];
        }
      }
      return dataTypesRow;
    }

    public string GetDTNameForPrxName(string dtPrxName)
    {
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        RepositoryDataSet.DataTypesRow[] dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select("ProxyName='" + dtPrxName + "'");
        if (dataTypesRowArray != null)
        {
          if (((IEnumerable<RepositoryDataSet.DataTypesRow>) dataTypesRowArray).Count<RepositoryDataSet.DataTypesRow>() == 1)
            str = dataTypesRowArray[0].Name;
        }
      }
      return str;
    }

    public string GetDTProxyName(string dtNamespace, string dtName)
    {
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        RepositoryDataSet.DataTypesRow[] dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select("NSName='" + dtNamespace + "' AND Name='" + dtName + "'");
        if (dataTypesRowArray != null)
        {
          if (((IEnumerable<RepositoryDataSet.DataTypesRow>) dataTypesRowArray).Count<RepositoryDataSet.DataTypesRow>() == 1)
            str = dataTypesRowArray[0].ProxyName;
        }
      }
      return str;
    }

    public List<string> GetNamespaces()
    {
      List<string> stringList = new List<string>();
      lock (this.queryHandler.lockObject)
      {
        foreach (RepositoryDataSet.NamespacesRow namespacesRow in (RepositoryDataSet.NamespacesRow[]) this.repositoryDataSet.Tables["Namespaces"].Select())
          stringList.Add(namespacesRow.NSName);
      }
      return stringList;
    }

    public RepositoryDataSet.BusinessObjectsRow[] QueryBOs(string category, string techCategory, string nameSpace, string proxyName, string name, string deploymentUnit, string writeaccess)
    {
      RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) null;
      string filterExpression = "";
      if (nameSpace != null && nameSpace.Length > 0)
        filterExpression = filterExpression + "NSName LIKE '" + nameSpace + "' AND ";
      if (name != null && name.Length > 0)
        filterExpression = filterExpression + "Name LIKE '" + name + "' AND ";
      if (category != null && category.Length > 0)
        filterExpression = filterExpression + "ObjectCategory IN ('" + category + "') AND ";
      if (techCategory != null && techCategory.Length > 0)
        filterExpression = filterExpression + "TechCategory='" + techCategory + "' AND ";
      if (proxyName != null && proxyName.Length > 0)
        filterExpression = filterExpression + "ProxyName LIKE '" + proxyName + "' AND ";
      if (deploymentUnit != null && deploymentUnit.Length > 0)
        filterExpression = filterExpression + "DeploymentUnit='" + deploymentUnit + "' AND ";
      if (writeaccess != null && writeaccess.Length > 0)
        filterExpression = filterExpression + "WriteAccess  LIKE '*" + writeaccess.Substring(1) + "'";
      if (filterExpression.EndsWith("AND "))
        filterExpression = filterExpression.Substring(0, filterExpression.Length - 4);
      try
      {
        lock (this.queryHandler.lockObject)
          businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select(filterExpression);
      }
      catch (EvaluateException ex)
      {
        Trace.TraceError(ex.Message);
      }
      return businessObjectsRowArray;
    }

    public RepositoryDataSet.DataTypesRow[] QueryDataTypes(string nameSpace, string proxyName, string name, string dtUUID, string BaseDTKeyName, string UsageCategory)
    {
      RepositoryDataSet.DataTypesRow[] dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) null;
      string filterExpression = "";
      if (nameSpace != null && nameSpace.Length > 0)
        filterExpression = filterExpression + "NSName LIKE '" + nameSpace + "' AND ";
      if (name != null && name.Length > 0)
        filterExpression = filterExpression + "Name LIKE '" + name + "' AND ";
      if (proxyName != null && proxyName.Length > 0)
        filterExpression = filterExpression + "ProxyName LIKE '" + proxyName + "' AND ";
      if (dtUUID != null && dtUUID.Length > 0)
        filterExpression = filterExpression + "DT_UUID='" + dtUUID + "' AND ";
      if (BaseDTKeyName != null && BaseDTKeyName.Length > 0)
        filterExpression = filterExpression + "BaseDTKeyName='" + BaseDTKeyName + "' AND ";
      if (UsageCategory != null && UsageCategory.Length > 0)
        filterExpression = filterExpression + "UsageCategory='" + UsageCategory + "'";
      if (filterExpression.EndsWith("AND "))
        filterExpression = filterExpression.Substring(0, filterExpression.Length - 4);
      try
      {
        lock (this.queryHandler.lockObject)
          dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select(filterExpression);
      }
      catch (EvaluateException ex)
      {
        Trace.TraceError(ex.Message);
      }
      return dataTypesRowArray;
    }

    public RepositoryDataSet.NodesRow[] QueryNodes(string boProxyName, string name, string proxyName, string parentNodeName)
    {
      RepositoryDataSet.NodesRow[] nodesRowArray = (RepositoryDataSet.NodesRow[]) null;
      string filterExpression = "";
      if (boProxyName != null && boProxyName.Length > 0)
        filterExpression = filterExpression + "BOProxyName LIKE '" + boProxyName + "' AND ";
      if (name != null && name.Length > 0)
        filterExpression = filterExpression + "Name LIKE '" + name + "' AND ";
      if (proxyName != null && proxyName.Length > 0)
        filterExpression = filterExpression + "ProxyName LIKE '" + proxyName + "' AND ";
      if (parentNodeName != null)
        filterExpression = filterExpression + "ParentNodeName='" + parentNodeName + "'";
      if (filterExpression.EndsWith("AND "))
        filterExpression = filterExpression.Substring(0, filterExpression.Length - 4);
      try
      {
        lock (this.queryHandler.lockObject)
          nodesRowArray = (RepositoryDataSet.NodesRow[]) this.repositoryDataSet.Tables["Nodes"].Select(filterExpression);
      }
      catch (EvaluateException ex)
      {
        Trace.TraceError(ex.Message);
      }
      return nodesRowArray;
    }

    public List<string> GetBONames()
    {
      List<string> stringList = new List<string>();
      lock (this.queryHandler.lockObject)
      {
        foreach (RepositoryDataSet.BusinessObjectsRow businessObjectsRow in (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select())
          stringList.Add(businessObjectsRow.Name);
      }
      return stringList;
    }

    public List<string> GetBONames(string namespaceName)
    {
      List<string> stringList = new List<string>();
      lock (this.queryHandler.lockObject)
      {
        if (string.IsNullOrEmpty(namespaceName))
          return stringList;
        foreach (RepositoryDataSet.BusinessObjectsRow businessObjectsRow in (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select("NSName='" + namespaceName + "'"))
          stringList.Add(businessObjectsRow.Name);
      }
      return stringList;
    }

    public List<string> GetBONodeNames(string namespaceName, string BOName)
    {
      List<string> stringList = new List<string>();
      lock (this.queryHandler.lockObject)
      {
        if (string.IsNullOrEmpty(namespaceName) || string.IsNullOrEmpty(BOName))
          return stringList;
        RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select("NsName='" + namespaceName + "' AND Name='" + BOName + "'");
        if (businessObjectsRowArray == null || businessObjectsRowArray.Length == 0)
          return stringList;
        foreach (RepositoryDataSet.NodesRow nodesRow in businessObjectsRowArray[0].GetNodesRows())
          stringList.Add(nodesRow.Name);
      }
      return stringList;
    }

    public string GetProxyNameFromBO(string namespaceName, string boName)
    {
      if (string.IsNullOrEmpty(namespaceName) || string.IsNullOrEmpty(boName))
        return (string) null;
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select("NsName='" + namespaceName + "' AND Name='" + boName + "'");
        if (businessObjectsRowArray != null && businessObjectsRowArray.Length > 0)
          str = businessObjectsRowArray[0].ProxyName;
        else
          Trace.TraceWarning("RepositoryDataCache.getProxyNameFromBO: No such BO - " + namespaceName + ":" + boName);
      }
      return str;
    }

    public bool BOExists(string boName)
    {
      bool flag = false;
      lock (this.queryHandler.lockObject)
      {
        if (boName != null)
        {
          RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select("Name='" + boName + "'");
          if (businessObjectsRowArray != null)
          {
            if (businessObjectsRowArray.Length > 0)
              flag = true;
          }
        }
      }
      return flag;
    }

    public string GetNodeProxyNameFromBOAndNode(string boProxyName, string nodeName, string solutionNamespace = "")
    {
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        if (nodeName != null)
        {
          RepositoryDataSet.NodesRow[] nodesRowArray1 = (RepositoryDataSet.NodesRow[]) this.repositoryDataSet.Tables["Nodes"].Select("BOProxyName='" + boProxyName + "' AND Name='" + nodeName + "'");
          if (nodesRowArray1 != null && nodesRowArray1.Length > 0)
          {
            str = nodesRowArray1[0].ProxyName;
            if (str.StartsWith("Y") & solutionNamespace != "")
            {
              string[] strArray1 = str.Split('_');
              string[] strArray2 = solutionNamespace.Split('/');
              if (strArray1.Length == 2)
              {
                if (strArray2.Length == 4)
                {
                  if (strArray2[3] != strArray1[0] + "_")
                  {
                    RepositoryDataSet.NodesRow[] nodesRowArray2 = (RepositoryDataSet.NodesRow[]) this.repositoryDataSet.Tables["Nodes"].Select("ProxyName='" + strArray2[3] + strArray1[1] + "'");
                    if (nodesRowArray2 != null)
                    {
                      if (nodesRowArray2.Length > 0)
                        str = nodesRowArray2[0].ProxyName;
                    }
                  }
                }
              }
            }
          }
          else
            Trace.TraceWarning("RepositoryDataCache.getNodeProxyNameFromBOAndNode: No such BO/Node - " + boProxyName + "/" + nodeName);
        }
      }
      return str;
    }

    public string GetBONameFromProxyName(string boProxyName)
    {
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        if (boProxyName != null)
        {
          RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select("ProxyName='" + boProxyName + "'");
          if (businessObjectsRowArray != null && businessObjectsRowArray.Length > 0)
            str = businessObjectsRowArray[0].Name;
          else
            Trace.TraceWarning("RepositoryDataCache.getBONameFromProxyName: No such BO - " + boProxyName);
        }
      }
      return str;
    }

    public string GetTransDefNameFromProxyName(string transDefProxyName)
    {
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        if (transDefProxyName != null)
        {
          RepositoryDataSet.LibrariesRow[] librariesRowArray = (RepositoryDataSet.LibrariesRow[]) this.repositoryDataSet.Tables["Libraries"].Select("ProxyName='" + transDefProxyName + "'");
          if (librariesRowArray != null && librariesRowArray.Length > 0)
            str = librariesRowArray[0].Name;
          else
            Trace.TraceWarning("RepositoryDataCache.GetTransDefNameFromProxyName: No such Transformation Definition - " + transDefProxyName);
        }
      }
      return str;
    }

    private string GetNodeNameFromProxyName(string boProxyName, string nodeProxyName)
    {
      return this.GetNodeNameFromProxyName(boProxyName, nodeProxyName, (string) null);
    }

    public string GetNodeNameFromProxyName(string boProxyName, string nodeProxyName, string doPrefixName)
    {
      string str1 = (string) null;
      lock (this.queryHandler.lockObject)
      {
        if (boProxyName != null)
        {
          if (nodeProxyName != null)
          {
            RepositoryDataSet.NodesRow[] nodesRowArray = (RepositoryDataSet.NodesRow[]) this.repositoryDataSet.Tables["Nodes"].Select("BOProxyName='" + boProxyName + "' AND ProxyName='" + nodeProxyName + "'");
            if (nodesRowArray != null && nodesRowArray.Length > 0)
            {
              if (doPrefixName != null)
              {
                foreach (string str2 in ((IEnumerable<RepositoryDataSet.NodesRow>) nodesRowArray).Where<RepositoryDataSet.NodesRow>((Func<RepositoryDataSet.NodesRow, bool>) (n => n.Name.StartsWith(doPrefixName + "."))).Select<RepositoryDataSet.NodesRow, string>((Func<RepositoryDataSet.NodesRow, string>) (n => n.Name)).ToArray<string>())
                {
                  if (!str2.Substring(doPrefixName.Length + 1).Contains<char>('.'))
                  {
                    str1 = str2;
                    break;
                  }
                }
              }
              else
                str1 = ((IEnumerable<RepositoryDataSet.NodesRow>) nodesRowArray).Single<RepositoryDataSet.NodesRow>((Func<RepositoryDataSet.NodesRow, bool>) (n => !n.Name.Contains("."))).Name;
            }
            else
              Trace.TraceError("RepositoryDataCache: Failed to lookup external node name from proxy names - BO/Node: " + boProxyName + "/" + nodeProxyName);
          }
        }
      }
      return str1;
    }

    public string GetUUIDForBOProxyName(string boProxyName)
    {
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        if (boProxyName != null)
        {
          RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select("ProxyName='" + boProxyName + "'");
          if (businessObjectsRowArray != null && businessObjectsRowArray.Length > 0)
            str = businessObjectsRowArray[0].BO_UUID;
          else
            Trace.TraceWarning("RepositoryDataCache.GetUUIDForBOProxyName: No such BO - " + boProxyName);
        }
      }
      return str;
    }

    public string GetNamespaceForBOProxyName(string boProxyName)
    {
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        if (boProxyName != null)
        {
          RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select("ProxyName='" + boProxyName + "'");
          if (businessObjectsRowArray != null && businessObjectsRowArray.Length > 0)
            str = businessObjectsRowArray[0].NSName;
          else
            Trace.TraceWarning("RepositoryDataCache.GetNamespaceForBOProxyName: No such BO - " + boProxyName);
        }
      }
      return str;
    }

    public string GetNamespaceForTransDefProxyName(string transDefProxyName)
    {
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        if (transDefProxyName != null)
        {
          RepositoryDataSet.LibrariesRow[] librariesRowArray = (RepositoryDataSet.LibrariesRow[]) this.repositoryDataSet.Tables["Libraries"].Select("ProxyName='" + transDefProxyName + "'");
          if (librariesRowArray != null && librariesRowArray.Length > 0)
            str = librariesRowArray[0].Namespace;
          else
            Trace.TraceWarning("RepositoryDataCache.GetNamespaceForTransDefProxyName: No such Transformation Definition - " + transDefProxyName);
        }
      }
      return str;
    }

    public string GetUUIDForDTProxyName(string dtProxyName)
    {
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        if (dtProxyName != null)
        {
          RepositoryDataSet.DataTypesRow[] dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select("ProxyName='" + dtProxyName + "'");
          if (dataTypesRowArray != null && dataTypesRowArray.Length > 0)
            str = dataTypesRowArray[0].DT_UUID;
          else
            Trace.TraceWarning("RepositoryDataCache.GetUUIDForDTProxyName: No such DT - " + dtProxyName);
        }
      }
      return str;
    }

    public string GetNamespaceForDTProxyName(string dtProxyName)
    {
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        if (dtProxyName != null)
        {
          RepositoryDataSet.DataTypesRow[] dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select("ProxyName='" + dtProxyName + "'");
          if (dataTypesRowArray != null && dataTypesRowArray.Length > 0)
            str = dataTypesRowArray[0].NSName;
          else
            Trace.TraceWarning("RepositoryDataCache.GetNamespaceForDTProxyName: No such DT - " + dtProxyName);
        }
      }
      return str;
    }

    public IList<string> GetNodeNamesForBO(string boProxyName)
    {
      IList<string> stringList = (IList<string>) new List<string>();
      lock (this.queryHandler.lockObject)
      {
        foreach (RepositoryDataSet.NodesRow nodesRow in (RepositoryDataSet.NodesRow[]) this.repositoryDataSet.Tables["Nodes"].Select("BOProxyName='" + boProxyName + "'"))
          stringList.Add(nodesRow.Name);
      }
      return stringList;
    }

    public string GetProxyNameForQualifiedDTName(string nsName, string typeName)
    {
      string str1 = (string) null;
      lock (this.queryHandler.lockObject)
      {
        if (nsName != null)
        {
          if (typeName != null)
          {
            string str2 = nsName.StartsWith("http://") ? nsName : ImportPathUtil.ConvertImportFromInternalToESR(nsName);
            RepositoryDataSet.DataTypesRow[] dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select("Name='" + typeName + "' AND NSName='" + str2 + "'");
            if (dataTypesRowArray != null && dataTypesRowArray.Length > 0)
              str1 = dataTypesRowArray[0].ProxyName;
            else
              Trace.TraceWarning("RepositoryDataCache.GetProxyNameForQualifiedDTName: No such DT - " + nsName + (object) '/' + typeName);
          }
        }
      }
      return str1;
    }

    public RepositoryDataSet.DataTypesRow[] GetMatchingTypesForDTName(string typeName)
    {
      RepositoryDataSet.DataTypesRow[] dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) null;
      lock (this.queryHandler.lockObject)
      {
        if (typeName != null)
        {
          dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select("Name='" + typeName + "'");
          if (dataTypesRowArray == null)
            Trace.TraceWarning("RepositoryDataCache.GetMatchingTypesForDTName: No such DT - " + typeName);
        }
      }
      return dataTypesRowArray;
    }

    public RepositoryDataSet.BusinessObjectsRow[] GetMatchingBusinessObjectsForName(string boName)
    {
      if (boName == null)
        return (RepositoryDataSet.BusinessObjectsRow[]) null;
      RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) null;
      lock (this.queryHandler.lockObject)
      {
        businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select("Name='" + boName + "'");
        if (businessObjectsRowArray == null)
          Trace.TraceWarning("RepositoryDataCache.GetMatchingBusinessObjectsForName: No such BO - " + boName);
      }
      return businessObjectsRowArray;
    }

    public RepositoryDataSet.DataTypesRow[] GetMatchingDataTypesForPattern(string dtNamePattern)
    {
      if (dtNamePattern == null)
        return (RepositoryDataSet.DataTypesRow[]) null;
      RepositoryDataSet.DataTypesRow[] dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) null;
      lock (this.queryHandler.lockObject)
      {
        dataTypesRowArray = (RepositoryDataSet.DataTypesRow[]) this.repositoryDataSet.Tables["DataTypes"].Select("Name LIKE '" + dtNamePattern + "'");
        if (dataTypesRowArray == null)
          Trace.TraceWarning("RepositoryDataCache.GetMatchingDataTypesForPattern: No such DT - " + dtNamePattern);
      }
      return dataTypesRowArray;
    }

    public void DeleteBusinessObject(string namespaceName, string name)
    {
      lock (this.queryHandler.lockObject)
      {
        RepositoryDataSet.BusinessObjectsDataTable businessObjects = this.repositoryDataSet.BusinessObjects;
        string filterExpression = "NsName='" + namespaceName + "' AND Name='" + name + "'";
        foreach (RepositoryDataSet.BusinessObjectsRow businessObjectsRow in businessObjects.Select(filterExpression))
        {
          foreach (DataRow childRow in businessObjectsRow.GetChildRows("BusinessObjects_Nodes"))
            childRow.Delete();
          string proxyName = businessObjectsRow.ProxyName;
          if (RepositoryDataCache.ModelChangeEventHandlerRegistry != null)
            RepositoryDataCache.ModelChangeEventHandlerRegistry(RepositoryDataCache.MetaObjectTypes.BusinessObject, proxyName, true);
          businessObjectsRow.Delete();
        }
      }
    }

    public string[] GetTransDefProxyNames(string name, string ns)
    {
      List<string> stringList = new List<string>();
      lock (this.queryHandler.lockObject)
      {
        if (name != null)
        {
          if (ns != null)
          {
            DataTable table = this.repositoryDataSet.Tables["Libraries"];
            string filterExpression = "ProxyName LIKE '" + name + "' AND Namespace='" + ns + "'";
            foreach (RepositoryDataSet.LibrariesRow librariesRow in (RepositoryDataSet.LibrariesRow[]) table.Select(filterExpression))
              stringList.Add(librariesRow.ProxyName);
          }
        }
      }
      return stringList.ToArray();
    }

    private string GetTransitiveHashFor(string tableName, string moProxyName)
    {
      if (moProxyName == null || tableName == null || !this.repositoryDataSet.Tables.Contains(tableName))
        return RepositoryDataCache.NOT_IN_CACHE;
      string str = (string) null;
      lock (this.queryHandler.lockObject)
      {
        DataRow[] dataRowArray = this.repositoryDataSet.Tables[tableName].Select("ProxyName='" + moProxyName + "'");
        if (dataRowArray != null)
        {
          if (dataRowArray.Length > 0)
            str = dataRowArray[0].Field<string>("TransitiveHash");
        }
      }
      return str ?? RepositoryDataCache.NOT_IN_CACHE;
    }

    public string GetLifeCycleStatusForBO(string boProxyName)
    {
      if (boProxyName == null)
        return (string) null;
      RepositoryDataSet.BusinessObjectsRow[] businessObjectsRowArray;
      lock (this.queryHandler.lockObject)
        businessObjectsRowArray = (RepositoryDataSet.BusinessObjectsRow[]) this.repositoryDataSet.Tables["BusinessObjects"].Select("ProxyName='" + boProxyName + "'");
      if (businessObjectsRowArray != null && businessObjectsRowArray.Length > 0)
        return businessObjectsRowArray[0].LifecycleStat;
      return (string) null;
    }

    internal string GetHashOfTransitiveClosureFor(RepositoryDataCache.MetaObjectTypes moType, string moProxyName)
    {
      switch (moType)
      {
        case RepositoryDataCache.MetaObjectTypes.BusinessObject:
          return this.GetTransitiveHashFor("BusinessObjects", moProxyName);
        case RepositoryDataCache.MetaObjectTypes.DataType:
          return this.GetTransitiveHashFor("DataTypes", moProxyName);
        case RepositoryDataCache.MetaObjectTypes.Library:
          return this.GetTransitiveHashFor("Libraries", moProxyName);
        case RepositoryDataCache.MetaObjectTypes.MessageType:
          return this.GetTransitiveHashFor("MessageTypes", moProxyName);
        default:
          return RepositoryDataCache.NOT_IN_CACHE;
      }
    }

    public void ChangeDataType(string proxyName, object[] dataType)
    {
      lock (this.queryHandler.lockObject)
      {
        DataRow[] dataRowArray = this.repositoryDataSet.Tables["DataTypes"].Select("ProxyName='" + proxyName + "'");
        if (dataRowArray == null || dataRowArray.Length <= 0)
          return;
        dataRowArray[0].ItemArray = dataType;
        if (RepositoryDataCache.ModelChangeEventHandlerRegistry == null)
          return;
        RepositoryDataCache.ModelChangeEventHandlerRegistry(RepositoryDataCache.MetaObjectTypes.DataType, proxyName, false);
      }
    }

    internal string AddMetaObject(RepositoryDataCache.MetaObjectTypes moType, string moProxyName, string xmlContent)
    {
      string transitiveClosureFor = this.GetHashOfTransitiveClosureFor(moType, moProxyName);
      Dictionary<string, LastChangedItem> dictionary = new Dictionary<string, LastChangedItem>();
      dictionary.Add(moProxyName, new LastChangedItem(moProxyName, DateTime.MinValue, transitiveClosureFor));
      lock (this.queryHandler.lockObject)
      {
        switch (moType)
        {
          case RepositoryDataCache.MetaObjectTypes.BusinessObject:
            this.resultSetParser.ParseBOsIntoDataSet(xmlContent, dictionary);
            break;
          case RepositoryDataCache.MetaObjectTypes.DataType:
            this.resultSetParser.ParseDTsIntoDataSet(xmlContent, dictionary);
            break;
          case RepositoryDataCache.MetaObjectTypes.Library:
            this.resultSetParser.ParseLibrariesIntoDataSet(xmlContent, dictionary);
            break;
          case RepositoryDataCache.MetaObjectTypes.MessageType:
            this.resultSetParser.ParseMTsIntoDataSet(xmlContent, dictionary);
            break;
        }
        return this.GetHashOfTransitiveClosureFor(moType, moProxyName);
      }
    }

    public void DeleteNamespaces(IList<string> namespaces)
    {
      if (namespaces == null)
        return;
      lock (this.queryHandler.lockObject)
      {
        foreach (string str in (IEnumerable<string>) namespaces)
        {
          foreach (DataRow dataRow in this.repositoryDataSet.Namespaces.Select("NsName='" + str + "'"))
          {
            foreach (DataRow childRow1 in dataRow.GetChildRows("Namespaces_BusinessObjects"))
            {
              foreach (DataRow childRow2 in childRow1.GetChildRows("BusinessObjects_Nodes"))
                childRow2.Delete();
              childRow1.Delete();
            }
            foreach (DataRow childRow in dataRow.GetChildRows("Namespaces_DataTypes"))
              childRow.Delete();
            dataRow.Delete();
          }
        }
      }
    }

    public void DeleteSolution(string solutionName, string solutionVersion)
    {
      lock (this.queryHandler.lockObject)
      {
        RepositoryDataSet.SolutionsDataTable solutions = this.repositoryDataSet.Solutions;
        string filterExpression = "Name='" + solutionName + "' AND Version='" + solutionVersion + "'";
        foreach (DataRow dataRow in solutions.Select(filterExpression))
          dataRow.Delete();
      }
    }

    private void setMsgDataTypeNodes()
    {
      foreach (RepositoryDataSet.MessageTypesRow row in (InternalDataCollectionBase) this.repositoryDataSet.MessageTypes.Rows)
        ;
    }

    public RepositoryDataSet.MessageTypesRow GetMessageTypeByName(string mtNameSpace, string mtName)
    {
      foreach (RepositoryDataSet.MessageTypesRow row in (InternalDataCollectionBase) this.repositoryDataSet.MessageTypes.Rows)
      {
        if (row.Namespace.Equals(mtNameSpace) && row.Name.Equals(mtName))
          return row;
      }
      return (RepositoryDataSet.MessageTypesRow) null;
    }

    public RepositoryDataSet.MessageTypesRow GetMessageTypeByProxyName(string mtProxyName)
    {
      foreach (RepositoryDataSet.MessageTypesRow row in (InternalDataCollectionBase) this.repositoryDataSet.MessageTypes.Rows)
      {
        if (row.ProxyName.Equals(mtProxyName))
          return row;
      }
      return (RepositoryDataSet.MessageTypesRow) null;
    }

    public string GetMessageTypeDataType(string mtNameSpace, string mtName)
    {
      foreach (RepositoryDataSet.MessageTypesRow row in (InternalDataCollectionBase) this.repositoryDataSet.MessageTypes.Rows)
      {
        if (row.Namespace.Equals(mtNameSpace) && row.Name.Equals(mtName))
          return row.DataTypeName;
      }
      return (string) null;
    }

    public enum MetaObjectTypes
    {
      Unknown,
      BusinessObject,
      DataType,
      Library,
      MessageType,
    }
  }
}
