﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Repository.ExtensionFieldType
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Repository
{
  public class ExtensionFieldType
  {
    private string name;
    private string extensibilityName;
    private string esrNameSpace;
    private string esrName;
    private string proxyName;

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public string ExtensibilityName
    {
      get
      {
        return this.extensibilityName;
      }
      set
      {
        this.extensibilityName = value;
      }
    }

    public string EsrNameSpace
    {
      get
      {
        return this.esrNameSpace;
      }
      set
      {
        this.esrNameSpace = value;
      }
    }

    public string EsrName
    {
      get
      {
        return this.esrName;
      }
      set
      {
        this.esrName = value;
      }
    }

    public string ProxyName
    {
      get
      {
        return this.proxyName;
      }
      set
      {
        this.proxyName = value;
      }
    }
  }
}
