﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Events.SolutionEventHandlerRegistry
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Threading;

namespace SAP.Copernicus.Core.Events
{
  public class SolutionEventHandlerRegistry
  {
    public static readonly SolutionEventHandlerRegistry INSTANCE = new SolutionEventHandlerRegistry();
    public EventHandlerOnSolutionClose onSolutionClose;
    public Action onSolutionOpen;
    public Action<bool> onMashupRootItemCreate;
    public Action<bool> onServiceRootItemCreate;
    public Action<bool> onPortBindingRootItemCreate;

    public void AddOnSolutionCloseEventHandler(EventHandlerOnSolutionClose handler)
    {
      this.onSolutionClose += handler;
    }

    private void ThreadSafeCallback(Delegate _targetDelegate, params object[] _parameters)
    {
      Delegate @delegate = Interlocked.CompareExchange<Delegate>(ref _targetDelegate, (Delegate) null, (Delegate) null);
      if ((object) @delegate == null)
        return;
      @delegate.DynamicInvoke(_parameters);
    }

    public void RaiseSolutionCloseEvent()
    {
      if (this.onSolutionClose == null)
        return;
      this.ThreadSafeCallback((Delegate) this.onSolutionClose);
    }

    public void AddOnSolutionOpenEventHandler(Action openSolutionHandler)
    {
      this.onSolutionOpen += openSolutionHandler;
    }

    public void RaiseSolutionOpenEvent()
    {
      if (this.onSolutionOpen == null)
        return;
      this.ThreadSafeCallback((Delegate) this.onSolutionOpen);
    }

    public void RaiseMashupRootFolderCreate(bool isMashupItemLoadForFirstTime)
    {
      if (this.onMashupRootItemCreate == null)
        return;
      this.ThreadSafeCallback((Delegate) this.onMashupRootItemCreate, (object) isMashupItemLoadForFirstTime);
    }

    public void AddMashupRootFolderCreatedEventHandler(Action<bool> createMashupRootFolderHandler)
    {
      this.onMashupRootItemCreate += createMashupRootFolderHandler;
    }

    public void RaiseServiceRootFolderCreate(bool isServiceItemLoadForFirstTime)
    {
      if (this.onServiceRootItemCreate == null)
        return;
      this.ThreadSafeCallback((Delegate) this.onServiceRootItemCreate, (object) isServiceItemLoadForFirstTime);
    }

    public void AddServiceRootFolderCreatedEventHandler(Action<bool> createServiceRootFolderHandler)
    {
      this.onServiceRootItemCreate += createServiceRootFolderHandler;
    }

    public void RaisePortBindingRootFolderCreate(bool isPortBindingItemLoadForFirstTime)
    {
      if (this.onPortBindingRootItemCreate == null)
        return;
      this.ThreadSafeCallback((Delegate) this.onPortBindingRootItemCreate, (object) isPortBindingItemLoadForFirstTime);
    }

    public void AddPortBindingRootFolderCreatedEventHandler(Action<bool> createPortBindingRootFolderHandler)
    {
      this.onPortBindingRootItemCreate += createPortBindingRootFolderHandler;
    }
  }
}
