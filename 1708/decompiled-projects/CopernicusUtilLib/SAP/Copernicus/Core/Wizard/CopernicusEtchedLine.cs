﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Wizard.CopernicusEtchedLine
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Wizard
{
  public class CopernicusEtchedLine : UserControl
  {
    private Color _darkColor = SystemColors.ControlDark;
    private Color _lightColor = SystemColors.ControlLightLight;
    private Container components;
    private EtchEdge _edge;

    [Category("Appearance")]
    private Color DarkColor
    {
      get
      {
        return this._darkColor;
      }
      set
      {
        this._darkColor = value;
        this.Refresh();
      }
    }

    [Category("Appearance")]
    private Color LightColor
    {
      get
      {
        return this._lightColor;
      }
      set
      {
        this._lightColor = value;
        this.Refresh();
      }
    }

    [Category("Appearance")]
    public EtchEdge Edge
    {
      get
      {
        return this._edge;
      }
      set
      {
        this._edge = value;
        this.Refresh();
      }
    }

    public CopernicusEtchedLine()
    {
      this.InitializeComponent();
      this.SetStyle(ControlStyles.Selectable, false);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.SuspendLayout();
      this.AutoScroll = true;
      this.AutoSize = true;
      this.Name = "CopernicusEtchedLine";
      this.ResumeLayout(false);
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);
      Brush brush1 = (Brush) new SolidBrush(this._lightColor);
      Brush brush2 = (Brush) new SolidBrush(this._darkColor);
      Pen pen1 = new Pen(brush1, 1f);
      Pen pen2 = new Pen(brush2, 1f);
      if (this.Edge == EtchEdge.Top)
      {
        e.Graphics.DrawLine(pen2, 0, 0, this.Width, 0);
        e.Graphics.DrawLine(pen1, 0, 1, this.Width, 1);
      }
      else
      {
        if (this.Edge != EtchEdge.Bottom)
          return;
        e.Graphics.DrawLine(pen2, 0, this.Height - 2, this.Width, this.Height - 2);
        e.Graphics.DrawLine(pen1, 0, this.Height - 1, this.Width, this.Height - 1);
      }
    }

    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);
      this.Refresh();
    }
  }
}
