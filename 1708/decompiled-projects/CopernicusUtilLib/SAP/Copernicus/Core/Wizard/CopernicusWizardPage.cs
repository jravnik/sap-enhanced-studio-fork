﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Wizard.CopernicusWizardPage
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Wizard
{
  [DefaultEvent("SetActive")]
  public class CopernicusWizardPage : UserControl
  {
    private Container components;

    [Category("Wizard")]
    public event CancelEventHandler SetActive;

    [Category("Wizard")]
    public event WizardPageEventHandler WizardNext;

    [Category("Wizard")]
    public event WizardPageEventHandler WizardBack;

    [Category("Wizard")]
    public event CancelEventHandler WizardFinish;

    [Category("Wizard")]
    public event CancelEventHandler QueryCancel;

    public CopernicusWizardPage()
    {
      this.InitializeComponent();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.SuspendLayout();
      this.AutoScroll = true;
      this.AutoSize = true;
      this.Name = "CopernicusWizardPage";
      this.Size = new Size(1103, 845);
      this.ResumeLayout(false);
    }

    protected CopernicusWizardSheet GetWizard()
    {
      return (CopernicusWizardSheet) this.ParentForm;
    }

    protected void SetWizardButtons(WizardPageType pageType)
    {
      this.GetWizard().SetWizardButtons(pageType);
    }

    protected void EnableNextButton(bool enableButton)
    {
      this.GetWizard().EnableNextButton(enableButton);
    }

    protected void EnableFinishButton(bool enableButton)
    {
      this.GetWizard().EnableFinishButton(enableButton);
    }

    protected void EnableBackButton(bool enableButton)
    {
      this.GetWizard().EnableBackButton(enableButton);
    }

    protected void EnableCancelButton(bool enableButton)
    {
      this.GetWizard().EnableCancelButton(enableButton);
    }

    protected void PressButton(WizardButtons buttons)
    {
      this.GetWizard().PressButton(buttons);
    }

    public virtual void OnSetActive(CancelEventArgs e)
    {
      if (this.SetActive == null)
        return;
      this.SetActive((object) this, e);
    }

    public virtual void OnWizardNext(CopernicusWizardPageEventArgs e)
    {
      if (this.WizardNext == null)
        return;
      this.WizardNext((object) this, e);
    }

    public virtual void OnWizardBack(CopernicusWizardPageEventArgs e)
    {
      if (this.WizardBack == null)
        return;
      this.WizardBack((object) this, e);
    }

    public virtual void OnWizardFinish(CancelEventArgs e)
    {
      if (this.WizardFinish == null)
        return;
      this.WizardFinish((object) this, e);
    }

    public virtual void OnQueryCancel(CancelEventArgs e)
    {
      if (this.QueryCancel == null)
        return;
      this.QueryCancel((object) this, e);
    }
  }
}
