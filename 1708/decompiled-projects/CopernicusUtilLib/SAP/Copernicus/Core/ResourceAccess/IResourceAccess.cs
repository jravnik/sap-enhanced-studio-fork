﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.IResourceAccess
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Repository;

namespace SAP.Copernicus.Core.ResourceAccess
{
  public interface IResourceAccess
  {
    Connection GetConnection();

    RepositoryDataCache GetRepositoryDataCache();
  }
}
