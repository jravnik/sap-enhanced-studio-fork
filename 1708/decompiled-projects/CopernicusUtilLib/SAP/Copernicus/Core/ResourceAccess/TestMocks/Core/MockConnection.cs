﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.TestMocks.Core.MockConnection
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using System;

namespace SAP.Copernicus.Core.ResourceAccess.TestMocks.Core
{
  public class MockConnection : Connection
  {
    public MockConnection(bool usePSM)
    {
      this.LogonData.UsePSM = usePSM;
      this.LogonData.PartnerDomain = "dummy";
      if (!TestController.TestMode)
        throw new Exception("Not in test mode!");
      Connection.DEFAULT_PATH_REP = TestController.TestInputRootPath + "MDRS\\";
      this.HeadLessFlag = true;
      this.connectionDataSet.Tables["SystemData"].Clear();
      this.connectionDataSet.Tables["SystemData"].Rows.Add((object) "TST", (object) "test.wdf.sap.corp", (object) 50000, (object) 50001, (object) 0, (object) 100);
      this.ConnectedSystem = (ConnectionDataSet.SystemDataRow) this.connectionDataSet.Tables["SystemData"].Rows[0];
      this.ConnectedSystem.User = "Test";
      Connection.instance = (Connection) this;
      TestController.PathUpdateEventRegistration += new TestController.EventHandlerOnPathUpdate(this.OnPathUpdate);
    }

    public void OnPathUpdate(string newPath)
    {
      Connection.DEFAULT_PATH_REP = newPath + "MDRS\\";
    }

    public override string GetJsonHttpURL()
    {
      return "file:///" + Connection.DEFAULT_PATH_REP;
    }
  }
}
