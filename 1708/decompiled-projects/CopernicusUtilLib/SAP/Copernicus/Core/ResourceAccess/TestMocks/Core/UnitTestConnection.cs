﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ResourceAccess.TestMocks.Core.UnitTestConnection
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;

namespace SAP.Copernicus.Core.ResourceAccess.TestMocks.Core
{
  public class UnitTestConnection : Connection
  {
    public UnitTestConnection()
    {
      PropertyAccess.GeneralProps.ProxySupport = true;
      Connection.instance = (Connection) this;
    }
  }
}
