﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Tracing.ITracingService
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.InteropServices;

namespace SAP.Copernicus.Core.Tracing
{
  [Guid("92F4A732-9668-43FE-A07A-D90B5FCEC4B2")]
  public interface ITracingService
  {
    bool IsActive { get; }

    void ActivateTrace(bool silent = false);

    void DeactivateTrace(bool silent = false);
  }
}
