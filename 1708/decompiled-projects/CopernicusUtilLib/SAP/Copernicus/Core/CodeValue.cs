﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.CodeValue
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core
{
  public class CodeValue
  {
    public string Name { get; set; }

    public string Value { get; set; }

    public CodeValue(string name, string value)
    {
      this.Name = name;
      this.Value = value;
    }
  }
}
