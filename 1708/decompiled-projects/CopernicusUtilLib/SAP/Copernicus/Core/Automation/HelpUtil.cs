﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Automation.HelpUtil
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using Microsoft.VisualStudio.VSHelp80;

namespace SAP.Copernicus.Core.Automation
{
  public class HelpUtil
  {
    public static void DisplayF1Help(string keyword)
    {
      // ISSUE: variable of a compiler-generated type
      Help2 help2 = (Help2) DTEUtil.GetDTE().GetObject("Help2");
      // ISSUE: reference to a compiler-generated method
      help2.DisplayTopicFromF1Keyword(keyword);
    }
  }
}
