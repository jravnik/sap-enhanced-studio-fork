﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ErrorList.ErrorSink
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.ErrorList
{
  public class ErrorSink
  {
    private static ErrorSink instance = new ErrorSink();
    private readonly string CLEAR_ERROR_LIST_EVENT_NAME = typeof (EventHandlerOnClearErrorList).Name;
    private readonly string UPDATE_ERROR_LIST_EVENT_NAME = typeof (EventHandlerOnUpdateErrorList).Name;
    private readonly Dictionary<string, Delegate> eventTable = new Dictionary<string, Delegate>();

    public static ErrorSink Instance
    {
      get
      {
        return ErrorSink.instance;
      }
    }

    private ErrorSink()
    {
    }

    private void AddHandler(Delegate handler, string eventHandlerType)
    {
      if ((object) handler == null)
        return;
      string key = eventHandlerType;
      lock (this.eventTable)
      {
        Delegate a;
        if (this.eventTable.TryGetValue(key, out a))
          this.eventTable[key] = Delegate.Combine(a, handler);
        else
          this.eventTable[key] = handler;
      }
    }

    public void AddHandler(EventHandlerOnClearErrorList handler)
    {
      this.AddHandler((Delegate) handler, this.CLEAR_ERROR_LIST_EVENT_NAME);
    }

    public void AddHandler(EventHandlerOnUpdateErrorList handler)
    {
      this.AddHandler((Delegate) handler, this.UPDATE_ERROR_LIST_EVENT_NAME);
    }

    private void RemoveHandler(Delegate handler, string eventHandlerType)
    {
      string index = eventHandlerType;
      if ((object) handler == null)
        return;
      lock (this.eventTable)
      {
        if ((object) this.eventTable[index] == null)
          return;
        this.eventTable[index] = Delegate.Remove(this.eventTable[index], handler);
      }
    }

    public void RemoveHandler(EventHandlerOnClearErrorList handler)
    {
      this.RemoveHandler((Delegate) handler, this.CLEAR_ERROR_LIST_EVENT_NAME);
    }

    public void RemoveHandler(EventHandlerOnUpdateErrorList handler)
    {
      this.RemoveHandler((Delegate) handler, this.UPDATE_ERROR_LIST_EVENT_NAME);
    }

    public void ClearTasksFor(Origin originMask)
    {
      this.RaiseClearErrorListEvent("", originMask);
    }

    public void ClearTasksFor(string filePath, Origin originMask = Origin.Unknown)
    {
      this.RaiseClearErrorListEvent(filePath, originMask);
    }

    private void RaiseClearErrorListEvent(string filePath, Origin origin)
    {
      string errorListEventName = this.CLEAR_ERROR_LIST_EVENT_NAME;
      if (!this.eventTable.ContainsKey(errorListEventName))
        return;
      lock (this.eventTable)
      {
        EventHandlerOnClearErrorList onClearErrorList = (EventHandlerOnClearErrorList) this.eventTable[errorListEventName];
        if (onClearErrorList == null)
          return;
        onClearErrorList(filePath, origin);
      }
    }

    public void AddMessages(PDI_LM_T_MSG_LIST[] messages, string messagePrefix = null, Origin origin = Origin.Unknown)
    {
      List<Task> taskList = new List<Task>();
      if (messages != null)
      {
        foreach (PDI_LM_T_MSG_LIST message1 in messages)
        {
          string message2 = string.IsNullOrEmpty(messagePrefix) ? message1.TEXT : "[" + messagePrefix + "] " + message1.TEXT;
          taskList.Add(new Task(message2, message1.FILE_NAME, message1.LINE_NO - 1, message1.COLUMN_NO - 1, message1.LENGTH, origin, ErrorSink.SeverityFromABAPCode(message1.SEVERITY), (EventHandler) null));
        }
      }
      this.AddTasks((IEnumerable<Task>) taskList);
    }

    public void AddTasks(IEnumerable<Task> tasks)
    {
      this.RaiseUpdateErrorListEvent(tasks);
    }

    public void AddTask(string message, Origin origin = Origin.Unknown, Severity severity = Severity.Error, EventHandler navigationHandler = null)
    {
      this.RaiseUpdateErrorListEvent((IEnumerable<Task>) new Task[1]
      {
        new Task(message, origin, severity, navigationHandler)
      });
    }

    public void AddTask(string message, string fileName, int line = 0, int column = 0, int length = 0, Origin origin = Origin.Unknown, Severity severity = Severity.Error, EventHandler navigationHandler = null)
    {
      this.RaiseUpdateErrorListEvent((IEnumerable<Task>) new Task[1]
      {
        new Task(message, fileName, line, column, length, origin, severity, navigationHandler)
      });
    }

    private void RaiseUpdateErrorListEvent(IEnumerable<Task> tasks)
    {
      string errorListEventName = this.UPDATE_ERROR_LIST_EVENT_NAME;
      if (!this.eventTable.ContainsKey(errorListEventName))
        return;
      lock (this.eventTable)
      {
        EventHandlerOnUpdateErrorList onUpdateErrorList = (EventHandlerOnUpdateErrorList) this.eventTable[errorListEventName];
        if (onUpdateErrorList == null)
          return;
        onUpdateErrorList(tasks);
      }
    }

    public static Severity SeverityFromABAPCode(string value)
    {
      switch (value.ToUpper())
      {
        case "E":
          return Severity.Error;
        case "I":
          return Severity.Info;
        case "S":
          return Severity.Info;
        case "W":
          return Severity.Warning;
        default:
          return Severity.Error;
      }
    }
  }
}
