﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ErrorList.Task
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;

namespace SAP.Copernicus.Core.ErrorList
{
  public class Task : IComparable<Task>
  {
    public const string NO_FILE = "";
    private readonly string message;
    private readonly Severity severity;
    private readonly Origin origin;
    private readonly int line;
    private readonly int column;
    private readonly int length;
    private readonly string fileName;
    private readonly EventHandler navigationHandler;

    public string Message
    {
      get
      {
        return this.message;
      }
    }

    public int Line
    {
      get
      {
        return this.line;
      }
    }

    public int Column
    {
      get
      {
        return this.column;
      }
    }

    public int Length
    {
      get
      {
        return this.length;
      }
    }

    public Severity Severity
    {
      get
      {
        return this.severity;
      }
    }

    public string FileName
    {
      get
      {
        return this.fileName;
      }
    }

    public Origin Origin
    {
      get
      {
        return this.origin;
      }
    }

    public EventHandler NavigationHandler
    {
      get
      {
        return this.navigationHandler;
      }
    }

    public Task(string message, Origin origin = Origin.Unknown, Severity severity = Severity.Error, EventHandler navigationHandler = null)
    {
      this.message = message;
      this.severity = severity;
      this.origin = origin;
      this.fileName = "";
      this.line = -1;
      this.column = -1;
      this.length = 0;
      this.navigationHandler = navigationHandler;
    }

    public Task(string message, string fileName, int line = 0, int column = 0, int length = 0, Origin origin = Origin.Unknown, Severity severity = Severity.Error, EventHandler navigationHandler = null)
    {
      this.message = message;
      this.severity = severity;
      this.origin = origin;
      this.fileName = fileName;
      this.line = line;
      this.column = column;
      this.length = length;
      this.navigationHandler = navigationHandler;
    }

    public override int GetHashCode()
    {
      return (int) ((Severity) (this.message.GetHashCode() ^ this.FileName.GetHashCode() ^ this.line ^ this.column ^ this.length) ^ this.severity ^ (Severity) this.origin);
    }

    public override bool Equals(object otherObj)
    {
      Task task = otherObj as Task;
      if (task != null && this.message == task.message && (this.severity == task.severity && this.origin == task.origin) && (this.fileName == task.fileName && this.line == task.line && this.column == task.column))
        return this.length == task.length;
      return false;
    }

    public override string ToString()
    {
      string str;
      switch (this.severity)
      {
        case Severity.Info:
          str = "Info";
          break;
        case Severity.Warning:
          str = "Warning";
          break;
        case Severity.Error:
          str = "Error";
          break;
        default:
          str = "UNKNOWN";
          break;
      }
      return string.Format("{0}: {1}, at line {2}:{3}, in {4}", (object) str, (object) this.message, (object) this.line, (object) this.column, (object) this.fileName);
    }

    public int CompareTo(Task other)
    {
      if (other == null)
        throw new ArgumentNullException("other");
      if (this.FileName == null && other.FileName != null)
        return -1;
      if (!this.FileName.Equals(other.FileName))
        return this.FileName.CompareTo(other.FileName);
      if (this.Line < other.Line)
        return -1;
      if (this.Line > other.Line)
        return 1;
      if (this.Column < other.Column)
        return -1;
      return this.Column > other.Column ? 1 : 0;
    }
  }
}
