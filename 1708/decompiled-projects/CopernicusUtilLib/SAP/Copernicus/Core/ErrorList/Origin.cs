﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.ErrorList.Origin
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.ErrorList
{
  public enum Origin
  {
    BOCompilerLevel1 = 1,
    BOCompilerLevel2 = 2,
    BOCompilerLevel3 = 4,
    Tracing = 8,
    DTDLParser = 16,
    DTDLWalker = 32,
    DTDLCompiler = 64,
    SolutionCompilerOutdated = 128,
    NewCompilerSimulation = 256,
    GenericSolutionAction = 512,
    GenericEditorCheck = 1024,
    CustomReuseLibraryOnSave = 2048,
    AnalyticsContent = 4096,
    XOdataService = 8192,
    Unknown = 1073741824,
    All = 2147483647,
  }
}
