﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.AssemblyCodeline
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;

namespace SAP.Copernicus.Core.Util
{
  [AttributeUsage(AttributeTargets.Assembly)]
  public class AssemblyCodeline : Attribute
  {
    private string _codeLine = "dev";

    public string CodeLine
    {
      get
      {
        return this._codeLine;
      }
    }

    public AssemblyCodeline(string codeLine)
    {
      this._codeLine = codeLine;
    }
  }
}
