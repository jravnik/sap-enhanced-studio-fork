﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.FileUtil
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;
using System.IO;

namespace SAP.Copernicus.Core.Util
{
  public static class FileUtil
  {
    public static List<string> GetFilesFromDir(string dirPath, string pattern)
    {
      List<string> fileNames = new List<string>();
      FileUtil.GetFilesFromDir(dirPath, pattern, fileNames);
      return fileNames;
    }

    private static void GetFilesFromDir(string dirPath, string pattern, List<string> fileNames)
    {
      fileNames.AddRange((IEnumerable<string>) Directory.GetFiles(dirPath, pattern));
      foreach (string directory in Directory.GetDirectories(dirPath))
        fileNames.AddRange((IEnumerable<string>) FileUtil.GetFilesFromDir(directory, pattern));
    }
  }
}
