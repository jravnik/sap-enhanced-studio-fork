﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.AssemblyCSN
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;

namespace SAP.Copernicus.Core.Util
{
  [AttributeUsage(AttributeTargets.Assembly)]
  public class AssemblyCSN : Attribute
  {
    private string _csnComponent = string.Empty;

    public string CsnComponent
    {
      get
      {
        return this._csnComponent;
      }
    }

    public AssemblyCSN(string csnComponent)
    {
      this._csnComponent = csnComponent;
    }
  }
}
