﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.XMLUtil
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.IO;
using System.Xml;

namespace SAP.Copernicus.Core.Util
{
  public class XMLUtil
  {
    private XMLUtil()
    {
    }

    public static string prettyPrint(string xml)
    {
      XmlDocument xmlDocument = new XmlDocument();
      if (string.IsNullOrEmpty(xml))
        return xml;
      xmlDocument.LoadXml(xml);
      XmlNodeReader xmlNodeReader = new XmlNodeReader((XmlNode) xmlDocument);
      StringWriter stringWriter = new StringWriter();
      new XmlTextWriter((TextWriter) stringWriter)
      {
        Formatting = Formatting.Indented,
        Indentation = 1,
        IndentChar = '\t'
      }.WriteNode((XmlReader) xmlNodeReader, true);
      return stringWriter.ToString();
    }
  }
}
