﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Util.Forms.CustomDialog
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Util.Forms
{
  public class CustomDialog : Form
  {
    private IContainer components;
    private Label labelText;

    public string LabelText
    {
      get
      {
        return this.labelText.Text;
      }
      set
      {
        this.labelText.Text = value;
        this.Refresh();
      }
    }

    public CustomDialog(Bitmap bitmap, string caption, string text)
    {
      this.InitializeComponent();
      this.SuspendLayout();
      if (bitmap != null)
        this.Icon = Icon.FromHandle(bitmap.GetHicon());
      else
        this.Icon = Resource.SAPBusinessByDesignStudioIcon;
      this.Text = caption;
      this.labelText.Text = text;
      this.ResumeLayout(false);
      this.PerformLayout();
      this.Show();
      this.Refresh();
    }

    public CustomDialog(string caption, string text)
      : this((Bitmap) null, caption, text)
    {
    }

    public void destroy()
    {
      this.Hide();
      this.Close();
      this.Dispose();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.labelText = new Label();
      this.SuspendLayout();
      this.labelText.AutoSize = true;
      this.labelText.Location = new Point(12, 19);
      this.labelText.Name = "labelText";
      this.labelText.Size = new Size(24, 13);
      this.labelText.TabIndex = 0;
      this.labelText.Text = "text";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(275, 56);
      this.Controls.Add((Control) this.labelText);
      this.Name = "CustomDialog";
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "CustomDialog";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
