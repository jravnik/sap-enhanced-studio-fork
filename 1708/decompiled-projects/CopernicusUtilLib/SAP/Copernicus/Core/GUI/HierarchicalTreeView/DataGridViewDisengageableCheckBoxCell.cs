﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.HierarchicalTreeView.DataGridViewDisengageableCheckBoxCell
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.HierarchicalTreeView
{
  public class DataGridViewDisengageableCheckBoxCell : DataGridViewCheckBoxCell, DataGridViewDisengageableCell
  {
    private bool enabled;

    public bool Enabled
    {
      get
      {
        return this.enabled;
      }
      set
      {
        this.enabled = value;
      }
    }

    public DataGridViewDisengageableCheckBoxCell()
    {
      this.enabled = true;
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
      if (!this.enabled)
      {
        if ((cellState & DataGridViewElementStates.Selected) != DataGridViewElementStates.None)
          graphics.FillRectangle((Brush) new SolidBrush(cellStyle.SelectionBackColor), cellBounds);
        else
          graphics.FillRectangle((Brush) new SolidBrush(cellStyle.BackColor), cellBounds);
        this.PaintBorder(graphics, clipBounds, cellBounds, cellStyle, advancedBorderStyle);
      }
      else
        base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
    }
  }
}
