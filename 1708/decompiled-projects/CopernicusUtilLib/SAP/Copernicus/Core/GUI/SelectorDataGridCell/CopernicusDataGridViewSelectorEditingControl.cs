﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.SelectorDataGridCell.CopernicusDataGridViewSelectorEditingControl
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.SelectorDataGridCell
{
  public class CopernicusDataGridViewSelectorEditingControl : UserControlSelectableText, IDataGridViewEditingControl
  {
    private DataGridView dataGridView;
    private bool valueChanged;
    private int rowIndex;

    public virtual DataGridView EditingControlDataGridView
    {
      get
      {
        return this.dataGridView;
      }
      set
      {
        this.dataGridView = value;
      }
    }

    public virtual object EditingControlFormattedValue
    {
      get
      {
        return this.GetEditingControlFormattedValue(DataGridViewDataErrorContexts.Formatting);
      }
      set
      {
        this.Value = (string) value;
      }
    }

    public virtual int EditingControlRowIndex
    {
      get
      {
        return this.rowIndex;
      }
      set
      {
        this.rowIndex = value;
      }
    }

    public virtual bool EditingControlValueChanged
    {
      get
      {
        return this.valueChanged;
      }
      set
      {
        this.valueChanged = value;
      }
    }

    public virtual Cursor EditingPanelCursor
    {
      get
      {
        return Cursors.Default;
      }
    }

    public virtual bool RepositionEditingControlOnValueChange
    {
      get
      {
        return false;
      }
    }

    public CopernicusDataGridViewSelectorEditingControl()
    {
      this.TabStop = false;
    }

    [DllImport("USER32.DLL", CharSet = CharSet.Auto)]
    private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

    public virtual void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
    {
      this.Font = dataGridViewCellStyle.Font;
      if ((int) dataGridViewCellStyle.BackColor.A < (int) byte.MaxValue)
      {
        Color color = Color.FromArgb((int) byte.MaxValue, dataGridViewCellStyle.BackColor);
        this.BackColor = color;
        this.dataGridView.EditingPanel.BackColor = color;
      }
      else
        this.BackColor = dataGridViewCellStyle.BackColor;
      this.ForeColor = dataGridViewCellStyle.ForeColor;
      this.TextAlign = CopernicusDataGridViewSelectorCell.TranslateAlignment(dataGridViewCellStyle.Alignment);
    }

    public virtual bool EditingControlWantsInputKey(Keys keyData, bool dataGridViewWantsInputKey)
    {
      bool isProcessedInside = false;
      switch (keyData & Keys.KeyCode)
      {
        case Keys.Return:
          string strCurrentTextValue = string.Empty;
          if (this.isFocusedText(out strCurrentTextValue) && strCurrentTextValue.Length > 0)
          {
            this.NotifyDataGridViewOfValueChange();
            return true;
          }
          break;
        case Keys.End:
        case Keys.Home:
          this.ProcessKey(Keys.End, out isProcessedInside);
          if (isProcessedInside)
            return true;
          break;
        case Keys.Left:
          this.ProcessKey(Keys.Left, out isProcessedInside);
          if (isProcessedInside)
            return true;
          break;
        case Keys.Up:
          if (this.Value.Length > 0)
            return true;
          break;
        case Keys.Right:
          this.ProcessKey(Keys.Right, out isProcessedInside);
          if (isProcessedInside)
            return true;
          break;
        case Keys.Down:
          if (this.Value.Length > 0)
            return true;
          break;
        case Keys.Delete:
          this.ProcessKey(Keys.Delete, out isProcessedInside);
          if (isProcessedInside)
            return true;
          break;
      }
      return !dataGridViewWantsInputKey;
    }

    public virtual object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
    {
      bool userEdit = this.UserEdit;
      try
      {
        this.UserEdit = (context & DataGridViewDataErrorContexts.Display) == (DataGridViewDataErrorContexts) 0;
        return this.Value == null ? (object) string.Empty : (object) this.Value.ToString();
      }
      finally
      {
        this.UserEdit = userEdit;
      }
    }

    public virtual void PrepareEditingControlForEdit(bool selectAll)
    {
    }

    private void NotifyDataGridViewOfValueChange()
    {
      if (this.valueChanged)
        return;
      this.valueChanged = true;
      this.dataGridView.NotifyCurrentCellDirty(true);
    }

    protected override void OnKeyPress(KeyPressEventArgs e)
    {
      base.OnKeyPress(e);
      bool flag = false;
      if (char.IsDigit(e.KeyChar))
      {
        flag = true;
      }
      else
      {
        NumberFormatInfo numberFormat = CultureInfo.CurrentCulture.NumberFormat;
        string decimalSeparator = numberFormat.NumberDecimalSeparator;
        string numberGroupSeparator = numberFormat.NumberGroupSeparator;
        string negativeSign = numberFormat.NegativeSign;
        if (!string.IsNullOrEmpty(decimalSeparator) && decimalSeparator.Length == 1)
          flag = (int) decimalSeparator[0] == (int) e.KeyChar;
        if (!flag && !string.IsNullOrEmpty(numberGroupSeparator) && numberGroupSeparator.Length == 1)
          flag = (int) numberGroupSeparator[0] == (int) e.KeyChar;
        if (!flag && !string.IsNullOrEmpty(negativeSign) && negativeSign.Length == 1)
          flag = (int) negativeSign[0] == (int) e.KeyChar;
      }
      if (!flag)
        return;
      this.NotifyDataGridViewOfValueChange();
    }

    protected override void OnValueChanged(EventArgs e)
    {
      base.OnValueChanged(e);
      this.valueChanged = true;
      this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
      this.NotifyDataGridViewOfValueChange();
    }

    protected override bool ProcessKeyEventArgs(ref Message m)
    {
      TextBox control = this.Controls[0] as TextBox;
      if (control == null)
        return base.ProcessKeyEventArgs(ref m);
      CopernicusDataGridViewSelectorEditingControl.SendMessage(control.Handle, m.Msg, m.WParam, m.LParam);
      return true;
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);
    }

    private void InitializeComponent()
    {
      this.SuspendLayout();
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.Name = "CopernicusDataGridViewSelectorEditingControl";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
