﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.SelectorDataGridCell.CopernicusPopupForm
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Wizard;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.SelectorDataGridCell
{
  public abstract class CopernicusPopupForm : Form
  {
    private Button btnOK;
    protected Panel buttonPanel;
    protected CopernicusEtchedLine etchedLine1;
    protected Button btnCancel;
    protected Panel pagePanel;
    private CopernicusPopupStructure datafield;
    private bool isSelectionValid;

    public bool IsCurrentSelectionValid
    {
      get
      {
        return this.isSelectionValid;
      }
      set
      {
        if (value == this.isSelectionValid)
          return;
        this.isSelectionValid = value;
        this.NotifyPropertyChanged("IsCurrentSelectionValid");
      }
    }

    protected string DataType
    {
      get
      {
        return this.datafield.DataType;
      }
      set
      {
        if (value == null || value.Equals(this.datafield.DataType))
          return;
        this.datafield.DataType = value;
      }
    }

    protected string NameSpace
    {
      get
      {
        return this.datafield.DataTypeNamespace;
      }
      set
      {
        if (value == null || value.Equals(this.datafield.DataTypeNamespace))
          return;
        this.datafield.DataTypeNamespace = value;
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public CopernicusPopupForm()
    {
      this.datafield = new CopernicusPopupStructure((string) null, (string) null);
      this.InitializeComponent();
      this.Icon = Resource.SAPBusinessByDesignStudioIcon;
      this.PropertyChanged += new PropertyChangedEventHandler(this.CopernicusCallbackPopupForm_PropertyChanged);
    }

    private void NotifyPropertyChanged(string info)
    {
      if (this.PropertyChanged == null)
        return;
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(info));
    }

    private void CopernicusCallbackPopupForm_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (this.btnOK.Enabled == this.isSelectionValid)
        return;
      this.btnOK.Enabled = this.isSelectionValid;
    }

    protected abstract void setDataFieldContent();

    protected abstract void reloadData();

    protected void setDataFieldContent(string strDataType, string strDataNameSpace)
    {
      this.datafield = new CopernicusPopupStructure((string) null, (string) null)
      {
        DataType = strDataType,
        DataTypeNamespace = strDataNameSpace
      };
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    public virtual CopernicusPopupStructure ShowModalDialog()
    {
      int num = (int) this.ShowDialog();
      this.setDataFieldContent();
      return this.datafield;
    }

    public virtual CopernicusPopupStructure ShowModalDialog(string strDataType, string strDataNameSpace)
    {
      this.setDataFieldContent(strDataType, strDataNameSpace);
      this.reloadData();
      if (this.ShowDialog() == DialogResult.OK)
        this.setDataFieldContent();
      return this.datafield;
    }

    private void InitializeComponent()
    {
      this.btnOK = new Button();
      this.buttonPanel = new Panel();
      this.etchedLine1 = new CopernicusEtchedLine();
      this.btnCancel = new Button();
      this.pagePanel = new Panel();
      this.buttonPanel.SuspendLayout();
      this.SuspendLayout();
      this.btnOK.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.btnOK.DialogResult = DialogResult.OK;
      this.btnOK.Enabled = false;
      this.btnOK.Location = new Point(299, 7);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new Size(75, 23);
      this.btnOK.TabIndex = 0;
      this.btnOK.Text = "OK";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new EventHandler(this.btnOK_Click);
      this.buttonPanel.Controls.Add((Control) this.etchedLine1);
      this.buttonPanel.Controls.Add((Control) this.btnOK);
      this.buttonPanel.Controls.Add((Control) this.btnCancel);
      this.buttonPanel.Dock = DockStyle.Bottom;
      this.buttonPanel.Location = new Point(0, 106);
      this.buttonPanel.Name = "buttonPanel";
      this.buttonPanel.Size = new Size(467, 40);
      this.buttonPanel.TabIndex = 5;
      this.etchedLine1.AutoScroll = true;
      this.etchedLine1.AutoSize = true;
      this.etchedLine1.Dock = DockStyle.Top;
      this.etchedLine1.Edge = EtchEdge.Top;
      this.etchedLine1.Location = new Point(0, 0);
      this.etchedLine1.Name = "etchedLine1";
      this.etchedLine1.Size = new Size(467, 0);
      this.etchedLine1.TabIndex = 4;
      this.btnCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.btnCancel.DialogResult = DialogResult.Cancel;
      this.btnCancel.FlatStyle = FlatStyle.System;
      this.btnCancel.Location = new Point(380, 7);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new Size(75, 23);
      this.btnCancel.TabIndex = 3;
      this.btnCancel.Text = "Cancel";
      this.pagePanel.Dock = DockStyle.Fill;
      this.pagePanel.Location = new Point(0, 0);
      this.pagePanel.Name = "pagePanel";
      this.pagePanel.Size = new Size(467, 106);
      this.pagePanel.TabIndex = 6;
      this.AcceptButton = (IButtonControl) this.btnOK;
      this.ClientSize = new Size(467, 146);
      this.Controls.Add((Control) this.buttonPanel);
      this.Name = "CopernicusPopupForm";
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.CenterParent;
      this.buttonPanel.ResumeLayout(false);
      this.buttonPanel.PerformLayout();
      this.ResumeLayout(false);
    }
  }
}
