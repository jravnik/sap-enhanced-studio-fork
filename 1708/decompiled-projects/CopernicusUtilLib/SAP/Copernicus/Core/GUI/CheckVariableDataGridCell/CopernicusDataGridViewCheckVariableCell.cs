﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.GUI.CheckVariableDataGridCell.CopernicusDataGridViewCheckVariableCell
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.GUI.CheckVariableDataGridCell
{
  public class CopernicusDataGridViewCheckVariableCell : DataGridViewTextBoxCell
  {
    private static readonly DataGridViewContentAlignment anyRight = DataGridViewContentAlignment.TopRight | DataGridViewContentAlignment.MiddleRight | DataGridViewContentAlignment.BottomRight;
    private static readonly DataGridViewContentAlignment anyCenter = DataGridViewContentAlignment.TopCenter | DataGridViewContentAlignment.MiddleCenter | DataGridViewContentAlignment.BottomCenter;
    private static Type defaultEditType = typeof (CopernicusDataGridViewCheckVariableEditingControl);
    private static Type defaultValueType = typeof (string);
    private const int DATAGRIDVIEWSELECTORCELL_defaultRenderingBitmapWidth = 100;
    private const int DATAGRIDVIEWSELECTORCELL_defaultRenderingBitmapHeight = 22;
    internal const bool DATAGRIDVIEWSELECTORCELL_defaultIsEnable = false;
    internal const string DATAGRIDVIEWSELECTORCELL_defaultCallbackHandler = "";
    internal bool isEnableRequiredValue;
    private string strCallbackFormClass;
    private string strCallbackFormPackageName;
    private CopernicusPopupClassMetaData filterClassMetaDataForCell;
    private bool isFilterValueSelectableForCell;
    [ThreadStatic]
    private static Bitmap renderingBitmap;
    [ThreadStatic]
    private static Bitmap emptyBitmap;
    [ThreadStatic]
    private static UserControlCheckVariable paintingContorlforFilterValue;

    [DefaultValue("")]
    public string strCallbackFormHandler
    {
      get
      {
        return this.strCallbackFormClass;
      }
      set
      {
        if (!(this.strCallbackFormClass != value))
          return;
        this.SetCallbackFormHandler(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    [DefaultValue("")]
    public string CallbackFormPackageName
    {
      get
      {
        return this.strCallbackFormPackageName;
      }
      set
      {
        if (!(this.strCallbackFormPackageName != value))
          return;
        this.SetCallbackFormPackageName(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    [DefaultValue(false)]
    public bool IsFilterValueSelectableForCell
    {
      get
      {
        return this.isFilterValueSelectableForCell;
      }
      set
      {
        if (this.isFilterValueSelectableForCell == value)
          return;
        this.SetCallbackFormIsFilterValueSelectableForCell(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    public CopernicusPopupClassMetaData FilterClassMetaDataForCell
    {
      get
      {
        return this.filterClassMetaDataForCell;
      }
      set
      {
        if (this.filterClassMetaDataForCell == value)
          return;
        this.SetFilterClassMetaDataForCell(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    [DefaultValue(false)]
    public bool IsEnableRequiredFilterValue
    {
      get
      {
        return this.isEnableRequiredValue;
      }
      set
      {
        if (this.isEnableRequiredValue == value)
          return;
        this.SetIsEnableRequiredValue(this.RowIndex, value);
        this.OnCommonChange();
      }
    }

    private CopernicusDataGridViewCheckVariableEditingControl EditingSelector
    {
      get
      {
        return this.DataGridView.EditingControl as CopernicusDataGridViewCheckVariableEditingControl;
      }
    }

    public override Type EditType
    {
      get
      {
        return CopernicusDataGridViewCheckVariableCell.defaultEditType;
      }
    }

    public override Type ValueType
    {
      get
      {
        Type valueType = base.ValueType;
        if (valueType != (Type) null)
          return valueType;
        return CopernicusDataGridViewCheckVariableCell.defaultValueType;
      }
    }

    public override object DefaultNewRowValue
    {
      get
      {
        return (object) string.Empty;
      }
    }

    public bool IsCellChecked
    {
      get
      {
        if (this.Tag == null)
          return false;
        return bool.Parse(this.Tag.ToString());
      }
      set
      {
        bool flag = this.Tag != null && bool.Parse(this.Tag.ToString());
        if (value == flag)
          return;
        this.Tag = (object) value;
      }
    }

    private event Action<Keys> MouseKeyCodeOnCellItem;

    public CopernicusDataGridViewCheckVariableCell()
    {
      if (CopernicusDataGridViewCheckVariableCell.renderingBitmap == null)
        CopernicusDataGridViewCheckVariableCell.renderingBitmap = new Bitmap(100, 22);
      if (CopernicusDataGridViewCheckVariableCell.emptyBitmap == null)
        CopernicusDataGridViewCheckVariableCell.emptyBitmap = new Bitmap(100, 22);
      if (CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue == null || CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.IsDisposedAlready)
      {
        CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue = new UserControlCheckVariable();
        CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.BorderStyle = BorderStyle.None;
      }
      this.isEnableRequiredValue = false;
    }

    [DllImport("USER32.DLL", CharSet = CharSet.Auto)]
    private static extern short VkKeyScan(char key);

    internal void SetCallbackFormHandler(int rowIndex, string value)
    {
      this.strCallbackFormClass = value;
      this.OwnsEditingCopernicusFilterValue(rowIndex);
    }

    internal void SetCallbackFormPackageName(int rowIndex, string value)
    {
      this.strCallbackFormPackageName = value;
      this.OwnsEditingCopernicusFilterValue(rowIndex);
    }

    internal void SetCallbackFormIsFilterValueSelectableForCell(int rowIndex, bool value)
    {
      this.isFilterValueSelectableForCell = value;
      this.OwnsEditingCopernicusFilterValue(rowIndex);
    }

    internal void SetFilterClassMetaDataForCell(int rowIndex, CopernicusPopupClassMetaData value)
    {
      this.filterClassMetaDataForCell = value;
      this.OwnsEditingCopernicusFilterValue(rowIndex);
    }

    internal void SetIsEnableRequiredValue(int rowIndex, bool value)
    {
      this.isEnableRequiredValue = value;
      this.OwnsEditingCopernicusFilterValue(rowIndex);
    }

    private void OnCommonChange()
    {
      if (this.DataGridView == null || this.DataGridView.IsDisposed || this.DataGridView.Disposing)
        return;
      if (this.RowIndex == -1)
        this.DataGridView.InvalidateColumn(this.ColumnIndex);
      else
        this.DataGridView.UpdateCellValue(this.ColumnIndex, this.RowIndex);
    }

    private bool OwnsEditingCopernicusFilterValue(int rowIndex)
    {
      if (rowIndex == -1 || this.DataGridView == null)
        return false;
      CopernicusDataGridViewCheckVariableEditingControl editingControl = this.DataGridView.EditingControl as CopernicusDataGridViewCheckVariableEditingControl;
      if (editingControl != null)
        return rowIndex == editingControl.EditingControlRowIndex;
      return false;
    }

    public override object Clone()
    {
      CopernicusDataGridViewCheckVariableCell checkVariableCell = base.Clone() as CopernicusDataGridViewCheckVariableCell;
      if (checkVariableCell != null)
      {
        checkVariableCell.IsEnableRequiredFilterValue = this.isEnableRequiredValue;
        checkVariableCell.strCallbackFormHandler = this.strCallbackFormClass;
        checkVariableCell.strCallbackFormPackageName = this.strCallbackFormPackageName;
        checkVariableCell.IsFilterValueSelectableForCell = this.isFilterValueSelectableForCell;
        checkVariableCell.FilterClassMetaDataForCell = this.filterClassMetaDataForCell;
      }
      return (object) checkVariableCell;
    }

    public void RaiseMouseKeyCodeCellForward(Keys keycode)
    {
      if (this.MouseKeyCodeOnCellItem == null)
        return;
      Action<Keys> action = Interlocked.CompareExchange<Action<Keys>>(ref this.MouseKeyCodeOnCellItem, (Action<Keys>) null, (Action<Keys>) null);
      if (action == null)
        return;
      action(keycode);
    }

    public void AddMouseKeyCodeEventHandler(Action<Keys> newKeyCodeEventHandler)
    {
      this.MouseKeyCodeOnCellItem += newKeyCodeEventHandler;
    }

    public void RemoveMouseKeyCodeEventHandler(Action<Keys> newKeyCodeEventHandler)
    {
      this.MouseKeyCodeOnCellItem -= newKeyCodeEventHandler;
    }

    public void ReceiveWindowPaneMessage(Keys keyCode)
    {
      UserControlCheckVariable editingControl = this.DataGridView.EditingControl as UserControlCheckVariable;
      if (editingControl == null)
        return;
      editingControl.ReceiveWindowPaneMessageInUserControl(keyCode, this.RowIndex, this.ColumnIndex);
    }

    private void TagChangeEvent(bool isCellChecked, int iRowIndex, int iColumnIndex)
    {
      if (this.RowIndex != iRowIndex || this.ColumnIndex != iColumnIndex)
        return;
      bool flag = this.Tag != null && bool.Parse(this.Tag.ToString());
      if (isCellChecked == flag)
        return;
      this.Tag = (object) isCellChecked;
    }

    public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
    {
      base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
      UserControlCheckVariable editingControl = this.DataGridView.EditingControl as UserControlCheckVariable;
      if (editingControl == null)
        return;
      editingControl.BorderStyle = BorderStyle.None;
      CopernicusF4MessageEvent tag = this.DataGridView.Tag as CopernicusF4MessageEvent;
      if (tag != null)
        tag.AddMouseKeyCodeEventHandler(new Action<Keys>(this.ReceiveWindowPaneMessage));
      editingControl.AddTagChangeMonitor(new Action<bool, int, int>(this.TagChangeEvent), this);
      editingControl.IsFilterValueSelectable = this.isFilterValueSelectableForCell;
      editingControl.CallbackModelFormPackageName = this.strCallbackFormPackageName;
      editingControl.CallbackModelFormClass = this.strCallbackFormHandler;
      editingControl.IsFilterBackendMandatory = this.isEnableRequiredValue;
      editingControl.IRowIndex = rowIndex;
      editingControl.IColumnIndex = this.ColumnIndex;
      editingControl.IsUserChecked = this.Tag != null && bool.Parse(this.Tag.ToString());
      editingControl.Value = this.Value != null ? this.Value.ToString() : string.Empty;
      editingControl.FilterClassMetaData = this.filterClassMetaDataForCell;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public override void DetachEditingControl()
    {
      DataGridView dataGridView = this.DataGridView;
      if (dataGridView == null || dataGridView.EditingControl == null)
        throw new InvalidOperationException("Cell is detached or its grid has no editing control.");
      if (dataGridView.EditingControl is UserControlCheckVariable)
      {
        CopernicusF4MessageEvent tag = this.DataGridView.Tag as CopernicusF4MessageEvent;
        if (tag != null)
          tag.RemoveMouseKeyCodeEventHandler(new Action<Keys>(this.ReceiveWindowPaneMessage));
      }
      base.DetachEditingControl();
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
    {
      if (this.DataGridView == null)
        return;
      base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts & ~(DataGridViewPaintParts.ContentForeground | DataGridViewPaintParts.ErrorIcon));
      Point currentCellAddress = this.DataGridView.CurrentCellAddress;
      if (currentCellAddress.X == this.ColumnIndex && currentCellAddress.Y == rowIndex && this.DataGridView.EditingControl != null)
        return;
      if (CopernicusDataGridViewCheckVariableCell.PartPainted(paintParts, DataGridViewPaintParts.ContentForeground))
      {
        Rectangle rectangle1 = this.BorderWidths(advancedBorderStyle);
        Rectangle editingControlBounds1 = cellBounds;
        editingControlBounds1.Offset(rectangle1.X, rectangle1.Y);
        editingControlBounds1.Width -= rectangle1.Right;
        editingControlBounds1.Height -= rectangle1.Bottom;
        if (cellStyle.Padding != Padding.Empty)
        {
          if (this.DataGridView.RightToLeft == RightToLeft.Yes)
            editingControlBounds1.Offset(cellStyle.Padding.Right, cellStyle.Padding.Top);
          else
            editingControlBounds1.Offset(cellStyle.Padding.Left, cellStyle.Padding.Top);
          editingControlBounds1.Width -= cellStyle.Padding.Horizontal;
          editingControlBounds1.Height -= cellStyle.Padding.Vertical;
        }
        Rectangle editingControlBounds2 = this.GetAdjustedEditingControlBounds(editingControlBounds1, cellStyle);
        bool flag1 = (cellState & DataGridViewElementStates.Selected) != DataGridViewElementStates.None;
        if (CopernicusDataGridViewCheckVariableCell.renderingBitmap.Width < editingControlBounds2.Width || CopernicusDataGridViewCheckVariableCell.renderingBitmap.Height < editingControlBounds2.Height)
        {
          CopernicusDataGridViewCheckVariableCell.renderingBitmap.Dispose();
          CopernicusDataGridViewCheckVariableCell.renderingBitmap = new Bitmap(editingControlBounds2.Width, editingControlBounds2.Height);
          CopernicusDataGridViewCheckVariableCell.emptyBitmap.Dispose();
          CopernicusDataGridViewCheckVariableCell.emptyBitmap = new Bitmap(editingControlBounds2.Width, editingControlBounds2.Height);
        }
        if (CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.Parent == null || !CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.Parent.Visible)
          CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.Parent = (Control) this.DataGridView;
        CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.TextAlign = CopernicusDataGridViewCheckVariableCell.TranslateAlignment(cellStyle.Alignment);
        CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.Font = cellStyle.Font;
        CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.Width = editingControlBounds2.Width;
        CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.Height = editingControlBounds2.Height;
        CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.RightToLeft = this.DataGridView.RightToLeft;
        CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.Location = new Point(0, -CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.Height);
        Color baseColor = !CopernicusDataGridViewCheckVariableCell.PartPainted(paintParts, DataGridViewPaintParts.SelectionBackground) || !flag1 ? cellStyle.BackColor : cellStyle.SelectionBackColor;
        if (CopernicusDataGridViewCheckVariableCell.PartPainted(paintParts, DataGridViewPaintParts.Background))
        {
          if ((int) baseColor.A < (int) byte.MaxValue)
            baseColor = Color.FromArgb((int) byte.MaxValue, baseColor);
          CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.BackColor = baseColor;
        }
        Rectangle rectangle2 = new Rectangle(0, 0, editingControlBounds2.Width, editingControlBounds2.Height);
        if (rectangle2.Width > 0 && rectangle2.Height > 0)
        {
          CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.CallbackModelFormClass = this.strCallbackFormHandler;
          CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.CallbackModelFormPackageName = this.CallbackFormPackageName;
          CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.IsFilterValueSelectable = this.isFilterValueSelectableForCell;
          CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.IsFilterBackendMandatory = this.isEnableRequiredValue;
          CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.IRowIndex = rowIndex;
          CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.IColumnIndex = this.ColumnIndex;
          bool flag2 = this.Tag != null && bool.Parse(this.Tag.ToString());
          CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.IsUserChecked = flag2;
          CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.FilterClassMetaData = this.filterClassMetaDataForCell;
          CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.Value = this.Value == null || this.Value.ToString().Length <= 0 ? string.Empty : this.Value.ToString();
          if (this.Value != null && this.Value.ToString().Length > 0 || flag2)
          {
            CopernicusDataGridViewCheckVariableCell.paintingContorlforFilterValue.DrawToBitmap(CopernicusDataGridViewCheckVariableCell.renderingBitmap, rectangle2);
            graphics.DrawImage((Image) CopernicusDataGridViewCheckVariableCell.renderingBitmap, new Rectangle(editingControlBounds2.Location, editingControlBounds2.Size), rectangle2, GraphicsUnit.Pixel);
          }
          else
            graphics.DrawImage((Image) CopernicusDataGridViewCheckVariableCell.emptyBitmap, new Rectangle(editingControlBounds2.Location, editingControlBounds2.Size), rectangle2, GraphicsUnit.Pixel);
        }
      }
      if (!CopernicusDataGridViewCheckVariableCell.PartPainted(paintParts, DataGridViewPaintParts.ErrorIcon))
        return;
      base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, DataGridViewPaintParts.ErrorIcon);
    }

    private static bool PartPainted(DataGridViewPaintParts paintParts, DataGridViewPaintParts paintPart)
    {
      return (paintParts & paintPart) != DataGridViewPaintParts.None;
    }

    public override void PositionEditingControl(bool setLocation, bool setSize, Rectangle cellBounds, Rectangle cellClip, DataGridViewCellStyle cellStyle, bool singleVerticalBorderAdded, bool singleHorizontalBorderAdded, bool isFirstDisplayedColumn, bool isFirstDisplayedRow)
    {
      Rectangle editingControlBounds = this.GetAdjustedEditingControlBounds(this.PositionEditingPanel(cellBounds, cellClip, cellStyle, singleVerticalBorderAdded, singleHorizontalBorderAdded, isFirstDisplayedColumn, isFirstDisplayedRow), cellStyle);
      this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
      this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
    }

    private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
    {
      ++editingControlBounds.X;
      editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);
      return editingControlBounds;
    }

    protected override Size GetPreferredSize(Graphics graphics, DataGridViewCellStyle cellStyle, int rowIndex, Size constraintSize)
    {
      if (this.DataGridView == null)
        return new Size(-1, -1);
      return base.GetPreferredSize(graphics, cellStyle, rowIndex, constraintSize);
    }

    protected override Rectangle GetErrorIconBounds(Graphics graphics, DataGridViewCellStyle cellStyle, int rowIndex)
    {
      Rectangle errorIconBounds = base.GetErrorIconBounds(graphics, cellStyle, rowIndex);
      errorIconBounds.X = this.DataGridView.RightToLeft != RightToLeft.Yes ? errorIconBounds.Left - 16 : errorIconBounds.Left + 16;
      return errorIconBounds;
    }

    protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context)
    {
      return base.GetFormattedValue(value, rowIndex, ref cellStyle, valueTypeConverter, formattedValueTypeConverter, context);
    }

    public override bool KeyEntersEditMode(KeyEventArgs e)
    {
      NumberFormatInfo numberFormat = CultureInfo.CurrentCulture.NumberFormat;
      Keys keys = Keys.None;
      string negativeSign = numberFormat.NegativeSign;
      if (!string.IsNullOrEmpty(negativeSign) && negativeSign.Length == 1)
        keys = (Keys) CopernicusDataGridViewCheckVariableCell.VkKeyScan(negativeSign[0]);
      return (char.IsDigit((char) e.KeyCode) || e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9 || (keys == e.KeyCode || Keys.Subtract == e.KeyCode)) && (!e.Shift && !e.Alt && !e.Control);
    }

    public override string ToString()
    {
      return "CopernicusDataGridViewCheckVariableCell { ColumnIndex=" + this.ColumnIndex.ToString((IFormatProvider) CultureInfo.CurrentCulture) + ", RowIndex=" + this.RowIndex.ToString((IFormatProvider) CultureInfo.CurrentCulture) + " }";
    }

    internal static HorizontalAlignment TranslateAlignment(DataGridViewContentAlignment align)
    {
      if ((align & CopernicusDataGridViewCheckVariableCell.anyRight) != DataGridViewContentAlignment.NotSet)
        return HorizontalAlignment.Right;
      return (align & CopernicusDataGridViewCheckVariableCell.anyCenter) != DataGridViewContentAlignment.NotSet ? HorizontalAlignment.Center : HorizontalAlignment.Left;
    }
  }
}
