﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Properties.PropertyAccess
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Properties
{
  public static class PropertyAccess
  {
    private static OptionPageFields generalProps;

    public static OptionPageFields GeneralProps
    {
      get
      {
        if (PropertyAccess.generalProps == null)
          PropertyAccess.generalProps = new OptionPageFields();
        return PropertyAccess.generalProps;
      }
      set
      {
        PropertyAccess.generalProps = value;
      }
    }
  }
}
