﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginMessage
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public sealed class LoginMessage : ILoginMessage
  {
    private readonly string _text;
    private readonly LoginMessageType _type;

    public string Text
    {
      get
      {
        return this._text;
      }
    }

    public LoginMessageType Type
    {
      get
      {
        return this._type;
      }
    }

    internal LoginMessage(string text, LoginMessageType type)
    {
      this._text = text;
      this._type = type;
    }
  }
}
