﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginAsyncResult
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.Collections.Generic;
using System.Net;

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  internal sealed class LoginAsyncResult : ILoginResult
  {
    private volatile IDictionary<LoginActionType, ILoginAction> _actions;
    private readonly List<ILoginMessage> _messages;
    private readonly LoginProcessor _loginProcessor;
    private readonly List<ILoginSession> _sessions;
    private CookieCollection _cookies;

    public CookieCollection Cookies
    {
      get
      {
        return this._cookies;
      }
      set
      {
        this._cookies = value;
      }
    }

    public IDictionary<string, ILoginParameter> Parameters { get; internal set; }

    internal ILoginAction PerformingAction { get; set; }

    internal bool EnableCertificate { get; set; }

    public bool CertificateSupport { get; internal set; }

    public IDictionary<LoginActionType, ILoginAction> Actions
    {
      get
      {
        return this._actions;
      }
      internal set
      {
        this._actions = value;
      }
    }

    public List<ILoginMessage> Messages
    {
      get
      {
        return this._messages;
      }
    }

    public LoginState State
    {
      get
      {
        return this._loginProcessor.LoginState;
      }
    }

    public List<ILoginSession> Sessions
    {
      get
      {
        return this._sessions;
      }
    }

    internal LoginAsyncResult(LoginProcessor loginProcessor)
    {
      this._loginProcessor = loginProcessor;
      this._messages = new List<ILoginMessage>();
      this._sessions = new List<ILoginSession>();
      this._cookies = new CookieCollection();
    }

    internal void NotifyResultHandler()
    {
      throw new NotImplementedException();
    }
  }
}
