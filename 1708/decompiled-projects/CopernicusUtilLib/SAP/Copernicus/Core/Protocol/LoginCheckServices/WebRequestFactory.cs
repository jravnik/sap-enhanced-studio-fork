﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.WebRequestFactory
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Util;
using System;
using System.Net;

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public sealed class WebRequestFactory
  {
    private readonly Uri _loginUri;

    internal WebRequestFactory(string SapBaseUri)
    {
      try
      {
        this._loginUri = new Uri(SapBaseUri + "/ap/ui/login?saml2=disabled");
      }
      catch (UriFormatException ex)
      {
        throw;
      }
    }

    internal HttpWebRequest CreateLoginRequest(string method, string contentType)
    {
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(this._loginUri);
      httpWebRequest.Method = method;
      if (contentType != null)
        httpWebRequest.ContentType = contentType;
      httpWebRequest.Headers["Cache-Control"] = "no-cache";
      httpWebRequest.Proxy = (IWebProxy) WebUtil.GetProxy();
      if (httpWebRequest.CookieContainer == null)
        httpWebRequest.CookieContainer = new CookieContainer();
      return httpWebRequest;
    }
  }
}
