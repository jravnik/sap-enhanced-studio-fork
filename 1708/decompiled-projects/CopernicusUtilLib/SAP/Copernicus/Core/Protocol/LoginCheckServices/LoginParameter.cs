﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.LoginParameter
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  internal sealed class LoginParameter : ILoginParameter
  {
    private readonly string _name;
    private string _value;
    private string _toString;

    public string Name
    {
      get
      {
        return this._name;
      }
    }

    public string Value
    {
      get
      {
        return this._value;
      }
      set
      {
        this._value = value;
      }
    }

    internal LoginParameter(string name, string value)
    {
      this._name = name;
      this._value = value;
    }

    public override string ToString()
    {
      if (this._toString == null)
        this._toString = this._name + "=" + this._value;
      return this._toString;
    }
  }
}
