﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.LoginCheckServices.ILoginResult
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.LoginCheckServices
{
  public interface ILoginResult
  {
    List<ILoginMessage> Messages { get; }

    LoginState State { get; }

    bool CertificateSupport { get; }

    IDictionary<LoginActionType, ILoginAction> Actions { get; }

    IDictionary<string, ILoginParameter> Parameters { get; }

    List<ILoginSession> Sessions { get; }
  }
}
