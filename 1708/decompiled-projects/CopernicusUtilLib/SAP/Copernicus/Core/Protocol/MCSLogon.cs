﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.MCSLogon
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.PDI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SAP.Copernicus.Core.Protocol
{
  public class MCSLogon : Form
  {
    private IContainer components;
    private RadioButton LoadMCSRadio;
    private Label label1;
    private TextBox textBox1;
    private RadioButton radioButton1;
    private Button button1;
    private Button button2;
    private Label MCSDeployToolTip;
    private Label MCSLoadToolTip;
    private ToolTip toolTip1;
    private ImageList imageList1;

    public bool Result { get; private set; }

    public string Key { get; private set; }

    public string Type { get; private set; }

    public MCSLogon()
    {
      this.InitializeComponent();
      this.Result = false;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (MCSLogon));
      this.LoadMCSRadio = new RadioButton();
      this.label1 = new Label();
      this.textBox1 = new TextBox();
      this.radioButton1 = new RadioButton();
      this.button1 = new Button();
      this.button2 = new Button();
      this.MCSLoadToolTip = new Label();
      this.imageList1 = new ImageList(this.components);
      this.MCSDeployToolTip = new Label();
      this.toolTip1 = new ToolTip(this.components);
      this.SuspendLayout();
      this.LoadMCSRadio.AutoSize = true;
      this.LoadMCSRadio.Location = new Point(37, 40);
      this.LoadMCSRadio.Name = "LoadMCSRadio";
      this.LoadMCSRadio.Size = new Size(116, 17);
      this.LoadMCSRadio.TabIndex = 0;
      this.LoadMCSRadio.TabStop = true;
      this.LoadMCSRadio.Text = "Load MCS Solution";
      this.LoadMCSRadio.UseVisualStyleBackColor = true;
      this.LoadMCSRadio.CheckedChanged += new EventHandler(this.radioButton1_CheckedChanged);
      this.label1.AutoSize = true;
      this.label1.Location = new Point(60, 75);
      this.label1.Name = "label1";
      this.label1.Size = new Size(78, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Installation Key";
      this.textBox1.Location = new Point(158, 68);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new Size(282, 20);
      this.textBox1.TabIndex = 2;
      this.radioButton1.AutoSize = true;
      this.radioButton1.Location = new Point(37, 116);
      this.radioButton1.Name = "radioButton1";
      this.radioButton1.Size = new Size(125, 17);
      this.radioButton1.TabIndex = 3;
      this.radioButton1.TabStop = true;
      this.radioButton1.Text = "Deploy MCS Solution";
      this.radioButton1.UseVisualStyleBackColor = true;
      this.radioButton1.CheckedChanged += new EventHandler(this.radioButton1_CheckedChanged_1);
      this.button1.Location = new Point(256, 168);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 23);
      this.button1.TabIndex = 4;
      this.button1.Text = "Cancel";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Location = new Point(365, 168);
      this.button2.Name = "button2";
      this.button2.Size = new Size(75, 23);
      this.button2.TabIndex = 5;
      this.button2.Text = "Continue";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.MCSLoadToolTip.ImageKey = "InformationMark.png";
      this.MCSLoadToolTip.ImageList = this.imageList1;
      this.MCSLoadToolTip.Location = new Point(159, 36);
      this.MCSLoadToolTip.Name = "MCSLoadToolTip";
      this.MCSLoadToolTip.Size = new Size(37, 23);
      this.MCSLoadToolTip.TabIndex = 6;
      this.toolTip1.SetToolTip((Control) this.MCSLoadToolTip, "Load MCS Solution");
      this.MCSLoadToolTip.Click += new EventHandler(this.MCSLoadToolTip_Click);
      this.imageList1.ImageStream = (ImageListStreamer) componentResourceManager.GetObject("imageList1.ImageStream");
      this.imageList1.TransparentColor = Color.Transparent;
      this.imageList1.Images.SetKeyName(0, "InformationMark.png");
      this.MCSDeployToolTip.ImageKey = "InformationMark.png";
      this.MCSDeployToolTip.ImageList = this.imageList1;
      this.MCSDeployToolTip.Location = new Point(159, 110);
      this.MCSDeployToolTip.Name = "MCSDeployToolTip";
      this.MCSDeployToolTip.Size = new Size(37, 23);
      this.MCSDeployToolTip.TabIndex = 7;
      this.MCSDeployToolTip.Click += new EventHandler(this.MCSDeployToolTip_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(477, 220);
      this.ControlBox = false;
      this.Controls.Add((Control) this.MCSDeployToolTip);
      this.Controls.Add((Control) this.MCSLoadToolTip);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.radioButton1);
      this.Controls.Add((Control) this.textBox1);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.LoadMCSRadio);
      this.Name = "MCSLogon";
      this.Text = "Multi-Customer Solution Logon Mode";
      this.Activated += new EventHandler(this.MCSLogon_Activated);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private void radioButton1_CheckedChanged(object sender, EventArgs e)
    {
      if (!(sender as RadioButton).Checked)
        return;
      this.textBox1.Enabled = true;
      this.radioButton1.Checked = false;
    }

    private void radioButton1_CheckedChanged_1(object sender, EventArgs e)
    {
      if (!(sender as RadioButton).Checked)
        return;
      this.LoadMCSRadio.Checked = false;
      this.textBox1.Clear();
      this.textBox1.Enabled = false;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.Result = false;
      this.Close();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      SolutionHandler solutionHandler = new SolutionHandler();
      if (this.LoadMCSRadio.Checked)
      {
        this.Type = "MCSLoad";
        if (this.textBox1.Text.Length == 0)
        {
          int num = (int) CopernicusMessageBox.Show("MCS Key Cannot be empty");
          return;
        }
        string str = solutionHandler.checkMCSInstallationKey(this.textBox1.Text, true);
        if (str.Length == 0)
        {
          int num = (int) CopernicusMessageBox.Show("MCS Key is not valid.Please enter a valid Key");
          return;
        }
        this.Key = str;
      }
      else if (this.radioButton1.Checked)
      {
        this.Type = "MCSDeploy";
        solutionHandler.checkMCSInstallationKey(this.textBox1.Text, false);
      }
      this.Result = true;
      this.Close();
    }

    private void MCSLogon_Activated(object sender, EventArgs e)
    {
      this.LoadMCSRadio.Checked = true;
    }

    private void MCSDeployToolTip_Click(object sender, EventArgs e)
    {
      int num = (int) CopernicusMessageBox.Show(Resource.MCSDeployToolTip, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    private void MCSLoadToolTip_Click(object sender, EventArgs e)
    {
      int num = (int) CopernicusMessageBox.Show(Resource.MCSLoadToolTip, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }
  }
}
