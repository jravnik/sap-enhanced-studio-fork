﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_UI_UPDATE_SCOPING
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_UI_UPDATE_SCOPING : AbstractRemoteFunction<PDI_UI_UPDATE_SCOPING.ImportingType, PDI_UI_UPDATE_SCOPING.ExportingType, PDI_UI_UPDATE_SCOPING.ChangingType, PDI_UI_UPDATE_SCOPING.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115011DDFAEE8E87D89F9C674";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string[] IT_WORKCENTERS;
      [DataMember]
      public string[] IT_WOCVIEWS;
      [DataMember]
      public bool IV_TRIGGER_RBAM_GENERATION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_RC;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
