﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PRODUCT_GET_TRKORR
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_PRODUCT_GET_TRKORR : AbstractRemoteFunction<PDI_LM_PRODUCT_GET_TRKORR.ImportingType, PDI_LM_PRODUCT_GET_TRKORR.ExportingType, PDI_LM_PRODUCT_GET_TRKORR.ChangingType, PDI_LM_PRODUCT_GET_TRKORR.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB1FDB5A8C90789D6";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_COMPONENT_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_TRKORR;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
