﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.CustomerObjectReferencesHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class CustomerObjectReferencesHandler : JSONHandler
  {
    public string ConvertModel2XML(__EXT__S_PDI_REF_UI_CUSTOM_FIELD[] IT_CUSTOM_FIELD_REFERENCES)
    {
      string s = "";
      __EXT__PDI_REF_UI_CONV_MODEL2XML refUiConvModeL2Xml = new __EXT__PDI_REF_UI_CONV_MODEL2XML();
      refUiConvModeL2Xml.Importing = new __EXT__PDI_REF_UI_CONV_MODEL2XML.ImportingType();
      refUiConvModeL2Xml.Importing.IT_CUSTOM_FIELD_REFERENCES = IT_CUSTOM_FIELD_REFERENCES;
      try
      {
        Client.getInstance().getJSONClient(false).callFunctionModule((SAPFunctionModule) refUiConvModeL2Xml, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) refUiConvModeL2Xml, false, false);
        if (refUiConvModeL2Xml.Exporting != null)
        {
          s = refUiConvModeL2Xml.Exporting.EV_XREP_XML;
          if (s != null)
            s = Encoding.UTF8.GetString(Convert.FromBase64String(s));
        }
      }
      catch (ThreadInterruptedException ex)
      {
        Trace.TraceInformation("Error in JSONClient: " + ex.Message);
      }
      catch (ThreadAbortException ex)
      {
        Trace.TraceInformation("Error in JSONClient: " + ex.Message);
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      return s;
    }

    public __EXT__S_PDI_REF_UI_CUSTOM_FIELD[] ConvertXML2Model(string IV_XREP_XML)
    {
      __EXT__S_PDI_REF_UI_CUSTOM_FIELD[] refUiCustomFieldArray = (__EXT__S_PDI_REF_UI_CUSTOM_FIELD[]) null;
      IV_XREP_XML = Convert.ToBase64String(Encoding.UTF8.GetBytes(IV_XREP_XML));
      __EXT__PDI_REF_UI_CONV_XML2MODEL refUiConvXmL2Model = new __EXT__PDI_REF_UI_CONV_XML2MODEL();
      refUiConvXmL2Model.Importing = new __EXT__PDI_REF_UI_CONV_XML2MODEL.ImportingType();
      refUiConvXmL2Model.Importing.IV_XREP_XML = IV_XREP_XML;
      refUiConvXmL2Model.Importing.IV_READ_ALL_CUSTOM_FIELDS = "X";
      refUiConvXmL2Model.Importing.IV_PRODUCT_NAME = ProjectUtil.GetOpenSolutionName();
      try
      {
        Client.getInstance().getJSONClient(false).callFunctionModule((SAPFunctionModule) refUiConvXmL2Model, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) refUiConvXmL2Model, false, false);
        if (refUiConvXmL2Model.Exporting != null)
          refUiCustomFieldArray = refUiConvXmL2Model.Exporting.ET_CUSTOM_FIELD_REFERENCES;
      }
      catch (ThreadInterruptedException ex)
      {
        Trace.TraceInformation("Error in JSONClient: " + ex.Message);
      }
      catch (ThreadAbortException ex)
      {
        Trace.TraceInformation("Error in JSONClient: " + ex.Message);
      }
      catch (Exception ex)
      {
        ProtocolException protocolException = ex as ProtocolException;
        string message = "Error in JSONClient: " + ex.Message;
        Trace.TraceError(message);
        new ProtocolException(ProtocolException.ErrorArea.CLIENT, message, ex).showPopup();
      }
      return refUiCustomFieldArray;
    }
  }
}
