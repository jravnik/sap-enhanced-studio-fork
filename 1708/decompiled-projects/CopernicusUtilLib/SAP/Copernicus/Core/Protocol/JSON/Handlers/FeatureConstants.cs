﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.FeatureConstants
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class FeatureConstants
  {
    public const string queryFeed = "QRY_FEED";
    public const string queryTTnav = "QRY_ES_TT_NAV";
    public const string firstImplProjTempl = "IMPL_PROJ_TMPL";
    public const string fineTuneImplProjTempl = "FT_IMPL_PROJ_TMPL";
  }
}
