﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ANA_GET_THINGTYPE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_ANA_GET_THINGTYPE : AbstractRemoteFunction<PDI_ANA_GET_THINGTYPE.ImportingType, PDI_ANA_GET_THINGTYPE.ExportingType, PDI_ANA_GET_THINGTYPE.ChangingType, PDI_ANA_GET_THINGTYPE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011ED19B8D5C0D18DAD4DC";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_BO_NAME;
      [DataMember]
      public string IV_NAMESPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_ANA_GET_THINGTYPE.OSLS_XREP_COMP_TT[] ET_THING_TYPE;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }

    [DataContract]
    public class OSLS_XREP_COMP_TT
    {
      [DataMember]
      public string TYPE;
      [DataMember]
      public string ID;
      [DataMember]
      public string SHORT_NAME;
    }
  }
}
