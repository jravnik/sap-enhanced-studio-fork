﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.BOgetGroupIdHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class BOgetGroupIdHandler : JSONHandler
  {
    public string getGroupId(string BOproxy, string filename)
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_BO_GET_GROUP_ID pdiBoGetGroupId = new PDI_BO_GET_GROUP_ID();
      pdiBoGetGroupId.Importing = new PDI_BO_GET_GROUP_ID.ImportingType();
      pdiBoGetGroupId.Importing.IV_PROXY = BOproxy;
      pdiBoGetGroupId.Importing.IV_UI_NAME = filename;
      try
      {
        try
        {
          jsonClient.callFunctionModule((SAPFunctionModule) pdiBoGetGroupId, false, false, false);
        }
        catch (Exception ex)
        {
          this.reportClientSideProtocolException(ex);
        }
      }
      catch (Exception ex)
      {
        return "";
      }
      if (pdiBoGetGroupId.Exporting == null)
        return "";
      if (!(pdiBoGetGroupId.Exporting.EV_SUCCESS == ""))
        return pdiBoGetGroupId.Exporting.EV_GROUP_CODE;
      this.reportServerSideProtocolException((SAPFunctionModule) pdiBoGetGroupId, false, false);
      return "";
    }
  }
}
