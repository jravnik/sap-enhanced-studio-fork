﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.OneOff_Handler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class OneOff_Handler : JSONHandler, IOneOff_Handler
  {
    public virtual bool PDI_OneOff_ASSEMBLE(string IV_SOLUTION_NAME, string IV_MCUST_DOWNLOAD_FOR_DELIVERY, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SOLUTION_ASSEMBLE)
    {
      PDI_OneOff_ASSEMBLE pdiOneOffAssemble = new PDI_OneOff_ASSEMBLE();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SOLUTION_ASSEMBLE = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiOneOffAssemble.Importing = new PDI_OneOff_ASSEMBLE.ImportingType();
        pdiOneOffAssemble.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        pdiOneOffAssemble.Importing.IV_SOLUTION_NAME = IV_SOLUTION_NAME;
        pdiOneOffAssemble.Importing.IV_MCUST_DOWNLOAD_FOR_DELIVERY = IV_MCUST_DOWNLOAD_FOR_DELIVERY;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiOneOffAssemble, false, false, false);
        if (pdiOneOffAssemble.Exporting != null)
        {
          EV_SUCCESS = pdiOneOffAssemble.Exporting.EV_SUCCESS;
          ET_MESSAGES = pdiOneOffAssemble.Exporting.ET_MESSAGES;
          EV_SOLUTION_ASSEMBLE = pdiOneOffAssemble.Exporting.EV_SOLUTION_ASSEMBLE;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) pdiOneOffAssemble, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_OneOff_UPLOAD(string IV_SOLUTION_ASSEMBLE, string IV_INSTALLATION_KEY, string IV_IS_UPLOAD_INTO_PATCH_SOL, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SOLUTION_NAME, out string EV_MAJOR_VERSION)
    {
      PDI_OneOff_DEPLOY pdiOneOffDeploy = new PDI_OneOff_DEPLOY();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SOLUTION_NAME = (string) null;
      EV_MAJOR_VERSION = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiOneOffDeploy.Importing = new PDI_OneOff_DEPLOY.ImportingType();
        pdiOneOffDeploy.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        pdiOneOffDeploy.Importing.IV_SOLUTION_ASSEMBLE = IV_SOLUTION_ASSEMBLE;
        pdiOneOffDeploy.Importing.IV_INSTALLATION_KEY = IV_INSTALLATION_KEY;
        pdiOneOffDeploy.Importing.IV_IS_UPLOAD_INTO_PATCH_SOL = IV_IS_UPLOAD_INTO_PATCH_SOL;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiOneOffDeploy, false, false, false);
        if (pdiOneOffDeploy.Exporting != null)
        {
          EV_SUCCESS = pdiOneOffDeploy.Exporting.EV_SUCCESS;
          ET_MESSAGES = pdiOneOffDeploy.Exporting.ET_MESSAGES;
          EV_SOLUTION_NAME = pdiOneOffDeploy.Exporting.EV_SOLUTION_NAME;
          EV_MAJOR_VERSION = pdiOneOffDeploy.Exporting.EV_MAJOR_VERSION;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) pdiOneOffDeploy, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDIGetSecurityInfo(string IV_KTD_NAME, out string EV_HTML, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS)
    {
      PDI_1O_GET_SECURITY_INFO oGetSecurityInfo = new PDI_1O_GET_SECURITY_INFO();
      EV_HTML = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SUCCESS = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        oGetSecurityInfo.Importing = new PDI_1O_GET_SECURITY_INFO.ImportingType();
        oGetSecurityInfo.Importing.IV_KTD_NAME = IV_KTD_NAME;
        jsonClient.callFunctionModule((SAPFunctionModule) oGetSecurityInfo, false, false, false);
        if (oGetSecurityInfo.Exporting != null)
        {
          EV_HTML = oGetSecurityInfo.Exporting.EV_HTML;
          ET_MESSAGES = oGetSecurityInfo.Exporting.ET_MESSAGES;
          EV_SUCCESS = oGetSecurityInfo.Exporting.EV_SUCCESS;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) oGetSecurityInfo, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDIGetZipInfo(string IV_SOLUTION_ASSEMBLE, string IV_FILE_NAME, out string EV_SOLUTION_NAME, out string EV_PATCH_VERSION, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_TOBE_ALIASED, out string EV_SUCCESS, out string EV_MAJOR_VERSION, out string EV_CORRECTION_SOLUTION, out string EV_DESCRIPTION, out PDI_1O_S_SDI_FILE[] ET_WEB_SERVICE, out string EV_WRITE_ACCESS_TO_SAP_BO, out string EV_PROJECT_TYPE, out string EV_SAP_ENTITIES_USED, out string EV_SAP_ENTITY_ACCESS, out string EV_USAGE_CATEGORY_OUTBOUND, out string EV_COMPILER_MIG_NEEDED, out string EV_REQ_MULTICUST_INST_KEY, out string EV_EXT_SOLUTION_NAME, out string EV_CAN_IMPORT_ORIGINAL, out string EV_PATCH_SOLUTION_EXISTS)
    {
      PDI_1O_DEPLOY_ZIP_GET_INFO deployZipGetInfo = new PDI_1O_DEPLOY_ZIP_GET_INFO();
      EV_SOLUTION_NAME = (string) null;
      EV_PATCH_VERSION = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_TOBE_ALIASED = (string) null;
      EV_SUCCESS = (string) null;
      EV_MAJOR_VERSION = (string) null;
      EV_CORRECTION_SOLUTION = (string) null;
      EV_DESCRIPTION = (string) null;
      ET_WEB_SERVICE = (PDI_1O_S_SDI_FILE[]) null;
      EV_WRITE_ACCESS_TO_SAP_BO = (string) null;
      EV_PROJECT_TYPE = (string) null;
      EV_SAP_ENTITIES_USED = (string) null;
      EV_SAP_ENTITY_ACCESS = (string) null;
      EV_USAGE_CATEGORY_OUTBOUND = (string) null;
      EV_COMPILER_MIG_NEEDED = (string) null;
      EV_REQ_MULTICUST_INST_KEY = (string) null;
      EV_EXT_SOLUTION_NAME = (string) null;
      EV_CAN_IMPORT_ORIGINAL = (string) null;
      EV_PATCH_SOLUTION_EXISTS = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        deployZipGetInfo.Importing = new PDI_1O_DEPLOY_ZIP_GET_INFO.ImportingType();
        deployZipGetInfo.Importing.IV_FILE_NAME = IV_FILE_NAME;
        deployZipGetInfo.Importing.IV_SOLUTION_ASSEMBLE = IV_SOLUTION_ASSEMBLE;
        jsonClient.callFunctionModule((SAPFunctionModule) deployZipGetInfo, false, false, false);
        if (deployZipGetInfo.Exporting != null)
        {
          EV_SOLUTION_NAME = deployZipGetInfo.Exporting.EV_SOLUTION_NAME;
          EV_PATCH_VERSION = deployZipGetInfo.Exporting.EV_PATCH_VERSION;
          ET_MESSAGES = deployZipGetInfo.Exporting.ET_MESSAGES;
          EV_TOBE_ALIASED = deployZipGetInfo.Exporting.EV_TOBE_ALIASED;
          EV_SUCCESS = deployZipGetInfo.Exporting.EV_SUCCESS;
          EV_MAJOR_VERSION = deployZipGetInfo.Exporting.EV_MAJOR_VERSION;
          EV_CORRECTION_SOLUTION = deployZipGetInfo.Exporting.EV_CORRECTION_SOLUTION;
          EV_DESCRIPTION = deployZipGetInfo.Exporting.EV_DESCRIPTION;
          ET_WEB_SERVICE = deployZipGetInfo.Exporting.ET_WEB_SERVICE;
          EV_WRITE_ACCESS_TO_SAP_BO = deployZipGetInfo.Exporting.EV_WRITE_ACCESS_TO_SAP_BO;
          EV_PROJECT_TYPE = deployZipGetInfo.Exporting.EV_PROJECT_TYPE;
          EV_SAP_ENTITIES_USED = deployZipGetInfo.Exporting.EV_SAP_ENTITIES_USED;
          EV_SAP_ENTITY_ACCESS = deployZipGetInfo.Exporting.EV_SAP_ENTITY_ACCESS;
          EV_USAGE_CATEGORY_OUTBOUND = deployZipGetInfo.Exporting.EV_USAGE_CATEGORY_OUTBOUND;
          EV_COMPILER_MIG_NEEDED = deployZipGetInfo.Exporting.EV_COMPILER_MIG_NEEDED;
          EV_REQ_MULTICUST_INST_KEY = deployZipGetInfo.Exporting.EV_REQ_MULTICUST_INST_KEY;
          EV_EXT_SOLUTION_NAME = deployZipGetInfo.Exporting.EV_EXT_SOLUTION_NAME;
          EV_CAN_IMPORT_ORIGINAL = deployZipGetInfo.Exporting.EV_CAN_IMPORT_ORIGINAL;
          EV_PATCH_SOLUTION_EXISTS = deployZipGetInfo.Exporting.EV_PATCH_SOLUTION_EXISTS;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) deployZipGetInfo, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_OneOff_ACTIVATE(string IV_SOLUTION_NAME, bool isPatchActivation, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_OneOff_ACTIVATE pdiOneOffActivate = new PDI_OneOff_ACTIVATE();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiOneOffActivate.Importing = new PDI_OneOff_ACTIVATE.ImportingType();
        pdiOneOffActivate.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        if (isPatchActivation)
          pdiOneOffActivate.Importing.IV_PHASE = "ACT_PTCH";
        else
          pdiOneOffActivate.Importing.IV_PHASE = "ACT";
        pdiOneOffActivate.Importing.IV_SOLUTION_NAME = IV_SOLUTION_NAME;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiOneOffActivate, false, false, false);
        if (pdiOneOffActivate.Exporting != null)
        {
          EV_SUCCESS = pdiOneOffActivate.Exporting.EV_SUCCESS;
          ET_MESSAGES = pdiOneOffActivate.Exporting.ET_MESSAGES;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) pdiOneOffActivate, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_OneOff_Split_Assemble(string IV_SOLUTION_NAME, bool isPatchActivation, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_SPLIT_ASSEMBLE pdiSplitAssemble = new PDI_SPLIT_ASSEMBLE();
      pdiSplitAssemble.Importing = new PDI_SPLIT_ASSEMBLE.ImportingType();
      pdiSplitAssemble.Importing.IV_PRODUCT_NAME = IV_SOLUTION_NAME;
      pdiSplitAssemble.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
      SAP.Copernicus.Core.Protocol.Connection.getInstance().CheckConnectionChanged();
      jsonClient.callFunctionModule((SAPFunctionModule) pdiSplitAssemble, false, false, false);
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) pdiSplitAssemble, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      return pdiSplitAssemble.Exporting.EV_SUCCESS == "X";
    }

    public virtual bool PDI_OneOff_Split_Download(string IV_SOLUTION_NAME, string IV_VERSION, string IV_TYPE, bool isPatchActivation, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SOLUTION_ASSEMBLE)
    {
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SOLUTION_ASSEMBLE = (string) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_SPLIT_DOWNLOAD pdiSplitDownload = new PDI_SPLIT_DOWNLOAD();
      pdiSplitDownload.Importing = new PDI_SPLIT_DOWNLOAD.ImportingType();
      pdiSplitDownload.Importing.IV_PROJECT_NAME = IV_SOLUTION_NAME;
      pdiSplitDownload.Importing.IV_MINOR_VERSION = IV_VERSION;
      pdiSplitDownload.Importing.IV_PROJECT_TYPE = IV_TYPE;
      SAP.Copernicus.Core.Protocol.Connection.getInstance().CheckConnectionChanged();
      jsonClient.callFunctionModule((SAPFunctionModule) pdiSplitDownload, false, false, false);
      if (pdiSplitDownload.Exporting != null)
      {
        EV_SUCCESS = pdiSplitDownload.Exporting.EV_SUCCESS;
        ET_MESSAGES = pdiSplitDownload.Exporting.ET_MESSAGES;
        EV_SOLUTION_ASSEMBLE = pdiSplitDownload.Exporting.EV_SOLUTION_ASSEMBLE;
      }
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) pdiSplitDownload, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
      return true;
    }

    public virtual bool PDI_OneOff_Split_Toggle(out string EV_SPLIT_ENABLED, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SPLIT_ENABLED = (string) null;
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      PDI_1O_SPLIT_TOGGLE pdi1OSplitToggle = new PDI_1O_SPLIT_TOGGLE();
      SAP.Copernicus.Core.Protocol.Connection.getInstance().CheckConnectionChanged();
      jsonClient.callFunctionModule((SAPFunctionModule) pdi1OSplitToggle, false, false, false);
      if (pdi1OSplitToggle.Exporting != null)
      {
        ET_MESSAGES = pdi1OSplitToggle.Exporting.ET_MESSAGES;
        EV_SPLIT_ENABLED = pdi1OSplitToggle.Exporting.EV_SPLIT_ENABLED;
      }
      try
      {
        this.reportServerSideProtocolException((SAPFunctionModule) pdi1OSplitToggle, false, false);
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
      return true;
    }

    public virtual bool PDI_OneOff_ACTIVATE_DELTA(string IV_SOLUTION_NAME, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_OneOff_ACTIVATE pdiOneOffActivate = new PDI_OneOff_ACTIVATE();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiOneOffActivate.Importing = new PDI_OneOff_ACTIVATE.ImportingType();
        pdiOneOffActivate.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        pdiOneOffActivate.Importing.IV_PHASE = "RAD";
        pdiOneOffActivate.Importing.IV_SOLUTION_NAME = IV_SOLUTION_NAME;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiOneOffActivate, false, false, false);
        if (pdiOneOffActivate.Exporting != null)
        {
          EV_SUCCESS = pdiOneOffActivate.Exporting.EV_SUCCESS;
          ET_MESSAGES = pdiOneOffActivate.Exporting.ET_MESSAGES;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) pdiOneOffActivate, false, false);
        EV_SUCCESS = "X";
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_OneOff_REACTIVATE(string IV_SOLUTION_NAME, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_OneOff_ACTIVATE pdiOneOffActivate = new PDI_OneOff_ACTIVATE();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiOneOffActivate.Importing = new PDI_OneOff_ACTIVATE.ImportingType();
        pdiOneOffActivate.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        pdiOneOffActivate.Importing.IV_PHASE = "RAS";
        pdiOneOffActivate.Importing.IV_SOLUTION_NAME = IV_SOLUTION_NAME;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiOneOffActivate, false, false, false);
        if (pdiOneOffActivate.Exporting != null)
        {
          EV_SUCCESS = pdiOneOffActivate.Exporting.EV_SUCCESS;
          ET_MESSAGES = pdiOneOffActivate.Exporting.ET_MESSAGES;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) pdiOneOffActivate, false, false);
        EV_SUCCESS = "X";
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_OneOff_POST_PROCESSING(string IV_SOLUTION_NAME, bool isPatchActivation, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_OneOff_ACTIVATE pdiOneOffActivate = new PDI_OneOff_ACTIVATE();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiOneOffActivate.Importing = new PDI_OneOff_ACTIVATE.ImportingType();
        pdiOneOffActivate.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        if (isPatchActivation)
          pdiOneOffActivate.Importing.IV_PHASE = "POS_PTCH";
        else
          pdiOneOffActivate.Importing.IV_PHASE = "POS";
        pdiOneOffActivate.Importing.IV_SOLUTION_NAME = IV_SOLUTION_NAME;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiOneOffActivate, false, false, false);
        if (pdiOneOffActivate.Exporting != null)
        {
          EV_SUCCESS = pdiOneOffActivate.Exporting.EV_SUCCESS;
          ET_MESSAGES = pdiOneOffActivate.Exporting.ET_MESSAGES;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) pdiOneOffActivate, false, false);
        EV_SUCCESS = "X";
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_OneOff_GET_SOLUTION_INFOS(string IV_SOLUTION_NAME, string IV_LOGLEVEL, bool suppressPopup, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out PDI_1O_SOLUTION_HEADER ES_SOLUTION_HEADER)
    {
      PDI_OneOff_GET_SOLUTION_INFOS getSolutionInfos = new PDI_OneOff_GET_SOLUTION_INFOS();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      ES_SOLUTION_HEADER = (PDI_1O_SOLUTION_HEADER) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        getSolutionInfos.Importing = new PDI_OneOff_GET_SOLUTION_INFOS.ImportingType();
        getSolutionInfos.Importing.IV_SOLUTION_NAME = IV_SOLUTION_NAME;
        getSolutionInfos.Importing.IV_LANGUAGE = "E";
        getSolutionInfos.Importing.IV_LOGLEVEL = IV_LOGLEVEL;
        jsonClient.callFunctionModule((SAPFunctionModule) getSolutionInfos, suppressPopup, false, false);
        if (getSolutionInfos.Exporting != null)
        {
          EV_SUCCESS = getSolutionInfos.Exporting.EV_SUCCESS;
          ET_MESSAGES = getSolutionInfos.Exporting.ET_MESSAGES;
          ES_SOLUTION_HEADER = getSolutionInfos.Exporting.ES_SOLUTION_HEADER;
        }
        if (!suppressPopup)
          this.reportServerSideProtocolException((SAPFunctionModule) getSolutionInfos, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_OneOff_SDI_GET_PROGRESS(string IV_PRODUCT_NAME, string IV_MAJOR_VERSION, string IV_PATCH_VERSION, out string EV_FINISHED, out string EV_PROGRESS_TEXT, out string EV_PHASE, out string EV_PHASE_STATUS)
    {
      PDI_OneOff_SDI_GET_PROGRESS offSdiGetProgress = new PDI_OneOff_SDI_GET_PROGRESS();
      EV_FINISHED = (string) null;
      EV_PROGRESS_TEXT = (string) null;
      EV_PHASE = (string) null;
      EV_PHASE_STATUS = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        offSdiGetProgress.Importing = new PDI_OneOff_SDI_GET_PROGRESS.ImportingType();
        offSdiGetProgress.Importing.IV_PRODUCT_NAME = IV_PRODUCT_NAME;
        offSdiGetProgress.Importing.IV_MAJOR_VERSION = IV_MAJOR_VERSION;
        offSdiGetProgress.Importing.IV_PATCH_VERSION = IV_PATCH_VERSION;
        jsonClient.callFunctionModule((SAPFunctionModule) offSdiGetProgress, false, false, false);
        if (offSdiGetProgress.Exporting != null)
        {
          EV_FINISHED = offSdiGetProgress.Exporting.EV_FINISHED;
          EV_PROGRESS_TEXT = offSdiGetProgress.Exporting.EV_PROGRESS_TEXT;
          EV_PHASE = offSdiGetProgress.Exporting.EV_PHASE;
          EV_PHASE_STATUS = offSdiGetProgress.Exporting.EV_PHASE_STATUS;
        }
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_OneOff_GET_SOLUTION_LOCKS(string IV_SOLUTION_NAME, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out PDI_1O_SOLUTION_LOCK[] ET_SOLUTION_LOCKS)
    {
      PDI_OneOff_GET_SOLUTION_LOCKS getSolutionLocks = new PDI_OneOff_GET_SOLUTION_LOCKS();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      ET_SOLUTION_LOCKS = (PDI_1O_SOLUTION_LOCK[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        getSolutionLocks.Importing = new PDI_OneOff_GET_SOLUTION_LOCKS.ImportingType();
        getSolutionLocks.Importing.IV_SOLUTION_NAME = IV_SOLUTION_NAME;
        jsonClient.callFunctionModule((SAPFunctionModule) getSolutionLocks, false, false, false);
        if (getSolutionLocks.Exporting != null)
        {
          EV_SUCCESS = getSolutionLocks.Exporting.EV_SUCCESS;
          ET_MESSAGES = getSolutionLocks.Exporting.ET_MESSAGES;
          ET_SOLUTION_LOCKS = getSolutionLocks.Exporting.ET_SOLUTION_LOCKS;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) getSolutionLocks, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_OneOff_CREATE_PATCH(string IV_PRODUCT_NAME, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_OneOff_CREATE_PATCH oneOffCreatePatch = new PDI_OneOff_CREATE_PATCH();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        oneOffCreatePatch.Importing = new PDI_OneOff_CREATE_PATCH.ImportingType();
        oneOffCreatePatch.Importing.IV_PRODUCT_NAME = IV_PRODUCT_NAME;
        oneOffCreatePatch.Importing.IV_DEPLOYMENT_MODE = "D";
        oneOffCreatePatch.Importing.IV_XREP_BRANCH = "";
        oneOffCreatePatch.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        jsonClient.callFunctionModule((SAPFunctionModule) oneOffCreatePatch, false, false, false);
        if (oneOffCreatePatch.Exporting != null)
        {
          EV_SUCCESS = oneOffCreatePatch.Exporting.EV_SUCCESS;
          ET_MESSAGES = oneOffCreatePatch.Exporting.ET_MESSAGES;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) oneOffCreatePatch, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_OneOff_CREATE_PATCH_BATCH(string IV_PRODUCT_NAME, bool IV_DELETION_PATCH, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_OneOff_CREATE_PATCH_BATCH createPatchBatch = new PDI_OneOff_CREATE_PATCH_BATCH();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        createPatchBatch.Importing = new PDI_OneOff_CREATE_PATCH_BATCH.ImportingType();
        createPatchBatch.Importing.IV_PRODUCT_NAME = IV_PRODUCT_NAME;
        createPatchBatch.Importing.IV_DELETION_PATCH = IV_DELETION_PATCH;
        createPatchBatch.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        jsonClient.callFunctionModule((SAPFunctionModule) createPatchBatch, false, false, false);
        if (createPatchBatch.Exporting != null)
        {
          EV_SUCCESS = createPatchBatch.Exporting.EV_SUCCESS;
          ET_MESSAGES = createPatchBatch.Exporting.ET_MESSAGES;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) createPatchBatch, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PdiOneOffProductTenantsStatus(string pstrProductName, out string pstrSuccess, out PDI_RI_S_MESSAGE[] ptblMessages, out PDI_1O_PRODUCT_TENANT_STATUS[] ptblTenantsStatus)
    {
      pstrSuccess = string.Empty;
      ptblMessages = (PDI_RI_S_MESSAGE[]) null;
      ptblTenantsStatus = new PDI_1O_PRODUCT_TENANT_STATUS[1];
      try
      {
        PDI_ONE_OFF_PRODUCT_TENANTS_STATUS productTenantsStatus = new PDI_ONE_OFF_PRODUCT_TENANTS_STATUS();
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        productTenantsStatus.Importing = new PDI_ONE_OFF_PRODUCT_TENANTS_STATUS.ImportingType();
        productTenantsStatus.Importing.IV_PRODUCT_NAME = pstrProductName;
        jsonClient.callFunctionModule((SAPFunctionModule) productTenantsStatus, false, false, false);
        if (productTenantsStatus.Exporting != null)
        {
          pstrSuccess = productTenantsStatus.Exporting.EV_SUCCESS;
          ptblMessages = productTenantsStatus.Exporting.ET_MESSAGES;
          ptblTenantsStatus = productTenantsStatus.Exporting.ET_PRODUCT_TENANT_STATUS;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) productTenantsStatus, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_1O_IS_COPY_REQUIRED(string IV_PRODUCT_NAME, out string EV_SUCCESS, out string EV_COPY_REQUIRED, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_1O_IS_COPY_REQUIRED obj = new PDI_1O_IS_COPY_REQUIRED();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_COPY_REQUIRED = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        obj.Importing = new PDI_1O_IS_COPY_REQUIRED.ImportingType();
        obj.Importing.IV_PRODUCT_NAME = IV_PRODUCT_NAME;
        jsonClient.callFunctionModule((SAPFunctionModule) obj, false, false, false);
        if (obj.Exporting != null)
        {
          EV_SUCCESS = obj.Exporting.EV_SUCCESS;
          ET_MESSAGES = obj.Exporting.ET_MESSAGES;
          EV_COPY_REQUIRED = obj.Exporting.EV_COPY_REQUIRED;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) obj, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_1O_COPY_SOLUTION(string IV_PRODUCT_NAME, out string EV_SUCCESS, out string EV_NEW_PRODUCT_NAME, out string EV_NEW_PATCH_VERSION, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_1O_COPY_SOLUTION pdi1OCopySolution = new PDI_1O_COPY_SOLUTION();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_NEW_PRODUCT_NAME = (string) null;
      EV_NEW_PATCH_VERSION = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdi1OCopySolution.Importing = new PDI_1O_COPY_SOLUTION.ImportingType();
        pdi1OCopySolution.Importing.IV_PRODUCT_NAME = IV_PRODUCT_NAME;
        jsonClient.callFunctionModule((SAPFunctionModule) pdi1OCopySolution, false, false, false);
        if (pdi1OCopySolution.Exporting != null)
        {
          EV_SUCCESS = pdi1OCopySolution.Exporting.EV_SUCCESS;
          ET_MESSAGES = pdi1OCopySolution.Exporting.ET_MESSAGES;
          EV_NEW_PRODUCT_NAME = pdi1OCopySolution.Exporting.EV_NEW_PRODUCT_NAME;
          EV_NEW_PATCH_VERSION = pdi1OCopySolution.Exporting.EV_NEW_PATCH_VERSION;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) pdi1OCopySolution, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_1O_PRODUCT_DELETE(string IV_PRODUCT_NAME)
    {
      PDI_OneOff_DISCARD pdiOneOffDiscard = new PDI_OneOff_DISCARD();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiOneOffDiscard.Importing = new PDI_OneOff_DISCARD.ImportingType();
        pdiOneOffDiscard.Importing.IV_PRODUCT_NAME = IV_PRODUCT_NAME;
        pdiOneOffDiscard.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        jsonClient.callFunctionModule((SAPFunctionModule) pdiOneOffDiscard, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiOneOffDiscard, false, false);
        if (pdiOneOffDiscard.Exporting.EV_SUCCESS == "X" && pdiOneOffDiscard.Exporting.ET_MESSAGES != null && pdiOneOffDiscard.Exporting.ET_MESSAGES.Length > 0)
          this.displayMessages(pdiOneOffDiscard.Exporting.ET_MESSAGES);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_1O_PRODUCT_DELETE_2(string IV_PRODUCT_NAME, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_OneOff_DISCARD pdiOneOffDiscard = new PDI_OneOff_DISCARD();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiOneOffDiscard.Importing = new PDI_OneOff_DISCARD.ImportingType();
        pdiOneOffDiscard.Importing.IV_PRODUCT_NAME = IV_PRODUCT_NAME;
        pdiOneOffDiscard.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        jsonClient.callFunctionModule((SAPFunctionModule) pdiOneOffDiscard, false, false, false);
        if (pdiOneOffDiscard.Exporting != null)
        {
          EV_SUCCESS = pdiOneOffDiscard.Exporting.EV_SUCCESS;
          ET_MESSAGES = pdiOneOffDiscard.Exporting.ET_MESSAGES;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) pdiOneOffDiscard, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool CanDeleteSolution(string Name)
    {
      PDI_OneOff_DISCARD pdiOneOffDiscard = new PDI_OneOff_DISCARD();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiOneOffDiscard.Importing = new PDI_OneOff_DISCARD.ImportingType();
        pdiOneOffDiscard.Importing.IV_PRODUCT_NAME = Name;
        pdiOneOffDiscard.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        pdiOneOffDiscard.Importing.IV_CHECK_ONLY = "X";
        jsonClient.callFunctionModule((SAPFunctionModule) pdiOneOffDiscard, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiOneOffDiscard, false, false);
        return pdiOneOffDiscard.Exporting.EV_SUCCESS == "X";
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    private bool executionWithErrorMessage(PDI_RI_S_MESSAGE[] messages)
    {
      foreach (PDI_RI_S_MESSAGE message in messages)
      {
        if (message.SEVERITY == "E")
          return true;
      }
      return false;
    }

    public virtual bool PDI_1O_ENABLE(string SolutionName)
    {
      PDI_1O_ENABLE pdi1OEnable = new PDI_1O_ENABLE();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdi1OEnable.Importing = new PDI_1O_ENABLE.ImportingType();
        pdi1OEnable.Importing.IV_SOLUTION_NAME = SolutionName;
        jsonClient.callFunctionModule((SAPFunctionModule) pdi1OEnable, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdi1OEnable, false, false);
        if (pdi1OEnable.Exporting.EV_SUCCESS == "X" && pdi1OEnable.Exporting.ET_MESSAGES != null && (pdi1OEnable.Exporting.ET_MESSAGES.Length > 0 && this.executionWithErrorMessage(pdi1OEnable.Exporting.ET_MESSAGES)))
          this.displayMessages(pdi1OEnable.Exporting.ET_MESSAGES);
        return pdi1OEnable.Exporting.EV_SUCCESS == "X";
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_1O_ENABLE_CHECK(string solutionName, out bool revokeIsPossible, out bool isRemoteWSSyncMergeRelevant)
    {
      revokeIsPossible = true;
      isRemoteWSSyncMergeRelevant = false;
      PDI_1O_ENABLE_CHECK pdi1OEnableCheck = new PDI_1O_ENABLE_CHECK();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdi1OEnableCheck.Importing = new PDI_1O_ENABLE_CHECK.ImportingType();
        pdi1OEnableCheck.Importing.IV_SOLUTION_NAME = solutionName;
        jsonClient.callFunctionModule((SAPFunctionModule) pdi1OEnableCheck, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdi1OEnableCheck, false, false);
        if (!(pdi1OEnableCheck.Exporting.EV_SUCCESS == "X"))
          return false;
        revokeIsPossible = pdi1OEnableCheck.Exporting.EV_REVOKE_IS_POSSIBLE == "X";
        isRemoteWSSyncMergeRelevant = pdi1OEnableCheck.Exporting.EV_IS_REMOTE_SYNC_MERGE_RLVNT == "X";
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_1O_DISABLE(string SolutionName)
    {
      PDI_1O_DISABLE pdi1ODisable = new PDI_1O_DISABLE();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdi1ODisable.Importing = new PDI_1O_DISABLE.ImportingType();
        pdi1ODisable.Importing.IV_SOLUTION_NAME = SolutionName;
        jsonClient.callFunctionModule((SAPFunctionModule) pdi1ODisable, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdi1ODisable, false, false);
        if (pdi1ODisable.Exporting.EV_SUCCESS == "X" && pdi1ODisable.Exporting.ET_MESSAGES != null && (pdi1ODisable.Exporting.ET_MESSAGES.Length > 0 && this.executionWithErrorMessage(pdi1ODisable.Exporting.ET_MESSAGES)))
          this.displayMessages(pdi1ODisable.Exporting.ET_MESSAGES);
        return pdi1ODisable.Exporting.EV_SUCCESS == "X";
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    private void displayMessages(PDI_RI_S_MESSAGE[] messages)
    {
      if (messages == null || messages.Length == 0)
        return;
      List<Message> messages1 = new List<Message>();
      foreach (PDI_RI_S_MESSAGE message in messages)
        messages1.Add(new Message(Message.MessageSeverity.FromABAPCode(message.SEVERITY), message.TEXT));
      int num = (int) new MessageDialog(messages1).ShowDialog();
    }

    public virtual bool PDIOneOffCreatePartner(string IV_PARTNER, string IV_CUSTOMER_ID, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS)
    {
      PDI_1O_PARTNER_CREATE pdi1OPartnerCreate = new PDI_1O_PARTNER_CREATE();
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SUCCESS = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdi1OPartnerCreate.Importing = new PDI_1O_PARTNER_CREATE.ImportingType();
        pdi1OPartnerCreate.Importing.IV_PARTNER = IV_PARTNER;
        pdi1OPartnerCreate.Importing.IV_CUSTOMER_ID = IV_CUSTOMER_ID;
        jsonClient.callFunctionModule((SAPFunctionModule) pdi1OPartnerCreate, false, false, false);
        if (pdi1OPartnerCreate.Exporting != null)
        {
          ET_MESSAGES = pdi1OPartnerCreate.Exporting.ET_MESSAGES;
          EV_SUCCESS = pdi1OPartnerCreate.Exporting.EV_SUCCESS;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) pdi1OPartnerCreate, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDIOneOffGetPartner(out PDI_1O_PARTNER_LIST ES_CURRENT_PARTNER, out PDI_1O_PARTNER_LIST[] ET_PARTNER, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS)
    {
      PDI_1O_PARTNER_GET pdi1OPartnerGet = new PDI_1O_PARTNER_GET();
      ES_CURRENT_PARTNER = (PDI_1O_PARTNER_LIST) null;
      ET_PARTNER = (PDI_1O_PARTNER_LIST[]) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SUCCESS = (string) null;
      try
      {
        Client.getInstance().getJSONClient(false).callFunctionModule((SAPFunctionModule) pdi1OPartnerGet, false, false, false);
        if (pdi1OPartnerGet.Exporting != null)
        {
          ES_CURRENT_PARTNER = pdi1OPartnerGet.Exporting.ES_CURRENT_PARTNER;
          ET_PARTNER = pdi1OPartnerGet.Exporting.ET_PARTNER;
          ET_MESSAGES = pdi1OPartnerGet.Exporting.ET_MESSAGES;
          EV_SUCCESS = pdi1OPartnerGet.Exporting.EV_SUCCESS;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) pdi1OPartnerGet, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDIOneOffAssignUserToPartner(string IV_PARTNER, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS)
    {
      PDI_1O_ASSIGN_USER_TO_PARTNER assignUserToPartner = new PDI_1O_ASSIGN_USER_TO_PARTNER();
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SUCCESS = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        assignUserToPartner.Importing = new PDI_1O_ASSIGN_USER_TO_PARTNER.ImportingType();
        assignUserToPartner.Importing.IV_PARTNER = IV_PARTNER;
        jsonClient.callFunctionModule((SAPFunctionModule) assignUserToPartner, false, false, false);
        if (assignUserToPartner.Exporting != null)
        {
          ET_MESSAGES = assignUserToPartner.Exporting.ET_MESSAGES;
          EV_SUCCESS = assignUserToPartner.Exporting.EV_SUCCESS;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) assignUserToPartner, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_1O_ASSEMBLE_AS_COPY(string IV_SOLUTION_NAME, string IV_COPY_SOLUTION_DESCRIPTION, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SOLUTION_ASSEMBLE, out string EV_COPY_SOLUTION_NAME)
    {
      PDI_1O_ASSEMBLE_AS_COPY obj = new PDI_1O_ASSEMBLE_AS_COPY();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SOLUTION_ASSEMBLE = (string) null;
      EV_COPY_SOLUTION_NAME = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        obj.Importing = new PDI_1O_ASSEMBLE_AS_COPY.ImportingType();
        obj.Importing.IV_SESSION_ID = SAP.Copernicus.Core.Protocol.Connection.getInstance().GetXRepSessionID();
        obj.Importing.IV_SOLUTION_NAME = IV_SOLUTION_NAME;
        obj.Importing.IV_COPY_SOLUTION_DESCRIPTION = IV_COPY_SOLUTION_DESCRIPTION;
        jsonClient.callFunctionModule((SAPFunctionModule) obj, false, false, false);
        if (obj.Exporting != null)
        {
          EV_SUCCESS = obj.Exporting.EV_SUCCESS;
          ET_MESSAGES = obj.Exporting.ET_MESSAGES;
          EV_SOLUTION_ASSEMBLE = obj.Exporting.EV_SOLUTION_ASSEMBLE;
          EV_COPY_SOLUTION_NAME = obj.Exporting.EV_COPY_SOLUTION_NAME;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) obj, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDIOneOffImportTemplate(string IV_SOLUTION_NAME, string IV_SOLUTION_ASSEMBLE, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_SUCCESS)
    {
      PDI_1O_IMPORT_TEMPLATE obj = new PDI_1O_IMPORT_TEMPLATE();
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_SUCCESS = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        obj.Importing = new PDI_1O_IMPORT_TEMPLATE.ImportingType();
        obj.Importing.IV_SOLUTION_NAME = IV_SOLUTION_NAME;
        obj.Importing.IV_SOLUTION_ASSEMBLE = IV_SOLUTION_ASSEMBLE;
        jsonClient.callFunctionModule((SAPFunctionModule) obj, false, false, false);
        if (obj.Exporting != null)
        {
          ET_MESSAGES = obj.Exporting.ET_MESSAGES;
          EV_SUCCESS = obj.Exporting.EV_SUCCESS;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) obj, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_1O_MC_CREATE_INSTALL_KEY(string IV_GENERATION_NAMESPACE, string IV_CUSTOMER_ID, out string EV_INSTALLATION_KEY, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES)
    {
      PDI_1O_MC_CREATE_INSTALL_KEY createInstallKey = new PDI_1O_MC_CREATE_INSTALL_KEY();
      EV_INSTALLATION_KEY = (string) null;
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        createInstallKey.Importing = new PDI_1O_MC_CREATE_INSTALL_KEY.ImportingType();
        createInstallKey.Importing.IV_GENERATION_NAMESPACE = IV_GENERATION_NAMESPACE;
        createInstallKey.Importing.IV_CUSTOMER_ID = IV_CUSTOMER_ID;
        jsonClient.callFunctionModule((SAPFunctionModule) createInstallKey, false, false, false);
        if (createInstallKey.Exporting != null)
        {
          EV_INSTALLATION_KEY = createInstallKey.Exporting.EV_INSTALLATION_KEY;
          EV_SUCCESS = createInstallKey.Exporting.EV_SUCCESS;
          ET_MESSAGES = createInstallKey.Exporting.ET_MESSAGES;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) createInstallKey, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_1O_MC_GET_QUALITY_INFO(string IV_GENERATION_NAMESPACE, out string EV_VALID_QUALITY_REVIEW_EXISTS, out string EV_SUCCESS, out PDI_RI_S_MESSAGE[] ET_MESSAGES, out string EV_QUALITY_IS_VALID_UNTIL)
    {
      PDI_1O_MC_GET_QUALITY_INFO mcGetQualityInfo = new PDI_1O_MC_GET_QUALITY_INFO();
      EV_VALID_QUALITY_REVIEW_EXISTS = (string) null;
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (PDI_RI_S_MESSAGE[]) null;
      EV_QUALITY_IS_VALID_UNTIL = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        mcGetQualityInfo.Importing = new PDI_1O_MC_GET_QUALITY_INFO.ImportingType();
        mcGetQualityInfo.Importing.IV_GENERATION_NAMESPACE = IV_GENERATION_NAMESPACE;
        jsonClient.callFunctionModule((SAPFunctionModule) mcGetQualityInfo, false, false, false);
        if (mcGetQualityInfo.Exporting != null)
        {
          EV_VALID_QUALITY_REVIEW_EXISTS = mcGetQualityInfo.Exporting.EV_VALID_QUALITY_REVIEW_EXISTS;
          EV_SUCCESS = mcGetQualityInfo.Exporting.EV_SUCCESS;
          ET_MESSAGES = mcGetQualityInfo.Exporting.ET_MESSAGES;
          EV_QUALITY_IS_VALID_UNTIL = mcGetQualityInfo.Exporting.EV_QUALITY_IS_VALID_UNTIL;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) mcGetQualityInfo, false, false);
        return true;
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_SDI_WORKLIST_SUPP(string IV_PROJECT_NAME, string IV_PROJECT_TYPE, string IV_CLIENT, out string EV_SUCCESS, out string ET_MESSAGES)
    {
      PDI_SDI_WORKLIST_SUPP pdiSdiWorklistSupp = new PDI_SDI_WORKLIST_SUPP();
      EV_SUCCESS = (string) null;
      ET_MESSAGES = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        pdiSdiWorklistSupp.Importing = new PDI_SDI_WORKLIST_SUPP.ImportingType();
        pdiSdiWorklistSupp.Importing.IV_PROJECT_NAME = IV_PROJECT_NAME;
        pdiSdiWorklistSupp.Importing.IV_PROJECT_TYPE = IV_PROJECT_TYPE;
        jsonClient.callFunctionModule((SAPFunctionModule) pdiSdiWorklistSupp, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) pdiSdiWorklistSupp, false, false);
        if (pdiSdiWorklistSupp.Exporting == null)
          return false;
        EV_SUCCESS = pdiSdiWorklistSupp.Exporting.EV_SUCCESS;
        return EV_SUCCESS == "X";
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }

    public virtual bool PDI_LM_PROD_UPLOAD_STATUS(string IV_PRODUCT_NAME, out string EV_SUCCESS, out string EV_UPL_STATUS)
    {
      PDI_LM_PROD_UPLOAD_STATUS prodUploadStatus = new PDI_LM_PROD_UPLOAD_STATUS();
      EV_SUCCESS = (string) null;
      EV_UPL_STATUS = (string) null;
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        prodUploadStatus.Importing = new PDI_LM_PROD_UPLOAD_STATUS.ImportingType();
        prodUploadStatus.Importing.IV_PRODUCT_NAME = IV_PRODUCT_NAME;
        jsonClient.callFunctionModule((SAPFunctionModule) prodUploadStatus, false, false, false);
        if (prodUploadStatus.Exporting != null)
        {
          EV_SUCCESS = prodUploadStatus.Exporting.EV_SUCCESS;
          EV_UPL_STATUS = prodUploadStatus.Exporting.EV_UPL_STATUS;
        }
        this.reportServerSideProtocolException((SAPFunctionModule) prodUploadStatus, false, false);
        return EV_SUCCESS == "X";
      }
      catch (ProtocolException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
        return false;
      }
    }
  }
}
