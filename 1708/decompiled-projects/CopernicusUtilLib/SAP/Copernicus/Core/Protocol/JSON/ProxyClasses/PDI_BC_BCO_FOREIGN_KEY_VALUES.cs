﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_BCO_FOREIGN_KEY_VALUES
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_BCO_FOREIGN_KEY_VALUES : AbstractRemoteFunction<PDI_BC_BCO_FOREIGN_KEY_VALUES.ImportingType, PDI_BC_BCO_FOREIGN_KEY_VALUES.ExportingType, PDI_BC_BCO_FOREIGN_KEY_VALUES.ChangingType, PDI_BC_BCO_FOREIGN_KEY_VALUES.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0122C41ED08EEF50AEE6EB9863";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_CODE_PROXYNAME;
      [DataMember]
      public string IV_NODE_ID;
      [DataMember]
      public string IV_FIELD_NAME;
      [DataMember]
      public string IV_NAMESPACE;
      [DataMember]
      public BCT_S_FIELD_VALUE_RANGE[] IT_VALUE_FILTER;
      [DataMember]
      public string IV_FLG_NO_PSM;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_S_BC_VALUE_DESCRIPTION[] ET_VALUE_DESCRIPTION;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
