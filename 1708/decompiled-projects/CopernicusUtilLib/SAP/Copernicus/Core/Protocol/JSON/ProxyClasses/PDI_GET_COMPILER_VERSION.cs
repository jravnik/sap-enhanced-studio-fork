﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_COMPILER_VERSION
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_GET_COMPILER_VERSION : AbstractRemoteFunction<PDI_GET_COMPILER_VERSION.ImportingType, PDI_GET_COMPILER_VERSION.ExportingType, PDI_GET_COMPILER_VERSION.ChangingType, PDI_GET_COMPILER_VERSION.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0383AC1EE2B5E9F2AA025E55B4";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_MDRS_NAMESPACE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_COMPILER_VERSION;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
