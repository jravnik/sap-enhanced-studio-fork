﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.BAC_PDI_S_SEM_LABEL
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class BAC_PDI_S_SEM_LABEL
  {
    [DataMember]
    public string SEMANTIC_LABEL_ID;
    [DataMember]
    public string SEMANTIC_LABEL_RELEASECODE;
    [DataMember]
    public string SEMANTIC_LABEL_USE_PARENT;
    [DataMember]
    public string SEMANTIC_LABEL_USE_CONSTRAINT;
  }
}
