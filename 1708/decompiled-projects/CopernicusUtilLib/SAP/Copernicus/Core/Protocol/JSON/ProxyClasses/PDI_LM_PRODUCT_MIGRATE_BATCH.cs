﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PRODUCT_MIGRATE_BATCH
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_LM_PRODUCT_MIGRATE_BATCH : AbstractRemoteFunction<PDI_LM_PRODUCT_MIGRATE_BATCH.ImportingType, PDI_LM_PRODUCT_MIGRATE_BATCH.ExportingType, PDI_LM_PRODUCT_MIGRATE_BATCH.ChangingType, PDI_LM_PRODUCT_MIGRATE_BATCH.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E03836E1ED2AB9A25ACA1A886AA";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SESSION_ID;
      [DataMember]
      public string IV_SOLUTION;
      [DataMember]
      public string IV_USER;
      [DataMember]
      public string IV_MODE;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_JOBNAME;
      [DataMember]
      public string EV_JOBCNT;
      [DataMember]
      public string EV_APPLOG_ID;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_LM_S_MSG_LIST[] ET_MSG_LIST;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
