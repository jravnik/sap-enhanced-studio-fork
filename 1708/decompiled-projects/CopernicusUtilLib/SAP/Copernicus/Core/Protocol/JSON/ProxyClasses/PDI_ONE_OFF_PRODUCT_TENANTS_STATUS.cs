﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ONE_OFF_PRODUCT_TENANTS_STATUS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ONE_OFF_PRODUCT_TENANTS_STATUS : AbstractRemoteFunction<PDI_ONE_OFF_PRODUCT_TENANTS_STATUS.ImportingType, PDI_ONE_OFF_PRODUCT_TENANTS_STATUS.ExportingType, PDI_ONE_OFF_PRODUCT_TENANTS_STATUS.ChangingType, PDI_ONE_OFF_PRODUCT_TENANTS_STATUS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0135861ED0A4EC7549313FDCF7";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_1O_PRODUCT_TENANT_STATUS[] ET_PRODUCT_TENANT_STATUS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
