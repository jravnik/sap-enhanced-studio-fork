﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PRODUCT_V
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PRODUCT_V
  {
    [DataMember]
    public string PRODUCT;
    [DataMember]
    public string VERSION;
    [DataMember]
    public string VERSION_ID;
    [DataMember]
    public string SAP_PRODUCT;
    [DataMember]
    public string SAP_PROD_RELEASE;
    [DataMember]
    public string STATUS;
    [DataMember]
    public string CERTI_STATUS;
    [DataMember]
    public string ASSEMBLY_STATUS;
    [DataMember]
    public string PREVIOUS_VERSION;
    [DataMember]
    public string LOCAL_VERSION;
    [DataMember]
    public string STATUS_RUNTIME;
    [DataMember]
    public string PPMS_NR;
    [DataMember]
    public string HAS_ADAPTATIONS;
    [DataMember]
    public string EDIT_STATUS;
  }
}
