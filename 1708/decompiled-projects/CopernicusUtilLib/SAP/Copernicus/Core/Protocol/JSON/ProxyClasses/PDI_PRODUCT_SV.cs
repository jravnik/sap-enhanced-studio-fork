﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_PRODUCT_SV
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_PRODUCT_SV
  {
    [DataMember]
    public string PRODUCT;
    [DataMember]
    public string PROD_VERSION;
    [DataMember]
    public string COMPONENT;
    [DataMember]
    public string COMP_VERSION;
    [DataMember]
    public string VERSION_ID;
    [DataMember]
    public string XREP_LAYERST;
    [DataMember]
    public string SDC_DELIVERY;
    [DataMember]
    public string SDC_DLIVERY_UNIT;
    [DataMember]
    public string PREVIOUS_VERSION;
    [DataMember]
    public string XREP_BRANCH;
    [DataMember]
    public string TRKORR;
    [DataMember]
    public string XREP_BC_BRANCH;
    [DataMember]
    public string CO_PAK_SMP_LINK1;
    [DataMember]
    public string CO_PAK_SMP_LINK2;
    [DataMember]
    public string EX_PAK_SMP_LINK1;
    [DataMember]
    public string EX_PAK_SMP_LINK2;
    [DataMember]
    public string PPMS_NR;
  }
}
