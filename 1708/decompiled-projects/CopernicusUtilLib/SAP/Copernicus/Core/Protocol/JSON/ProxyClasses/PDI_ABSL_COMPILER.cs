﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_COMPILER
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_COMPILER : AbstractRemoteFunction<PDI_ABSL_COMPILER.ImportingType, PDI_ABSL_COMPILER.ExportingType, PDI_ABSL_COMPILER.ChangingType, PDI_ABSL_COMPILER.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0135921EE0BAE74CA2C23A94B3";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_MDRS_NAMESPACE;
      [DataMember]
      public string IV_ABSL_COMPILER_VER;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
