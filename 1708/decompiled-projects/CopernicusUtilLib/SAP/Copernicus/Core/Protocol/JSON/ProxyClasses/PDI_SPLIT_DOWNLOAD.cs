﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_SPLIT_DOWNLOAD
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_SPLIT_DOWNLOAD : AbstractRemoteFunction<PDI_SPLIT_DOWNLOAD.ImportingType, PDI_SPLIT_DOWNLOAD.ExportingType, PDI_SPLIT_DOWNLOAD.ChangingType, PDI_SPLIT_DOWNLOAD.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0975CB1ED4B79AD6AC1C161314";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PROJECT_NAME;
      [DataMember]
      public string IV_PROJECT_TYPE;
      [DataMember]
      public string IV_MINOR_VERSION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SOLUTION_ASSEMBLE;
      [DataMember]
      public string EV_JOBNAME;
      [DataMember]
      public string EV_JOBCNT;
      [DataMember]
      public string EV_APPLOG_ID;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
