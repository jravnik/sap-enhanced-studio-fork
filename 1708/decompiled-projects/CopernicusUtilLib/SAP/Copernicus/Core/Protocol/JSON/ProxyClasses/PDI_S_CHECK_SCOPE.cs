﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_CHECK_SCOPE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_CHECK_SCOPE
  {
    [DataMember]
    public string DESIGN_TIME;
    [DataMember]
    public string GENERATED_ARTIFACTS;
    [DataMember]
    public string TEST_READYNESS;
    [DataMember]
    public string DESIGN_TIME_ORPHANED;
  }
}
