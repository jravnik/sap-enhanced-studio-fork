﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.DDICHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class DDICHandler : JSONHandler
  {
    public List<PDI_RI_S_DDIC_COMP_DESCR> GetDDICMetadata(string ddicName)
    {
      List<PDI_RI_S_DDIC_COMP_DESCR> riSDdicCompDescrList = new List<PDI_RI_S_DDIC_COMP_DESCR>();
      try
      {
        JSONClient jsonClient = Client.getInstance().getJSONClient(false);
        PDI_RI_DI_GET_STRUCT_INFO riDiGetStructInfo = new PDI_RI_DI_GET_STRUCT_INFO();
        riDiGetStructInfo.Importing = new PDI_RI_DI_GET_STRUCT_INFO.ImportingType();
        riDiGetStructInfo.Importing.IV_DDIC_NAME = ddicName;
        jsonClient.callFunctionModule((SAPFunctionModule) riDiGetStructInfo, false, false, false);
        this.reportServerSideProtocolException((SAPFunctionModule) riDiGetStructInfo, false, false);
        riSDdicCompDescrList.AddRange((IEnumerable<PDI_RI_S_DDIC_COMP_DESCR>) ((IEnumerable<PDI_RI_S_DDIC_COMP_DESCR>) riDiGetStructInfo.Exporting.ET_COMP_TABLE).ToList<PDI_RI_S_DDIC_COMP_DESCR>());
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      return riSDdicCompDescrList;
    }
  }
}
