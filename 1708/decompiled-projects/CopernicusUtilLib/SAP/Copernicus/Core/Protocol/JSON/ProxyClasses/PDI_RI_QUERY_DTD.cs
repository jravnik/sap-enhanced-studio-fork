﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_QUERY_DTD
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_QUERY_DTD : AbstractRemoteFunction<PDI_RI_QUERY_DTD.ImportingType, PDI_RI_QUERY_DTD.ExportingType, PDI_RI_QUERY_DTD.ChangingType, PDI_RI_QUERY_DTD.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB7222C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public SESF_QUERY_OPTIONS IS_QUERY_OPTION;
      [DataMember]
      public string IV_WITH_PSM_FILTER;
      [DataMember]
      public SESF_SELECTION_PARAMETER[] IT_SELECTION_PARAMETER;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_RESULT_SET;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
