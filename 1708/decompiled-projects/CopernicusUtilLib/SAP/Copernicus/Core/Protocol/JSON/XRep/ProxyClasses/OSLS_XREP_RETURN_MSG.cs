﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses.OSLS_XREP_RETURN_MSG
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.XRep.ProxyClasses
{
  [DataContract]
  public class OSLS_XREP_RETURN_MSG
  {
    [DataMember]
    public string TYPE;
    [DataMember]
    public string ID;
    [DataMember]
    public string NUMBER;
    [DataMember]
    public string MESSAGE;
    [DataMember]
    public string LOG_NO;
    [DataMember]
    public string LOG_MSG_NO;
    [DataMember]
    public string MESSAGE_V1;
    [DataMember]
    public string MESSAGE_V2;
    [DataMember]
    public string MESSAGE_V3;
    [DataMember]
    public string MESSAGE_V4;
    [DataMember]
    public string PARAMETER;
    [DataMember]
    public string ROW;
    [DataMember]
    public string FIELD;
    [DataMember]
    public string SYSTEM;
    [DataMember]
    public string EXCEPTION_CLASS;
  }
}
