﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ANA_GET_TRANS_DEF_DESCR
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_ANA_GET_TRANS_DEF_DESCR : AbstractRemoteFunction<PDI_ANA_GET_TRANS_DEF_DESCR.ImportingType, PDI_ANA_GET_TRANS_DEF_DESCR.ExportingType, PDI_ANA_GET_TRANS_DEF_DESCR.ChangingType, PDI_ANA_GET_TRANS_DEF_DESCR.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E01151C1ED083BB90A693D9886E";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_LANGUAGE_CODE;
      [DataMember]
      public PDI_PROXY_NAME[] IT_TD_PROXY_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_PROXY_NAME_AND_DESCR[] ET_TD_PROXY_NAME_AND_DESCR;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_LANGUAGE_CODE;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
