﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.TTEC_DT_TEXTS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class TTEC_DT_TEXTS
  {
    [DataMember]
    public string CLIENT;
    [DataMember]
    public string LANGUAGE;
    [DataMember]
    public string TREEID;
    [DataMember]
    public string TREETYPE;
    [DataMember]
    public string VERSION;
    [DataMember]
    public string TEXTID;
    [DataMember]
    public string TEXT;
  }
}
