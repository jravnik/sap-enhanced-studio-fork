﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ABSL_S_DUMP
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_ABSL_S_DUMP
  {
    [DataMember]
    public string DATUM;
    [DataMember]
    public string UZEIT;
    [DataMember]
    public string USERALIAS;
    [DataMember]
    public string ERRID;
    [DataMember]
    public string ERRTEXT;
    [DataMember]
    public string SOLUTION;
    [DataMember]
    public string PATH;
    [DataMember]
    public int LINE;
    [DataMember]
    public int COL;
    [DataMember]
    public string STATUS;
    [DataMember]
    public string TRANSID;
  }
}
