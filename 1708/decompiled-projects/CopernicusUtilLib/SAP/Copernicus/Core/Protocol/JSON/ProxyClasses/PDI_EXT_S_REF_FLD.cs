﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_EXT_S_REF_FLD
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_EXT_S_REF_FLD
  {
    [DataMember]
    public string TYPE;
    [DataMember]
    public string BUNDLE;
    [DataMember]
    public string FIELD;
    [DataMember]
    public string DESCRIPTION;
    [DataMember]
    public string UI_MODEL;
    [DataMember]
    public string UI_MODEL_PATH;
    [DataMember]
    public string BO_ESR_NAME;
    [DataMember]
    public string BO_ESR_NAMESPACE;
  }
}
