﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_ASSIGN_USER_TO_PARTNER
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_ASSIGN_USER_TO_PARTNER : AbstractRemoteFunction<PDI_1O_ASSIGN_USER_TO_PARTNER.ImportingType, PDI_1O_ASSIGN_USER_TO_PARTNER.ExportingType, PDI_1O_ASSIGN_USER_TO_PARTNER.ChangingType, PDI_1O_ASSIGN_USER_TO_PARTNER.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011EE19BBB2048EBD741C5";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PARTNER;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
