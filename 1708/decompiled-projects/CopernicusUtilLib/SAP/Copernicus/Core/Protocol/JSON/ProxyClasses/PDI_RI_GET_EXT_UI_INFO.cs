﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_GET_EXT_UI_INFO
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_GET_EXT_UI_INFO : AbstractRemoteFunction<PDI_RI_GET_EXT_UI_INFO.ImportingType, PDI_RI_GET_EXT_UI_INFO.ExportingType, PDI_RI_GET_EXT_UI_INFO.ChangingType, PDI_RI_GET_EXT_UI_INFO.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0114E41ED08287FF0ADB544EDA";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_XREP_PATH;
      [DataMember]
      public string IV_CLEAN_CALL;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_T_EXT_UI_INFO[] ET_EXT_UI_INFO;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
