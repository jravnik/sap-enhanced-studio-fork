﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_BCO_SCHEMA_NODE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_BCO_SCHEMA_NODE
  {
    [DataMember]
    public string BCO_NODE_NAME_PROXY;
    [DataMember]
    public string BCO_PARENT_NODE_PROXY;
    [DataMember]
    public string BCO_NODE_NAME;
    [DataMember]
    public string BCO_NODE_NAME_DESCRIPTION;
    [DataMember]
    public string BCO_NODE_PSM_STATUS;
    [DataMember]
    public string BCO_NODE_PSM_READ_ONLY;
    [DataMember]
    public string BCO_NODE_DOCUMENTATION;
    [DataMember]
    public string BC_NODE_ID;
    [DataMember]
    public string BC_NODE_KEY_ATTR_NAME;
    [DataMember]
    public string BC_SCHEMA_ID;
    [DataMember]
    public string BC_NODE_IS_TEXT;
    [DataMember]
    public I_S_BCO_SCHEMA_CONTENT[] T_CONTENT;
  }
}
