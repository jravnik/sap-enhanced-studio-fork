﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_GET_REUSE_UIS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_GET_REUSE_UIS : AbstractRemoteFunction<PDI_GET_REUSE_UIS.ImportingType, PDI_GET_REUSE_UIS.ExportingType, PDI_GET_REUSE_UIS.ChangingType, PDI_GET_REUSE_UIS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E03836E1ED2B8F7D948912D1F77";
      }
    }

    [DataContract]
    public class ImportingType
    {
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_REUSE_UI[] ET_REUSE_UIS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
