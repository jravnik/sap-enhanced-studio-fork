﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PRODUCT_CREATE
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_PRODUCT_CREATE : AbstractRemoteFunction<PDI_LM_PRODUCT_CREATE.ImportingType, PDI_LM_PRODUCT_CREATE.ExportingType, PDI_LM_PRODUCT_CREATE.ChangingType, PDI_LM_PRODUCT_CREATE.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194E54BB71FEC9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
      [DataMember]
      public string IV_PRODUCT_DESCRIPTION;
      [DataMember]
      public string IV_PRODUCT_TYPE;
      [DataMember]
      public string IV_CREATE_MODE;
      [DataMember]
      public string IV_DEPLOYMENT_UNIT;
      [DataMember]
      public string IV_SESSION_ID;
      [DataMember]
      public string IV_DEV_PARTNER;
      [DataMember]
      public string IV_CONTACT_PERSON;
      [DataMember]
      public string IV_EMAIL;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public PDI_S_PRODUCT_VERS_AND_COMPS ES_PRODUCT;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
