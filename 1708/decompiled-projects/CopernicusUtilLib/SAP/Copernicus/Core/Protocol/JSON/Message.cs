﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Message
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Collections.Generic;
using System.Text;

namespace SAP.Copernicus.Core.Protocol.JSON
{
  public class Message
  {
    public string Text { get; private set; }

    public Message.MessageSeverity Severity { get; private set; }

    public Message(Message.MessageSeverity severity, string text)
    {
      this.Severity = severity;
      this.Text = text;
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("Message(");
      stringBuilder.Append((object) this.Severity).Append("): " + this.Text);
      return stringBuilder.ToString();
    }

    public class MessageSeverity
    {
      public static readonly Message.MessageSeverity Error = new Message.MessageSeverity(0);
      public static readonly Message.MessageSeverity Info = new Message.MessageSeverity(1);
      public static readonly Message.MessageSeverity Success = new Message.MessageSeverity(2);
      public static readonly Message.MessageSeverity Warning = new Message.MessageSeverity(3);
      private readonly int Value;

      public static Message.MessageSeverity[] Values
      {
        get
        {
          return new Message.MessageSeverity[4]{ Message.MessageSeverity.Error, Message.MessageSeverity.Info, Message.MessageSeverity.Success, Message.MessageSeverity.Warning };
        }
      }

      public static IEnumerable<Message.MessageSeverity> ValuesEnumerator
      {
        get
        {
          yield return Message.MessageSeverity.Error;
          yield return Message.MessageSeverity.Info;
          yield return Message.MessageSeverity.Success;
          yield return Message.MessageSeverity.Warning;
        }
      }

      public int IconIndex
      {
        get
        {
          return this.Value;
        }
      }

      private MessageSeverity(int value)
      {
        this.Value = value;
      }

      public static Message.MessageSeverity FromValue(int value)
      {
        switch (value)
        {
          case 0:
            return Message.MessageSeverity.Error;
          case 1:
            return Message.MessageSeverity.Info;
          case 2:
            return Message.MessageSeverity.Success;
          case 3:
            return Message.MessageSeverity.Warning;
          default:
            return (Message.MessageSeverity) null;
        }
      }

      public static Message.MessageSeverity FromABAPCode(string value)
      {
        switch (value.ToUpper())
        {
          case "E":
            return Message.MessageSeverity.Error;
          case "I":
            return Message.MessageSeverity.Info;
          case "S":
            return Message.MessageSeverity.Success;
          case "W":
            return Message.MessageSeverity.Warning;
          default:
            return (Message.MessageSeverity) null;
        }
      }

      public override string ToString()
      {
        switch (this.Value)
        {
          case 0:
            return "Error";
          case 1:
            return "Info";
          case 2:
            return "Success";
          case 3:
            return "Warning";
          default:
            return (string) null;
        }
      }
    }
  }
}
