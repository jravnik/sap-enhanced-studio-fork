﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_NODE_EXT_GET_SCENARIOS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_NODE_EXT_GET_SCENARIOS : AbstractRemoteFunction<PDI_NODE_EXT_GET_SCENARIOS.ImportingType, PDI_NODE_EXT_GET_SCENARIOS.ExportingType, PDI_NODE_EXT_GET_SCENARIOS.ChangingType, PDI_NODE_EXT_GET_SCENARIOS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E1267F91EE6A9B230F2F0AF1A88";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public PDI_NODE_EXT_S_BO_NODE_KEY[] IT_BO_NODES;
      [DataMember]
      public string IV_SOLUTION_PREFIX;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SCENARIOS;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_ALT_KEY_EXISTS;
      [DataMember]
      public string EV_SCENARIO_USED;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
