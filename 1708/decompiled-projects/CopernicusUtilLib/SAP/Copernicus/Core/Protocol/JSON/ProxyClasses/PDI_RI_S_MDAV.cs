﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_S_MDAV
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_S_MDAV
  {
    [DataMember]
    public PDI_RI_S_ASSOC_KEY KEY;
    [DataMember]
    public string MDAV_NAME;
    [DataMember]
    public string CHAR_NAME;
    [DataMember]
    public string NEEDS_COMPOUNDING;
    [DataMember]
    public string UI_TEXT;
  }
}
