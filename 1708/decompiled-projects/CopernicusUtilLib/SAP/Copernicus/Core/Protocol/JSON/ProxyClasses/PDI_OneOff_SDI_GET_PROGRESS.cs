﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_OneOff_SDI_GET_PROGRESS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_OneOff_SDI_GET_PROGRESS : AbstractRemoteFunction<PDI_OneOff_SDI_GET_PROGRESS.ImportingType, PDI_OneOff_SDI_GET_PROGRESS.ExportingType, PDI_OneOff_SDI_GET_PROGRESS.ChangingType, PDI_OneOff_SDI_GET_PROGRESS.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E028DA61EE1AAF88D654A2CD359";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
      [DataMember]
      public string IV_MAJOR_VERSION;
      [DataMember]
      public string IV_PATCH_VERSION;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_FINISHED;
      [DataMember]
      public string EV_PROGRESS_TEXT;
      [DataMember]
      public string EV_PHASE;
      [DataMember]
      public string EV_PHASE_STATUS;
      [DataMember]
      public string EV_PATCH_PRODUCT_NAME;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
