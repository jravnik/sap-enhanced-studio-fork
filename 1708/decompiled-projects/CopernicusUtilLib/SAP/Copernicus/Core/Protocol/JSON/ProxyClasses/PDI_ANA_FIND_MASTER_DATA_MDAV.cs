﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_ANA_FIND_MASTER_DATA_MDAV
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  public class PDI_ANA_FIND_MASTER_DATA_MDAV : AbstractRemoteFunction<PDI_ANA_FIND_MASTER_DATA_MDAV.ImportingType, PDI_ANA_FIND_MASTER_DATA_MDAV.ExportingType, PDI_ANA_FIND_MASTER_DATA_MDAV.ChangingType, PDI_ANA_FIND_MASTER_DATA_MDAV.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0115B01DDFB194EC88B8F06C9B";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public PDI_RI_S_ASSOC_KEY[] IT_BO_ASSOCIATION_KEY;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MDAV[] ET_MASTER_DATA;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
