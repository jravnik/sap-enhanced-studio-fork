﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_10_MC_INSTLN_KEY_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  internal class PDI_10_MC_INSTLN_KEY_CHECK : AbstractRemoteFunction<PDI_10_MC_INSTLN_KEY_CHECK.ImportingType, PDI_10_MC_INSTLN_KEY_CHECK.ExportingType, PDI_10_MC_INSTLN_KEY_CHECK.ChangingType, PDI_10_MC_INSTLN_KEY_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0520391ED3A6917AAE9C9C1D46";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_MCS_MODE;
      [DataMember]
      public string IV_INSTALLATION_KEY;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_SOLUTION_NAME;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
