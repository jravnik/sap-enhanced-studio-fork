﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_1O_ENABLE_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_1O_ENABLE_CHECK : AbstractRemoteFunction<PDI_1O_ENABLE_CHECK.ImportingType, PDI_1O_ENABLE_CHECK.ExportingType, PDI_1O_ENABLE_CHECK.ChangingType, PDI_1O_ENABLE_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "0000000000011EE19CFFBE1BF8639539";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_ENABLE_IS_ALLOWED;
      [DataMember]
      public string EV_REVOKE_IS_POSSIBLE;
      [DataMember]
      public string EV_IS_REMOTE_SYNC_MERGE_RLVNT;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
