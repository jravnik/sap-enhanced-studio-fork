﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_LM_PRODUCT_ACTIVATE_BATCH
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_LM_PRODUCT_ACTIVATE_BATCH : AbstractRemoteFunction<PDI_LM_PRODUCT_ACTIVATE_BATCH.ImportingType, PDI_LM_PRODUCT_ACTIVATE_BATCH.ExportingType, PDI_LM_PRODUCT_ACTIVATE_BATCH.ChangingType, PDI_LM_PRODUCT_ACTIVATE_BATCH.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E028DAE1EE1ABE2189C1FF64B07";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_PRODUCT_NAME;
      [DataMember]
      public string IV_SESSION_ID;
      [DataMember]
      public string IV_MODE;
      [DataMember]
      public string IV_DELTA_MODE;
      [DataMember]
      public string IV_CALLER;
      [DataMember]
      public string IV_ACT_SPLIT;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_JOBNAME;
      [DataMember]
      public string EV_JOBCNT;
      [DataMember]
      public string EV_APPLOG_ID;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
