﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_CRL_S_PARAM_TYPE_HELP
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_CRL_S_PARAM_TYPE_HELP
  {
    [DataMember]
    public PDI_CRL_PARAM_DATA_TYPE DATA_TYPE;
    [DataMember]
    public PDI_CRL_PARAM_NODE_TYPE NODE_TYPE;
    [DataMember]
    public string TOOLTIP_NAME;
    [DataMember]
    public string IS_DATA_TYPE;
    [DataMember]
    public string IS_NODE_TYPE;
    [DataMember]
    public string IS_DEPRECATED;
  }
}
