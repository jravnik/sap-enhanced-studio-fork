﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_BC_CODE_GDT
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_BC_CODE_GDT
  {
    public string NAMEfield;
    public string PROXYNAMEfield;
    public string NAMESPACEfield;
    public int LENGTHfield;
    public int DECIMALfield;

    [DataMember]
    public string NAME
    {
      get
      {
        return this.NAMEfield;
      }
      set
      {
        this.NAMEfield = value;
      }
    }

    [DataMember]
    public string PROXYNAME
    {
      get
      {
        return this.PROXYNAMEfield;
      }
      set
      {
        this.PROXYNAMEfield = value;
      }
    }

    [DataMember]
    public string NAMESPACE
    {
      get
      {
        return this.NAMESPACEfield;
      }
      set
      {
        this.NAMESPACEfield = value;
      }
    }

    [DataMember]
    public int LENGTH
    {
      get
      {
        return this.LENGTHfield;
      }
      set
      {
        this.LENGTHfield = value;
      }
    }

    [DataMember]
    public int DECIMAL
    {
      get
      {
        return this.DECIMALfield;
      }
      set
      {
        this.DECIMALfield = value;
      }
    }
  }
}
