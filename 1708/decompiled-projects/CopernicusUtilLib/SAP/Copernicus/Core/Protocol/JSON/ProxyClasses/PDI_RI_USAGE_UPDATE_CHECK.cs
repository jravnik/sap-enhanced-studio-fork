﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_RI_USAGE_UPDATE_CHECK
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector.Services.JSONConnector;
using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_RI_USAGE_UPDATE_CHECK : AbstractRemoteFunction<PDI_RI_USAGE_UPDATE_CHECK.ImportingType, PDI_RI_USAGE_UPDATE_CHECK.ExportingType, PDI_RI_USAGE_UPDATE_CHECK.ChangingType, PDI_RI_USAGE_UPDATE_CHECK.TablesType>
  {
    public override string FunctionName
    {
      get
      {
        return "00163E0135771EE0B5F046FAD48F48EF";
      }
    }

    [DataContract]
    public class ImportingType
    {
      [DataMember]
      public string IV_SOLUTION_NAME;
    }

    [DataContract]
    public class ExportingType
    {
      [DataMember]
      public PDI_RI_S_MESSAGE[] ET_MESSAGES;
      [DataMember]
      public string EV_SUCCESS;
      [DataMember]
      public string EV_ALREADY_UPDATED;
    }

    [DataContract]
    public class ChangingType
    {
    }

    [DataContract]
    public class TablesType
    {
    }
  }
}
