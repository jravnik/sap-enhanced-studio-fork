﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.PDI_S_PRODUCT_V_AND_COMPONENTS
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class PDI_S_PRODUCT_V_AND_COMPONENTS
  {
    [DataMember]
    public PDI_PRODUCT_V PRODUCT_VERSION;
    [DataMember]
    public PDI_PRODUCT_V_T[] PRODUCT_VERSION_TEXTS;
    [DataMember]
    public PDI_S_PRODUCT_COMP_VERS_FULL[] COMP_VERSIONS;
    [DataMember]
    public PDI_PRODUCT_V_SL[] PRODUCT_VERSION_SUPPORTED_LANG;
    [DataMember]
    public PDI_PROD_PATCH[] PRODUCT_VERSION_PATCH;
    [DataMember]
    public string PV_OVERALL_STATUS;
    [DataMember]
    public string PV_CERTIFICATION_STATUS;
    [DataMember]
    public string PV_1O_PATCH_SOLUTION;
    [DataMember]
    public string EXTERNAL_NAMESPACE;
    [DataMember]
    public string IS_PROD_FIX_ALLOWED;
    [DataMember]
    public string IS_PROD_FIX_ASSIGNED;
    [DataMember]
    public string LONG_TEXT;
    [DataMember]
    public string KEYWORD;
  }
}
