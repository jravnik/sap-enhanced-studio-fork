﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.Handlers.XODataHandler
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using com.sap.JSONConnector;
using com.sap.JSONConnector.Services.JSONConnector;
using SAP.Copernicus.Core.Protocol.JSON.ProxyClasses;
using System;

namespace SAP.Copernicus.Core.Protocol.JSON.Handlers
{
  public class XODataHandler : JSONHandler
  {
    public string[] GetStandardOdataServices()
    {
      JSONClient jsonClient = Client.getInstance().getJSONClient(false);
      GET_ODATA_SERVICE_NAMES odataServiceNames = new GET_ODATA_SERVICE_NAMES();
      odataServiceNames.Importing = new GET_ODATA_SERVICE_NAMES.ImportingType();
      try
      {
        jsonClient.callFunctionModule((SAPFunctionModule) odataServiceNames, false, false, false);
      }
      catch (Exception ex)
      {
        this.reportClientSideProtocolException(ex);
      }
      if (odataServiceNames.Exporting != null)
        return odataServiceNames.Exporting.SERVICE_LIST;
      return (string[]) null;
    }
  }
}
