﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.Protocol.JSON.ProxyClasses.OSLS_XREP_FILE_ATTR
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace SAP.Copernicus.Core.Protocol.JSON.ProxyClasses
{
  [DataContract]
  public class OSLS_XREP_FILE_ATTR
  {
    [DataMember]
    public string SPRAS;
    [DataMember]
    public string NAME;
    [DataMember]
    public string VALUE;
    [DataMember]
    public string SEQUENCE_NUMBER;
    [DataMember]
    public string NEED_TRANSLATION;
    [DataMember]
    public string TYPE;
  }
}
