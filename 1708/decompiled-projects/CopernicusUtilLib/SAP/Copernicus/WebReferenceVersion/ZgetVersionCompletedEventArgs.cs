﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.WebReferenceVersion.ZgetVersionCompletedEventArgs
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace SAP.Copernicus.WebReferenceVersion
{
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [GeneratedCode("System.Web.Services", "4.0.30319.17929")]
  public class ZgetVersionCompletedEventArgs : AsyncCompletedEventArgs
  {
    private object[] results;

    public ZgetVersionResponse Result
    {
      get
      {
        this.RaiseExceptionIfNecessary();
        return (ZgetVersionResponse) this.results[0];
      }
    }

    internal ZgetVersionCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState)
      : base(exception, cancelled, userState)
    {
      this.results = results;
    }
  }
}
