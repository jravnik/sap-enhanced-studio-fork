﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.TextPoolEntryType
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
  [DebuggerStepThrough]
  [Serializable]
  public class TextPoolEntryType : ModelEntity
  {
    private string textUuidField;
    private string textField;
    private TextCategoryType textCategoryField;

    [XmlAttribute]
    public string textUuid
    {
      get
      {
        return this.textUuidField;
      }
      set
      {
        this.textUuidField = value;
      }
    }

    [XmlAttribute]
    public string text
    {
      get
      {
        return this.textField;
      }
      set
      {
        this.textField = value;
      }
    }

    [XmlAttribute]
    public TextCategoryType textCategory
    {
      get
      {
        return this.textCategoryField;
      }
      set
      {
        this.textCategoryField = value;
      }
    }
  }
}
