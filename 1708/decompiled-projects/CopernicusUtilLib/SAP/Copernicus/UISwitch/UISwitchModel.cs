﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.UISwitchModel
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using SAP.Copernicus.Core.Util;
using System.IO;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  public class UISwitchModel
  {
    public SAP.Copernicus.UISwitch.UISwitch BuildUISwitchXMLObject(string SelectedProjectNameSpace, string UISwitchName, string UISwitchDescr)
    {
      string abapNamespacePrefix = RepositoryUtil.GetLocalABAPNamespacePrefix(SelectedProjectNameSpace);
      string proxyNameForName1 = RepositoryUtil.GetProxyNameForName(abapNamespacePrefix, (string) null, UISwitchName, 32);
      string proxyNameForName2 = RepositoryUtil.GetProxyNameForName(abapNamespacePrefix, (string) null, UISwitchName + "DESCRIPTION", 32);
      string str1 = abapNamespacePrefix + UISwitchName + "NAME";
      string str2 = abapNamespacePrefix + UISwitchName + "DESCRIPTION";
      SAP.Copernicus.UISwitch.UISwitch uiSwitch = new SAP.Copernicus.UISwitch.UISwitch();
      uiSwitch.id = abapNamespacePrefix + UISwitchName;
      uiSwitch.nameTextPoolId = proxyNameForName1;
      uiSwitch.descriptionTextPoolId = proxyNameForName2;
      uiSwitch.TextPool = new TextBlockType[1];
      uiSwitch.TextPool[0] = new TextBlockType();
      uiSwitch.TextPool[0].language = "EN";
      uiSwitch.TextPool[0].masterLanguage = true;
      uiSwitch.TextPool[0].TextPoolEntry = new TextPoolEntryType[2];
      uiSwitch.TextPool[0].TextPoolEntry[0] = new TextPoolEntryType();
      uiSwitch.TextPool[0].TextPoolEntry[1] = new TextPoolEntryType();
      uiSwitch.TextPool[0].TextPoolEntry[0].id = str1;
      uiSwitch.TextPool[0].TextPoolEntry[0].textUuid = proxyNameForName1;
      uiSwitch.TextPool[0].TextPoolEntry[0].text = UISwitchName;
      uiSwitch.TextPool[0].TextPoolEntry[0].textCategory = TextCategoryType.XTXT;
      uiSwitch.TextPool[0].TextPoolEntry[0].textCategory = TextCategoryType.XTXT;
      uiSwitch.TextPool[0].TextPoolEntry[1].id = str2;
      uiSwitch.TextPool[0].TextPoolEntry[1].textUuid = proxyNameForName2;
      uiSwitch.TextPool[0].TextPoolEntry[1].text = UISwitchDescr;
      uiSwitch.TextPool[0].TextPoolEntry[1].textCategory = TextCategoryType.XTXT;
      uiSwitch.TextPool[0].TextPoolEntry[1].textCategory = TextCategoryType.YTXT;
      return uiSwitch;
    }

    public string ConvertXMLObjectToXMLString(SAP.Copernicus.UISwitch.UISwitch XMLObject)
    {
      XmlSerializer xmlSerializer = new XmlSerializer(XMLObject.GetType());
      StringWriterUTF8 stringWriterUtF8 = new StringWriterUTF8();
      xmlSerializer.Serialize((TextWriter) stringWriterUtF8, (object) XMLObject);
      return stringWriterUtF8.GetStringBuilder().ToString();
    }

    public SAP.Copernicus.UISwitch.UISwitch ConvertXMLStringToXMLObject(string localXMLFilePath)
    {
      using (FileStream fileStream = new FileStream(localXMLFilePath, FileMode.Open, FileAccess.Read))
        return (SAP.Copernicus.UISwitch.UISwitch) new XmlSerializer(typeof (SAP.Copernicus.UISwitch.UISwitch)).Deserialize((Stream) fileStream);
    }
  }
}
