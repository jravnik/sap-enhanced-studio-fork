﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.UISwitch.PropertyBagType
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SAP.Copernicus.UISwitch
{
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://www.sap.com/a1s/cd/oberon/base-1.0")]
  [GeneratedCode("xsd", "2.0.50727.3038")]
  [DesignerCategory("code")]
  [Serializable]
  public class PropertyBagType : ModelEntity
  {
    private PropertyType[] propertyField;
    private PropertyBagType[] subPropertyBagField;
    private string nameField;

    [XmlElement("Property")]
    public PropertyType[] Property
    {
      get
      {
        return this.propertyField;
      }
      set
      {
        this.propertyField = value;
      }
    }

    [XmlElement("SubPropertyBag")]
    public PropertyBagType[] SubPropertyBag
    {
      get
      {
        return this.subPropertyBagField;
      }
      set
      {
        this.subPropertyBagField = value;
      }
    }

    [XmlAttribute]
    public string name
    {
      get
      {
        return this.nameField;
      }
      set
      {
        this.nameField = value;
      }
    }
  }
}
