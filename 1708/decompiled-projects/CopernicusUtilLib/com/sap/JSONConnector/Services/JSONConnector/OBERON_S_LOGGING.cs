﻿// Decompiled with JetBrains decompiler
// Type: com.sap.JSONConnector.Services.JSONConnector.OBERON_S_LOGGING
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.Serialization;

namespace com.sap.JSONConnector.Services.JSONConnector
{
  [DataContract]
  public class OBERON_S_LOGGING
  {
    [DataMember]
    public string ID;
    [DataMember]
    public string TYPE;
    [DataMember]
    public string TIMESTAMPL;
    [DataMember]
    public string DESCRIPTION;
  }
}
