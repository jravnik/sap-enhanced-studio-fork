﻿// Decompiled with JetBrains decompiler
// Type: WindowsInstaller.Database
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace WindowsInstaller
{
  [TypeIdentifier]
  [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
  [Guid("000C109D-0000-0000-C000-000000000046")]
  [CompilerGenerated]
  [ComImport]
  public interface Database
  {
    [DispId(3)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.Interface)]
    View OpenView([MarshalAs(UnmanagedType.BStr), In] string Sql);
  }
}
