﻿// Decompiled with JetBrains decompiler
// Type: WindowsInstaller.View
// Assembly: CopernicusUtilLib, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 57C39D84-2A9B-4EC5-8489-2433DD93270E
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\CopernicusUtilLib.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace WindowsInstaller
{
  [Guid("000C109C-0000-0000-C000-000000000046")]
  [TypeIdentifier]
  [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
  [CompilerGenerated]
  [ComImport]
  public interface View
  {
    [DispId(1)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    void Execute([MarshalAs(UnmanagedType.Interface), In] Record Params = null);

    [DispId(2)]
    [MethodImpl(MethodImplOptions.PreserveSig)]
    [return: MarshalAs(UnmanagedType.Interface)]
    Record Fetch();
  }
}
