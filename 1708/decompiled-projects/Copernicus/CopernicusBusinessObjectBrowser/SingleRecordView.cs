﻿// Decompiled with JetBrains decompiler
// Type: CopernicusBusinessObjectBrowser.SingleRecordView
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace CopernicusBusinessObjectBrowser
{
  public class SingleRecordView : Form
  {
    private IContainer components;
    private DataGridView dataGridView1;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private DataGridViewTextBoxColumn Attribut;
    private DataGridViewTextBoxColumn Value;

    public SingleRecordView(string[][] table)
    {
      this.InitializeComponent();
      this.setupTable(table);
    }

    private void setupTable(string[][] table)
    {
      for (int index = 0; index < table.Length; ++index)
        this.dataGridView1.Rows.Add((object) table[index][0], (object) table[index][1]);
    }

    private void SingleRecordView_Shown(object sender, EventArgs e)
    {
      this.dataGridView1.Height = this.dataGridView1.GetRowDisplayRectangle(this.dataGridView1.Rows.Count - 1, true).Bottom + this.dataGridView1.GetRowDisplayRectangle(this.dataGridView1.Rows.Count - 1, false).Height;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.dataGridView1 = new DataGridView();
      this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
      this.Attribut = new DataGridViewTextBoxColumn();
      this.Value = new DataGridViewTextBoxColumn();
      ((ISupportInitialize) this.dataGridView1).BeginInit();
      this.SuspendLayout();
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
      this.dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
      this.dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange((DataGridViewColumn) this.Attribut, (DataGridViewColumn) this.Value);
      this.dataGridView1.Dock = DockStyle.Fill;
      this.dataGridView1.EditMode = DataGridViewEditMode.EditProgrammatically;
      this.dataGridView1.Location = new Point(0, 0);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.Size = new Size(519, 444);
      this.dataGridView1.TabIndex = 0;
      this.dataGridViewTextBoxColumn1.HeaderText = "Attribut";
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.Width = 65;
      this.dataGridViewTextBoxColumn2.HeaderText = "Value";
      this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
      this.dataGridViewTextBoxColumn2.Width = 59;
      this.Attribut.HeaderText = "Attribute";
      this.Attribut.Name = "Attribut";
      this.Attribut.Width = 71;
      this.Value.HeaderText = "Value";
      this.Value.Name = "Value";
      this.Value.Width = 59;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.ClientSize = new Size(519, 444);
      this.Controls.Add((Control) this.dataGridView1);
      this.Name = "SingleRecordView";
      this.Text = "Single-Record View";
      this.Shown += new EventHandler(this.SingleRecordView_Shown);
      ((ISupportInitialize) this.dataGridView1).EndInit();
      this.ResumeLayout(false);
    }
  }
}
