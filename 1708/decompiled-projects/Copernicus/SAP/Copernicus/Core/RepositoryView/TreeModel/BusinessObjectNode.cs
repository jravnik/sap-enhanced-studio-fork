﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.Core.RepositoryView.TreeModel.BusinessObjectNode
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.Core.Repository.DataModel;
using System.Data;

namespace SAP.Copernicus.Core.RepositoryView.TreeModel
{
  public class BusinessObjectNode : BaseNode
  {
    private string boName;

    public override string BOName
    {
      get
      {
        return this.boName;
      }
    }

    public string BOProxyName { get; set; }

    public BusinessObjectNode(RepositoryDataSet.BusinessObjectsRow boRow)
      : base((DataRow) boRow)
    {
      this.boName = boRow.Name;
      this.BOProxyName = boRow.ProxyName;
      this.Text = this.boName;
      this.ImageIndex = 6;
      this.SelectedImageIndex = 6;
    }

    public RepositoryDataSet.BusinessObjectsRow getData()
    {
      return (RepositoryDataSet.BusinessObjectsRow) this.content;
    }

    public override NodeType getNodeType()
    {
      return NodeType.BusinessObject;
    }
  }
}
