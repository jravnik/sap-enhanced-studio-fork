﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.DynamicContextMenuManager
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using Microsoft.VisualStudio.Shell;
using SAP.Copernicus.Administration;
using System;
using System.ComponentModel.Design;

namespace SAP.Copernicus
{
  public class DynamicContextMenuManager
  {
    private static OleMenuCommand _signToBackendCommand;
    private static OleMenuCommand _signOffBackendCommand;
    private static OleMenuCommand _openUIDesigner;
    private static OleMenuCommand _logonToBrowser;
    private static OleMenuCommand _SaveActivateCommand;
    private static OleMenuCommand _ShowSolExpCommand;

    public static OleMenuCommand SignToBackendCommand
    {
      get
      {
        return DynamicContextMenuManager._signToBackendCommand;
      }
    }

    public static OleMenuCommand SignOffBackendCommand
    {
      get
      {
        return DynamicContextMenuManager._signOffBackendCommand;
      }
    }

    public static OleMenuCommand OpenUIDesigner
    {
      get
      {
        return DynamicContextMenuManager._openUIDesigner;
      }
    }

    public static OleMenuCommand LogonToBrowser
    {
      get
      {
        return DynamicContextMenuManager._logonToBrowser;
      }
    }

    public static OleMenuCommand SaveActivateCommand
    {
      get
      {
        return DynamicContextMenuManager._SaveActivateCommand;
      }
    }

    public static OleMenuCommand ShowSolExpCommand
    {
      get
      {
        return DynamicContextMenuManager._ShowSolExpCommand;
      }
    }

    public static void InitializeContextMenus(OleMenuCommandService mcs)
    {
      DynamicContextMenuManager.createSAPMenuCommandOnTopToolbar(mcs);
      DynamicContextMenuManager.createSoltionExplorerToolbar(mcs);
      DynamicContextMenuManager.createContextMenuItemsforBCONode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforBONode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforPINode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforXBONode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforEmbComponentNode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforUINode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforFormNode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforEditableNode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforProjectNode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforTaskNode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforKeyUser(mcs);
      DynamicContextMenuManager.createContextMenuItemsforAccessRights(mcs);
      DynamicContextMenuManager.createContextMenuItemsforMCGetInstallationKey(mcs);
      DynamicContextMenuManager.createContextMenuItemsforImplemetationProjectTemplateNode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforMDRONode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforCSD(mcs);
      DynamicContextMenuManager.createContextMenuItemsforWSDefinitionNode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforWSConsumption(mcs);
      DynamicContextMenuManager.createContextMenuItemsforAnalyticalObject(mcs);
      DynamicContextMenuManager.createContextMenuItemsforTranslationNode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforCustomReuseLibraryNode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforScriptNode(mcs);
      DynamicContextMenuManager.createContextMenuItemsforXODataNode(mcs);
    }

    private static void createContextMenuItemsforXODataNode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.XODataOpen), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetXODataNode, 1862));
      oleMenuCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand);
    }

    public static void createContextMenuItemsforBCONode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ConvertToCodelist), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBCONode, 1312));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateBCSetFromPartnerBCO), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBCONode, 272));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
      OleMenuCommand oleMenuCommand3 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateBCViewFromPartnerBCO), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBCONode, 273));
      oleMenuCommand3.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand3);
    }

    public static void createContextMenuItemsforImplemetationProjectTemplateNode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ImplementationProjectTemplateOverviewHandler), new CommandID(GuidList.guidcmdSetBCImplProjectTempl, 769));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ImplementationProjectTemplateCompleteHandler), new CommandID(GuidList.guidcmdSetBCImplProjectTempl, 770));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
      OleMenuCommand oleMenuCommand3 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ImplementationProjectTemplateReOpenHandler), new CommandID(GuidList.guidcmdSetBCImplProjectTempl, 771));
      oleMenuCommand3.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand3);
    }

    private static void createSAPMenuCommandOnTopToolbar(OleMenuCommandService mcs)
    {
      DynamicContextMenuManager._signToBackendCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.SignToBackendHandler), new CommandID(GuidList.guidSAPMenuEntryCmdSet, 32785));
      DynamicContextMenuManager._signToBackendCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySAPMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) DynamicContextMenuManager._signToBackendCommand);
      DynamicContextMenuManager._signOffBackendCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.SignOffBackendHandler), new CommandID(GuidList.guidSAPMenuEntryCmdSet, 32801));
      DynamicContextMenuManager._signOffBackendCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySAPMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) DynamicContextMenuManager._signOffBackendCommand);
      DynamicContextMenuManager._openUIDesigner = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.OpenUIDesignerHandler), new CommandID(GuidList.guidSAPMenuEntryCmdSet, 36999));
      DynamicContextMenuManager._openUIDesigner.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySAPMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) DynamicContextMenuManager._openUIDesigner);
      DynamicContextMenuManager._logonToBrowser = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.LogonToBrowserHandler), new CommandID(GuidList.guidSAPMenuEntryCmdSet, 37001));
      DynamicContextMenuManager._logonToBrowser.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySAPMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) DynamicContextMenuManager._logonToBrowser);
      DynamicContextMenuManager._SaveActivateCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.SaveActivateHandler), new CommandID(GuidList.guidCSaveActivateCmdSet, 257));
      DynamicContextMenuManager._SaveActivateCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySAPMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) DynamicContextMenuManager._SaveActivateCommand);
      DynamicContextMenuManager._ShowSolExpCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ShowSolExpHandler), new CommandID(GuidList.guidcmdidShowSolExpCmdSet, 369));
      DynamicContextMenuManager._ShowSolExpCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySAPMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) DynamicContextMenuManager._ShowSolExpCommand);
      OleMenuCommand oleMenuCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CallVS), new CommandID(GuidList.guidSAPMenuEntryCmdSet, 307));
      oleMenuCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySAPMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand);
    }

    public static void createContextMenuItemsforTranslationNode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ExportUITransText), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetTranslationNode, 13668));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ImportUITransText), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetTranslationNode, 13669));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
      OleMenuCommand oleMenuCommand3 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CheckUITransText), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetTranslationNode, 13670));
      oleMenuCommand3.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand3);
      OleMenuCommand oleMenuCommand4 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ExportSolTransText), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13671));
      oleMenuCommand4.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand4);
      OleMenuCommand oleMenuCommand5 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ImportSolTransText), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13672));
      oleMenuCommand5.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand5);
      OleMenuCommand oleMenuCommand6 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CheckSolTransText), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13673));
      oleMenuCommand6.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand6);
    }

    public static void createSoltionExplorerToolbar(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.RefreshSolution), new CommandID(GuidList.guidCopernicusCmdSet, 401));
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.AddNewItem), new CommandID(GuidList.guidCopernicusCmdSet, 402));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpToolbarCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
      OleMenuCommand oleMenuCommand3 = new OleMenuCommand(new EventHandler(MigrateSolutionHandler.MigrateSolution), new CommandID(GuidList.guidCopernicusCmdSet, 403));
      oleMenuCommand3.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpToolbarCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand3);
      OleMenuCommand oleMenuCommand4 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.SearchSolution), new CommandID(GuidList.guidCopernicusCmdSet, 405));
      oleMenuCommand4.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpToolbarCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand4);
      OleMenuCommand oleMenuCommand5 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreatePatchOpen), new CommandID(GuidList.guidCopernicusCmdSet, 406));
      oleMenuCommand5.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpToolbarCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand5);
      OleMenuCommand oleMenuCommand6 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreatePatchClose), new CommandID(GuidList.guidCopernicusCmdSet, 407));
      oleMenuCommand6.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpToolbarCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand6);
    }

    public static void createContextMenuItemsforEmbComponentNode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.EmbCompUploadDLL), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetEmbComponentNode, 352));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.EmbCompEdit), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetEmbComponentNode, 353));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
      OleMenuCommand oleMenuCommand3 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.EmbCompDelete), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetEmbComponentNode, 354));
      oleMenuCommand3.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand3);
    }

    public static void createContextMenuItemsforXBONode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.EnhScreen), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetXBONode, 261));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateXBOScriptFiles), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetXBONode, 306));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
      OleMenuCommand oleMenuCommand3 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.EnhanceForm), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetXBONode, 320));
      oleMenuCommand3.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand3);
      OleMenuCommand oleMenuCommand4 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.EnhanceReports), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetXBONode, 321));
      oleMenuCommand4.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand4);
      OleMenuCommand oleMenuCommand5 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.EnhanceEnterpriseSearch), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetXBONode, 322));
      oleMenuCommand5.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand5);
      OleMenuCommand oleMenuCommand6 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.MigrateXbo), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetXBONode, 323));
      oleMenuCommand6.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand6);
    }

    public static void createContextMenuItemsforScriptNode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.RevertProdChange), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetScriptNode, 2457));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.PerformanceCheck), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetScriptNode, 24580));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
    }

    public static void createContextMenuItemsforMDRONode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateMDROScr), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetMDRONode, 1298));
      oleMenuCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand);
    }

    public static void createContextMenuItemsforCustomReuseLibraryNode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.AddCustomReuseFunction), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetCustomReuseLibrary, 257));
      oleMenuCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand);
    }

    public static void createContextMenuItemsforBONode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateScriptFiles), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 260));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.GenerateBOService), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 4129));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
      OleMenuCommand oleMenuCommand3 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateUI), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 277));
      oleMenuCommand3.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand3);
      OleMenuCommand oleMenuCommand4 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateApprovalTask), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 279));
      oleMenuCommand4.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand4);
      OleMenuCommand oleMenuCommand5 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateMSApprovalTask), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 535));
      oleMenuCommand5.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand5);
      OleMenuCommand oleMenuCommand6 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.AddBusinessTask), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 281));
      oleMenuCommand6.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand6);
      OleMenuCommand oleMenuCommand7 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateProcessIntegration), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 288));
      oleMenuCommand7.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand7);
      OleMenuCommand oleMenuCommand8 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateQD), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 292));
      oleMenuCommand8.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand8);
      OleMenuCommand oleMenuCommand9 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.EnableNR), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 313));
      oleMenuCommand9.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand9);
      OleMenuCommand oleMenuCommand10 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateMDRO), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 1299));
      oleMenuCommand10.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand10);
      OleMenuCommand oleMenuCommand11 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateDataSource), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 336));
      oleMenuCommand11.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand11);
      OleMenuCommand oleMenuCommand12 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.PreviewQuery), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 296));
      oleMenuCommand12.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand12);
      OleMenuCommand oleMenuCommand13 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateReportQuery), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 297));
      oleMenuCommand13.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand13);
      OleMenuCommand oleMenuCommand14 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ManageReportsQuery), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 304));
      oleMenuCommand14.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand14);
      OleMenuCommand oleMenuCommand15 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.GenericActivate), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetGenericActivate, 24577));
      oleMenuCommand15.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand15);
      OleMenuCommand oleMenuCommand16 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.GenericCheck), new CommandID(GuidList.guidcmdSetGenericActionCmdSet, 24579));
      oleMenuCommand16.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand16);
      OleMenuCommand oleMenuCommand17 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.GenericClean), new CommandID(GuidList.guidcmdSetGenericActionCmdSet, 24578));
      oleMenuCommand17.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand17);
      OleMenuCommand oleMenuCommand18 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateForm), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 278));
      oleMenuCommand18.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand18);
      OleMenuCommand oleMenuCommand19 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ExecuteQuery), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 4471));
      oleMenuCommand19.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand19);
      OleMenuCommand oleMenuCommand20 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateTTReference), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 1313));
      oleMenuCommand20.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand20);
      OleMenuCommand oleMenuCommand21 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateMappingScript), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetBONode, 4472));
      oleMenuCommand21.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand21);
    }

    public static void createContextMenuItemsforPINode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ConfigurePI), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetPINode, 312));
      oleMenuCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand);
    }

    public static void createContextMenuItemsforUINode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.OpenFloorPlan), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetEditScreen, 293));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.RunFloorPlan), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetUINode, 294));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
      OleMenuCommand oleMenuCommand3 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateUIScriptFile), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetUINode, 13665));
      oleMenuCommand3.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand3);
    }

    public static void createContextMenuItemsforFormNode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.OpenAdobeDesigner), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetOpenFormNode, 4403));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ManageFTV), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetFormNode, 4408));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
      OleMenuCommand oleMenuCommand3 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CheckEFECompliance), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetFormNode, 4409));
      oleMenuCommand3.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand3);
    }

    public static void createContextMenuItemsforEditableNode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.EditContent), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetEditableNode, 8504));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ActivateContent), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetEditableNode, 8505));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
    }

    public static void createContextMenuItemsforKeyUser(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.SwitchKeyUser), new CommandID(GuidList.guidSAPMenuEntryCmdSet, 20481));
      oleMenuCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySAPMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand);
    }

    public static void createContextMenuItemsforAccessRights(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.UpdateAccessRights), new CommandID(GuidList.guidSAPMenuEntryCmdSet, 28673));
      oleMenuCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySAPMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand);
    }

    public static void createContextMenuItemsforMCGetInstallationKey(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.MCGetInstallationKey), new CommandID(GuidList.guidSAPMenuEntryCmdSet, 20483));
      oleMenuCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySAPMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand);
    }

    public static void createContextMenuItemsforProjectNode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ActivateAll), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 12608));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ActivateSolution), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13681));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
      OleMenuCommand oleMenuCommand3 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ActivateSolutionDelta), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13682));
      oleMenuCommand3.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand3);
      OleMenuCommand oleMenuCommand4 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CheckSolutionItems), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13696));
      oleMenuCommand4.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand4);
      OleMenuCommand oleMenuCommand5 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CheckSolutionItemsAndRuntime), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13697));
      oleMenuCommand5.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand5);
      OleMenuCommand oleMenuCommand6 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CheckHTML5), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13701));
      oleMenuCommand6.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand6);
      OleMenuCommand oleMenuCommand7 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.EnableBackgroundMode), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13698));
      oleMenuCommand7.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand7);
      OleMenuCommand oleMenuCommand8 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.DisableBackgroundMode), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13700));
      oleMenuCommand8.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand8);
      OleMenuCommand oleMenuCommand9 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.Displaylogs), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13699));
      oleMenuCommand9.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand9);
      OleMenuCommand oleMenuCommand10 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CleanUpSolution), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 12612));
      oleMenuCommand10.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand10);
      OleMenuCommand oleMenuCommand11 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.DeployBC), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetProjectNode, 13680));
      oleMenuCommand11.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand11);
    }

    public static void createContextMenuItemsforTaskNode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.AddTaskExeUI), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetApprovalNode, 310));
      oleMenuCommand.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand);
    }

    public static void createContextMenuItemsforCSD(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateCommunicationSystem), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetCSDNode, 368));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateCommunicationArrangement), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetCSDNode, 369));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
    }

    public static void createContextMenuItemsforWSDefinitionNode(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.DownloadWSDL), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetWEBSRVNode, 4181));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateWsAuthFile), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetWEBSRVNode, 5476));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
    }

    public static void createContextMenuItemsforWSConsumption(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.CreateCSDFile), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetWSIDNode, 5480));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.ShowWsidTestTool), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetWSIDNode, 5490));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
    }

    public static void createContextMenuItemsforAnalyticalObject(OleMenuCommandService mcs)
    {
      OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.AnalyticalObjectHandler), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetOpenKeyUserUI, 5481));
      oleMenuCommand1.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand1);
      OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(ContextMenuCommandHandler.PreviewReport), new CommandID(GuidList.guidDynamicMenuDevelopmentCmdSetOpenKeyUserUI, 5488));
      oleMenuCommand2.BeforeQueryStatus += new EventHandler(ContextMenuCommandHandler.QuerySolExpMenuCommand_BeforeQueryStatus);
      mcs.AddCommand((MenuCommand) oleMenuCommand2);
    }
  }
}
