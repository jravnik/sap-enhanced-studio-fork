﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.CopernicusPackage
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.ComponentModelHost;
using Microsoft.VisualStudio.Modeling.Shell;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SAP.Copernicus.ABAPScriptLanguage.Debugger;
using SAP.Copernicus.ABAPScriptLanguage.DumpAnalysis;
using SAP.Copernicus.ABAPScriptLanguage.SignatureAdornment;
using SAP.Copernicus.BOCompiler.Common;
using SAP.Copernicus.BOXSGeneration.BO_Service;
using SAP.Copernicus.BOXSGeneration.ExternalUIEditor;
using SAP.Copernicus.BusinessObjectLanguage;
using SAP.Copernicus.Core;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.ErrorList;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol.Update;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Settings;
using SAP.Copernicus.Core.Util;
using SAP.Copernicus.CustomReuseLibrary.FileHandler;
using SAP.Copernicus.Extension;
using SAP.Copernicus.PrintForms.FormUtil;
using SAP.Copernicus.QueryDefinition;
using SAP.CopernicusProjectView;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SAP.Copernicus
{
  [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
  [ProvideMenuResource(1000, 1)]
  [PackageRegistration(UseManagedResourcesOnly = true)]
  [ProvideToolWindow(typeof (RepositoryViewWindow))]
  [ProvideAutoLoad("ADFC4E64-0397-11D1-9F4E-00A0C911004F")]
  [ProvideLoadKey("Standard", "1.0", "Copernicus", "SAP AG", 113)]
  [ProvideToolWindow(typeof (CopernicusBusinessObjectBrowserWindow))]
  [ProvideToolWindow(typeof (DumpAnalysisWindow))]
  [ProvideOptionPage(typeof (CopernicusOptionPage), "SAP", "General", 1, 2, true)]
  [ProvideCommandLineSwitch("trace_file", "trace file name", Arguments = 1)]
  [Guid("a9ad806d-b284-4c78-ac4b-2b7781dd894a")]
  public sealed class CopernicusPackage : Package
  {
    private IVsSelectionEvents mySelectionEvents;
    private uint mySelectionEventsCookie;
    private CopernicusErrorListProvider errorListProvider;
    private BOFileHandler fileEventHandler;
    private CRLFileHandler crlFileEventHandler;
    private readonly DevTrace trace;

    public CopernicusPackage()
    {
      this.trace = DevTrace.Start();
    }

    private void ShowToolWindow(object sender, EventArgs e)
    {
      ToolWindowPane toolWindow = this.FindToolWindow(typeof (RepositoryViewWindow), 0, true);
      if (toolWindow == null || toolWindow.Frame == null)
        throw new NotSupportedException(CopernicusResources.CanNotCreateWindow);
      ErrorHandler.ThrowOnFailure(((IVsWindowFrame) toolWindow.Frame).Show());
    }

    private void ShowBusinessObjectBrowser(object sender, EventArgs e)
    {
      ToolWindowPane toolWindow = this.FindToolWindow(typeof (CopernicusBusinessObjectBrowserWindow), 0, true);
      if (toolWindow == null || toolWindow.Frame == null)
        throw new NotSupportedException(CopernicusResources.CanNotCreateWindow);
      ErrorHandler.ThrowOnFailure(((IVsWindowFrame) toolWindow.Frame).Show());
    }

    public DTE2 GetDTE()
    {
      DTE2 service = (DTE2) this.GetService(typeof (SDTE));
      if (service == null)
        throw new InvalidOperationException("Unable to get DTE service.");
      return service;
    }

    protected override void Initialize()
    {
      base.Initialize();
      CopernicusOptionPage dialogPage1 = (CopernicusOptionPage) this.GetDialogPage(typeof (CopernicusOptionPage));
      dialogPage1.Initialize();
      PropertyAccess.GeneralProps = (OptionPageFields) dialogPage1;
      OptionPageFields generalProps = PropertyAccess.GeneralProps;
      Trace.WriteLine(string.Format("Copernicus build {0} codeline {1} from {2} started on {3} {4} at {5}", (object) generalProps.buildNumber, (object) generalProps.codeline, (object) generalProps.buildDate, (object) Environment.OSVersion.VersionString, Environment.Is64BitOperatingSystem ? (object) "64 bit" : (object) "32 bit", (object) DateTime.Now));
      CompositionContainer compositionContainer = (CompositionContainer) null;
      IComponentModel service1 = this.GetService(typeof (SComponentModel)) as IComponentModel;
      if (service1 != null)
        compositionContainer = new CompositionContainer(service1.DefaultCatalog, new ExportProvider[0]);
      VSPackageUtil.CompositionContainer = compositionContainer;
      VSPackageUtil.ServiceProvider = (IServiceProvider) this;
      IVsShell service2 = this.GetService(typeof (SVsShell)) as IVsShell;
      if (service2 == null)
        return;
      int pfInstalled = 0;
      Guid guidPackage = new Guid("56072b29-f62b-4274-8ed5-a26458487f07");
      service2.IsPackageInstalled(ref guidPackage, out pfInstalled);
      VSPackageUtil.VsTestHostInstalled = pfInstalled == 1;
      Application.EnableVisualStyles();
      DTEUtil.Initialize(new DTEUtil.GetDteDelegate(this.GetDTE));
      NodeEventHandlerRegistry.setDoubleClickDelegateforNodeExtn(".ui", new NodeEventHandlerRegistry.NodeDoubleClickDelegate(ContextMenuCommandHandler.OpenUIFloorPlan_UIDesigner));
      Trace.WriteLine(string.Format((IFormatProvider) CultureInfo.CurrentCulture, "Entering constructor for: {0}", new object[1]
      {
        (object) this.ToString()
      }));
      NodeEventHandlerRegistry.setDoubleClickDelegateforNodeExtn(".report", new NodeEventHandlerRegistry.NodeDoubleClickDelegate(KeyUserUtil.handleDoubleClick));
      NodeEventHandlerRegistry.setDoubleClickDelegateforNodeExtn(".kf", new NodeEventHandlerRegistry.NodeDoubleClickDelegate(KeyUserUtil.handleDoubleClick));
      NodeEventHandlerRegistry.setDoubleClickDelegateforNodeExtn(".jds", new NodeEventHandlerRegistry.NodeDoubleClickDelegate(KeyUserUtil.handleDoubleClick));
      NodeEventHandlerRegistry.setDoubleClickDelegateforNodeExtn(".cds", new NodeEventHandlerRegistry.NodeDoubleClickDelegate(KeyUserUtil.handleDoubleClick));
      NodeEventHandlerRegistry.setDoubleClickDelegateforNodeExtn(".xodata", new NodeEventHandlerRegistry.NodeDoubleClickDelegate(ContextMenuCommandHandler.XOdataDoubleClick));
      CopernicusOptionPage dialogPage2 = (CopernicusOptionPage) this.GetDialogPage(typeof (CopernicusOptionPage));
      OleMenuCommandService service3 = this.GetService(typeof (IMenuCommandService)) as OleMenuCommandService;
      if (service3 != null)
      {
        MenuCommand command1 = new MenuCommand(new EventHandler(this.ShowToolWindow), new CommandID(GuidList.guidCopernicusCmdSet, 257));
        service3.AddCommand(command1);
        MenuCommand command2 = new MenuCommand(new EventHandler(this.ShowBusinessObjectBrowser), new CommandID(GuidList.guidCopernicusCmdSet, 258));
        service3.AddCommand(command2);
        DynamicContextMenuManager.InitializeContextMenus(service3);
        service3.AddCommand(new MenuCommand(new EventHandler(this.scnHandler), new CommandID(GuidList.guidcmdSetHelpMenuSCN, 36869))
        {
          Visible = true,
          Enabled = true
        });
        service3.AddCommand(new MenuCommand(new EventHandler(this.checkForUpdateHandler), new CommandID(GuidList.guidcmdSetHelpMenuCheckForUpdate, 39320))
        {
          Visible = true,
          Enabled = false
        });
        MenuCommand command3 = new MenuCommand(new EventHandler(this.aboutHandler), new CommandID(GuidList.guidcmdSetHelpMenuAbout, 39321));
        command3.Enabled = true;
        command3.Visible = true;
        service3.AddCommand(command3);
        MenuCommand command4 = new MenuCommand(new EventHandler(this.helpHandler), new CommandID(GuidList.guidcmdSetHelpMenuBydStudio, 36865));
        command3.Enabled = true;
        command3.Visible = true;
        service3.AddCommand(command4);
        MenuCommand command5 = new MenuCommand(new EventHandler(this.helpContentInstallationHandler), new CommandID(GuidList.guidcmdSetHelpMenuBydStudio, 36867));
        service3.AddCommand(command5);
        MenuCommand command6 = new MenuCommand(new EventHandler(this.optionSAPHandler), new CommandID(GuidList.guidSAPMenuEntryCmdSet, 37009));
        service3.AddCommand(command6);
        ASDebuggerCommandHandler debuggerCommandHandler = new ASDebuggerCommandHandler((Package) this);
        CommandID id1 = new CommandID(GuidList.guidABSLDebuggerCmdSet, 256);
        OleMenuCommand oleMenuCommand1 = new OleMenuCommand(new EventHandler(debuggerCommandHandler.StartOrContinue), id1);
        oleMenuCommand1.BeforeQueryStatus += new EventHandler(debuggerCommandHandler.StartOrContinue_BeforeQueryStatus);
        service3.AddCommand((MenuCommand) oleMenuCommand1);
        CommandID id2 = new CommandID(GuidList.guidABSLDebuggerCmdSet, 257);
        OleMenuCommand oleMenuCommand2 = new OleMenuCommand(new EventHandler(debuggerCommandHandler.StepOver), id2);
        oleMenuCommand2.BeforeQueryStatus += new EventHandler(debuggerCommandHandler.StepOver_BeforeQueryStatus);
        service3.AddCommand((MenuCommand) oleMenuCommand2);
        OleMenuCommand oleMenuCommand3 = new OleMenuCommand(new EventHandler(this.showABSLSignatureAdornmentHandler), new CommandID(GuidList.guidABSLSignatureAdornmentCmdSet, 38145));
        oleMenuCommand3.BeforeQueryStatus += new EventHandler(this.menuABSLSignatureAdornment_BeforeQueryStatus);
        oleMenuCommand3.Enabled = true;
        oleMenuCommand3.Visible = true;
        oleMenuCommand3.Checked = true;
        service3.AddCommand((MenuCommand) oleMenuCommand3);
        CommandID id3 = new CommandID(GuidList.guidABSLDebuggerCmdSet, 258);
        OleMenuCommand oleMenuCommand4 = new OleMenuCommand(new EventHandler(debuggerCommandHandler.DumpAnalysis), id3);
        service3.AddCommand((MenuCommand) oleMenuCommand4);
      }
      this.mySelectionEvents = (IVsSelectionEvents) new SelectionEvents();
      ((IVsMonitorSelection) this.GetService(typeof (SVsShellMonitorSelection))).AdviseSelectionEvents(this.mySelectionEvents, out this.mySelectionEventsCookie);
      this.errorListProvider = CopernicusErrorListProvider.GetInstance((IServiceProvider) this);
      CompilerEventRegistry.INSTANCE.AddHandler(new SAP.Copernicus.BOCompiler.Common.EventHandlerOnClearErrorList(this.errorListProvider.ClearErrors));
      CompilerEventRegistry.INSTANCE.AddHandler(new SAP.Copernicus.BOCompiler.Common.EventHandlerOnUpdateErrorList(this.errorListProvider.UpdateErrorList));
      ErrorSink.Instance.AddHandler(new SAP.Copernicus.Core.ErrorList.EventHandlerOnClearErrorList(this.errorListProvider.ClearErrorList));
      ErrorSink.Instance.AddHandler(new SAP.Copernicus.Core.ErrorList.EventHandlerOnUpdateErrorList(this.errorListProvider.UpdateErrorList));
      this.fileEventHandler = BOFileHandler.Instance;
      FileHandlerRegistry.INSTANCE.AddHandler("bo", new EventHandlerOnFileDeletion(this.fileEventHandler.OnFileDeletion));
      FileHandlerRegistry.INSTANCE.AddHandler("bo", new EventHandlerOnFileSave(this.fileEventHandler.OnFileSave));
      this.crlFileEventHandler = CRLFileHandler.Instance;
      FileHandlerRegistry.INSTANCE.AddHandler("library", new EventHandlerOnFileSave(this.crlFileEventHandler.OnFileSave));
      FileHandlerRegistry.INSTANCE.AddPostActivateHandler("wsid", new EventHandlerPostGenricActivate2(SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.Util.PostActivateEventHandler));
      FileHandlerRegistry.INSTANCE.AddPostCleanHandler("wsid", new EventHandlerPostGenricClean2(SAP.Copernicus.ProcessIntegration.B2B_WebService_Integration.Util.PostCleanEventHandler));
      FileHandlerRegistry.INSTANCE.AddPostActivateHandler("webservice", new EventHandlerPostGenricActivate2(WebServiceUtil.updateLocalXrepFile));
      FileHandlerRegistry.INSTANCE.AddPreActivateHandler("webservice", new EventHandlerPreGenericActivate(WebServiceUtil.HandlePreWebServiceActivate));
      FileHandlerRegistry.INSTANCE.AddPostActivateHandler("wsauth", new EventHandlerPostGenricActivate2(ExtUIEditorUtil.postActivateForWSAuthFile));
      FileHandlerRegistry.INSTANCE.AddPostCleanHandler("wsauth", new EventHandlerPostGenricClean2(ExtUIEditorUtil.postCleanHandlerForWSAuth));
      FileHandlerRegistry.INSTANCE.AddPreCheckHandler("bo", new EventHandlerPreGenericCheck(this.fileEventHandler.HandleBOPreCheck));
      FileHandlerRegistry.INSTANCE.AddPreActivateHandler("bo", new EventHandlerPreGenericActivate(this.fileEventHandler.HandleBOPreActivate));
      FileHandlerRegistry.INSTANCE.AddPreActivateHandler("xbo", new EventHandlerPreGenericActivate(XBOFileHandler.Instance.OnFileActivate));
      FileHandlerRegistry.INSTANCE.AddPreCheckHandler("xbo", new EventHandlerPreGenericCheck(XBOFileHandler.Instance.OnFileCheck));
      FileHandlerRegistry.INSTANCE.AddPreActivateHandler("fthd", new EventHandlerPreGenericActivate(FormTemplateHeader.Instance.OnPreActivate));
      FileHandlerRegistry.INSTANCE.AddPreCheckHandler("fthd", new EventHandlerPreGenericCheck(FormTemplateHeader.Instance.OnPreCheck));
      CopernicusDependentFileNode.prepareNodeDeletion = new CopernicusDependentFileNode.PrepareNodeDeletion(QueryDefinitionUtil.DeleteQRTN);
      CopernicusStatusBar.Instance = (IStatus) new CopernicusStatusBar(this.GetDTE());
      CopernicusOutputWindow.Instance = (IOutputWindow) new CopernicusOutputWindow(this.GetDTE());
      SignatureProviderFactory.Instance.LibraryFunctionSignatureProvider = (ISignatureProvider) new SAP.Copernicus.CustomReuseLibrary.SignatureProvider.SignatureProvider();
    }

    private void menuABSLSignatureAdornment_BeforeQueryStatus(object sender, EventArgs e)
    {
      (sender as OleMenuCommand).Checked = SettingsAccess.Instance.ShowABSLSignatureAdornment;
    }

    protected override void Dispose(bool disposing)
    {
      IVsMonitorSelection service = (IVsMonitorSelection) this.GetService(typeof (SVsShellMonitorSelection));
      if (service != null)
        service.UnadviseSelectionEvents(this.mySelectionEventsCookie);
      if (this.errorListProvider != null)
      {
        CompilerEventRegistry.INSTANCE.RemoveHandler(new SAP.Copernicus.BOCompiler.Common.EventHandlerOnClearErrorList(this.errorListProvider.ClearErrors));
        CompilerEventRegistry.INSTANCE.RemoveHandler(new SAP.Copernicus.BOCompiler.Common.EventHandlerOnUpdateErrorList(this.errorListProvider.UpdateErrorList));
      }
      if (this.fileEventHandler != null)
      {
        FileHandlerRegistry.INSTANCE.RemoveHandler(new EventHandlerOnFileDeletion(this.fileEventHandler.OnFileDeletion));
        FileHandlerRegistry.INSTANCE.RemoveHandler(new EventHandlerOnFileSave(this.fileEventHandler.OnFileSave));
      }
      FileHandlerRegistry.INSTANCE.RemovePreActivateHandler(new EventHandlerPreGenericActivate(this.fileEventHandler.HandleBOPreActivate));
      FileHandlerRegistry.INSTANCE.RemovePreCheckHandler(new EventHandlerPreGenericCheck(this.fileEventHandler.HandleBOPreCheck));
      if (this.crlFileEventHandler != null)
        FileHandlerRegistry.INSTANCE.RemoveHandler(new EventHandlerOnFileSave(this.crlFileEventHandler.OnFileSave));
      if (this.trace != null)
        this.trace.Stop();
      base.Dispose(disposing);
    }

    private void scnHandler(object sender, EventArgs e)
    {
      try
      {
        ExternalApplication.LaunchExternalBrowser(PropertyAccess.GeneralProps.CsnUrl);
      }
      catch (Exception ex)
      {
      }
    }

    private void checkForUpdateHandler(object sender, EventArgs e)
    {
      new UpdateHandler(true).StartCheckForUpdate();
    }

    private void aboutHandler(object sender, EventArgs e)
    {
      try
      {
        int num = (int) new SAP.Copernicus.AboutBox.AboutBox().ShowDialog();
      }
      catch (Exception ex)
      {
      }
    }

    private void showABSLSignatureAdornmentHandler(object sender, EventArgs e)
    {
      try
      {
        SettingsAccess.Instance.ShowABSLSignatureAdornment = !SettingsAccess.Instance.ShowABSLSignatureAdornment;
        List<string> filePaths = new List<string>();
        foreach (Document document in this.GetDTE().Documents)
        {
          if (document.FullName.EndsWith(".absl"))
            filePaths.Add(document.FullName);
        }
        if (filePaths.Count <= 0)
          return;
        SignatureInvalidationManager.Instance.InvalidateSignaturesForFiles(filePaths);
      }
      catch (Exception ex)
      {
      }
    }

    private void helpHandler(object sender, EventArgs e)
    {
      try
      {
        ExternalApplication.LaunchExternalBrowser(PropertyAccess.GeneralProps.HelpURL);
      }
      catch (Exception ex)
      {
      }
    }

    private void helpContentInstallationHandler(object sender, EventArgs e)
    {
      try
      {
        ExternalApplication.LaunchExternalBrowser(PropertyAccess.GeneralProps.HelpContentInstallationURL);
      }
      catch (Exception ex)
      {
      }
    }

    private void optionSAPHandler(object sender, EventArgs e)
    {
      try
      {
        this.ShowOptionPage(typeof (CopernicusOptionPage));
      }
      catch (Exception ex)
      {
      }
    }
  }
}
