﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.AboutBox.AboutBox
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace SAP.Copernicus.AboutBox
{
  internal class AboutBox : Form
  {
    private IContainer components;
    private Button btnOK;
    private Label labelBuildNumber;
    private Label labelBuildDate;
    private TextBox buildNumber;
    private TextBox buildDate;
    private TextBox textBoxCopyrightSAP;
    private Label labelRepositoryVersion;
    private TextBox repositoryVersion;
    private TextBox logonPatchLevel;
    private Label labelPatchLevel;
    private Label labelRelease;
    private TextBox release;

    public string AssemblyTitle
    {
      get
      {
        object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof (AssemblyTitleAttribute), false);
        if (customAttributes.Length > 0)
        {
          AssemblyTitleAttribute assemblyTitleAttribute = (AssemblyTitleAttribute) customAttributes[0];
          if (assemblyTitleAttribute.Title != "")
            return assemblyTitleAttribute.Title;
        }
        return Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
      }
    }

    public string AssemblyVersion
    {
      get
      {
        return Assembly.GetExecutingAssembly().GetName().Version.ToString();
      }
    }

    public string AssemblyDescription
    {
      get
      {
        object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof (AssemblyDescriptionAttribute), false);
        if (customAttributes.Length == 0)
          return "";
        return ((AssemblyDescriptionAttribute) customAttributes[0]).Description;
      }
    }

    public string AssemblyProduct
    {
      get
      {
        object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof (AssemblyProductAttribute), false);
        if (customAttributes.Length == 0)
          return "";
        return ((AssemblyProductAttribute) customAttributes[0]).Product;
      }
    }

    public string AssemblyCopyright
    {
      get
      {
        object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof (AssemblyCopyrightAttribute), false);
        if (customAttributes.Length == 0)
          return "";
        return ((AssemblyCopyrightAttribute) customAttributes[0]).Copyright;
      }
    }

    public string AssemblyCompany
    {
      get
      {
        object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof (AssemblyCompanyAttribute), false);
        if (customAttributes.Length == 0)
          return "";
        return ((AssemblyCompanyAttribute) customAttributes[0]).Company;
      }
    }

    public AboutBox()
    {
      this.InitializeComponent();
      this.GetTexts();
    }

    private void GetTexts()
    {
      this.labelBuildDate.Text = AboutBoxTexts.GetString("labelBuildNumber");
      this.labelBuildDate.Text = AboutBoxTexts.GetString("labelBuildDate");
      this.textBoxCopyrightSAP.Text = AboutBoxTexts.GetString("copyright2010SAP");
      this.labelPatchLevel.Text = AboutBoxTexts.GetString("LogonPatchLevel");
      this.labelRepositoryVersion.Text = AboutBoxTexts.GetString("RepositoryVersion");
      this.buildNumber.Text = AboutBoxTexts.GetBuildNumber;
      this.buildDate.Text = AboutBoxTexts.GetBuildDate;
      this.logonPatchLevel.Text = AboutBoxTexts.GetLogonPatchLevel;
      this.repositoryVersion.Text = AboutBoxTexts.GetRepositoryVersion;
      this.release.Text = AboutBoxTexts.GetRelease;
      this.Text = AboutBoxTexts.GetString("titleAboutBox");
      this.btnOK.Text = AboutBoxTexts.GetString("Btn_Ok");
    }

    private void AboutBox_Load(object sender, EventArgs e)
    {
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      this.labelBuildNumber.Font.Dispose();
      this.labelBuildDate.Font.Dispose();
      this.labelRepositoryVersion.Font.Dispose();
      this.labelPatchLevel.Font.Dispose();
      this.labelRelease.Font.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (SAP.Copernicus.AboutBox.AboutBox));
      this.btnOK = new Button();
      this.labelBuildNumber = new Label();
      this.labelBuildDate = new Label();
      this.buildNumber = new TextBox();
      this.buildDate = new TextBox();
      this.textBoxCopyrightSAP = new TextBox();
      this.labelRepositoryVersion = new Label();
      this.repositoryVersion = new TextBox();
      this.logonPatchLevel = new TextBox();
      this.labelPatchLevel = new Label();
      this.labelRelease = new Label();
      this.release = new TextBox();
      this.SuspendLayout();
      this.btnOK.Location = new Point(321, 340);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new Size(75, 23);
      this.btnOK.TabIndex = 0;
      this.btnOK.Text = "OK";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new EventHandler(this.btnOK_Click);
      this.labelBuildNumber.AutoSize = true;
      this.labelBuildNumber.BackColor = SystemColors.Window;
      this.labelBuildNumber.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.labelBuildNumber.Location = new Point(23, 183);
      this.labelBuildNumber.Name = "labelBuildNumber";
      this.labelBuildNumber.Size = new Size(39, 13);
      this.labelBuildNumber.TabIndex = 1;
      this.labelBuildNumber.Text = "Build:";
      this.labelBuildDate.AutoSize = true;
      this.labelBuildDate.BackColor = SystemColors.Window;
      this.labelBuildDate.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.labelBuildDate.Location = new Point(23, 196);
      this.labelBuildDate.Name = "labelBuildDate";
      this.labelBuildDate.Size = new Size(70, 13);
      this.labelBuildDate.TabIndex = 2;
      this.labelBuildDate.Text = "Build Date:";
      this.buildNumber.BackColor = SystemColors.Window;
      this.buildNumber.BorderStyle = BorderStyle.None;
      this.buildNumber.Location = new Point(141, 170);
      this.buildNumber.Name = "buildNumber";
      this.buildNumber.ReadOnly = true;
      this.buildNumber.Size = new Size(100, 13);
      this.buildNumber.TabIndex = 3;
      this.buildDate.BackColor = SystemColors.Window;
      this.buildDate.BorderStyle = BorderStyle.None;
      this.buildDate.Location = new Point(141, 196);
      this.buildDate.Name = "buildDate";
      this.buildDate.ReadOnly = true;
      this.buildDate.Size = new Size(140, 13);
      this.buildDate.TabIndex = 4;
      this.textBoxCopyrightSAP.BackColor = SystemColors.Window;
      this.textBoxCopyrightSAP.BorderStyle = BorderStyle.None;
      this.textBoxCopyrightSAP.Location = new Point(26, 249);
      this.textBoxCopyrightSAP.Multiline = true;
      this.textBoxCopyrightSAP.Name = "textBoxCopyrightSAP";
      this.textBoxCopyrightSAP.ReadOnly = true;
      this.textBoxCopyrightSAP.Size = new Size(192, 30);
      this.textBoxCopyrightSAP.TabIndex = 6;
      this.labelRepositoryVersion.AutoSize = true;
      this.labelRepositoryVersion.BackColor = SystemColors.Window;
      this.labelRepositoryVersion.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.labelRepositoryVersion.Location = new Point(23, 209);
      this.labelRepositoryVersion.Name = "labelRepositoryVersion";
      this.labelRepositoryVersion.Size = new Size(117, 13);
      this.labelRepositoryVersion.TabIndex = 7;
      this.labelRepositoryVersion.Text = "Repository Version:";
      this.repositoryVersion.BackColor = SystemColors.Window;
      this.repositoryVersion.BorderStyle = BorderStyle.None;
      this.repositoryVersion.Location = new Point(141, 209);
      this.repositoryVersion.Name = "repositoryVersion";
      this.repositoryVersion.ReadOnly = true;
      this.repositoryVersion.Size = new Size(100, 13);
      this.repositoryVersion.TabIndex = 8;
      this.logonPatchLevel.BackColor = SystemColors.Window;
      this.logonPatchLevel.BorderStyle = BorderStyle.None;
      this.logonPatchLevel.Location = new Point(141, 222);
      this.logonPatchLevel.Name = "logonPatchLevel";
      this.logonPatchLevel.ReadOnly = true;
      this.logonPatchLevel.Size = new Size(100, 13);
      this.logonPatchLevel.TabIndex = 10;
      this.labelPatchLevel.AutoSize = true;
      this.labelPatchLevel.BackColor = SystemColors.Window;
      this.labelPatchLevel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.labelPatchLevel.Location = new Point(23, 222);
      this.labelPatchLevel.Name = "labelPatchLevel";
      this.labelPatchLevel.Size = new Size(118, 13);
      this.labelPatchLevel.TabIndex = 9;
      this.labelPatchLevel.Text = "Logon Patch Level:";
      this.labelRelease.AutoSize = true;
      this.labelRelease.BackColor = SystemColors.Window;
      this.labelRelease.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.labelRelease.Location = new Point(23, 170);
      this.labelRelease.Name = "labelRelease";
      this.labelRelease.Size = new Size(53, 13);
      this.labelRelease.TabIndex = 11;
      this.labelRelease.Text = "Version:";
      this.release.BackColor = SystemColors.Window;
      this.release.BorderStyle = BorderStyle.None;
      this.release.Location = new Point(140, 170);
      this.release.Name = "release";
      this.release.ReadOnly = true;
      this.release.Size = new Size(141, 13);
      this.release.TabIndex = 12;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackgroundImage = (Image) componentResourceManager.GetObject("$this.BackgroundImage");
      this.BackgroundImageLayout = ImageLayout.None;
      this.ClientSize = new Size(431, 385);
      this.Controls.Add((Control) this.labelRelease);
      this.Controls.Add((Control) this.release);
      this.Controls.Add((Control) this.logonPatchLevel);
      this.Controls.Add((Control) this.labelPatchLevel);
      this.Controls.Add((Control) this.repositoryVersion);
      this.Controls.Add((Control) this.labelRepositoryVersion);
      this.Controls.Add((Control) this.textBoxCopyrightSAP);
      this.Controls.Add((Control) this.buildDate);
      this.Controls.Add((Control) this.buildNumber);
      this.Controls.Add((Control) this.labelBuildDate);
      this.Controls.Add((Control) this.labelBuildNumber);
      this.Controls.Add((Control) this.btnOK);
      this.FormBorderStyle = FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "AboutBox";
      this.Padding = new Padding(9);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Load += new EventHandler(this.AboutBox_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
