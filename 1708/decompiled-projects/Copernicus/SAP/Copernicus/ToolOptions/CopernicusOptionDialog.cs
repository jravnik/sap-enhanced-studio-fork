﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ToolOptions.CopernicusOptionDialog
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using SAP.Copernicus.BOCompiler.Common;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Project;
using SAP.Copernicus.Core.Properties;
using SAP.Copernicus.Core.Protocol;
using SAP.Copernicus.Core.Protocol.ConnectionModel;
using SAP.Copernicus.Core.Protocol.JSON.Handlers;
using SAP.Copernicus.Core.Protocol.Update;
using SAP.Copernicus.Core.Repository;
using SAP.Copernicus.Core.Util;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace SAP.Copernicus.ToolOptions {
    public class CopernicusOptionDialog : UserControl {
        private IContainer components;
        private GroupBox groupBoxConnectivity;
        private GroupBox groupBoxSystems;
        private Button buttonImport;
        private Button buttonExport;
        private Button buttonDiscard;
        private Button buttonSave;
        private Button buttonUp;
        private ImageList imageList;
        private Button buttonDelete;
        private Button buttonDown;
        private CheckBox checkBoxMinimalSAP;
        private TabPage tabPageBuildInfo;
        private TabPage tabPageConnectivity;
        private TabPage tabPageDebugging;
        private TextBox textBoxTaktNumber;
        private TextBox textBoxRelease;
        private Label label11;
        private TextBox textBoxBuildNumber;
        private Label label8;
        private TextBox textBoxBuildDate;
        private Label label9;
        private Label label10;
        private TabPage tabPageContent;
        private CheckBox checkBoxUseHTTPS;
        private CheckBox checkBoxUseHTTPProxy;
        private Label label1;
        private TextBox textBoxProxyURL;
        private CheckBox checkBoxUseProxyAuth;
        private Label label5;
        private Label label2;
        private TextBox textBoxProxyUser;
        private TextBox textBoxProxyPwd;
        private DataGridView dataGridViewConnections;
        private Button buttonClearErrorList;
        private Button button1;
        private TabPage tabPageTools;
        private TabControl tabPage;
        private Label lblVSIntroTxt;
        private Button btnVSPath;
        private GroupBox grpVSConf;
        private TextBox txtVSText;
        private CheckBox checkBoxTraceContent;
        private Label labelContentTraceLocation;
        private TextBox textBoxContentTraceLocation;
        private TabPage tabPageTracing;
        private TextBox textBoxTracingUser;
        private CheckBox checkBoxTracingActiveForUser;
        private GroupBox groupBoxDebuggingAndTracing;
        private GroupBox groupBoxDesignedFor;
        private Label labelRepositoryVersion;
        private TextBox textBoxRepositoryRelease;
        private TextBox textBoxRepositoryPatchLevel;
        private Label labelLogonPatchLevel;
        private Button buttonSolutionContentImport;
        private Button buttonSolutionContentExport;
        private Button buttonDRTContentExport;
        private DataGridViewTextBoxColumn NameColumn;
        private DataGridViewTextBoxColumn HostColumn;
        private DataGridViewTextBoxColumn PortColumn;
        private DataGridViewTextBoxColumn SSLColumn;
        private DataGridViewTextBoxColumn SysNoColumn;
        private DataGridViewTextBoxColumn ClientColumn;
        private DataGridViewTextBoxColumn LanguageColumn;
        private DataGridViewTextBoxColumn GroupColumn;
        private DataGridViewTextBoxColumn DefaultUserColumn;
        private ToolTip toolTip1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private GroupBox groupBoxDebugging;
        private ComboBox comboBoxSaveRefreshActivate;
        private Label labelBeforeStartingDebugger;
        private TabPage tabPageUpdate;
        private GroupBox groupBox1;
        private CheckBox checkBoxAutoUpdateWanted;
        private Label labelDays;
        private NumericUpDown numericUpDownDays;
        public CopernicusOptionPage OptionsPage;
        private TabPage DebugTab;

        public CopernicusOptionDialog(CopernicusOptionPage optionsPage) {
            this.InitializeComponent();
            this.OptionsPage = optionsPage;
        }

        protected override void Dispose(bool disposing) {
            if (disposing && this.components != null)
                this.components.Dispose();
            this.grpVSConf.Font.Dispose();
            this.txtVSText.Font.Dispose();
            this.btnVSPath.Font.Dispose();
            this.lblVSIntroTxt.Font.Dispose();
            this.groupBoxDebugging.Font.Dispose();
            this.comboBoxSaveRefreshActivate.Font.Dispose();
            this.groupBoxDebuggingAndTracing.Font.Dispose();
            this.checkBoxTracingActiveForUser.Font.Dispose();
            this.textBoxTracingUser.Font.Dispose();
            this.groupBox1.Font.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent() {
            this.components = (IContainer) new Container();
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(CopernicusOptionDialog));
            this.groupBoxConnectivity = new GroupBox();
            this.checkBoxUseProxyAuth = new CheckBox();
            this.checkBoxUseHTTPProxy = new CheckBox();
            this.label5 = new Label();
            this.label2 = new Label();
            this.label1 = new Label();
            this.textBoxProxyUser = new TextBox();
            this.textBoxProxyPwd = new TextBox();
            this.textBoxProxyURL = new TextBox();
            this.checkBoxMinimalSAP = new CheckBox();
            this.groupBoxSystems = new GroupBox();
            this.dataGridViewConnections = new DataGridView();
            this.NameColumn = new DataGridViewTextBoxColumn();
            this.HostColumn = new DataGridViewTextBoxColumn();
            this.PortColumn = new DataGridViewTextBoxColumn();
            this.SSLColumn = new DataGridViewTextBoxColumn();
            this.SysNoColumn = new DataGridViewTextBoxColumn();
            this.ClientColumn = new DataGridViewTextBoxColumn();
            this.LanguageColumn = new DataGridViewTextBoxColumn();
            this.GroupColumn = new DataGridViewTextBoxColumn();
            this.DefaultUserColumn = new DataGridViewTextBoxColumn();
            this.buttonDelete = new Button();
            this.imageList = new ImageList(this.components);
            this.buttonDown = new Button();
            this.buttonUp = new Button();
            this.buttonImport = new Button();
            this.buttonExport = new Button();
            this.buttonDiscard = new Button();
            this.buttonSave = new Button();
            this.tabPage = new TabControl();
            this.tabPageBuildInfo = new TabPage();
            this.groupBoxDesignedFor = new GroupBox();
            this.labelLogonPatchLevel = new Label();
            this.labelRepositoryVersion = new Label();
            this.textBoxRepositoryRelease = new TextBox();
            this.textBoxRepositoryPatchLevel = new TextBox();
            this.textBoxTaktNumber = new TextBox();
            this.textBoxRelease = new TextBox();
            this.label11 = new Label();
            this.textBoxBuildNumber = new TextBox();
            this.label8 = new Label();
            this.textBoxBuildDate = new TextBox();
            this.label9 = new Label();
            this.label10 = new Label();
            this.tabPageConnectivity = new TabPage();
            this.checkBoxUseHTTPS = new CheckBox();
            this.tabPageContent = new TabPage();
            this.labelContentTraceLocation = new Label();
            this.textBoxContentTraceLocation = new TextBox();
            this.checkBoxTraceContent = new CheckBox();
            this.button1 = new Button();
            this.tabPageTools = new TabPage();
            this.grpVSConf = new GroupBox();
            this.txtVSText = new TextBox();
            this.btnVSPath = new Button();
            this.lblVSIntroTxt = new Label();
            this.tabPageTracing = new TabPage();
            this.groupBoxDebugging = new GroupBox();
            this.labelBeforeStartingDebugger = new Label();
            this.comboBoxSaveRefreshActivate = new ComboBox();
            this.groupBoxDebuggingAndTracing = new GroupBox();
            this.checkBoxTracingActiveForUser = new CheckBox();
            this.textBoxTracingUser = new TextBox();
            this.tabPageDebugging = new TabPage();
            this.buttonSolutionContentExport = new Button();
            this.buttonSolutionContentImport = new Button();
            this.buttonClearErrorList = new Button();
            this.buttonDRTContentExport = new Button();
            this.tabPageUpdate = new TabPage();
            this.groupBox1 = new GroupBox();
            this.labelDays = new Label();
            this.numericUpDownDays = new NumericUpDown();
            this.checkBoxAutoUpdateWanted = new CheckBox();
            this.toolTip1 = new ToolTip(this.components);
            this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new DataGridViewTextBoxColumn();
            this.groupBoxConnectivity.SuspendLayout();
            this.groupBoxSystems.SuspendLayout();
            ((ISupportInitialize) this.dataGridViewConnections).BeginInit();
            this.tabPage.SuspendLayout();
            this.tabPageBuildInfo.SuspendLayout();
            this.groupBoxDesignedFor.SuspendLayout();
            this.tabPageConnectivity.SuspendLayout();
            this.tabPageContent.SuspendLayout();
            this.tabPageTools.SuspendLayout();
            this.grpVSConf.SuspendLayout();
            this.tabPageTracing.SuspendLayout();
            this.groupBoxDebugging.SuspendLayout();
            this.groupBoxDebuggingAndTracing.SuspendLayout();
            this.tabPageDebugging.SuspendLayout();
            this.tabPageUpdate.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.numericUpDownDays.BeginInit();
            this.SuspendLayout();
            this.groupBoxConnectivity.Controls.Add((Control) this.checkBoxUseProxyAuth);
            this.groupBoxConnectivity.Controls.Add((Control) this.checkBoxUseHTTPProxy);
            this.groupBoxConnectivity.Controls.Add((Control) this.label5);
            this.groupBoxConnectivity.Controls.Add((Control) this.label2);
            this.groupBoxConnectivity.Controls.Add((Control) this.label1);
            this.groupBoxConnectivity.Controls.Add((Control) this.textBoxProxyUser);
            this.groupBoxConnectivity.Controls.Add((Control) this.textBoxProxyPwd);
            this.groupBoxConnectivity.Controls.Add((Control) this.textBoxProxyURL);
            this.groupBoxConnectivity.Location = new Point(0, 187);
            this.groupBoxConnectivity.Name = "groupBoxConnectivity";
            this.groupBoxConnectivity.Size = new Size(385, 73);
            this.groupBoxConnectivity.TabIndex = 0;
            this.groupBoxConnectivity.TabStop = false;
            this.groupBoxConnectivity.Text = "Proxy Settings";
            this.checkBoxUseProxyAuth.AutoSize = true;
            this.checkBoxUseProxyAuth.Location = new Point(8, 42);
            this.checkBoxUseProxyAuth.Name = "checkBoxUseProxyAuth";
            this.checkBoxUseProxyAuth.Size = new Size(123, 17);
            this.checkBoxUseProxyAuth.TabIndex = 9;
            this.checkBoxUseProxyAuth.Text = "Proxy Authentication";
            this.checkBoxUseProxyAuth.UseVisualStyleBackColor = true;
            this.checkBoxUseProxyAuth.CheckedChanged += new EventHandler(this.checkBoxUseProxyAuth_CheckedChanged);
            this.checkBoxUseHTTPProxy.AutoSize = true;
            this.checkBoxUseHTTPProxy.Location = new Point(8, 19);
            this.checkBoxUseHTTPProxy.Name = "checkBoxUseHTTPProxy";
            this.checkBoxUseHTTPProxy.Size = new Size(106, 17);
            this.checkBoxUseHTTPProxy.TabIndex = 7;
            this.checkBoxUseHTTPProxy.Text = "Use HTTP Proxy";
            this.checkBoxUseHTTPProxy.UseVisualStyleBackColor = true;
            this.checkBoxUseHTTPProxy.CheckedChanged += new EventHandler(this.checkBoxUseHTTPProxy_CheckedChanged);
            this.label5.AutoSize = true;
            this.label5.Location = new Point(150, 42);
            this.label5.Name = "label5";
            this.label5.Size = new Size(32, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "User:";
            this.label2.AutoSize = true;
            this.label2.Location = new Point(264, 42);
            this.label2.Name = "label2";
            this.label2.Size = new Size(31, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Pwd:";
            this.label1.AutoSize = true;
            this.label1.Location = new Point(120, 20);
            this.label1.Name = "label1";
            this.label1.Size = new Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Proxy URL:";
            this.textBoxProxyUser.Location = new Point(187, 39);
            this.textBoxProxyUser.Name = "textBoxProxyUser";
            this.textBoxProxyUser.Size = new Size(71, 20);
            this.textBoxProxyUser.TabIndex = 10;
            this.textBoxProxyUser.TextChanged += new EventHandler(this.textBoxProxyUser_TextChanged);
            this.textBoxProxyPwd.Location = new Point(301, 39);
            this.textBoxProxyPwd.Name = "textBoxProxyPwd";
            this.textBoxProxyPwd.PasswordChar = '*';
            this.textBoxProxyPwd.Size = new Size(76, 20);
            this.textBoxProxyPwd.TabIndex = 11;
            this.textBoxProxyPwd.TextChanged += new EventHandler(this.textBoxProxyPwd_TextChanged);
            this.textBoxProxyURL.Location = new Point(187, 17);
            this.textBoxProxyURL.Name = "textBoxProxyURL";
            this.textBoxProxyURL.Size = new Size(190, 20);
            this.textBoxProxyURL.TabIndex = 8;
            this.textBoxProxyURL.TextChanged += new EventHandler(this.textBoxProxyURL_TextChanged);
            this.checkBoxMinimalSAP.AutoSize = true;
            this.checkBoxMinimalSAP.Location = new Point(9, 13);
            this.checkBoxMinimalSAP.Name = "checkBoxMinimalSAP";
            this.checkBoxMinimalSAP.Size = new Size(152, 17);
            this.checkBoxMinimalSAP.TabIndex = 3;
            this.checkBoxMinimalSAP.Text = "Load Minimal SAP Content";
            this.checkBoxMinimalSAP.UseVisualStyleBackColor = true;
            this.checkBoxMinimalSAP.Visible = false;
            this.checkBoxMinimalSAP.CheckStateChanged += new EventHandler(this.checkBoxMinimalSAP_CheckStateChanged);
            this.groupBoxSystems.Controls.Add((Control) this.dataGridViewConnections);
            this.groupBoxSystems.Controls.Add((Control) this.buttonDelete);
            this.groupBoxSystems.Controls.Add((Control) this.buttonDown);
            this.groupBoxSystems.Controls.Add((Control) this.buttonUp);
            this.groupBoxSystems.Controls.Add((Control) this.buttonImport);
            this.groupBoxSystems.Controls.Add((Control) this.buttonExport);
            this.groupBoxSystems.Controls.Add((Control) this.buttonDiscard);
            this.groupBoxSystems.Controls.Add((Control) this.buttonSave);
            this.groupBoxSystems.Location = new Point(0, 28);
            this.groupBoxSystems.Name = "groupBoxSystems";
            this.groupBoxSystems.Size = new Size(385, 153);
            this.groupBoxSystems.TabIndex = 0;
            this.groupBoxSystems.TabStop = false;
            this.groupBoxSystems.Text = "Repository System Connections";
            this.dataGridViewConnections.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewConnections.Columns.AddRange((DataGridViewColumn) this.NameColumn, (DataGridViewColumn) this.HostColumn, (DataGridViewColumn) this.PortColumn, (DataGridViewColumn) this.SSLColumn, (DataGridViewColumn) this.SysNoColumn, (DataGridViewColumn) this.ClientColumn, (DataGridViewColumn) this.LanguageColumn, (DataGridViewColumn) this.GroupColumn, (DataGridViewColumn) this.DefaultUserColumn);
            this.dataGridViewConnections.Location = new Point(7, 19);
            this.dataGridViewConnections.Name = "dataGridViewConnections";
            this.dataGridViewConnections.RowHeadersVisible = false;
            this.dataGridViewConnections.Size = new Size(372, 99);
            this.dataGridViewConnections.TabIndex = 3;
            this.dataGridViewConnections.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.dataGridViewConnections_EditingControlShowing);
            this.NameColumn.HeaderText = "Name";
            this.NameColumn.MaxInputLength = 20;
            this.NameColumn.Name = "NameColumn";
            this.HostColumn.HeaderText = "Host";
            this.HostColumn.Name = "HostColumn";
            this.PortColumn.HeaderText = "Port";
            this.PortColumn.Name = "PortColumn";
            this.PortColumn.Width = 5;
            this.SSLColumn.HeaderText = "SSL";
            this.SSLColumn.Name = "SSLColumn";
            this.SSLColumn.Width = 5;
            this.SysNoColumn.HeaderText = "SysNo";
            this.SysNoColumn.Name = "SysNoColumn";
            this.ClientColumn.HeaderText = "Client";
            this.ClientColumn.Name = "ClientColumn";
            this.LanguageColumn.HeaderText = "Language";
            this.LanguageColumn.Name = "LanguageColumn";
            this.LanguageColumn.Width = 5;
            this.GroupColumn.HeaderText = "Group";
            this.GroupColumn.MaxInputLength = 20;
            this.GroupColumn.Name = "GroupColumn";
            this.DefaultUserColumn.HeaderText = "Default User";
            this.DefaultUserColumn.Name = "DefaultUserColumn";
            this.buttonDelete.ImageIndex = 3;
            this.buttonDelete.ImageList = this.imageList;
            this.buttonDelete.Location = new Point(351, 124);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new Size(28, 23);
            this.buttonDelete.TabIndex = 0;
            this.toolTip1.SetToolTip((Control) this.buttonDelete, "Delete Connection");
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new EventHandler(this.buttonDelete_Click);
            this.imageList.ImageStream = (ImageListStreamer) componentResourceManager.GetObject("imageList.ImageStream");
            this.imageList.TransparentColor = Color.Transparent;
            this.imageList.Images.SetKeyName(0, "garbage.png");
            this.imageList.Images.SetKeyName(1, "arrow_down_green.png");
            this.imageList.Images.SetKeyName(2, "arrow_up_green.png");
            this.imageList.Images.SetKeyName(3, "delete2.png");
            this.buttonDown.ImageIndex = 1;
            this.buttonDown.ImageList = this.imageList;
            this.buttonDown.Location = new Point(317, 124);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new Size(28, 23);
            this.buttonDown.TabIndex = 0;
            this.toolTip1.SetToolTip((Control) this.buttonDown, "Move Down");
            this.buttonDown.UseVisualStyleBackColor = true;
            this.buttonDown.Click += new EventHandler(this.buttonDown_Click);
            this.buttonUp.ImageIndex = 2;
            this.buttonUp.ImageList = this.imageList;
            this.buttonUp.Location = new Point(283, 124);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new Size(28, 23);
            this.buttonUp.TabIndex = 0;
            this.toolTip1.SetToolTip((Control) this.buttonUp, "Move Up");
            this.buttonUp.UseVisualStyleBackColor = true;
            this.buttonUp.Click += new EventHandler(this.buttonUp_Click);
            this.buttonImport.Location = new Point(138, 124);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new Size(60, 23);
            this.buttonImport.TabIndex = 6;
            this.buttonImport.Text = "Import";
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new EventHandler(this.buttonImport_Click);
            this.buttonExport.Location = new Point(204, 124);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new Size(60, 23);
            this.buttonExport.TabIndex = 7;
            this.buttonExport.Text = "Export";
            this.buttonExport.UseVisualStyleBackColor = true;
            this.buttonExport.Click += new EventHandler(this.buttonExport_Click);
            this.buttonDiscard.Location = new Point(72, 124);
            this.buttonDiscard.Name = "buttonDiscard";
            this.buttonDiscard.Size = new Size(60, 23);
            this.buttonDiscard.TabIndex = 5;
            this.buttonDiscard.Text = "Discard";
            this.buttonDiscard.UseVisualStyleBackColor = true;
            this.buttonDiscard.Click += new EventHandler(this.buttonDiscard_Click);
            this.buttonSave.Location = new Point(6, 124);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new Size(60, 23);
            this.buttonSave.TabIndex = 4;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new EventHandler(this.buttonSave_Click);
            this.tabPage.Controls.Add((Control) this.tabPageBuildInfo);
            this.tabPage.Controls.Add((Control) this.tabPageConnectivity);
            this.tabPage.Controls.Add((Control) this.tabPageContent);
            this.tabPage.Controls.Add((Control) this.tabPageTools);
            this.tabPage.Controls.Add((Control) this.tabPageTracing);
            this.tabPage.Controls.Add((Control) this.tabPageDebugging);
            this.tabPage.Controls.Add((Control) this.tabPageUpdate);
            this.tabPage.Location = new Point(0, 0);
            this.tabPage.Name = "tabPage";
            this.tabPage.SelectedIndex = 0;
            this.tabPage.Size = new Size(398, 286);
            this.tabPage.TabIndex = 0;
            this.tabPageBuildInfo.Controls.Add((Control) this.groupBoxDesignedFor);
            this.tabPageBuildInfo.Controls.Add((Control) this.textBoxTaktNumber);
            this.tabPageBuildInfo.Controls.Add((Control) this.textBoxRelease);
            this.tabPageBuildInfo.Controls.Add((Control) this.label11);
            this.tabPageBuildInfo.Controls.Add((Control) this.textBoxBuildNumber);
            this.tabPageBuildInfo.Controls.Add((Control) this.label8);
            this.tabPageBuildInfo.Controls.Add((Control) this.textBoxBuildDate);
            this.tabPageBuildInfo.Controls.Add((Control) this.label9);
            this.tabPageBuildInfo.Controls.Add((Control) this.label10);
            this.tabPageBuildInfo.Location = new Point(4, 22);
            this.tabPageBuildInfo.Name = "tabPageBuildInfo";
            this.tabPageBuildInfo.Size = new Size(390, 260);
            this.tabPageBuildInfo.TabIndex = 0;
            this.tabPageBuildInfo.Text = "Build Information";
            this.tabPageBuildInfo.UseVisualStyleBackColor = true;
            this.groupBoxDesignedFor.Controls.Add((Control) this.labelLogonPatchLevel);
            this.groupBoxDesignedFor.Controls.Add((Control) this.labelRepositoryVersion);
            this.groupBoxDesignedFor.Controls.Add((Control) this.textBoxRepositoryRelease);
            this.groupBoxDesignedFor.Controls.Add((Control) this.textBoxRepositoryPatchLevel);
            this.groupBoxDesignedFor.Location = new Point(9, 80);
            this.groupBoxDesignedFor.Name = "groupBoxDesignedFor";
            this.groupBoxDesignedFor.Size = new Size(366, 79);
            this.groupBoxDesignedFor.TabIndex = 6;
            this.groupBoxDesignedFor.TabStop = false;
            this.groupBoxDesignedFor.Text = "Built for";
            this.labelLogonPatchLevel.AutoSize = true;
            this.labelLogonPatchLevel.Location = new Point(6, 48);
            this.labelLogonPatchLevel.Name = "labelLogonPatchLevel";
            this.labelLogonPatchLevel.Size = new Size(100, 13);
            this.labelLogonPatchLevel.TabIndex = 6;
            this.labelLogonPatchLevel.Text = "Logon Patch Level:";
            this.labelRepositoryVersion.AutoSize = true;
            this.labelRepositoryVersion.Location = new Point(6, 22);
            this.labelRepositoryVersion.Name = "labelRepositoryVersion";
            this.labelRepositoryVersion.Size = new Size(98, 13);
            this.labelRepositoryVersion.TabIndex = 4;
            this.labelRepositoryVersion.Text = "Repository Version:";
            this.textBoxRepositoryRelease.Enabled = false;
            this.textBoxRepositoryRelease.Location = new Point(123, 19);
            this.textBoxRepositoryRelease.Name = "textBoxRepositoryRelease";
            this.textBoxRepositoryRelease.Size = new Size(100, 20);
            this.textBoxRepositoryRelease.TabIndex = 5;
            this.textBoxRepositoryPatchLevel.Enabled = false;
            this.textBoxRepositoryPatchLevel.Location = new Point(123, 45);
            this.textBoxRepositoryPatchLevel.Name = "textBoxRepositoryPatchLevel";
            this.textBoxRepositoryPatchLevel.Size = new Size(100, 20);
            this.textBoxRepositoryPatchLevel.TabIndex = 7;
            this.textBoxTaktNumber.BackColor = SystemColors.Window;
            this.textBoxTaktNumber.Location = new Point(92, 10);
            this.textBoxTaktNumber.Name = "textBoxTaktNumber";
            this.textBoxTaktNumber.ReadOnly = true;
            this.textBoxTaktNumber.Size = new Size(49, 20);
            this.textBoxTaktNumber.TabIndex = 3;
            this.textBoxTaktNumber.Text = "n/a";
            this.textBoxRelease.BackColor = SystemColors.Window;
            this.textBoxRelease.Location = new Point(338, 10);
            this.textBoxRelease.Name = "textBoxRelease";
            this.textBoxRelease.ReadOnly = true;
            this.textBoxRelease.Size = new Size(37, 20);
            this.textBoxRelease.TabIndex = 3;
            this.textBoxRelease.Text = "n/a";
            this.label11.AutoSize = true;
            this.label11.Location = new Point(6, 13);
            this.label11.Name = "label11";
            this.label11.Size = new Size(72, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Takt Number:";
            this.textBoxBuildNumber.BackColor = SystemColors.Window;
            this.textBoxBuildNumber.Location = new Point(338, 41);
            this.textBoxBuildNumber.Name = "textBoxBuildNumber";
            this.textBoxBuildNumber.ReadOnly = true;
            this.textBoxBuildNumber.Size = new Size(37, 20);
            this.textBoxBuildNumber.TabIndex = 3;
            this.textBoxBuildNumber.Text = "n/a";
            this.label8.AutoSize = true;
            this.label8.Location = new Point(6, 44);
            this.label8.Name = "label8";
            this.label8.Size = new Size(59, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Build Date:";
            this.textBoxBuildDate.BackColor = SystemColors.Window;
            this.textBoxBuildDate.Location = new Point(92, 41);
            this.textBoxBuildDate.Name = "textBoxBuildDate";
            this.textBoxBuildDate.ReadOnly = true;
            this.textBoxBuildDate.Size = new Size(140, 20);
            this.textBoxBuildDate.TabIndex = 2;
            this.textBoxBuildDate.Text = "n/a";
            this.label9.AutoSize = true;
            this.label9.Location = new Point(259, 44);
            this.label9.Name = "label9";
            this.label9.Size = new Size(73, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Build Number:";
            this.label10.AutoSize = true;
            this.label10.Location = new Point(259, 13);
            this.label10.Name = "label10";
            this.label10.Size = new Size(51, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Codeline:";
            this.tabPageConnectivity.Controls.Add((Control) this.checkBoxUseHTTPS);
            this.tabPageConnectivity.Controls.Add((Control) this.groupBoxConnectivity);
            this.tabPageConnectivity.Controls.Add((Control) this.groupBoxSystems);
            this.tabPageConnectivity.Location = new Point(4, 22);
            this.tabPageConnectivity.Name = "tabPageConnectivity";
            this.tabPageConnectivity.Size = new Size(390, 260);
            this.tabPageConnectivity.TabIndex = 1;
            this.tabPageConnectivity.Text = "Connectivity";
            this.tabPageConnectivity.UseVisualStyleBackColor = true;
            this.checkBoxUseHTTPS.AutoSize = true;
            this.checkBoxUseHTTPS.Enabled = false;
            this.checkBoxUseHTTPS.Location = new Point(7, 11);
            this.checkBoxUseHTTPS.Name = "checkBoxUseHTTPS";
            this.checkBoxUseHTTPS.Size = new Size(84, 17);
            this.checkBoxUseHTTPS.TabIndex = 1;
            this.checkBoxUseHTTPS.Text = "Use HTTPS";
            this.checkBoxUseHTTPS.UseVisualStyleBackColor = true;
            this.tabPageContent.Controls.Add((Control) this.labelContentTraceLocation);
            this.tabPageContent.Controls.Add((Control) this.textBoxContentTraceLocation);
            this.tabPageContent.Controls.Add((Control) this.checkBoxTraceContent);
            this.tabPageContent.Controls.Add((Control) this.button1);
            this.tabPageContent.Controls.Add((Control) this.checkBoxMinimalSAP);
            this.tabPageContent.Location = new Point(4, 22);
            this.tabPageContent.Name = "tabPageContent";
            this.tabPageContent.Size = new Size(390, 260);
            this.tabPageContent.TabIndex = 3;
            this.tabPageContent.Text = "Runtime Metadata";
            this.tabPageContent.UseVisualStyleBackColor = true;
            this.labelContentTraceLocation.AutoSize = true;
            this.labelContentTraceLocation.Location = new Point(8, 143);
            this.labelContentTraceLocation.Name = "labelContentTraceLocation";
            this.labelContentTraceLocation.Size = new Size(82, 13);
            this.labelContentTraceLocation.TabIndex = 9;
            this.labelContentTraceLocation.Text = "Trace Location:";
            this.labelContentTraceLocation.Visible = false;
            this.textBoxContentTraceLocation.Location = new Point(96, 140);
            this.textBoxContentTraceLocation.Name = "textBoxContentTraceLocation";
            this.textBoxContentTraceLocation.Size = new Size(190, 20);
            this.textBoxContentTraceLocation.TabIndex = 10;
            this.textBoxContentTraceLocation.Visible = false;
            this.textBoxContentTraceLocation.TextChanged += new EventHandler(this.textBoxContentTraceLocation_TextChanged);
            this.checkBoxTraceContent.AutoSize = true;
            this.checkBoxTraceContent.Location = new Point(9, 117);
            this.checkBoxTraceContent.Name = "checkBoxTraceContent";
            this.checkBoxTraceContent.Size = new Size(120, 17);
            this.checkBoxTraceContent.TabIndex = 5;
            this.checkBoxTraceContent.Text = "Trace Write Access";
            this.checkBoxTraceContent.UseVisualStyleBackColor = true;
            this.checkBoxTraceContent.Visible = false;
            this.checkBoxTraceContent.CheckedChanged += new EventHandler(this.checkBoxTraceContent_CheckedChanged);
            this.button1.Location = new Point(9, 36);
            this.button1.Name = "button1";
            this.button1.Size = new Size(110, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Clear Cache";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new EventHandler(this.buttonClearCache_click);
            this.tabPageTools.Controls.Add((Control) this.grpVSConf);
            this.tabPageTools.Location = new Point(4, 22);
            this.tabPageTools.Name = "tabPageTools";
            this.tabPageTools.Size = new Size(390, 260);
            this.tabPageTools.TabIndex = 4;
            this.tabPageTools.Text = "External Tools";
            this.tabPageTools.UseVisualStyleBackColor = true;
            this.grpVSConf.Controls.Add((Control) this.txtVSText);
            this.grpVSConf.Controls.Add((Control) this.btnVSPath);
            this.grpVSConf.Controls.Add((Control) this.lblVSIntroTxt);
            this.grpVSConf.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
            this.grpVSConf.Location = new Point(7, 20);
            this.grpVSConf.Name = "grpVSConf";
            this.grpVSConf.Size = new Size(362, 84);
            this.grpVSConf.TabIndex = 4;
            this.grpVSConf.TabStop = false;
            this.grpVSConf.Text = "Visual Studio 2010";
            this.txtVSText.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
            this.txtVSText.Location = new Point(6, 53);
            this.txtVSText.Name = "txtVSText";
            this.txtVSText.Size = new Size(285, 20);
            this.txtVSText.TabIndex = 3;
            this.btnVSPath.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
            this.btnVSPath.Location = new Point(297, 53);
            this.btnVSPath.Name = "btnVSPath";
            this.btnVSPath.Size = new Size(59, 21);
            this.btnVSPath.TabIndex = 2;
            this.btnVSPath.Text = "Browse";
            this.btnVSPath.UseVisualStyleBackColor = true;
            this.btnVSPath.Click += new EventHandler(this.btnVSPath_Click);
            this.lblVSIntroTxt.AutoSize = true;
            this.lblVSIntroTxt.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
            this.lblVSIntroTxt.Location = new Point(6, 16);
            this.lblVSIntroTxt.MaximumSize = new Size(350, 30);
            this.lblVSIntroTxt.Name = "lblVSIntroTxt";
            this.lblVSIntroTxt.Size = new Size(349, 26);
            this.lblVSIntroTxt.TabIndex = 1;
            this.lblVSIntroTxt.Text = "Specify the location of Microsoft Visual Studio 2010, which is required for developing custom panes.";
            this.tabPageTracing.Controls.Add((Control) this.groupBoxDebugging);
            this.tabPageTracing.Controls.Add((Control) this.groupBoxDebuggingAndTracing);
            this.tabPageTracing.Location = new Point(4, 22);
            this.tabPageTracing.Name = "tabPageTracing";
            this.tabPageTracing.Size = new Size(390, 260);
            this.tabPageTracing.TabIndex = 5;
            this.tabPageTracing.Text = "Debugging and Tracing";
            this.tabPageTracing.UseVisualStyleBackColor = true;
            this.tabPageTracing.Layout += new LayoutEventHandler(this.tabPageTracing_Layout);
            this.groupBoxDebugging.Controls.Add((Control) this.labelBeforeStartingDebugger);
            this.groupBoxDebugging.Controls.Add((Control) this.comboBoxSaveRefreshActivate);
            this.groupBoxDebugging.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
            this.groupBoxDebugging.Location = new Point(7, 74);
            this.groupBoxDebugging.Name = "groupBoxDebugging";
            this.groupBoxDebugging.Size = new Size(362, 66);
            this.groupBoxDebugging.TabIndex = 14;
            this.groupBoxDebugging.TabStop = false;
            this.groupBoxDebugging.Text = "Debugging";
            this.labelBeforeStartingDebugger.AutoSize = true;
            this.labelBeforeStartingDebugger.Location = new Point(9, 20);
            this.labelBeforeStartingDebugger.Name = "labelBeforeStartingDebugger";
            this.labelBeforeStartingDebugger.Size = new Size(144, 13);
            this.labelBeforeStartingDebugger.TabIndex = 2;
            this.labelBeforeStartingDebugger.Text = "Before starting the debugger:";
            this.comboBoxSaveRefreshActivate.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxSaveRefreshActivate.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
            this.comboBoxSaveRefreshActivate.FormattingEnabled = true;
            this.comboBoxSaveRefreshActivate.Items.AddRange(new object[3]
            {
        (object) "Never save, refresh, and activate",
        (object) "Prompt to save, refresh, and activate",
        (object) "Always save, refresh, and activate"
            });
            this.comboBoxSaveRefreshActivate.Location = new Point(9, 36);
            this.comboBoxSaveRefreshActivate.Name = "comboBoxSaveRefreshActivate";
            this.comboBoxSaveRefreshActivate.Size = new Size(251, 21);
            this.comboBoxSaveRefreshActivate.TabIndex = 1;
            this.comboBoxSaveRefreshActivate.SelectedIndexChanged += new EventHandler(this.comboBoxActivateChanges_SelectedIndexChanged);
            this.groupBoxDebuggingAndTracing.Controls.Add((Control) this.checkBoxTracingActiveForUser);
            this.groupBoxDebuggingAndTracing.Controls.Add((Control) this.textBoxTracingUser);
            this.groupBoxDebuggingAndTracing.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
            this.groupBoxDebuggingAndTracing.Location = new Point(7, 20);
            this.groupBoxDebuggingAndTracing.Name = "groupBoxDebuggingAndTracing";
            this.groupBoxDebuggingAndTracing.RightToLeft = RightToLeft.No;
            this.groupBoxDebuggingAndTracing.Size = new Size(362, 48);
            this.groupBoxDebuggingAndTracing.TabIndex = 13;
            this.groupBoxDebuggingAndTracing.TabStop = false;
            this.groupBoxDebuggingAndTracing.Text = "Debugging and Tracing";
            this.checkBoxTracingActiveForUser.AutoSize = true;
            this.checkBoxTracingActiveForUser.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
            this.checkBoxTracingActiveForUser.Location = new Point(9, 20);
            this.checkBoxTracingActiveForUser.Name = "checkBoxTracingActiveForUser";
            this.checkBoxTracingActiveForUser.Size = new Size(144, 17);
            this.checkBoxTracingActiveForUser.TabIndex = 4;
            this.checkBoxTracingActiveForUser.Text = "Enable for business user:";
            this.checkBoxTracingActiveForUser.UseVisualStyleBackColor = true;
            this.checkBoxTracingActiveForUser.CheckedChanged += new EventHandler(this.checkBoxTracingActiveForUser_CheckedChanged);
            this.textBoxTracingUser.CharacterCasing = CharacterCasing.Upper;
            this.textBoxTracingUser.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
            this.textBoxTracingUser.Location = new Point(153, 19);
            this.textBoxTracingUser.MaxLength = 40;
            this.textBoxTracingUser.Name = "textBoxTracingUser";
            this.textBoxTracingUser.Size = new Size(108, 20);
            this.textBoxTracingUser.TabIndex = 11;
            this.textBoxTracingUser.TextChanged += new EventHandler(this.textBoxTracingUser_TextChanged);
            this.tabPageDebugging.Controls.Add((Control) this.buttonSolutionContentExport);
            this.tabPageDebugging.Controls.Add((Control) this.buttonSolutionContentImport);
            this.tabPageDebugging.Controls.Add((Control) this.buttonClearErrorList);
            this.tabPageDebugging.Controls.Add((Control) this.buttonDRTContentExport);
            this.tabPageDebugging.Location = new Point(4, 22);
            this.tabPageDebugging.Name = "tabPageDebugging";
            this.tabPageDebugging.Size = new Size(390, 260);
            this.tabPageDebugging.TabIndex = 2;
            this.tabPageDebugging.Text = "Debugging";
            this.tabPageDebugging.UseVisualStyleBackColor = true;
            this.buttonSolutionContentExport.Enabled = false;
            this.buttonSolutionContentExport.Location = new Point(3, 80);
            this.buttonSolutionContentExport.Name = "buttonSolutionContentExport";
            this.buttonSolutionContentExport.Size = new Size(131, 23);
            this.buttonSolutionContentExport.TabIndex = 8;
            this.buttonSolutionContentExport.Text = "Export Solution Content";
            this.buttonSolutionContentExport.UseVisualStyleBackColor = true;
            this.buttonSolutionContentExport.Click += new EventHandler(this.buttonSolutionContentExport_Click);
            this.buttonSolutionContentImport.Enabled = false;
            this.buttonSolutionContentImport.Location = new Point(3, 109);
            this.buttonSolutionContentImport.Name = "buttonSolutionContentImport";
            this.buttonSolutionContentImport.Size = new Size(131, 23);
            this.buttonSolutionContentImport.TabIndex = 7;
            this.buttonSolutionContentImport.Text = "Import Solution Content";
            this.buttonSolutionContentImport.UseVisualStyleBackColor = true;
            this.buttonSolutionContentImport.Click += new EventHandler(this.buttonSolutionContentImport_Click);
            this.buttonClearErrorList.Location = new Point(3, 22);
            this.buttonClearErrorList.Name = "buttonClearErrorList";
            this.buttonClearErrorList.Size = new Size(131, 23);
            this.buttonClearErrorList.TabIndex = 5;
            this.buttonClearErrorList.Text = "Clear Global Errors";
            this.buttonClearErrorList.UseVisualStyleBackColor = true;
            this.buttonClearErrorList.Click += new EventHandler(this.buttonClearErrorList_Click);
            this.buttonDRTContentExport.Enabled = false;
            this.buttonDRTContentExport.Location = new Point(3, 51);
            this.buttonDRTContentExport.Name = "buttonDRTContentExport";
            this.buttonDRTContentExport.Size = new Size(131, 23);
            this.buttonDRTContentExport.TabIndex = 9;
            this.buttonDRTContentExport.Text = "Export DRT Content";
            this.buttonDRTContentExport.UseVisualStyleBackColor = true;
            this.buttonDRTContentExport.Click += new EventHandler(this.buttonDRTContentExport_Click);
            this.tabPageUpdate.Controls.Add((Control) this.groupBox1);
            this.tabPageUpdate.Location = new Point(4, 22);
            this.tabPageUpdate.Name = "tabPageUpdate";
            this.tabPageUpdate.Padding = new Padding(3);
            this.tabPageUpdate.Size = new Size(390, 260);
            this.tabPageUpdate.TabIndex = 6;
            this.tabPageUpdate.Text = "Update";
            this.tabPageUpdate.UseVisualStyleBackColor = true;
            this.groupBox1.Controls.Add((Control) this.labelDays);
            this.groupBox1.Controls.Add((Control) this.numericUpDownDays);
            this.groupBox1.Controls.Add((Control) this.checkBoxAutoUpdateWanted);
            this.groupBox1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
            this.groupBox1.Location = new Point(7, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = RightToLeft.No;
            this.groupBox1.Size = new Size(362, 48);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Check for Updates";
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new Point(198, 20);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new Size(38, 13);
            this.labelDays.TabIndex = 16;
            this.labelDays.Text = "day(s).";
            this.labelDays.Visible = false;
            this.numericUpDownDays.Enabled = false;
            this.numericUpDownDays.Location = new Point(152, 18);
            this.numericUpDownDays.Maximum = new Decimal(new int[4]
            {
        14,
        0,
        0,
        0
            });
            this.numericUpDownDays.Minimum = new Decimal(new int[4]
            {
        1,
        0,
        0,
        0
            });
            this.numericUpDownDays.Name = "numericUpDownDays";
            this.numericUpDownDays.Size = new Size(40, 20);
            this.numericUpDownDays.TabIndex = 15;
            this.numericUpDownDays.Tag = (object) "";
            this.numericUpDownDays.Value = new Decimal(new int[4]
            {
        1,
        0,
        0,
        0
            });
            this.numericUpDownDays.Visible = false;
            this.numericUpDownDays.ValueChanged += new EventHandler(this.numericUpDownDays_ValueChanged);
            this.checkBoxAutoUpdateWanted.AutoSize = true;
            this.checkBoxAutoUpdateWanted.Location = new Point(10, 19);
            this.checkBoxAutoUpdateWanted.Name = "checkBoxAutoUpdateWanted";
            this.checkBoxAutoUpdateWanted.Size = new Size(142, 17);
            this.checkBoxAutoUpdateWanted.TabIndex = 11;
            this.checkBoxAutoUpdateWanted.Text = "Check for updates";
            this.checkBoxAutoUpdateWanted.UseVisualStyleBackColor = true;
            this.checkBoxAutoUpdateWanted.CheckedChanged += new EventHandler(this.checkBoxAutoUpdateWanted_CheckedChanged_1);
            this.dataGridViewTextBoxColumn1.HeaderText = "Name";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn2.HeaderText = "Host";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn3.HeaderText = "Port";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 5;
            this.dataGridViewTextBoxColumn4.HeaderText = "SSL";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 5;
            this.dataGridViewTextBoxColumn5.HeaderText = "SysNo";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn6.HeaderText = "Client";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn7.HeaderText = "Language";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 5;
            this.dataGridViewTextBoxColumn8.HeaderText = "Group";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn9.HeaderText = "Default User";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add((Control) this.tabPage);
            this.Name = "CopernicusOptionDialog";
            this.Size = new Size(398, 289);
            this.groupBoxConnectivity.ResumeLayout(false);
            this.groupBoxConnectivity.PerformLayout();
            this.groupBoxSystems.ResumeLayout(false);
            ((ISupportInitialize) this.dataGridViewConnections).EndInit();
            this.tabPage.ResumeLayout(false);
            this.tabPageBuildInfo.ResumeLayout(false);
            this.tabPageBuildInfo.PerformLayout();
            this.groupBoxDesignedFor.ResumeLayout(false);
            this.groupBoxDesignedFor.PerformLayout();
            this.tabPageConnectivity.ResumeLayout(false);
            this.tabPageConnectivity.PerformLayout();
            this.tabPageContent.ResumeLayout(false);
            this.tabPageContent.PerformLayout();
            this.tabPageTools.ResumeLayout(false);
            this.grpVSConf.ResumeLayout(false);
            this.grpVSConf.PerformLayout();
            this.tabPageTracing.ResumeLayout(false);
            this.groupBoxDebugging.ResumeLayout(false);
            this.groupBoxDebugging.PerformLayout();
            this.groupBoxDebuggingAndTracing.ResumeLayout(false);
            this.groupBoxDebuggingAndTracing.PerformLayout();
            this.tabPageDebugging.ResumeLayout(false);
            this.tabPageUpdate.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.numericUpDownDays.EndInit();
            this.ResumeLayout(false);
        }

        protected override void OnLayout(LayoutEventArgs e) {
            this.SetARTExportButtonStatus();
            this.SetDRTExportButtonStatus();
            base.OnLayout(e);
        }

        private void SetARTExportButtonStatus() {
            if (Connection.getInstance().isConnected() && ProjectUtil.GetOpenSolutionName() != null) {
                this.buttonSolutionContentExport.Enabled = true;
                this.buttonSolutionContentImport.Enabled = true;
            } else {
                this.buttonSolutionContentExport.Enabled = false;
                this.buttonSolutionContentImport.Enabled = false;
            }
        }

        private void SetDRTExportButtonStatus() {
            if (this.OptionsPage.buildNumber == null && Connection.getInstance().isConnected() && ProjectUtil.GetOpenSolutionName() != null)
                this.buttonDRTContentExport.Enabled = true;
            else
                this.buttonDRTContentExport.Enabled = false;
        }

        public void Initialize() {
            if (this.OptionsPage.buildTaktNumber != null)
                this.textBoxTaktNumber.Text = this.OptionsPage.buildTaktNumber;
            if (this.OptionsPage.buildNumber != null)
                this.textBoxBuildNumber.Text = this.OptionsPage.buildNumber;
            if (this.OptionsPage.buildDate != null)
                this.textBoxBuildDate.Text = this.OptionsPage.buildDate;
            if (this.OptionsPage.codeline != null)
                this.textBoxRelease.Text = this.OptionsPage.codeline;
            this.textBoxRepositoryRelease.Text = this.OptionsPage.RepositoryVersion;
            this.textBoxRepositoryPatchLevel.Text = this.OptionsPage.LogonPatchLevel;
            this.checkBoxMinimalSAP.Checked = this.OptionsPage.UseMinimalSAPNamespaces;
            this.checkBoxUseHTTPS.Checked = this.OptionsPage.HTTPSSupport;
            this.checkBoxUseHTTPProxy.Checked = this.OptionsPage.ProxySupport;
            this.checkBoxUseProxyAuth.Checked = this.OptionsPage.ProxyAuthentication;
            this.textBoxProxyURL.Text = this.OptionsPage.ProxyURL;
            this.textBoxProxyUser.Text = this.OptionsPage.ProxyUser;
            this.textBoxProxyPwd.Text = SecureStringUtil.decrypt(this.OptionsPage.ProxyPwd);
            this.txtVSText.Text = this.OptionsPage.VSPath;
            this.checkBoxTraceContent.Checked = TraceUtil.Instance.IsOn;
            this.textBoxContentTraceLocation.Text = this.OptionsPage.ContentTraceLocation;
            this.CreateDataGridRows();
            this.ConfigureDataGrid();
            this.checkBoxTracingActiveForUser.Checked = this.OptionsPage.TracingActiveForUser;
            this.textBoxTracingUser.Text = this.OptionsPage.TracingUser;
            if (!this.checkBoxTracingActiveForUser.Checked)
                this.textBoxTracingUser.Enabled = false;
            if (this.IsJaProBuildAndProductiveRelease())
                this.tabPage.TabPages.Remove(this.tabPageDebugging);
            this.tabPage.TabPages.Remove(this.tabPageBuildInfo);
            UpdateHandler updateHandler = new UpdateHandler(false);
            this.checkBoxAutoUpdateWanted.Checked = updateHandler.AutoUpdateWanted;
            if (!updateHandler.AutoUpdateWanted)
                this.numericUpDownDays.Enabled = false;
            else
                this.numericUpDownDays.Enabled = true;
            this.numericUpDownDays.Value = (Decimal) UpdateHandler.UpdateReminderDelayDays;
        }

        private void CreateDataGridRows() {
            this.dataGridViewConnections.Rows.Clear();
            int count = Connection.getInstance().Connections.Tables[0].Rows.Count;
            if (count <= 0)
                return;
            this.dataGridViewConnections.Rows.Add(count);
            int index = 0;
            foreach (DataRow row in (InternalDataCollectionBase) Connection.getInstance().Connections.Tables[0].Rows) {
                this.dataGridViewConnections[0, index].Value = (object) row[0].ToString();
                this.dataGridViewConnections[1, index].Value = (object) row[1].ToString();
                this.dataGridViewConnections[2, index].Value = (object) row[2].ToString();
                this.dataGridViewConnections[3, index].Value = (object) row[3].ToString();
                this.dataGridViewConnections[4, index].Value = (object) row[4].ToString();
                this.dataGridViewConnections[5, index].Value = (object) row[5].ToString();
                this.dataGridViewConnections[6, index].Value = (object) row[6].ToString();
                this.dataGridViewConnections[7, index].Value = (object) row[7].ToString();
                this.dataGridViewConnections[8, index].Value = (object) row[8].ToString();
                ++index;
            }
        }

        private bool IsJaProBuildAndProductiveRelease() {
            bool flag1 = false;
            bool flag2 = false;
            StreamReader streamReader = new StreamReader(Assembly.GetCallingAssembly().GetManifestResourceStream("SAP.Copernicus.Build.version.properties"));
            while (streamReader.Peek() != -1) {
                string str1 = streamReader.ReadLine();
                if (str1 != null && str1.Length > 0 && str1.Contains("=")) {
                    string[] strArray = str1.Split('=');
                    string str2 = strArray[0];
                    string str3 = strArray[1];
                    if (str3 != null && !str3.StartsWith("@") && !str3.StartsWith("$")) {
                        switch (str2) {
                            case "buildNumber":
                                if (str3.Length == 0) {
                                    flag1 = true;
                                    continue;
                                }
                                continue;
                            case "release":
                                flag2 = !"lsfrontend_stream".Equals(str3, StringComparison.InvariantCultureIgnoreCase);
                                continue;
                            default:
                                continue;
                        }
                    }
                }
            }
            bool flag3 = flag1 && flag2;
            BuildInfo.IsProductiveJaProBuild = flag3;
            return flag3;
        }

        [Conditional("DEBUG")]
        private void EnableDebugTab() {
            this.tabPage.TabPages.Insert(2, this.DebugTab);
        }

        [Conditional("DEBUG")]
        private void MakeHiddenFieldsVisible() {
            this.checkBoxTraceContent.Visible = true;
            this.textBoxContentTraceLocation.Visible = true;
            this.labelContentTraceLocation.Visible = true;
        }

        public void ConfigureDataGrid() {
            this.dataGridViewConnections.MultiSelect = false;
            this.dataGridViewConnections.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewConnections.AllowUserToAddRows = true;
            this.dataGridViewConnections.AllowUserToDeleteRows = true;
            this.dataGridViewConnections.AllowUserToOrderColumns = false;
            this.dataGridViewConnections.AllowUserToResizeColumns = true;
            foreach (DataGridViewColumn column in (BaseCollection) this.dataGridViewConnections.Columns)
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewConnections.Columns[0].Width = 150;
            this.dataGridViewConnections.Columns[1].Width = 190;
            this.dataGridViewConnections.Columns[2].Width = 0;
            this.dataGridViewConnections.Columns[2].HeaderText = "Port";
            this.dataGridViewConnections.Columns[2].Visible = false;
            this.dataGridViewConnections.Columns[3].Width = 45;
            this.dataGridViewConnections.Columns[3].HeaderText = "SSL";
            this.dataGridViewConnections.Columns[3].Visible = false;
            this.dataGridViewConnections.Columns[4].Width = 50;
            this.dataGridViewConnections.Columns[4].Visible = false;
            this.dataGridViewConnections.Columns[5].Width = 50;
            this.dataGridViewConnections.Columns[5].Visible = false;
            this.dataGridViewConnections.Columns[6].Width = 40;
            this.dataGridViewConnections.Columns[6].HeaderText = "Lang";
            this.dataGridViewConnections.Columns[6].Visible = false;
            this.dataGridViewConnections.Columns[7].Width = 100;
            this.dataGridViewConnections.Columns[7].HeaderText = "Group";
            this.dataGridViewConnections.Columns[8].Width = 80;
            this.dataGridViewConnections.Columns[8].HeaderText = "Default User";
            this.dataGridViewConnections.DataError += new DataGridViewDataErrorEventHandler(this.HandleInputError);
            this.dataGridViewConnections.CellValidating += new DataGridViewCellValidatingEventHandler(this.CellValidating);
        }

        private void textBoxProxyURL_TextChanged(object sender, EventArgs e) {
            this.OptionsPage.ProxyURL = this.textBoxProxyURL.Text;
        }

        private void textBoxProxyUser_TextChanged(object sender, EventArgs e) {
            this.OptionsPage.ProxyUser = this.textBoxProxyUser.Text;
        }

        private void textBoxProxyPwd_TextChanged(object sender, EventArgs e) {
            this.OptionsPage.ProxyPwd = this.textBoxProxyPwd.Text;
        }

        private void checkBoxMinimalSAP_CheckStateChanged(object sender, EventArgs e) {
            this.OptionsPage.UseMinimalSAPNamespaces = this.checkBoxMinimalSAP.Checked;
        }

        private void checkBoxUseHTTPProxy_CheckedChanged(object sender, EventArgs e) {
            this.OptionsPage.ProxySupport = this.checkBoxUseHTTPProxy.Checked;
        }

        private void checkBoxUseProxyAuth_CheckedChanged(object sender, EventArgs e) {
            this.OptionsPage.ProxyAuthentication = this.checkBoxUseProxyAuth.Checked;
        }

        private void HandleInputError(object source, DataGridViewDataErrorEventArgs args) {
            string str = "Repository system connection field '" + this.dataGridViewConnections.Columns[args.ColumnIndex].Name + "' has numeric type.";
            Trace.TraceError(str);
            int num = (int) CopernicusMessageBox.Show(str, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        private void CellValidating(object sender, DataGridViewCellValidatingEventArgs e) {
            string columnName = Connection.getInstance().Connections.Tables[0].Columns[e.ColumnIndex].ColumnName;
            if (columnName == "Name") {
                string formattedValue = (string) e.FormattedValue;
                if (formattedValue == null || formattedValue.Length <= 0)
                    return;
                string upper = formattedValue.ToUpper();
                if (RepositoryUtil.IsValidNameKeyName(upper)) {
                    ((DataGridView) sender).Rows[e.RowIndex].Cells[0].Value = (object) upper;
                } else {
                    int num = (int) CopernicusMessageBox.Show(Resource.MsgInvalidConnectionName, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    this.dataGridViewConnections.CurrentCell = this.dataGridViewConnections[e.ColumnIndex, e.RowIndex];
                    this.dataGridViewConnections.EditingControl.Text = "";
                    e.Cancel = true;
                }
            } else {
                if (!(columnName == "Host"))
                    return;
                string str = ((string) e.FormattedValue).TrimStart(' ').TrimEnd(' ').Replace("https://", "");
                if (this.dataGridViewConnections.EditingControl == null)
                    return;
                this.dataGridViewConnections.EditingControl.Text = str;
            }
        }

        private void buttonDiscard_Click(object sender, EventArgs e) {
            this.OptionsPage.ReadConnectionDataFromProps();
            this.CreateDataGridRows();
        }

        private void buttonImport_Click(object sender, EventArgs e) {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Title = "Choose Systems File";
            openFileDialog.Filter = "Systems CSV (*.csv)|*.csv";

            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                string importFile = openFileDialog.FileName.ToString();

                DataTable table = Connection.getInstance().Connections.Tables[0];
                table.Clear();

                StreamReader reader = new StreamReader(File.OpenRead(@importFile));

                while (!reader.EndOfStream) {
                    string line = reader.ReadLine();

                    if (!String.IsNullOrWhiteSpace(line)) {
                        string[] values = line.Split(';');

                        if (values.Length >= 4) {
                            DataRow row = table.NewRow();

                            row[0] = values[0];
                            row[1] = values[1];
                            row[2] = (object) "50000";
                            row[3] = (object) "443";
                            row[4] = (object) "0";
                            row[5] = (object) -1;
                            row[6] = (object) "EN";
                            row[7] = values[2];
                            row[8] = values[3];

                            try {
                                table.Rows.Add(row);
                            } catch (ConstraintException ex) {
                                CopernicusMessageBox.Show(Resource.MsgConnectionNameNotUnique, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            }
                        }
                    }
                }

                this.CreateDataGridRows();
                this.OptionsPage.SaveConnectionDataToProps();
            }
        }

        private void buttonExport_Click(object sender, EventArgs e) {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Title = "Save Systems File";
            saveFileDialog.Filter = "Systems CSV (*.csv)|*.csv";

            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                string exportFile = saveFileDialog.FileName.ToString();
                string systems = "";

                foreach (DataGridViewRow row in (IEnumerable) this.dataGridViewConnections.Rows) {
                    if (row.Cells[0].Value != null || row.Cells[1].Value != null) {
                        systems += String.Format("{0};{1};{2};{3}", row.Cells[0].Value, row.Cells[1].Value, row.Cells[7].Value, row.Cells[8].Value);
                        systems += System.Environment.NewLine;
                    }
                }

                File.WriteAllText(@exportFile, systems);
            }
        }

        private void buttonSave_Click(object sender, EventArgs e) {
            foreach (DataGridViewRow row in (IEnumerable) this.dataGridViewConnections.Rows) {
                if (row.Cells[0].Value != null || row.Cells[1].Value != null) {
                    if (row.Cells[0].Value == null) {
                        int num = (int) CopernicusMessageBox.Show(Resource.MsgConnectionName, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        this.dataGridViewConnections.CurrentCell = this.dataGridViewConnections[0, row.Index];
                        return;
                    }
                    if (!RepositoryUtil.IsValidNameKeyName(row.Cells[0].Value.ToString())) {
                        int num = (int) CopernicusMessageBox.Show(Resource.MsgInvalidConnectionName, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        this.dataGridViewConnections.CurrentCell = this.dataGridViewConnections[0, row.Index];
                        return;
                    }
                    if (row.Cells[1].Value == null) {
                        int num = (int) CopernicusMessageBox.Show(Resource.MsgConenctionHost, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        this.dataGridViewConnections.CurrentCell = this.dataGridViewConnections[1, row.Index];
                        return;
                    }
                }
            }
            this.saveConnectionGridToTable();
            this.OptionsPage.SaveConnectionDataToProps();
        }

        private void buttonUp_Click(object sender, EventArgs e) {
            DataGridViewSelectedRowCollection selectedRows = this.dataGridViewConnections.SelectedRows;
            if (selectedRows == null || selectedRows.Count != 1)
                return;
            int index = selectedRows[0].Index;
            if (index >= this.dataGridViewConnections.Rows.Count - 1 || index < 1)
                return;
            DataGridViewRow row = this.dataGridViewConnections.Rows[index];
            this.dataGridViewConnections.Rows.Remove(row);
            this.dataGridViewConnections.Rows.Insert(index - 1, row);
            this.dataGridViewConnections.CurrentCell = this.dataGridViewConnections.Rows[index - 1].Cells[0];
        }

        private void saveConnectionGridToTable() {
            DataTable table = Connection.getInstance().Connections.Tables[0];
            table.Clear();
            foreach (DataGridViewRow row1 in (IEnumerable) this.dataGridViewConnections.Rows) {
                if (row1.Cells[0].Value != null || row1.Cells[1].Value != null) {
                    DataRow row2 = table.NewRow();
                    row2[0] = row1.Cells[0].Value;
                    row2[1] = row1.Cells[1].Value;
                    row2[2] = (object) "50000";
                    row2[3] = (object) "443";
                    row2[4] = (object) "0";
                    row2[5] = (object) -1;
                    row2[6] = (object) "EN";
                    row2[7] = (row1.Cells[7].Value != null ? row1.Cells[7].Value : "Other");
                    row2[8] = (row1.Cells[8].Value != null ? row1.Cells[8].Value : "");
                    try {
                        table.Rows.Add(row2);
                    } catch (ConstraintException ex) {
                        int num = (int) CopernicusMessageBox.Show(Resource.MsgConnectionNameNotUnique, Resource.MsgBYD, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                }
            }
        }

        private void buttonDown_Click(object sender, EventArgs e) {
            DataGridViewSelectedRowCollection selectedRows = this.dataGridViewConnections.SelectedRows;
            if (selectedRows == null || selectedRows.Count != 1)
                return;
            int index = selectedRows[0].Index;
            if (index >= this.dataGridViewConnections.Rows.Count - 2)
                return;
            DataGridViewRow row = this.dataGridViewConnections.Rows[index];
            this.dataGridViewConnections.Rows.Remove(row);
            this.dataGridViewConnections.Rows.Insert(index + 1, row);
            this.dataGridViewConnections.CurrentCell = this.dataGridViewConnections.Rows[index + 1].Cells[0];
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            DataGridViewSelectedRowCollection selectedRows = this.dataGridViewConnections.SelectedRows;
            if (selectedRows == null || selectedRows.Count != 1)
                return;
            int index = selectedRows[0].Index;
            if (selectedRows[0].Index >= this.dataGridViewConnections.Rows.Count - 1)
                return;
            this.dataGridViewConnections.Rows.RemoveAt(index);
        }

        private void buttonClearErrorList_Click(object sender, EventArgs e) {
            CompilerEventRegistry.INSTANCE.RaiseClearErrorListEvent("");
        }

        private void buttonClearCache_click(object sender, EventArgs e) {
            Connection instance = Connection.getInstance();
            if (!instance.isConnected())
                return;
            MDRSHandler.ClearCache(instance.GetConnectionName(), instance.UsePSM);
            ExtensionDataCache.GetInstance().Reset();
        }

        private void btnVSPath_Click(object sender, EventArgs e) {
            this.txtVSText.Text = this.OptionsPage.VSPath;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Choose Visual Studio Path";
            openFileDialog.Filter = "Visual Studio 2010 (devenv.exe)|devenv.exe";
            int num = (int) openFileDialog.ShowDialog();
            this.OptionsPage.VSPath = this.txtVSText.Text = openFileDialog.FileName.ToString();
        }

        private void checkBoxTraceContent_CheckedChanged(object sender, EventArgs e) {
            TraceUtil.Instance.IsOn = this.checkBoxTraceContent.Checked;
        }

        private void textBoxContentTraceLocation_TextChanged(object sender, EventArgs e) {
            this.OptionsPage.ContentTraceLocation = this.textBoxContentTraceLocation.Text;
        }

        private void checkBoxTracingActiveForUser_CheckedChanged(object sender, EventArgs e) {
            this.OptionsPage.TracingActiveForUser = this.checkBoxTracingActiveForUser.Checked;
            if (!this.checkBoxTracingActiveForUser.Checked) {
                this.textBoxTracingUser.Enabled = false;
            } else {
                if (!this.checkBoxTracingActiveForUser.Checked)
                    return;
                this.textBoxTracingUser.Enabled = true;
            }
        }

        private void textBoxTracingUser_TextChanged(object sender, EventArgs e) {
            this.OptionsPage.TracingUser = this.textBoxTracingUser.Text;
        }

        private void buttonSolutionContentExport_Click(object sender, EventArgs e) {
            SolutionContentExport solutionContentExport = new SolutionContentExport();
            if (!solutionContentExport.IsSolutionOpen())
                return;
            int num = (int) solutionContentExport.ShowDialog();
        }

        private void buttonSolutionContentImport_Click(object sender, EventArgs e) {
            SolutionContentImport solutionContentImport = new SolutionContentImport();
            if (!solutionContentImport.IsSolutionOpen())
                return;
            int num = (int) solutionContentImport.ShowDialog();
        }

        private void buttonDRTContentExport_Click(object sender, EventArgs e) {
            DRTContentExport drtContentExport = new DRTContentExport();
            if (!drtContentExport.IsSolutionOpen())
                return;
            int num = (int) drtContentExport.ShowDialog();
        }

        private void checkBoxDebugABSLCompilation_CheckedChanged(object sender, EventArgs e) {
        }

        private void dataGridViewConnections_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e) {
            if (this.dataGridViewConnections.CurrentCell.ColumnIndex == 0)
                ((TextBox) e.Control).CharacterCasing = CharacterCasing.Upper;
            if (this.dataGridViewConnections.CurrentCell.ColumnIndex != 1)
                return;
            ((TextBox) e.Control).CharacterCasing = CharacterCasing.Normal;
        }

        private void comboBoxActivateChanges_SelectedIndexChanged(object sender, EventArgs e) {
            this.OptionsPage.ActivateBeforeDebugging = (OptionsTriState) this.comboBoxSaveRefreshActivate.SelectedIndex;
        }

        private void tabPageTracing_Layout(object sender, LayoutEventArgs e) {
            this.comboBoxSaveRefreshActivate.SelectedIndex = (int) this.OptionsPage.ActivateBeforeDebugging;
        }

        private void checkBoxAutoUpdateWanted_CheckedChanged_1(object sender, EventArgs e) {
            bool flag = this.checkBoxAutoUpdateWanted.Checked;
            new UpdateHandler(false).AutoUpdateWanted = flag;
            if (flag)
                this.numericUpDownDays.Enabled = true;
            else
                this.numericUpDownDays.Enabled = false;
        }

        private void numericUpDownDays_ValueChanged(object sender, EventArgs e) {
            UpdateHandler.UpdateReminderDelayDays = (int) this.numericUpDownDays.Value;
        }
    }
}
