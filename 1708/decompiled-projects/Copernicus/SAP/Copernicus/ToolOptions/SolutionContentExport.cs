﻿// Decompiled with JetBrains decompiler
// Type: SAP.Copernicus.ToolOptions.SolutionContentExport
// Assembly: Copernicus, Version=142.0.3110.706, Culture=neutral, PublicKeyToken=null
// MVID: 2A455149-3177-4417-80EE-C1F743A59301
// Assembly location: C:\Program Files (x86)\SAP\SAP Cloud Applications Studio 1708\Extensions\Application\Copernicus.dll

using EnvDTE;
using SAP.Copernicus.Core.Automation;
using SAP.Copernicus.Core.GUI;
using SAP.Copernicus.Core.Util;
using SAP.CopernicusProjectView;
using SAP.CopernicusProjectView.XRepSynchronizer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SAP.Copernicus.ToolOptions
{
  public class SolutionContentExport : Form
  {
    private static string MainFolder = "Main";
    private static string BCFolder = "BC";
    private static string AttrExt = ".attr";
    private CopernicusProjectNode project;
    private string projectNamespace;
    private string sourceDir;
    private string bcDir;
    private string targetDir;
    private IContainer components;
    private FolderBrowserDialog folderBrowserDialog;
    private TextBox textBoxExportLog;
    private Button s;
    private Label label3;
    private Button buttonExit;
    private Button buttonExport;
    private Label exportSolution;
    private TextBox textBoxTargetDir;
    private Label label2;
    private Label label1;

    public SolutionContentExport()
    {
      this.InitializeComponent();
      this.Icon = SAP.Copernicus.Resource.SAPBusinessByDesignStudioIcon;
      this.folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;
      if (!string.IsNullOrEmpty(this.textBoxTargetDir.Text))
        this.folderBrowserDialog.SelectedPath = this.textBoxTargetDir.Text;
      this.Activated += new EventHandler(this.ActivatedEvent);
    }

    public bool IsSolutionOpen()
    {
      try
      {
        Solution solution = DTEUtil.GetDTE().Solution;
        IEnumerator enumerator = solution.Projects.GetEnumerator();
        try
        {
          if (enumerator.MoveNext())
          {
            this.project = ((Project) enumerator.Current).Object as CopernicusProjectNode;
            this.exportSolution.Text = Path.GetFileNameWithoutExtension(solution.FullName) + " - " + this.project.Caption;
            this.projectNamespace = this.project.RepositoryNamespace;
            return true;
          }
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            disposable.Dispose();
        }
      }
      catch (NullReferenceException ex)
      {
        int num = (int) CopernicusMessageBox.Show("Open the solution from which the content should be exported!");
      }
      return false;
    }

    private void ActivatedEvent(object sender, EventArgs e)
    {
      this.buttonExit.Focus();
    }

    private void buttonExit_Click(object sender, EventArgs e)
    {
      this.Close();
      this.Dispose();
    }

    private void buttonExport_Click(object sender, EventArgs e)
    {
      List<string> stringList = new List<string>();
      this.textBoxExportLog.Lines = new string[0];
      this.Refresh();
      XRepMapper.GetInstance();
      this.sourceDir = this.project.BaseURI.Directory;
      this.bcDir = XRepMapper.GetInstance().GetBCFolderPath();
      this.targetDir = this.textBoxTargetDir.Text;
      if (string.IsNullOrEmpty(this.sourceDir) || string.IsNullOrEmpty(this.targetDir))
        return;
      Stopwatch stopwatch = new Stopwatch();
      stopwatch.Start();
      int num1 = 0;
      if (this.sourceDir != null && Directory.Exists(this.sourceDir))
      {
        stringList.Add("Exporting solution content into folder " + this.targetDir + (object) Path.DirectorySeparatorChar + SolutionContentExport.MainFolder + " ...");
        List<string> filesFromDir = FileUtil.GetFilesFromDir(this.sourceDir, "*.*");
        foreach (string str in filesFromDir)
        {
          ++num1;
          stringList.Add("Exporting (" + string.Concat((object) num1).PadLeft(3, '0') + "/" + string.Concat((object) filesFromDir.Count).PadLeft(3, '0') + "): " + Path.GetFileName(str));
          string source = File.ReadAllText(str);
          IDictionary<string, string> fileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(str);
          string targetFilename = this.GetTargetFilename(str, true);
          this.ExportSource(targetFilename, source);
          if (this.ExportAttributes(targetFilename, fileAttributes))
            stringList.Add("-> Attributes exported.");
          this.textBoxExportLog.Lines = stringList.ToArray();
          this.textBoxExportLog.SelectionStart = this.textBoxExportLog.TextLength;
          this.textBoxExportLog.ScrollToCaret();
          this.Refresh();
        }
      }
      int num2 = 0;
      if (this.bcDir != null && Directory.Exists(this.bcDir))
      {
        stringList.Add("----");
        stringList.Add("Exporting solution business configuration into folder " + this.targetDir + (object) Path.DirectorySeparatorChar + SolutionContentExport.BCFolder + "...");
        List<string> filesFromDir = FileUtil.GetFilesFromDir(this.bcDir, "*.*");
        foreach (string str in filesFromDir)
        {
          ++num2;
          stringList.Add("Exporting (" + string.Concat((object) num2).PadLeft(3, '0') + "/" + string.Concat((object) filesFromDir.Count).PadLeft(3, '0') + "): " + Path.GetFileName(str));
          string source = File.ReadAllText(str);
          IDictionary<string, string> fileAttributes = SAP.CopernicusProjectView.XRepSynchronizer.XRepSynchronizer.GetInstance().getFileAttributes(str);
          string targetFilename = this.GetTargetFilename(str, false);
          this.ExportSource(targetFilename, source);
          if (this.ExportAttributes(targetFilename, fileAttributes))
            stringList.Add("-> Attributes exported.");
          this.textBoxExportLog.Lines = stringList.ToArray();
          this.textBoxExportLog.SelectionStart = this.textBoxExportLog.TextLength;
          this.textBoxExportLog.ScrollToCaret();
          this.Refresh();
        }
      }
      stopwatch.Stop();
      stringList.Add("--------------------");
      stringList.Add("Export complete in " + (object) stopwatch.Elapsed + " (" + (object) num2 + " files).");
      this.textBoxExportLog.Lines = stringList.ToArray();
      this.textBoxExportLog.SelectionStart = this.textBoxExportLog.TextLength;
      this.textBoxExportLog.ScrollToCaret();
      this.Refresh();
    }

    private string GetTargetFilename(string filename, bool main)
    {
      if (main)
      {
        int length = this.sourceDir.Length;
        return this.targetDir + (object) Path.DirectorySeparatorChar + SolutionContentExport.MainFolder + filename.Substring(length);
      }
      int length1 = this.bcDir.Length;
      return this.targetDir + (object) Path.DirectorySeparatorChar + SolutionContentExport.BCFolder + filename.Substring(length1);
    }

    private void ExportSource(string filename, string source)
    {
      Directory.CreateDirectory(Path.GetDirectoryName(filename));
      File.WriteAllText(filename, source, Encoding.UTF8);
    }

    private bool ExportAttributes(string filename, IDictionary<string, string> attributes)
    {
      if (attributes != null)
      {
        string path = filename + SolutionContentExport.AttrExt;
        bool flag = false;
        foreach (string key in (IEnumerable<string>) attributes.Keys)
        {
          if (!key.StartsWith("~"))
          {
            flag = true;
            break;
          }
        }
        if (flag)
        {
          using (StreamWriter text = File.CreateText(path))
          {
            foreach (string key in (IEnumerable<string>) attributes.Keys)
            {
              if (!key.StartsWith("~"))
              {
                string attribute = attributes[key];
                text.WriteLine(key + "=" + attribute);
              }
            }
          }
          return true;
        }
      }
      return false;
    }

    private void buttonSelTarget_Click(object sender, EventArgs e)
    {
      int num = (int) this.folderBrowserDialog.ShowDialog();
      this.textBoxTargetDir.Text = this.folderBrowserDialog.SelectedPath;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.folderBrowserDialog = new FolderBrowserDialog();
      this.textBoxExportLog = new TextBox();
      this.s = new Button();
      this.label3 = new Label();
      this.buttonExit = new Button();
      this.buttonExport = new Button();
      this.exportSolution = new Label();
      this.textBoxTargetDir = new TextBox();
      this.label2 = new Label();
      this.label1 = new Label();
      this.SuspendLayout();
      this.textBoxExportLog.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.textBoxExportLog.BackColor = SystemColors.Window;
      this.textBoxExportLog.Location = new Point(12, 93);
      this.textBoxExportLog.Multiline = true;
      this.textBoxExportLog.Name = "textBoxExportLog";
      this.textBoxExportLog.ReadOnly = true;
      this.textBoxExportLog.ScrollBars = ScrollBars.Both;
      this.textBoxExportLog.Size = new Size(560, 218);
      this.textBoxExportLog.TabIndex = 0;
      this.textBoxExportLog.WordWrap = false;
      this.s.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.s.Location = new Point(538, 33);
      this.s.Name = "s";
      this.s.Size = new Size(34, 23);
      this.s.TabIndex = 4;
      this.s.Text = "...";
      this.s.UseVisualStyleBackColor = true;
      this.s.Click += new EventHandler(this.buttonSelTarget_Click);
      this.label3.AutoSize = true;
      this.label3.Location = new Point(9, 77);
      this.label3.Name = "label3";
      this.label3.Size = new Size(61, 13);
      this.label3.TabIndex = 0;
      this.label3.Text = "Export Log:";
      this.buttonExit.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.buttonExit.Location = new Point(497, 327);
      this.buttonExit.Name = "buttonExit";
      this.buttonExit.Size = new Size(75, 23);
      this.buttonExit.TabIndex = 6;
      this.buttonExit.Text = "Exit";
      this.buttonExit.UseVisualStyleBackColor = true;
      this.buttonExit.Click += new EventHandler(this.buttonExit_Click);
      this.buttonExport.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.buttonExport.Location = new Point(416, 327);
      this.buttonExport.Name = "buttonExport";
      this.buttonExport.Size = new Size(75, 23);
      this.buttonExport.TabIndex = 5;
      this.buttonExport.Text = "Export";
      this.buttonExport.UseVisualStyleBackColor = true;
      this.buttonExport.Click += new EventHandler(this.buttonExport_Click);
      this.exportSolution.AutoSize = true;
      this.exportSolution.Location = new Point(96, 9);
      this.exportSolution.Name = "exportSolution";
      this.exportSolution.Size = new Size(16, 13);
      this.exportSolution.TabIndex = 11;
      this.exportSolution.Text = "---";
      this.textBoxTargetDir.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.textBoxTargetDir.BackColor = SystemColors.Window;
      this.textBoxTargetDir.Location = new Point(99, 35);
      this.textBoxTargetDir.Name = "textBoxTargetDir";
      this.textBoxTargetDir.Size = new Size(429, 20);
      this.textBoxTargetDir.TabIndex = 10;
      this.textBoxTargetDir.Text = "C:\\SolutionContent";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(9, 9);
      this.label2.Name = "label2";
      this.label2.Size = new Size(81, 13);
      this.label2.TabIndex = 8;
      this.label2.Text = "Export Solution:";
      this.label1.AutoSize = true;
      this.label1.Location = new Point(9, 38);
      this.label1.Name = "label1";
      this.label1.Size = new Size(73, 13);
      this.label1.TabIndex = 9;
      this.label1.Text = "Target Folder:";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(584, 362);
      this.Controls.Add((Control) this.exportSolution);
      this.Controls.Add((Control) this.textBoxTargetDir);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.buttonExport);
      this.Controls.Add((Control) this.buttonExit);
      this.Controls.Add((Control) this.s);
      this.Controls.Add((Control) this.textBoxExportLog);
      this.Controls.Add((Control) this.label3);
      this.Name = "SolutionContentExport";
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Solution Content Export";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
